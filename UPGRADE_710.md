# For product teams

## Changed API

To support [PLUGWEB-773](https://ecosystem.atlassian.net/browse/PLUGWEB-773) the `WebResourceIntegration` SPI was extended to expect `com.atlassian.sal.apiApplicationProperties` to be returned from the `getApplicationProperties` function. 
