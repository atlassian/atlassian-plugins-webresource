# Contributing to Web Resource Manager (WRM)

## Prerequisites

You will need the following tools in your development environment:

- Maven 3.6.3
- Java 8+

If you are working on the front-end code and not managing your workflow via Maven, you will need additional tools in
your development environment:

- Node 14.17+ (you can use `nvm`)
- [Yarn 1.22+][yarn]

## Getting started

In the root folder, run `mvn clean install`. This will build all the projects and packages in the repo.

The following commands are available to you:

- `yarn build` - Cleans, installs and builds all packages in the project using Maven.
- `yarn start:refapp` - Runs the project in a Refapp demo instance.
- `yarn test` - Runs tests for all packages in the project.
- `yarn test:js:watch` - Runs tests for all javascript packages in watch mode.

## Repository structure

This repository contains multiple maven projects. Most contain backend code, some contain frontend code.

- `atlassian-plugins-webresource-api` (Java) consumer API. Interfaces for constructing pages and loading resources go
  here.
- `atlassian-plugins-webresource-spi` (Java) service provider interface layer. Product teams implement this in order to
  provide WRM with the data and objects it needs from the product runtime.
- `atlassian-plugins-webresource` (Java) base library implementation. All business logic exists here. Implements the API
  package. Consumes the SPI package.
- `atlassian-plugins-webresource-plugin` (Java, Node, JavaScript) atlassian plugin that implements low-level client-side
  APIs, along with platform-provided web-resource conditions and transformers.
- `atlassian-plugins-webresource-rest` (Java, Node, TypeScript) atlassian plugin that implements REST endpoints for
  inspecting and loading resources from the clientside (asynchronously) at product runtime.
- `atlassian-plugins-webresource-tests` (Java, JavaScript) atlassian plugin containing integration tests to verify behaviours work
  in a running product.

A high-level overview of WRM's architecture:

```
|---------------------------------------|  |----------------------------------------|  
| Layer 5a: URL / REST handlers         |  | Layer 5b: JS API                       |  
|                                       |  |                                        |  
| AbstractFileServerServlet,            |  | WRM.require, WRM.requireLazily,        |  
| PhaseAwareResources, ListOfResources, |  | WRM.contextPath, WRM.I18n,             |  
| RequestablesResources                 |  | WRM.setLoggingLevel                    |  
|                                       |  |                                        |  
|---------------------------------------|  |----------------------------------------|  

|---------------------------------------|  |----------------------------------------|
| Layer 4:  Composing client payloads   |  | Layer 4: Handling additional client    |
|                                       |  |          requests for resources        |
|                                       |  |                                        |
| PageBuilderService,                   |  | Router, Controller,                    |
| WebResourceAssembler,                 |  | AsyncWebResourceLoader                 |
| WebResourceSet                        |  |                                        |
|                                       |  |                                        |
|-----------------------------------------------------------------------------------|
| Layer 4: The state of the request, information about what a client needs          |
|          and already has loaded.                                                  |
|                                                                                   |
| RequestState, RawRequest                                                          |
|                                                                                   |
|-----------------------------------------------------------------------------------|

|-----------------------------------------------------------------------------------|
| Layer 3: Resource marhsalling subsystems                                          |
|                                                                                   |
| `com.atlassian.webresource.api.assembler.resource`                                |
|    - PluginUrlResource                                                            |
| `com.atlassian.plugin.webresource.impl.helpers.url`                               |
|    - UrlGenerationHelpers                                                         |
|    - CalculatedBatches                                                            |
| `com.atlassian.plugin.webresource.assembler`                                      |
|    - HtmlTagWriter                                                                |
|                                                                                   |
|-----------------------------------------------------------------------------------|
| Layer 3: Resource transformation subsystems                                       |
|                                                                                   |
| DownloadableResource, Content,                                                    |
| UrlReadingWebResourceTransformer,                                                 |
| UrlReadingContentTransformer                                                      |
|                                                                                   |
|-----------------------------------------------------------------------------------|
| Layer 3: Resource address and resolution subsystems.                              |
|                                                                                   |
| UrlReadingCondition,                                                              |
| Dimension, Coord                                                                  |
|                                                                                   |
| New (v5):                                                                         |
| `com.atlassian.plugin.webresource.models`                                         |
|   - Requestable                                                                   |
| `com.atlassian.plugin.webresource.graph`                                          |
|   - DependencyGraph                                                               |
|                                                                                   |
| Old (v4 and lower):                                                               |
| `com.atlassian.plugin.webresource.impl.snapshot`                                  |
|    - Bundle                                                                       |
| `com.atlassian.plugin.webresource.impl.discovery`                                 |
|   - BundleFinder                                                                  |
|   - BundleWalker                                                                  |
|                                                                                   |
|-----------------------------------------------------------------------------------|

|-----------------------------------------------------------------------------------|
| Layer 2: The state of the system, information about the web resources and config  |
|                                                                                   |
| Config, Snapshot, Globals.                                                        |
|-----------------------------------------------------------------------------------|

|---------------------------------------|   |---------------------------------------|
| Layer 1a: SPI                         |   | Layer 1b: Platform and Plugin System  |
|                                       |   |                                       |
| WebResourceIntegration,               |   | ModuleDescriptor, Resource,           |
| ResourceBatchingConfiguration         |   | Plugin                                |
|---------------------------------------|   |---------------------------------------|
```

## Code rules

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

All commits will be checked for conformance to the code rules.

### Frontend code

All frontend code in this repository conforms to
the [Server Frontend Platform team's code rules (pending)](https://hello.atlassian.net/wiki/spaces/TEC/pages/540067361/Code+formatting)
.

- [SPFE-856](https://ecosystem.atlassian.net/browse/SPFE-856) must be completed to enable ESLint support.
- [SPFE-857](https://ecosystem.atlassian.net/browse/SPFE-857) must be completed to enable Prettier support.

### Backend code

All backend code must conform
to [The Server Backend Platform team's code rules (pending)](https://hello.atlassian.net/wiki/spaces/AB/pages/331784335/Forming+the+team)
.

## Test configuration

### base

We group all common Jest configuration in `jest.config.base.js`. If you need to modify the configuration of ALL packages
(including root config), please apply your changes in this file.

### root config

We maintain a root Jest configuration that make use of jest projects to run all package tests in a single run and
generate a single `coverage` report. Try to avoid adding something here.

### packages config

Each package extends `base` configuration, and are free to add anything extra if they need to (ex: envs). Be wise on
editing these configs, and limit it to only modify per package use case. Otherwise, modify the `base` if the
configuration can be shared across packages.

## Testing the project

### Running all unit tests

Run `mvn verify`. This will run all Java, JavaScript, and TypeScript unit tests.

The maven build uses the `frontend-maven-plugin` to download Node and Yarn for running the tests.

### JavaScript unit testing

The `atlassian-plugins-webresource-plugin` submodule uses [jest](https://jestjs.io/) for javascript unit testing.

To run tests in watch mode while developing:

1. Run `yarn install`
2. Run `yarn test:watch`

### Running integration tests locally

1. Clone this repository from `master`
2. Install the dependencies

   ```sh
   mvn install
   ```

3. Run the integration tests against the refapp via CLI:

   ```sh
   mvn verify -DskipUTs -Dproduct=refapp
   ```

## Debugging refapp

1. Clone this repository from `master`
2. Install the dependencies

    ```sh
    mvn install
    ```

3. Run the refapp via CLI:

    ```sh
    yarn start:refapp
    ```

4. Wait for `refapp` to start until you see the message:

    ```
    refapp started successfully in 32s at http://localhost:5990/refapp
    ```

5. Open your IDE.
6. Choose an integration test in the `atlassian-plugins-webresource-tests` project and run it.

   This will run Selenium against the running refapp instance.

### Connecting JVM debugger

You can run the **refapp** in debug mode by using `yarn debug:refapp` command.

Once the refapp is started, you can connect
the [Remote JVM Debugger](https://www.jetbrains.com/help/idea/tutorial-remote-debug.html#95db161b) from IntelliJ IDEA.

### Logs

While the **refapp** is running you can watch the product logs by using the `tail` command:

```shell
tail -f ./atlassian-plugins-webresource-tests/target/refapp.log
```

The logs are generated when refapp is running in both debug and non-debug modes.

## Testing local version in a product

Depending on the project, testing in a product has a different workflow.

All projects can be tested in a product by first running `mvn install` then `yarn start:<productname>`.

e.g., `mvn install && yarn start:jira` will re-build all projects, install them in a local Jira instance, and start the
instance.

You can skip the `mvn install` step if you haven't made changes to any projects. It's safer to run both, especially when
switching branches.

### `atlassian-plugins-webresource` and `atlassian-plugins-webresource-api`

These are library projects, not atlassian-plugins. They must be installed before a product starts.

### `atlassian-plugins-webresource-plugin` and `atlassian-plugins-webresource-rest`

These are atlassian-plugin projects, so can be re-installed via QuickReload.

1. Start a product via `yarn start:<productname>`
2. Make your change to the relevant webresource project
3. Run `mvn package -DskipTests`

The change should propagate to the product every time you perform step (3).

### `atlassian-plugins-webresource-spi`

Any changes to the SPI require implementation in a product.

1. Create a new branch in the webresource repo.
2. Make the change to the SPI.
3. Run `mvn install`.
4. Clone the product repository.
5. Create a new branch in the product repository.
6. Update the `webresource.version` property in the product's `pom.xml` to this project's `SNAPSHOT` version.
7. Implement the necessary changes in the concrete SPI implementations.

You can now build and run the product to test against. Follow the product repository's guidelines for building the
product. To run this repository's tests against the product, you will need to set the

## Documentation

The documentation is kept in [the Atlassian SDK documentation repository][docs] and published to [DAC][dac].

Documentation is written using [DAC writing toolkit](https://developer.atlassian.com/platform/writing-toolkit).

Make sure to got through
the [Getting Started](https://developer.atlassian.com/platform/writing-toolkit/getting-started/) guide to gain access to
the right package and docker images to work with the writing toolkit.

## Builds

Each push to a branch triggers a Bamboo build to check if your code builds successfully.

The builds are [hosted on EcoBAC under the `PLUGWEBR` project key][builds].

Our builds use [the `sfp-base-agent` image from the SFP monorepo](https://bitbucket.org/atlassian/sfp).

## Releasing

Refer to [releasing](./RELEASING.md) for release and bug fixing process.

### Releasing changes for products to test

Any changes to the API or SPI projects require implementation in a product.

1. Create a new branch in the webresource repo.
2. Make the change to the SPI.
3. [Release a hash version of the webresource project](./RELEASING.md).
4. Clone the product repository.
5. Create a new branch in the product repository.
6. Update the `webresource.version` property in the product's `pom.xml` to the hash version released in step 3.
7. Implement the necessary changes in the concrete SPI implementations.
8. Push both branches to the relevant repositories.
9. Notify a frontend champion for the product about the branch you have created.

Depending on need, you may request the frontend champion release a test version of the product with the updated SPI.
This would allow you to update the product's version in this repository and run its acceptance tests.

### Keep documentation up to date

If you're adding a new API, or changing an existing one, make sure to add those changes to
[the documentation][docs]. After releasing a new version of the packages, also [release a new version of the
documentation](./RELEASING.md) if needed.


[builds]: https://ecosystem-bamboo.internal.atlassian.com/browse/PLUGWEBR
[docs]: https://bitbucket.org/atlassian-developers/atlassian-sdk-docs
[dac]: https://developer.atlassian.com/server/framework/atlassian-sdk
[yarn]: https://classic.yarnpkg.com/
