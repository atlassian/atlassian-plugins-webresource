import $ from 'jquery';
import { setLoggingLevel } from '@wrm/logger';
global.window.fetch = jest.fn();
global.jQuery = global.window.jQuery = $;

window.WRM = {
    contextPath: function () {
        return "";
    },
    data: {
        claim: jest.fn(),
    },
    _dataArrived: function () {},
    curl: function () {},
};

// Use different level for more verbose logging @see wrm/logger
setLoggingLevel('error');

jest.mock("@wrm/globals", () => ({
    contextPath: jest.fn(),
}));
