#!/usr/bin/env node
const fs = require('fs');
const sortJson = require('sort-json');
const argv = require('yargs-parser')(process.argv.slice(2));

const sortOptions = { ignoreCase: true, indentSize: 2 };
const isDryRun = 'dryRun' in argv;
const files = argv._;

function checkDifference(original) {
    const sorted = sortJson(original, sortOptions);
    const sortedStr = JSON.stringify(sorted);
    const originalStr = JSON.stringify(original);
    return (sortedStr !== originalStr);
}

if (isDryRun) {
    console.log('Linting JSON contents of files...', files);

    const results = files
        .map(file => fs.readFileSync(file, 'utf8'))
        .map(JSON.parse)
        .map(checkDifference);
    const numFilesWithDifferences = results.filter(Boolean).length;
    console.log(`Number of unsorted files: ${numFilesWithDifferences}`);
    process.exit(numFilesWithDifferences);
}

console.log('Sorting JSON contents of files...', files);
sortJson.overwrite(files, sortOptions);
