const { pathsToModuleNameMapper } = require("ts-jest/utils");
const { compilerOptions: { paths: tsconfigPaths } } = require("./tsconfig");

const base = require("../jest.config.base");
const pack = require("./package.json");

module.exports = {
    ...base,
    displayName: pack.name,
    testEnvironment: "jsdom",
    preset: "ts-jest/presets/js-with-ts",
    reporters: ["default", "jest-junit"],
    testPathIgnorePatterns: ["/node_modules/", "/target/"],
    clearMocks: true,
    setupFilesAfterEnv: ["jest-extended/all"],
    testMatch: ["<rootDir>/src/main/clientside/**/*.test.(js|ts)"],
    moduleFileExtensions: ["ts", "js"],
    setupFiles: ["<rootDir>/setup-jest.js"],
    modulePaths: ["<rootDir>"],
    moduleNameMapper: pathsToModuleNameMapper(tsconfigPaths),
};
