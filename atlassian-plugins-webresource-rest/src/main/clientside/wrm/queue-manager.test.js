import {makeQueueManager} from "./queue-manager";

const mockDocumentReadyState = (state) => {
    Object.defineProperty(document, "readyState", {
        get() {
            return state;
        },
        configurable: true,
    });
};

const mockAddEventListener = () => {
    return jest.spyOn(window.document, 'addEventListener');
};

const createProcess = (shouldSucceed = true) =>
    jest
        .fn()
        .mockImplementation((queuedElements) => {
            const response = { result: undefined, queuedElements };
            return shouldSucceed ? Promise.resolve(response) : Promise.reject(response);
        });

const realSetTimeout = setTimeout;
const asyncThings = () => new Promise(resolve => {
    jest.runAllTimers();
    jest.runAllTicks();
    realSetTimeout(resolve);
});
const drainQueue = () => asyncThings();

beforeAll(() => {
    jest.useFakeTimers();
    jest.setTimeout(1000);
});

describe("Queue Manager", () => {
    test("when process fails", async () => {
        const process = createProcess(false);
        const queue = makeQueueManager(process);
        expect(process).not.toHaveBeenCalled();
        await expect(queue.enqueue(["a.js"], "require")).toReject();
        expect(process).toHaveBeenCalledTimes(1);
    });

    describe("document.readyState", () => {
        test("when loading, registers event handlers for both queues", () => {
            mockDocumentReadyState("loading");
            const add = mockAddEventListener();

            makeQueueManager(createProcess());
            expect(add).toHaveBeenCalledTimes(2);

            expect(add).toHaveBeenCalledWith("readystatechange", expect.any(Function), {
                once: true,
            });
            expect(add).toHaveBeenCalledWith("DOMContentLoaded", expect.any(Function), {
                once: true,
            });
        });

        test("when interactive, registers event handler for interaction queue", () => {
            mockDocumentReadyState("interactive");
            const add = mockAddEventListener();

            makeQueueManager(createProcess());
            expect(add).toHaveBeenCalledTimes(1);
            expect(add).toHaveBeenCalledWith("DOMContentLoaded", expect.any(Function), {
                once: true,
            });
        });

        test("when complete, no event handler registered", () => {
            mockDocumentReadyState("complete");
            const add = mockAddEventListener();

            makeQueueManager(createProcess());
            makeQueueManager(createProcess());
            expect(add).not.toHaveBeenCalled();
        });
    });

    describe("in 'loading' page ready state", () => {
        let process;
        let queue;
        let addEventListener;
        let registeredDclCallback;
        let registeredReadyStateChangeCallback;

        beforeEach(async () => {
            await asyncThings();

            mockDocumentReadyState("loading");
            addEventListener = mockAddEventListener();

            process = createProcess();
            queue = makeQueueManager(process);

            expect(process).not.toHaveBeenCalled();
            expect(addEventListener).toHaveBeenCalledTimes(2);
            expect(addEventListener).toHaveBeenCalledWith("DOMContentLoaded", expect.any(Function), {
                once: true,
            });
            expect(addEventListener).toHaveBeenCalledWith("readystatechange", expect.any(Function), {
                once: true,
            });
            registeredDclCallback = addEventListener.mock.calls[0][1];
            registeredReadyStateChangeCallback = addEventListener.mock.calls[1][1];
        });

        afterEach(() => {
            expect(addEventListener).toHaveBeenCalledTimes(2);
        });

        test("enqueue interaction, then require", async () => {
            const loadingA = queue.enqueue(["a.js"], "interaction");
            expect(process).not.toHaveBeenCalled();

            await drainQueue();
            expect(process).not.toHaveBeenCalled();

            const loadingB = queue.enqueue(["b.js"], "require");
            expect(process).not.toHaveBeenCalled();

            mockDocumentReadyState('interactive')
            registeredReadyStateChangeCallback()
            await drainQueue();
            await expect(loadingB).toResolve();
            expect(process).toHaveBeenCalledTimes(1);

            registeredDclCallback();
            await expect(loadingA).toResolve();
            expect(process).toHaveBeenCalledTimes(2);
        });

        test("enqueue require only", async () => {
            const loadingA = queue.enqueue(["a.js"], "require");
            expect(process).not.toHaveBeenCalled();

            // Processing doesn't happen until after ready state change to 'interactive'
            await drainQueue();
            expect(process).not.toHaveBeenCalled();

            mockDocumentReadyState('interactive')
            registeredReadyStateChangeCallback();
            await expect(loadingA).toResolve();
        });

        test("enqueue interaction only", async () => {
            const loadingA = queue.enqueue(["a.js"], "interaction");
            expect(process).not.toHaveBeenCalled();

            // Processing doesn't happen until after DCL
            await drainQueue();
            expect(process).not.toHaveBeenCalled();

            registeredDclCallback();
            await expect(loadingA).toResolve();
        });
    });

    describe("in 'interactive' page ready state, but before DCL", () => {
        let process;
        let queue;
        let addEventListener;
        let registeredDclCallback;

        beforeEach(async () => {
            await asyncThings();

            mockDocumentReadyState("interactive");
            addEventListener = mockAddEventListener();

            process = createProcess();
            queue = makeQueueManager(process);

            expect(process).not.toHaveBeenCalled();
            expect(addEventListener).toHaveBeenCalledTimes(1);
            expect(addEventListener).toHaveBeenCalledWith("DOMContentLoaded", expect.any(Function), {
                once: true,
            });
            registeredDclCallback = addEventListener.mock.calls[0][1];
        });

        afterEach(() => {
            expect(addEventListener).toHaveBeenCalledTimes(1);
        });

        test("enqueue interaction, then require", async () => {
            const loadingA = queue.enqueue(["a.js"], "interaction");
            expect(process).not.toHaveBeenCalled();

            await drainQueue();
            expect(process).not.toHaveBeenCalled();

            const loadingB = queue.enqueue(["b.js"], "require");
            expect(process).not.toHaveBeenCalled();

            await drainQueue();
            await expect(loadingB).toResolve();
            expect(process).toHaveBeenCalledTimes(1);

            registeredDclCallback();
            await expect(loadingA).toResolve();
            expect(process).toHaveBeenCalledTimes(2);
        });

        test("enqueue require only", async () => {
            const loadingA = queue.enqueue(["a.js"], "require");
            expect(process).not.toHaveBeenCalled();
            const loadingB = queue.enqueue(["b.js"], "require");
            expect(process).not.toHaveBeenCalled();

            await drainQueue();
            await expect(loadingA).toResolve();
            await expect(loadingB).toResolve();

            expect(process).toHaveBeenCalledTimes(1);
        });

        test("enqueue interaction only", async () => {
            const loadingA = queue.enqueue(["a.js"], "interaction");
            expect(process).not.toHaveBeenCalled();

            // Processing doesn't happen until after DCL
            await drainQueue();
            expect(process).not.toHaveBeenCalled();

            registeredDclCallback();
            await expect(loadingA).toResolve();
        });
    });

    describe("after DCL", () => {
        let process;
        let queue;
        let addEventListener;

        beforeEach(() => {
            mockDocumentReadyState("complete");
            addEventListener = mockAddEventListener();

            process = createProcess();
            queue = makeQueueManager(process);

            expect(process).not.toHaveBeenCalled();
            expect(addEventListener).toHaveBeenCalledTimes(0);
        });

        afterEach(() => {
            expect(addEventListener).toHaveBeenCalledTimes(0);
        });

        test("enqueue interaction, then require", async () => {
            const loadingA = queue.enqueue(["a.js"], "interaction");
            await loadingA;
            expect(process).toHaveBeenCalledTimes(1);

            const loadingB = queue.enqueue(["b.js"], "require");
            await loadingB;
            expect(process).toHaveBeenCalledTimes(2);
        });

        test("enqueue require only", async () => {
            const loadingB = queue.enqueue(["b.js"], "require");
            await loadingB;
            expect(process).toHaveBeenCalledTimes(1);
        });

        test("enqueue interaction only", async () => {
            const loadingA = queue.enqueue(["a.js"], "interaction");
            await loadingA;
            expect(process).toHaveBeenCalledTimes(1);
        });
    });
});
