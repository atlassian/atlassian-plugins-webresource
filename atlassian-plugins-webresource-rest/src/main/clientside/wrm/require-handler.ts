import {contextPath} from "@wrm/globals";
import {BatchType, RequestedResourcesTracker} from "@wrm/requested-resources-tracker";
import {makeQueueManager, QueueManager} from "@wrm/queue-manager";
import {resourceTypes} from "@wrm/constants";
import {createLogger} from "@wrm/logger";
import {Phase, ProcessedResult, QueueElement} from "@wrm/types";
import {loadResources} from "./resource-loader";

declare const WRM: typeof window.WRM & {
    _unparsedData: object | undefined;
    _unparsedErrors: object | undefined;
    _dataArrived: () => void;
    curl?: Function;
};

/**
 * @typedef {Object} Resource
 * @property {string} url - the location of the resource on the server.
 * @property {string} key - the complete module key for the resource's parent web-resource.
 * @property {('JAVASCRIPT'|'CSS')} resourceType - the kind of asset at the given url.
 * @property {('context'|'resource')} batchType - whether this url represents a context batch or a standalone resource.
 * @property {string} [media] - (for CSS resources) the type of media the resource should render to (e.g., print).
 */

/**
 * @enum
 */
const failcases = {
    DISCOVERY: 1,
    LOADER: 2,
    NETWORK: 4,
    EXCEPTION: 8,
};
const logger = createLogger({ context: "handler" });

const noop = () => {};

type Resource = {
    url: string;
    media: string;
    resourceType: string;
    key: string;
    batchType: string;
};

// To be sure that it will not override the real curl.
if (!("curl" in WRM)) {
    WRM.curl = () => {
        logger.error(
            "WRM.curl is deprecated. Use WRM.require instead. See https://bitbucket.org/atlassian/atlassian-plugins-webresource/src/master/UPGRADE_500.md"
        );
    };
}

/**
 * Loads CSS and sets media queries as appropriate.
 * @param resource
 * @private
 */
function loadCss(resource: Resource) {
    logger.warn("asynchronously loading a CSS resource containing a media query", resource.url);
    var tag = document.createElement("link");
    tag.setAttribute("rel", "stylesheet");
    tag.setAttribute("type", "text/css");
    tag.setAttribute("href", resource.url);
    tag.setAttribute("media", resource.media);
    document.head.appendChild(tag);
}

/**
 * Checks if a script element whose src is the given url exists on the page
 * @param url url
 * @return {boolean} True if the script is on the page, otherwise false
 * @private
 */
function isJsAlreadyLoaded(url: string) {
    // TODO PLUGWEB-685 we should be monitoring this
    return Boolean(document.querySelector(`script[src='${url}']`));
}

/**
 * Checks if a link element whose href is the given url exists on the page
 * @param url url
 * @return {boolean} True if the link is on the page, otherwise false
 * @private
 */
function isCSSAlreadyLoaded(url: string) {
    // TODO PLUGWEB-685 we should be monitoring this
    return Boolean(document.querySelector(`link[href='${url}']`));
}

function getResourceKeys(queuedElements: QueueElement[], phase: Phase) {
    return queuedElements.filter((el) => el.phase === phase).flatMap((el) => el.resourceKeys);
}

type DeferredState = "pending" | "resolved" | "rejected";

type Deferred<T> = Promise<T> & {
    done: (cb: Function) => {};
    fail: (cb: Function) => {};
    always: (cb: () => void) => {};
    promise: () => Promise<T>;
    progress: () => Promise<T>;
    state: () => DeferredState;
};

type RequireCache = {
    [key: string]: Promise<unknown>;
};

type Options = {
    phase: Phase;
};

const deferrify = <T>(promise: Promise<T>) => {
    // legacy for jQuery.Deferred API shape
    const deferred = promise as Deferred<T>;
    let state: DeferredState = "pending";
    deferred.state = (): DeferredState => state;
    deferred.done = (cb) => {
        promise.then(() => {
            state = "resolved";
            return cb();
        }, noop);
        return deferred;
    };
    deferred.fail = (cb) => {
        promise.catch((vals: any) => {
            state = "rejected";
            return cb.apply(undefined, vals);
        });
        return deferred;
    };
    deferred.always = (cb) => {
        promise.finally(cb);
        return deferred;
    };
    deferred.promise = () => deferred;
    deferred.progress = () => deferred;

    return deferred;
};

const buildCacheKey = (resources: string[]) => {
    return resources.join(",");
};

const withLocale = (url: string) => {
    const locale = window.WRM.__localeOverride;
    if (locale === undefined) {
        return url;
    }
    if (typeof locale !== "string" || locale.trim() === "") {
        logger.warn(
            "Locale override failed. The window.WRM.__localeOverride should either be undefined or a non-empty, non-blank string."
        );
        return url;
    }
    try {
        const urlObject = new URL(url);
        urlObject.searchParams.set("locale", locale);
        logger.warn("Resource locale is overriden by window.WRM.__localeOverride: ", locale);
        return urlObject.toString();
    } catch (e) {
        logger.warn("Locale override failed. Invalid resource URL. Falling back to the default locale.");
        return url;
    }
};

export class RequireHandler {
    private _requireCache: RequireCache;
    private _queueManager: QueueManager;
    private _tracker: RequestedResourcesTracker;

    constructor() {
        this._requireCache = {
            // handle empty resource requests gracefully
            "": Promise.resolve(),
        };

        // Contains a promise for the currently executing to the WRM REST API, otherwise false
        this._queueManager = makeQueueManager(this._getScriptsForResources.bind(this));
        // Helps keep track of already-loaded resources
        this._tracker = new RequestedResourcesTracker(document);
    }

    /**
     * Requires resources on the page.
     * @param {LoaderKey[]} requiredResources list of resources (eg webresources or webresource contexts)
     * @param {Function} [cb] [Deprecated] callback that is executed when the returned Promise resolves.
     * @param {{ phase: 'interaction' }} options
     * @return {Promise} a Promise that is resolved when all JS / CSS resources have been included on the page, or
     * rejects if any errors occur during the loading process.
     */
    private _genericRequire(requiredResources: string | string[], cb: Function, options: Options): Deferred<unknown> {
        // This takes care of requiredResources being `undefined`, `null`, empty strings or something else
        // which is not an Array of strings
        const resources = ([] as string[])
            .concat(requiredResources)
            .filter((res) => typeof res === "string" && res.trim().length > 0);

        // Requests are cached in this._requireCache so that if a client makes multiple requests for the same
        // set of resources, we can piggyback off the promise for the first request.
        // In other words, if a client calls require([a, b]), then does some work, then calls require([a, b])
        // again, the second call should resolve immediately (or after the first call has resolved).
        const cacheKey = buildCacheKey(resources);

        // Just in case some resource being called `hasOwnProperty`
        if (!this._requireCache.hasOwnProperty(cacheKey)) {
            this._requireCache[cacheKey] = new Promise<unknown>((resolve, reject) => {
                this._resolveAsync(resources, options.phase).then(resolve, reject);
            });
        }

        const promise = this._requireCache[cacheKey];

        const deferred = deferrify(promise);

        // legacy callback-based style instead of promise-based.
        if (typeof cb === "function") {
            deferred.done(cb);
        }

        return deferred;
    }

    /**
     * Requires resources on the page.
     * @param {LoaderKey[]} requiredResources list of resources (eg webresources or webresource contexts)
     * @param {Function} [callback] [Deprecated] callback that is executed when the returned Promise resolves.
     * @return {Promise} a Promise that is resolved when all JS / CSS resources have been included on the page, or
     * rejects if any errors occur during the loading process.
     */
    require(requiredResources: string | string[], callback: Function) {
        return this._genericRequire(requiredResources, callback, {
            phase: "require",
        });
    }

    /**
     * Requires resources lazily on the page.
     * @param {LoaderKey[]} requiredResources list of resources (eg webresources or webresource contexts)
     * @param {Function} [callback] [Deprecated] callback that is executed when the returned Promise resolves.
     * @return {Promise} a Promise that is resolved when all JS / CSS resources have been included on the page, or
     * rejects if any errors occur during the loading process.
     */
    requireLazily(requiredResources: string | string[], callback: Function) {
        return this._genericRequire(requiredResources, callback, {
            phase: "interaction",
        });
    }

    /**
     * Given a list of resources, translates those resources to actual CSS / JS files and includes them on the page
     * @param {LoaderKey[]} resourceKeys - the list of requests made to `WRM.require`
     * @param {'interaction'} phase
     * @return a Promise that is resolved only after all resources have been included on the page
     * @private
     */
    private _resolveAsync(resourceKeys: string[], phase: Phase) {
        const onRequestFail = ([failcase, args]: [number, any[]]) => {
            if (failcase !== failcases.LOADER) {
                const cacheKey = buildCacheKey(resourceKeys);
                delete this._requireCache[cacheKey];
            }
            return Promise.reject(args);
        };

        return this._queueManager.enqueue(resourceKeys, phase).catch(onRequestFail);
    }

    /**
     * Processes a response from the WRM REST endpoint.
     * This needs to have a 1:1 call relationship between _getScriptsForResources' use of fetch
     * @param {Object} resourceResponse
     * @param {Resource[]} resourceResponse.resources
     * @param {Object} [resourceResponse.unparsedData]
     * @param {Object} [resourceResponse.unparsedErrors]
     * @private
     */
    private _processResourceResponse({
        unparsedData,
        unparsedErrors,
        resources,
    }: {
        unparsedData?: object;
        unparsedErrors?: object;
        resources: Resource[];
    }) {
        // TODO: Missing tests cases when resources variable is empty and unparsedData or unparsedError contain sth.
        if (unparsedData) {
            WRM._unparsedData || (WRM._unparsedData = {});
            Object.assign(WRM._unparsedData, unparsedData);
            WRM._dataArrived();
        }
        if (unparsedErrors) {
            WRM._unparsedErrors || (WRM._unparsedErrors = {});
            Object.assign(WRM._unparsedErrors, unparsedErrors);
            WRM._dataArrived();
        }

        if (!resources.length) {
            logger.debug("There is nothing to be requested by resource-loader");
            return Promise.resolve();
        }

        const resourcesToLoad: string[] = [];
        const cssMediaResourcesToLoad: Resource[] = [];

        for (let resource of resources) {
            const url = withLocale(resource.url);

            this._tracker.addResource(resource.key, resource.batchType as BatchType);

            if (resource.resourceType === resourceTypes.JS) {
                if (!isJsAlreadyLoaded(url)) {
                    resourcesToLoad.push(`js!${url}`);
                }
            } else if (resource.resourceType === resourceTypes.CSS) {
                if (!isCSSAlreadyLoaded(url)) {
                    if (resource.media && "all" !== resource.media) {
                        // HACK: this can't be loaded by curl.js. The solution is to the DOM immediately
                        // using a <link> tag. This means that the callback may be called before the CSS
                        // has been loaded, resulting in a flash of unstyled content.
                        cssMediaResourcesToLoad.push(resource);
                    } else {
                        resourcesToLoad.push(`css!${url}`);
                    }
                }
            } else {
                logger.log("Unknown resource type required", url);
            }
        }
        logger.log("Downloading resources", resourcesToLoad);

        return new Promise(function (resolve, reject) {
            logger.debug("resource-loader requesting resources: ", resourcesToLoad);
            loadResources(
                resourcesToLoad,
                (...args) => {
                    // Add all css media resources. This is done after curl resources to ensure ordering is consistent
                    // with the way resources are delivered on the server.
                    cssMediaResourcesToLoad.forEach(loadCss);
                    logger.debug("resource-loader resolves", args);
                    resolve(args);
                },
                (...args) => {
                    logger.debug("resource-loader fails", args);
                    reject(args);
                }
            );
        });
    }

    /**
     * Makes a fetch request to retrieve the actual JS and CSS files required to satisfy the requested resources.
     * @param {Array<LoaderKey[]>} requests - the list of requests made to `WRM.require`
     * @return a Promise that either resolves when all resources from the requested web-resources and contexts
     * have been loaded, or rejects if any errors occur during the loading process.
     * @private
     */
    private _getScriptsForResources(queuedElements: QueueElement[]): Promise<ProcessedResult> {
        if (queuedElements.length === 0) {
            logger.debug("Nothing to request from WRM API");
            return Promise.resolve({ result: undefined, queuedElements });
        }

        // Determine what to request from the server
        const payload = {
            require: getResourceKeys(queuedElements, "require"),
            interaction: getResourceKeys(queuedElements, "interaction"),
            // Determine what resources have already been loaded previously,
            // so we can avoid loading them multiple times.
            exclude: this._tracker.getResources(),
        };

        logger.debug("Payload for WRM API call prepared: ", payload);
        // Wrap the server request; resolve once any post-processing and side-effects
        // have occurred (e.g., adding scripts and links to the page).
        return new Promise<ProcessedResult>((resolve, reject) => {
            let err: undefined | [number, any[]];

            const failBy = (reason: number) => (vals: any[]) => {
                err = err || [reason, vals];
                return Promise.reject();
            };

            logger.debug("Calling WRM API...");
            fetch(contextPath() + "/rest/wrm/2.0/resources", {
                // the request data may get too long to represent in GET parameters.
                method: "POST",
                // each request to the REST endpoint should be considered unique.
                cache: "no-cache",
                // ensure we send user credentials.
                // especially relevant for older browsers where `omit` was the default.
                credentials: "same-origin",
                // we should only ever send requests to the same server the original HTML came from.
                mode: "same-origin",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(payload),
            })
                .then((resp) => (resp.ok ? resp.json() : Promise.reject(resp)), failBy(failcases.NETWORK))
                .then(({ require, interaction }) => {
                    logger.debug("WRM API responded: ", {
                        require,
                        interaction,
                    });

                    // This is the place where the phases are forced to load in sequence.
                    // We don't start loading lazy scripts until all non-lazy are executed.
                    const requireQueuePromise = () => this._processResourceResponse(require);
                    const interactionQueuePromise = () => this._processResourceResponse(interaction);

                    return requireQueuePromise().then(interactionQueuePromise);
                }, failBy(failcases.DISCOVERY))
                .then((result) => resolve({ result, queuedElements }), failBy(failcases.LOADER))
                .catch((data) => {
                    const error = data ? [failcases.EXCEPTION, data] : err;
                    logger.debug("WRM API call failed", error);

                    reject({ result: error, queuedElements });
                });
        });
    }
}
