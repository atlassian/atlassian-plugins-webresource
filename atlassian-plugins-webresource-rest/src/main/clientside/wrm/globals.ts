declare global {
    interface Window {
        WRM: {
            contextPath: () => string;
            __localeOverride?: string;
        };
    }
}

const contextPath = window.WRM.contextPath;

export { contextPath };
