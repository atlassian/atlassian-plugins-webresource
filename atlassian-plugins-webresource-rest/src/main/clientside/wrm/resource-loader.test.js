import {loadResources} from "@wrm/resource-loader";

/**
 * @param {(...args: unknown[]) => HTMLElement} selectNode
 * @param {'load' | 'error'} eventName
 * @param {string} url
 */
const settleResource = (selectNode) => (eventName) => (url) => {
    const node = selectNode(url);

    const event = document.createEvent(eventName === "load" ? "Event" : eventName === "error" ? "ErrorEvent" : null);
    event.initEvent(eventName, true, true);
    node.dispatchEvent(event);

    return Promise.resolve();
};

const settleScript = settleResource((scriptSrc) => document.querySelector(`script[src="${scriptSrc}"]`));
const resolveScript = settleScript("load");
const rejectScript = settleScript("error");
const makeScriptId = (src) => `js!${src}`;
const makeScriptFixture = (src) => [src, makeScriptId(src), resolveScript, rejectScript];

const settleStyle = settleResource((styleHref) => document.querySelector(`link[href="${styleHref}"]`));
const resolveStyle = settleStyle("load");
const rejectStyle = settleStyle("error");
const makeStyleId = (href) => `css!${href}`;
const makeStyleFixture = (href) => [href, makeStyleId(href), resolveStyle, rejectStyle];

describe("load resource", () => {
    const fixtures = [
        ["single script", [makeScriptFixture("a.js")]],
        ["single style", [makeStyleFixture("a.css")]],
        [
            "mixed sequence of resources",
            [
                makeScriptFixture("a.js"),
                makeScriptFixture("b.js"),
                makeStyleFixture("a.css"),
                makeStyleFixture("b.css"),
            ],
        ],
    ];

    describe("successfully", () => {
        beforeEach(() => {
            document.head.innerHTML = "";
        });

        fixtures.forEach(([label, resources]) => {
            test(label, async () => {
                const ids = resources.map(([url, id, resolve, reject]) => id);
                const resolves = resources.map(([url, id, resolve, reject]) => () => resolve(url));
                const resolveAll = () => Promise.all(resolves.map((f) => f()));

                const success = jest.fn();
                const fail = jest.fn();

                await Promise.all([loadResources(ids, success, fail), resolveAll()]);

                expect(success).toHaveBeenCalled();
                expect(fail).not.toHaveBeenCalled();
            });
        });
    });

    describe("unsuccessfully", () => {
        beforeEach(() => {
            document.head.innerHTML = "";
        });

        fixtures.forEach(([label, resources]) => {
            test(label, async () => {
                const ids = resources.map(([url, id, resolve, reject]) => id);
                const rejects = resources.map(([url, id, resolve, reject]) => () => reject(url));
                const rejectAll = () => Promise.all(rejects.map((f) => f()));

                const success = jest.fn();
                const fail = jest.fn();

                await Promise.all([loadResources(ids, success, fail), rejectAll()]);

                expect(success).not.toHaveBeenCalled();
                expect(fail).toHaveBeenCalled();
            });
        });
    });
});

describe("invalid usage", () => {
    test("missing prefix", async () => {
        const errorHandler = jest.fn();
        await loadResources(["unprefixed-script.js"]).catch(errorHandler);
        expect(errorHandler).toHaveBeenCalledWith(new Error("Unsupported resource identifier"));
    });
});

describe("Stylesheet verification", () => {

    test("when is stylesheet", async () => {
        window.PerformanceObserver = {}
        //when
        const actual = isStylesheet("link", "https://localhost:8080/s/830ead90ef32e1df8bbf822daf7df864-CDN/-mn7l96/900000/9i038o/6866384d18bce9b1833cb67d2585cba4/_/download/contextbatch/css/batch.css")
        //then
        expect(actual).toBe(true)
    });

    test("when is not stylesheet", async () => {
        window.PerformanceObserver = {}
        //when
        const actual = isStylesheet("link", "https://localhost:8080/s/830ead90ef32e1df8bbf822daf7df864-CDN/-mn7l96/900000/9i038o/6866384d18bce9b1833cb67d2585cba4/_/download/contextbatch/js/batch.js")
        //then
        expect(actual).toBe(false)
    });
});

test("PLUGWEB-670 PLUGWEB-672: The minimum phase for clientside includes is DEFER as to prevent racing serverside includes",  async () => {
    document.head.innerHTML = "";
    const irrelevantCallback = () => {}

    // given
    const script = "some-client-side-included-resource.js";

    // when
    loadResources([makeScriptId(script)], irrelevantCallback, irrelevantCallback);

    // then
    const addedScriptTag = document.head.querySelector('script');
    expect(addedScriptTag.defer).toBe(true);

    expect(addedScriptTag.src).toEndWith(script) // make sure we're testing the right thing
});

const isStylesheet = (type, name) => {
    return type === 'link' && new URL(name).pathname.split('.').pop() === 'css';
}
