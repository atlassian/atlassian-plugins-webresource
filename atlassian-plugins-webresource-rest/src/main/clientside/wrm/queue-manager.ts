import {createLogger} from "@wrm/logger";
import {Phase, Process, ProcessedResult, QueueElement} from "@wrm/types";

const logger = createLogger({ context: "queue" });

type QueuesByPhase = {
    [key in Phase]: Queue;
};

type Enqueue = (resourceKeys: string[], phase: Phase) => Promise<unknown>;

export type QueueManager = {
    enqueue: Enqueue;
};

type Queue = {
    enqueue: (resourceKeys: string[], onEnqueued: () => void) => Promise<unknown>;
    isNotEmpty: () => boolean;
    drain: () => QueueElement[];
};

const makeQueue = (phase: Phase): Queue => {
    const items: QueueElement[] = [];
    const enqueue: Queue["enqueue"] = (resourceKeys, onEnqueued) =>
        new Promise<unknown>((resolve, reject) => {
            logger.debug("Resources entering the queue... ", { resourceKeys, phase });

            items.push({
                phase,
                resourceKeys,
                resolve,
                reject,
            });

            onEnqueued();
        });
    const isNotEmpty = () => items.length > 0;
    const drain = () => {
        const queuedElements = [...items];
        items.length = 0;
        return queuedElements;
    };

    return {
        enqueue,
        isNotEmpty,
        drain,
    };
};

const settleItemPromises =
    (variant: "resolve" | "reject") =>
    ({ result, queuedElements }: ProcessedResult): Promise<unknown> =>
        Promise.all(queuedElements.map((queuedElement) => queuedElement[variant](result)));

export const makeQueueManager = (process: Process): QueueManager => {
    const queuesByPhase: QueuesByPhase = {
        require: makeQueue("require"),
        interaction: makeQueue("interaction"),
    };

    const drainableQueues: Queue[] = [];

    const drainAllQueues = () => Promise.all(drainableQueues.flatMap((queue) => queue.drain()));
    const areQueuesNonEmpty = () => drainableQueues.some((queue) => queue.isNotEmpty());
    const semaphore = ((draining?: Promise<any>) => () => {
        if (areQueuesNonEmpty()) {
            logger.debug("There are resources waiting in queue: ", queuesByPhase);
        } else {
            logger.debug("Nothing waiting in queue. Draining won't be scheduled this time.");
            return;
        }

        // prevent draining until any in-process drain is done
        if (draining) {
            logger.debug("Draining already scheduled");
            return;
        }

        logger.debug("Scheduling next draining");
        draining = Promise.resolve()
            .then(drainAllQueues)
            .then(process)
            .then(settleItemPromises("resolve"), settleItemPromises("reject"))
            .finally(() => {
                logger.debug("Resources from drained queues processed");
                draining = undefined;
                semaphore();
            });
    })();

    switch (document.readyState) {
        default:
            // Prefer not loading over double loading -- easier to troubleshoot
            logger.error("There's a missing readyState, fix it ASAP! No guarantees the page will ever work nor load")
        case "loading":
            document.addEventListener("DOMContentLoaded", () => {
                drainableQueues.push(queuesByPhase.interaction);
                semaphore();
            }, { once: true });
            document.addEventListener("readystatechange", () => {
                if (document.readyState === 'interactive') {
                    drainableQueues.push(queuesByPhase.require);
                    semaphore();
                }
            }, { once: true })
            break;
        case "interactive":
            document.addEventListener("DOMContentLoaded", () => {
                drainableQueues.push(queuesByPhase.interaction);
                semaphore();
            }, { once: true });
            drainableQueues.push(queuesByPhase.require);
            semaphore();
            break;
        case "complete":
            drainableQueues.push(queuesByPhase.require);
            drainableQueues.push(queuesByPhase.interaction);
            semaphore();
            break;

    }
    // Normally should use the logger, this runs before the logging level can be set to debug or below
    // AND it's for debug level (i.e. normally not seen)
    console.debug(`[WRM] Client-side initialised during '${document.readyState}' state`);

    return {
        enqueue: (resourceKeys, phase) => queuesByPhase[phase].enqueue(resourceKeys, semaphore),
    };
};
