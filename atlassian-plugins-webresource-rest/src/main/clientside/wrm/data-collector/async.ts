// its called data-collector to try not to get killed by certain chrome extensions - lets see how that goes
declare const AJS: { trigger: (name: 'analytics', data: { name: string, data: Record<string, number | string | boolean> }) => void };
type transferSize = number;
type encodedBodySize = number;
type name = string;
type type = 'script' | 'css';
declare const __observedResources: Array<[transferSize, encodedBodySize, name, type]>;
const EVENT_NAME = 'wrm.caching.data.collector';

const isSSL = () => new URL(location.href).protocol === 'https:';
const assetsLoadFromForeignOrigin = () => {
    const originSet = new Set<string>()
    document.querySelectorAll<HTMLScriptElement>('script[data-wrm-key]')
        .forEach(scriptElem => {
            if (scriptElem.src) {
                originSet.add(new URL(scriptElem.src).origin)
            }
        })

    // something is really wrong here
    if (originSet.size === 0) {
        return false;
    }

    // we assume if there is more than one origin at least one of them is a CDN
    if (originSet.size === 2) {
        return true;
    }

    // if there is just one - compare it to the current pages origin - in case of mismatch its a CDN
    const loadedFromSameOrigin = originSet.has(new URL(location.href).origin)
    return !loadedFromSameOrigin;
}

const cachedResourceStats = () => {
    if (!('__observedResources' in window) || __observedResources.length === 0) {
        return {}
    }

    const result = {
        CacheHits: 0,
        CacheHitSize: 0,
        CacheMisses: 0,
        CacheMissedSize: 0,
        CacheHitsJs: 0,
        CacheHitSizeJs: 0,
        CacheMissesJs: 0,
        CacheMissedSizeJs: 0,
        CacheHitsCss: 0,
        CacheHitSizeCss: 0,
        CacheMissesCss: 0,
        CacheMissedSizeCss: 0,
    };
    __observedResources.forEach(([transferSize, encodedBodySize, name, type]) => {
        if (transferSize === 0) {
            result.CacheHits += 1;
            result.CacheHitSize += encodedBodySize;
            if (type === 'script') {
                result.CacheHitsJs += 1;
                result.CacheHitSizeJs += encodedBodySize;
            } else {
                result.CacheHitsCss += 1;
                result.CacheHitSizeCss += encodedBodySize;
            }
        } else {
            result.CacheMisses += 1;
            result.CacheMissedSize += encodedBodySize;
            if (type === 'script') {
                result.CacheMissesJs += 1;
                result.CacheMissedSizeJs += encodedBodySize;
            } else {
                result.CacheMissesCss += 1;
                result.CacheMissedSizeCss += encodedBodySize;
            }
        }
    });
    return result;
}

if (AJS && AJS.trigger) {
    AJS.trigger('analytics', {
        name: EVENT_NAME,
        data: Object.assign({
            SSL: isSSL(),
            AssetsForeignOrigin: assetsLoadFromForeignOrigin(),
        }, cachedResourceStats())
    });
}
