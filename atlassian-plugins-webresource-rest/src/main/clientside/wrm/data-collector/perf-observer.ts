if ("PerformanceObserver" in window) {
    const observerBuffer: Array<[number, number, string, 'script' | 'css']> = [];

    // @ts-expect-error typescript doesnt like assignments to window
    window.__observedResources = observerBuffer;

    const getType = (initiatorType: 'script' | 'link') => {
        return initiatorType === 'script' ? 'script' : 'css';
    }

    const observer = new PerformanceObserver(list => {
        (list.getEntries() as PerformanceResourceTiming[])
            .filter(({initiatorType, name}) => {
                const isValidType = initiatorType === 'script' || isStylesheet(initiatorType, name);
                const hasSameOrigin = new URL(location.href).origin === new URL(name).origin;
                return isValidType && hasSameOrigin
            })
            .forEach(({name, transferSize, encodedBodySize, initiatorType}) => {
                observerBuffer.push([transferSize, encodedBodySize, name, getType(initiatorType as 'script' | 'link')])
            })
    });
    observer.observe({type: 'resource'});
}

const isStylesheet = (type: string, name: string) => {
    return type === 'link' && name.split('.').pop() === 'css';
}
