import {LoadResources} from "./types";

const injectIntoDocument = (element: HTMLElement) => {
    document.head.appendChild(element);

    return new Promise((resolve, reject) => {
        element.addEventListener("load", resolve);
        element.addEventListener("error", reject);
    });
}

const loadStyle = (href: string) => {
    const linkNode = document.createElement("link");
    linkNode.href = href;
    linkNode.rel = "stylesheet";

    return injectIntoDocument(linkNode);
};

const loadScript = (src: string) => {
    const scriptNode = document.createElement("script");
    scriptNode.src = src;
    scriptNode.async = false; // violates ordering contract of the WRM
    scriptNode.defer = true; // minimum phase is DEFER -- for performance and to prevent racing server included DEFERs

    return injectIntoDocument(scriptNode);
};

const load = (key: string) => {
    const [id, url] = key.split("!");
    if (id === "js") {
        return loadScript(url);
    }
    if (id === "css") {
        return loadStyle(url);
    }
    return Promise.reject(new Error("Unsupported resource identifier"));
};

export const loadResources: LoadResources = (keys, onSuccess, onFail) => {
    return Promise.all(keys.map(load)).then(onSuccess, onFail);
};
