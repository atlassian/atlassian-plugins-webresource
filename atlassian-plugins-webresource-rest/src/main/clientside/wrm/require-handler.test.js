import {RequireHandler} from '@wrm/require-handler';
import {loadResources} from '@wrm/resource-loader';
import {mockDocumentReadyState} from "@wrm/queue-manager.test";

jest.mock("@wrm/resource-loader", () => {
  return {
      loadResources: jest.fn()
  }
})

const resourceTypes = {
    JS: 'JS',
    CSS: 'CSS',
};

function createResource(data) {
    const resource = {
        key: "",
        batchType: "resource"
    };
    ['url', 'resourceType', 'batchType', 'media'].forEach(prop => {
        const val = data[prop];
        if (val) {
            resource[prop] = val;
        }
    });
    return resource;
}

const getRequestBodyByIndex = (index) => JSON.parse(server.requests[index].requestBody);

const createJs = (url) => createResource({
    url: url,
    resourceType: resourceTypes.JS
});

const createCss = (url, media) => createResource({
    url: url,
    resourceType: resourceTypes.CSS,
    media: media
});

const findTestLinks = () => document.querySelectorAll("head link[href^=test-]");

const realSetTimeout = setTimeout;
const asyncThings = () => new Promise(resolve => {
    jest.runAllTimers();
    jest.runAllTicks();
    realSetTimeout(resolve);
});
const drainQueue = () => asyncThings();

const FakeServer = function () {
    const reqs = [];
    const responders = [];
    const fakeFetch = jest.fn().mockImplementation((url, data) => {
        var req = {};
        var p = new Promise(function(resolve, reject) {
            req.resolve = resolve;
            req.reject = reject;
        });
        req.p = p;
        req.url = url;
        req.requestData = data;
        req.requestBody = data.body;
        req.requestIndex = reqs.length + 1;
        reqs.push(req);
        return p;
    });
    const originalFetch = window.fetch;
    window.fetch = fakeFetch;

    return {
        get requests() {
            return reqs;
        },

        async respond(status = 404) {
            await drainQueue();
            const responses = reqs.filter(req => !req.status).map(req => {
                const responder = responders.find(resp => resp.condition(req));

                if (responder) {
                    req.status = 200;
                    req.resolve({
                        ok: true,
                        status: 200,
                        json: function () {
                            return JSON.parse(responder.response);
                        }
                    });
                } else {
                    req.status = status;
                    req.resolve({
                        ok: status > 199 && status < 300,
                        status,
                        fake: 'response',
                        json() { return { require: { resources: [] }, interaction: { resources: [] }, fake: 'response-body' }; }
                    });
                }
                return req.p;
            });
            return Promise.allSettled([...responses, asyncThings()]);
        },

        respondWith(resp) {
            responders.push(resp);
        },

        restore() {
            reqs.length = 0;
            responders.length = 0;
            window.fetch = originalFetch;
        },
    };
}

let server;

/**
 * @deprecated
 */
function stubRequireXHR(resources, data, condition, errors) {
    data = data || {};
    errors = errors || {};
    condition = condition || (() => true);

    const response = JSON.stringify({require: {
        resources: resources,
        unparsedData: data,
        unparsedErrors: errors
    }, interaction: {resources: []}});

    server.respondWith({
        condition,
        response,
    });
}

function stubWrmApiResponse({
    require = {},
    interaction = {},
    condition = () => true
} = {}) {
    const defaultPhaseResponse = {resources: [], unparsedData: {}, unparsedErrors: {}};

    const response = JSON.stringify({
        require: {
            ...defaultPhaseResponse,
            ...require
        },
        interaction: {
            ...defaultPhaseResponse,
            ...interaction
        }
    });

    server.respondWith({
        condition,
        response,
    });
}


function formatResourceKeysForRestResponse(resourceKeys) {
    return resourceKeys.map(key => createJs(`${key}.js`))
}

function formatResourceKeysForResourceLoaderRequest(resourceKeys) {
    return resourceKeys.map(key => `js!${key}.js`)
}

function invokeResourceLoader(asSuccess = true, ...args) {
    const lastCall = loadResources.mock.calls[loadResources.mock.calls.length - 1];
    if (!lastCall) throw new Error("No calls to loadResources have been made!");
    const fn = asSuccess ? lastCall[1] : lastCall[2];
    fn(...args);
    return asyncThings();
}

const mockDocumentReadyState = (state) => {
    Object.defineProperty(document, "readyState", {
        get() {
            return state;
        },
        configurable: true,
    });
};

beforeAll(() => {
    jest.useFakeTimers();
    jest.setTimeout(60000);
});

beforeEach(() => {
    server = new FakeServer();
});

afterEach(() => {
    server.restore();
    findTestLinks().forEach(link => link.parentElement.removeChild(link));
    jest.clearAllMocks();
});

test("Base js", async () => {
    stubWrmApiResponse({
        require: {resources: formatResourceKeysForRestResponse(["blah"])},
    });

    const requireHandler = new RequireHandler();

    requireHandler.require("x");
    await server.respond();

    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(loadResources).toHaveBeenNthCalledWith(1, formatResourceKeysForResourceLoaderRequest(['blah']), expect.any(Function), expect.any(Function));

});

test("Base css", async () => {
    stubRequireXHR([createCss("blah.css")]);
    const requireHandler = new RequireHandler();

    requireHandler.require("x");
    await server.respond();

    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(loadResources).toHaveBeenNthCalledWith(1, ["css!blah.css"], expect.any(Function), expect.any(Function));
});

test("JS + CSS", async () => {
    stubRequireXHR([createJs("blah.js"), createCss("blah.css"), createJs("blah1.js"), createCss("blah1.css")]);
    const requireHandler = new RequireHandler();

    requireHandler.require("x");
    await server.respond();

    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(loadResources).toHaveBeenNthCalledWith(1, ["js!blah.js", "css!blah.css", "js!blah1.js", "css!blah1.css"], expect.any(Function), expect.any(Function));
});

test("Passed callback is executed", async () => {
    stubRequireXHR([createJs("blah.js")]);
    const requireCallback = jest.fn();
    const requireHandler = new RequireHandler();

    requireHandler.require("x", requireCallback);
    await server.respond();
    await invokeResourceLoader();

    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(requireCallback).toHaveBeenCalledTimes(1);
});

test("Deferred callback is executed", async () => {
    stubRequireXHR([createJs("blah.js")]);
    const doneCallback = jest.fn();
    const requireHandler = new RequireHandler();

    requireHandler.require("x").done(doneCallback);
    await server.respond();
    await invokeResourceLoader();

    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(doneCallback).toHaveBeenCalledTimes(1);
});

test("Handles empty requests", async () => {
    stubRequireXHR([]);
    const requireCallback = jest.fn();
    const requireHandler = new RequireHandler();

    requireHandler.require("", requireCallback);
    requireHandler.require([], requireCallback);
    requireHandler.require(false, requireCallback);
    await asyncThings();

    expect(loadResources).not.toHaveBeenCalled();
    expect(requireCallback).toHaveBeenCalledTimes(3);
});

test("Combines all requests in same stack", async () => {
    const requireHandler = new RequireHandler();
    requireHandler.require("x");
    requireHandler.require("y");
    requireHandler.require("z");
    await drainQueue();
    requireHandler.require("a");
    requireHandler.require("b");
    requireHandler.require("c");
    await server.respond(200);

    expect(server.requests.length).toEqual(2);
    expect(JSON.parse(server.requests[0].requestBody).require).toEqual(["x","y","z"]);
    expect(JSON.parse(server.requests[1].requestBody).require).toEqual(["a","b","c"]);
});

// PLUGWEB-437
test("Blocks subsequent server requests until first is handled", async () => {
    stubRequireXHR([createJs("x.js"), createJs("y.js"), createJs("z.js")]);
    const requireHandler = new RequireHandler();
    requireHandler.require("x");
    requireHandler.require("y");
    requireHandler.require("z");
    await drainQueue();
    requireHandler.require("a");
    requireHandler.require("b");
    requireHandler.require("c");
    await drainQueue();

    expect(server.requests.length).toEqual(1);

    await server.respond(200);

    expect(loadResources).toHaveBeenCalledTimes(1);
});

// PLUGWEB-437
test("Blocks subsequent server requests until resource-loader resolves", async () => {
    stubRequireXHR([createJs("x.js"), createJs("y.js"), createJs("z.js")]);
    const requireHandler = new RequireHandler();
    requireHandler.require("x");
    requireHandler.require("y");
    requireHandler.require("z");
    await drainQueue();
    requireHandler.require("a");
    requireHandler.require("b");
    requireHandler.require("c");
    await drainQueue();

    expect(server.requests.length).toEqual(1);

    await server.respond(200);
    expect(server.requests.length).toEqual(1);

    await invokeResourceLoader();

    expect(server.requests.length).toEqual(2);
    expect(loadResources).toHaveBeenCalledTimes(1);
});

test("Caches resource requests for multiple invocations", async () => {
    stubRequireXHR([createJs("blah.js")]);
    const doneCallback = jest.fn();
    const requireHandler = new RequireHandler();

    requireHandler.require("x").done(doneCallback);
    await server.respond();
    requireHandler.require("x").done(doneCallback);
    await invokeResourceLoader();

    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(server.requests.length).toEqual(1);
    expect(doneCallback).toHaveBeenCalledTimes(2);
});

test("Caches resource requests for multiple invocations before first request completes", async () => {
    stubRequireXHR([createJs("blah.js")]);
    const doneCallback = jest.fn();
    const requireHandler = new RequireHandler();

    requireHandler.require("x").done(doneCallback);
    await drainQueue();
    requireHandler.require("x").done(doneCallback);
    await server.respond();
    await invokeResourceLoader();

    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(server.requests.length).toEqual(1);
    expect(doneCallback).toHaveBeenCalledTimes(2);
});

test("Caches resource requests for multiple invocations (array version)", async () => {
    stubRequireXHR([createJs("blah.js")]);
    const doneCallback = jest.fn();
    const requireHandler = new RequireHandler();

    requireHandler.require(["x", "y", "z"]).done(doneCallback);
    await server.respond();
    requireHandler.require(["x", "y", "z"]).done(doneCallback);
    await invokeResourceLoader();

    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(server.requests.length).toEqual(1);
    expect(doneCallback).toHaveBeenCalledTimes(2);
});

test("Does not do resource request cache collisions between unrelated objects", async () => {
    stubRequireXHR([createJs("blah1.js")], null, xhr => {
        const resources = JSON.parse(xhr.requestBody).require;
        return resources.length === 1 && resources[0] === "wr!xxx";
    });
    stubRequireXHR([createJs("blah2.js")], null, xhr => {
        const resources = JSON.parse(xhr.requestBody).require;
        return resources.length === 1 && resources[0] === "wr!yyy";
    });
    const doneCallback1 = jest.fn();
    const doneCallback2 = jest.fn();
    const requireHandler = new RequireHandler();

    requireHandler.require("wr!xxx").done(doneCallback1);
    await drainQueue();
    requireHandler.require("wr!yyy").done(doneCallback2);
    await server.respond();
    await invokeResourceLoader();
    await server.respond();
    await invokeResourceLoader();

    expect(server.requests.length).toEqual(2);
    expect(doneCallback1).toHaveBeenCalledTimes(1);
    expect(doneCallback2).toHaveBeenCalledTimes(1);

    expect(loadResources).toHaveBeenCalledTimes(2);
    expect(server.requests.length).toEqual(2);
});

test("Fires failure callback if request fails", async () => {
    const expectRequireFailure = jest.fn(); // fails because required resource doesn't exist
    const requireHandler = new RequireHandler();

    requireHandler.require("x").fail(expectRequireFailure);
    await server.respond();

    expect(expectRequireFailure).toBeCalledTimes(1);
});

test("Fires failure callback if request fails for request waiting after failed previous request", async () => {
    const failCallback = jest.fn(); // fails because required resources doesn't exist
    const requireHandler = new RequireHandler();

    requireHandler.require("y").fail(failCallback);
    requireHandler.require("x").fail(failCallback);
    await server.respond();
    await server.respond();

    expect(failCallback).toHaveBeenCalledTimes(2);
});

test("Fires failure callback if request fails for request waiting after successful previous request", async () => {
    stubRequireXHR([createJs("blah1.js")], null, xhr => {
        const resources = JSON.parse(xhr.requestBody).require;
        return resources.length === 1 && resources[0] === "wr!xxx";
    });
    const expectRequireSuccess = jest.fn();
    const expectRequireFailure = jest.fn(); //fails because required resource doesn't exist

    const requireHandler = new RequireHandler();

    requireHandler.require("wr!xxx").done(expectRequireSuccess);
    await drainQueue();
    requireHandler.require("x").fail(expectRequireFailure);

    await server.respond();
    await invokeResourceLoader();
    expect(expectRequireSuccess).toHaveBeenCalledTimes(1);

    await server.respond();
    expect(expectRequireFailure).toHaveBeenCalledTimes(1);

    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(server.requests.length).toEqual(2);
});

test("Fires success callback if request was successful for request waiting after failed previous request", async () => {
    stubRequireXHR([createJs("blah1.js")], null, xhr => {
        const resources = JSON.parse(xhr.requestBody).require;
        return resources.length === 1 && resources[0] === "wr!xxx";
    });
    const expectRequireSuccess = jest.fn();
    const expectRequireFailure = jest.fn(); // fails because required resource doesn't exist
    const requireHandler = new RequireHandler();

    requireHandler.require("x").fail(expectRequireFailure);
    await drainQueue();
    requireHandler.require("wr!xxx").done(expectRequireSuccess);

    await server.respond();
    expect(expectRequireFailure).toHaveBeenCalledTimes(1);

    await server.respond();
    await invokeResourceLoader();
    expect(expectRequireSuccess).toHaveBeenCalledTimes(1);

    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(server.requests.length).toEqual(2);
});

test("Cache of resource request for multiple invocations is removed after request failed", async () => {
    const expectRequireFailure = jest.fn(); // fails because required resources doesn't exist
    const requireHandler = new RequireHandler();

    requireHandler.require("x").fail(expectRequireFailure);
    await drainQueue();
    requireHandler.require("x").fail(expectRequireFailure);
    await server.respond();
    await server.respond();

    // Request was cached before server responded with error
    expect(server.requests.length).toEqual(1);

    requireHandler.require("x").fail(expectRequireFailure);
    await server.respond();

    // Request was removed from cache on fail, so should be requested second time
    expect(server.requests.length).toEqual(2);
    expect(expectRequireFailure).toBeCalledTimes(3);
});

test("Failure callback is removed from cache if initial request fails", async () => {
    const expectRequireFailure = jest.fn(); // fails because required resources doesn't exist
    const requireHandler = new RequireHandler();

    requireHandler.require("x").fail(expectRequireFailure);
    requireHandler.require("x").fail(expectRequireFailure);
    await server.respond();
    await server.respond();

    // Request was cached before server responded with error
    expect(server.requests.length).toEqual(1);

    requireHandler.require("x").fail(expectRequireFailure);
    await server.respond();

    // Request was removed from cache on fail, so should be requested second time
    expect(server.requests.length).toEqual(2);
    expect(expectRequireFailure).toBeCalledTimes(3);
});

test("Fires failure callback if resource-loader request fails", async () => {
    stubRequireXHR([createJs("blah.js")]);
    const mockedFail = jest.fn();
    const requireHandler = new RequireHandler();

    requireHandler.require("x").fail(mockedFail);
    await server.respond();
    await invokeResourceLoader(false, "arg1", "arg2");

    expect(mockedFail).toHaveBeenCalledTimes(1)
    expect(mockedFail).toHaveBeenCalledWith("arg1", "arg2");
    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(loadResources).toHaveBeenNthCalledWith(1, formatResourceKeysForResourceLoaderRequest(['blah']), expect.any(Function), expect.any(Function));
});

test("Failure callback is cached if resource-loader request fails", async () => {
    stubRequireXHR([createJs("blah.js")]);
    const mockedFail = jest.fn();
    const requireHandler = new RequireHandler();

    requireHandler.require("x").fail(mockedFail);
    await server.respond();
    requireHandler.require("x").fail(mockedFail);

    await invokeResourceLoader(false, "arg1", "arg2");

    expect(server.requests.length).toEqual(1);
    expect(mockedFail).toHaveBeenCalledTimes(2)
    expect(mockedFail).toHaveBeenCalledWith("arg1", "arg2");
    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(loadResources).toHaveBeenNthCalledWith(1, formatResourceKeysForResourceLoaderRequest(['blah']), expect.any(Function), expect.any(Function));
});

test("CSS with media queries are not added via resource-loader", async () => {
    stubRequireXHR([createCss("test-blah.css", "print")]);
    const requireHandler = new RequireHandler();

    requireHandler.require("x");
    await server.respond();

    expect(findTestLinks().length).toEqual(0);

    await invokeResourceLoader();

    const links = findTestLinks();
    expect(links.length).toEqual(1);
    expect(links[0].getAttribute("href")).toEqual("test-blah.css");
    expect(links[0].getAttribute('media')).toEqual('print');
    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(loadResources).toHaveBeenNthCalledWith(1, [], expect.any(Function), expect.any(Function));
});

test("CSS with media queries are not added via resource-loader", async () => {
    stubRequireXHR([createCss("test-blah.css", "print")]);
    const requireHandler = new RequireHandler();

    requireHandler.require("x");
    await server.respond();

    expect(findTestLinks().length).toEqual(0);

    await invokeResourceLoader();

    const links = findTestLinks();
    expect(links.length).toEqual(1);
    expect(links[0].getAttribute("href")).toEqual("test-blah.css");
    expect(links[0].getAttribute("media")).toEqual("print");
    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(loadResources).toHaveBeenNthCalledWith(1, [], expect.any(Function), expect.any(Function));
});

test("CSS with media queries are discovered on subsequent calls", async () => {
    stubRequireXHR([createCss("test-blah.css", "print")]);
    const requireHandler = new RequireHandler();

    requireHandler.require("x");
    await server.respond();
    await invokeResourceLoader();
    requireHandler.require("y");
    await server.respond();
    await invokeResourceLoader();

    const links = findTestLinks();
    expect(links.length).toEqual(1);
    expect(links[0].getAttribute("href")).toEqual("test-blah.css");
    expect(links[0].getAttribute("media")).toEqual("print");
    expect(loadResources).toHaveBeenCalledTimes(2);
    expect(loadResources).toHaveBeenNthCalledWith(1, [], expect.any(Function), expect.any(Function));
});

test("CSS media query check respects media='all'", async () => {
    stubRequireXHR([createCss("test-blah.css", "all")]);
    const requireHandler = new RequireHandler();

    requireHandler.require("x");
    await server.respond();

    expect(findTestLinks().length).toEqual(0);
    expect(loadResources).toHaveBeenCalledTimes(1);
    expect(loadResources).toHaveBeenNthCalledWith(1, ["css!test-blah.css"], expect.any(Function), expect.any(Function));
});

describe('JS that has provided', () => {
    test("resources and unparsedData", async () => {
        // This case depends on `web-resource/data` module, but we don't have it here, stubbing it instead.
        WRM.data.claim.mockImplementation((key) => JSON.parse(WRM._unparsedData[key]));

        stubRequireXHR([createJs("blah.js")], {"com.foo.x": JSON.stringify({x: "y"})});
        const requireHandler = new RequireHandler();

        requireHandler.require("x");
        await server.respond();

        const claim = WRM.data.claim("com.foo.x");
        expect(claim.x).toEqual("y");
        expect(loadResources).toHaveBeenCalledTimes(1);
        expect(loadResources).toHaveBeenNthCalledWith(1, formatResourceKeysForResourceLoaderRequest(['blah']), expect.any(Function), expect.any(Function));
    });

    test("unparsedData but resources are empty", async () => {
        // This case depends on `web-resource/data` module, but we don't have it here, stubbing it instead.
        WRM.data.claim.mockImplementation((key) => JSON.parse(WRM._unparsedData[key]));

        stubRequireXHR([], {"com.foo.x": JSON.stringify({x: "y"})});
        const requireHandler = new RequireHandler();

        requireHandler.require("x");
        await server.respond();

        const claim = WRM.data.claim("com.foo.x");
        expect(claim.x).toEqual("y");
        expect(loadResources).not.toHaveBeenCalled();
    });
});

describe('JS that has provided', () => {
    test("errors and resources", async () => {
        // This case depends on `web-resource/data` module, but we don't have it here, stubbing it instead.
        WRM.data.claim.mockImplementation((key) => JSON.parse(WRM._unparsedData[key]));

        stubRequireXHR(
            [createJs("blah.js")],
            {},
            null,
            {"com.foo.x": JSON.stringify("")}
        );
        const requireHandler = new RequireHandler();

        requireHandler.require("x");
        await server.respond();

        expect(loadResources).toHaveBeenCalledTimes(1);
        expect(loadResources).toHaveBeenNthCalledWith(1, formatResourceKeysForResourceLoaderRequest(['blah']), expect.any(Function), expect.any(Function));
        expect(WRM._unparsedErrors).toEqual({'com.foo.x': '""'});
    });

    test("errors but without resources", async () => {
        // This case depends on `web-resource/data` module, but we don't have it here, stubbing it instead.
        WRM.data.claim.mockImplementation((key) => JSON.parse(WRM._unparsedData[key]));

        stubRequireXHR(
            [],
            {},
            null,
            {"com.foo.x": JSON.stringify("")}
        );
        const requireHandler = new RequireHandler();

        requireHandler.require("x");
        await server.respond();

        expect(loadResources).not.toHaveBeenCalled();
        expect(WRM._unparsedErrors).toEqual({'com.foo.x': '""'});
    });
});

// PLUGWEB-437
test("Require calls are queued while one is in flight and consolidated to reduce network utilisation", async () => {
    // It doesn't matter what the server tells the browser to load;
    // all that matters is the number of requests the client makes to find out.
    stubRequireXHR([]);

    const requireHandler = new RequireHandler();

    requireHandler.require('1st');
    await drainQueue();
    expect(server.requests.length).toEqual(1);

    requireHandler.require('2nd');
    requireHandler.require(['3rd', '4th']);

    // there should still be just one request made at this point because the first has not yet finished
    expect(server.requests.length).toEqual(1);

    // Resolve the first request, which should cause a second request to be queued.
    // The subsequent require calls should be compacted in to a single request.
    await server.respond();
    expect(server.requests.length).toEqual(2);

    // first request should be for a single resource.
    // second request should be for remaining resources.
    expect(getRequestBodyByIndex(0).require).toEqual(['1st']);
    expect(getRequestBodyByIndex(1).require).toEqual(['2nd', '3rd', '4th']);
});

// PLUGWEB-437
test("Nested require calls are queued and consolidated to reduce network utilisation when callback used", async () => {
    // It doesn't matter what the server tells the browser to load;
    // all that matters is the number of requests the client makes to find out.
    stubRequireXHR([]);

    const requireHandler = new RequireHandler();

    requireHandler.require('main', () => {
        requireHandler.require('1st', () => {
            requireHandler.require('deep1');
        });
        requireHandler.require('2nd');
    });
    requireHandler.require('secondary', () => {
        requireHandler.require(['3rd', '4th'], () => {
            requireHandler.require('deep2');
        });
    });

    // Respond an unusual number of times.
    await server.respond();
    await server.respond();
    await server.respond();
    await server.respond();

    // should make the minimum possible number of requests (3, as per nesting depth + when the queue is drained
    expect(server.requests.length).toEqual(3);
    // first request should be for the first two top-level calls
    expect(getRequestBodyByIndex(0).require).toEqual(['main', 'secondary']);
    // second request is for calls discovered after first response
    expect(getRequestBodyByIndex(1).require).toEqual(['1st', '2nd', '3rd', '4th']);
    // third request is for calls discovered after second response
    expect(getRequestBodyByIndex(2).require).toEqual(['deep1', 'deep2']);
});

// PLUGWEB-437
test("Nested require calls are queued and consolidated to reduce network utilisation when Promise then used", async () => {
    // It doesn't matter what the server tells the browser to load;
    // all that matters is the number of requests the client makes to find out.
    stubRequireXHR([]);

    const requireHandler = new RequireHandler();

    requireHandler.require('main').then(() => {
        requireHandler.require('1st').then(() => {
            requireHandler.require('deep1');
        });
        requireHandler.require('2nd');
    });
    requireHandler.require('secondary').then(() => {
        requireHandler.require(['3rd', '4th']).then(() => {
            requireHandler.require('deep2');
        });
    });

    // Respond an unusual number of times.
    await server.respond();
    await server.respond();
    await server.respond();
    await server.respond();

    // should make the minimum possible number of requests (3, as per nesting depth + when the queue is drained)
    expect(server.requests.length).toEqual(3);
    // first request should be for the first two top-level calls
    expect(getRequestBodyByIndex(0).require).toEqual(['main', 'secondary']);
    // second request is for calls discovered after first response
    expect(getRequestBodyByIndex(1).require).toEqual(['1st', '2nd', '3rd', '4th']);
    // third request is for calls discovered after second response
    expect(getRequestBodyByIndex(2).require).toEqual(['deep1', 'deep2']);
});

// PLUGWEB-437
test("Order for resolving require callbacks when resources loaded should align with require call order", async () => {
    // It doesn't matter what the server tells the browser to load;
    // in this test we're only concerned with resolution order.
    stubRequireXHR([]);

    const step = jest.fn();
    const verifySteps = function(correctOrder) {
        const actualOrder = [].concat(step.mock.calls).map(arg => arg[0]);
        expect(actualOrder).toEqual(correctOrder);
    };

    const requireHandler = new RequireHandler();

    // Make our first require which, when resolved, will make a nested require call.
    step('before requiring 1st');
    requireHandler.require(['1st']).done(() => {
        step('1st callback');

        // Make a nested require, which should only occur once the first response from the server comes back.
        step('before requiring bar');
        requireHandler.require('bar', () => {
            step('bar callback');
        });
        step('after requiring bar');
    });

    // Make a second and third require call in serial.
    step('before requiring 2nd');
    requireHandler.require(['2nd'], () => {
        step('2nd callback');
    });

    step('before requiring 3rd');
    requireHandler.require(['3rd'], () => {
        step('3rd callback');
    });

    step('after all top-level require calls');

    // Resolve the first request, which should cause a second request to be queued
    await server.respond();
    // Resolve the second request, which should cause the remaining queued require
    // callbacks to resolve as well.
    await server.respond();
    verifySteps([
        'before requiring 1st',
        'before requiring 2nd',
        'before requiring 3rd',
        'after all top-level require calls',
        '1st callback',
        'before requiring bar',
        'after requiring bar',
        '2nd callback',
        '3rd callback',
        'bar callback',
    ]);
});

// PLUGWEB-445
// NOTE: This test is checking for the root cause of a problem, but not actually testing the behaviours a consumer would see.
//       To do that, we would need a test that showed the callback for require call #3 executes before call #2.
it("Multiple require calls waiting on a consolidated request do not result in multiple resource-loader calls", async () => {
    stubRequireXHR([createJs('blocking-request.js')], null, ({requestIndex}) => requestIndex === 1);
    const requireHandler = new RequireHandler();

    // Start lining up some require calls.
    requireHandler.require(['wr!blocking-request']);
    // Make the first require call. While the server has not responded, this blocks other requests.
    await drainQueue();
    stubRequireXHR([createJs("a.js"), createJs("b.js"), createJs("c.js"), createJs("d.js")], null, ({requestIndex}) => requestIndex === 2);
    // Make the second and third require calls. These calls will get enqueued,
    // thus will get consolidated in to a single AJAX call.
    requireHandler.require(['wr!a', 'wr!b']);
    requireHandler.require(['wr!c', 'wr!d']);

    await server.respond(); // Respond to the blocking request.
    await invokeResourceLoader();
    await server.respond(); // Respond to the second consolidated request.

    expect(loadResources).toHaveBeenCalledTimes(2);
    expect(loadResources).toHaveBeenNthCalledWith(1, formatResourceKeysForResourceLoaderRequest(["blocking-request"]), expect.any(Function), expect.any(Function));
    expect(loadResources).toHaveBeenNthCalledWith(2, formatResourceKeysForResourceLoaderRequest(["a", "b", "c", "d"]), expect.any(Function), expect.any(Function));
});

describe('[deprecated] jQuery.Deferred API', () => {
    test("#fail called if request fails", async () => {
        const expectRequireFailure = jest.fn(); // fails because required resource doesn't exist
        const requireHandler = new RequireHandler();

        requireHandler.require("x").fail(expectRequireFailure);
        await server.respond(400);

        expect(expectRequireFailure).toBeCalledTimes(1);
    });

    test("#fail chained calls do not affect each-other", async () => {
        const onFail = jest.fn().mockReturnValue("changed fail");
        const onDone = jest.fn().mockReturnValue("changed done");
        const requireHandler = new RequireHandler();

        requireHandler.require("a").fail(onFail).done(onDone);
        requireHandler.require("b").done(onDone).fail(onFail);
        requireHandler.require("c").done(onDone).done(onDone);
        requireHandler.require("d").fail(onFail).fail(onFail);
        await server.respond(400);

        expect(onDone).toBeCalledTimes(0);
        expect(onFail).toBeCalledTimes(4);
        expect(onFail).not.toHaveBeenCalledWith("changed fail");
    });

    test("#done called in success case", async () => {
        const expectRequireSuccess = jest.fn();
        const requireHandler = new RequireHandler();

        requireHandler.require("y").done(expectRequireSuccess);
        await server.respond(200);

        expect(expectRequireSuccess).toBeCalledTimes(1);
    });

    test("#done chained calls do not affect each-other", async () => {
        const onFail = jest.fn().mockReturnValue("changed fail");
        const onDone = jest.fn().mockReturnValue("changed done");
        const requireHandler = new RequireHandler();

        requireHandler.require("a").fail(onFail).done(onDone);
        requireHandler.require("b").done(onDone).fail(onFail);
        requireHandler.require("c").done(onDone).done(onDone);
        requireHandler.require("d").fail(onFail).fail(onFail);
        await server.respond(200);

        expect(onDone).toBeCalledTimes(4);
        expect(onFail).toBeCalledTimes(0);
        expect(onDone).not.toHaveBeenCalledWith("changed done");
    });

    test("#always calls work", async () => {
        const onAll = jest.fn().mockReturnValue("changed all");
        const onFail = jest.fn().mockReturnValue("changed fail");
        const onDone = jest.fn().mockReturnValue("changed done");
        const requireHandler = new RequireHandler();

        requireHandler.require("a").always(onAll);
        requireHandler.require("b").always(onAll).always(onAll);
        requireHandler.require("c").done(onDone).always(onAll);
        requireHandler.require("d").fail(onFail).always(onAll);
        await server.respond(200);

        expect(onDone).toBeCalledTimes(1);
        expect(onAll).toBeCalledTimes(5);
        expect(onAll).not.toHaveBeenCalledWith("changed all");
    });

    test("#progress chained calls work", async () => {
        const onProgress = jest.fn().mockReturnValue("changed progress");
        const onDone = jest.fn().mockReturnValue("changed done");
        const requireHandler = new RequireHandler();

        requireHandler.require("a").progress(onProgress).done(onDone).progress(onProgress);
        await server.respond(200);

        expect(onDone).toBeCalledTimes(1);
    });

    test("jQuery.when usage", async () => {
        const onDone = jest.fn().mockReturnValue("changed done");
        const requireHandler = new RequireHandler();

        const a = requireHandler.require("a");
        const b = requireHandler.require("b");
        const c = requireHandler.require("c");

        jQuery.when(a, b, c).then(onDone);
        expect(onDone).toBeCalledTimes(0);

        await server.respond(200);
        // jQuery.when likely set up a timer to process the resolution.
        await asyncThings();

        expect(onDone).toBeCalledTimes(1);
    });
});

describe('requireLazily', () => {
    const mockAddEventListener = () => {
        window.document.addEventListener = jest.fn();
    };

    describe('resource-loader request is resolved after require request', () => {
        test('when phase requests are grouped', async () => {
            stubWrmApiResponse({
                require: {resources: formatResourceKeysForRestResponse(["a", "b", "c"])},
                interaction: {resources: formatResourceKeysForRestResponse(["x", "y", "z"])}
            });

            const requireHandler = new RequireHandler();
            requireHandler.requireLazily("wr!x");
            requireHandler.requireLazily("wr!y");
            requireHandler.requireLazily("wr!z");
            requireHandler.require("wr!a");
            requireHandler.require("wr!b");
            requireHandler.require("wr!c");

            await drainQueue();
            await server.respond();
            await invokeResourceLoader();
            await invokeResourceLoader();

            expect(server.requests.length).toEqual(1)
            expect(loadResources).toHaveBeenCalledTimes(2);
            expect(loadResources).toHaveBeenNthCalledWith(1, formatResourceKeysForResourceLoaderRequest(["a", "b", "c"]), expect.any(Function), expect.any(Function));
            expect(loadResources).toHaveBeenNthCalledWith(2, formatResourceKeysForResourceLoaderRequest(["x", "y", "z"]), expect.any(Function), expect.any(Function));
        });

        it('when phase requests are mixed', async () => {
            stubWrmApiResponse({
                require: {resources: formatResourceKeysForRestResponse(["a", "b", "c"])},
                interaction: {resources: formatResourceKeysForRestResponse(["x", "y", "z"])}
            });

            const requireHandler = new RequireHandler();
            requireHandler.requireLazily("wr!x");
            requireHandler.require("wr!a");
            requireHandler.requireLazily("wr!y");
            requireHandler.require("wr!b");
            requireHandler.requireLazily("wr!z");
            requireHandler.require("wr!c");

            await drainQueue();
            await server.respond();
            await invokeResourceLoader();
            await invokeResourceLoader();

            expect(server.requests.length).toEqual(1)
            expect(loadResources).toHaveBeenCalledTimes(2);
            expect(loadResources).toHaveBeenNthCalledWith(1, formatResourceKeysForResourceLoaderRequest(["a", "b", "c"]), expect.any(Function), expect.any(Function));
            expect(loadResources).toHaveBeenNthCalledWith(2, formatResourceKeysForResourceLoaderRequest(["x", "y", "z"]), expect.any(Function), expect.any(Function));
        });
    });

    test('when only requireLazily is used', async () => {
        stubWrmApiResponse({
            interaction: {resources: formatResourceKeysForRestResponse(["x", "y", "z"])}
        });

        const requireHandler = new RequireHandler();
        requireHandler.requireLazily("wr!x");
        requireHandler.requireLazily("wr!y");
        requireHandler.requireLazily("wr!z");

        await drainQueue();
        await server.respond();
        await invokeResourceLoader();

        expect(server.requests.length).toEqual(1)
        expect(loadResources).toHaveBeenCalledTimes(1);
        expect(loadResources).toHaveBeenNthCalledWith(1, formatResourceKeysForResourceLoaderRequest(["x", "y", "z"]), expect.any(Function), expect.any(Function));
    });

    describe("before DomContentLoaded", () => {
        let registeredDclCallback;
        let requireHandler;

        beforeEach(() => {
            mockDocumentReadyState("interactive");
            mockAddEventListener();

            requireHandler = new RequireHandler();
            registeredDclCallback = window.document.addEventListener.mock.calls[0][1];
        });

        test('requireLazily resources not drained before DCL', async () => {
            stubWrmApiResponse({
                require: {resources: formatResourceKeysForRestResponse(["a", "b", "c"])},
                condition: (req) => req.requestBody.includes('wr!a')
            });
            stubWrmApiResponse({
                interaction: {resources: formatResourceKeysForRestResponse(["x", "y", "z"])},
                condition: (req) => req.requestBody.includes('wr!x')
            });

            requireHandler.requireLazily("wr!x");
            requireHandler.require("wr!a");
            requireHandler.requireLazily("wr!y");
            requireHandler.require("wr!b");
            requireHandler.requireLazily("wr!z");
            requireHandler.require("wr!c");

            await drainQueue();
            await server.respond();
            await invokeResourceLoader();

            registeredDclCallback();
            await drainQueue();
            await server.respond();
            await invokeResourceLoader();

            expect(loadResources).toHaveBeenCalledTimes(2);
            expect(loadResources).toHaveBeenNthCalledWith(1, formatResourceKeysForResourceLoaderRequest(["a", "b", "c"]), expect.any(Function), expect.any(Function));
            expect(loadResources).toHaveBeenNthCalledWith(2, formatResourceKeysForResourceLoaderRequest(["x", "y", "z"]), expect.any(Function), expect.any(Function));
        });

        test('when requireLazily is used before DCL', async () => {
            stubWrmApiResponse();
            requireHandler.requireLazily("wr!x");
            requireHandler.requireLazily("wr!y");
            requireHandler.requireLazily("wr!z");

            await drainQueue();
            expect(server.requests.length).toEqual(0);

            registeredDclCallback();
            await drainQueue();
            expect(server.requests.length).toEqual(1);
        });

        test('when requireLazily is used before and after DCL', async () => {
            stubWrmApiResponse();

            requireHandler.requireLazily("wr!x");

            registeredDclCallback();
            await drainQueue();
            await server.respond();

            requireHandler.requireLazily("wr!y");
            await drainQueue();
            await server.respond();

            expect(server.requests.length).toEqual(2);
        });
    });
});

test('should not call WRM API when requested resource keys are empty', async () => {
    const requireHandler = new RequireHandler();

    requireHandler.require('  ');
    requireHandler.require([]);
    requireHandler.require(['']);
    requireHandler.require(['  ']);
    requireHandler.require([{}]);
    await drainQueue();

    expect(server.requests.length).toEqual(0);
});

test('should allow overriding resource locale', async () => {
    mockDocumentReadyState('complete')
    const requireHandler = new RequireHandler();

    const makeUrl = (locale) => `http://localhost/some-script.js?locale=${locale}`
    const defaultLocale = 'en';

    const loadAndCheck = async (expectedLocale) => {
        await drainQueue();
        await server.respond();
        await invokeResourceLoader();
        expect(loadResources.mock.calls).toEqual([
            [[`js!${makeUrl(expectedLocale)}`], expect.any(Function), expect.any(Function)]
        ])
        loadResources.mockClear();
    }

    stubWrmApiResponse({
        require: {resources: [createJs(makeUrl(defaultLocale))]}
    });

    requireHandler.require(['some-script-1']);
    await loadAndCheck(defaultLocale)

    window.WRM.__localeOverride = 'en-MOON';
    requireHandler.require(['some-script-2']);
    await loadAndCheck('en-MOON')

    delete window.WRM.__localeOverride;
    requireHandler.require(['some-script-3']);
    await loadAndCheck(defaultLocale)

    window.WRM.__localeOverride = '';
    requireHandler.require(['some-script-4']);
    await loadAndCheck(defaultLocale)
})
