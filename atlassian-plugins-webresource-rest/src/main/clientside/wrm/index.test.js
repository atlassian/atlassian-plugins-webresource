import {RequireHandler} from "@wrm/require-handler";

jest.mock("@wrm/require-handler");

describe("index", () => {
    afterEach(() => {
        // Reset modules after each test to override define function
        jest.resetModules();
    });

    describe("without define function", () => {
        beforeEach(async () => {
            await import("@wrm");
        });

        test("require and requireLazily should use same instance of RequireHandler regardless of the number of calls", async () => {
            WRM.require([]);
            WRM.require([]);
            WRM.requireLazily([]);
            WRM.requireLazily([]);

            expect(RequireHandler).toHaveBeenCalledTimes(1);
        });

        describe("require()", () => {
            test("is definied", (done) => {
                expect(WRM.require).toBeDefined();
                done();
            });

            test("should not fail with dummy data", (done) => {
                const callback = () => WRM.require([], () => {});

                expect(callback).not.toThrowError();
                done();
            });
        });

        describe("requireLazily()", () => {
            test("is definied", (done) => {
                expect(WRM.requireLazily).toBeDefined();
                done();
            });

            test("should not fail with dummy data", (done) => {
                const callback = () => WRM.requireLazily([], () => {});

                expect(callback).not.toThrowError();
                done();
            });
        });
    });

    describe("with define", () => {
        test("define should be called with proper AMD module names", async () => {
            global.define = jest.fn();

            await import("@wrm");

            expect(define).toHaveBeenCalledTimes(4);
            expect(define).toHaveBeenNthCalledWith(1, "wrm/require", expect.any(Function));
            expect(define).toHaveBeenNthCalledWith(2, "wrm/require-lazily", expect.any(Function));
            expect(define).toHaveBeenNthCalledWith(3, "wrm/set-logging-level", expect.any(Function));
            expect(define).toHaveBeenNthCalledWith(4, "wrm", expect.any(Function));
        });

        test("defined modules should be callable", async () => {
            let functions = [];
            global.define = jest.fn().mockImplementation((name, f) => {
                functions.push(f);
            });

            await import("@wrm");

            expect(define).toHaveBeenCalledTimes(4);
            expect(() => functions.forEach((f) => f())).not.toThrow();
        });
    });
});
