type Level = "debug" | "info" | "log" | "warn" | "error";

const levelMap: { [key in Level]: number } = {
    debug: 1,
    info: 2,
    log: 3,
    warn: 4,
    error: 5,
};

let loggingLevelName: Level = 'log';

const isLoggable = (level: number): boolean => {
    const usedLevel = levelMap[loggingLevelName] || levelMap.log;

    return level >= usedLevel;
};

const generateLogFn = (levelName: Level, headers: string[]) => (...args: any[]) => {
    if (isLoggable(levelMap[levelName])) {
        console[levelName](...headers, ...args);
    }
};

export const createLogger = ({ context = "default" }) => {
    const headers = [`[WRM]`, `[${context}]`];

    return Object.keys(levelMap).reduce((acc, levelName) => {
        return {
            ...acc,
            [levelName]: generateLogFn(levelName as Level, headers),
        };
    }, {} as {[key in Level]: (...args: any[]) => {}});
};

export const setLoggingLevel = (levelName: Level) => {
    loggingLevelName = levelName;
};