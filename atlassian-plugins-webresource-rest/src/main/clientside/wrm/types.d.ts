export type Phase = "require" | "interaction";

export type LoadResources = (
    url: string[],
    onSuccess?: (...args: unknown[]) => void,
    onFail?: (...args: unknown[]) => void
) => void;

export type QueueElement = {
    phase: Phase;
    resourceKeys: string[];
    resolve: (value: unknown) => void;
    reject: (value: unknown) => void;
};

export type ProcessedResult = {
    result: unknown;
    queuedElements: QueueElement[];
};

export type Process = (queuedItems: QueueElement[]) => Promise<ProcessedResult>;
