import {RequestedResourcesTracker} from '@wrm/requested-resources-tracker';

const serverSideRenderedAttribute = "data-initially-rendered"
const wrmKeyAttribute = "data-wrm-key";
const wrmBatchTypeAttribute = "data-wrm-batch-type";
let el;

function addScript(scripts) {
    scripts.keys.forEach((key) => {
        const script = document.createElement("script");
        script.setAttribute(wrmKeyAttribute, key);
        script.setAttribute(wrmBatchTypeAttribute, scripts.type);
        script.setAttribute(serverSideRenderedAttribute, '');
        el.appendChild(script);
    });
}

function addStylesheet(scripts) {
    scripts.keys.forEach((key) => {
        const link = document.createElement("link");
        link.rel = "stylesheet";
        link.setAttribute(wrmKeyAttribute, key);
        link.setAttribute(wrmBatchTypeAttribute, scripts.type);
        link.setAttribute(serverSideRenderedAttribute, '');
        el.appendChild(link);
    });
}

const addContextScript = (keys) => addScript({ keys, type: "context" })
const addContextLink = (keys) => addStylesheet({ keys, type: "context" })
const addResourceScript = (keys) => addScript({ keys, type: "resource" })
const addResourceLink = (keys) => addStylesheet({ keys, type: "resource" })

describe("RequestedResourcesTracker", () => {
    afterAll(() => {
        jest.resetAllMocks();
    });

    beforeEach(() => {
        el = document.createElement("div");
    });

    describe("addResource", () => {
        test("should omit unsupported batch type", () => {
            console.error = jest.fn();

            const tracker = new RequestedResourcesTracker(el);

            const expectedErrorMessage = `Unknown batch type 'unsupportedBatchType' discovered when adding a resource 'x'`

            expect(() => tracker.addResource("x", "unsupportedBatchType"))
                .toThrowError(expectedErrorMessage);
            expect(console.error).toHaveBeenNthCalledWith(
                1,
                "[WRM]",
                "[tracker]",
                expectedErrorMessage
            );
        });

        test("should not fail when passed empty string", () => {
            const tracker = new RequestedResourcesTracker(el);

            tracker.addResource("", "resource");

            expect(tracker.getResources()).toEqual([]);
        });

        test('should not add resource with "-"', () => {
            const tracker = new RequestedResourcesTracker(el);

            tracker.addResource("x,-do.not.add","resource");

            expect(tracker.getResources()).toEqual(["wr!x"]);
        });

        test('should add resource when batchType is uppercased', () => {
            const tracker = new RequestedResourcesTracker(el);

            tracker.addResource("x","RESOURCE");

            expect(tracker.getResources()).toEqual(["wr!x"]);
        });
    });

    describe('getResource', () => {
        test('should store unique value for context', () => {
            const tracker = new RequestedResourcesTracker(el);

            addContextScript(['x']);
            addContextLink(['x']);

            expect(tracker.getResources()).toEqual(["wrc!x"]);
        });

        test('should store unique value for resource', () => {
            const tracker = new RequestedResourcesTracker(el);

            addResourceScript(['x']);
            addResourceLink(['x']);

            expect(tracker.getResources()).toEqual(["wr!x"]);
        });

        test('should split comma separated resources', () => {
            const tracker = new RequestedResourcesTracker(el);

            addContextScript(['cx,cy'])
            addContextLink(['ca,cb']);
            addResourceScript(['x,y']);
            addResourceLink(['a,b']);

            expect(tracker.getResources()).toEqual([
                "wrc!cx",
                "wrc!cy",
                "wrc!ca",
                "wrc!cb",
                "wr!x",
                "wr!y",
                "wr!a",
                "wr!b",
            ]);
        });

        test("should parse DOM for SCRIPT and LINK only once (SSR HTML)", () => {
            // It will parse DOM for SCRIPT and LINK only once to collect data from server side rendered HTML.
            // Attributes @see src/main/resources/js/wrm/requested-resources-tracker.js are added on the server side only.
            // Next scripts will be added by addResource function @see wrm/require-hanlder:_processResourceResponse.
            const tracker = new RequestedResourcesTracker(el);

            // Add SCRIPT tag before tracker initialization (first call of getResources())
            addContextScript(['x']);
            expect(tracker.getResources()).toEqual(["wrc!x"]);

            // Add additonal SCRIPT tag (without tracker knowing about this)
            // This checks if DOM scanning for scripts happens only on the first getResources call
            addContextScript(['y']);

            expect(tracker.getResources()).toEqual(["wrc!x"]);
        });

        test("should allow adding resources after initial DOM parsing using addResources API", () => {
            const tracker = new RequestedResourcesTracker(el);

            addContextScript(['x']);
            expect(tracker.getResources()).toEqual(["wrc!x"]);

            tracker.addResource('y', 'context');
            expect(tracker.getResources()).toEqual(["wrc!x", "wrc!y"]);
        });
    });
});
