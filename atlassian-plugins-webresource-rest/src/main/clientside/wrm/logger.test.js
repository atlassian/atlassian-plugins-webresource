import { createLogger, setLoggingLevel } from '@wrm/logger';

describe('Logger', () => {
    afterEach(() => {
        setLoggingLevel('error');
    })

    test.each([
        ['debug'],
        ['info'],
        ['log'],
        ['warn'],
        ['error']
    ])('should contain %s function', (loggingLevel) => {
        const logger = createLogger({context: 'context'});

        expect(logger[loggingLevel]).toBeDefined();
    });

    test.each([
        ['debug'],
        ['info'],
        ['log'],
        ['warn'],
        ['error']
    ])('should use "console" "%s" function to log when given level is enabled', (logFunction) => {
        const logger = createLogger({context: 'context'});
        // Level same as log function name
        setLoggingLevel(logFunction);
        console[logFunction] = jest.fn();

        logger[logFunction]('test');

        expect(console[logFunction]).toHaveBeenNthCalledWith(1, '[WRM]', '[context]', 'test');
    });

    test.each([
        ['debug', 'info'],
        ['info', 'log'],
        ['log', 'warn'],
        ['warn', 'error']
    ])('should NOT use "console" "%s" function to log when higer level (%s) is used', (logFunction, loggingLevel) => {
        const logger = createLogger({context: 'context'});
        setLoggingLevel(loggingLevel);
        console[logFunction] = jest.fn();

        logger[logFunction]('test');

        expect(console[logFunction]).not.toHaveBeenCalled();
    });

    describe('when loggingLevel is undefined it should use level "log" as default', () => {
        test.each([
            ['debug', 0],
            ['info', 0],
            ['log', 1],
            ['warn', 1],
            ['error', 1]
        ])('and should call "console" "%s" function %s times', (logFunction, calledTimes) => {
            const logger = createLogger({context: 'context'});
            setLoggingLevel(undefined);
            console[logFunction] = jest.fn();

            logger[logFunction]('test');

            expect(console[logFunction]).toHaveBeenCalledTimes(calledTimes);
        });
    });

    test('should use "default" context if not specified', () => {
        const logger = createLogger({});
        setLoggingLevel('info');
        console.info = jest.fn();

        logger.info('test');

        expect(console.info).toHaveBeenCalledWith('[WRM]', '[default]', 'test');
    });
});
