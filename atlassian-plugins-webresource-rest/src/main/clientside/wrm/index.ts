import {RequireHandler} from '@wrm/require-handler';
import {setLoggingLevel} from '@wrm/logger';

type Require = ReturnType<typeof makeRequire>;
type SetLoggingLevel = typeof setLoggingLevel;

declare const WRM: typeof window.WRM & {
    require: Require;
    requireLazily: Require;
    setLoggingLevel: SetLoggingLevel;
};
declare const define: Function | undefined;

const requireHandler = new RequireHandler();

const makeRequire =
    (functionName: 'require' | 'requireLazily') => (resources: string | string[], callback: Function) => {
        return requireHandler[functionName](resources, callback);
    };

const wrmRequire = makeRequire('require');
const wrmRequireLazily = makeRequire('requireLazily');

// We expose those in a way that preserves context of RequireHandler instance
WRM.require = wrmRequire;
WRM.requireLazily = wrmRequireLazily;
WRM.setLoggingLevel = setLoggingLevel;

// add also AMD module
if (typeof define === 'function') {
    define('wrm/require', () => wrmRequire);
    define('wrm/require-lazily', () => wrmRequireLazily);
    define('wrm/set-logging-level', () => setLoggingLevel);
    define('wrm', () => WRM);
}
