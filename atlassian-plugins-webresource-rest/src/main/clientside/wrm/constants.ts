import { Phase } from "@wrm/types";

const phases: { [key in Phase]: Phase } = {
    require: "require",
    interaction: "interaction",
};

const resourceTypes = {
    JS: "JS",
    CSS: "CSS",
};

export { phases, resourceTypes };
