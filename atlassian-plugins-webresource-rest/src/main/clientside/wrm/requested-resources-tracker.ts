import {createLogger} from "@wrm/logger";

const logger = createLogger({ context: "tracker" });
const serverSideRenderedAttribute = "data-initially-rendered"
const wrmKeyAttribute = "data-wrm-key";
const wrmBatchTypeAttribute = "data-wrm-batch-type";

type BatchTypeLowercase = 'resource' | 'context';
export type BatchType = BatchTypeLowercase | Uppercase<BatchTypeLowercase> | Capitalize<BatchTypeLowercase>;

const batchTypePrefixMap: Record<BatchTypeLowercase, string> = {
    resource: "wr!",
    context: "wrc!",
};

const keyPartIsAValidInclusion = (key: string) =>
    key.length > 0 &&
    key[0] !== "-"; // Exclusions start with "-"

export class RequestedResourcesTracker {
    private _initialized = false;
    private _resourceKeys = new Set();

    constructor(private _container: Document) {}

    /**
     * @private
     */
    private _getLoaded() {
        return Array.from(this._resourceKeys);
    }

    /**
     * PLUGWEB-399: Inspect the DOM to see if any <script> or <link> tags have already been added, in which case
     * they should not be loaded again.
     *
     * PLUGWEB-402: Only <script> or <link> tags in the DOM with the "data-initially-rendered" attribute are
     * inspected to see which resources have already been loaded. HOWEVER, these attributes are only added if the
     * <script> or <link> tags were rendered on the server-side. These must all have the "data-wrm-key" and
     * "data-wrm-batch-type" attributes for the clientside to know what needs to be excluded.
     *
     * Resources loaded on the clientside do not have this attribute and are instead tracked when the batch download
     * URLs are returned from "/rest/wrm/2.0/resources" and are processed.
     *
     * PLUGWEB-670: An existing assumption is that the script tags on the page DO NOT change neither before nor after
     * this is called. Jira "SPA" transitions _currently_ respect this.
     */
    getResources() {
        if (this._initialized) {
            return this._getLoaded();
        }

        this._container.querySelectorAll(`[${serverSideRenderedAttribute}]`).forEach((el) => {
            if (el.nodeName === "SCRIPT" || el.nodeName === "LINK") {
                this.addResource(
                    el.getAttribute(wrmKeyAttribute) ?? '',
                    (el.getAttribute(wrmBatchTypeAttribute) ?? '') as BatchType
                );
            }
        });

        this._initialized = true;

        return this._getLoaded();
    }

    /**
     * @param {string} wrmKey This is a bad name for what it really is, which is batch key, it can be a single resource, single
     * context, or a batch of them, potentially with exclusions of what was previously loaded.
     * @param {BatchType} batchType
     */
    addResource(wrmKey: string, batchType: BatchType) {
        const lowercasedBatchType = batchType.toLowerCase() as BatchTypeLowercase;

        if (!Object.keys(batchTypePrefixMap).includes(lowercasedBatchType)) {
            const unknownBatchTypeErrorMsg = `Unknown batch type '${batchType}' discovered when adding a resource '${wrmKey}'`;
            logger.error(unknownBatchTypeErrorMsg);
            throw new Error(unknownBatchTypeErrorMsg);
        }

        const prefix = batchTypePrefixMap[lowercasedBatchType];

        wrmKey
            .split(",") // split the batch
            // TODO: Fix PLUGWEB-691 clientside inclusions cannot overcome serverside exclusions
            .filter(keyPartIsAValidInclusion)
            .forEach((key) => {
                this._resourceKeys.add(`${prefix}${key}`);
            });
    }
}
