package com.atlassian.webresource.plugin.rest.one.zero.model;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
public class ResolveResourcesJson {
    @Nonnull
    private final List<String> resources;

    @Nonnull
    private final List<String> contexts;

    @Nonnull
    private final List<String> excludeResources;

    @Nonnull
    private final List<String> excludeContexts;

    public ResolveResourcesJson() {
        resources = Collections.emptyList();
        contexts = Collections.emptyList();
        excludeResources = Collections.emptyList();
        excludeContexts = Collections.emptyList();
    }

    @JsonCreator
    public ResolveResourcesJson(
            @JsonProperty("r") @Nonnull final List<String> resources,
            @JsonProperty("c") @Nonnull final List<String> contexts,
            @JsonProperty("xr") @Nonnull final List<String> excludeResources,
            @JsonProperty("xc") @Nonnull final List<String> excludeContexts) {
        this.resources = resources;
        this.contexts = contexts;
        this.excludeResources = excludeResources;
        this.excludeContexts = excludeContexts;
    }

    @Nonnull
    @JsonProperty("r")
    public List<String> getResources() {
        return resources;
    }

    @Nonnull
    @JsonProperty("c")
    public List<String> getContexts() {
        return contexts;
    }

    @Nonnull
    @JsonProperty("xr")
    public List<String> getExcludeResources() {
        return excludeResources;
    }

    @Nonnull
    @JsonProperty("xc")
    public List<String> getExcludeContexts() {
        return excludeContexts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ResolveResourcesJson that = (ResolveResourcesJson) o;
        return Objects.equals(resources, that.resources)
                && Objects.equals(contexts, that.contexts)
                && Objects.equals(excludeResources, that.excludeResources)
                && Objects.equals(excludeContexts, that.excludeContexts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resources, contexts, excludeResources, excludeContexts);
    }
}
