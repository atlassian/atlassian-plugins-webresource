package com.atlassian.webresource.plugin.rest.two.zero.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.plugin.webresource.models.Requestable;
import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.plugin.async.model.OutputShape;
import com.atlassian.webresource.plugin.async.model.ResourceTypeAndUrl;
import com.atlassian.webresource.plugin.rest.two.zero.PhasesAwareResources;
import com.atlassian.webresource.plugin.rest.two.zero.model.ResourceType;
import com.atlassian.webresource.plugin.rest.two.zero.model.UrlFetchableResourceJson;
import com.atlassian.webresource.plugin.rest.two.zero.model.UrlFetchableResourcesWithDataJson;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE;

/**
 * A util class to help the {@link PhasesAwareResources} REST API transform data from REST models to WRM canon models
 */
public class PhasesAwareResourcesModelMapperUtil {

    @VisibleForTesting
    public static final String WEB_RESOURCE_CONTEXT_PREFIX = "wrc!";

    private static final String WEB_RESOURCE_PREFIX = "wr!";

    private PhasesAwareResourcesModelMapperUtil() throws Exception {
        throw new Exception("Static util class is not meant to be instantiated");
    }

    public static <T extends Requestable> Map<ResourcePhase, Set<T>> byPhase(
            final Set<T> require, final Set<T> requireForInteraction) {
        final Map<ResourcePhase, Set<T>> requestablesPerPhase = new HashMap<>();
        requestablesPerPhase.put(REQUIRE, require);
        requestablesPerPhase.put(INTERACTION, requireForInteraction);
        return requestablesPerPhase;
    }

    public static UrlFetchableResourcesWithDataJson transformOutputShapeToUrlFetchableResourcesWithDataJson(
            @Nonnull final OutputShape outputShape) {
        return new UrlFetchableResourcesWithDataJson(
                outputShape.getResources().stream()
                        .map(PhasesAwareResourcesModelMapperUtil::transformResourceTypeAndUrlToUrlFetchableResourceJson)
                        .collect(toList()),
                outputShape.getUnparsedData(),
                outputShape.getUnparsedErrors());
    }

    private static UrlFetchableResourceJson transformResourceTypeAndUrlToUrlFetchableResourceJson(
            @Nonnull final ResourceTypeAndUrl resourceTypeAndUrl) {
        return new UrlFetchableResourceJson(
                resourceTypeAndUrl.getBatchType(),
                resourceTypeAndUrl.getKey(),
                ResourceType.valueOf(resourceTypeAndUrl.getResourceType().name().toUpperCase()),
                resourceTypeAndUrl.getUrl());
    }

    public static <T extends Requestable> Set<T> transformStringsToRequestable(
            final Collection<String> requestableStrings,
            final String identifyingPrefix,
            final Function<String, T> constructor) {
        return requestableStrings.stream()
                .filter(requestableString -> requestableString.startsWith(identifyingPrefix))
                .map(requestableString -> requestableString.replaceFirst(identifyingPrefix, ""))
                .filter(StringUtils::isNotBlank)
                .map(constructor)
                .collect(toCollection(LinkedHashSet::new));
    }

    public static Set<WebResourceContextKey> transformStringsToFallbackRequestable(
            final Collection<String> requestableStrings) {
        return requestableStrings.stream()
                .filter(requestableString -> !requestableString.startsWith(WEB_RESOURCE_PREFIX)
                        && !requestableString.startsWith(WEB_RESOURCE_CONTEXT_PREFIX))
                .filter(StringUtils::isNotBlank)
                .map(WebResourceContextKey::new)
                .collect(toCollection(LinkedHashSet::new));
    }

    public static Set<WebResourceKey> transformStringsToWebResourceKeysSet(
            final Collection<String> requestableStrings) {
        return transformStringsToRequestable(requestableStrings, WEB_RESOURCE_PREFIX, WebResourceKey::new);
    }

    public static Set<WebResourceContextKey> transformStringsToWebResourceContextKeysSet(
            final Collection<String> requestableStrings) {
        final Set<WebResourceContextKey> setOfWebResourceContextKeys = transformStringsToRequestable(
                requestableStrings, WEB_RESOURCE_CONTEXT_PREFIX, WebResourceContextKey::new);
        setOfWebResourceContextKeys.addAll(transformStringsToFallbackRequestable(requestableStrings));
        return setOfWebResourceContextKeys;
    }
}
