package com.atlassian.webresource.plugin.rest.one.zero.model;

import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;

import static com.atlassian.webresource.api.UrlMode.RELATIVE;
import static com.atlassian.webresource.plugin.rest.one.zero.model.Resource.ResourceType.CSS;
import static com.atlassian.webresource.plugin.rest.one.zero.model.Resource.ResourceType.JAVASCRIPT;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
public class Resource {
    private final String url;
    private final ResourceType resourceType;
    private final String media;
    private final String key;
    private final String batchType;

    public Resource(@Nonnull final PluginJsResource resource) {
        this(resource.getStaticUrl(RELATIVE), JAVASCRIPT, null, resource.getKey(), resource.getBatchType());
    }

    public Resource(@Nonnull final PluginCssResource cssResource) {
        this(
                cssResource.getStaticUrl(RELATIVE),
                CSS,
                cssResource.getParams().media(),
                cssResource.getKey(),
                cssResource.getBatchType());
    }

    @Deprecated
    @JsonCreator
    public Resource(
            @Nonnull @JsonProperty("url") final String url,
            @Nonnull @JsonProperty("resourceType") final ResourceType resourceType,
            @Nullable @JsonProperty("media") final String media,
            @Nonnull @JsonProperty("key") final String key,
            @Nonnull @JsonProperty("batchType") final BatchType batchType) {
        this.url = url;
        this.resourceType = resourceType;
        this.media = media;
        this.key = key;
        this.batchType = batchType.name().toUpperCase();
    }

    public enum ResourceType {
        JAVASCRIPT,
        CSS
    }

    @JsonProperty
    @Nonnull
    public BatchType getBatchType() {
        return BatchType.valueOf(batchType);
    }

    @JsonProperty
    @Nonnull
    public String getKey() {
        return key;
    }

    @JsonProperty
    @Nonnull
    public ResourceType getResourceType() {
        return resourceType;
    }

    @JsonProperty
    @Nonnull
    public String getUrl() {
        return url;
    }

    @JsonProperty
    public String getMedia() {
        return media;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof Resource) {
            final Resource otherResource = (Resource) other;
            return Objects.equals(url, otherResource.url)
                    && Objects.equals(resourceType, otherResource.resourceType)
                    && Objects.equals(media, otherResource.media)
                    && Objects.equals(key, otherResource.key)
                    && Objects.equals(batchType, otherResource.batchType);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, resourceType, media, key, batchType);
    }

    @Override
    public String toString() {
        return "Resource{" + "key='"
                + key + '\'' + ", batchType='"
                + batchType + '\'' + ", resourceType="
                + resourceType + ", media='"
                + media + '\'' + ", url='"
                + url + '\'' + '}';
    }
}
