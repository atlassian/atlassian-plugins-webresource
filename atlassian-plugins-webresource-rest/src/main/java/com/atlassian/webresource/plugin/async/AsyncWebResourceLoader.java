package com.atlassian.webresource.plugin.async;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.plugin.async.model.ResourcesAndData;

/**
 * Resolves web resources into a format suitable for delivery via REST
 *
 * @since v3.0
 */
public interface AsyncWebResourceLoader {

    ResourcesAndData resolve(
            Map<ResourcePhase, Set<WebResourceKey>> webResourcesByPhase,
            Map<ResourcePhase, Set<WebResourceContextKey>> contextsByPhase,
            Set<WebResourceKey> excludeResources,
            Set<WebResourceContextKey> excludeContexts)
            throws IOException;
}
