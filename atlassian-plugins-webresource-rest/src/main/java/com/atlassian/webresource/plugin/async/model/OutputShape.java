package com.atlassian.webresource.plugin.async.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class OutputShape {
    private final Collection<ResourceTypeAndUrl> resources;
    private final Map<String, String> unparsedData;
    private final Map<String, String> unparsedErrors;

    public OutputShape(
            @Nonnull final Collection<? extends ResourceTypeAndUrl> resources,
            @Nonnull final Map<String, String> unparsedData,
            @Nonnull final Map<String, String> unparsedErrors) {
        requireNonNull(resources, "The resources and urls are mandatory.");
        requireNonNull(unparsedData, "The unparsed data is mandatory.");
        requireNonNull(unparsedErrors, "The unparse error is mandatory.");
        this.resources = new ArrayList<>(resources);
        this.unparsedData = new HashMap<>(unparsedData);
        this.unparsedErrors = new HashMap<>(unparsedErrors);
    }

    @Nonnull
    public Collection<ResourceTypeAndUrl> getResources() {
        return new ArrayList<>(resources);
    }

    @Nonnull
    public Map<String, String> getUnparsedData() {
        return new HashMap<>(unparsedData);
    }

    @Nonnull
    public Map<String, String> getUnparsedErrors() {
        return new HashMap<>(unparsedErrors);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof OutputShape) {
            final OutputShape otherOutputShape = (OutputShape) other;
            return resources.equals(otherOutputShape.resources)
                    && unparsedData.equals(otherOutputShape.unparsedData)
                    && unparsedErrors.equals(otherOutputShape.unparsedErrors);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(resources, unparsedData, unparsedErrors);
    }

    @Override
    public String toString() {
        return "OutputShape{" + "resources="
                + resources.stream().map(ResourceTypeAndUrl::getKey).collect(Collectors.joining("|"))
                + ", unparsedData="
                + unparsedData.keySet() + ", unparsedErrors="
                + unparsedErrors.keySet() + '}';
    }
}
