package com.atlassian.webresource.plugin.rest.two.zero.graph;

import com.atlassian.plugin.webresource.graph.DependencyGraph;
import com.atlassian.plugin.webresource.graph.RequestableKeyValidator;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.models.Requestable;
import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;

public class RequestableGraphServiceImpl implements RequestableGraphService {

    @Override
    public boolean hasById(String requestableId) {
        Requestable key = create(requestableId);
        return getGraph().hasDependency(key);
    }

    @Override
    public RequestableGraph getConsumerGraphById(String requestableId) {
        Requestable key = create(requestableId);
        DependencyGraph<Requestable> ancestorGraph = getGraph().findDependantsSubGraphByKey(key);
        return new RequestableGraph(ancestorGraph);
    }

    @Override
    public RequestableGraph getDependencyGraphById(String requestableId) {
        Requestable key = create(requestableId);
        DependencyGraph<Requestable> dependencyGraph = getGraph().findDependencySubGraphByRequestableKey(key);
        return new RequestableGraph(dependencyGraph);
    }

    private Requestable create(String key) {
        if (RequestableKeyValidator.isWebResourceContext(key)) {
            return new WebResourceContextKey(key);
        }
        return new WebResourceKey(key);
    }

    private DependencyGraph<Requestable> getGraph() {
        return Config.getDependencyGraph();
    }
}
