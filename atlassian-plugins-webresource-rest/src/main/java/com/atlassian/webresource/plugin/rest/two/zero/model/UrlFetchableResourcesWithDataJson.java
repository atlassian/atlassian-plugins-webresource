package com.atlassian.webresource.plugin.rest.two.zero.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

@JsonPropertyOrder(alphabetic = true)
public class UrlFetchableResourcesWithDataJson {
    private final Collection<UrlFetchableResourceJson> resources;
    private final Map<String, String> unparsedData;
    private final Map<String, String> unparsedErrors;

    @JsonCreator
    public UrlFetchableResourcesWithDataJson(
            @Nonnull @JsonProperty("resources") final Collection<? extends UrlFetchableResourceJson> resources,
            @Nonnull @JsonProperty("unparsedData") final Map<String, String> unparsedData,
            @Nonnull @JsonProperty("unparsedErrors") final Map<String, String> unparsedErrors) {
        requireNonNull(resources, "The resources and urls are mandatory.");
        requireNonNull(unparsedData, "The unparsed data is mandatory.");
        requireNonNull(unparsedErrors, "The unparsed error is mandatory.");
        this.resources = new ArrayList<>(resources);
        this.unparsedData = new HashMap<>(unparsedData);
        this.unparsedErrors = new HashMap<>(unparsedErrors);
    }

    @Nonnull
    @JsonProperty("resources")
    public Collection<UrlFetchableResourceJson> getResources() {
        return resources;
    }

    @Nonnull
    @JsonProperty("unparsedData")
    public Map<String, String> getUnparsedData() {
        return unparsedData;
    }

    @Nonnull
    @JsonProperty("unparsedErrors")
    public Map<String, String> getUnparsedErrors() {
        return unparsedErrors;
    }

    @Override
    public boolean equals(final Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject instanceof UrlFetchableResourcesWithDataJson) {
            final UrlFetchableResourcesWithDataJson other = (UrlFetchableResourcesWithDataJson) otherObject;
            return resources.equals(other.resources)
                    && unparsedData.equals(other.unparsedData)
                    && unparsedErrors.equals(other.unparsedErrors);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(resources, unparsedData, unparsedErrors);
    }

    @Override
    public String toString() {
        return reflectionToString(this, JSON_STYLE);
    }
}
