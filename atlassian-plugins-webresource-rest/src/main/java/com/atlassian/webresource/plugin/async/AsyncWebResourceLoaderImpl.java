package com.atlassian.webresource.plugin.async;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.webresource.models.Requestable;
import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.plugin.async.model.ResourcesAndData;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;

import static com.atlassian.plugin.webresource.util.WebResourceKeyHelper.isWebResourceKey;

/**
 * Implementation of AsyncWebResourceLoader
 *
 * @since v3.0
 */
public class AsyncWebResourceLoaderImpl implements AsyncWebResourceLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncWebResourceLoaderImpl.class);

    private final WebResourceAssemblerFactory webResourceAssemblerFactory;

    public AsyncWebResourceLoaderImpl(@Nonnull final WebResourceAssemblerFactory webResourceAssemblerFactory) {
        this.webResourceAssemblerFactory = requireNonNull(webResourceAssemblerFactory);
    }

    @Nonnull
    @Override
    public ResourcesAndData resolve(
            @Nonnull final Map<ResourcePhase, Set<WebResourceKey>> webResourcesByPhase,
            @Nonnull final Map<ResourcePhase, Set<WebResourceContextKey>> contextsByPhase,
            @Nonnull final Set<WebResourceKey> excludeResources,
            @Nonnull final Set<WebResourceContextKey> excludeContexts)
            throws IOException {
        final WebResourceSet webResourceSet =
                resolveWebResourceSet(webResourcesByPhase, contextsByPhase, excludeResources, excludeContexts);
        final ResourcesAndData resourcesAndData = new ResourcesAndData(webResourceSet.getResources());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("AsyncWebResourceLoaderImpl.resolve Returning async resource response: {}", resourcesAndData);
        }
        return resourcesAndData;
    }

    private WebResourceSet resolveWebResourceSet(
            final Map<ResourcePhase, Set<WebResourceKey>> webResourcesByPhase,
            final Map<ResourcePhase, Set<WebResourceContextKey>> contextsByPhase,
            final Set<WebResourceKey> excludeResources,
            final Set<WebResourceContextKey> excludeContexts) {
        if (LOGGER.isDebugEnabled()) {
            webResourcesByPhase.forEach((resourcePhase, webResourceKeys) ->
                    LOGGER.debug("Requiring webresources with phase {} : {}", resourcePhase, webResourceKeys));
            contextsByPhase.forEach((resourcePhase, webResourceContextKeys) ->
                    LOGGER.debug("Requiring contexts with phase {} : {}", resourcePhase, webResourceContextKeys));
            LOGGER.debug("Excluding webresources {}", excludeResources);
            LOGGER.debug("Excluding contexts {}", excludeContexts);
        }

        // Async requests should not include the super batch by default - unless explicitly required
        final WebResourceAssembler assembler = webResourceAssemblerFactory
                .create()
                .includeSuperbatchResources(false)
                .build();
        assembler
                .resources()
                .exclude(
                        excludeResources.stream().map(Requestable::toLooseType).collect(toSet()),
                        excludeContexts.stream().map(Requestable::toLooseType).collect(toSet()));

        webResourcesByPhase.forEach((resourcePhase, webResourceKeys) -> {
            for (final WebResourceKey webResource : webResourceKeys) {
                if (isWebResourceKey(webResource.toLooseType())) {
                    assembler.resources().requireWebResource(resourcePhase, webResource.toLooseType());
                }
            }
        });

        contextsByPhase.forEach(
                (resourcePhase, webResourceContextKeys) -> webResourceContextKeys.forEach(webResourceContextKey ->
                        assembler.resources().requireContext(resourcePhase, webResourceContextKey.toLooseType())));

        return assembler.assembled().drainIncludedResources();
    }
}
