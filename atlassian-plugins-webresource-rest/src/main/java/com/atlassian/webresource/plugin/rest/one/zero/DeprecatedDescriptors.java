package com.atlassian.webresource.plugin.rest.one.zero;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.collect.Lists;

import com.atlassian.plugins.rest.api.security.annotation.SystemAdminOnly;

import static java.util.Collections.emptyList;

/**
 * REST endpoint for retrieving list of deprecated transform1 and condition1's.
 * Provided to allow
 *
 * @since v3.1.0
 */
@SystemAdminOnly
@Path("deprecatedDescriptors")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class DeprecatedDescriptors {

    @GET
    public Result getInfo() {
        return new Result(emptyList(), emptyList(), emptyList());
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPropertyOrder(alphabetic = true)
    public static class WebresourceDescriptor {
        @JsonProperty
        public final String moduleKey;

        @JsonProperty
        public List<String> condition1keys;

        @JsonProperty
        public List<String> transform1keys;

        @JsonProperty
        public List<String> context;

        @JsonProperty
        public Boolean superbatch;

        public WebresourceDescriptor(String moduleKey) {
            this.moduleKey = moduleKey;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPropertyOrder(alphabetic = true)
    private static class Result {
        @JsonProperty
        public final List<String> condition1s;

        @JsonProperty
        public final List<String> transform1s;

        @JsonProperty
        public final Iterable<WebresourceDescriptor> webresources;

        @JsonCreator
        public Result(
                Iterable<String> condition1s,
                Iterable<String> transform1s,
                Iterable<WebresourceDescriptor> webresources) {
            this.condition1s = Lists.newArrayList(condition1s);
            this.transform1s = Lists.newArrayList(transform1s);
            this.webresources = webresources;
        }
    }
}
