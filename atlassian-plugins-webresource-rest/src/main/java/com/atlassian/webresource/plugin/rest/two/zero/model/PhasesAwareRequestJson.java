package com.atlassian.webresource.plugin.rest.two.zero.model;

import java.util.Collection;
import java.util.Objects;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import com.atlassian.annotations.VisibleForTesting;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
public class PhasesAwareRequestJson {
    private static final String RESOURCES_CANNOT_BE_NULL_MSG = " resource cannot be null";

    @VisibleForTesting
    public static final String REQUIRE_RESOURCES_NULL_MSG = "'require' phase" + RESOURCES_CANNOT_BE_NULL_MSG;

    @VisibleForTesting
    public static final String INTERACTION_RESOURCES_NULL_MSG = "'interaction' phase" + RESOURCES_CANNOT_BE_NULL_MSG;

    @VisibleForTesting
    public static final String EXCLUDE_RESOURCES_NULL_MSG = "'exclude'" + RESOURCES_CANNOT_BE_NULL_MSG;

    @Nonnull
    @JsonProperty("require")
    private final Collection<String> require;

    @Nonnull
    @JsonProperty("interaction")
    private final Collection<String> requireForInteraction;

    @Nonnull
    @JsonProperty("exclude")
    private final Collection<String> exclude;

    public PhasesAwareRequestJson() {
        this(emptyList(), emptyList(), emptyList());
    }

    @JsonCreator
    public PhasesAwareRequestJson(
            @JsonProperty("require") @Nonnull final Collection<String> require,
            @JsonProperty("interaction") @Nonnull final Collection<String> requireForInteraction,
            @JsonProperty("exclude") @Nonnull final Collection<String> exclude) {
        this.require = requireNonNull(require, REQUIRE_RESOURCES_NULL_MSG);
        this.requireForInteraction = requireNonNull(requireForInteraction, INTERACTION_RESOURCES_NULL_MSG);
        this.exclude = requireNonNull(exclude, EXCLUDE_RESOURCES_NULL_MSG);
    }

    @Nonnull
    public Collection<String> getRequire() {
        return require;
    }

    @Nonnull
    public Collection<String> getRequireForInteraction() {
        return requireForInteraction;
    }

    @Nonnull
    public Collection<String> getExclude() {
        return exclude;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof PhasesAwareRequestJson) {
            final PhasesAwareRequestJson otherPhasesAwareRequestJson = (PhasesAwareRequestJson) other;
            return Objects.equals(require, otherPhasesAwareRequestJson.require)
                    && Objects.equals(requireForInteraction, otherPhasesAwareRequestJson.requireForInteraction)
                    && Objects.equals(exclude, otherPhasesAwareRequestJson.exclude);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(require, requireForInteraction, exclude);
    }
}
