package com.atlassian.webresource.plugin.rest.two.zero.model;

import java.util.Objects;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

@JsonPropertyOrder(alphabetic = true)
public final class UrlFetchableResourceJson {
    private final BatchType batchType;
    private final String key;
    private final ResourceType resourceType;
    private final String url;

    @JsonCreator
    public UrlFetchableResourceJson(
            @Nonnull @JsonProperty("batchType") final BatchType batchType,
            @Nonnull @JsonProperty("key") final String key,
            @Nonnull @JsonProperty("resourceType") final ResourceType resourceType,
            @Nonnull @JsonProperty("url") final String url) {
        this.batchType = requireNonNull(batchType, "The batch type is mandatory.");
        this.key = requireNonNull(key, "The resource key is mandatory.");
        this.resourceType = requireNonNull(resourceType, "The resource type is mandatory.");
        this.url = requireNonNull(url, "The resource url is mandatory.");
    }

    @JsonProperty
    @Nonnull
    public BatchType getBatchType() {
        return batchType;
    }

    @JsonProperty
    @Nonnull
    public String getKey() {
        return key;
    }

    @JsonProperty
    @Nonnull
    public ResourceType getResourceType() {
        return resourceType;
    }

    @JsonProperty
    @Nonnull
    public String getUrl() {
        return url;
    }

    @Override
    public boolean equals(final Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject instanceof UrlFetchableResourceJson) {
            final UrlFetchableResourceJson other = (UrlFetchableResourceJson) otherObject;
            return batchType == other.batchType
                    && key.equals(other.key)
                    && resourceType == other.resourceType
                    && url.equals(other.url);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(batchType, key, resourceType, url);
    }

    @Override
    public String toString() {
        return reflectionToString(this, JSON_STYLE);
    }
}
