package com.atlassian.webresource.plugin.rest.two.zero;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.plugin.async.AsyncWebResourceLoader;
import com.atlassian.webresource.plugin.async.model.ResourcesAndData;
import com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareRequestJson;
import com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareResourcesResponseJson;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import static com.atlassian.webresource.plugin.rest.two.zero.util.PhasesAwareResourcesModelMapperUtil.byPhase;
import static com.atlassian.webresource.plugin.rest.two.zero.util.PhasesAwareResourcesModelMapperUtil.transformOutputShapeToUrlFetchableResourcesWithDataJson;
import static com.atlassian.webresource.plugin.rest.two.zero.util.PhasesAwareResourcesModelMapperUtil.transformStringsToWebResourceContextKeysSet;
import static com.atlassian.webresource.plugin.rest.two.zero.util.PhasesAwareResourcesModelMapperUtil.transformStringsToWebResourceKeysSet;

/**
 * REST endpoint for retrieving resolved JS, CSS (and more) resources with resource phases
 *
 * @since v5.0
 */
@UnrestrictedAccess
@OpenAPIDefinition(
        info =
                @Info(
                        title = "Web Resource Manager",
                        version = "2.0",
                        description = "REST API for retrieving web resources with phases"))
@Path("resources")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class PhasesAwareResources {
    private final AsyncWebResourceLoader asyncWebResourceLoader;

    @Inject
    public PhasesAwareResources(@Nonnull final AsyncWebResourceLoader asyncWebResourceLoader) {
        this.asyncWebResourceLoader = asyncWebResourceLoader;
    }

    /**
     * Calculate the missing JS, CSS and extra data required by the client, taking into consider exclusions (which
     * are what the client already has).
     *
     * @param request The resources and contexts to include and exclude.
     */
    @POST
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    @Operation(
            summary = "Retrieve resolved resources",
            tags = {"resources"})
    @ApiResponse(
            responseCode = "200",
            description = "Successful operation",
            content = @Content(schema = @Schema(implementation = PhasesAwareResourcesResponseJson.class)))
    public PhasesAwareResourcesResponseJson post(@Parameter(required = true) PhasesAwareRequestJson request)
            throws IOException {

        final Map<ResourcePhase, Set<WebResourceKey>> phasesAwareIncludedWebResources = byPhase(
                transformStringsToWebResourceKeysSet(request.getRequire()),
                transformStringsToWebResourceKeysSet(request.getRequireForInteraction()));

        final Map<ResourcePhase, Set<WebResourceContextKey>> phasesAwareIncludedWebResourceContexts = byPhase(
                transformStringsToWebResourceContextKeysSet(request.getRequire()),
                transformStringsToWebResourceContextKeysSet(request.getRequireForInteraction()));

        final Set<WebResourceKey> excludeWebResources = transformStringsToWebResourceKeysSet(request.getExclude());
        final Set<WebResourceContextKey> excludeWebResourceContexts =
                transformStringsToWebResourceContextKeysSet(request.getExclude());

        final ResourcesAndData resolvedResourcesAndData = asyncWebResourceLoader.resolve(
                phasesAwareIncludedWebResources,
                phasesAwareIncludedWebResourceContexts,
                excludeWebResources,
                excludeWebResourceContexts);

        return new PhasesAwareResourcesResponseJson(
                transformOutputShapeToUrlFetchableResourcesWithDataJson(resolvedResourcesAndData.getInteraction()),
                transformOutputShapeToUrlFetchableResourcesWithDataJson(resolvedResourcesAndData.getRequire()));
    }
}
