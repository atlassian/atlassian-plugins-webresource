package com.atlassian.webresource.plugin.async.model;

import java.util.Objects;
import javax.annotation.Nonnull;

import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;

import static com.atlassian.webresource.api.UrlMode.RELATIVE;

public class ResourceTypeAndUrl {
    private final PluginUrlResource<?> pluginUrlResource;

    public ResourceTypeAndUrl(@Nonnull final PluginUrlResource<?> pluginUrlResource) {
        this.pluginUrlResource = pluginUrlResource;
    }

    @Nonnull
    public BatchType getBatchType() {
        return pluginUrlResource.getBatchType();
    }

    @Nonnull
    public String getKey() {
        return pluginUrlResource.getKey();
    }

    @Nonnull
    public PluginUrlResource<?> getPluginUrlResource() {
        return pluginUrlResource;
    }

    @Nonnull
    public ResourceType getResourceType() {
        return ResourceType.getResourceType(pluginUrlResource);
    }

    @Nonnull
    public String getUrl() {
        return pluginUrlResource.getStaticUrl(RELATIVE);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof ResourceTypeAndUrl) {
            final ResourceTypeAndUrl otherResourceTypeAndUrl = (ResourceTypeAndUrl) other;
            return getBatchType() == otherResourceTypeAndUrl.getBatchType()
                    && getKey().equals(otherResourceTypeAndUrl.getKey())
                    && getResourceType() == otherResourceTypeAndUrl.getResourceType()
                    && getUrl().equals(otherResourceTypeAndUrl.getUrl());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBatchType(), getKey(), getResourceType(), getUrl());
    }

    @Override
    public String toString() {
        return "ResourceTypeAndUrl{" + "batchType="
                + getBatchType() + ",key="
                + getKey() + ",resourceType="
                + getResourceType() + ",url="
                + getUrl() + '}';
    }
}
