package com.atlassian.webresource.plugin.rest.two.zero.graph;

import java.util.Collection;

import com.atlassian.plugin.webresource.graph.DependencyEdge;
import com.atlassian.plugin.webresource.graph.DependencyGraph;
import com.atlassian.webresource.plugin.rest.two.zero.model.ResourcePhase;

import static java.util.stream.Collectors.toList;

public class RequestableGraph {
    private final Collection<RequestableEdge> edges;

    public RequestableGraph(DependencyGraph<com.atlassian.plugin.webresource.models.Requestable> graph) {
        this.edges = graph.getEdges().stream().map(RequestableGraph::toDTO).collect(toList());
    }

    public Iterable<RequestableEdge> getEdges() {
        return edges;
    }

    private static RequestableEdge toDTO(final DependencyEdge edge) {
        ResourcePhase phaseDTO = null;
        return new RequestableEdge(
                toDTO((com.atlassian.plugin.webresource.models.Requestable) edge.getSource()),
                toDTO((com.atlassian.plugin.webresource.models.Requestable) edge.getTarget()),
                phaseDTO);
    }

    private static Requestable toDTO(final com.atlassian.plugin.webresource.models.Requestable node) {
        return new Requestable(node.toLooseType());
    }
}
