package com.atlassian.webresource.plugin.rest.two.zero.exception;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.exc.ValueInstantiationException;
import com.google.gson.JsonObject;

@Priority(Priorities.USER)
@Provider
public class ValueInstantiationExceptionMapper extends Throwable
        implements ExceptionMapper<ValueInstantiationException> {
    @Override
    public Response toResponse(ValueInstantiationException e) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("errorMessage", e.getMessage());
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(jsonObject.toString())
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}
