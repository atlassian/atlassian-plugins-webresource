package com.atlassian.webresource.plugin.rest.one.zero.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.plugin.async.model.ResourceTypeAndUrl;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Stream.of;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
public class ResourcesAndData {
    @Nonnull
    public final List<Resource> resources;

    @Nonnull
    public final Map<String, String> unparsedData;

    @Nonnull
    public final Map<String, String> unparsedErrors;

    public ResourcesAndData(
            @Nonnull final com.atlassian.webresource.plugin.async.model.ResourcesAndData resourcesAndData) {
        final ResourcesAndData convertedResourcesAndData = convert(resourcesAndData);
        resources = convertedResourcesAndData.resources;
        unparsedData = convertedResourcesAndData.unparsedData;
        unparsedErrors = convertedResourcesAndData.unparsedErrors;
    }

    @JsonCreator
    public ResourcesAndData(
            @Nonnull @JsonProperty("resources") final List<Resource> resources,
            @Nonnull @JsonProperty("unparsedData") final Map<String, String> unparsedData,
            @Nonnull @JsonProperty("unparsedErrors") final Map<String, String> unparsedErrors) {
        this.resources = resources;
        this.unparsedData = new HashMap<>(unparsedData);
        this.unparsedErrors = new HashMap<>(unparsedErrors);
    }

    @Nonnull
    @JsonProperty("resources")
    public List<Resource> getResources() {
        return resources;
    }

    @Nonnull
    @JsonProperty("unparsedData")
    public Map<String, String> getUnparsedData() {
        return unparsedData;
    }

    @Nonnull
    @JsonProperty("unparsedErrors")
    public Map<String, String> getUnparsedErrors() {
        return unparsedErrors;
    }

    private static ResourcesAndData convert(
            final com.atlassian.webresource.plugin.async.model.ResourcesAndData resourcesAndData) {

        final Iterable<Resource> resources = of(
                        resourcesAndData.getRequireResources(), resourcesAndData.getResourcesForInteration())
                .flatMap(Collection::stream)
                .map(ResourceTypeAndUrl::getPluginUrlResource)
                .filter(resource -> resource instanceof PluginJsResource || resource instanceof PluginCssResource)
                .map(webResource -> {
                    if (webResource instanceof PluginJsResource) {
                        return new Resource((PluginJsResource) webResource);
                    }
                    return new Resource((PluginCssResource) webResource);
                })
                .collect(toCollection(ArrayList::new));

        final Map<String, String> unparsedData = of(
                        resourcesAndData.getRequiredResourcesUnparsedData(),
                        resourcesAndData.getResourcesForInterationUnparsedData())
                .flatMap(data -> data.entrySet().stream())
                .collect(toMap(Entry::getKey, Entry::getValue));

        final Map<String, String> unparsedErrors = of(
                        resourcesAndData.getRequireResourcesUnparsedErrors(),
                        resourcesAndData.getResourcesForInterationUnparsedErrors())
                .flatMap(data -> data.entrySet().stream())
                .collect(toMap(Entry::getKey, Entry::getValue));
        return new ResourcesAndData(
                StreamSupport.stream(resources.spliterator(), false).collect(Collectors.toList()),
                unparsedData,
                unparsedErrors);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof ResourcesAndData) {
            final ResourcesAndData otherResourcesAndData = (ResourcesAndData) other;
            return Objects.equals(resources, otherResourcesAndData.resources)
                    && Objects.equals(unparsedData, otherResourcesAndData.unparsedData)
                    && Objects.equals(unparsedErrors, otherResourcesAndData.unparsedErrors);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(resources, unparsedData, unparsedErrors);
    }

    @Override
    public String toString() {
        return "ResourcesAndData{" + "resources="
                + resources + ", unparsedData="
                + unparsedData + ", unparsedErrors="
                + unparsedErrors + '}';
    }
}
