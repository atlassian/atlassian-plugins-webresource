package com.atlassian.webresource.plugin.rest.two.zero.graph;

/**
 * Data Transfer Object (DTO) to adapt a {@code Requestable} item for transfer via REST.
 */
public class Requestable {
    private final String key;

    public Requestable(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
