package com.atlassian.webresource.plugin.rest.two.zero.model;

import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.atlassian.webresource.plugin.rest.two.zero.graph.Requestable;

/**
 * JSON shape for sending {@code Requestable} item relationships to the client via REST.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public final class RequestableEdgeJson {
    private final String source;
    private final String target;
    private final String phase;

    public RequestableEdgeJson(Requestable source, Requestable target, ResourcePhase phase) {
        this(source.getKey(), target.getKey(), phase != null ? phase.getName() : null);
    }

    @JsonCreator
    public RequestableEdgeJson(
            @Nonnull @JsonProperty("source") final String source,
            @Nonnull @JsonProperty("target") final String target,
            @Nullable @JsonProperty("phase") final String phase) {
        this.source = source;
        this.target = target;
        this.phase = phase;
    }

    @JsonProperty("source")
    @Nonnull
    public String getSource() {
        return source;
    }

    @JsonProperty("target")
    @Nonnull
    public String getTarget() {
        return target;
    }

    @JsonProperty("phase")
    @Nonnull
    public String getPhase() {
        return StringUtils.isNotBlank(phase) ? phase : "unknown";
    }

    @Override
    public String toString() {
        return String.format("RequestableEdge(%s): %s -> %s", phase, source, target);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestableEdgeJson that = (RequestableEdgeJson) o;
        return Objects.equals(source, that.source)
                && Objects.equals(target, that.target)
                && Objects.equals(phase, that.phase);
    }

    @Override
    public int hashCode() {
        return Objects.hash(source, target, phase);
    }
}
