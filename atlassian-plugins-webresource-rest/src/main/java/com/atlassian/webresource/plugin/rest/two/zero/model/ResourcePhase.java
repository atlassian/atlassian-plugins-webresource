package com.atlassian.webresource.plugin.rest.two.zero.model;

import javax.annotation.Nonnull;

/**
 * Data Transfer Object (DTO) to adapt a {@code ResourcePhase} enumeration for transfer via REST.
 */
public class ResourcePhase {
    private final String name;

    public ResourcePhase(@Nonnull final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
