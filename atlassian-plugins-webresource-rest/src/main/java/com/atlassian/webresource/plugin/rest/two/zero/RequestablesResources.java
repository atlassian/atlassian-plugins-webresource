package com.atlassian.webresource.plugin.rest.two.zero;

import java.util.Collection;
import java.util.function.Function;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import io.swagger.v3.oas.annotations.responses.ApiResponse;

import com.atlassian.plugins.rest.api.security.annotation.LicensedOnly;
import com.atlassian.webresource.plugin.rest.two.zero.graph.RequestableGraph;
import com.atlassian.webresource.plugin.rest.two.zero.graph.RequestableGraphService;
import com.atlassian.webresource.plugin.rest.two.zero.model.ErrorResponseJson;
import com.atlassian.webresource.plugin.rest.two.zero.model.RequestableEdgeJson;
import com.atlassian.webresource.plugin.rest.two.zero.model.RequestableEdgeListResponseJson;

import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

/**
 * REST endpoint for exploring the inter-relationship between {@code Requestable} objects in the system,
 * such as finding all of its consumers, or finding all of its dependencies.
 *
 * @since v5.3.0
 */
@LicensedOnly
@Path("requestables")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class RequestablesResources {
    private final RequestableGraphService requestableGraphService;

    @GET
    public Response getIndex() {
        return ok().build();
    }

    @Inject
    public RequestablesResources(RequestableGraphService requestableGraphService) {
        this.requestableGraphService = requestableGraphService;
    }

    @Path("{requestable}/consumer-graph")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    @ApiResponse(responseCode = "404", description = "requestable not found")
    @ApiResponse(responseCode = "200", description = "graph of all requestables that consume this one")
    @GET
    public Response getSerializedConsumerGraph(@PathParam("requestable") String requestableId) {
        return responseOf(requestableId, requestableGraphService::getConsumerGraphById);
    }

    @Path("{requestable}/dependency-graph")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    @ApiResponse(responseCode = "404", description = "requestable not found")
    @ApiResponse(responseCode = "200", description = "graph of all requestables that this one depends on")
    @GET
    public Response getSerializedDependencyGraph(@PathParam("requestable") String requestableId) {
        return responseOf(requestableId, requestableGraphService::getDependencyGraphById);
    }

    private Response responseOf(final String requestableId, Function<String, RequestableGraph> graphProducer) {
        if (requestableGraphService.hasById(requestableId)) {
            RequestableGraph graph = graphProducer.apply(requestableId);
            // serialize the edges of the graph
            return ok(convertGraphEdgesToRequestableItems(graph)).build();
        } else {
            return status(NOT_FOUND)
                    .entity(new ErrorResponseJson("Could not find `" + requestableId + "` in the graph."))
                    .build();
        }
    }

    private RequestableEdgeListResponseJson convertGraphEdgesToRequestableItems(RequestableGraph graph) {
        Collection<RequestableEdgeJson> items = stream(graph.getEdges().spliterator(), false)
                .map(edge -> new RequestableEdgeJson(edge.getSource(), edge.getTarget(), edge.getPhase()))
                .collect(toList());
        return new RequestableEdgeListResponseJson(items);
    }
}
