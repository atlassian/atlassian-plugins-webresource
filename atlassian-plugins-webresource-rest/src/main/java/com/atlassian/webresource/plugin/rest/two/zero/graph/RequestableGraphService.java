package com.atlassian.webresource.plugin.rest.two.zero.graph;

/**
 * Queries the WRM's dependency graph to discover and provide contextually relevant sub-graphs.
 */
public interface RequestableGraphService {
    /**
     * Determine whether a specific {@code Requestable} exists in the WRM's dependency graph or not.
     * @param id the unique name of the {@code Requestable}.
     * @return true if the {@code Requestable} object is present in the WRM's dependency graph, false otherwise.
     */
    boolean hasById(String id);

    /**
     * Given a {@code Requestable} ID, return a sub-graph of all other {@code Requestable} items that depend on it.
     * @param id the unique name of the {@code Requestable}.
     * @return a complete graph of all consumers, along with their transitive consumers.
     */
    RequestableGraph getConsumerGraphById(String id);

    /**
     * Given a {@code Requestable} ID, return a sub-graph of all {@code Requestable} items that this one depends upon.
     * @param id the unique name of the {@code Requestable}.
     * @return a complete graph of all dependencies, along with their transitive consumers.
     */
    RequestableGraph getDependencyGraphById(String id);
}
