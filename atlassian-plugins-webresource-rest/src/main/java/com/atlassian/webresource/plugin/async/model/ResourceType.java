package com.atlassian.webresource.plugin.async.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.ObjectUtils.notEqual;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import static com.atlassian.plugin.webresource.Flags.isDevMode;
import static com.atlassian.webresource.api.UrlMode.RELATIVE;

public enum ResourceType {
    JS,
    CSS;

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceType.class);

    /**
     * Get the resurce type according to its instance type.
     *
     * @param pluginUrlResource The plugin url resource used to verify its instance type.
     * @return The found resource type.
     * @throws IllegalArgumentException If the instance type is not supported.
     */
    public static ResourceType getResourceType(final PluginUrlResource<?> pluginUrlResource) {
        if (pluginUrlResource instanceof PluginJsResource) {
            return JS;
        }
        if (pluginUrlResource instanceof PluginCssResource) {
            checkLogMediaQueryError((PluginCssResource) pluginUrlResource);
            return CSS;
        }
        final String errorMessage =
                format("The provided class type %s is not supported.", pluginUrlResource.getClass());
        throw new IllegalArgumentException(errorMessage);
    }

    /**
     * Checks if the given resource contains a media query, and logs a warning if it does. Logging is done in dev mode only.
     *
     * @param cssResource css resource to check
     */
    private static void checkLogMediaQueryError(final PluginCssResource cssResource) {
        final String mediaQueryParameters = cssResource.getParams().media();
        ofNullable(mediaQueryParameters)
                .filter(params -> isNotEmpty(params) && notEqual("all", params) && isDevMode())
                .ifPresent(params -> LOGGER.warn(
                        "WARN: asynchronously loading a CSS resource containing a media query: {}",
                        cssResource.getStaticUrl(RELATIVE)));
    }
}
