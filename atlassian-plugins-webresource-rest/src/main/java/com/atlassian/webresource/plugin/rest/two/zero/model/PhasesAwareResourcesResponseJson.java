package com.atlassian.webresource.plugin.rest.two.zero.model;

import java.util.Objects;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

@JsonPropertyOrder(alphabetic = true)
public final class PhasesAwareResourcesResponseJson {
    private final UrlFetchableResourcesWithDataJson require;
    private final UrlFetchableResourcesWithDataJson interaction;

    @JsonCreator
    public PhasesAwareResourcesResponseJson(
            @Nonnull @JsonProperty("interaction") final UrlFetchableResourcesWithDataJson interaction,
            @Nonnull @JsonProperty("require") final UrlFetchableResourcesWithDataJson require) {
        this.require = requireNonNull(require, "The required resources are mandatory.");
        this.interaction = requireNonNull(interaction, "The resources for interaction are mandatory.");
    }

    @Nonnull
    @JsonProperty("require")
    public UrlFetchableResourcesWithDataJson getRequire() {
        return require;
    }

    @Nonnull
    @JsonProperty("interaction")
    public UrlFetchableResourcesWithDataJson getInteraction() {
        return interaction;
    }

    @Override
    public boolean equals(final Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject instanceof PhasesAwareResourcesResponseJson) {
            final PhasesAwareResourcesResponseJson other = (PhasesAwareResourcesResponseJson) otherObject;
            return require.equals(other.require) && interaction.equals(other.interaction);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(require, interaction);
    }

    @Override
    public String toString() {
        return reflectionToString(this, JSON_STYLE);
    }
}
