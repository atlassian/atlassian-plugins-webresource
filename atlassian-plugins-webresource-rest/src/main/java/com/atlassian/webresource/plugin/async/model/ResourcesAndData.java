package com.atlassian.webresource.plugin.async.model;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nonnull;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.assembler.WebResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.api.data.PluginDataResource;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.StreamSupport.stream;

import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE;

public class ResourcesAndData {
    private final OutputShape require;
    private final OutputShape interaction;

    public ResourcesAndData(@Nonnull final OutputShape require, @Nonnull final OutputShape interaction) {
        this.require = requireNonNull(require, "The required resources are mandatory.");
        this.interaction = requireNonNull(interaction, "The resources for interaction are mandatory.");
    }

    public ResourcesAndData(@Nonnull final Iterable<WebResource> resources) {
        this(buildOutputShape(resources, REQUIRE), buildOutputShape(resources, INTERACTION));
    }

    @Nonnull
    public OutputShape getRequire() {
        return require;
    }

    @Nonnull
    public Collection<ResourceTypeAndUrl> getRequireResources() {
        return new ArrayList<>(require.getResources());
    }

    @Nonnull
    public Map<String, String> getRequiredResourcesUnparsedData() {
        return new HashMap<>(require.getUnparsedData());
    }

    @Nonnull
    public Map<String, String> getRequireResourcesUnparsedErrors() {
        return new HashMap<>(require.getUnparsedErrors());
    }

    @Nonnull
    public Collection<ResourceTypeAndUrl> getResourcesForInteration() {
        return new ArrayList<>(interaction.getResources());
    }

    @Nonnull
    public Map<String, String> getResourcesForInterationUnparsedData() {
        return new HashMap<>(interaction.getUnparsedData());
    }

    @Nonnull
    public Map<String, String> getResourcesForInterationUnparsedErrors() {
        return new HashMap<>(interaction.getUnparsedErrors());
    }

    @Nonnull
    public OutputShape getInteraction() {
        return interaction;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof ResourcesAndData) {
            final ResourcesAndData otherResourcesAndData = (ResourcesAndData) other;
            return require.equals(otherResourcesAndData.require)
                    && interaction.equals(otherResourcesAndData.interaction);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(require, interaction);
    }

    @Override
    public String toString() {
        return "ResourcesAndData{ requirePhase=" + require + ", interactionPhase=" + interaction + " }";
    }

    private static OutputShape buildOutputShape(
            final Iterable<WebResource> resources, final ResourcePhase resourcePhase) {
        requireNonNull(resources, "The resources are mandatory to perform the conversion to OutputShape.");

        final Map<String, String> data = stream(resources.spliterator(), false)
                .filter(resource -> resourcePhase == resource.getResourcePhase())
                .filter(PluginDataResource.class::isInstance)
                .map(PluginDataResource.class::cast)
                .filter(resource -> resource.getData().isPresent())
                .collect(toMap(PluginDataResource::getKey, resource -> jsonToString(resource.getJsonable())));

        final Map<String, String> errors = stream(resources.spliterator(), false)
                .filter(resource -> resourcePhase == resource.getResourcePhase())
                .filter(PluginDataResource.class::isInstance)
                .map(PluginDataResource.class::cast)
                .filter(resource -> !resource.getData().isPresent())
                .collect(toMap(PluginDataResource::getKey, resource -> jsonToString(resource.getJsonable())));

        final Collection<ResourceTypeAndUrl> resourcesTypesAndUrls = stream(resources.spliterator(), false)
                .filter(resource -> resourcePhase == resource.getResourcePhase())
                .filter(PluginUrlResource.class::isInstance)
                .map(PluginUrlResource.class::cast)
                .map(ResourceTypeAndUrl::new)
                .collect(toCollection(ArrayList::new));

        return new OutputShape(resourcesTypesAndUrls, data, errors);
    }

    private static String jsonToString(final Jsonable jsonable) {
        try {
            final StringWriter out = new StringWriter();
            jsonable.write(out);
            return out.toString();
        } catch (final IOException exception) {
            throw new IllegalStateException(exception);
        }
    }
}
