package com.atlassian.webresource.plugin.rest.two.zero.graph;

import com.atlassian.webresource.plugin.rest.two.zero.model.ResourcePhase;

/**
 * Data Transfer Object (DTO) to adapt pairs of {@code Requestable} objects in a {@code DependencyGraph}
 * for transfer via REST.
 */
public class RequestableEdge {
    private final Requestable source;
    private final Requestable target;
    private final ResourcePhase phase;

    public RequestableEdge(Requestable source, Requestable target, ResourcePhase phase) {
        this.source = source;
        this.target = target;
        this.phase = phase;
    }

    public Requestable getSource() {
        return source;
    }

    public Requestable getTarget() {
        return target;
    }

    public ResourcePhase getPhase() {
        return phase;
    }
}
