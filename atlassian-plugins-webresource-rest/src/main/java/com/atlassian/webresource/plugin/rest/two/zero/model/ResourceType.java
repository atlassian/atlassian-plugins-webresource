package com.atlassian.webresource.plugin.rest.two.zero.model;

public enum ResourceType {
    JS,
    CSS;
}
