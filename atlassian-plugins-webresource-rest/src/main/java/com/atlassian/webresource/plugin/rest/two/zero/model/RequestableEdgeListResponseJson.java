package com.atlassian.webresource.plugin.rest.two.zero.model;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * JSON shape for sending a list of {@link RequestableEdgeJson} items to the client via REST.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
public final class RequestableEdgeListResponseJson {
    Collection<RequestableEdgeJson> items;

    @JsonCreator
    public RequestableEdgeListResponseJson(
            @Nonnull @JsonProperty("items") final Collection<RequestableEdgeJson> items) {
        this.items = items;
    }

    @Nonnull
    @JsonProperty("items")
    public Collection<RequestableEdgeJson> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return String.format(
                "RequestableEdgeList: %d edges.%n%s",
                items.size(), items.stream().map(Objects::toString).collect(Collectors.joining("\n")));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestableEdgeListResponseJson that = (RequestableEdgeListResponseJson) o;
        return Objects.equals(items, that.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(items);
    }
}
