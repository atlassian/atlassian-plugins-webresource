package com.atlassian.webresource.plugin.util;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.databind.ObjectMapper;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static com.fasterxml.jackson.databind.type.TypeFactory.defaultInstance;

/**
 * Class responsible for manipulating the data related to a JSON object.
 */
public final class JSONUtil {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.setSerializationInclusion(NON_EMPTY);
    }

    private JSONUtil() {}

    /**
     * Read a certain JSON file and map its value to a certain bean type.
     *
     * @param path The of the file to be read.
     * @param type The {@link JavaType} used for the conversion.
     * @return The representation of the file content as a bean.
     */
    public static <T> T fileToBean(final String path, final Class<T> type) {
        try {
            final InputStream file = JSONUtil.class.getResourceAsStream(path);
            return MAPPER.readValue(file, defaultInstance().constructType(type));
        } catch (final IOException exception) {
            throw new IllegalStateException(exception);
        }
    }
}
