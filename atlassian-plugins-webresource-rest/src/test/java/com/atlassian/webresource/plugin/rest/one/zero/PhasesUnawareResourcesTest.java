package com.atlassian.webresource.plugin.rest.one.zero;

import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.plugin.async.AsyncWebResourceLoader;
import com.atlassian.webresource.plugin.async.model.ResourcesAndData;
import com.atlassian.webresource.plugin.rest.one.zero.model.ResolveResourcesJson;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonMap;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PhasesUnawareResourcesTest {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private AsyncWebResourceLoader asyncWebResourceLoader;

    @Before
    public void setUp() throws IOException {
        when(asyncWebResourceLoader.resolve(anyMap(), anyMap(), anySet(), anySet()))
                .thenReturn(mock(ResourcesAndData.class));
    }

    @Test
    public void getDecodesCommaSeparatedValues() throws Exception {
        // when
        final PhasesUnawareResources phasesUnawareResources = new PhasesUnawareResources(asyncWebResourceLoader);
        phasesUnawareResources.get("wr1,wr2", "c1,c2", "xr1,xr2", "xc1,xc2");

        // then
        verify(asyncWebResourceLoader)
                .resolve(
                        singletonMap(
                                ResourcePhase.defaultPhase(),
                                Sets.newSet(new WebResourceKey("wr1"), new WebResourceKey("wr2"))),
                        singletonMap(
                                ResourcePhase.defaultPhase(),
                                Sets.newSet(new WebResourceContextKey("c1"), new WebResourceContextKey("c2"))),
                        PhasesUnawareResources.mapsStringsToRequestables(
                                Sets.newSet("xr1", "xr2"), WebResourceKey::new),
                        PhasesUnawareResources.mapsStringsToRequestables(
                                Sets.newSet("xc1", "xc2"), WebResourceContextKey::new));
    }

    @Test
    public void getDecodesSingleValues() throws Exception {
        // when
        PhasesUnawareResources phasesUnawareResources = new PhasesUnawareResources(asyncWebResourceLoader);
        phasesUnawareResources.get("wr1", "c1", "xr1", "xc1");

        // then
        verify(asyncWebResourceLoader)
                .resolve(
                        singletonMap(ResourcePhase.defaultPhase(), Sets.newSet(new WebResourceKey("wr1"))),
                        singletonMap(ResourcePhase.defaultPhase(), Sets.newSet(new WebResourceContextKey("c1"))),
                        singleton(new WebResourceKey("xr1")),
                        singleton(new WebResourceContextKey("xc1")));
    }

    @Test
    public void getDecodesEmptyValues() throws Exception {
        // when
        final PhasesUnawareResources phasesUnawareResources = new PhasesUnawareResources(asyncWebResourceLoader);
        phasesUnawareResources.get("", "", "", "");

        // then
        verify(asyncWebResourceLoader).resolve(emptyMap(), emptyMap(), emptySet(), emptySet());
    }

    @Test
    public void postPassesRequestArgumentsThroughToAsyncWebResourceLoader() throws Exception {
        // when
        final PhasesUnawareResources phasesUnawareResources = new PhasesUnawareResources(asyncWebResourceLoader);
        phasesUnawareResources.post(new ResolveResourcesJson(
                asList("wr1", "wr2"), asList("c1", "c2"), asList("xr1", "xr2"), asList("xc1", "xc2")));

        // then
        verify(asyncWebResourceLoader)
                .resolve(
                        singletonMap(
                                ResourcePhase.defaultPhase(),
                                Sets.newSet(new WebResourceKey("wr1"), new WebResourceKey("wr2"))),
                        singletonMap(
                                ResourcePhase.defaultPhase(),
                                Sets.newSet(new WebResourceContextKey("c1"), new WebResourceContextKey("c2"))),
                        PhasesUnawareResources.mapsStringsToRequestables(
                                Sets.newSet("xr1", "xr2"), WebResourceKey::new),
                        PhasesUnawareResources.mapsStringsToRequestables(
                                Sets.newSet("xc1", "xc2"), WebResourceContextKey::new));
    }

    @Test
    public void requestCanBeInstantiatedWithoutArgumentsAsRequiredByJersey() {
        new ResolveResourcesJson();
    }
}
