package com.atlassian.webresource.plugin.async.model;

import java.util.Collection;
import java.util.Map;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OutputShapeMock extends OutputShape {
    @JsonCreator
    public OutputShapeMock(
            @Nonnull @JsonProperty("resources") final Collection<ResourceTypeAndUrlMock> resources,
            @Nonnull @JsonProperty("unparsedData") final Map<String, String> unparsedData,
            @Nonnull @JsonProperty("unparsedErrors") final Map<String, String> unparsedErrors) {
        super(resources, unparsedData, unparsedErrors);
    }
}
