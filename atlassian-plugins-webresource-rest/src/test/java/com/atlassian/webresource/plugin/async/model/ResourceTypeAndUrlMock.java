package com.atlassian.webresource.plugin.async.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginCssResourceParams;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResourceParams;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;

import static java.lang.String.format;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.spy;

import static com.atlassian.webresource.api.UrlMode.RELATIVE;

/**
 * Helper class used to easily mock the results for {@link ResourceTypeAndUrl}.
 */
public class ResourceTypeAndUrlMock extends ResourceTypeAndUrl {

    @JsonCreator
    public ResourceTypeAndUrlMock(
            @JsonProperty("batchType") final BatchType batchType,
            @JsonProperty("key") final String key,
            @JsonProperty("resourceType") final ResourceType resourceType,
            @JsonProperty("url") final String url,
            @JsonProperty("parameters") final Map<String, String> parameters) {
        super(buildPluginUrlResource(resourceType, parameters));
        lenient().when(getPluginUrlResource().getBatchType()).thenReturn(batchType);
        lenient().when(getPluginUrlResource().getKey()).thenReturn(key);
        lenient().when(getPluginUrlResource().getStaticUrl(RELATIVE)).thenReturn(url);
    }

    private static PluginUrlResource<?> buildPluginUrlResource(
            final ResourceType resourceType, final Map<String, String> parameters) {
        switch (resourceType) {
            case CSS: {
                final PluginCssResource pluginResource = spy(PluginCssResource.class);
                final PluginCssResourceParams params = spy(PluginCssResourceParams.class);
                lenient().when(pluginResource.getParams()).thenReturn(params);
                lenient().when(params.all()).thenReturn(parameters);
                return pluginResource;
            }
            case JS: {
                final PluginJsResource pluginResource = spy(PluginJsResource.class);
                final PluginJsResourceParams params = spy(PluginJsResourceParams.class);
                lenient().when(pluginResource.getParams()).thenReturn(params);
                lenient().when(params.all()).thenReturn(parameters);
                return pluginResource;
            }
            default: {
                final String errorMessage = format("The provided type %s is not supported.", resourceType);
                throw new IllegalArgumentException(errorMessage);
            }
        }
    }
}
