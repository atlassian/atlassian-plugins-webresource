package com.atlassian.webresource.plugin.async.model;

import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ResourcesAndDataMock extends ResourcesAndData {
    @JsonCreator
    public ResourcesAndDataMock(
            @Nonnull @JsonProperty("require") final OutputShapeMock require,
            @Nonnull @JsonProperty("interaction") final OutputShapeMock interaction) {
        super(require, interaction);
    }
}
