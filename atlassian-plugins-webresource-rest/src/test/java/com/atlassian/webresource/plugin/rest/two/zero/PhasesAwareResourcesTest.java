package com.atlassian.webresource.plugin.rest.two.zero;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.webresource.models.WebResourceKey;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.plugin.async.AsyncWebResourceLoader;
import com.atlassian.webresource.plugin.async.model.ResourcesAndData;
import com.atlassian.webresource.plugin.async.model.ResourcesAndDataMock;
import com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareRequestJson;
import com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareResourcesResponseJson;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.webresource.plugin.util.JSONUtil.fileToBean;

@RunWith(MockitoJUnitRunner.class)
public class PhasesAwareResourcesTest {
    private static final String TEST_CASES_BASE_PATH = "/test-case/rest/phases-aware-resources/%s";

    @Captor
    ArgumentCaptor<Map<ResourcePhase, Set<WebResourceKey>>> resourceByPhaseCaptor;

    @Mock
    private AsyncWebResourceLoader asyncWebResourceLoader;

    @InjectMocks
    private PhasesAwareResources underTest;

    @Test
    public void when_ThereAreResources_Then_ReturnAllResources() throws IOException {
        // given
        final String mockedResourceAndDataFullPath = format(TEST_CASES_BASE_PATH, "there-are-resources/mock.json");
        final ResourcesAndData mockedResourceAndData =
                fileToBean(mockedResourceAndDataFullPath, ResourcesAndDataMock.class);
        when(asyncWebResourceLoader.resolve(any(), any(), any(), any())).thenReturn(mockedResourceAndData);

        // when
        final String requestFullPath = format(TEST_CASES_BASE_PATH, "there-are-resources/request.json");
        final PhasesAwareRequestJson mockedRequest = fileToBean(requestFullPath, PhasesAwareRequestJson.class);
        final PhasesAwareResourcesResponseJson actualResponse = underTest.post(mockedRequest);

        // then
        final String expectedResponseFullPath =
                format(TEST_CASES_BASE_PATH, "there-are-resources/expectedResponse.json");
        final PhasesAwareResourcesResponseJson expectedResponse =
                fileToBean(expectedResponseFullPath, PhasesAwareResourcesResponseJson.class);
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void when_ResourcesRequested_Then_InclusionOrderPreserved() throws IOException {
        // given
        final String mockedResourceAndDataFullPath = format(TEST_CASES_BASE_PATH, "empty-response/mock.json");
        final ResourcesAndData mockedResourceAndData =
                fileToBean(mockedResourceAndDataFullPath, ResourcesAndDataMock.class);
        when(asyncWebResourceLoader.resolve(any(), any(), any(), any())).thenReturn(mockedResourceAndData);

        // when
        final PhasesAwareRequestJson requestOne = new PhasesAwareRequestJson(
                asList("wr!one", "wr!two", "wr!three", "wr!four", "wr!five", "wr!six"), emptyList(), emptyList());
        underTest.post(requestOne);

        // then
        verify(asyncWebResourceLoader).resolve(resourceByPhaseCaptor.capture(), anyMap(), anySet(), anySet());
        final Map<ResourcePhase, Set<WebResourceKey>> value = resourceByPhaseCaptor.getValue();
        final List<String> requestedKeysForRequire = value.get(ResourcePhase.REQUIRE).stream()
                .map(WebResourceKey::getKey)
                .collect(toList());
        assertThat(requestedKeysForRequire, contains("one", "two", "three", "four", "five", "six"));
    }
}
