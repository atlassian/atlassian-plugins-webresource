import path from "path";
import rollupTypescript from "@rollup/plugin-typescript";
import resolve from "@rollup/plugin-node-resolve";
import terser from '@rollup/plugin-terser';

const OUTPUT_PATH = path.join(__dirname, "target/classes/js/dist");

const plugins = [resolve(), rollupTypescript({ include: "src/main/clientside/**/*.{ts,js}" })];

function getOutputConfig(fileName) {
    const filePath = `${OUTPUT_PATH}/${fileName}.js`;
    const minFilePath = `${OUTPUT_PATH}/${fileName}.min.js`;

    return [
        {
            file: filePath,
            format: "iife",
        },
        {
            file: minFilePath,
            format: "iife",
            plugins: [terser()],
        },
    ];
}

export default [
    {
        input: "src/main/clientside/wrm/data-collector/async.ts",
        output: getOutputConfig("data-collector-async"),
        plugins,
    },
    {
        input: "src/main/clientside/wrm/data-collector/perf-observer.ts",
        output: getOutputConfig("data-collector-perf-observer"),
        plugins,
    },
    {
        input: "src/main/clientside/wrm/index.ts",
        output: getOutputConfig("wrm"),
        plugins,
    },
];
