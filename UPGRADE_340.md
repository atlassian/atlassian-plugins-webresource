# Host application upgrade notes

## Decorators

Prior to 3.4.0, there were several ways to drain and emit the collected resources into &lt;script> and &lt;link>
tags onto a page:

* In the earlier version of the WRM, host applications were responsible for calling
  `wrm.includeResources(out, ...)` in a decorator.
* In 3.0, WebResourceManager was deprecated in favor of WebResourceAssembler, and host applications
  could call `PageBuilderService.assembler().assembled().drainIncludedResources().writeHtmlTags(out, ...)`.
  They could also call the old deprecated wrm.includeResources().
* In both cases, the WRM would guarantee that after calling those methods, everything that had required
  would be written to the page.
  
In 3.4.0, this last guarantee does not apply to asynchronously required data (bigpipe data) added
to the assembler via `requireData(String key, CompletionStage<Jsonable> promise)`. This is because
`drainIncludedResources()` will not block, and hence will not capture any incomplete promises.

For host applications that want to support bigpipe, they need to call `pollIncludedResources()` in addition to
(or instead of) `drainIncludedResources()`. The expected pattern is:

* Call `drainIncludedResources()` during the rendering of the `<head>`.
* Some decorators may call `drainIncludedResources()` multiple times in `<head>` (if they are flushing head early).
* Just before the closing `</body>` tag, loop over `pollIncludedResources()` like so:

```java
WebResourceSet resources;
do {
  resources = assembler.assembled().pollIncludedResources();
  resources.writeHtmlTags(out, UrlMode.AUTO);
  out.flush();
} while (!resources.isComplete());      
```

* Decorators can call `drainIncludedResources()` multiple times at any other point between `<body>` and `</body>`,
  if that is a good opportunity to flush complete promises through to the client side.

## Source Maps

In the previous version Source Maps supposed to be enabled in Development only, now it's possible to enable it 
for Production too without the performance degradation.

``` java
WebResourceBatchingConfiguration.isSourceMapEnabled
```

Source Maps performance could be tuned, this option configures Web Resource if it should optimise Source Map for 
Production or Development. If you enable this option Source Map generation will be much faster. But, the JS & CSS 
generation will be slightly slower, so while this option could be good for Development, in Production it should be 
disabled.

``` java
WebResourceBatchingConfiguration.optimiseSourceMapsForDevelopment
```

## Incremental Cache

Enables the incremental build cache, it's a dark feature, it should be disabled.

``` java
WebResourceIntegration.isIncrementalCacheEnabled
```

## AMD

Register new descriptor `web-modules` as `com.atlassian.plugin.webresource.impl.modules.ModulesDescriptor`. 
In JIRA it should be done in the `JiraModuleDescriptorFactory`:

``` java
addModuleDescriptor("web-modules", ModulesDescriptor.class);
```

The WRM will resolve AMD dependencies and deliver it to the Browser in the right order, but the products need to 
provide the AMD loader by itself. So the next step is to provide AMD loader, any AMD loader can be used, for example 
`almond.js`, it needs to be added to the superbatch. 

Add the AMD loader to the superbatch.

If you use `almond.js` add one more dependency `com.atlassian.plugins.atlassian-plugins-webresource-plugin:loaders` 
after it. If you don't use `almond.js`, you need to provide the implementation for the `wr!`, `css!` and `less!` loaders 
by yourself.

Add the `com.atlassian.plugins.atlassian-plugins-webresource-plugin:loaders` to the superbatch.

The WRM object has been exposed as a global variable `window.WRM`, now it's also additionally exposed as a set of AMD 
modules: `wrm`, `wrm/require`, `wrm/data`, `wrm/context-path`. Some products may already have such modules defined, 
if so those definitions needs to be removed so there will be no conflict.

AMD is disabled by default, if you would like to enbable it please set the following to true:

```
WebResourceIntegration.amdEnabled
```

Also please read [AMD in WRM Quick Start](https://extranet.atlassian.com/display/FED/AMD+in+WRM+Quick+Start).