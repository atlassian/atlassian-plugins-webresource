# For everyone

## Changed API

### Moving types (classes and interfaces) around
We introduced replacements for existing API. This has the following advantages;
- What is API is better defined, you should be able to get everything you need from the API maven module
- We were able to remove third-party types from the API decoupling from other dependencies such as Guava and Dom4j,
  which means:
    - WRM is no longer a blocker in upgrading them
    - those dependencies don't need to be used to be able to use WRM

Here is the list of affected classes and what to migrate to:

| Old fully-qualified type name                                                 | New fully-qualified type name                                              |
|-------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| com.atlassian.plugin.webresource.condition.UrlReadingCondition                | com.atlassian.webresource.spi.condition.UrlReadingCondition                |
| com.atlassian.plugin.webresource.transformer.TransformableResource            | com.atlassian.webresource.api.transformer.TransformableResource            |
| com.atlassian.plugin.webresource.transformer.TransformerParameters            | com.atlassian.webresource.api.transformer.TransformerParameters            |
| com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder            | com.atlassian.webresource.spi.transformer.TransformerUrlBuilder            |
| com.atlassian.plugin.webresource.transformer.TwoPhaseResourceTransformer      | com.atlassian.webresource.spi.transformer.TwoPhaseResourceTransformer      |
| com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer | com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer |
| com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory    | com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory    |
| com.atlassian.plugin.webresource.url.UrlBuilder                               | com.atlassian.webresource.api.url.UrlBuilder                               |
| com.atlassian.plugin.webresource.WebResourceUrlProvider                       | com.atlassian.webresource.api.WebResourceUrlProvider                       |
| com.atlassian.plugin.webresource.WebResourceManager                           | com.atlassian.webresource.api.WebResourceManager                           |
| com.atlassian.plugin.webresource.QueryParams                                  | com.atlassian.webresource.api.QueryParams                                  |
| com.atlassian.plugin.webresource.WebResourceFilter                            | com.atlassian.webresource.api.WebResourceFilter                            |
| com.atlassian.plugin.webresource.WebResourceUrlProvider                       | com.atlassian.webresource.api.WebResourceUrlProvider                       |
| com.atlassian.plugin.webresource.UrlMode                                      | com.atlassian.webresource.api.UrlMode                                      |

### Module descriptors

`WebResourceModuleDescriptor` and `UrlReadingWebResourceTransformerModuleDescriptor` have been extracted as `ModuleDescriptor` interfaces and moved under the `com.atlassian.webresource.api.decorator` package.

The new public version of `WebResourceModuleDescriptor` doesn't quite have everything, but it should have everything you need for inspecting web-resources at runtime.

There shouldn't be a need to create custom types of web-resources; Transformers and Conditions are very powerful and can be given parameters in plugin descriptors. [Learn more](https://developer.atlassian.com/server/framework/atlassian-sdk/stateless-web-resource-transforms-and-conditions/).

If these aren't powerful enough, reach out to us in [this ticket](https://ecosystem.atlassian.net/browse/PLUGWEB-765) or on Developer Community explaining your use-case.

### Removal of dom4j
The `init` method of Module descriptors implement the new API. The old `init(Plugin, org.dom4j.Element)` method has been changed to the new `init(Plugin, com.atlassian.plugin.module.Element)` one. The API shape is the same, so it's just a matter of changing imports.

### Removal of Guava

- `WebResourceDataProvider` will extend from Java's supplier instead of Guava's
- `WebResourceSet#(Writer, UrlMode, Predicate<WebResource>)` will use Java's predicate instead of Guava's

In both cases you shouldn't have to anything other than recompile against the newer version of WRM. Guava's supplier and predicate both extend from the Java equivalents.

## Removed API / SPI

### Legacy / stateful conditions and transformers on web-resources

`com.atlassian.plugin.web.Condition` can no longer be used on web-resources, use `UrlReadingCondition` instead. `Condition` will not be deleted, it will remain part of web fragments and can continue to be used with web-item, web-section, and web-panel.

`com.atlassian.plugin.webresource.transformer.WebResourceTransformer` can no longer be used at all, update to use `WebResourceTransformerFactory` instead.

We have an existing guide which goes in depth about how to make the transition, [learn more](https://developer.atlassian.com/server/framework/atlassian-sdk/stateless-web-resource-transforms-and-conditions/).

### Internet Explorer support

We dropped IE support in 2019 for all of our DC and Server products. Remove from your code:

- [conditional comments](https://en.wikipedia.org/wiki/Conditional_comment)
- IE only web-resources (and resources on web-resources) - these can be found by looking for `<param name="ieonly"` and `<param name="conditionalComment"`
- and any calls to the following methods:
  - `PluginUrlResourceParams#conditionalComment`
  - `PluginUrlResourceParams#ieOnly`
  - `PluginUrlResourceParams#other`

### Google Closure Compiler

GCC was setup to remove whitespace from all files in Jira. This could potentially introduce subtle and hard to debug issues as the specifications move on along with increasing startup so we've opted to remove it. AMPS will already minify resources for you using GCC, but it's transparent at build-time. JS build tools like Webpack, Parcel, Vite, etc. will also do minification and sourcemap creation for you, we recommend setting those up. The WRM will automatically use any file. We've removed the related interfaces, you should remove any reference to them in your code:

* `com.atlassian.webresource.spi.CompilerEntry`
* `com.atlassian.webresource.spi.CompilerUtil`
* `com.atlassian.webresource.spi.NoOpResourceCompiler`
* `com.atlassian.webresource.spi.ResourceCompiler`

### `SimpleUrlReadingCondition`
Extend from `AbstractBooleanUrlReadingCondition` instead.

### `WebResourceTransformerFactory#makeResourceTransformer(Element, TransformerParameters)`
Instead, only use the `TransformerParameters` which will provide the plugin and module key.

### Prebake signatures

- `AbstractBooleanUrlReadingCondition` used to implement prebake interfaces. You just need to recompile with a new version of the WRM.
- `UrlBuilder` has had two methods removed; `#addPrebakeError(PrebakeError)` and `#addAllPrebakeErrors(Collection<PrebakeError>)`. You can just remove your implementation of these

### Prebake

Prebake support is dropped. We ask you to remove any reference to these in your code, whether it be implementation or testing. There is no alternative feature to migrate to, this was a Cloud only feature.

For most migrating from `DimensionAwareUrlReadingCondition` to `UrlReadingCondition` and from `DimensionAwareWebResourceTransformerFactory` to `WebResourceTransformerFactory` will naturally lead to removing the majority of references to prebake. The URL reading conditions and transformers are only type subsets.

The REST API under `/webResources/ct-cdn/` and `/wrm/ct-cdn/`

Removed elements:
- the `atlassian.webresource.ignore.prebake.warnings` configuration property
- the `atlassian.webresource.enable.css.prebake` configuration property
- the `atlassian.webresource.enable.prebake` configuration property
- `com.atlassian.webresource.api.assembler.resource.PrebakeError`
- `com.atlassian.webresource.api.assembler.resource.PrebakeWarning`
- `com.atlassian.webresource.api.assembler.resource.PluginUrlResource#isTainted`
- `com.atlassian.webresource.api.assembler.resource.PluginUrlResource#getPrebakeErrors`
- `com.atlassian.webresource.api.prebake.Coordinate`
- `com.atlassian.webresource.api.prebake.CoordinateImpl`
- `com.atlassian.webresource.api.prebake.DimensionAwareTransformerUrlBuilder`
- `com.atlassian.webresource.api.prebake.DimensionAwareUrlReadingCondition`
- `com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory`
- `com.atlassian.webresource.api.prebake.Dimensions`
- `com.atlassian.webresource.api.prebake.DimensionsImpl`
- `com.atlassian.webresource.api.prebake.QueryParam`
- `com.atlassian.webresource.plugin.rest.one.zero.CrossTenantCdnResource`
- `com.atlassian.webresource.plugin.rest.two.zero.CrossTenantCdnResource`
- `com.atlassian.plugin.webresource.impl.PrebakeErrorFactory`
- `com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory`
- `com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerBuilder`
- `com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssembler`

### AMD signatures

- `RequiredResources#requireModule` - this method hasn't worked since WRM v5, remove any usages of it and replace with calls to require a regular web-resource instead.

### BigPipe signatures

BigPipe support was removed in v5. Instead, use resource phases to defer loading of resources and remove any calls to `WebResourceAssemblerBuilder#asyncDataDeadline`.

Additionally, the `RequiredData#requireData(String key, CompletionStage<Jsonable> promise)` and `AssembledResources#pollIncludedResources()` methods are removed. Their use made sense for asynchronous resources, so we are removing them as well.

### curl web-resource
The curl web-resource was removed. If you need it, you can always bundle it yourself. [We used 0.7.3](https://github.com/cujojs/curl)

### `WebResourceAssemblerFactory#clearCache`

This cleared the web-resource cache, there's no need to call this so remove any calls. The WRM automatically clears its internal caches as web-resources, plugins, and web-resource transformers work their way through lifecycle phases.

### `PluginResourceLocator#temporaryWayToGetGlobalsDoNotUseIt`

The Globals class exposes the whole WRM and so much that cannot be supported. For product teams; there's no need for this method, the object can be injected (DI).

### Replaced methods on `WebResourceManager`

Many deprecated methods have been removed, here are their replacements;

| Replaced method                                                                                                          | New method                                                                                                                      |
|--------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| `WebResourceManager#executeInNewContext(Supplier<T>)`                                                                    | `WebResourceAssemblerFactory#create`                                                                                            |
| `WebResourceManager#getResourceTags(String moduleCompleteKey)`                                                           | `WebResourceManager#getResourceTags(String moduleCompleteKey, UrlMode urlMode)`                                                 |
| `WebResourceManager#getRequiredResources()`                                                                              | `WebResourceManager#getRequiredResources(UrlMode urlMode)`                                                                      |
| `WebResourceManager#getStaticResourcePrefix()`                                                                           | `WebResourceUrlProvider#getStaticResourcePrefix(UrlMode urlMode)`                                                               |
| `WebResourceManager#getStaticResourcePrefix(String resourceCounter)`                                                     | `WebResourceUrlProvider#getStaticResourcePrefix(String resourceCounter, UrlMode urlMode)`                                       |
| `WebResourceManager#getStaticPluginResource(String moduleCompleteKey, String resourceName)`                              | `WebResourceUrlProvider#getStaticPluginResourceUrl(String moduleCompleteKey, String resourceName, UrlMode urlMode)`             |
| `WebResourceManager#getStaticPluginResource(String moduleCompleteKey, String resourceName, UrlMode urlMode)`             | `WebResourceUrlProvider#getStaticPluginResourceUrl(String moduleCompleteKey, String resourceName, UrlMode urlMode)`             |
| `WebResourceManager#getStaticPluginResource(ModuleDescriptor<?> moduleDescriptor, String resourceName)`                  | `WebResourceUrlProvider#getStaticPluginResourceUrl(ModuleDescriptor<?> moduleDescriptor, String resourceName, UrlMode urlMode)` |
| `WebResourceManager#getStaticPluginResource(ModuleDescriptor<?> moduleDescriptor, String resourceName, UrlMode urlMode)` | `WebResourceUrlProvider#getStaticPluginResourceUrl(ModuleDescriptor<?> moduleDescriptor, String resourceName, UrlMode urlMode)` |
| `WebResourceManager#includeResources(Writer writer)`                                                                     | `WebResourceManager#includeResources(Writer writer, UrlMode urlMode)`                                                           |
| `WebResourceManager#requireResource(String moduleCompleteKey, Writer writer)`                                            | `WebResourceManager#requireResource(String moduleCompleteKey, Writer writer, UrlMode urlMode)`                                  |

# For host application engineers

## Data collector web-resource dependencies

Please remove dependencies:
 - com.atlassian.auiplugin:aui-core
 - com.atlassian.analytics.analytics-client:js-events

If they were added specifically for `data-collector-async` web resource.

## Registering module descriptors

Please use the new interfaces whereever possible, but for registering the module descriptors please use the old/implementation types:
- `com.atlassian.plugin.webresource.WebResourceModuleDescriptor`
- `com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor`
