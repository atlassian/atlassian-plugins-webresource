module.exports = {
    verbose: true,
    testTimeout: 60000,
    testPathIgnorePatterns: ['/node_modules/', '/dist/', '/target/', '/__fixtures__/'],
    clearMocks: true,
};
