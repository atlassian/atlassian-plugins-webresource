const config = require('./jest.config');

module.exports = {
    ...config,
    // configure Jest to produce coverage and execution reports
    collectCoverage: true,
    // this will make sure to collect coverage from all file, even if they don't have any test
    // https://jestjs.io/docs/en/configuration.html#collectcoveragefrom-array
    collectCoverageFrom: ['**/*.{ts,tsx}', '!**/__tests__/**', '!**/dist/**', '!**/node_modules/**'],
    coverageDirectory: '<rootDir>/coverage/jest',
    reporters: [
        'default',
        [
            'jest-junit',
            {
                // https://support.atlassian.com/bitbucket-cloud/docs/test-reporting-in-pipelines/
                outputDirectory: './test-reports/',
                outputName: 'jest-coverage-report.xml',
            },
        ],
        [
            'jest-sonar',
            {
                outputDirectory: './coverage/jest',
                outputName: 'sonar-report.xml',
            },
        ],
    ],
};
