# Host application upgrade notes

Version 5.0.0 of the WRM includes the following backward-incompatible changes:

* Removed support for IE 11
* Removed dependencies on jQuery
* Removed unused `web-modules` behaviour
* Removed "async" loading flag
* Now using ES2015 syntax
* New method signature for `WRM.require`
* `WRM.curl` is deprecated
* Updated HTML encoder dependency

## `web-modules` removal

In WRM 5.0, we have removed the `web-modules` behaviour and all associated code paths.

This feature was introduced in WRM 3.5.0, but for various reasons, the feature has been unused by any
Atlassian Server or DC product.

The feature was coupled to shipping an AMD loader at runtime and transforming code to fit in to this loader.
These transformations were complicated, limited in scope of supported frontend technologies, and had problems related
to naming and lookup of needed modules.

The frontend development world is moving more toward build-time compilation. To follow suit, the WRM is
discouraging and moving away from runtime transformations, limiting its focus on pushing ordered effects to the
browser client.

The uptake of [`atlassian-webresource-webpack-plugin`](wrm-webpack) has allowed developers to make use of AMD modules
and ES2015 modules in their source code, then compile them away for runtime. This avoids exposing internal
implementation details for features to third parties and can result in smaller bundles.

### What to remove

You will need to find and remove any of the following code from your products and plugins.

* In `atlassian-plugin.xml`:
    * `<web-modules ...`
    * `<web-module ...`
    * `<module-dependency ...`
    * `com.atlassian.plugins.atlassian-plugins-webresource-plugin:atlassian-modules`
    * `com.atlassian.plugins.atlassian-plugins-webresource-plugin:loaders`

* In JavaScript:
    * `WRM.atlassianModule`

* In Java packages:
    * `com.atlassian.plugin.webresource.impl.annotators.ModuleAnnotator`
    * `com.atlassian.plugin.webresource.impl.modules.ModulesDescriptor`
    * `com.atlassian.plugin.webresource.impl.snapshot.WebModule`
    * `com.atlassian.plugin.webresource.impl.snapshot.resource.ModuleResource`

* In Java classes:
    * `Config#amdEnabled`
    * `Config#parseModules`
    * `Config#parseAtlassianModules`
    * `Config#getModuleResource`
    * `Config#useConditionsForWebModules`
    * `Config.nameAndLoaderToNameWithExtension`
    * `Config.nameAndLoaderToNameWithLoader`
    * `Config.parseNameWithLoader`
    * `Config~WebModuleDescriptor`
    * `ResourceFactory#createModuleResource`
    * `ResourceFactory#createContextResource`
    * `ResourceServingHelpers#getModuleResource`
    * `TransformerParameters#isAmdModule`
    * `TransformerParameters#getAmdModuleLocation`
    * `WebResourceModuleDescriptor#getModuleDependencies`
    * `WebResourceModuleDescriptorBuilder#addModuleDependency`
    * `WebResourceIntegration#amdEnabled`
    * `WebResourceIntegration#getDefaultAmdModuleTransformers`

### What to change

In any location where a boolean value was set for whether AMD is enabled or not, assume it evaluates to `false`.

In Java:

* Replace `JavaScriptWebResource(boolean)` with `JavaScriptWebResource()`. If you use `#matches`, ensure you do not expect it to match `.soy` files.
* Replace `CssWebResource(boolean)` with `CssWebResource()`. If you use `#matches`, ensure you do not expect it to match `.less` files.
* Replace `DefaultWebResourceFilter(boolean)` with `DefaultWebResourceFilter()`. See the above two points if you use `#matches`.
* `TransformerParameters(String pluginKey, String moduleKey, String amdModuleLocation)` is deprecated. Use `TransformerParameters(String pluginKey, String moduleKey)` instead.

## "Async" loading flag removal

In WRM 5.0, we have removed the `useAsyncAttributeForScripts` flag.

Enabling this flag would cause all `<script>` tags to render with an `async` attribute. Since `async` scripts will
execute as soon as they are loaded, and since scripts loaded via WRM are order-dependent, all content for the scripts
would be transformed and wrapped in a call to `WRM.InOrderLoader` so that the order of execution would be preserved.

This behaviour was originally added when supported browsers had bugs in their `defer` attribute implementations.
Now that supported browsers do not include IE, all supported browsers have a working `defer` attribute that guarantees
order of execution. This means the WRM behaviour is no longer needed.

### What to remove

In JavaScript:

* `WRM.InOrderLoader`

In Java classes:

* `WebResourceIntegration#useAsyncAttributeForScripts`

### What to change

In any location where `WebResourceIntegration#useAsyncAttributeForScripts` was used, assume a value of `false`.

If you want to load assets without blocking the initial page render, use the new feature phases capability.

## `WRM.require` changes

### Returned value

The `WRM.require` method returns a `Promise` object instead of `jQuery.Deferred`.

Usage of this API should be updated to `WRM.require(...).then(onLoadedCallback, onFailedCallback)`.

In order to provide limited backward compatibility, the promise object returned has been augmented
with some `jQuery.Deferred` methods: `#done`, `#fail`, `#always`, `#state`, and `#progress`.

Note that `Promise#then` chains its return values, where most `jQuery.Deferred` methods did not.
To illustrate: if you call `WRM.require(...).done(c1).done(c2).done(c3)`, all three callbacks
will receive the exact same values. In contrast, when calling `WRM.require(...).then(c1).then(c2).then(c3)`,
the `c2` callback receives the return value of `c1`, and `c3` receives the return value of `c2`.

### Error handling

Any error in the process of retrieving resources will result in a rejected promise.

It is strongly encouraged to handle any failure cases: `WRM.require(...).catch(onErrorCallback)`.
Otherwise, `Unhandled promise rejection` errors will be thrown in failure cases.

### Order of execution

The `WRM.require` method is now always asynchronous, even if the resources requested were loaded previously.

This should make the behaviour of the API more predictable. However, it does mean there can be subtle
order of execution problems in code that assumed the resolution was synchronous.

You can ensure your code executes in the appropriate order by using the Promise returned -- either
via `WRM.require(...).then(doStuff);` or by `await WRM.require(...);`.

#### Queueing and retrieval behaviour

In 5.0.0, all resources requested in the same tick will be requested together.

The following example illustrates the difference to 4.x and below:

```js
WRM.require("a").then(() => WRM.require("1"));
WRM.require("b").then(() => WRM.require("2"));
WRM.require("c").then(() => WRM.require("3"));
```

In 4.x, there would be three requests:

1. `["a"]`
2. `["b", "c", "1"]`
3. `["2", "3"]`

In 5.x, these are made in a more predictable way over 2 requests:

1. `["a", "b", "c"]`
2. `["1", "2", "3"]`

While no explicit effort is required to adapt to this change, there is a small chance it will cause unexpected
behaviours when loading multiple resources.

## `WRM.curl` deprecation

The WRM used `curl.js` for two purposes: to define AMD modules for WRM's internals, and to load CSS + JS resource URLs
on to a page.

Now that WRM only supports modern browsers (no IE support), curl only adds unnecessary page weight and complexity.

The library has been moved to its own `com.atlassian.plugins.atlassian-plugins-webresource-rest:curl` web-resource
and deprecated. It will be removed in the next major version.

Direct use of `WRM.curl` is discouraged in all scenarios.

* All feature code should load via `web-resource` definitions.
* Loading resource URLs manually can cause unexpected and serious errors due to order of execution problems.

## Other

### What to remove

You will need to find and remove any of the following code from your products and plugins.

* In Java classes:
  * `Config#getStreamFor`
  * `Resource#getFilePath`

### What to change

In Java:

* Replace `WebResourceTransformer#transform(Element configElement, ResourceLocation location, String filePath, DownloadableResource nextResource)` is deprecated.
  Use `WebResourceTransformer#transform(Element configElement, ResourceLocation location, DownloadableResource nextResource)`
* Replace `TransformableResource(ResourceLocation location, String filePath, DownloadableResource nextResource)` is deprecated.
  Use `TransformableResource(ResourceLocation location, DownloadableResource nextResource)`

## Backend implementation changes

### Writing data

The order that the WRM outputs data to the DOM is not guaranteed. The `WRM._unparsedData` and `WRM._unparsedErrors` are
internal implementation details and should never be used directly. If, for any reason, you have any regular expressions
or string scraping for WRM's `<script>` tags, they will break.

### Escaping data

Single- and double-quotes in JSON strings will use unicode points instead of literal characters. Instead of generating
something like `<script>var foo="\"bar\"";</script>`, the WRM will now generate
`<script>var foo="\u0022bar\u0022";</script>`.


[wrm-webpack]: https://bitbucket.org/atlassianlabs/atlassian-webresource-webpack-plugin
