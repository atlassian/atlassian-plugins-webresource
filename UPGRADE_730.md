# For everyone

## I18n getText (JS) returns empty strings

Anything that relied upon the I18n key being returned will now need to be modified.
- If another I18n system is in use then removing the call to `#getText` is enough. Please schedule a migration to this system though, I18n systems other than this are unsupported.
- In the more complicated cases, JavaScript like below should work:

```javascript
const translatedString = getText('some.i18n.key');
return translatedString === '' ? 'some.i18n.key' : translatedString;
```

# For host application engineers

## Provided Spring wiring

Please move to using the provided Spring wiring. Wiring the WRM manually is now deprecated.

1. Inspect `WebResourceManagerWiring` to see what differences exist to the wiring you already have
2. Extend `WebResourceManagerWiring` in a new class annotated with `@Configuration`
3. Override methods to change bean wiring, remember to add the `@Override` annotation
4. Remove existing wiring
5. Make a new bean of the new class

More details here: https://hello.atlassian.net/wiki/spaces/DCCore/pages/4163242445/RFC+Provided+wiring+for+DC+libraries
