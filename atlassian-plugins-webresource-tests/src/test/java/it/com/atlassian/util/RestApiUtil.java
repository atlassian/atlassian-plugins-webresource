package it.com.atlassian.util;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

import static io.restassured.http.ContentType.JSON;
import static it.com.atlassian.rest.util.CommonRestTestConstants.getAdminPassword;
import static it.com.atlassian.rest.util.CommonRestTestConstants.getAdminUser;

public final class RestApiUtil {

    private RestApiUtil() {}

    public static RequestSpecification adminJsonRequest() {
        return RestAssured.given()
                .auth()
                .preemptive()
                .basic(getAdminUser(), getAdminPassword())
                .contentType(JSON);
    }
}
