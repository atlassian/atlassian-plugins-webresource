package it.com.atlassian.util;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import it.com.atlassian.rest.util.CommonRestTestConstants;

import static it.com.atlassian.util.RestApiUtil.adminJsonRequest;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;

public class EnableBatchingRule extends TestWatcher {
    private boolean batchingWasEnabledBefore = false;

    @Override
    protected void starting(Description description) {
        batchingWasEnabledBefore = adminJsonRequest()
                .when()
                .get(CommonRestTestConstants.getRestBaseUrl() + "/qr/batching")
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getBoolean("batchingEnabled");

        // Enable batching
        adminJsonRequest()
                .when()
                .put(CommonRestTestConstants.getRestBaseUrl() + "/qr/batching")
                .then()
                .statusCode(200);
    }

    @Override
    protected void finished(Description description) {
        if (batchingWasEnabledBefore) {
            return;
        }

        // Disable batching
        adminJsonRequest()
                .when()
                .delete(CommonRestTestConstants.getRestBaseUrl() + "/qr/batching")
                .then()
                .statusCode(anyOf(is(200), is(204)));
    }
}
