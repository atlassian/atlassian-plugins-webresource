package it.com.atlassian.util;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import static it.com.atlassian.util.RunningProductUtil.getRunningProductInstanceId;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assume.assumeThat;

public class HasAnalyticsImplementedRule implements TestRule {

    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            public void evaluate() {
                assumeThat(
                        getRunningProductInstanceId(),
                        both(not(equalTo("refapp"))).and(is(not(emptyOrNullString()))));
            }
        };
    }
}
