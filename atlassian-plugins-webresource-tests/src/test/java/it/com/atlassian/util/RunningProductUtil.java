package it.com.atlassian.util;

import static io.restassured.http.ContentType.JSON;
import static it.com.atlassian.rest.util.CommonRestTestConstants.getRestBaseUrl;
import static it.com.atlassian.util.RestApiUtil.adminJsonRequest;

public final class RunningProductUtil {

    private RunningProductUtil() {}

    public static String getRunningProductInstanceId() {
        return adminJsonRequest()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .contentType(JSON)
                .when()
                .get(getQuickreloadUrl("/systemproperties"))
                .jsonPath()
                .getMap("properties")
                .getOrDefault("product", "")
                .toString();
    }

    private static String getQuickreloadUrl(String path) {
        return getRestBaseUrl() + "/qr" + path;
    }
}
