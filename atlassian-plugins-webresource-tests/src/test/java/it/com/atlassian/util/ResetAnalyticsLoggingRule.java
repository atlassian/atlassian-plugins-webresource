package it.com.atlassian.util;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import static it.com.atlassian.util.AnalyticsRestApiUtil.clearAnalyticsLogs;
import static it.com.atlassian.util.AnalyticsRestApiUtil.getIsAnalyticsLoggingEnabled;
import static it.com.atlassian.util.AnalyticsRestApiUtil.toggleAnalyticsLogging;

public class ResetAnalyticsLoggingRule extends TestWatcher {

    private boolean hasAnalyticsLoggingBeenEnabled;

    @Override
    protected void starting(Description description) {
        clearAnalyticsLogs();

        hasAnalyticsLoggingBeenEnabled = getIsAnalyticsLoggingEnabled();
        if (!hasAnalyticsLoggingBeenEnabled) {
            toggleAnalyticsLogging(true);
        }
    }

    @Override
    protected void finished(Description description) {
        if (!hasAnalyticsLoggingBeenEnabled) {
            toggleAnalyticsLogging(false);
        }
    }
}
