package it.com.atlassian.util;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Catalog containing all the possible configuration properties used by the integration tests.
 *
 * @since 5.0.0
 */
public enum ConfigurationProperty {
    REFAPP_SYNC_BATCHING_ON_KEY("refapp.sync.batching.on"),
    REFAPP_SUPER_BATCHING_ON_KEY("refapp.super.batching.on"),
    REFAPP_CONTEXT_BATCHING_ON_KEY("refapp.context.batching.on"),
    REFAPP_WEBRESOURCE_BATCHING_ON_KEY("refapp.webresource.batching.on"),
    WEBRESOURCE_BATCHING_OFF_KEY("plugin.webresource.batching.off");

    private final String key;

    ConfigurationProperty(@Nonnull final String key) {
        this.key = requireNonNull(key, "The key is mandatory for the property.");
    }
}
