package it.com.atlassian.util;

import java.util.Collection;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.lang.NonNull;
import org.intellij.lang.annotations.Language;
import org.junit.rules.TestRule;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.atlassian.bamboo.pageobjects.BambooTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.DashboardPage;
import com.atlassian.crowd.pageobjects.CrowdConsole;
import com.atlassian.crowd.pageobjects.CrowdLoginPage;
import com.atlassian.crowd.pageobjects.CrowdTestedProduct;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.JiraAdminHomePage;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.elements.query.AbstractTimedQuery;
import com.atlassian.pageobjects.elements.query.ExpirationHandler;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.bitbucket.BitbucketTestedProduct;
import com.atlassian.webdriver.bitbucket.page.BitbucketHomePage;
import com.atlassian.webdriver.bitbucket.page.BitbucketLoginPage;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;

import static com.atlassian.pageobjects.TestedProductFactory.create;
import static com.atlassian.pageobjects.elements.query.ExpirationHandler.RETURN_CURRENT;
import static com.atlassian.pageobjects.elements.timeout.TimeoutType.PAGE_LOAD;

/**
 * Wraps all the stuff tying these E2E tests to atlassian-webdriver.
 */
public final class WebDriverTestUtil {

    public static final RefappTestedProduct REFAPP = create(RefappTestedProduct.class);
    public static final JiraTestedProduct JIRA = create(JiraTestedProduct.class, "jira", null);
    public static final ConfluenceTestedProduct CONFLUENCE = create(ConfluenceTestedProduct.class, "confluence", null);
    public static final BambooTestedProduct BAMBOO = create(BambooTestedProduct.class, "bamboo", null);
    public static final CrowdTestedProduct CROWD = create(CrowdTestedProduct.class, "crowd", null);
    public static final BitbucketTestedProduct BITBUCKET = create(BitbucketTestedProduct.class, "bitbucket", null);
    private static final String PRODUCT = System.getProperty("product");
    private static boolean loggedIn = false;

    private WebDriverTestUtil() {}

    @Nullable
    public static Object executeAsyncScript(@Nonnull final String script) {
        requireNonNull(script, "The async script to be executed is mandatory.");
        final JavascriptExecutor jsExecutor = (JavascriptExecutor) getDriver(getTestedProduct());
        return jsExecutor.executeAsyncScript(script);
    }

    @Nullable
    public static Object executeScript(@Nonnull final String script) {
        requireNonNull(script, "The script to be executed is mandatory.");
        final JavascriptExecutor jsExecutor = (JavascriptExecutor) getDriver(getTestedProduct());
        return jsExecutor.executeScript(script);
    }

    @Nonnull
    public static TestRule getScreenShotsAndHtmlOnTestFailure() {
        return new WebDriverScreenshotRule(getDriver(getTestedProduct()));
    }

    @Nullable
    public static List<String> findConsoleInfoMessages() {
        return ((List<Object>) executeScript("return console.logs"))
                .stream().map(String::valueOf).collect(toList());
    }

    @Nullable
    public static List<String> findConsoleWarnMessages() {
        return (List<String>) executeScript("return console.warnings");
    }

    @Nullable
    public static List<String> findConsoleErrors() {
        return (List<String>) executeScript("return console.errors");
    }

    @Nullable
    public static List<String> findConsoleErrorMessages() {
        return ((List<Object>) executeScript("return console.errors"))
                .stream().map(String::valueOf).collect(toList());
    }

    /**
     * Assert there is no errors in console
     */
    public static void assertNoConsoleErrors() {
        final List<String> actualErrors = findConsoleErrors();
        assertThat(
                "No javascript errors should be thrown or error messages logged",
                actualErrors,
                anyOf(
                        emptyCollectionOf(String.class),
                        nullValue(),
                        both(hasSize(1)).and(contains(equalTo("null")))));
    }

    @Nullable
    public static WebElement findElement(@Nonnull final By selector) {
        requireNonNull(selector, "The selector is mandatory.");
        return getDriver(getTestedProduct()).findElement(selector);
    }

    @Nullable
    public static List<WebElement> findElements(@Nonnull final By selector) {
        requireNonNull(selector, "The selector is mandatory.");
        return getDriver(getTestedProduct()).findElements(selector);
    }

    public static boolean waitForElement(By selector) {
        final AbstractTimedQuery<Boolean> getElementLoaded =
                new AbstractTimedQuery(
                        new DefaultTimeouts().timeoutFor(TimeoutType.PAGE_LOAD),
                        Timeouts.DEFAULT_INTERVAL,
                        ExpirationHandler.RETURN_CURRENT) {
                    @Override
                    protected boolean shouldReturn(Object currentEval) {
                        return currentEval instanceof Boolean && ((Boolean) currentEval);
                    }

                    @Override
                    protected Boolean currentValue() {
                        try {
                            WebElement el = findElement(selector);
                            return el != null; // found it.
                        } catch (RuntimeException e) {
                            // swallow NoSuchElementException.
                            return false;
                        }
                    }
                };

        return getElementLoaded.byDefaultTimeout();
    }

    /**
     * Waits for the page to be loaded and for an element with the id "end-of-test" to be present before asserting
     */
    public static void waitForPageToFinishLoading() {
        @Language("JavaScript")
        final String waitForPageLoad =
                "const callback = arguments[arguments.length - 1];" + "if (document.readyState === 'complete') {"
                        + "    callback()"
                        + "} else {"
                        + "    document.addEventListener('load', () => callback())"
                        + "}";
        executeAsyncScript(waitForPageLoad);
        // It's just easier than mucking around with AMD and running async scripts with WebDriver
        waitForElement(By.id("end-of-test"));
    }

    @NonNull
    public static Collection<String> getLoadedScriptsLog(final int numberOfExpectedScripts) {
        return new NumberOfExpectedScriptsQuery(numberOfExpectedScripts).byDefaultTimeout();
    }

    public static void navigate(@Nonnull final String basePath, @Nonnull final String servlet) {
        requireNonNull(basePath, "The base path is mandatory.");
        requireNonNull(servlet, "The servlet name is mandatory.");

        if (PRODUCT != null && !PRODUCT.equals("refapp") && !loggedIn) {
            login();
        }

        getDriver(getTestedProduct()).navigate().to(buildUrl(getTestedProduct(), basePath, servlet));
    }

    private static void login() {
        switch (PRODUCT) {
            case "confluence" -> {
                if (!CONFLUENCE.gotoLoginPage().getHeader().isLoggedIn()) {
                    CONFLUENCE.gotoLoginPage().loginAsSysAdmin(DashboardPage.class);
                    // Wait for Confluence to complete the login action before proceeding
                    waitForElement(By.cssSelector("title:contains('Dashboard - Confluence')"));
                    loggedIn = true;
                }
            }
            case "bamboo" -> {
                if (!BAMBOO.gotoLoginPage().getHeader().isLoggedIn()) {
                    BAMBOO.gotoLoginPage().loginAsSysAdmin();
                    loggedIn = true;
                }
            }
            case "jira" -> {
                JIRA.gotoLoginPage().loginAsSysAdmin(JiraAdminHomePage.class);
                loggedIn = true;
            }
            case "crowd" -> {
                CROWD.visit(CrowdLoginPage.class).loginAsSysAdmin(CrowdConsole.class);
                loggedIn = true;
            }
            case "bitbucket" -> {
                BITBUCKET.visit(BitbucketLoginPage.class).loginAsSysAdmin(BitbucketHomePage.class);
                loggedIn = true;
            }
            default -> throw new IllegalStateException("Unsupported 'product' value: " + PRODUCT);
        }
    }

    private static TestedProduct<WebDriverTester> getTestedProduct() {
        if (PRODUCT == null) {
            return REFAPP;
        }
        return switch (PRODUCT) {
            case "confluence" -> CONFLUENCE;
            case "bamboo" -> BAMBOO;
            case "jira" -> JIRA;
            case "crowd" -> CROWD;
            case "bitbucket" -> BITBUCKET;
            case "refapp" -> REFAPP;
            default -> throw new IllegalStateException("Unsupported 'product' value: " + PRODUCT);
        };
    }

    private static WebDriver getDriver(final TestedProduct<WebDriverTester> product) {
        return product.getTester().getDriver();
    }

    private static String buildUrl(
            final TestedProduct<WebDriverTester> product, final String basePath, final String servlet) {
        return product.getProductInstance().getBaseUrl() + basePath + servlet;
    }

    private static class NumberOfExpectedScriptsQuery extends AbstractTimedQuery<Collection<String>> {
        private final int numberOfExpectedScripts;

        private NumberOfExpectedScriptsQuery(final int numberOfExpectedScripts) {
            super(new DefaultTimeouts().timeoutFor(PAGE_LOAD), Timeouts.DEFAULT_INTERVAL, RETURN_CURRENT);
            this.numberOfExpectedScripts = numberOfExpectedScripts;
        }

        @Override
        protected boolean shouldReturn(final Collection<String> currentEval) {
            return currentEval.size() >= numberOfExpectedScripts;
        }

        @Override
        protected Collection<String> currentValue() {
            @Language("JavaScript")
            final String getLoadOrder = "return scriptLoadOrder = window.scriptLoadOrder || null";
            final Object currentEval = executeScript(getLoadOrder);
            return currentEval instanceof Collection ? (Collection<String>) currentEval : emptyList();
        }
    }
}
