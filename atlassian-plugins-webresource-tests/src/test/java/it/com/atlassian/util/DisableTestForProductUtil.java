package it.com.atlassian.util;

import static org.junit.Assume.assumeFalse;

public class DisableTestForProductUtil {

    private static final String product = System.getProperty("product");

    public static void disableTestForProduct(String productName) {
        assumeFalse(productName.equals(product));
    }
}
