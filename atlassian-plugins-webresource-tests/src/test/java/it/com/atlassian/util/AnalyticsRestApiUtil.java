package it.com.atlassian.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.restassured.filter.log.LogDetail;

import it.com.atlassian.rest.util.CommonRestTestConstants;

import static it.com.atlassian.util.RestApiUtil.adminJsonRequest;

public class AnalyticsRestApiUtil {

    public static final String ANALYTICS_REPORT_URL = "/analytics/1.0/report";

    private static final String PROCESSED_EVENTS_MODE = "btf_processed";

    private AnalyticsRestApiUtil() {}

    public static String getAnalyticsReportBaseUrl() {
        return CommonRestTestConstants.getRestBaseUrl() + ANALYTICS_REPORT_URL;
    }

    public static String getAnalyticsProcessedLogsUrl() {
        return getAnalyticsReportBaseUrl() + String.format("?mode=%s", PROCESSED_EVENTS_MODE);
    }

    public static List<Map<String, String>> getAnalyticsEvents() {
        return adminJsonRequest()
                .when()
                .get(getAnalyticsProcessedLogsUrl())
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getList("events");
    }

    public static boolean getIsAnalyticsLoggingEnabled() {
        return adminJsonRequest()
                .when()
                .get(getAnalyticsProcessedLogsUrl())
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getBoolean("capturing");
    }

    public static void toggleAnalyticsLogging(boolean state) {
        Map<String, String> body = new HashMap<>();
        body.put("capturing", Boolean.toString(state));

        adminJsonRequest()
                .body(body)
                .when()
                .put(getAnalyticsReportBaseUrl())
                .then()
                .log()
                .ifValidationFails(LogDetail.BODY)
                .statusCode(200);
    }

    public static void clearAnalyticsLogs() {
        adminJsonRequest().when().delete(getAnalyticsReportBaseUrl()).then().statusCode(200);
    }
}
