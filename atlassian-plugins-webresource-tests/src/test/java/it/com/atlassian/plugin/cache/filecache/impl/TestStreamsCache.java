package it.com.atlassian.plugin.cache.filecache.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.catalina.connector.ClientAbortException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.atlassian.plugin.cache.filecache.impl.StreamsCache;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(IOUtils.class)
public class TestStreamsCache {

    @Test
    public void testClientAbortExceptionSwallowed() throws IOException {
        File file = File.createTempFile("TestCachedFile", "test");
        OutputStream out = mock(OutputStream.class);

        PowerMockito.mockStatic(IOUtils.class);
        when(IOUtils.copyLarge(any(InputStream.class), any(OutputStream.class))).thenThrow(ClientAbortException.class);

        try {
            StreamsCache.streamFromFile(file, out);
        } catch (RuntimeException e) {
            assertThat(e.getMessage(), not(containsString("ClientAbortException")));
            fail("Client Abort Exception should be swallowed.");
        }
    }
}
