package it.com.atlassian.webresource.i18n;

import java.util.Collection;
import java.util.List;

import org.springframework.lang.NonNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import com.fasterxml.jackson.core.type.TypeReference;

import it.com.atlassian.util.DisableBatchingRule;
import it.com.atlassian.util.WebDriverTestUtil;

import static it.com.atlassian.util.JSONUtil.fileToEntity;
import static it.com.atlassian.util.WebDriverTestUtil.assertNoConsoleErrors;
import static it.com.atlassian.util.WebDriverTestUtil.findConsoleInfoMessages;
import static it.com.atlassian.util.WebDriverTestUtil.getScreenShotsAndHtmlOnTestFailure;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInRelativeOrder;

/**
 * <p>
 * Asserts that various server-side and client-side calls to the WRM requiring i18n files results in files with right content.
 * </p>
 * <ul>
 *   <li>
 *     Production assets for the tests are stored in
 *     <a href="filepath:../../../../../../main/resources">src/main/resources</a>.
 *   </li>
 *   <li>
 *     Expected values are stored in JSON files at
 *     <a href="filepath:../../../../../resources/test-cases/wrm-require-calls">test/resources/test-cases/i18n</a>
 *   </li>
 * </ul>
 * <p>
 *   Each test is run using both Soy and Velocity, so that we demonstrate the APIs are callable from
 *   supported templating languages.
 * </p>
 */
@RunWith(Parameterized.class)
public class InternationalizationTest {
    @Rule
    public DisableBatchingRule disableBatchingRule = new DisableBatchingRule();

    private final String servletBaseUrl;
    private final String configurationFilePath;
    private String currentProduct;

    public InternationalizationTest(@NonNull final String servletBaseUrl, @NonNull final String configurationFilePath) {
        this.servletBaseUrl = requireNonNull(servletBaseUrl, "The servlet base url is mandatory.");
        this.configurationFilePath = requireNonNull(configurationFilePath, "The configuration file path is mandatory.");
    }

    @Before
    public void setup() {
        this.currentProduct = System.getProperty("product");
    }

    @Rule
    public TestRule screenShotsAndHtmlOnTestFailure = getScreenShotsAndHtmlOnTestFailure();

    /**
     * Configure all the possible values used for the tests.
     *
     * @return The collection containing the parameter for the {@link InternationalizationTest} constructor.
     */
    @Parameters(name = "servletBaseUrl: {0}")
    public static Collection<Object[]> configure() {
        return asList(new Object[][] {
            {"/plugins/servlet/resources/soy", "/test-cases/i18n/soy"},
            {"/plugins/servlet/resources/velocity", "/test-cases/i18n/velocity"}
        });
    }

    @Test
    public void
            when_LoadScriptsWithI18nEnabled_And_ThereIsTranslationForKeys_Then_ShouldReturnTranslatedValues_And_NoErrors() {
        // when
        navigate("/i18n");
        // then
        testInternationalizedMessages("/when-load-scripts-with-i18n-then-no-errors/expected.json");
    }

    /**
     * Builds the configuration file full path based on a base path.
     *
     * @param configurationFilePath The configuration file sub-path.
     * @return The final configuration file path.
     */
    private String buildConfigurationFilePath(@NonNull final String configurationFilePath) {
        requireNonNull(configurationFilePath, "The configuration file sub-path is mandatory");
        return this.configurationFilePath + configurationFilePath;
    }

    /**
     * Perform the navigation to the page according to the defined path.
     *
     * @param pagePath The path of the page to be visited.
     */
    private void navigate(final String pagePath) {
        requireNonNull(pagePath, "The page path is mandatory");
        WebDriverTestUtil.navigate(servletBaseUrl, pagePath);
    }

    /**
     * Tests if the expected messages were shown in the console.
     *
     * @param expectedFilePath The expected file configuration.
     */
    private void testInternationalizedMessages(final String expectedFilePath) {
        requireNonNull(expectedFilePath, "The expected file path is mandatory");

        assertNoConsoleErrors();

        final String configurationFileFullPath = buildConfigurationFilePath(expectedFilePath);
        final List<String> expectedInfoMessage = fileToEntity(configurationFileFullPath, new ListTypeReference());
        final List<String> actualInfoMessages = findConsoleInfoMessages();
        assertThat(actualInfoMessages, containsInRelativeOrder(expectedInfoMessage.toArray()));
    }

    private static class ListTypeReference extends TypeReference<List<String>> {}
}
