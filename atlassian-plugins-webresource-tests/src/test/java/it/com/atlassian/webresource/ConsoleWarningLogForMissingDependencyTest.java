package it.com.atlassian.webresource;

import java.util.List;

import org.junit.Rule;
import org.junit.Test;

import it.com.atlassian.util.DisableBatchingRule;
import it.com.atlassian.util.WebDriverTestUtil;

import static it.com.atlassian.util.DisableTestForProductUtil.disableTestForProduct;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertNotNull;

public class ConsoleWarningLogForMissingDependencyTest {
    @Rule
    public DisableBatchingRule disableBatchingRule = new DisableBatchingRule();

    private static final String servletBaseUrl = "/plugins/servlet/resources/velocity";
    private static final String consoleLogWarning =
            "The I18n web-resource is missing, please add com.atlassian.plugins.atlassian-plugins-webresource-plugin:i18n as a dependency to the web-resource. Learn more: https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin-javascript";

    @Test
    public void givenWebResourceHasI18nDependency_whenWebResourceIsLoaded_thenWarningMessageForI18IsNotLogged() {
        // BSP-6537: Temporarily disabled for Confluence until fix missing I18n dependency in confluence-baseurl-plugin
        disableTestForProduct("confluence");
        // DCA11Y-1605: Temporarily disabled for Jira until fix problem with missing AUI wrm dependency on I18n
        disableTestForProduct("jira");

        navigate("/withDependencies");

        List<String> consoleInfoMessages = WebDriverTestUtil.findConsoleWarnMessages();

        assertThat(consoleInfoMessages, not(hasItem(consoleLogWarning)));
    }

    @Test
    public void
            givenWebResourceDoesNotDeclareI18nDependency_whenWebResourceIsLoaded_thenWarningMessageForI18IsLogged() {
        // BSP-6520: Temporarily disabled for Crowd due to 'atlassian.webresource.twophase.js.i18n' property
        // currently being force-disabled in Crowd.
        disableTestForProduct("crowd");

        navigate("/withoutDependencies");

        List<String> consoleInfoMessages = WebDriverTestUtil.findConsoleWarnMessages();

        assertNotNull(consoleInfoMessages);
        assertThat(consoleInfoMessages, hasItem(consoleLogWarning));
    }

    private void navigate(final String pagePath) {
        requireNonNull(pagePath, "The page path is mandatory");
        WebDriverTestUtil.navigate(servletBaseUrl, pagePath);
    }
}
