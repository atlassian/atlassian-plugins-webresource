package it.com.atlassian.webresource.i18n;

import java.util.Collection;
import javax.annotation.Nonnull;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import com.google.common.collect.Lists;

import it.com.atlassian.util.DisableBatchingRule;

import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static it.com.atlassian.util.DisableTestForProductUtil.disableTestForProduct;
import static it.com.atlassian.util.WebDriverTestUtil.getLoadedScriptsLog;
import static it.com.atlassian.util.WebDriverTestUtil.getScreenShotsAndHtmlOnTestFailure;
import static it.com.atlassian.util.WebDriverTestUtil.navigate;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInRelativeOrder;

/**
 * Tests whether the i18n transformation will pick up a set of compiled i18n usages.
 * Note that WRM's refapp integration tests project explicitly disables the AMPS step that would
 * typically compile these values via {@code <processI18nUsage>false</processI18nUsage>}.
 * We do that so the "runtime" code doesn't get analyzed and compiled, too ;)
 */
@RunWith(Parameterized.class)
public class WrmI18nTest {
    @Rule
    public DisableBatchingRule disableBatchingRule = new DisableBatchingRule();

    private static final String PLUGIN_ID = "com.atlassian.plugins.atlassian-plugins-webresource-tests";
    private final String phaseAsString;

    public WrmI18nTest(final String phaseAsString) {
        this.phaseAsString = phaseAsString;
    }

    @Rule
    public TestRule screenShotsAndHtmlOnTestFailure = getScreenShotsAndHtmlOnTestFailure();

    @Nonnull
    @Parameters(name = "in phase: {0}")
    public static Collection<String> testCaseNames() {
        return stream(ResourcePhase.values()).map(ResourcePhase::name).collect(toList());
    }

    @Test
    public void testCompiledI18n() {
        // BSP-6520: Temporarily disabled for Crowd due to 'atlassian.webresource.twophase.js.i18n' property
        // currently being force-disabled in Crowd.
        disableTestForProduct("crowd");

        final String WRM_KEY = PLUGIN_ID + ":atlassian-plugins-webresource-it-plugin-resources-i18n-compiled";
        navigate("/plugins/servlet/i18n/", WRM_KEY + "?phase=" + phaseAsString);
        final Collection<String> expectedOutput = Lists.newArrayList(
                "Contain keys: true", "Simple text:  Foo, bar, and baz", "Format text:  We have 10 issues");
        final Collection<String> actualOutput = getLoadedScriptsLog(3);
        assertThat(actualOutput, containsInRelativeOrder(expectedOutput.toArray()));
    }

    @Test
    public void testRuntimeI18n() {
        final String WRM_KEY = PLUGIN_ID + ":atlassian-plugins-webresource-it-plugin-resources-i18n-runtime";
        navigate("/plugins/servlet/i18n/", WRM_KEY + "?phase=" + phaseAsString);
        final Collection<String> expectedOutput = Lists.newArrayList(
                "Contain keys: false",
                "Simple text:  Foo, bar, and baz",
                "Format text:  We have 10 issues",
                "Another plain text:  Another string. It might be passed to WRM.format.");
        final Collection<String> actualOutput = getLoadedScriptsLog(4);
        assertThat(actualOutput, containsInRelativeOrder(expectedOutput.toArray()));
    }
}
