package it.com.atlassian.webresource;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import it.com.atlassian.util.EnableBatchingRule;

import static it.com.atlassian.util.DisableTestForProductUtil.disableTestForProduct;
import static it.com.atlassian.util.WebDriverTestUtil.findConsoleErrorMessages;
import static it.com.atlassian.util.WebDriverTestUtil.findConsoleErrors;
import static it.com.atlassian.util.WebDriverTestUtil.findConsoleInfoMessages;
import static it.com.atlassian.util.WebDriverTestUtil.getScreenShotsAndHtmlOnTestFailure;
import static it.com.atlassian.util.WebDriverTestUtil.navigate;
import static it.com.atlassian.util.WebDriverTestUtil.waitForPageToFinishLoading;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsInRelativeOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;

/**
 * These tests can be simplified by removing assertions once they are correctly tested in other integration tests, for
 * now they have to be checked to ensure these tests are valid (otherwise the tests could pass without the problem being
 * solved).
 */
public class ClientsideRacingTest {
    private static final String BASE_PATH = "/plugins/servlet/resources/velocity";

    @Rule
    public EnableBatchingRule enableBatching = new EnableBatchingRule();

    @Rule
    public TestRule screenShotsAndHtmlOnTestFailure = getScreenShotsAndHtmlOnTestFailure();

    @BeforeClass
    public static void setupAll() {
        // Tests are disabled for confluence and bitbucket because of issues related to loading amd-loader resource
        disableTestForProduct("confluence");
        disableTestForProduct("bitbucket");
    }

    @Test
    public void
            givenTheClientsideLoadedWhenReadyStateIsLoading_whenWebResourcesAreRequiredFromThroughTheClientside_thenAllTheServersideLoadedResourcesShouldBeExcluded() {
        navigate(BASE_PATH, "/clientside-racing-in-loading-state");

        waitForPageToFinishLoading();
        List<String> infoLogs = requireNonNull(findConsoleInfoMessages());

        assertThat(
                "Resources should NEVER be loaded twice",
                infoLogs,
                not(containsInRelativeOrder(
                        "serverside included resource executed", "serverside included resource executed")));
        assertThat(
                "Clientside resources should never be run before server-side ones because that implies"
                        + "that server-side included ones may not have been excluded -> double loading resources",
                infoLogs,
                not(containsInRelativeOrder("wrm/require resource executed", "serverside included resource executed")));

        // Check this is a valid test
        assertNoUnexpectedJsError();
        // Test other parts of the WRM contract that imply this is a valid test
        assertThat(
                "require is drained before requireLazily -- that's the whole point of the API",
                infoLogs,
                containsInRelativeOrder("wrm/require resource executed", "wrm/requireLazily resource executed"));
        // make sure we're testing the right thing
        assertThat(
                "this test is for the WRM clientside being loaded in the 'loading' document state",
                infoLogs,
                containsInRelativeOrder(
                        "[WRM] Client-side initialised during 'loading' state",
                        "document is now in interactive state"));
    }

    @Test
    public void
            givenTheClientsideLoadedWhenReadyStateIsInteractive_whenWebResourcesAreRequiredFromThroughTheClientside_thenAllTheServersideLoadedResourcesShouldBeExcluded() {
        navigate(BASE_PATH, "/clientside-racing-in-interactive-state");

        waitForPageToFinishLoading();
        List<String> infoLogs = requireNonNull(findConsoleInfoMessages());

        assertThat(
                "Resources should NEVER be loaded twice",
                infoLogs,
                not(containsInRelativeOrder(
                        "serverside included resource executed", "serverside included resource executed")));
        assertThat(
                "Clientside resources should never be run before server-side ones because that implies"
                        + "that server-side included ones may not have been excluded -> double loading resources",
                infoLogs,
                not(containsInRelativeOrder("wrm/require resource executed", "serverside included resource executed")));

        // Check this is a valid test
        assertNoUnexpectedJsError();
        // Test other parts of the WRM contract that imply this is a valid test
        assertThat(
                "require is drained before requireLazily -- that's the whole point of the API",
                infoLogs,
                containsInRelativeOrder("wrm/require resource executed", "wrm/requireLazily resource executed"));
        // make sure we're testing the right thing
        // "[WRM] Client-side initialised during 'interactive' state" - do not appear in the console for Jira
        if (!"jira".equals(System.getProperty("product"))) {
            assertThat(
                    "this test is for the WRM clientside being loaded in the 'interactive' document state",
                    infoLogs,
                    containsInRelativeOrder(
                            "document is now in interactive state",
                            "[WRM] Client-side initialised during 'interactive' state",
                            "DOMContentLoaded was fired"));
        }
    }

    @Test
    public void
            givenTheClientsideLoadedWhenReadyStateIsComplete_whenWebResourcesAreRequiredFromThroughTheClientside_thenAllTheServersideLoadedResourcesShouldBeExcluded() {
        navigate(BASE_PATH, "/clientside-racing-in-complete-state");

        waitForPageToFinishLoading();
        List<String> infoLogs = requireNonNull(findConsoleInfoMessages());

        assertThat(
                "Resources should NEVER be loaded twice",
                infoLogs,
                not(containsInRelativeOrder(
                        "serverside included resource executed", "serverside included resource executed")));
        assertThat(
                "Clientside resources should never be run before server-side ones because that implies"
                        + "that server-side included ones may not have been excluded -> double loading resources",
                infoLogs,
                not(containsInRelativeOrder("wrm/require resource executed", "serverside included resource executed")));

        // Check this is a valid test
        assertNoUnexpectedJsError();
        // Test other parts of the WRM contract that imply this is a valid test
        assertThat(
                "require is drained before requireLazily -- that's the whole point of the API",
                infoLogs,
                containsInRelativeOrder("wrm/require resource executed", "wrm/requireLazily resource executed"));
        // make sure we're testing the right thing
        assertThat(
                "this test is for the WRM clientside being loaded in the 'complete' document state",
                infoLogs,
                containsInRelativeOrder(
                        "document is now in complete state", "[WRM] Client-side initialised during 'complete' state"));
    }

    /**
     * What's in the DEFER server side inclusions could be excluded in the client side includes, so making all
     * clientside includes execute afterward is safe to uphold the WRM contract.
     * <b>This test will not reliably fail, it requires an accompanying unit test over the require handler</b>
     */
    @Test
    public void
            givenServerSideIncludesInDeferPhase_whenIncludingFromTheClientside_thenThereIsNoRacingAgainstTheServer() {
        navigate(BASE_PATH, "/clientside-racing-deferred-for-safety");

        waitForPageToFinishLoading();

        final List<String> actualErrors = findConsoleErrors();
        assertThat(
                "No JS errors should be thrown or error messages logged",
                actualErrors,
                emptyCollectionOf(String.class));

        final String[] expectedInfoMessage = new String[] {
            "blocking JS executes",
            "first deferred dependency executes -- consumer explicitly asked for it to be deferred",
            "second deferred dependency executes -- was deferred for safety"
        };
        final List<String> actualInfoMessages = findConsoleInfoMessages();
        assertThat(actualInfoMessages, containsInRelativeOrder(expectedInfoMessage));
    }

    private static void assertNoUnexpectedJsError() {
        if ("jira".equals(System.getProperty("product"))) {
            assertThat(
                    "No unexpected JS errors should be thrown or error messages logged",
                    findConsoleErrorMessages(),
                    everyItem(anyOf(
                            // Expected errors to be present in the console for Jira
                            containsString("WRM.I18n is undefined"),
                            containsString("AJS.namespace is not a function"),
                            containsString("goog is not defined"),
                            containsString("jira/flag missing aui/flag"),
                            containsString("Data with key 'com.atlassian.plugins.atlassian-plugins-"
                                    + "webresource-plugin:context-path.context-path' has already been claimed"))));
        } else {
            assertThat(
                    "No JS errors should be thrown or error messages logged",
                    findConsoleErrors(),
                    emptyCollectionOf(String.class));
        }
    }
}
