package it.com.atlassian.webresource;

import java.util.Collection;
import java.util.List;

import org.springframework.lang.NonNull;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import com.fasterxml.jackson.core.type.TypeReference;

import it.com.atlassian.util.DisableBatchingRule;
import it.com.atlassian.util.WebDriverTestUtil;

import static it.com.atlassian.util.JSONUtil.fileToEntity;
import static it.com.atlassian.util.WebDriverTestUtil.assertNoConsoleErrors;
import static it.com.atlassian.util.WebDriverTestUtil.getLoadedScriptsLog;
import static it.com.atlassian.util.WebDriverTestUtil.getScreenShotsAndHtmlOnTestFailure;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

/**
 * <p>
 * Asserts that various server-side and client-side calls to the WRM result in
 * resources being loaded in an expected order, at an expected time, and with expected properties.
 * </p>
 * <ul>
 *   <li>
 *     Production assets for the tests are stored in
 *     <a href="filepath:../../../../../../main/resources">src/main/resources</a>.
 *   </li>
 *   <li>
 *     Expected values are stored in JSON files at
 *     <a href="filepath:../../../../../resources/test-cases/wrm-require-calls">test/resources/test-cases/wrm-require-calls</a>
 *   </li>
 * </ul>
 * <p>
 *   Each test is run using both Soy and Velocity, so that we demonstrate the APIs are callable from
 *   supported templating languages.
 * </p>
 */
@RunWith(Parameterized.class)
public class WrmRequireCallsTest {
    @Rule
    public DisableBatchingRule disableBatchingRule = new DisableBatchingRule();

    @Rule
    public TestRule screenShotsAndHtmlOnTestFailure = getScreenShotsAndHtmlOnTestFailure();

    private final String servletBaseUrl;
    private final String configurationFilePath;

    public WrmRequireCallsTest(@NonNull final String servletBaseUrl, @NonNull final String configurationFilePath) {
        this.servletBaseUrl = requireNonNull(servletBaseUrl, "The servlet base url is mandatory.");
        this.configurationFilePath = requireNonNull(configurationFilePath, "The configuration file path is mandatory.");
    }

    /**
     * Configure all the possible values used for the tests.
     * @return The collection containing the parameter for the {@link WrmRequireCallsTest} constructor.
     */
    @Parameters(name = "servletBaseUrl: {0}, configurationFilePath: {1}")
    public static Collection<Object[]> configure() {
        return asList(new Object[][] {
            {"/plugins/servlet/resources/soy", "/test-cases/wrm-require-calls/soy"},
            {"/plugins/servlet/resources/velocity", "/test-cases/wrm-require-calls/velocity"}
        });
    }

    @Test
    public void when_LoadRequiredScriptsAfterPage_Then_ShouldLoadRequiredScriptsAfterPageScripts() {
        // when
        navigate("/wrmRequire");
        // then
        testDependencies("/wrm-require/expected.json");
    }

    @Test
    public void when_LoadRequireLazilyAfterRequire_Then_ShouldLoadInOrder() {
        // when
        navigate("/wrmRequireLazilyAfterRequire");
        // then
        testDependencies("/wrm-require-lazily-after-require/expected.json");
    }

    @Test
    public void when_LoadRequireLazilyAfterInline_Then_ShouldLoadInOrder() {
        // when
        navigate("/wrmRequireLazilyAfterInline");
        // then
        testDependencies("/wrm-require-lazily-after-inline/expected.json");
    }

    @Test
    public void given_conditionalDiamondDependencyGraph_when_allSidesTrue_and_requiringInline_Then_LeftLoadsInline() {
        // when
        navigate("/wrmRequireInline?left&right&bottom");
        // then
        testDependencies("/wrm-require-inline/expected.json");
    }

    @Test
    // PLUGWEB-631
    public void given_conditionalDiamondDependencyGraph_when_allSidesTrue_then_BottomLoadsEagerly() {
        // when
        navigate("/wrmConditionalDiamond?left=&right=&bottom=");
        // then
        testDependencies("/wrm-conditional-diamond/expected-left-right-bottom.json");
    }

    @Test
    // PLUGWEB-631
    public void given_conditionalDiamondDependencyGraph_when_leftFalse_then_BottomLoadsLazily() {
        // when
        navigate("/wrmConditionalDiamond?right=&bottom=");
        // then
        testDependencies("/wrm-conditional-diamond/expected-right-bottom.json");
    }

    /**
     * Builds the configuration file full path based on a base path.
     *
     * @param configurationFilePath The configuration file sub-path.
     * @return The final configuration file path.
     */
    private String buildConfigurationFilePath(@NonNull final String configurationFilePath) {
        requireNonNull(configurationFilePath, "The configuration file sub-path is mandatory");
        return this.configurationFilePath + configurationFilePath;
    }

    /**
     * Perform the navigation to the page according to the defined path.
     *
     * @param pagePath The path of the page to be visited.
     */
    private void navigate(final String pagePath) {
        requireNonNull(pagePath, "The page path is mandatory");
        WebDriverTestUtil.navigate(servletBaseUrl, pagePath);
    }

    /**
     * Tests if the expected dependencies where added correctly to the path.
     *
     * @param expectedFilePath The expected file configuration.
     */
    private void testDependencies(final String expectedFilePath) {
        requireNonNull(expectedFilePath, "The expected file path is mandatory");

        assertNoConsoleErrors();

        final String configurationFileFullPath = buildConfigurationFilePath(expectedFilePath);
        final List<String> expectedDependencies = fileToEntity(configurationFileFullPath, new ListTypeReference());
        final List<String> actualDependencies = (List<String>) getLoadedScriptsLog(expectedDependencies.size());
        assertThat(actualDependencies, contains(expectedDependencies.toArray()));
    }

    private static class ListTypeReference extends TypeReference<List<String>> {}
}
