package it.com.atlassian.webresource;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.hamcrest.Description;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import io.restassured.response.Response;

import it.com.atlassian.rest.util.AsyncTestUtils;
import it.com.atlassian.rest.util.WaitCondition;
import it.com.atlassian.util.DisableBatchingRule;

import static io.restassured.http.ContentType.JSON;
import static it.com.atlassian.rest.util.CommonRestTestConstants.getRestBaseUrl;
import static it.com.atlassian.util.RestApiUtil.adminJsonRequest;
import static it.com.atlassian.util.RunningProductUtil.getRunningProductInstanceId;
import static java.util.Collections.singletonList;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assume.assumeThat;

public class ProfilingMetricsTest {
    @Rule
    public DisableBatchingRule disableBatchingRule = new DisableBatchingRule();

    @Before
    public void setup() {
        // These tests only work in the refapp at the moment, as it's the only place where `atlassian-profiling`
        // has been configured to make the test work.
        assumeThat(getRunningProductInstanceId(), either(equalTo("refapp")).or(is(emptyOrNullString())));
    }

    @Test
    public void whenWebConditionsAreTriggered_thenJmxBeansShouldIncludeTheTimeTakenForConditionShouldDisplayLogic() {
        startConditionsSimulationOperation();

        AtomicReference<List<String>> emittedConditionBeans = new AtomicReference<>();

        waitForAssertionToBeTrue(() -> {
            final List<String> mBeanNames = getAllEmittedJmxBeans();
            emittedConditionBeans.set(getWebConditionsBeanNames(mBeanNames));
            // Wait for the beans and their count to be emitted. The count has a slight delay
            return emittedConditionBeans.get().size() >= 2
                    && getBeanCallCount(getDetailedJmxEntry(
                                    emittedConditionBeans.get().get(0)))
                            > 0;
        });

        assertThat(
                "Condition metrics bean have been emitted to JMX",
                emittedConditionBeans.get().size(),
                equalTo(2));

        emittedConditionBeans.get().forEach(bean -> {
            Response jmxDetails = getDetailedJmxEntry(bean);
            int callsCount = getBeanCallCount(jmxDetails);
            assertThat("Condition calls are counted", callsCount, greaterThan(0));
        });
    }

    private int getBeanCallCount(Response jmxDetails) {
        return Integer.parseInt(jmxDetails.jsonPath().get("Count"));
    }

    private Response getDetailedJmxEntry(String bean) {
        return adminJsonRequest()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .contentType(JSON)
                .when()
                .get(getNoisyNeighbourUrl("/jmx/" + bean));
    }

    private List<String> getAllEmittedJmxBeans() {
        return Arrays.asList(adminJsonRequest()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .contentType(JSON)
                .when()
                .get(getNoisyNeighbourUrl("/jmx"))
                .as(String[].class));
    }

    private void startConditionsSimulationOperation() {
        String operationsResourceUrl = getNoisyNeighbourUrl("/admin");

        final List<String> operationTask = singletonList("WRM_CONDITION");

        adminJsonRequest()
                .given()
                .body(operationTask)
                .contentType(JSON)
                .when()
                .post(operationsResourceUrl)
                .then()
                .log()
                .ifValidationFails()
                .statusCode(NO_CONTENT.getStatusCode());
    }

    private String getNoisyNeighbourUrl(String path) {
        return getRestBaseUrl() + "/noisyneighbour/latest" + path;
    }

    private void waitForAssertionToBeTrue(Supplier<Boolean> assertionFn) {
        int retryIntervalMs = 100;
        AsyncTestUtils.waitFor(
                new WaitCondition() {
                    @Override
                    public void describeFailure(Description description) {
                        description.appendText("\nExpected Web conditions beans to be emitted but wasn't");
                    }

                    @Override
                    public boolean test() {
                        return assertionFn.get();
                    }
                },
                10_000 + retryIntervalMs,
                retryIntervalMs);
    }

    @NotNull
    private List<String> getWebConditionsBeanNames(List<String> mBeanNames) {
        return mBeanNames.stream()
                // Match on the transform task metric
                .filter(n -> n.contains("category00=web,category01=resource,name=condition"))
                // Match it's from the noisy neighbour plugin' plugin
                .filter(n -> n.contains("com.atlassian.diagnostics.noisy-neighbour-plugin"))
                .collect(Collectors.toList());
    }
}
