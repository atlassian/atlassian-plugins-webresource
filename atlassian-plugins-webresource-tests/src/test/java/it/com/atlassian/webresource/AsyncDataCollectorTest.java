package it.com.atlassian.webresource;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;

import it.com.atlassian.util.HasAnalyticsImplementedRule;
import it.com.atlassian.util.ResetAnalyticsLoggingRule;
import it.com.atlassian.util.WebDriverTestUtil;

import static it.com.atlassian.util.AnalyticsRestApiUtil.getAnalyticsEvents;
import static it.com.atlassian.util.WebDriverTestUtil.assertNoConsoleErrors;
import static it.com.atlassian.util.WebDriverTestUtil.navigate;
import static org.junit.Assert.assertTrue;

public class AsyncDataCollectorTest {
    @Rule
    public TestRule chainAnalyticsRules =
            RuleChain.outerRule(new HasAnalyticsImplementedRule()).around(new ResetAnalyticsLoggingRule());

    private static final String SERVLET_BASE_URL = "/plugins/servlet/resources/velocity";

    /**
     * This checks that data-collector-async loads without errors and emits 'analytics' event
     * without the need to specify any dependencies explicitly.
     * Limitation: it's possible that other parallel test might trigger expected event.
     *
     * @link <a href="https://ecosystem.atlassian.net/browse/PLUGWEB-682">PLUGWEB-682</a>
     */
    @Test
    public void whenDataCollectorIsRequired_thenAnalyticsRestApiIsCalled() throws InterruptedException {
        // given
        final String DATA_COLLECTOR_ANALYTICS_EVENT = "wrm.caching.data.collector";

        // when
        navigate(SERVLET_BASE_URL, "/asyncDataCollectorResource");
        WebDriverTestUtil.waitForPageToFinishLoading();

        // then
        assertNoConsoleErrors();

        // Prevent flaky behaviour by waiting for the analytics event to be processed
        TimeUnit.MILLISECONDS.sleep(150);
        assertAnalyticsEventRecorded(DATA_COLLECTOR_ANALYTICS_EVENT);
    }

    private void assertAnalyticsEventRecorded(String name) {
        List<Map<String, String>> analyticsEvents = getAnalyticsEvents();
        boolean isEventInAnalyticsLogs = analyticsEvents.stream().anyMatch(analyticsEvent -> {
            if (analyticsEvent.containsKey("name")) {
                return name.equals(analyticsEvent.get("name"));
            }
            return false;
        });

        assertTrue("Async Data Collector analytics event is missing", isEventInAnalyticsLogs);
    }
}
