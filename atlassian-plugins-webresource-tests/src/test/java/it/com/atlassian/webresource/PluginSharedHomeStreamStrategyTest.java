package it.com.atlassian.webresource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import it.com.atlassian.util.DisableBatchingRule;

import static it.com.atlassian.rest.util.CommonRestTestConstants.getWebResourcePluginKey;
import static it.com.atlassian.rest.util.CommonRestTestConstants.getWebResourcePluginPrefix;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PluginSharedHomeStreamStrategyTest {
    @Rule
    public DisableBatchingRule disableBatchingRule = new DisableBatchingRule();

    private static final String ENDPOINT =
            String.join("/", "http://127.0.0.1:5990/refapp", "s", "123", "_", "download");
    private static final String WEB_RESOURCE_KEY = getWebResourcePluginPrefix() + "resource-resolved-from-shared-home";
    private static final String BATCH_URL =
            String.join("/", ENDPOINT, "batch", WEB_RESOURCE_KEY, WEB_RESOURCE_KEY + ".js");

    private static final String PRODUCT = System.getProperty("product");
    private static final Path USER_DIR = Paths.get(System.getProperty("user.dir"));
    private static final Map<String, Path> PRODUCTS_SHARED_HOME = Map.of(
            "bamboo", USER_DIR.resolve("target/bamboo/home/shared"),
            "bitbucket", USER_DIR.resolve("target/bitbucket/home/shared"),
            "confluence", USER_DIR.resolve("target/confluence/home"),
            "crowd", USER_DIR.resolve("target/crowd/home"),
            "jira", USER_DIR.resolve("target/jira/home"),
            "refapp", USER_DIR.resolve("target/refapp/home"));
    private static final Path PLUGIN_SHARED_HOME_PATH = resolvePluginSharedHomePath();

    @Before
    public void setUp() {}

    @After
    public void tearDown() {
        removeResources();
    }

    @Test
    public void given_resourceResolvedFromSharedHomeIsRequested_when_resourceInstalled_than_itsDownloaded()
            throws IOException {
        // given
        prepareResources();

        // when
        final Response response = RestAssured.given().when().get(BATCH_URL).andReturn();

        // then
        assertThat(response.statusCode(), equalTo(200));
        assertTrue(response.getBody().print().contains("SHARED HOME RESOLUTION WORKED!"));
    }

    @Test
    public void given_resourceResolvedFromSharedHomeIsRequested_when_resourceIsNotAvailable_than_itsNotDownloaded() {
        // given
        removeResources();

        // when
        final Response response = RestAssured.given().when().get(BATCH_URL).andReturn();

        // then
        assertThat(response.statusCode(), equalTo(200));
        assertFalse(response.getBody().print().contains("SHARED HOME RESOLUTION WORKED!"));
    }

    private void prepareResources() throws IOException {
        Files.createDirectories(PLUGIN_SHARED_HOME_PATH);

        Files.copy(
                Paths.get("src/test/resources/js/check-shared-home-resolution.js"),
                PLUGIN_SHARED_HOME_PATH.resolve("check-shared-home-resolution.js"),
                StandardCopyOption.REPLACE_EXISTING);
    }

    private void removeResources() {
        try {
            Files.delete(PLUGIN_SHARED_HOME_PATH.resolve("check-shared-home-resolution.js"));
        } catch (IOException e) {
            // It's fine... (really - we might have deleted it in one of the tests ;) )
        }
    }

    private static Path resolvePluginSharedHomePath() {
        Path basePath = PRODUCT != null ? PRODUCTS_SHARED_HOME.get(PRODUCT) : PRODUCTS_SHARED_HOME.get("refapp");
        return basePath.resolve("web-resources").resolve(getWebResourcePluginKey());
    }
}
