package it.com.atlassian.rest.router;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import it.com.atlassian.util.DisableBatchingRule;

import static it.com.atlassian.rest.util.CommonRestTestConstants.getWebResourcePluginPrefix;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import static com.atlassian.plugin.webresource.ResourceUtils.STATIC_HASH;

public class ResourceUrlTest {
    @Rule
    public DisableBatchingRule disableBatchingRule = new DisableBatchingRule();

    private static final String ENDPOINT =
            String.join("/", "http://127.0.0.1:5990/refapp", "s", "123", "_", "download");
    private static final String WEB_RESOURCE_KEY =
            getWebResourcePluginPrefix() + "atlassian-plugins-webresource-it-plugin-resources-42";
    private static final String BATCH_URL =
            String.join("/", ENDPOINT, "batch", WEB_RESOURCE_KEY, WEB_RESOURCE_KEY + ".js");

    @Test
    public void given_ifModifiedSinceNotSet_when_requestBatchFile_then_returnsContent() {
        // when
        final Response response = RestAssured.given()
                .when()
                .get(BATCH_URL + String.format("?%s=%s", STATIC_HASH, "123"))
                .andReturn();

        // then
        assertThat(response.statusCode(), equalTo(200));
        assertThat(response.getBody().print(), not(emptyString()));
    }

    @Test
    @Ignore(
            "PLUGWEB-657 - Setting If-Modified-Since is insufficient info to decide whether to serve HTTP 200 or 304. Our behaviour is insufficient right now."
                    + "While it won't be possible to affect the batch file's last modified date from an integration test, we do need to "
                    + "(1) assume or determine the time it was generated,"
                    + "(2) write a test where if-modified-since is set earlier than that date (should serve 200), and"
                    + "(3) write a test where if-modified-since is after that date (should serve 304)"
                    + "Also keep in mind that ETag and If-None-Match are more important.")
    public void given_ifModifiedSinceValueValidDate_when_requestBatchFile_then_INSERT_VALID_BEHAVIOUR_HERE() {
        // given
        final String ifModifiedSince = "Wed, 21 Oct 2015 07:28:00 GMT";
        // todo plugweb-657 set appropriate value for batch file's actual modification time

        // when
        final Response response = RestAssured.given()
                .header("If-Modified-Since", ifModifiedSince)
                .when()
                .get(BATCH_URL + String.format("?%s=%s", STATIC_HASH, "123"))
                .andReturn();

        // then
        assertThat(response.statusCode(), equalTo(200));
        assertThat(response.getBody().print(), not(emptyString()));
    }

    @Test
    @Ignore("PLUG-1268 - the atlassian-plugins-webresource-common module does not handle invalid http headers at all")
    public void given_ifModifiedSinceValueInvalid_when_requestBatchFile_then_returnsContent() {
        // given
        final String ifModifiedSince = "not-a-date";

        // when
        final Response response = RestAssured.given()
                .header("If-Modified-Since", ifModifiedSince)
                .when()
                .get(BATCH_URL + String.format("?%s=%s", STATIC_HASH, "123"))
                .andReturn();

        // then
        assertThat(response.statusCode(), equalTo(200));
        assertThat(response.getBody().print(), not(emptyString()));
    }
}
