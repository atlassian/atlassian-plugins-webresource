package it.com.atlassian.rest.util;

import org.hamcrest.Description;
import org.hamcrest.StringDescription;

public class AsyncTestUtils {

    private AsyncTestUtils() {}

    /**
     * Waits for the {@link WaitCondition#test()} to return true.
     * Throws an AssertionException if testCondition does not become true within the timeout period.
     *
     * @param waitCondition   the condition
     * @param timeoutMs       the maximum time in millis to wait for the condition to become true
     * @param retryIntervalMs the time in millis between tests
     */
    public static void waitFor(WaitCondition waitCondition, long timeoutMs, long retryIntervalMs) {
        try {
            long startTime = System.currentTimeMillis();
            boolean conditionPassed = waitCondition.test();
            while (!conditionPassed && System.currentTimeMillis() - startTime < timeoutMs) {
                Thread.sleep(retryIntervalMs);
                conditionPassed = waitCondition.test();
            }

            if (!conditionPassed) {
                Description desc = new StringDescription();
                waitCondition.describeFailure(desc);
                throw new AssertionError(desc.toString());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
