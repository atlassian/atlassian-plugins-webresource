package it.com.atlassian.rest.two.zero.util;

import static it.com.atlassian.rest.util.CommonRestTestConstants.getRestBaseUrl;

public class CommonTwoZeroRestTestConstants {
    public static String getContextUrl() {
        return getRestBaseUrl() + "/wrm/2.0";
    }
}
