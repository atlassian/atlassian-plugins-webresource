package it.com.atlassian.rest.client;

import javax.annotation.Nonnull;

import com.atlassian.pageobjects.TestedProduct;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

final class HttpClientBuilder {
    private final Class<? extends TestedProduct> testProductClazz;
    private String baseContextPath;

    HttpClientBuilder(@Nonnull final Class<? extends TestedProduct> testProductClazz) {
        this.testProductClazz = requireNonNull(testProductClazz, "The product instance type is mandatory");
    }

    @Nonnull
    HttpClientBuilder setBaseContextPath(@Nonnull final String baseContextPath) {
        this.baseContextPath = requireNonNull(baseContextPath, "The base context path is mandatory.");
        return this;
    }

    @Nonnull
    HttpClient build() {
        return ofNullable(baseContextPath)
                .map(contextPath -> new HttpClient(testProductClazz, contextPath))
                .orElseGet(() -> new HttpClient(testProductClazz));
    }
}
