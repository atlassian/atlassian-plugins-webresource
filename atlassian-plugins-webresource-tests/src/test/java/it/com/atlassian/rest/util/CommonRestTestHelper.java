package it.com.atlassian.rest.util;

import java.io.IOException;
import javax.annotation.Nonnull;

import org.hamcrest.Matcher;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

public class CommonRestTestHelper {
    public static String writeJsonObjectToString(Object jsonObject) throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
        return objectMapper.writeValueAsString(jsonObject);
    }

    @Nonnull
    public static Matcher<Object> webresourceBatchItemFor(@Nonnull final String key) {
        return allOf(
                hasProperty("batchType", equalTo(PluginUrlResource.BatchType.RESOURCE)),
                hasProperty("key", equalTo(key)));
    }

    @Nonnull
    public static Matcher<Object> contextBatchItemFor(@Nonnull final String key) {
        return allOf(
                hasProperty("batchType", equalTo(PluginUrlResource.BatchType.CONTEXT)),
                hasProperty("key", equalTo(key)));
    }
}
