package it.com.atlassian.rest;

import java.util.Collection;
import javax.annotation.Nonnull;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import it.com.atlassian.rest.matcher.IsPhasedAwareResourcesResponseEquals;
import it.com.atlassian.util.DisableBatchingRule;

import com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareRequestJson;
import com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareResourcesResponseJson;

import static it.com.atlassian.rest.client.ProductHttpClient.REFAPP_API_2_0;
import static it.com.atlassian.util.JSONUtil.fileToEntity;
import static it.com.atlassian.util.WebDriverTestUtil.REFAPP;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.MatcherAssert.assertThat;

import static com.atlassian.plugins.features.testing.EnableDarkFeaturesRule.enableFeatureForAllUsersForProducts;

/**
 * <p>Class responsible for performing tests against the REST 2.0 WRM endpoints.</p>
 * <p>This class uses a path standard to configure and validate the tests.</p>
 *
 * @since 5.0.0
 */
@RunWith(Parameterized.class)
public class PhasesAwareResourcesTest {
    @Rule
    public DisableBatchingRule disableBatchingRule = new DisableBatchingRule();

    /**
     * The base test folder which will be added as prefix to the final configuration file path.
     */
    private static final String BASE_TEST_PATH = "/test-cases/rest/phases-aware-resources/%s";

    private static final String PERFORMANCE_TRACKING_FLAG =
            "atlassian.darkfeature.atlassian.webresource.performance.tracking.disable";

    /**
     * The test case name used to retrive the tests configuration files.
     */
    private final String testCaseName;

    @Rule
    public RuleChain perfTracking = enableFeatureForAllUsersForProducts(PERFORMANCE_TRACKING_FLAG, REFAPP);

    public PhasesAwareResourcesTest(@Nonnull final String testCaseName) {
        this.testCaseName = requireNonNull(testCaseName, "The test case path is mandatory.");
    }

    @Nonnull
    @Parameters(name = "{0}")
    public static Collection<String> testCaseNames() {
        return asList(
                "without-excluding-web-resource-javascript-api",
                "excluding-web-resource-javascript-api",
                "iteration-order-preserved");
    }

    /**
     * This test is responsible for verifying that WRM JS API files are not added to the response.
     * All the tests cases related to this scenario must have a folder containing a {@code {testeCaseName}/request.json and {testeCaseName}/expectedResponse.json}
     */
    @Test
    public void when_GetResources_Then_ShouldNotIncludeWebResourceJavascriptAPIResources() {
        // when
        final String requestFullPath = format(BASE_TEST_PATH, testCaseName + "/request.json");
        final PhasesAwareRequestJson payload = fileToEntity(requestFullPath, PhasesAwareRequestJson.class);
        final PhasesAwareResourcesResponseJson actualResponse =
                REFAPP_API_2_0.post("/resources", payload, PhasesAwareResourcesResponseJson.class);

        // then
        final String expectedFullPath = format(BASE_TEST_PATH, testCaseName + "/expectedResponse.json");
        final PhasesAwareResourcesResponseJson expectedResponse =
                fileToEntity(expectedFullPath, PhasesAwareResourcesResponseJson.class);
        assertThat(actualResponse, new IsPhasedAwareResourcesResponseEquals(expectedResponse));
    }
}
