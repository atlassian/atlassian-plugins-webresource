package it.com.atlassian.rest.client;

import java.io.IOException;
import javax.annotation.Nonnull;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.com.atlassian.rest.property.ProductInstanceProperty;
import it.com.atlassian.util.JSONUtil;

import com.atlassian.pageobjects.TestedProduct;

import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Represents an http client used to perform request against a certain product.
 *
 * @since 5.0.0
 */
final class HttpClient {
    private static final String EMPTY = "";
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpClient.class);

    /**
     * Represents the http client to perform requests against a certain product instance.
     */
    private final org.apache.http.client.HttpClient httpClient;

    /**
     * Represents the http host with name and port.
     */
    private final HttpHost httpHost;

    /**
     * Represents the context path to be added as part of the url when making a request.
     */
    private final String baseContextPath;

    HttpClient(@Nonnull final Class<? extends TestedProduct> testedProductClass, final String baseContextPath) {
        requireNonNull(testedProductClass, "The product instance type is mandatory");
        this.baseContextPath = requireNonNull(baseContextPath, "The base context path is mandatory");
        this.httpClient = HttpClientBuilder.create().build();
        this.httpHost = new ProductInstanceProperty(testedProductClass)
                .getBaseUrl()
                .map(baseUrl -> baseUrl.replace("http://", ""))
                .map(HttpHost::new)
                .orElseThrow(() ->
                        new IllegalArgumentException("There is not any base url defined for the product instance"));
    }

    HttpClient(@Nonnull final Class<? extends TestedProduct> testedProductClass) {
        this(testedProductClass, EMPTY);
    }

    @Nonnull
    <T> HttpResponse post(@Nonnull final String url, @Nonnull final T payload) {
        requireNonNull(url, "The url is mandatory");
        requireNonNull(payload, "The payload is mandatory.");
        try {
            final HttpPost httpPost = new HttpPost(httpHost + baseContextPath + url);
            httpPost.setHeader(CONTENT_TYPE, APPLICATION_JSON);

            final StringEntity entity = new StringEntity(JSONUtil.toString(payload));
            httpPost.setEntity(entity);

            return httpClient.execute(httpPost);
        } catch (final IOException exception) {
            LOGGER.error("Error while performing post for the url: {}.", httpHost + baseContextPath + url);
            throw new IllegalStateException(exception);
        }
    }
}
