package it.com.atlassian.rest.property;

import java.util.Optional;
import javax.annotation.Nonnull;

import com.atlassian.pageobjects.TestedProduct;

import static java.lang.String.format;
import static java.lang.System.getProperty;
import static java.util.Optional.ofNullable;

/**
 * Startegy responsible for retriving information based on AMPS plugin's configuration.
 *
 * @since 5.0.0
 */
final class AmpsPropertyStrategy implements PropertyStrategy {
    private static final String BASE_URL_PATTERN = "baseurl.%s";
    private static final String CONTEXT_PATH_PATTERN = "context.%s.path";
    private static final String HTTP_PORT = "http.%s.port";

    private final String instanceId;

    AmpsPropertyStrategy(@Nonnull final Class<? extends TestedProduct> clazz) {
        this.instanceId = new ProductInstanceDefaults(clazz).getInstanceId();
    }

    @Nonnull
    @Override
    public Optional<String> getBaseUrl() {
        final String property = format(BASE_URL_PATTERN, instanceId);
        return ofNullable(getProperty(property));
    }

    @Nonnull
    @Override
    public Optional<String> getContextPath() {
        final String property = format(CONTEXT_PATH_PATTERN, instanceId);
        return ofNullable(getProperty(property));
    }

    @Nonnull
    @Override
    public Optional<String> getHttpPort() {
        final String property = format(HTTP_PORT, instanceId);
        return ofNullable(getProperty(property));
    }
}
