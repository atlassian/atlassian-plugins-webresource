package it.com.atlassian.rest.property;

import javax.annotation.Nonnull;

import com.atlassian.pageobjects.Defaults;
import com.atlassian.pageobjects.TestedProduct;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

/**
 * Class responsible for retriving products instance's default information.
 *
 * @since 5.0.0
 */
final class ProductInstanceDefaults {
    private final Defaults defaults;

    ProductInstanceDefaults(@Nonnull final Class<? extends TestedProduct> clazz) {
        requireNonNull(clazz, "The tested product class is mandatory");
        this.defaults = ofNullable(clazz.getAnnotation(Defaults.class)).orElseThrow(() -> {
            final String message =
                    format("The tested product class '%s' is missing the @Defaults annotation", clazz.getName());
            return new IllegalArgumentException(message);
        });
    }

    /**
     * Get product instance's context-path.
     * @return The product instance's context-path.
     */
    @Nonnull
    String getContextPath() {
        return defaults.contextPath();
    }

    /**
     * Get product instance's http port.
     * @return The product instance's http port.
     */
    @Nonnull
    String getHttpPort() {
        return valueOf(defaults.httpPort());
    }

    /**
     * Get product instance's identifier.
     * @return The product instance's identifier.
     */
    @Nonnull
    String getInstanceId() {
        return defaults.instanceId();
    }
}
