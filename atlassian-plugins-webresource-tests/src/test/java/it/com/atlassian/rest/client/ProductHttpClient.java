package it.com.atlassian.rest.client;

import javax.annotation.Nonnull;

import org.apache.http.HttpResponse;

import com.atlassian.webdriver.refapp.RefappTestedProduct;

import static it.com.atlassian.util.JSONUtil.toEntity;
import static java.util.Objects.requireNonNull;

public enum ProductHttpClient {
    /**
     * Represents the http client used to perform requests against the Resource phase REST endpoint.
     */
    REFAPP_API_2_0(new HttpClientBuilder(RefappTestedProduct.class)
            .setBaseContextPath("/rest/wrm/2.0")
            .build());

    private final HttpClient httpClient;

    ProductHttpClient(@Nonnull final HttpClient httpClient) {
        this.httpClient = requireNonNull(httpClient, "The http client is mandatory.");
    }

    @Nonnull
    public <T, R> R post(@Nonnull final String url, @Nonnull final T payload, @Nonnull final Class<R> responseType) {
        requireNonNull(url, "The url is mandatory");
        requireNonNull(payload, "The payload is mandatory.");
        requireNonNull(responseType, "The response type is mandatory.");
        final HttpResponse response = this.httpClient.post(url, payload);
        return toEntity(response.getEntity(), responseType);
    }
}
