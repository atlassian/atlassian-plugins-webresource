package it.com.atlassian.rest.one.zero.util;

import static it.com.atlassian.rest.util.CommonRestTestConstants.getRestBaseUrl;

public class CommonOneZeroRestTestConstants {
    public static String getContextUrl() {
        return getRestBaseUrl() + "/webResources/1.0";
    }
}
