package it.com.atlassian.rest.property;

import java.util.Optional;
import javax.annotation.Nonnull;

import com.atlassian.pageobjects.TestedProduct;

import static java.util.Objects.requireNonNull;

/**
 * Class responsible for retriving product instance's properties.
 *
 * @since 5.0.0
 */
public final class ProductInstanceProperty {
    private final PropertyStrategy instancePropertyStrategy;

    public ProductInstanceProperty(@Nonnull final Class<? extends TestedProduct> clazz) {
        requireNonNull(clazz, "The tested product instance type is mandatory");
        this.instancePropertyStrategy = InstancePropertyStrategyFactory.get(clazz);
    }

    /**
     * Get the product instance base url.
     * @return The product instance base url.
     */
    @Nonnull
    public Optional<String> getBaseUrl() {
        return instancePropertyStrategy.getBaseUrl();
    }

    /**
     * Get product instance's context-path.
     * @return The product instance's context-path.
     */
    @Nonnull
    public Optional<String> getContextPath() {
        return instancePropertyStrategy.getContextPath();
    }

    /**
     * Get product instance's http port.
     * @return The product instance's http port.
     */
    @Nonnull
    public Optional<String> getHttpPort() {
        return instancePropertyStrategy.getHttpPort();
    }
}
