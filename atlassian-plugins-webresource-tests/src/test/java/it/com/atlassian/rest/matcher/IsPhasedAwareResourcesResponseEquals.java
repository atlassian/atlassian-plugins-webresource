package it.com.atlassian.rest.matcher;

import java.util.Collection;
import javax.annotation.Nonnull;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareResourcesResponseJson;
import com.atlassian.webresource.plugin.rest.two.zero.model.UrlFetchableResourceJson;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * Matcher used to perform verifications between two {@link PhasesAwareResourcesResponseJson}.
 *
 * @since 5.0.0
 */
public class IsPhasedAwareResourcesResponseEquals extends TypeSafeMatcher<PhasesAwareResourcesResponseJson> {

    private final PhasesAwareResourcesResponseJson expected;

    public IsPhasedAwareResourcesResponseEquals(@Nonnull final PhasesAwareResourcesResponseJson expected) {
        this.expected = requireNonNull(expected, "The source is mandatory to compare with the target.");
    }

    @Override
    protected boolean matchesSafely(@Nonnull final PhasesAwareResourcesResponseJson actual) {
        final Collection<UrlFetchableResourceJson> actualInteractionResources =
                normalize(actual.getInteraction().getResources());
        final Collection<UrlFetchableResourceJson> expectedInteractionResources =
                expected.getInteraction().getResources();

        final Collection<UrlFetchableResourceJson> actualRequireResources =
                normalize(actual.getRequire().getResources());
        final Collection<UrlFetchableResourceJson> expectedRequireResources =
                expected.getRequire().getResources();

        return actualInteractionResources.equals(expectedInteractionResources)
                && actualRequireResources.equals(expectedRequireResources);
    }

    @Override
    public void describeTo(@Nonnull final Description description) {
        description.appendText(expected.toString());
    }

    private Collection<UrlFetchableResourceJson> normalize(final Collection<UrlFetchableResourceJson> resources) {
        return resources.stream()
                .map(resource -> {
                    final int fileNameIndex = resource.getUrl().lastIndexOf('/') + 1;
                    return new UrlFetchableResourceJson(
                            resource.getBatchType(),
                            resource.getKey(),
                            resource.getResourceType(),
                            resource.getUrl().substring(fileNameIndex));
                })
                .collect(toList());
    }
}
