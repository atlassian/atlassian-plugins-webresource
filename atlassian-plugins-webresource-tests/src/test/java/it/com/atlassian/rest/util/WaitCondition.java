package it.com.atlassian.rest.util;

import org.hamcrest.Description;

public interface WaitCondition {

    void describeFailure(Description description) throws Exception;

    boolean test() throws Exception;
}
