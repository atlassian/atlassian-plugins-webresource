package it.com.atlassian.rest.two.zero;

import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import io.restassured.RestAssured;
import io.restassured.response.ResponseBodyExtractionOptions;

import it.com.atlassian.util.DisableBatchingRule;

import com.atlassian.webresource.plugin.rest.two.zero.model.ErrorResponseJson;
import com.atlassian.webresource.plugin.rest.two.zero.model.RequestableEdgeListResponseJson;

import static it.com.atlassian.rest.two.zero.util.CommonTwoZeroRestTestConstants.getContextUrl;
import static it.com.atlassian.util.JSONUtil.fileToEntity;
import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RequestablesResourcesRestTest {
    @Rule
    public DisableBatchingRule disableBatchingRule = new DisableBatchingRule();

    private static final String TEST_CASES_BASE_PATH = "/test-cases/rest/requestables-resources/%s";
    private static final String ENDPOINT = getContextUrl() + "/requestables";
    private static final String VALID_USER = "admin";
    private static final String VALID_PASSWORD = "admin";

    private static String getConsumerGraph(final String requestableKey) {
        return String.join("/", ENDPOINT, requestableKey, "consumer-graph");
    }

    private static String getDependencyGraph(final String requestableKey) {
        return String.join("/", ENDPOINT, requestableKey, "dependency-graph");
    }

    @Test
    public void testRequiresValidAuthentication() {
        final String url = getConsumerGraph("graphtest-ctx-features");

        ResponseBodyExtractionOptions body = RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .when()
                .get(url)
                .then()
                .statusCode(UNAUTHORIZED.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .body();

        assertResponseMessage(body);
    }

    private void assertResponseMessage(ResponseBodyExtractionOptions body) {
        // Bitbucket handles unauthorized exceptions differently from other products.
        if ("bitbucket".equals(System.getProperty("product"))) {
            assertThat(
                    body.jsonPath().getList("errors.message", String.class),
                    equalTo(List.of("You are not permitted to access this resource")));
        } else {
            assertThat(
                    body.as(ErrorResponseJson.class).getMessage(),
                    equalTo("Client must be authenticated to access this resource."));
        }
    }

    @Test
    public void testNotFoundResponse() {
        // given
        final String url = getDependencyGraph("nonexistent.plugin.key:webresource-key");

        // when
        final ErrorResponseJson actualResponse = RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .auth()
                .preemptive()
                .basic(VALID_USER, VALID_PASSWORD)
                .when()
                .get(url)
                .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .as(ErrorResponseJson.class);

        // then
        assertThat(
                actualResponse.getMessage(),
                equalTo("Could not find `nonexistent.plugin.key:webresource-key` in the graph."));
    }

    @Test
    public void testFindDependendenciesForContextNorth() {
        // given
        final String url = getDependencyGraph("graphtest-ctx-north");

        // when
        final RequestableEdgeListResponseJson actualResponse = RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .auth()
                .preemptive()
                .basic(VALID_USER, VALID_PASSWORD)
                .when()
                .get(url)
                .then()
                .statusCode(OK.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .body()
                .as(RequestableEdgeListResponseJson.class);

        // then
        final String expectedResponseFullPath =
                format(TEST_CASES_BASE_PATH, "dependency-graph-ctx-north/expectedResponse.json");
        final RequestableEdgeListResponseJson expectedResponse =
                fileToEntity(expectedResponseFullPath, RequestableEdgeListResponseJson.class);

        assertThat(actualResponse, equalTo(expectedResponse));
    }

    @Test
    public void testFindAncestorsForWebresourceCommonLibraryAAA() {
        // given
        final String url = getConsumerGraph(
                "com.atlassian.plugins.atlassian-plugins-webresource-tests:graphtest-common-library-aaa");

        // when
        final RequestableEdgeListResponseJson actualResponse = RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .auth()
                .preemptive()
                .basic(VALID_USER, VALID_PASSWORD)
                .when()
                .get(url)
                .then()
                .statusCode(OK.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .body()
                .as(RequestableEdgeListResponseJson.class);

        // then
        final String expectedResponseFullPath =
                format(TEST_CASES_BASE_PATH, "consumer-graph-common-library-aaa/expectedResponse.json");
        final RequestableEdgeListResponseJson expectedResponse =
                fileToEntity(expectedResponseFullPath, RequestableEdgeListResponseJson.class);

        assertThat(actualResponse, equalTo(expectedResponse));
    }
}
