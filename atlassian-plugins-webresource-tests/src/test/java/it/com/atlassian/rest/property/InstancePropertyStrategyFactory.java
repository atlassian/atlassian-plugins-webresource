package it.com.atlassian.rest.property;

import javax.annotation.Nonnull;

import com.atlassian.pageobjects.TestedProduct;

import static java.util.Objects.requireNonNull;

/**
 * Factory responsible for performing the retrieval of {@link PropertyStrategy}.
 *
 * @since 5.0.0
 */
final class InstancePropertyStrategyFactory {

    /**
     * Get an {@link PropertyStrategy} based on the {@link TestedProduct}.
     *
     * @param clazz The product instance type.
     * @return The found {@link PropertyStrategy} based on the instance type.
     */
    @Nonnull
    static PropertyStrategy get(@Nonnull final Class<? extends TestedProduct> clazz) {
        requireNonNull(clazz, "The tested product class is mandatory");
        final PropertyStrategy defaultStrategy = new AmpsPropertyStrategy(clazz);
        return defaultStrategy
                .getBaseUrl()
                .map(baseUrl -> defaultStrategy)
                .orElseGet(() -> new WebDriverPropertyStrategy(clazz));
    }
}
