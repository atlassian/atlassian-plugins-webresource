package it.com.atlassian.rest.property;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;
import javax.annotation.Nonnull;

import com.atlassian.pageobjects.TestedProduct;

import static java.lang.String.format;
import static java.util.Optional.of;

import static com.atlassian.pageobjects.util.Env.getVar;

/**
 * Startegy responsible for retriving information based on web driver's information.
 *
 * @since 5.0.0
 */
final class WebDriverPropertyStrategy implements PropertyStrategy {
    private static final String BASE_URL_PATTERN = "%s:%s%s";
    private static final String WEBDRIVER_TARGET_HOST_ENV = "WEBDRIVER_TARGET_HOST";
    private static final String WEBDRIVER_TARGET_PORT_ENV = "WEBDRIVER_TARGET_PORT";

    private final ProductInstanceDefaults productInstanceDefaults;

    WebDriverPropertyStrategy(@Nonnull final Class<? extends TestedProduct> clazz) {
        this.productInstanceDefaults = new ProductInstanceDefaults(clazz);
    }

    private static String getLocalHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (final UnknownHostException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Nonnull
    @Override
    public Optional<String> getBaseUrl() {
        final String hostName = getVar(WEBDRIVER_TARGET_HOST_ENV, getLocalHostName());
        final String hostPort = getHttpPort()
                .map(httpPort -> getVar(WEBDRIVER_TARGET_PORT_ENV, httpPort))
                .orElseThrow(() -> new IllegalStateException("There is not any defined port for product instance"));
        return getContextPath().map(contextPath -> format(BASE_URL_PATTERN, hostName, hostPort, contextPath));
    }

    @Nonnull
    @Override
    public Optional<String> getContextPath() {
        return of(productInstanceDefaults.getContextPath());
    }

    @Nonnull
    @Override
    public Optional<String> getHttpPort() {
        return of(productInstanceDefaults.getHttpPort());
    }
}
