package it.com.atlassian.rest.one.zero;

import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import io.restassured.RestAssured;

import it.com.atlassian.util.DisableBatchingRule;

import com.atlassian.webresource.plugin.rest.one.zero.model.ResolveResourcesJson;
import com.atlassian.webresource.plugin.rest.one.zero.model.ResourcesAndData;

import static it.com.atlassian.rest.one.zero.util.CommonOneZeroRestTestConstants.getContextUrl;
import static it.com.atlassian.rest.util.CommonRestTestConstants.getWebResourcePluginPrefix;
import static it.com.atlassian.rest.util.CommonRestTestHelper.contextBatchItemFor;
import static it.com.atlassian.rest.util.CommonRestTestHelper.webresourceBatchItemFor;
import static it.com.atlassian.rest.util.CommonRestTestHelper.writeJsonObjectToString;
import static it.com.atlassian.util.WebDriverTestUtil.REFAPP;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.plugins.features.testing.EnableDarkFeaturesRule.enableFeatureForAllUsersForProducts;

public class PhasesUnawareResourcesRestTest {
    @Rule
    public DisableBatchingRule disableBatchingRule = new DisableBatchingRule();

    private static final String ENDPOINT = getContextUrl() + "/resources";
    private static final String PERFORMANCE_TRACKING_FLAG =
            "atlassian.darkfeature.atlassian.webresource.performance.tracking.disable";

    final String FIRST_FEATURE_WEBRESOURCE_KEY = getWebResourcePluginPrefix() + "first";
    final String SECOND_FEATURE_WEBRESOURCE_KEY = getWebResourcePluginPrefix() + "second";
    final String THIRD_FEATURE_WEBRESOURCE_KEY = getWebResourcePluginPrefix() + "third";
    final String FOURTH_FEATURE_WEBRESOURCE_KEY = getWebResourcePluginPrefix() + "fourth";
    final String FIFTH_FEATURE_WEBRESOURCE_KEY = getWebResourcePluginPrefix() + "fifth";
    final String SIXTH_FEATURE_WEBRESOURCE_KEY = getWebResourcePluginPrefix() + "sixth";

    final ResourcesAndData emptyResponse = new ResourcesAndData(emptyList(), emptyMap(), emptyMap());

    @Rule
    public RuleChain perfTracking = enableFeatureForAllUsersForProducts(PERFORMANCE_TRACKING_FLAG, REFAPP);

    @Test
    public void testEmptyGetRequestReturnsNothing() {
        final ResourcesAndData actualResponse = RestAssured.given()
                .accept(APPLICATION_JSON)
                .when()
                .get(ENDPOINT)
                .then()
                .statusCode(OK.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .body()
                .as(ResourcesAndData.class);

        assertThat(actualResponse, equalTo(emptyResponse));
    }

    @Test
    public void testEmptyPostRequestReturnsNothing() throws IOException {
        final String emptyRequest = writeJsonObjectToString(new ResolveResourcesJson());

        final ResourcesAndData actualResponse = RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(emptyRequest)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(OK.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .body()
                .as(ResourcesAndData.class);

        assertThat(actualResponse, equalTo(emptyResponse));
    }

    @Test
    public void testRequestForMultipleWebResources() throws IOException {
        final String requestBody = writeJsonObjectToString(new ResolveResourcesJson(
                asList(FIRST_FEATURE_WEBRESOURCE_KEY, FOURTH_FEATURE_WEBRESOURCE_KEY),
                emptyList(),
                emptyList(),
                emptyList()));

        final ResourcesAndData actualResponse = RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(requestBody)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(OK.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .body()
                .as(ResourcesAndData.class);

        assertThat(
                actualResponse.getResources(),
                either(containsInAnyOrder(
                                webresourceBatchItemFor(FIRST_FEATURE_WEBRESOURCE_KEY),
                                webresourceBatchItemFor(FOURTH_FEATURE_WEBRESOURCE_KEY)))
                        .or(contains(contextBatchItemFor(
                                String.join(",", FIRST_FEATURE_WEBRESOURCE_KEY, FOURTH_FEATURE_WEBRESOURCE_KEY)))));
    }

    @Test
    public void testRequestForMultipleContexts() throws IOException {
        final String requestBody = writeJsonObjectToString(new ResolveResourcesJson(
                emptyList(), asList("test-ordered.odd", "test-ordered.even"), emptyList(), emptyList()));

        final ResourcesAndData actualResponse = RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(requestBody)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(OK.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .body()
                .as(ResourcesAndData.class);

        assertThat(
                actualResponse.getResources(),
                either(containsInAnyOrder(
                                webresourceBatchItemFor(FIRST_FEATURE_WEBRESOURCE_KEY),
                                webresourceBatchItemFor(THIRD_FEATURE_WEBRESOURCE_KEY),
                                webresourceBatchItemFor(FIFTH_FEATURE_WEBRESOURCE_KEY),
                                webresourceBatchItemFor(SECOND_FEATURE_WEBRESOURCE_KEY),
                                webresourceBatchItemFor(FOURTH_FEATURE_WEBRESOURCE_KEY),
                                webresourceBatchItemFor(SIXTH_FEATURE_WEBRESOURCE_KEY)))
                        .or(containsInAnyOrder(
                                contextBatchItemFor("test-ordered.odd"), contextBatchItemFor("test-ordered.even")))
                        .or(contains(contextBatchItemFor(String.join(",", "test-ordered.odd", "test-ordered.even")))));
    }

    @Test
    public void testRequestForMultipleInclusionsAndExclusions() throws IOException {
        final String requestBody = writeJsonObjectToString(new ResolveResourcesJson(
                emptyList(),
                asList("test-ordered.features"),
                asList(THIRD_FEATURE_WEBRESOURCE_KEY, FIFTH_FEATURE_WEBRESOURCE_KEY),
                asList("test-ordered.even")));

        final ResourcesAndData actualResponse = RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(requestBody)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(OK.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .body()
                .as(ResourcesAndData.class);

        assertThat(
                actualResponse.getResources(),
                either(containsInAnyOrder(webresourceBatchItemFor(FIRST_FEATURE_WEBRESOURCE_KEY)))
                        .or(contains(contextBatchItemFor(String.join(
                                ",",
                                "test-ordered.features",
                                "-com.atlassian.plugins.atlassian-plugins-webresource-tests:fifth",
                                "-test-ordered.even",
                                "-com.atlassian.plugins.atlassian-plugins-webresource-tests:third")))));
    }

    @Test
    public void testInvalidJsonReturnsBadRequest() {
        final String badRequest = "thisAintNoJson";

        RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(badRequest)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }
}
