package it.com.atlassian.rest.property;

import java.util.Optional;
import javax.annotation.Nonnull;

interface PropertyStrategy {

    /**
     * Get the product instance base url.
     * @return The product instance base url.
     */
    @Nonnull
    Optional<String> getBaseUrl();

    /**
     * Get product instance's context-path.
     * @return The product instance's context-path.
     */
    @Nonnull
    Optional<String> getContextPath();

    /**
     * Get product instance's http port.
     * @return The product instance's http port.
     */
    @Nonnull
    Optional<String> getHttpPort();
}
