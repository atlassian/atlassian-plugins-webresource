package it.com.atlassian.rest.two.zero;

import java.io.IOException;
import javax.annotation.Nonnull;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import io.restassured.RestAssured;

import it.com.atlassian.util.DisableBatchingRule;

import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareRequestJson;
import com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareResourcesResponseJson;
import com.atlassian.webresource.plugin.rest.two.zero.model.UrlFetchableResourcesWithDataJson;

import static it.com.atlassian.rest.two.zero.util.CommonTwoZeroRestTestConstants.getContextUrl;
import static it.com.atlassian.rest.util.CommonRestTestConstants.getWebResourceContextPrefix;
import static it.com.atlassian.rest.util.CommonRestTestConstants.getWebResourcePluginPrefix;
import static it.com.atlassian.rest.util.CommonRestTestHelper.writeJsonObjectToString;
import static it.com.atlassian.util.WebDriverTestUtil.REFAPP;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_ACCEPTABLE;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.UNSUPPORTED_MEDIA_TYPE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;

import static com.atlassian.plugins.features.testing.EnableDarkFeaturesRule.enableFeatureForAllUsersForProducts;
import static com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareRequestJson.EXCLUDE_RESOURCES_NULL_MSG;
import static com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareRequestJson.INTERACTION_RESOURCES_NULL_MSG;
import static com.atlassian.webresource.plugin.rest.two.zero.model.PhasesAwareRequestJson.REQUIRE_RESOURCES_NULL_MSG;

public class PhasesAwareResourcesRestTest {
    @Rule
    public DisableBatchingRule disableBatchingRule = new DisableBatchingRule();

    private static final String ENDPOINT = getContextUrl() + "/resources";
    private static final String PERFORMANCE_TRACKING_FLAG =
            "atlassian.darkfeature.atlassian.webresource.performance.tracking.disable";

    final String EMPTY_REQUEST = writeJsonObjectToString(new PhasesAwareRequestJson());

    final String REQUIRE_WEB_RESOURCE_KEY = getWebResourcePluginPrefix() + "test-phases-require";
    final String INTERACTION_WEB_RESOURCE_KEY = getWebResourcePluginPrefix() + "test-phases-interaction";
    final String EXCLUDE_WEB_RESOURCE_KEY = getWebResourcePluginPrefix() + "test-phases-exclude";

    final String REQUIRE_WEB_RESOURCE_CONTEXT_KEY = getWebResourceContextPrefix() + "test-phases-require";
    final String INTERACTION_WEB_RESOURCE_CONTEXT_KEY = getWebResourceContextPrefix() + "test-phases-interaction";
    final String EXCLUDE_WEB_RESOURCE_CONTEXT_KEY = getWebResourceContextPrefix() + "test-phases-exclude";

    @Rule
    public RuleChain perfTracking = enableFeatureForAllUsersForProducts(PERFORMANCE_TRACKING_FLAG, REFAPP);

    public PhasesAwareResourcesRestTest() throws IOException {}

    @Test
    public void testEmptyRequestReturnsNothing() {
        final PhasesAwareResourcesResponseJson emptyResponse = new PhasesAwareResourcesResponseJson(
                new UrlFetchableResourcesWithDataJson(emptyList(), emptyMap(), emptyMap()),
                new UrlFetchableResourcesWithDataJson(emptyList(), emptyMap(), emptyMap()));

        PhasesAwareResourcesResponseJson actualResponse = RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(EMPTY_REQUEST)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(OK.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .body()
                .as(PhasesAwareResourcesResponseJson.class);

        assertThat(actualResponse, equalTo(emptyResponse));
    }

    @Test
    public void testResourcesAndDataArePhased() throws IOException {
        final String requestOneOfEachPhase = writeJsonObjectToString(new PhasesAwareRequestJson(
                singletonList(REQUIRE_WEB_RESOURCE_KEY),
                singletonList(INTERACTION_WEB_RESOURCE_KEY),
                singletonList(EXCLUDE_WEB_RESOURCE_KEY)));

        PhasesAwareResourcesResponseJson actualResponse = RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(requestOneOfEachPhase)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(OK.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .body()
                .as(PhasesAwareResourcesResponseJson.class);

        // "Interaction" resource. Item type may vary depending on instance's batching configuration.
        assertThat(
                actualResponse.getInteraction().getResources(),
                contains(either(contextBatchItemFor(INTERACTION_WEB_RESOURCE_KEY))
                        .or(webresourceBatchItemFor(INTERACTION_WEB_RESOURCE_KEY))));

        //  "Require" resource. Item type may vary depending on instance's batching configuration.
        assertThat(
                actualResponse.getRequire().getResources(),
                hasItem(either(contextBatchItemFor(REQUIRE_WEB_RESOURCE_KEY))
                        .or(webresourceBatchItemFor(REQUIRE_WEB_RESOURCE_KEY))));
    }

    @Test
    public void testContextsArePhased() throws IOException {
        final String requestOneOfEachPhase = writeJsonObjectToString(new PhasesAwareRequestJson(
                singletonList(REQUIRE_WEB_RESOURCE_CONTEXT_KEY),
                singletonList(INTERACTION_WEB_RESOURCE_CONTEXT_KEY),
                singletonList(EXCLUDE_WEB_RESOURCE_CONTEXT_KEY)));

        PhasesAwareResourcesResponseJson actualResponse = RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(requestOneOfEachPhase)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(OK.getStatusCode())
                .contentType(APPLICATION_JSON)
                .extract()
                .body()
                .as(PhasesAwareResourcesResponseJson.class);

        // "Interaction" resource. Item type may vary depending on instance's batching configuration.
        assertThat(
                actualResponse.getInteraction().getResources(),
                contains(either(contextBatchItemFor("test-phases-interaction"))
                        .or(webresourceBatchItemFor(INTERACTION_WEB_RESOURCE_KEY))));

        //  "Require" resource. Item type may vary depending on instance's batching configuration.
        assertThat(
                actualResponse.getRequire().getResources(),
                hasItem(either(contextBatchItemFor("test-phases-require"))
                        .or(webresourceBatchItemFor(REQUIRE_WEB_RESOURCE_KEY))));
    }

    @Test
    public void testInvalidJsonReturnsBadRequest() {
        final String badRequest = "thisAintNoJson";

        RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(badRequest)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void testRequestJsonMissingRequireReturnsBadRequest() {
        final String badRequest = "{\"interaction\":[],\"exclude\":[]}";

        RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(badRequest)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(BAD_REQUEST.getStatusCode())
                .body("errorMessage", containsString(REQUIRE_RESOURCES_NULL_MSG));
    }

    @Test
    public void testRequestJsonMissingInteractionReturnsBadRequest() {
        final String badRequest = "{\"require\":[],\"exclude\":[]}";

        RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(badRequest)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(BAD_REQUEST.getStatusCode())
                .body("errorMessage", containsString(INTERACTION_RESOURCES_NULL_MSG));
    }

    @Test
    public void testRequestJsonMissingExcludeReturnsBadRequest() {
        final String badRequest = "{\"require\":[],\"interaction\":[]}";

        RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(badRequest)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(BAD_REQUEST.getStatusCode())
                .body("errorMessage", containsString(EXCLUDE_RESOURCES_NULL_MSG));
    }

    @Test
    public void testRequestWithWrongMediaTypeReturnsUnsupportedMediaType() {
        final String textRequest = "According to all known laws of aviation, there is no way that a bee should be able "
                + "to fly. Its wings are too small to get its fat little body off the ground. The bee, of course, flies "
                + "anyways. Because bees don't care what humans think is impossible.";

        RestAssured.given()
                .contentType(TEXT_PLAIN)
                .accept(APPLICATION_JSON)
                .body(textRequest)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(UNSUPPORTED_MEDIA_TYPE.getStatusCode());
    }

    @Test
    public void testRequestWithWrongAcceptTypeReturnsNotAcceptable() {
        RestAssured.given()
                .contentType(APPLICATION_JSON)
                .accept(TEXT_PLAIN)
                .body(EMPTY_REQUEST)
                .when()
                .post(ENDPOINT)
                .then()
                .statusCode(NOT_ACCEPTABLE.getStatusCode());
    }

    @Nonnull
    private Matcher<Object> webresourceBatchItemFor(@Nonnull final String key) {
        return allOf(
                hasProperty("batchType", equalTo(PluginUrlResource.BatchType.RESOURCE)),
                hasProperty("key", equalTo(key)));
    }

    @Nonnull
    private Matcher<Object> contextBatchItemFor(@Nonnull final String key) {
        return allOf(
                hasProperty("batchType", equalTo(PluginUrlResource.BatchType.CONTEXT)),
                hasProperty("key", equalTo(key)));
    }
}
