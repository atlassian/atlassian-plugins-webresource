package it.com.atlassian.rest.util;

import static com.atlassian.webresource.plugin.rest.two.zero.util.PhasesAwareResourcesModelMapperUtil.WEB_RESOURCE_CONTEXT_PREFIX;

public class CommonRestTestConstants {
    public static String getRestBaseUrl() {
        return "http://127.0.0.1:5990/refapp/rest";
    }

    public static String getWebResourcePluginKey() {
        return "com.atlassian.plugins.atlassian-plugins-webresource-tests";
    }

    public static String getWebResourcePluginPrefix() {
        return getWebResourcePluginKey() + ":";
    }

    public static String getWebResourceContextPrefix() {
        return WEB_RESOURCE_CONTEXT_PREFIX;
    }

    public static String getAdminUser() {
        return "admin";
    }

    public static String getAdminPassword() {
        return "admin";
    }
}
