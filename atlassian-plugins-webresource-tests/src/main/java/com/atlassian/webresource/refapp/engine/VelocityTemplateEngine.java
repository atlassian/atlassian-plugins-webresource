package com.atlassian.webresource.refapp.engine;

import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.refapp.configuration.ApplicationProperties;
import com.atlassian.webresource.refapp.util.TemplateRenderHelper;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_LOADER;

@Component
class VelocityTemplateEngine implements TemplateEngine {
    private static final String TEMPLATE_PATH_PATTERN = "/%s.vm";
    private static final String VELOCITY_RESOURCE_LOADER_CLASS = "classpath.resource.loader.class";
    private static final Logger LOGGER = LoggerFactory.getLogger(VelocityTemplateEngine.class);
    private static final String ERROR_MSG = "Error while retrieving template";

    private final VelocityEngine engine;
    private final Map<String, Object> defaultContext;
    private final ApplicationProperties applicationProperties;

    @Autowired
    VelocityTemplateEngine(
            @NonNull final ApplicationContext applicationContext,
            @NonNull final ApplicationProperties applicationProperties) {
        this.applicationProperties = requireNonNull(applicationProperties, "The application properties is mandatory.");

        defaultContext = new HashMap<>();
        defaultContext.put("templateRenderHelper", applicationContext.getBean(TemplateRenderHelper.class));
        for (final ResourcePhase resourcePhase : ResourcePhase.values()) {
            defaultContext.put(resourcePhase.name().toLowerCase() + "ResourcePhase", resourcePhase);
        }

        engine = new VelocityEngine();
        engine.setProperty(RESOURCE_LOADER, "classpath");
        engine.setProperty(VELOCITY_RESOURCE_LOADER_CLASS, ClasspathResourceLoader.class.getName());
    }

    @NonNull
    @Override
    public String render(@NonNull final String templateName, @NonNull final Map<String, Object> context) {
        requireNonNull(templateName, "The template path is mandatory to render the template.");
        requireNonNull(context, "The context is mandatory to build the template.");

        final VelocityContext velocityContext = new VelocityContext();
        context.forEach(velocityContext::put);
        defaultContext.forEach(velocityContext::put);

        final Writer writer = new StringWriter();
        final String templatePath = format(TEMPLATE_PATH_PATTERN, templateName);
        try {
            engine.getTemplate(applicationProperties.getEngineVelocityBaseUrl() + templatePath)
                    .merge(velocityContext, writer);
        } catch (Exception e) {
            LOGGER.error(ERROR_MSG, e);
        }
        return writer.toString();
    }
}
