package com.atlassian.webresource.refapp.servlet;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.lang.NonNull;

import com.atlassian.webresource.refapp.engine.TemplateEngine;
import com.atlassian.webresource.refapp.util.TemplateRenderHelper;

import static java.util.Collections.singletonMap;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

public class LoadLegacyScriptServlet extends TemplateServlet {
    private static final String PLUGIN_ID = "com.atlassian.plugins.atlassian-plugins-webresource-tests";

    private final Map<String, RenderEventHandler> handlers;

    @Autowired
    LoadLegacyScriptServlet(
            @NonNull @Qualifier("soyTemplateEngine") final TemplateEngine soyTemplateEngine,
            @NonNull final TemplateRenderHelper templateRenderHelper) {
        super(soyTemplateEngine);
        handlers = new HashMap<>();

        handlers.put(
                "staticViaContext",
                new SoyRenderEventHandler(
                        () -> templateRenderHelper.requireContext("test.ie-resources.static"), templateRenderHelper));
        handlers.put(
                "staticViaExplicit",
                new SoyRenderEventHandler(
                        () -> templateRenderHelper.requireWebResource(PLUGIN_ID + ":ie-resources-for-static"),
                        templateRenderHelper));
        handlers.put(
                "asyncViaContext",
                new SoyRenderEventHandler(
                        () -> templateRenderHelper.requireWebResource(
                                "com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager"),
                        singletonMap("loadString", "wrc!test.ie-resources.async"),
                        templateRenderHelper));
        handlers.put(
                "asyncViaExplicit",
                new SoyRenderEventHandler(
                        () -> templateRenderHelper.requireWebResource(
                                "com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager"),
                        singletonMap("loadString", "wr!" + PLUGIN_ID + ":ie-resources-for-async"),
                        templateRenderHelper));
    }

    @NonNull
    @Override
    public Optional<RenderEventHandler> getRenderEventHandlerByAction(@NonNull final String action) {
        requireNonNull(action, "The action is mandatory");
        return ofNullable(handlers.get(action));
    }
}
