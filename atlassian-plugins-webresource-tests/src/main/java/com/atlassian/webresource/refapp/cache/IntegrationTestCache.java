package com.atlassian.webresource.refapp.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * A poor man's cache for use during integration testing. It's not thread- or concurrency-safe,
 * but that should be okay for its purpose.
 */
public class IntegrationTestCache {
    public static final String EVALUATED_IN_CONDITION_VALUE = "via-condition-addtourl";
    public static final String EVALUATED_IN_HTTP_GET_VALUE = "via-http-get";

    private static final IntegrationTestCache INSTANCE = new IntegrationTestCache();

    private IntegrationTestCache() {}

    private final Map<String, String> poorMansCache = new HashMap<>();

    public String get(String key, Supplier<String> valueProvider) {
        return poorMansCache.computeIfAbsent(key, k -> valueProvider.get());
    }

    public void clear() {
        poorMansCache.clear();
    }

    public static IntegrationTestCache getInstance() {
        return INSTANCE;
    }
}
