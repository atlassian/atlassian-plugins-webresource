package com.atlassian.webresource.refapp.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
class ApplicationRootConfiguration {

    @Bean
    PageBuilderService pageBuilderService() {
        return importOsgiService(PageBuilderService.class);
    }

    @Bean
    WebResourceUrlProvider webResourceUrlProvider() {
        return importOsgiService(WebResourceUrlProvider.class);
    }
}
