package com.atlassian.webresource.refapp.servlet;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.lang.NonNull;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;

public class I18nServlet extends HttpServlet {
    private static final String TEMPLATE_HTML_CONTENT_TYPE = "text/html;charset=UTF-8";
    private static final String QUERY_PARAM_PHASE = "phase";
    private final PageBuilderService pageBuilderService;

    @Inject
    public I18nServlet(@NonNull PageBuilderService pageBuilderService) {
        this.pageBuilderService = requireNonNull(pageBuilderService);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String completeModuleKey = getCompleteModuleKey(request);
        final ResourcePhase resourcePhase = getPhaseKey(request);

        response.setContentType(TEMPLATE_HTML_CONTENT_TYPE);
        response.getWriter().write("<html><body><p>Testing I18n web-resources. Check browser console.</p>");
        pageBuilderService
                .assembler()
                .resources()
                .excludeSuperbatch()
                .requireWebResource(resourcePhase, completeModuleKey);
        pageBuilderService
                .assembler()
                .assembled()
                .drainIncludedResources()
                .writeHtmlTags(response.getWriter(), UrlMode.AUTO);
        response.getWriter().write("</body></html>");
    }

    private String getCompleteModuleKey(final HttpServletRequest request) {
        final String requestPath = request.getPathInfo();
        final int requestContextIndex = requestPath.lastIndexOf('/') + 1;
        return requestPath.substring(requestContextIndex);
    }

    private ResourcePhase getPhaseKey(final HttpServletRequest request) {
        final String desiredPhase = request.getParameter(QUERY_PARAM_PHASE);
        return stream(ResourcePhase.values())
                .filter(resourcePhase -> resourcePhase.name().equalsIgnoreCase(desiredPhase))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("phase does not exist"));
    }
}
