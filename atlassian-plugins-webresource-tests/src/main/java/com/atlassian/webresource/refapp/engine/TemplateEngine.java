package com.atlassian.webresource.refapp.engine;

import java.util.HashMap;
import java.util.Map;

import org.springframework.lang.NonNull;

public interface TemplateEngine {

    @NonNull
    default String render(@NonNull final String templateName) {
        return this.render(templateName, new HashMap<>());
    }

    /**
     * Render a certain template by its path and inject the context into it.
     * @param templateName The template path.
     * @param context The context used to build the template.
     * @return The final string containing the generated content.
     */
    @NonNull
    String render(@NonNull String templateName, @NonNull Map<String, Object> context);
}
