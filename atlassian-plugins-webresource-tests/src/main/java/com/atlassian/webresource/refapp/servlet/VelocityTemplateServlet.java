package com.atlassian.webresource.refapp.servlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import com.atlassian.webresource.refapp.engine.TemplateEngine;

@Component
class VelocityTemplateServlet extends TemplateServlet {

    @Autowired
    VelocityTemplateServlet(@NonNull @Qualifier("velocityTemplateEngine") final TemplateEngine velocityTemplateEngine) {
        super(velocityTemplateEngine);
    }
}
