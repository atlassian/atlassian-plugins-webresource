package com.atlassian.webresource.refapp.servlet;

import java.io.IOException;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.lang.NonNull;

import com.atlassian.webresource.refapp.engine.TemplateEngine;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;

class TemplateServlet extends IntegrationTestHttpServlet {
    private static final String TEMPLATE_HTML_CONTENT_TYPE = "text/html;charset=UTF-8";

    private final TemplateEngine templateEngine;

    TemplateServlet(@NonNull final TemplateEngine templateEngine) {
        this.templateEngine = requireNonNull(templateEngine, "The template engine is mandatory.");
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
        // Do integration test things.
        super.doGet(request, response);

        // Determine which template to invoke.
        final String actionPath = getActionPath(request);
        try {
            final String content = getRenderEventHandlerByAction(actionPath)
                    .map(runnable -> {
                        runnable.handleBeforeRender();
                        return templateEngine.render(actionPath, runnable.getContext());
                    })
                    .orElseGet(() -> templateEngine.render(actionPath));

            response.setContentType(TEMPLATE_HTML_CONTENT_TYPE);
            response.getWriter().write(content);
        } catch (final IOException exception) {
            final String errorMessage = String.format("Error while rendering the template %s.", actionPath);
            throw new IllegalArgumentException(errorMessage);
        } finally {
            getRenderEventHandlerByAction(actionPath).ifPresent(RenderEventHandler::handleAfterRender);
        }
    }

    @NonNull
    Optional<RenderEventHandler> getRenderEventHandlerByAction(@NonNull final String action) {
        return empty();
    }

    private String getActionPath(final HttpServletRequest request) {
        final String requestPath = request.getPathInfo();
        final int requestContextIndex = requestPath.lastIndexOf('/') + 1;
        return requestPath.substring(requestContextIndex);
    }
}
