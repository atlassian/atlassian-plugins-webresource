package com.atlassian.webresource.refapp.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:atlassian-plugins-webresource-it-plugin.properties")
public class ApplicationProperties {
    @Value("${engine.velocity.base-url}")
    private String engineVelocityBaseUrl;

    @NonNull
    public String getEngineVelocityBaseUrl() {
        return engineVelocityBaseUrl;
    }
}
