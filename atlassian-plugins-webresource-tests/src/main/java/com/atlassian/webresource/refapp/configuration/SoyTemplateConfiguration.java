package com.atlassian.webresource.refapp.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atlassian.soy.renderer.SoyTemplateRenderer;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
class SoyTemplateConfiguration {

    @Bean
    SoyTemplateRenderer soyTemplateRenderer() {
        return importOsgiService(SoyTemplateRenderer.class);
    }
}
