package com.atlassian.webresource.refapp.servlet;

import java.util.HashMap;
import java.util.Map;

import org.springframework.lang.NonNull;

import com.atlassian.soy.renderer.SanitizedString;
import com.atlassian.webresource.refapp.util.TemplateRenderHelper;

public class SoyRenderEventHandler extends RenderEventHandler {
    private static final String PAGE_META_TAGS = "metaTags";
    private static final String CONSOLE_LOGGER_DEPENDENCY = "consoleLogger";
    private static final String PAGE_CONTENT_BODY = "htmlTags";

    SoyRenderEventHandler(
            @NonNull final Runnable beforeRenderRunnable,
            @NonNull final Map<String, Object> additionalContext,
            @NonNull final TemplateRenderHelper templateRenderHelper) {
        super(beforeRenderRunnable, () -> {}, () -> {
            final Map<String, Object> context = new HashMap<>(additionalContext);
            // Flush resources for the request.
            context.put(PAGE_META_TAGS, new SanitizedString(templateRenderHelper.renderRequiredMetaTags()));
            context.put(PAGE_CONTENT_BODY, new SanitizedString(templateRenderHelper.renderRequiredResources()));
            // Addition of custom console logger for test analysis. We flush it second in an attempt to
            // control what ends up in the flush; it should be just the console logger.
            context.put(CONSOLE_LOGGER_DEPENDENCY, new SanitizedString(templateRenderHelper.renderConsoleLogger()));
            return context;
        });
    }

    SoyRenderEventHandler(
            @NonNull final Runnable beforeRenderRunnable, @NonNull final TemplateRenderHelper templateRenderHelper) {
        this(beforeRenderRunnable, new HashMap<>(), templateRenderHelper);
    }
}
