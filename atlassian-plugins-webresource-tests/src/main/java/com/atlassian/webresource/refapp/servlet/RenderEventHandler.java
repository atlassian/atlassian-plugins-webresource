package com.atlassian.webresource.refapp.servlet;

import java.util.Map;
import java.util.function.Supplier;

import org.springframework.lang.NonNull;

import static java.util.Objects.requireNonNull;

/**
 * Represent a render task responsible for configuring the environment according to render events.
 *
 * @since 5.0.0
 */
class RenderEventHandler {
    private final Runnable beforeRenderRunnable;
    private final Runnable afterRenderRunnable;
    private final Supplier<Map<String, Object>> contextSupplier;

    RenderEventHandler(
            @NonNull final Runnable beforeRenderRunnable,
            @NonNull final Runnable afterRenderRunnable,
            @NonNull final Supplier<Map<String, Object>> contextSupplier) {
        this.contextSupplier = requireNonNull(contextSupplier, "The context is mandatory.");
        this.beforeRenderRunnable = requireNonNull(beforeRenderRunnable, "The set up runnable is mandatory.");
        this.afterRenderRunnable = requireNonNull(afterRenderRunnable, "The tear down runnable is mandatory.");
    }

    /**
     * Set up the environment before the rendering execution.
     */
    public void handleBeforeRender() {
        this.beforeRenderRunnable.run();
    }

    /**
     * Rollback the environment after the rendering execution.
     */
    public void handleAfterRender() {
        this.afterRenderRunnable.run();
    }

    /**
     * Retrieves the context used to render a page.
     *
     * @return The context used to build the page.
     */
    @NonNull
    public Map<String, Object> getContext() {
        return this.contextSupplier.get();
    }
}
