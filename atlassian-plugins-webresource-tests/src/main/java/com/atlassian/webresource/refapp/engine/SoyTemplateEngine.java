package com.atlassian.webresource.refapp.engine;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import com.atlassian.soy.renderer.SoyTemplateRenderer;

import static java.util.Objects.requireNonNull;

@Component
class SoyTemplateEngine implements TemplateEngine {
    private final SoyTemplateRenderer renderer;

    @Autowired
    SoyTemplateEngine(@NonNull final SoyTemplateRenderer soyTemplateRenderer) {
        this.renderer = requireNonNull(soyTemplateRenderer, "The template renderer is mandatory.");
    }

    @NonNull
    @Override
    public String render(@NonNull final String templateName, @NonNull final Map<String, Object> context) {
        final Writer writer = new StringWriter();
        renderer.render(
                writer,
                "com.atlassian.plugins.atlassian-plugins-webresource-tests:page-with-resources",
                "Atlassian.WebResource.ItTests." + templateName,
                context);
        return writer.toString();
    }
}
