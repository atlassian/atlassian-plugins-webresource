package com.atlassian.webresource.refapp.util;

import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static java.util.Objects.requireNonNull;

import static com.atlassian.webresource.api.UrlMode.ABSOLUTE;
import static com.atlassian.webresource.api.UrlMode.AUTO;
import static com.atlassian.webresource.api.UrlMode.RELATIVE;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INLINE;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.defaultPhase;

/**
 * Helper used to perform the management of the render of an HTML content.
 * It will allow to perform actions like require for resources and rendering the required contents.
 *
 * @since 5.0.0
 */
@Component
public class TemplateRenderHelper {
    private static final String CONSOLE_LOGGER_WEBRESOURCE_KEY =
            "com.atlassian.plugins.atlassian-plugins-webresource-tests:consoleLogger";

    private final PageBuilderService pageBuilderService;
    private final WebResourceUrlProvider webResourceUrlProvider;

    public TemplateRenderHelper(
            @NonNull final PageBuilderService pageBuilderService,
            @NonNull final WebResourceUrlProvider webResourceUrlProvider) {
        this.pageBuilderService = requireNonNull(pageBuilderService, "The page builder is mandatory.");
        this.webResourceUrlProvider =
                requireNonNull(webResourceUrlProvider, "The web-resource URL provider is mandatory.");
    }

    public String getWebResourceUrl(@NonNull final String webResourceKey, @NonNull final String fileName) {
        return "src=\"" + webResourceUrlProvider.getStaticPluginResourceUrl(webResourceKey, fileName, RELATIVE) + '"';
    }

    public void excludeSuperBatch() {
        pageBuilderService.assembler().resources().excludeSuperbatch();
    }

    public void populateContextPath() {
        pageBuilderService
                .assembler()
                .data()
                .requireData(
                        "com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path.context-path",
                        new JsonableString(webResourceUrlProvider.getBaseUrl(RELATIVE)));
    }

    public void requireWebResource(@NonNull final String key) {
        requireNonNull(key, "The web resource key is mandatory.");
        requireWebResource(defaultPhase(), key);
    }

    public void requireWebResource(@NonNull ResourcePhase resourcePhase, @NonNull final String key) {
        requireNonNull(key, "The web resource key is mandatory.");
        pageBuilderService.assembler().resources().requireWebResource(resourcePhase, key);
    }

    public void requireContext(@NonNull ResourcePhase resourcePhase, @NonNull final String key) {
        requireNonNull(key, "The context key is mandatory.");
        pageBuilderService.assembler().resources().requireContext(resourcePhase, key);
    }

    public void requireContext(@NonNull final String key) {
        requireNonNull(key, "The context key is mandatory.");
        requireContext(defaultPhase(), key);
    }

    @NonNull
    public String renderConsoleLogger() {
        requireWebResource(INLINE, CONSOLE_LOGGER_WEBRESOURCE_KEY);
        return renderRequiredResources();
    }

    /**
     * Render all the import tags for the current required resources.
     *
     * @return A {@link String} containing all the imported tags.
     */
    @NonNull
    public String renderRequiredResources() {
        final StringWriter htmlTagsWriter = new StringWriter();
        pageBuilderService.assembler().assembled().drainIncludedResources().writeHtmlTags(htmlTagsWriter, AUTO);
        return htmlTagsWriter.toString();
    }

    @NonNull
    public String renderRequiredMetaTags() {
        String baseUrl = webResourceUrlProvider.getBaseUrl(ABSOLUTE);

        try {
            var uri = new URI(baseUrl);

            return String.format(
                    """
               <meta name="ajs-server-scheme" content="%1s">
               <meta name="ajs-server-port" content="%2s">
               <meta name="ajs-server-name" content="%3s">
               <meta name="ajs-base-url" content="%4s">
               """,
                    uri.getScheme(), uri.getPort(), uri.getHost(), baseUrl);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Invalid base URL", e);
        }
    }
}
