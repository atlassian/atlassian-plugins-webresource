package com.atlassian.webresource.refapp.servlet;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import com.atlassian.webresource.refapp.engine.TemplateEngine;
import com.atlassian.webresource.refapp.util.TemplateRenderHelper;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.DEFER;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INLINE;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE;

@Component
class SoyTemplateServlet extends TemplateServlet {
    private static final String PLUGIN_ID = "com.atlassian.plugins.atlassian-plugins-webresource-tests";

    private final Map<String, RenderEventHandler> handlers;

    @Autowired
    SoyTemplateServlet(
            @NonNull @Qualifier("soyTemplateEngine") final TemplateEngine soyTemplateEngine,
            @NonNull final TemplateRenderHelper templateRenderHelper) {
        super(soyTemplateEngine);
        handlers = new HashMap<>();
        handlers.put(
                "cssMedia",
                new SoyRenderEventHandler(
                        () -> templateRenderHelper.requireContext("css_batching"), templateRenderHelper));
        handlers.put(
                "i18n",
                new SoyRenderEventHandler(
                        () -> {
                            templateRenderHelper.requireWebResource(
                                    "com.atlassian.plugins.atlassian-plugins-webresource-plugin:i18n");
                            templateRenderHelper.requireWebResource(
                                    PLUGIN_ID + ":atlassian-plugins-webresource-it-plugin-resources-i18n-standard");
                        },
                        templateRenderHelper));
        handlers.put(
                "wrmRequire",
                new SoyRenderEventHandler(
                        () -> templateRenderHelper.requireWebResource(PLUGIN_ID + ":test-resources-after-wrm-require"),
                        templateRenderHelper));
        handlers.put(
                "wrmRequireInline",
                new SoyRenderEventHandler(
                        () -> {
                            templateRenderHelper.requireWebResource(
                                    INLINE, PLUGIN_ID + ":wrm-conditional-diamond-right");
                            templateRenderHelper.requireWebResource(
                                    INLINE, PLUGIN_ID + ":wrm-conditional-diamond-wedge-1");
                            templateRenderHelper.requireWebResource(
                                    INLINE, PLUGIN_ID + ":wrm-conditional-diamond-left");
                        },
                        templateRenderHelper));
        handlers.put(
                "wrmRequireLazilyAfterInline",
                new SoyRenderEventHandler(
                        () -> {
                            templateRenderHelper.requireWebResource(
                                    INLINE, PLUGIN_ID + ":atlassian-plugins-webresource-it-plugin-resources-42");
                            templateRenderHelper.requireWebResource(INLINE, PLUGIN_ID + ":wrm-require-lazily-resource");
                        },
                        templateRenderHelper));
        handlers.put(
                "wrmRequireLazilyAfterRequire",
                new SoyRenderEventHandler(
                        () -> {
                            templateRenderHelper.requireWebResource(
                                    REQUIRE, PLUGIN_ID + ":atlassian-plugins-webresource-it-plugin-resources-42");
                            templateRenderHelper.requireWebResource(
                                    REQUIRE, PLUGIN_ID + ":wrm-require-lazily-resource");
                        },
                        templateRenderHelper));
        handlers.put(
                "alltest",
                new SoyRenderEventHandler(
                        () -> {
                            templateRenderHelper.requireWebResource(REQUIRE, PLUGIN_ID + ":first-feature");
                            templateRenderHelper.requireContext(DEFER, "all");
                        },
                        templateRenderHelper));
        handlers.put(
                "wrmConditionalDiamond",
                new SoyRenderEventHandler(
                        () -> {
                            templateRenderHelper.requireWebResource(
                                    REQUIRE, PLUGIN_ID + ":wrm-conditional-diamond-left");
                            templateRenderHelper.requireWebResource(
                                    DEFER, PLUGIN_ID + ":wrm-conditional-diamond-wedge-1");
                            templateRenderHelper.requireWebResource(
                                    INTERACTION, PLUGIN_ID + ":wrm-conditional-diamond-right");
                        },
                        templateRenderHelper));
    }

    @NonNull
    @Override
    public Optional<RenderEventHandler> getRenderEventHandlerByAction(@NonNull final String action) {
        requireNonNull(action, "The action is mandatory");
        return ofNullable(handlers.get(action));
    }
}
