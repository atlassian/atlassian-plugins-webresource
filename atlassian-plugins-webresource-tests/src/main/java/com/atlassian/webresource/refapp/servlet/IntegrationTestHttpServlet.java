package com.atlassian.webresource.refapp.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.webresource.refapp.cache.IntegrationTestCache;

import static com.atlassian.webresource.refapp.cache.IntegrationTestCache.EVALUATED_IN_HTTP_GET_VALUE;

public class IntegrationTestHttpServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        // Update state for an integration test. We assume 1 test == 1 GET. Let's hope that lasts...
        IntegrationTestCache.getInstance().clear();
        for (String key : req.getParameterMap().keySet()) {
            IntegrationTestCache.getInstance().get(key, () -> EVALUATED_IN_HTTP_GET_VALUE);
        }
    }
}
