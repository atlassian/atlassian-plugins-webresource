package com.atlassian.webresource.refapp.condition;

import java.util.Map;
import java.util.Optional;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.refapp.cache.IntegrationTestCache;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

import static com.atlassian.webresource.refapp.cache.IntegrationTestCache.EVALUATED_IN_CONDITION_VALUE;
import static com.atlassian.webresource.refapp.cache.IntegrationTestCache.EVALUATED_IN_HTTP_GET_VALUE;

/**
 * WRM conditions get used when walking the dependency graph, but in two discrete phases:
 *
 * 1) When responding to a client's initial request for a page
 * 2) When serving additional assets to that client for the same page
 *
 * When the condition is evaluated for the first time, the result needs to be frozen for this client,
 * so that all subsequent graph walks or subsequent HTTP requests they make for that page also
 * evaluate the same way.
 *
 * In scenario (1), all system state is available to the condition.
 * The WRM will call `addToUrl`, but from behind a cache. It is assumed the frozen condition evaluation
 * is serialized to the URL query params.
 *
 * In scenario (2), it is assumed the frozen value is present in the URL query params,
 * so the WRM calls `shouldDisplay`.
 *
 * Now, we use this particular condition in acceptance tests to verify graph walks work when conditions
 * evaluate a certain way. Conditions don't have access to the currently-executing HTTP request/response
 * neither in scenario (1) nor (2), so I needed a way to persist the state such that the test could affect
 * the condition evaluation but also interrogate the number of times it was accessed or updated.
 */
public class IntegrationTestCacheCheckingUrlReadingCondition implements UrlReadingCondition {
    private final IntegrationTestCache cache = IntegrationTestCache.getInstance();
    private Optional<String> keyToCheck = Optional.empty();

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
        keyToCheck = Optional.ofNullable(params.get("key"));
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder) {
        if (keyToCheck.isPresent()) {
            final String key = keyToCheck.get();
            urlBuilder.addToQueryString(key, cache.get(key, () -> EVALUATED_IN_CONDITION_VALUE));
        }
    }

    @Override
    public boolean shouldDisplay(QueryParams params) {
        return keyToCheck.isPresent() && EVALUATED_IN_HTTP_GET_VALUE.equals(params.get(keyToCheck.get()));
    }
}
