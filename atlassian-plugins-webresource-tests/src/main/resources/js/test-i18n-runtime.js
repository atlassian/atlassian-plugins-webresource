(function(aWindow) {
    let datas = aWindow.scriptLoadOrder = aWindow.scriptLoadOrder || [];

    const log = (...args) => {
        datas.push(args.join(' '));
        console.log(...args);
    }

    const keysToCheck = ['foo.bar.baz', 'foo.bar.compiled', 'foo.bar.issue-count'];

    log("Contain keys: " + keysToCheck.every(key => WRM.I18n.km.has(key)));
    log("Simple text: ", WRM.I18n.getText('foo.bar.baz'));
    log("Format text: ", WRM.I18n.getText('foo.bar.issue-count', 10));
    log("Another plain text: ", WRM.I18n.getText('foo.bar.another'));
}(window))
