(function(aWindow) {
    var scriptName = "require-resource-loaded";

    aWindow.scriptLoadOrder = aWindow.scriptLoadOrder || [];
    aWindow.scriptLoadOrder.push(scriptName);

    console.log(scriptName+" loaded!");

    WRM.require("wr!com.atlassian.plugins.atlassian-plugins-webresource-tests:atlassian-plugins-webresource-it-plugin-resources-1", function() {
        var scriptName = "require-resource-resolved";
        aWindow.scriptLoadOrder = aWindow.scriptLoadOrder || [];
        aWindow.scriptLoadOrder.push(scriptName);

        console.log(scriptName+" loaded!");
    });
})(window);