(function(aWindow) {
    var scriptName = "webresource-it-test-42";
    aWindow.scriptLoadOrder = aWindow.scriptLoadOrder || [];
    aWindow.scriptLoadOrder.push(scriptName);

    console.log(scriptName+" loaded!");
})(window);
