(function(aWindow) {
    const header = 'require-lazily--';
    const scriptName = `${header}resource-loaded`;

    aWindow.scriptLoadOrder = aWindow.scriptLoadOrder || [];
    aWindow.scriptLoadOrder.push(scriptName);

    console.log(`${scriptName} loaded!`);

    const loadAsyncResources = () => {
        aWindow.scriptLoadOrder.push('async-resource-function-called');
        console.log('async resource function invoked!');

        WRM.require("wr!com.atlassian.plugins.atlassian-plugins-webresource-tests:atlassian-plugins-webresource-it-plugin-resources-1", () => {
            const scriptName = `${header}require-resource-resolved`;
            aWindow.scriptLoadOrder.push(scriptName);

            console.log(`${scriptName} resolved!`);
        });

        WRM.requireLazily("wr!com.atlassian.plugins.atlassian-plugins-webresource-tests:atlassian-plugins-webresource-it-plugin-resources-3", () => {
            const scriptName = `${header}lazy-resource-resolved`;
            aWindow.scriptLoadOrder.push(scriptName);

            console.log(`${scriptName} resolved!`);
        });

        WRM.require("wr!com.atlassian.plugins.atlassian-plugins-webresource-tests:atlassian-plugins-webresource-it-plugin-resources-2", () => {
            const scriptName = `${header}require-resource-2-resolved`;
            aWindow.scriptLoadOrder.push(scriptName);

            console.log(`${scriptName} resolved!`);
        });

        // this should have been loaded in an earlier phase already.
        // Tests will verify that it doesn't re-load, but the resolved callback should fire.
        WRM.requireLazily("wr!com.atlassian.plugins.atlassian-plugins-webresource-tests:atlassian-plugins-webresource-it-plugin-resources-42", () => {
            const scriptName = `${header}require-resource-42-resolved`;
            aWindow.scriptLoadOrder.push(scriptName);

            console.log(`${scriptName} resolved!`);
        });
    };

    const afterDCL = (cb, delay = 100) => {
        const laterCb = () => setTimeout(cb, delay);
        if (document.readyState !== 'complete') {
            document.addEventListener('DOMContentLoaded', laterCb, {once: true});
        } else {
            laterCb();
        }
    }

    // I'm triggering all calls after a delay as a hack.
    // When testing in non-refapp products, we want to avoid any product WRM.require or WRM.requireLazily
    // calls queueing up before ours, which would have a material impact on the execution order.
    afterDCL(loadAsyncResources);

})(window);
