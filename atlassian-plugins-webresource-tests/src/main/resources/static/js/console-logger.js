(function (console, window) {
    function stringify(a) {
        return a instanceof ErrorEvent ? stringify(a.error) : JSON.parse(JSON.stringify(a, Object.getOwnPropertyNames(a)));
    }

    function serialize(arr) {
        return [...arr].map(stringify)
    }
    console.logs = [];

    console.stddebug = console.debug.bind(console);
    console.debug = function () {
        console.logs.push(...serialize(arguments));
        console.stddebug.apply(console, arguments);
    }

    console.stdlog = console.log.bind(console);
    console.log = function () {
        console.logs.push(...serialize(arguments));
        console.stdlog.apply(console, arguments);
    }

    console.warnings = [];
    console.stdwarn = console.warn.bind(console);
    console.warn = function () {
        console.warnings.push(...serialize(arguments));
        console.stdwarn.apply(console, arguments);
    }

    console.errors = [];
    console.stderr = console.error.bind(console);
    console.error = function () {
        console.errors.push(...serialize(arguments));
        console.stderr.apply(console, arguments);
    }

    window.addEventListener('error', function (message) {
      console.error(message);
    }, true);
}(console, window))
