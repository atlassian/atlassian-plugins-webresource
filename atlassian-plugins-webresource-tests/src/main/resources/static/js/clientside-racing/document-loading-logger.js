console.log(`document is now in ${document.readyState} state`);
document.addEventListener("readystatechange", () => {
    console.log(`document is now in ${document.readyState} state`)
});
document.addEventListener("DOMContentLoaded", () => {
    console.log('DOMContentLoaded was fired')
});
document.addEventListener("load", () => {
    console.log('load was fired')
});
