(function () {
    console.log("Simple text: " + WRM.I18n.getText('foo.bar.baz'));
    console.log("Compiled text: " + WRM.I18n.getText('foo.bar.compiled'));
    console.log("Format text: " + WRM.I18n.getText('foo.bar.issue-count', 10));
}())
