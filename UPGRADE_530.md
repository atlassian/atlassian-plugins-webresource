# Host application upgrade notes

## Translations

### Two-phase transforms

The JsI18nTransformer now supports an alternate strategy for adding translations at product runtime.

For every `[filename].js` file to be transformed, the WRM will check whether a `[filename].i18n.properties` file exists.
If it does, the WRM will assume that every key listed in the file is an i18n translation key in use in the `.js` file.

For instance, if you have the following javascript code in a `foo.js` file:

```javascript
WRM.I18n.getText('i18n.one');
WRM.I18n.getText('i18n.two', 2);
WRM.I18n.getText('i18n.three');
```

And you provided the following `foo.js.i18n.properties` file:

```properties
i18n.one=1
i18n.two=1
```

The WRM will assume this means the file only uses two translation keys: `i18n.one` and `i18n.two`.
When it runs, the transformer will prepend the contents of the javascript with a map of the used keys along
with their locale-specific translations:

```javascript
(k=>{k.put('i18n.one','translation string for i18n.one');k.put('i18n.two','translation string for i18n.two');})(WRM.I18n.km);
WRM.I18n.getText('i18n.one');
WRM.I18n.getText('i18n.two', 2);
WRM.I18n.getText('i18n.three');
```

The `WRM.I18n.getText` calls now perform a lookup of translation keys in a `Map` located at `WRM.I18n.km`.

If a properties file exists and is empty, the WRM will assume this means there are no translations needed for this file
and will skip the transformation entirely. This results in a significant reduction in processing time per file.

#### What to do

1. Ensure that your product provides the `com.atlassian.plugins.atlassian-plugins-webresource-plugin:i18n` web-resource
   in its superbatch as early as possible. This will define the `WRM.I18n` and `AJS.I18n` objects, then add the `km`
   map to them.

2. Review any overrides to the `WRM` and `AJS` variables on `window` and remove them.
   If the `WRM.I18n` object reference changes, the `km` value may not be accessible, resulting in JavaScript errors:

   ```text
   Uncaught TypeError: cannot read property `km` of undefined
   ```

#### How to generate the i18n usage files

To get the benefits of this two-phase transformation, developers will need to generate the `.i18n.properties` files.
These files could be written by hand, but that would be quite tedious. Instead, we expect that other build tools for JS
will receive plugins to generate these files at build-time. As a case-in-point, AMPS will be updated to support 
generation of these files as part of https://ecosystem.atlassian.net/browse/AMPS-1589.

#### Dark feature kill-switch

If, for whatever reason, this feature needs to be disabled, one of the following things should disable it:

* Setting an `atlassian.darkfeature.atlassian.webresource.twophase.js.i18n.disabled=true` system property.
* Setting `atlassian.webresource.twophase.js.i18n.disabled` to `true` via the product's `DarkFeatureManager` impl.

### Testing

The JsI18nTransformer now supports more complicated namespaces and reduced the maximum supported length of the
identifier prior to the `I18n.getText(`, thus there is a small chance that the output has changed. A blitz is
recommended during adoption. If any issues are found please ping in the #server-frontend channel.

Double check that the implementation of `WebResourceIntegration#getI18nText` and `WebResourceIntegration#getI18nRawText`
do return the key when nothing is found for that key. This was in the javadocs since 2014, but this was still doubly
implemented inside the WRM. With this minor version, that double implementation internal to the WRM has been removed.

### Refactoring

WriterOutputStream was removed, this was copy-pasted into the repo (from Apache commons IO 2.0.1). Usages in the
products and marketplace apps were not found. If this is not the case please report this in the #server-frontend
channel, so we can find the hole is our scanning process.

### Additions

For testing purposes the WRM client consumes global `window.WRM.__localeOverride` property which can be used to override locale used
to load resources. By default the locale returned from WRM REST API is used.
