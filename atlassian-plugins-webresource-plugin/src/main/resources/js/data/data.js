;(function(BAD_RETURN) {

    function parseJsonData(unparsedData) {
        if (!unparsedData) {
            return unparsedData;
        }
        return JSON.parse(unparsedData);
    }

    function _addCallback(key, cb, callbacks) {
        if (cb && !callbacks.has(key)) {
            callbacks.set(key, cb);
        }
    }

    function _extractData(key, unparsed, claimed) {
        try {
            var data = parseJsonData(unparsed[key]);
            delete unparsed[key]; // purge from unparsed data map
            claimed.set(key, true); // mark data as claimed
            return data;
        }
        catch (e) {
            console.error(`Exception extracting data with key '${key}'`, e);
            return BAD_RETURN;
        }
    }

    function _callbackLater(callback, data, key) {
        setTimeout(() => {
            try {
                callback(data);
            } catch (e) {
                console.error(`Exception calling data callback for '${key}'`, e);
            }
        });
    }

    /**
     * Tries to find any callbacks for data in `unparsed`. If we find a callback for a key,
     * but the value cannot be parsed, then we try and call a callback in `fallbackCallbacks`.
     *
     * @param unparsed Object that contains unparsed key-value pairs
     * @param {Map<String,boolean>} claimed
     * @param {Map<String,Function>} callbacks map of key->callback
     * @param {Map<String,Function>} fallbackCallbacks map of key->callback
     * @private
     */
    function _findAndCallCallbacks(unparsed, claimed, callbacks, fallbackCallbacks) {
        for (let k of Object.keys(unparsed)) {
            if (callbacks.has(k)) {
                const data = _extractData(k, unparsed, claimed);
                const callback = (data !== BAD_RETURN) ? callbacks.get(k) : fallbackCallbacks.get(k);
                callbacks.delete(k); // you only get it once
                fallbackCallbacks.delete(k); // you only get it once
                if (callback) {
                    _callbackLater(callback, data, k);
                }
            }
        }
    }

    /**
     * A constructor for the internal implementation of WRM.data. Can be instantiated
     * many times for testing, but the global instance is at WRM.data
     * @param globalHolder the object in which to find _unparsedData, etc. (typically window.WRM)
     * @return the data api object
     */
    WRM._createDataImpl = function(globalHolder) {
        globalHolder._unparsedData = globalHolder._unparsedData || {};
        globalHolder._unparsedErrors = globalHolder._unparsedErrors || {};
        globalHolder._claimedData = globalHolder._claimedData || new Map();

        /** @type Map<String, successCallback> */
        const successCallbacks = new Map();
        /** @type Map<String, failureCallback> */
        const failureCallbacks = new Map();

        /**
         * internal method, exposed so can be called from script tags output by the WRM
         */
        globalHolder._dataArrived = function() {
            _findAndCallCallbacks(globalHolder._unparsedData, globalHolder._claimedData, successCallbacks, failureCallbacks);
            _findAndCallCallbacks(globalHolder._unparsedErrors, globalHolder._claimedData, failureCallbacks, failureCallbacks);
        };

        return {
            /**
             * Called when the promise completed normally
             * @callback successCallback
             * @param {*} JSON data
             */

            /**
             * Called when the promise completed excpetionally or timed out. This callback takes no arguments.
             * @callback failureCallback
             */

            /**
             * Claims the data returned from the server under the given key.
             *
             * Data for a specific key is retrieved only once. Subsequent requests for the same key will return no data.
             * If the data is required by multiple callers, the suggested pattern here is to wrap an API eg
             * WRM.contextPath around the WRM.data.claim method and cache inside that external API - see context-path.js
             * in this directory for an example.
             *
             * This method has two forms: claim(key) that is synchronous, and claim(key, successcb, failurecb) that is
             * asynchronous.
             *
             * For synchronous callers:
             * Callers must ensure that any JS requesting data appears *after* the data is inserted into the page - this
             * is achieved via the caller expressing a web-resource <dependency> on the resource serving the data.
             *
             * For asynchronous callers:
             * Either the successcb or failurecb callbacks will be called. If the data is already known (either success,
             * or failure) then a callback will be invoked presently (on a timeout(0)).
             *
             * @param {string} key data key.
             * @param {successCallback} [successcb]
             * @param {failureCallback} [failurecb]
             * @returns {*|undefined} JSON data in the synchronous version, undefined in the async version.
             */
            claim(key, successcb, failurecb) {
                if (!successcb && !failurecb) {
                    if (globalHolder._claimedData.has(key)) {
                        console.error(`Data with key '${key}' has already been claimed`);
                        return BAD_RETURN;
                    }
                    // Lazily parses JSON data from WRM._unparsedData
                    // This is extremely defensive, but we use Object.prototype.hasOwnProperty as developers could include
                    // a piece of data with key "hasOwnProperty"
                    if (Object.hasOwnProperty.call(globalHolder._unparsedData, key)) {
                        return _extractData(key, globalHolder._unparsedData, globalHolder._claimedData); // synchronous
                    }
                } else { // async version
                    _addCallback(key, successcb, successCallbacks);
                    _addCallback(key, failurecb, failureCallbacks);
                    globalHolder._dataArrived(); // we might already have that data
                }
            }
        }
    };

    // create the global WRM.data instance
    WRM.data = WRM._createDataImpl(WRM);

    if (typeof define === 'function') {
        define('wrm/data', () => WRM.data);
    }

}());
