;(function() {
    const I18n = {
        /**
         * A map of translation keys to translated values.
         * @type {Map<string, string>}
         * @note it's called `km` and not `keys` for historical reasons...
         *       AUI ships an `AJS.I18n.keys` object that we do not want to conflict with.
         */
        km: new Map(),

        /**
         * A local reference to {@see formatString}.
         * @note this reference is used by {@see #getText} at runtime when translations were provided
         *       in {@see #km}, or when the js i18n transformer rewrites certain `I18n.getText` calls to
         *       `I18n.format` calls instead.
         */
        format: WRM.format,

        /**
         * Converts a translation key and optional parameters in to a translated value in the current user's locale.
         * @param key the i18n translation key to look up the translated value of
         * @param params any values that should be substituted in to the translation string.
         * @returns {string} the translated string, or the translation key itself if there was no translated value stored.
         */
        getText(key, ...params) {
            if (!I18n.km.has(key)) {
                console.debug(
                    `No translation found for '${key}'. Have you included the "jsI18n" transformation in your web-resource configuration?`
                );
                return key;
            }
            const val = I18n.km.get(key);
            return I18n.format.apply(null, [val, ...params]);
        },
    };

    if (typeof define === 'function') {
        define('wrm/i18n', () => I18n);
    }

    // Export to globals.
    WRM.I18n = I18n;
    // This used to be part of AUI, so preserve the legacy.
    AJS.I18n = I18n;
}());
