;(function () {
    /**
     * Replaces tokens in a string with arguments, similar to Java's MessageFormat.
     * Tokens are in the form {0}, {1}, {2}, etc.
     *
     * This version also provides support for simple choice formats (excluding floating point numbers) of the form
     * {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}
     *
     * Number format is currently not implemented, tokens of the form {0,number} will simply be printed as {0}
     *
     * @method format
     * @param message the message to replace tokens in
     * @param args (optional) replacement value for token {0}, with subsequent arguments being {1}, etc.
     * @return {String} the message with the tokens replaced
     * @usage formatString("This is a {0} test", "simple");
     */
    function formatString(message, ...args) { //eslint-disable-line
        const apos = /'(?!')/g; // finds "'", but not "''" // TODO: does not work for floating point numbers!
        const simpleFormat = /^\d+$/;
        const numberFormat = /^(\d+),\s*number\s*$/; // TODO: incomplete, as doesn't support floating point numbers
        const choiceFormat = /^(\d+),\s*choice\s*,\s*(.+)/;
        const choicePart = /^(\d+)\s*([#<])(.+)/;
        const pipe = /\|\s*/;
        // we are caching RegExps, so will not spend time on recreating them on each call

        // formats a value, currently choice and simple replacement are implemented, proper
        const getParamValue = function (format, args) {
            const simpleFormatMatch = format.match(simpleFormat);
            if (simpleFormatMatch !== null) { // TODO: heavy guns for checking whether format is a simple number...
                const argIndex = parseInt(format, 10);
                return args.length > argIndex ? args[argIndex] : ''; // use the argument as is, or use '' if not found
            }

            const numberFormatMatch = format.match(numberFormat);
            if (numberFormatMatch !== null) {
                // TODO: doesn't actually format the number...
                const argIndex = parseInt(numberFormatMatch[1], 10);
                return args.length > argIndex ? args[argIndex] : '';
            }

            const choiceFormatMatch = format.match(choiceFormat);
            if (choiceFormatMatch !== null) {
                // format: "0,choice,0#0 issues|1#1 issue|1<{0,number} issues"
                // match[0]: "0,choice,0#0 issues|1#1 issue|1<{0,number} issues"
                // match[1]: "0"
                // match[2]: "0#0 issues|1#1 issue|1<{0,number} issues"

                // get the argument value we base the choice on
                const argIndex = parseInt(choiceFormatMatch[1], 10);
                const value = (args.length > argIndex ? args[argIndex] : null);

                if (value === null) {
                    return '';
                }

                // go through all options, checking against the number, according to following formula,
                // if X <= the first entry then the first entry is returned, if X > last entry, the last entry is returned
                //
                //    X matches j if and only if limit[j] <= X < limit[j+1]
                //
                const options = choiceFormatMatch[2].split(pipe);
                let isInvalidFormat = false;
                let prevOptionValue = null; // holds last passed option
                let res = '';

                for (let i = 0; i < options.length; i++) {
                    // option: "0#0 issues"
                    // part[0]: "0#0 issues"
                    // part[1]: "0"
                    // part[2]: "#"
                    // part[3]" "0 issues";
                    const parts = options[i].match(choicePart);

                    if (parts == null) {
                        isInvalidFormat = true;
                        continue;
                    }
                    const argValue = parseInt(parts[1], 10);

                    // if value is equal the condition, and the match is equality match we accept it
                    if (value === argValue && parts[2] === '#') {
                        res = parts[3];
                        break;
                    }

                    // if value is smaller or equal, we take the previous value, or the current if no previous exists
                    if (value <= argValue) {
                        if (prevOptionValue) {
                            res = prevOptionValue;
                            break;
                        } else {
                            res = parts[3];
                            break;
                        }
                    }

                    // value is greater the condition, fall through to next iteration

                    // check whether we are the last option, in which case accept it even if the option does not match
                    if (i === options.length - 1) {
                        res = parts[3];
                    }

                    // retain current option
                    prevOptionValue = parts[3];
                }

                if (isInvalidFormat) {
                    console && console.error && console.error('The format "' + format + '" from message "' + message + '" is invalid.');
                }

                // run result through format, as the parts might contain substitutes themselves
                return formatString(res, ...args);
            }

            return '';
        };

        // drop in replacement for the token regex
        // splits the message to return the next occurrence of an i18n placeholder.
        // Does not use regexps as we need to support nested placeholders
        // text between single ticks ' are ignored
        const _performTokenRegex = function (message) {
            let tick = false;
            let openIndex = -1;
            let openCount = 0;
            for (let i = 0; i < message.length; i++) {
                // handle ticks
                const c = message.charAt(i);
                if (c === "'") {
                    // toggle
                    tick = !tick;
                }
                // skip if we are between ticks
                if (tick) {
                    continue;
                }
                // check open brackets
                if (c === '{') {
                    if (openCount === 0) {
                        openIndex = i;
                    }
                    openCount++;
                } else if (c === '}') {
                    if (openCount > 0) {
                        openCount--;
                        if (openCount === 0) {
                            // we found a bracket match - generate the result array (
                            let match = [];
                            match.push(message.substring(0, i + 1)); // from begin to match
                            match.push(message.substring(0, openIndex)); // everything until match start
                            match.push(message.substring(openIndex + 1, i)); // matched content
                            return match;
                        }
                    }
                }
            }
            return null;
        };

        const _formatString = function (message, args) {
            let res = '';

            if (!message) {
                return res;
            }

            let match = _performTokenRegex(message);

            while (match) {
                // reduce message to string after match
                message = message.substring(match[0].length);

                // add value before match to result
                res += match[1].replace(apos, '');

                // add formatted parameter
                res += getParamValue(match[2], args);

                // check for next match
                match = _performTokenRegex(message); //message.match(token);
            }
            // add remaining message to result
            res += message.replace(apos, '');
            return res;
        };

        return _formatString(message, args);
    }

    // Export to AMD.
    if (typeof define === 'function') {
        define('wrm/format', function() {
            return formatString;
        });
    }

    // Export to globals.
    WRM.format = formatString;
    // This used to be part of AUI, so preserve the legacy.
    AJS.format = formatString;
}());
