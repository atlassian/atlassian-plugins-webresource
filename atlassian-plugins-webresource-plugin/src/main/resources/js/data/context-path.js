;(function() {
    var contextPath = null;

    function getContextPath() {
        if (contextPath === null) {
            contextPath = WRM.data.claim('com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path.context-path');
        }
        return contextPath;
    }

    if (typeof define === 'function') {
        define('wrm/context-path', () => getContextPath);
    }

    // Export a global variable
    WRM.contextPath = getContextPath;
    // This used to be part of AUI, so preserve the legacy.
    AJS.contextPath = getContextPath;
}());
