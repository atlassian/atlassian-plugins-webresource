/**
 * @fileoverview
 * Define the global variables that the WRM needs in order to share its code.
 * We do it in one place so that we save bytes elsewhere.
 */

/** The webresource-manager's root namespace. All APIs are kept here. */
window.WRM || (window.WRM = {});

/**
 * The legacy namespace for "Atlassian JavaScript", which was also the first name
 * for the "Atlassian UI" (AUI) library. Functions that were extracted from
 * that library will be namespaced here... until we deprecate the API usage
 * from this namespace.
 */
window.AJS || (window.AJS = {});
