package com.atlassian.webresource.plugin.i18n;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.html.encode.JavascriptEncoder;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.Flags;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.transformer.AbstractTransformedDownloadableResource;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformableResource;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;
import com.atlassian.webresource.spi.transformer.TwoPhaseResourceTransformer;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

/**
 * Web resource transformer to translate i18n methods in JavaScript to the literal strings.
 * <p>
 * This transforms the content by pattern matching on the text AJS.I18n.getText("key")
 * where key can only contain letters, numbers or dots and hyphens. It replaces this syntax with the literal
 * string translation before serving the resource.
 * <p>
 * If a comma is found after the key instead of a closing brace ')', it will translate the string but also
 * wrap it in a WRM.format("translation", args) syntax so that the message can be formatted.
 *
 * @since 3.1.0
 */
public class JsI18nTransformer implements WebResourceTransformerFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsI18nTransformer.class);

    @VisibleForTesting
    static final String TWO_PHASE_JS_I18N_DISABLED = "atlassian.webresource.twophase.js.i18n.disabled";

    // Not a very sophisticated matcher. Doesn't check for variables after the key if a comma has been detected.
    private static final Pattern I18N_GET_TEXT_PATTERN = Pattern.compile(
            // PERF NOTE: Matching for different forms of I18n is about ~15% of the time taken
            "(?:\\.I18n|\\['I18n']|\\[\"I18n\"])"
                    + // PLUGWEB-454: Babel 6 and Babel 7 + Webpack 4 uses bracket notation
                    "\\.getText"
                    + "\\(\\s*"
                    + // open "function"
                    "(['\"])([\\w.-]+)\\1"
                    + // single or double quoted word
                    "\\s*([),])" // close of "function", or ',' character for formatting argument, whatever is first
            );

    // PERF NOTE: Matching for long identifiers is about ~5% of time taken, another way to solve this is by applying
    // this transform to individual files rather than a batch, likewise if this was re-written to taken advantage of the
    // semantics of newlines (although that advantage is diminishing with minifiers removing new lines)
    private static final int LONGEST_ACCEPTED_IDENTIFIER = 120;
    // These two are directly linked  1 starting char + 100 char + 12 ["default"] + 8 .default
    private static final Pattern SINGLE_IDENTIFIER_PATTERN = Pattern.compile("(" +
            // "identifiers can contain only alphanumeric characters (or "$" or "_"), and may not start
            // with a digit" https://developer.mozilla.org/en-US/docs/Glossary/Identifier
            "(?:\\p{Alpha}|[$_])(?:\\p{Alnum}|[$_]){0,100}"
            +
            // optional ["default"] for babel 5, see PLUGWEB-306
            "(?:\\[['\"]default['\"]])?"
            +
            // optional .default for babel 6
            "(?:\\.default)?"
            + ")$");

    private static final int LONGEST_PROP_ACCESSOR = 20;
    // These two are directly linked  1 char that could return an object + 9 space chars + 1 newline + 9 space
    private static final Pattern LONG_NAMESPACE_PROP_ACCESSOR_PATTERN = Pattern.compile("(" + "([.\\]]\\p{Space}{0,19})"
            + // Explicit property accessor
            "|"
            + // OR
            "([])$_\\p{Alnum}]\\p{Space}{0,9}\\n\\p{Space}{0,9})"
            + // Implicit property accessor
            ")$");

    private static final String I18N_CONSOLE_WARN =
            "console.warn('The I18n web-resource is missing, please add com.atlassian.plugins.atlassian-plugins-webresource-plugin:i18n as a dependency to the web-resource. Learn more: https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin-javascript');";
    private static final String FUNCTION_ARGS_SEPARATOR = ",";
    private static final String HASH_KEY = "locale-hash";
    private static final String QUERY_KEY = "locale";

    private final WebResourceIntegration webResourceIntegration;

    public JsI18nTransformer(WebResourceIntegration webResourceIntegration) {
        this.webResourceIntegration = webResourceIntegration;
    }

    private boolean isTwoPhaseJsI18nDisabled() {
        // While this should NOT be null, it might be due to testing or integration in to the product issues
        if (webResourceIntegration.getDarkFeatureManager() == null) {
            return false;
        }
        return webResourceIntegration
                .getDarkFeatureManager()
                .isEnabledForAllUsers(TWO_PHASE_JS_I18N_DISABLED)
                .orElse(false);
    }

    private final class JsI18nUrlReadingWebResourceTransformer
            implements UrlReadingWebResourceTransformer, TwoPhaseResourceTransformer {
        private Map<String, String> twoPhaseProps;

        @Override
        public boolean hasTwoPhaseProperties() {
            return twoPhaseProps != null;
        }

        @Override
        public void loadTwoPhaseProperties(
                ResourceLocation resourceLocation, Function<String, InputStream> loadFromFile) {
            twoPhaseProps = null;

            // Bail on the optimisation if it's disabled by dark feature flags.
            if (isTwoPhaseJsI18nDisabled()) {
                return;
            }

            final String filepath = resourceLocation.getLocation();
            if (filepath == null) {
                LOGGER.debug("ResourceLocation has no location value. This should never happen.");
                return;
            }
            // todo: consolidate path resolution for minified files here and in InitialMinifiedContentBuilder
            final String filePropertiesPath = filepath.replaceAll("[-.]min\\.js$", ".js") + ".i18n.properties";

            try {
                final InputStream postProcessedPropertiesStream = loadFromFile.apply(filePropertiesPath);

                // Only set props when there actually a file where we expected it to be
                if (null != postProcessedPropertiesStream) {
                    final Map<String, String> map = new HashMap<>();
                    final Properties postProcessedProperties = new Properties();
                    postProcessedProperties.load(postProcessedPropertiesStream);
                    for (Map.Entry<Object, Object> entry : postProcessedProperties.entrySet()) {
                        map.put((String) entry.getKey(), (String) entry.getValue());
                    }
                    twoPhaseProps = Collections.unmodifiableMap(map);
                }
            } catch (IOException e) {
                LOGGER.debug("Resource `{}` has no backing `{}` file", filepath, filePropertiesPath);
            }
        }

        @Override
        public DownloadableResource transform(TransformableResource transformableResource, QueryParams params) {
            final Locale locale = getLocaleFromQueryParams(params);
            final DownloadableResource original = transformableResource.nextResource();

            // Try for a more efficient annotation-style transformation first,
            // in which we prepend the used translation keys to the top of the file, along
            // with their specific translations.
            if (hasTwoPhaseProperties()) {
                Collection<String> usedI18nKeys = twoPhaseProps.keySet();

                // Special case: the build-time process determined there was no i18n usage in this file,
                // so let's skip the expenses of cloning the output stream, searching for keys, etc.
                if (usedI18nKeys.isEmpty()) {
                    return original;
                }

                // Instead of doing in-place modification of getText calls, we will prepend used keys
                // to the start of the content.
                return new AbstractTransformedDownloadableResource(original) {

                    @Override
                    public void streamResource(OutputStream out) throws DownloadException {
                        // There are i18n keys in use in this file.
                        // Prepend all translations to a global Map at
                        // `WRM.I18n.km` so they can be looked up by the
                        // `WRM.I18n.getText` function at runtime.
                        // todo: could do multilang support @ runtime if we had a map per locale...
                        final StringBuilder builder = new StringBuilder();
                        if (Flags.isDevMode()) {
                            // For people still using atlassian-browsers 3.0 (Chrome 76 which is pre-optional chaining)
                            builder.append("if (!(window.WRM && window.WRM.I18n )) {");
                            builder.append("\n");
                            builder.append(I18N_CONSOLE_WARN);
                            builder.append("\n");
                            builder.append("}");
                            builder.append("\n");
                        }
                        builder.append("(k=>{");
                        usedI18nKeys.stream().sorted().forEach(key -> {
                            final String val = webResourceIntegration.getI18nRawText(locale, key);
                            builder.append("k.set('")
                                    .append(jsEncode(key))
                                    .append("','")
                                    .append(jsEncode(val))
                                    .append("');");
                        });
                        builder.append("})(WRM.I18n.km);\n");

                        try {
                            // Write the map data first
                            out.write(builder.toString().getBytes(StandardCharsets.UTF_8));

                            // Write the original JS output second
                            original.streamResource(out);
                        } catch (IOException e) {
                            throw new DownloadException("Could not prepend i18n keys", e);
                        }
                    }
                };
            }

            //
            // Fall back to the just-in-time approach...
            //

            return new CharSequenceDownloadableResource(original) {
                @Override
                // PERF NOTE: Just getting to the point of calling this function consumes 20% of the time
                // It's mostly just spent turning streams -> charSequences before every transformer in the chain.
                // PERF NOTE: Building the output string consumes about ~30% of time
                protected CharSequence transform(final CharSequence originalContent) {
                    // Matches the `I18n.getText(..` bit
                    final Matcher i18nMatcher = I18N_GET_TEXT_PATTERN.matcher(originalContent);
                    // Matches a single identifier in front of `I18n.getText(`
                    final Matcher singleIdentifierMatcher = SINGLE_IDENTIFIER_PATTERN.matcher(originalContent);
                    // Matches if the namespace is longer than a single identifier
                    final Matcher longNamespaceMatcher = LONG_NAMESPACE_PROP_ACCESSOR_PATTERN.matcher(originalContent);
                    final StringBuffer outputToReturn = new StringBuffer(); // Requires JDK 11 to be a StringBuilder

                    // PERF NOTE: Just finding the next `I18n.getText(..` accounts for about 25% of time taken.
                    while (i18nMatcher.find()) {
                        limitMatcherUptoStartOfAPreviousMatch(
                                i18nMatcher, singleIdentifierMatcher, LONGEST_ACCEPTED_IDENTIFIER);
                        // PERF NOTE: Consumes about ~5% of time [checking to make sure it's safe to replace]
                        // Looks for an identifier in front of `I18n.getText(`.
                        if (!singleIdentifierMatcher.find()) {
                            // Do nothing, we are going to play it safe and not replace anything since there was
                            // nothing before the `I18n.getText.....` call because we expect it to be on the AJS
                            // or WRM global objects (or renamed because of bundlers).
                            continue;
                        }

                        final String translationKey = i18nMatcher.group(2);
                        // If theres a ',' instead of a ')' after the `I18n.getText('key'` then string interpolation is
                        // used and thus the WRM#format must be called to do it.
                        final boolean formatFnNeeded = FUNCTION_ARGS_SEPARATOR.equals(i18nMatcher.group(3));

                        final StringBuilder replacedTextStringBuilder = new StringBuilder();

                        // It would be possible to simplify the complexity below entirely to a single function call if
                        // it was safe to make the assumption that WRM#format is always loaded true. It is not a safe
                        // assumption for backwards compatibility reasons with pages that may not need to use WRM#format
                        // at all AND thus may not have added it as a dependency (they would error because the function
                        // would not exist).
                        if (formatFnNeeded) {
                            replaceI18nGetTextCallWithACallToWrmFormat(
                                    i18nMatcher,
                                    outputToReturn,
                                    translationKey,
                                    replacedTextStringBuilder,
                                    locale,
                                    webResourceIntegration,
                                    formatFnNeeded);
                        } else {
                            limitMatcherUptoStartOfAPreviousMatch(
                                    singleIdentifierMatcher, longNamespaceMatcher, LONGEST_PROP_ACCESSOR);
                            // Checks if the namespace is longer than a single identifier
                            if (longNamespaceMatcher.find()) {
                                replaceI18nGetTextCallWithACallToWrmFormat(
                                        i18nMatcher,
                                        outputToReturn,
                                        translationKey,
                                        replacedTextStringBuilder,
                                        locale,
                                        webResourceIntegration,
                                        formatFnNeeded);
                            } else {
                                replaceNameSpaceAndI18nGetTextCallWithJustTheFormattedTranslationString(
                                        i18nMatcher,
                                        singleIdentifierMatcher,
                                        outputToReturn,
                                        translationKey,
                                        replacedTextStringBuilder,
                                        locale,
                                        webResourceIntegration);
                            }
                        }

                        outputToReturn.append(replacedTextStringBuilder);
                    }

                    // Append everything after the last captured `I18n.getText(` match to the output
                    i18nMatcher.appendTail(outputToReturn);
                    return outputToReturn;
                }
            };
        }
    }

    /**
     * Returns the locale from query params
     *
     * @param params query parameters
     * @return the locale discovered in the query params, or the application's default locale if none is found
     */
    private static Locale getLocaleFromQueryParams(QueryParams params) {
        String localeKey = params.get(QUERY_KEY);

        if (localeKey == null || localeKey.trim().isEmpty()) {
            return Locale.US;
        }

        return LocaleUtils.deserialize(localeKey);
    }

    /**
     * Encodes a string that will be written into JS
     *
     * @param stringToEncode that will be inserted into JS
     * @return a string that is safe to insert into JS
     */
    private static String jsEncode(String stringToEncode) {
        try {
            StringWriter writer = new StringWriter();
            JavascriptEncoder.escape(writer, stringToEncode);
            return writer.toString();
        } catch (IOException e) {
            // this should never ever happen while writing to a StringWriter
            LOGGER.error("Error during javascript encoding", e);
            return "";
        }
    }

    /**
     * Limits the region for a matcher to scan through to immediately before an existing match.
     * <p>
     * There is no method to look-back from a starting point in Java without constructing a string copy which is
     * expensive. This is a work-around by reusing a separate matcher created out of a performance need.
     *
     * @param previousMatch
     * @param matcherToLimit
     * @param matchLimit
     */
    private static void limitMatcherUptoStartOfAPreviousMatch(
            final Matcher previousMatch, final Matcher matcherToLimit, final int matchLimit) {
        matcherToLimit.region(
                Math.max(0, previousMatch.toMatchResult().start() - matchLimit),
                // Limit the end to where `I18n.getText(` starts so the matcher works
                previousMatch.toMatchResult().start());
    }

    private static void removeI18nGetTextCall(Matcher i18nMatcher, StringBuffer outputToReturn) {
        i18nMatcher.appendReplacement(outputToReturn, "");
    }

    private static void replaceNameSpaceAndI18nGetTextCallWithJustTheFormattedTranslationString(
            final Matcher i18nMatcher,
            final Matcher singleIdentifierMatcher,
            final StringBuffer outputToReturn,
            final String translationKey,
            final StringBuilder replacedTextStringBuilder,
            final Locale locale,
            final WebResourceIntegration webResourceIntegration) {
        removeI18nGetTextCall(i18nMatcher, outputToReturn);

        // There's no need to call WRM#format, so format it now by calling getText() instead of getRawText()
        String formattedTranslation = webResourceIntegration.getI18nText(locale, translationKey);
        replacedTextStringBuilder
                .append("\"")
                .append(jsEncode(formattedTranslation))
                .append("\"");

        // Remove the namespace preceding the `I18n.getText(...` call since we're inserting the translated string
        outputToReturn.delete(
                outputToReturn.length() - singleIdentifierMatcher.group(0).length(), outputToReturn.length());
    }

    private static void replaceI18nGetTextCallWithACallToWrmFormat(
            final Matcher i18nMatcher,
            final StringBuffer outputToReturn,
            final String translationKey,
            final StringBuilder replacedTextStringBuilder,
            final Locale locale,
            final WebResourceIntegration webResourceIntegration,
            final boolean moreFormatFnArgs) {
        removeI18nGetTextCall(i18nMatcher, outputToReturn);

        // Need to get raw text, let WRM#format do the formatting
        String rawTranslation = webResourceIntegration.getI18nRawText(locale, translationKey);
        replacedTextStringBuilder
                .append(".format(\"")
                .append(jsEncode(rawTranslation))
                .append("\"")
                .append(moreFormatFnArgs ? "," : ")");
    }

    @Override
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters params) {
        return new JsI18nTransformerUrlBuilder();
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters params) {
        return new JsI18nUrlReadingWebResourceTransformer();
    }

    @Override
    public Set<String> allUsedQueryParameters() {
        return Set.of(QUERY_KEY);
    }

    private final class JsI18nTransformerUrlBuilder implements TransformerUrlBuilder {
        @Override
        public void addToUrl(UrlBuilder urlBuilder) {
            String locale = LocaleUtils.serialize(webResourceIntegration.getLocale());
            addToUrl(urlBuilder, locale);
        }

        private void addToUrl(UrlBuilder urlBuilder, String locale) {
            urlBuilder.addToQueryString(QUERY_KEY, locale);
            // We add webResourceIntegration.getI18nStateHash() to the key;
            // this value varies whenever any plugins that contribute to i18n (eg language
            // packs) are changed, but does not include current locale as it's already in the query string.
            urlBuilder.addToHash(HASH_KEY, webResourceIntegration.getI18nStateHash());
        }
    }
}
