package com.atlassian.webresource.plugin.i18n;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import com.google.common.collect.ImmutableMap;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.transformer.DefaultTransformableResource;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformableResource;
import com.atlassian.webresource.spi.transformer.TwoPhaseResourceTransformer;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.webresource.util.HashBuilder.buildHash;

public class TestJsI18nTransformer {
    private static final Locale MOCK_LOCALE_OBJECT = new Locale("en", "gb");
    private static final String MOCK_LOCALE = LocaleUtils.serialize(MOCK_LOCALE_OBJECT);
    private static final List<Locale> LOCALES = asList(
            MOCK_LOCALE_OBJECT,
            new Locale("en", "us"),
            new Locale("fr", "FR"),
            new Locale("de", "DE"),
            new Locale("ja", "JP"),
            new Locale("es", "ES"));
    private final Map<String, Optional<String>> stubbedKeys = new HashMap<>();
    JsI18nTransformer transformerFactory;
    WebResourceIntegration webResourceIntegration;
    DarkFeatureManager darkFeatureManager;

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void setUp() throws Exception {
        darkFeatureManager = mock(DarkFeatureManager.class);
        when(darkFeatureManager.isEnabledForAllUsers(JsI18nTransformer.TWO_PHASE_JS_I18N_DISABLED))
                .thenReturn(Optional.of(false));
        webResourceIntegration = mock(WebResourceIntegration.class);
        when(webResourceIntegration.getDarkFeatureManager()).thenReturn(darkFeatureManager);
        stubI18nResolution();
        transformerFactory = new JsI18nTransformer(webResourceIntegration);
    }

    @Test
    public void testLocale() {
        String hashValue = "doge_s0-H4sh-WoWW";
        when(webResourceIntegration.getLocale()).thenReturn(MOCK_LOCALE_OBJECT);
        when(webResourceIntegration.getI18nStateHash()).thenReturn(hashValue);
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();

        transformerFactory.makeUrlBuilder(null).addToUrl(urlBuilder);

        assertEquals(MOCK_LOCALE, urlBuilder.buildParams().get("locale"));
        assertEquals(buildHash(hashValue), urlBuilder.buildHash());
    }

    @Test
    public void testCheckedQueryParameters() {
        assertEquals(transformerFactory.allUsedQueryParameters(), Set.of("locale"));
    }

    @Test
    public void testSimple() {
        String javascript = "var label = WRM.I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var label = \"bar\";", transform(javascript));
    }

    @Test
    public void testSimpleLocaleCaseInsensitive() {
        String javascript = "var label = WRM.I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var label = \"bar\";", transform(javascript, ImmutableMap.of("locale", "en-gb")::get));
        assertEquals("var label = \"bar\";", transform(javascript, ImmutableMap.of("locale", "en-GB")::get));
    }

    @Test
    public void testMultipleLines() {
        String key = "foo.bar";
        String key2 = "awesome.label";
        String function = "var someFunction = function() { return 0; };";

        String javascript = "var label = WRM.I18n.getText(\"" + key + "\");\n"
                + "var anotherLabel = WRM.I18n.getText(\"" + key2 + "\");\n" + function;

        stubTranslation(key, "Foo Bar");
        stubTranslation(key2, "Awesome");
        assertEquals("var label = \"Foo Bar\";\nvar anotherLabel = \"Awesome\";\n" + function, transform(javascript));
    }

    @Test
    public void testMissingKey() {
        String javascript = "var t = WRM.I18n.getText(\"blah\");";

        assertEquals("var t = \"blah\";", transform(javascript));
    }

    @Test
    public void testMissingKeyNull() {
        String javascript = "var t = WRM.I18n.getText(\"blah\");";
        stubNullTranslation("key");

        assertEquals("var t = \"blah\";", transform(javascript));
    }

    @Test
    public void testMissingRawKeyNull() {
        String javascript = "var t = WRM.I18n.getText(\"key\", yada);";
        stubNullTranslation("key");

        assertEquals("var t = WRM.format(\"key\", yada);", transform(javascript));
    }

    @Test
    public void testKeyWithSingleQuotes() {
        String javascript = "var t = WRM.I18n.getText('blah');";
        stubTranslation("blah", "Blah");

        assertEquals("var t = \"Blah\";", transform(javascript));
    }

    @Test
    public void testKeyWithoutDots() {
        String javascript = "var t = AJS.I18n.getText(\"blah\");";
        stubTranslation("blah", "Blah");

        assertEquals("var t = \"Blah\";", transform(javascript));
    }

    @Test
    public void testKeyWithHyphens() {
        String key = "foo-bar";
        String javascript = "var str = AJS.I18n.getText(\"" + key + "\");";
        stubTranslation(key, "Foo Bar");
        assertEquals("var str = \"Foo Bar\";", transform(javascript));
    }

    @Test
    public void testValueGetsEscaped() {
        String key = "apos.key";
        String javascript = "var str = AJS.I18n.getText(\"" + key + "\");";
        stubTranslation(key, "That''s Awesome! \"Woot!\"");
        assertEquals("var str = \"That\\u0027s Awesome! \\u0022Woot!\\u0022\";", transform(javascript));
    }

    @Test
    public void testNonMatchingString() {
        String javascript = "var s = 0; var t = AJS.I18n;, var u = AJSI18ngetText(\"foo\")";
        assertEquals(javascript, transform(javascript));

        // mismatched quotes
        javascript = "var str = AJS.I18n.getText('apos.key\");";
        assertEquals(javascript, transform(javascript));
        javascript = "var str = AJS.I18n.getText(\"apos.key');";
        assertEquals(javascript, transform(javascript));
    }

    @Test
    public void testWhitespaceBetweenArgs() {
        stubTranslation("blah", "Blah");

        String javascript = "var t = AJS.I18n.getText( 'blah');";
        assertEquals("var t = \"Blah\";", transform(javascript));

        javascript = "var t = AJS.I18n.getText('blah' );";
        assertEquals("var t = \"Blah\";", transform(javascript));

        javascript = "var t = AJS.I18n.getText( 'blah' );";
        assertEquals("var t = \"Blah\";", transform(javascript));

        javascript = "var t = AJS.I18n.getText('blah',1,2);";
        assertEquals("var t = AJS.format(\"Blah\",1,2);", transform(javascript));

        javascript = "var t = AJS.I18n.getText('blah', 1,2);";
        assertEquals("var t = AJS.format(\"Blah\", 1,2);", transform(javascript));

        javascript = "var t = AJS.I18n.getText('blah' , 1,2);";
        assertEquals("var t = AJS.format(\"Blah\", 1,2);", transform(javascript));
    }

    @Test
    public void testMessageWithQuotes() {
        String key = "key.with.quotes";
        String translation = "Could not find file ''{0}''.";
        String jsEscapedTranslation = "Could not find file \\u0027\\u0027{0}\\u0027\\u0027.";
        String javascript = "var t = WRM.I18n.getText(\"" + key + "\", results, total);";
        stubTranslation(key, translation);

        assertEquals("var t = WRM.format(\"" + jsEscapedTranslation + "\", results, total);", transform(javascript));
    }

    @Test
    public void testFormattedMessageWithArgs() {
        String key = "key.with.format";
        String translation = "Found {0} out of {1}";
        String javascript = "var t = WRM.I18n.getText(\"" + key + "\", results, total);";
        stubTranslation(key, translation);

        assertEquals("var t = WRM.format(\"" + translation + "\", results, total);", transform(javascript));
    }

    @Test
    public void testFormattedMessageNoArgs() {
        String key = "key.with.format";
        String translation = "open-curly-zero '{0}' open-curley-one '{1}'";
        String javascript = "var t = WRM.I18n.getText(\"" + key + "\");";
        stubTranslation(key, translation);

        assertEquals("var t = \"open-curly-zero {0} open-curley-one {1}\";", transform(javascript));
    }

    @Test
    public void testFormattedWithoutLocaleInQueryKey() {
        when(webResourceIntegration.getLocale()).thenReturn(MOCK_LOCALE_OBJECT);
        String javascript = "var t = WRM.I18n.getText('drink');";
        stubTranslation("en-GB", "drink", "tea");
        stubTranslation("en-US", "drink", "cawfee");

        assertEquals("var t = \"tea\";", transform(javascript, ImmutableMap.of("locale", "en-GB")::get));
        assertEquals("var t = \"cawfee\";", transform(javascript, ImmutableMap.of("locale", "en-US")::get));
        assertEquals("var t = \"cawfee\";", transform(javascript, Collections.<String, String>emptyMap()::get));
    }

    @Test
    public void testLegacyAjsNamespaceNoArgs() {
        String javascript = "var t = AJS.I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var t = \"bar\";", transform(javascript));
    }

    @Test
    public void testLegacyAjsNamespaceWithArgs() {
        String key = "key.with.format";
        String translation = "Found {0} out of {1}";
        String javascript = "var t = AJS.I18n.getText(\"" + key + "\", results, total);";
        stubTranslation(key, translation);

        assertEquals("var t = AJS.format(\"" + translation + "\", results, total);", transform(javascript));
    }

    @Test
    public void testRenamedAjsNamespace() {
        String javascript = "var t = a.I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var t = \"bar\";", transform(javascript));
    }

    @Test
    public void testRenamedAjsNamespaceWithFormat() {
        String javascript = "var t = HiMum.I18n.getText(\"foo\", results, total);";

        stubTranslation("foo", "bar");
        assertEquals("var t = HiMum.format(\"bar\", results, total);", transform(javascript));
    }

    @Test
    public void testBabeledAjsNamespace() {
        String javascript = "var t = _AJS[\"default\"].I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var t = \"bar\";", transform(javascript));
    }

    @Test
    public void testBabeledAjsNamespaceWithFormat() {
        String javascript = "var t = _AJS[\"default\"].I18n.getText(\"foo\", results, total);";

        stubTranslation("foo", "bar");
        assertEquals("var t = _AJS[\"default\"].format(\"bar\", results, total);", transform(javascript));
    }

    @Test
    public void testTranspiledOutputFromBabel7WithWebpack4() {
        String javascript = "var t = ANY_MODULE['I18n'].getText('foo');";

        stubTranslation("foo", "bar");
        assertEquals("var t = \"bar\";", transform(javascript));
    }

    @Test
    public void testTranspiledOutputFromBabel7WithWebpack4WithFormat() {
        String javascript = "var t = ANY_MODULE[\"I18n\"].getText(\"foo\", results, total);";

        stubTranslation("foo", "bar");
        assertEquals("var t = ANY_MODULE.format(\"bar\", results, total);", transform(javascript));
    }

    @Test
    public void testNoPrecedingWhitespaceAjsNamespace() {
        String javascript = "var t = \"str\"+AJS.I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var t = \"str\"+\"bar\";", transform(javascript));
    }

    @Test
    public void testWrappedAjsNamespace() {
        String javascript = "var t = [].join(AJS.I18n.getText(\"foo\"));";

        stubTranslation("foo", "bar");
        assertEquals("var t = [].join(\"bar\");", transform(javascript));
    }

    @Test
    public void testUsedInArrayReference() {
        String javascript = "var t = mymap[AJS.I18n.getText(\"foo\")];";

        stubTranslation("foo", "bar");
        assertEquals("var t = mymap[\"bar\"];", transform(javascript));
    }

    @Test
    public void testMissingNamespaceIsIgnored() {
        String javascript = "var label = I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals(javascript, transform(javascript));
    }

    @Test
    public void testLongNamespace() {
        String javascript = "var label = one.two.three.four.I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var label = one.two.three.four.format(\"bar\");", transform(javascript));
    }

    @Test
    public void testImplicitPropertyAccessor() {
        String javascript = "window\n\rWRM.I18n.getText(\"foo\");";
        stubTranslation("foo", "bar");
        assertEquals("window\n\rWRM.format(\"bar\");", transform(javascript));
    }

    @Test
    public void testComplicatedNamespaceIsIgnored() {
        String javascript = "var label = (() => window)().I18n.getText('foo');";

        stubTranslation("foo", "bar");
        assertEquals(javascript, transform(javascript));
    }

    @Test
    @Ignore
    public void testHowLongThisWillTake() throws URISyntaxException, IOException {
        final Path filePath = Paths.get(getClass()
                .getClassLoader()
                .getResource("js/i18n/jira-manyDcApps-adminBatch-dashboardsBatch-combined.js")
                .toURI());
        final String longJsFileContent = String.join("", Files.readAllLines(filePath, UTF_8));

        final Instant start = Instant.now();
        transform(longJsFileContent);
        final Instant stop = Instant.now();

        System.out.println(Duration.between(start, stop));
    }

    // See PLUGWEB-397 for details.
    @Test
    public void testIsNotSucceptableToStackOverflow() throws IOException {
        String js = "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC"
                + "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD94cGFja2V0IGVuZD0idyI/Pv/iDFhJQ0NfUF"
                + "JPRklMRQABAQAADEhMaW5vAhAAAG1u;";
        stubTranslation("a", "b");
        transform(js, ImmutableMap.of("locale", "en-gb")::get);
    }

    @Test
    public void
            given_I18nPropertiesFilePresent_and_FileHasContent_when_jsContentTransformed_then_TranslationsPrependedToContentAsMap() {
        when(webResourceIntegration.getI18nRawText(MOCK_LOCALE_OBJECT, "foo")).thenReturn("wow, a build-time miracle!");
        when(webResourceIntegration.getI18nRawText(MOCK_LOCALE_OBJECT, "bar")).thenReturn("build-time is better ;)");

        final String sourceCode = "WRM.I18n.getText('foo')";
        final String expectedCode = "(k=>{" + "k.set('bar','build-time is better ;)');"
                + "k.set('foo','wow, a build-time miracle!');"
                + "})(WRM.I18n.km);\n"
                + "WRM.I18n.getText('foo')";
        final String result = twoPhaseTransform(sourceCode, "foo=1\nbar=1\n");
        assertEquals(
                "transform should inject translated keys for foo and bar in alphabetical order", expectedCode, result);
    }

    @Test
    public void
            given_I18nPropertiesFilePresent_and_FileIsEmpty_when_jsContentTransformed_then_NoTranslationsPrependedToFile() {
        when(webResourceIntegration.getI18nRawText(MOCK_LOCALE_OBJECT, "foo"))
                .thenReturn("it\'s a build-time miracle!");
        when(webResourceIntegration.getI18nRawText(MOCK_LOCALE_OBJECT, "bar")).thenReturn("build-time is better ;)");

        final String sourceCode = "WRM.I18n.getText('foo')";
        final String expectedCode = "WRM.I18n.getText('foo')";
        final String result = twoPhaseTransform(sourceCode, "");
        assertEquals(
                "transform should leave source alone because build tool found no i18n usages", expectedCode, result);
    }

    @Test
    public void
            given_I18nPropertiesFilePresent_and_FeatureDisabled_when_jsContentTransformed_then_NoTranslationsPrependedToFile() {
        when(webResourceIntegration.getI18nText(MOCK_LOCALE_OBJECT, "bar")).thenReturn("build-time is better ;)");
        when(darkFeatureManager.isEnabledForAllUsers(JsI18nTransformer.TWO_PHASE_JS_I18N_DISABLED))
                .thenReturn(Optional.of(true));

        final String sourceCode = "WRM.I18n.getText('bar')";
        final String expectedCode = "\"build-time is better ;)\"";
        final String result = twoPhaseTransform(sourceCode, "foo=1\nbar=1\n");
        assertEquals("transform should alter source because two-phase feature disabled", expectedCode, result);
    }

    @Test
    public void
            given_I18nPropertiesFileIsAbsent_when_jsContentTransformed_then_TranslationsSubstitutedInlineLikeNormal() {
        when(webResourceIntegration.getI18nText(MOCK_LOCALE_OBJECT, "bar")).thenReturn("build-time is better ;)");

        final String sourceCode = "WRM.I18n.getText('bar')";
        final String expectedCode = "\"build-time is better ;)\"";
        final String result = twoPhaseTransform(sourceCode, null);
        assertEquals("transform should run at runtime and inject translations inline", expectedCode, result);
    }

    @Test
    public void testStandardAndMinifiedFilePatternsResolveToSamePropertiesFile() {
        final TwoPhaseResourceTransformer transformer =
                (TwoPhaseResourceTransformer) transformerFactory.makeResourceTransformer(null);
        final Set<String> filepathsRequested = new HashSet<>();
        final Function<String, InputStream> fn = filepath -> {
            filepathsRequested.add(filepath);
            return null;
        };

        ResourceLocation test1 = mock(ResourceLocation.class);
        when(test1.getLocation()).thenReturn("a/nested/js-file.js");

        ResourceLocation test2 = mock(ResourceLocation.class);
        when(test1.getLocation()).thenReturn("a/nested/js-file-min.js");

        ResourceLocation test3 = mock(ResourceLocation.class);
        when(test1.getLocation()).thenReturn("a/nested/js-file.min.js");

        transformer.loadTwoPhaseProperties(test1, fn);
        transformer.loadTwoPhaseProperties(test2, fn);
        transformer.loadTwoPhaseProperties(test3, fn);

        assertTrue(
                "should have requested the unminified properties path",
                filepathsRequested.contains("a/nested/js-file.js.i18n.properties"));
        assertEquals("should only have one unique filepath request", 1, filepathsRequested.size());
    }

    @Test
    public void
            given_I18nPropertiesFilePresent_and_FileHasContent_andIsDevMode_when_jsContentTransformed_then_TranslationsPrependedToContentAsMap() {
        when(webResourceIntegration.getI18nRawText(MOCK_LOCALE_OBJECT, "foo")).thenReturn("wow, a build-time miracle!");
        when(webResourceIntegration.getI18nRawText(MOCK_LOCALE_OBJECT, "bar")).thenReturn("build-time is better ;)");
        System.setProperty("atlassian.dev.mode", "true");
        final String sourceCode = "WRM.I18n.getText('foo')";
        final String expectedCode = "if (!(window.WRM && window.WRM.I18n )) {\n"
                + "console.warn('The I18n web-resource is missing, please add com.atlassian.plugins.atlassian-plugins-webresource-plugin:i18n as a dependency to the web-resource. Learn more: https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin-javascript');\n"
                + "}\n"
                + "(k=>{"
                + "k.set('bar','build-time is better ;)');"
                + "k.set('foo','wow, a build-time miracle!');"
                + "})(WRM.I18n.km);\n"
                + "WRM.I18n.getText('foo')";
        final String result = twoPhaseTransform(sourceCode, "foo=1\nbar=1\n");
        assertEquals(
                "transform should inject translated keys for foo and bar in alphabetical order", expectedCode, result);
    }

    private void stubTranslation(final String key, final String raw) {
        stubTranslation(MOCK_LOCALE, key, raw);
    }

    private void stubTranslation(final String locale, final String key, final String raw) {
        stubbedKeys.put(locale + "-" + key, Optional.of(raw));
    }

    private void stubNullTranslation(final String key) {
        stubTranslation(key, key);
    }

    private void stubI18nResolution() {
        when(webResourceIntegration.getI18nRawText(any(Locale.class), anyString()))
                .thenAnswer(invocation -> {
                    Locale locale = (Locale) invocation.getArguments()[0];
                    String key = (String) invocation.getArguments()[1];
                    String fullKey = LocaleUtils.serialize(locale) + "-" + key;
                    if (stubbedKeys.containsKey(fullKey)) {
                        Optional<String> value = stubbedKeys.get(fullKey);
                        return value.orElse(null);
                    } else {
                        return key;
                    }
                });
        when(webResourceIntegration.getI18nText(any(Locale.class), anyString())).thenAnswer(invocation -> {
            Locale locale = (Locale) invocation.getArguments()[0];
            String key = (String) invocation.getArguments()[1];
            String fullKey = LocaleUtils.serialize(locale) + "-" + key;
            if (stubbedKeys.containsKey(fullKey)) {
                Optional<String> value = stubbedKeys.get(fullKey);
                return value.isPresent() ? MessageFormat.format(value.get(), new Object[0]) : null;
            } else {
                return key;
            }
        });

        when(webResourceIntegration.getSupportedLocales()).thenReturn(LOCALES);
    }

    private String transform(String javascript) {
        return twoPhaseTransform(javascript, null);
    }

    private String transform(String javascript, QueryParams queryParams) {
        return twoPhaseTransform(javascript, queryParams, null);
    }

    private String twoPhaseTransform(String javascript, String usedI18nKeys) {
        return twoPhaseTransform(javascript, ImmutableMap.of("locale", MOCK_LOCALE)::get, usedI18nKeys);
    }

    private String twoPhaseTransform(String javascript, QueryParams queryParams, String usedI18nKeys) {
        try {
            TransformableResource origResource =
                    new DefaultTransformableResource(null, new StringDownloadableResource(javascript));
            UrlReadingWebResourceTransformer transformer = transformerFactory.makeResourceTransformer(null);
            if (transformer instanceof TwoPhaseResourceTransformer) {
                // Mock the location of the file we're transforming
                ResourceLocation location = mock(ResourceLocation.class);
                when(location.getLocation()).thenReturn("fake-file.js");
                // Mock the content of a file generated by the build tool.
                // Would typically be in a `fake-file.js.i18n.properties` file.
                final InputStream is =
                        usedI18nKeys != null ? new ByteArrayInputStream(usedI18nKeys.getBytes(UTF_8)) : null;
                ((TwoPhaseResourceTransformer) transformer).loadTwoPhaseProperties(location, filepath -> is);
            }
            DownloadableResource resource = transformer.transform(origResource, queryParams);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            resource.streamResource(out);
            return out.toString("UTF-8");
        } catch (DownloadException | UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }

    private class StringDownloadableResource implements DownloadableResource {
        private final String content;

        StringDownloadableResource(final String content) {
            this.content = content;
        }

        public void streamResource(final OutputStream out) throws DownloadException {
            try {
                out.write(content.getBytes());
            } catch (final IOException e) {
                throw new DownloadException(e);
            }
        }

        public void serveResource(final HttpServletRequest request, final HttpServletResponse response)
                throws DownloadException {}

        public boolean isResourceModified(final HttpServletRequest request, final HttpServletResponse response) {
            return false;
        }

        public String getContentType() {
            return null;
        }
    }
}
