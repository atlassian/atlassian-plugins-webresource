
require('../../../../main/resources/js/data/format');
require('../../../../main/resources/js/data/i18n');

beforeEach(() => {
    // local instantiation of WRM and WRM.data
    AJS.I18n.km.set("a", "1");
    AJS.I18n.km.set("b", "");
});


test("Translates when data is provided", () => {
    expect(AJS.I18n.getText("a")).toBe("1");
})

test("Translates when data is an empty string", () => {
    expect(AJS.I18n.getText("b")).toBe("");
})

test("Returns passed key when data is not provided", () => {
    expect(AJS.I18n.getText("c")).toBe("c");
})

