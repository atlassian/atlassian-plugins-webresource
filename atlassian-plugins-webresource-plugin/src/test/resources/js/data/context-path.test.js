const originalClaim = WRM.data.claim;
const mockClaim = jest.fn();

beforeEach(() => {
    mockClaim.mockReset();
    WRM.data.claim = mockClaim;
    // We load it now so that it gets re-evaluated for each test and its internal state is clean.
    require('../../../../main/resources/js/data/context-path');
});

afterEach(() => {
    WRM.data.claim = originalClaim;
    // Reset modules after each test to clean their state
    jest.resetModules();
});

test("Delivers context path", () => {
    mockClaim.mockReturnValue("/j-doge");

    expect(WRM.contextPath()).toEqual("/j-doge");
});

test("Delivers empty context path successfully", () => {
    mockClaim.mockReturnValue("");

    expect(WRM.contextPath()).toEqual("");
});
