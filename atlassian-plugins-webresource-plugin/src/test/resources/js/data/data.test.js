jest.useFakeTimers();

const mockConsole = require('jest-mock-console');
require('../../../../main/resources/js/data/data');

const BAD_RETURN = undefined;
let wrmData;
let wrmStore;

beforeEach(() => {
    // local instantiation of WRM and WRM.data
    wrmStore = {};
    wrmData = WRM._createDataImpl(wrmStore);
});

function addData(key, jsonValue) {
    wrmStore._unparsedData[key] = jsonValue;
    wrmStore._dataArrived();
    jest.runAllTimers();
}

function addError(key) {
    wrmStore._unparsedErrors[key] = "";
    wrmStore._dataArrived();
    jest.runAllTimers();
}

test("Does not fail when data does not exist", () => {
    expect(wrmData.claim("On a steel horse I ride")).toBe(BAD_RETURN);
});

test("Returns basic data", () => {
    addData("bon", JSON.stringify("jovi"));

    expect(wrmData.claim("bon")).toEqual("jovi");
});

test("Multiple data claims on same key throw an error", () => {
    mockConsole();
    addData("bon", JSON.stringify("doge-vi"));
    expect(wrmData.claim("bon")).toEqual("doge-vi");
    expect(wrmData.claim("bon")).toEqual(BAD_RETURN);
    expect(console.error).toHaveBeenCalledWith("Data with key 'bon' has already been claimed");
});

test("Returns JSON data", () => {
    addData("bon-jovi", JSON.stringify({ shot: "through the heart" }));

    expect(wrmData.claim("bon-jovi")).toEqual({
        shot: "through the heart"
    });
});

test("Returns data with pathologically bad keys", () => {
    addData("hasOwnProperty", JSON.stringify("mwahhahahah"));

    expect(wrmData.claim("hasOwnProperty")).toEqual("mwahhahahah");
});

test("Handles malformed data", () => {
    mockConsole();
    addData("malformed", "{");
    expect(wrmData.claim("malformed")).toBe(BAD_RETURN);
    // Test that an error message is logged to the console
    expect(console.error).toHaveBeenCalledTimes(1);

    // Test that error message is logged every time
    wrmData.claim("malformed");
    expect(console.error).toHaveBeenCalledTimes(2);
});

test("Data arrives after async handler", () => {
    const cb = jest.fn();
    wrmData.claim("bon",cb);
    expect(cb).not.toHaveBeenCalled();
    addData("bon", JSON.stringify("jovi"));
    expect(cb).toHaveBeenCalledWith("jovi");
});

test("Error arrives after async handler", () => {
    const failCb = jest.fn();
    wrmData.claim("bon", null, failCb);
    expect(failCb).not.toHaveBeenCalled();
    addError("bon");
    expect(failCb).toHaveBeenCalled();
});

test("Data arrives before async handler", () => {
    const cb = jest.fn();
    addData("bon", JSON.stringify("jovi"));
    wrmData.claim("bon", cb);
    jest.runAllTimers();
    expect(cb).toHaveBeenCalledWith("jovi");
});

test("Error arrives before async handler", () => {
    const failCb = jest.fn();
    addError("bon");
    wrmData.claim("bon", null, failCb);
    jest.runAllTimers();
    expect(failCb).toHaveBeenCalled();
});

test("invalid json is recieved as an failure async callback", () => {
    let val = "nothing";
    wrmData.claim("bon",
        () => { val = "gotdata"; },
        () => { val = "goterror"; }
    );
    addData("bon", "{");
    expect(val).toEqual("goterror");
});

test("all callbacks get called even if they throw exceptions", () => {
    mockConsole();
    let claimA = jest.fn().mockImplementation(() => { throw "a" });
    let claimB = jest.fn().mockImplementation(() => { throw "b" });
    wrmData.claim("a", claimA);
    wrmData.claim("b", claimB);

    addData("a", JSON.stringify("jovi"));
    expect(claimA).toHaveBeenCalled();
    expect(claimB).not.toHaveBeenCalled();
    addData("b", JSON.stringify("jovi"));
    expect(claimB).toHaveBeenCalled();
});
