const mockConsole = require('jest-mock-console');
require('../../../../main/resources/js/data/format');

const { format } = WRM;

test('with 1 parameter', () => {
    const testFormat = format('hello {0}', 'world');
    expect(testFormat).toEqual('hello world');
});

test('with 2 parameters', () => {
    const testFormat = format('hello {0} {1}', 'world', 'again');
    expect(testFormat).toEqual('hello world again');
});

test('with 3 parameters', () => {
    const testFormat = format('hello {0} {1} {2}', 'world', 'again', '!');
    expect(testFormat).toEqual('hello world again !');
});

test('with 4 parameters', () => {
    const testFormat = format('hello {0} {1} {2} {3}', 'world', 'again', '!', 'test');
    expect(testFormat).toEqual('hello world again ! test');
});

test('with symbols', () => {
    const testFormat = format('hello {0}', '!@#$%^&*()');
    expect(testFormat).toEqual('hello !@#$%^&*()');
});

test('with curly braces', () => {
    const testFormat = format('hello {0}', '{}');
    expect(testFormat).toEqual('hello {}');
});

test('with repeated parameters', () => {
    const testFormat = format('hello {0}, {0}, {0}', 'world');
    expect(testFormat).toEqual('hello world, world, world');
});

test('with apostrophe', () => {
    const testFormat = format('hello \'{0}\' {0} {0}', 'world');
    expect(testFormat).toEqual('hello {0} world world');
});

test('with very long parameters', () => {
    const testFormat = format('hello {0}', new Array(25).join('this parameter is very long ')); // we're joining 25 empty values together, which means we'll get our join string 24 times.
    expect(testFormat).toEqual('hello this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long '); // eslint-disable-line
});

// positional equivalence
test('with prefix', () => {
    const testFormat = format('{0} two three', 'one');
    expect(testFormat).toEqual('one two three');
});

test('with infix', () => {
    const testFormat = format('one {0} three', 'two');
    expect(testFormat).toEqual('one two three');
});

test('with postfix', () => {
    const testFormat = format('one two {0}', 'three');
    expect(testFormat).toEqual('one two three');
});

test('with differing order', () => {
    const testFormat = format('{2} {0} {1}', 'two', 'three', 'one');
    expect(testFormat).toEqual('one two three');
});

// choices
test('with a choice value missing parameter', () => {
    const testFormat = format('We got {0,choice,0#|1#1 issue|1<{1,number} issues}');
    expect(testFormat).toEqual('We got ');
});

test('with a choice value with parameter lower first option', () => {
    const testFormat = format('We got {0,choice,0#0 issues|1#1|1<{1,number} issues}', -1, -1);
    expect(testFormat).toEqual('We got 0 issues');
});

test('with a choice value first option', () => {
    const testFormat = format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 0);
    expect(testFormat).toEqual('We got 0 issues');
});

test('with a choice value middle option', () => {
    const testFormat = format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 1);
    expect(testFormat).toEqual('We got 1 issue');
});

test('with a choice value last option', () => {
    const testFormat = format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 2);
    expect(testFormat).toEqual('We got 2 issues');
});

test('with a choice value with missing number parameter option', () => {
    const testFormat = format('We got {0,choice,0# |1#1 issue|1<{1,number} issues}', 2);
    expect(testFormat).toEqual('We got  issues');
});

test('with a choice value with valid second option', () => {
    const testFormat = format('We got {0,choice,0# |1#1 issue|1<{1,number} issues}', 10, 10);
    expect(testFormat).toEqual('We got 10 issues');
});

test('with a choice value with whitespace after pipe', () => {
    const testFormat = format('We got {0,choice,0#0 issues| 1#1| 1<{1,number} issues}', -1, -1);
    expect(testFormat).toEqual('We got 0 issues');
});

test('with a choice value with whitespace before pipe', () => {
    const testFormat = format('We got {0,choice,0#0 issues |1#1 |1<{1,number} issues}', -1, -1);
    expect(testFormat).toEqual('We got 0 issues ');
});

test('with a choice value with whitespace after comma', () => {
    const testFormat = format('We got {0, choice, 0#0 issues|1#1|1<{1, number} issues}', -1, -1);
    expect(testFormat).toEqual('We got 0 issues');
});

test('with a choice value with whitespace after argument number', () => {
    const testFormat = format('We got {0 ,choice,0#0 issues|1#1|1<{1,number} issues}', -1, -1);
    expect(testFormat).toEqual('We got ');
});

test('with a choice value with whitespace after choice keyword', () => {
    const testFormat = format('We got {0,choice ,0#0 issues|1#1|1<{1,number} issues}', -1, -1);
    expect(testFormat).toEqual('We got 0 issues');
});

test('with a choice value with whitespace after hash', () => {
    const testFormat = format('We got {0,choice ,0# 0 issues|1# 1|1<{1 ,number} issues}', -1, -1);
    expect(testFormat).toEqual('We got  0 issues');
});

test('with a choice value with whitespace before hash', () => {
    const testFormat = format('We got {0,choice ,0 #0 issues|1 #1|1<{1 ,number} issues}', -1, -1);
    expect(testFormat).toEqual('We got 0 issues');
});

test('with a choice value with multiple whitespaces around separators', () => {
    const testFormat = format('We got {0, choice ,  0 # 0 issues| 1#   1   | 1<{1,number} issues}', -1, -1);
    expect(testFormat).toEqual('We got  0 issues');
});

// number
test('with a number value', () => {
    const testFormat = format('Give me {0,number}!', 5);
    expect(testFormat).toEqual('Give me 5!');
});

test('with a floating point value', () => {
    const testFormat = format('Give me {0,number}!', Math.PI);
    expect(testFormat).toEqual('Give me 3.141592653589793!');
});

test('with a number value with whitespace after comma', () => {
    const testFormat = format('Give me {0, number}!', 5);
    expect(testFormat).toEqual('Give me 5!');
});

test('with a number value with whitespace after number keyword', () => {
    const testFormat = format('Give me {0,number }!', 5);
    expect(testFormat).toEqual('Give me 5!');
});

test('with a number value with multiple whitespaces', () => {
    const testFormat = format('Give me {0,  number}!', 5);
    expect(testFormat).toEqual('Give me 5!');
});

test('with a number value and missing parameter', () => {
    const testFormat = format('Give me {0,number}!');
    expect(testFormat).toEqual('Give me !');
});

/** @note Not implemented according to jsdoc */
// test('with a number value and non-numeric parameter', function () {
//     var testFormat = format('Give me {0,number}!', 'everything');
//     expect(testFormat).toEqual('Give me !');
// });

test('with a bit of everything', () => {
    const issueChoice = '{0,choice,0#0 zgłoszeń|1#1 zgłoszenia|4<{0,number} zgłoszenie|5<{0,number} zgłoszeń}';
    const timeChoice = '{1, choice, 0#brak czasu|1#1 minuty|4<{1, number} minut|5<{1, number} minut}';
    const goodLuck = 'powodzenia';
    // reads: try to solve x issues within y minutes or less. good luck!
    const testFormat = format(`spróbuj rozwiązać ${issueChoice} w ciągu ${timeChoice} lub szybciej. {2}!`, 4, 12, goodLuck);
    expect(testFormat).toEqual('spróbuj rozwiązać 1 zgłoszenia w ciągu 12 minut lub szybciej. powodzenia!');
});

describe('wrm/format - with invalid choicePart formats', () => {
    const invalidFormatToErrorMap = {
        'approval approvals: {0} {0,choice,1#|1<}': 'The format "0,choice,1#|1<" from message "approval approvals: {0} {0,choice,1#|1<}" is invalid.',
        'approval approvals: {0} {0,choice,1|1<}': 'The format "0,choice,1|1<" from message "approval approvals: {0} {0,choice,1|1<}" is invalid.',
        'approval approvals: {0} {0,choice,1}': 'The format "0,choice,1" from message "approval approvals: {0} {0,choice,1}" is invalid.',
    };

    Object.keys(invalidFormatToErrorMap).forEach(invalidFormat => {
        test(`for invalid format "${invalidFormat}"`, () => {
            const restore = mockConsole();
            const error = invalidFormatToErrorMap[invalidFormat];

            expect(() => {
                format(invalidFormat); // valid since no substitutions made.
                format(invalidFormat, 1);
                format(invalidFormat, 1, 5, 7, 9, 11);
            }).not.toThrow();

            expect(console.error).toHaveBeenCalledTimes(2);
            expect(console.error).toHaveBeenCalledWith(error);

            restore();
        });
    });
});
