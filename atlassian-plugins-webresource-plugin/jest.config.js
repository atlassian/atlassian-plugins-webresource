const base = require("../jest.config.base");
const pack = require("./package.json");

module.exports = {
    ...base,
    displayName: pack.name,
    testEnvironment: "jsdom",
    reporters: ["default", "jest-junit"],
    setupFiles: [
        "<rootDir>/setup-jest.js",
    ],
    setupFilesAfterEnv: ["jest-extended/all"],
    testMatch: ["<rootDir>/src/test/**/*.test.(js|ts)"],
    moduleFileExtensions: ["ts", "js"],
    modulePaths: ["<rootDir>"],
};
