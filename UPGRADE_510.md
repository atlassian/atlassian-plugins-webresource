# Host application upgrade notes

Version 5.1.0 includes Analytics to track effectiveness of resource caching.

## Analytics

It is enabled out of the box, but there is an option to disable it by system property.

```xml
<!-- pom.xml-->
<systemPropertyVariables>
    <!-- ... -->
    <atlassian.darkfeature.atlassian.webresource.performance.tracking.disable>
        true
    </atlassian.darkfeature.atlassian.webresource.performance.tracking.disable>
   <!-- ... -->
</systemPropertyVariables>
```
