package com.atlassian.webresource.api.assembler.resource;

/**
 * A CSS webresource
 *
 * @since v3.0
 */
public interface PluginCssResource extends PluginUrlResource<PluginCssResourceParams> {}
