package com.atlassian.webresource.api.assembler;

/**
 * Builder for constructing WebResourceAssembler
 *
 * @since v3.0
 */
public interface WebResourceAssemblerBuilder {
    /**
     * In addition to any resources explicitly added to the WebResourceAssembler, always implicitly include resources
     * from the superbatch. When disabled, superbatch resources are not implicitly included, but may be individually
     * (and explicitly) included by requiring them with the WebResourceAssembler.
     *
     * @deprecated since 5.5.0. Use {@link RequiredResources#excludeSuperbatch()} instead.
     */
    @Deprecated
    WebResourceAssemblerBuilder includeSuperbatchResources(boolean include);

    /**
     * The sync batch is implicitly included in every request. When disabled, resources in the sync batch will not be
     * served unless they are explicitly required with the WebResourceAssembler.
     *
     * @param include whether to serve the sync batch contents or not.
     * @since 5.5.0
     */
    WebResourceAssemblerBuilder includeSyncbatchResources(boolean include);

    /**
     * Occasionally a WRM feature requires supporting frontend code to work, such as when the interactive or lazy-load
     * resource phases are used. When enabled (the default value), when such features are used, relevant frontend code
     * will be added to the list of required resources and contexts. When disabled, these resources will not be served
     * unless they are explicitly required with the WebResourceAssembler.
     *
     * @param include whether to automatically include frontend runtime modules or not.
     * @since 5.5.0
     */
    WebResourceAssemblerBuilder autoIncludeFrontendRuntime(boolean include);

    /**
     * Constructs a WebResourceAssembler
     *
     * @return a WebResourceAssembler
     */
    WebResourceAssembler build();
}
