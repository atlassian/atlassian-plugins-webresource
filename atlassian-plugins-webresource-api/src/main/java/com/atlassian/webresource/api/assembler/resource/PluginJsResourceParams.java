package com.atlassian.webresource.api.assembler.resource;

/**
 * Parameters for a Javascript webresource
 *
 * @since v3.0
 */
public interface PluginJsResourceParams extends PluginUrlResourceParams {}
