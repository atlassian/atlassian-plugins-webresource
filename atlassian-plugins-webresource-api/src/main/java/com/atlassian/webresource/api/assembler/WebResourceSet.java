package com.atlassian.webresource.api.assembler;

import java.io.Writer;
import java.util.function.Predicate;

import com.atlassian.webresource.api.UrlMode;

/**
 * Set of assembled web resources and web resources contexts.
 *
 * @since v3.0
 */
public interface WebResourceSet {
    /**
     * Write the HTML tags for this object's resources to the writer. Writing will be done in order of
     * webResourceFormatters so that all CSS resources will be output before Javascript.
     *
     * @param writer  writer to output to
     * @param urlMode url formatting mode
     */
    void writeHtmlTags(Writer writer, UrlMode urlMode);

    /**
     * Write the HTML tags for this object's resources to the writer. Writing will be done in order of
     * webResourceFormatters so that all CSS resources will be output before Javascript.
     *
     * @param writer    writer to output to
     * @param urlMode   url formatting mode
     * @param predicate only resources matching this predicate will be written
     */
    void writeHtmlTags(Writer writer, UrlMode urlMode, Predicate<WebResource> predicate);

    /**
     * Write the HTML tags for non-data resources as prefetch links: {@code <link rel="prefetch">}.
     * <b>Data resources will not be output.</b>
     * Writing will be done in the same order as the resources appear in this set, not ordered by type.
     * <p>
     * Typical usage:
     * <pre><code>
     * WebResourceAssembler pageAssembler = wr.getPageBuilderService().assembler();
     * WebResourceAssembler prefetchAssembler = pageAssembler.copy();
     *
     * pageAssembler.resources().requireWebResource("myplugin:resource-for-this-page");
     * WebResourceSet pageSet = pageAssembler.assembled().drainIncludedResources();
     * pageSet.writeHtmlTags(writer, urlMode);
     *
     * prefetchAssembler.resources().requireWebResource("myplugin:resource-for-next-page");
     * WebResourceSet prefetchSet = prefetchAssembler.assembled().drainIncludedResources();
     * prefetchSet.writePrefetchLinks(writer, urlMode);
     * </code></pre>
     *
     * @param writer  writer to output to
     * @param urlMode url formatting mode
     * @since 3.6.0
     */
    void writePrefetchLinks(Writer writer, UrlMode urlMode);

    /**
     * Returns a list of included WebResources
     *
     * @return included resources
     */
    Iterable<WebResource> getResources();

    /**
     * Returns a filtered list of included WebResources matching the given class
     *
     * @param clazz class of webresource to return
     * @return included resources
     */
    <T extends WebResource> Iterable<T> getResources(Class<T> clazz);
}
