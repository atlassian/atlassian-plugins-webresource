package com.atlassian.webresource.api.assembler.resource;

/**
 * A Javascript webresource
 *
 * @since v3.0
 */
public interface PluginJsResource extends PluginUrlResource<PluginJsResourceParams> {}
