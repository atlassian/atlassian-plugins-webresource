package com.atlassian.webresource.api.assembler.resource;

import java.util.Map;

/**
 * Parameters that affect a resource that occur outside its querystring - e.g.: media query, and other attributes on an
 * HTML tag e.g.: charset
 *
 * @since v3.0
 */
public interface PluginUrlResourceParams {

    /**
     * @return All params defined for this resource
     */
    Map<String, String> all();
}
