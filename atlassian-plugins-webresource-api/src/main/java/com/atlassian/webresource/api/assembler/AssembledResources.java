package com.atlassian.webresource.api.assembler;

/**
 * Interface for clients to include web resources that have been required via {@link RequiredResources}.
 *
 * @see WebResourceAssembler
 * @see PageBuilderService
 * @since v3.0
 */
public interface AssembledResources {
    /**
     * Calculates dependencies, returns requested resources, then clears the list of webresources + contexts that have
     * been included since the last call to {@link #drainIncludedResources()}.
     * <p>
     * This method ensures that no individual webresource will be emitted twice, either as part of a superbatch, context
     * batch or individual include.
     * <p>
     * Resources are ordered in the following way:
     * <ol>
     * <li>
     *      From earliest {@link com.atlassian.webresource.api.assembler.resource.ResourcePhase} to latest.
     * </li>
     * <li>
     *     From highest priority resource to lowest.
     *     In case of equal priorities, the earliest call to {@link RequiredResources} wins.
     * </li>
     * <li>
     *     (If superbatch is enabled) renders the superbatch context.
     * </li>
     * <li>
     *     Renders all required contexts.
     * </li>
     * <li>
     *     Renders all required web-resources.
     * </li>
     * </ol>
     *
     * @return list of plugin resources to emit.
     */
    WebResourceSet drainIncludedResources();

    /**
     * Returns the currently requested resources as per {@link #drainIncludedResources()}, however does not clear the
     * internal list. Intended to be used to debugging or introspecting the current state. This does not block on any
     * promises, and importantly does not return any undrained complete promises.
     */
    WebResourceSet peek();

    /**
     * Calculates dependencies, returns requested resources, then clears the list of webresources + context for the
     * resources that should be written at the top of the page.
     *
     * @return List of plugin resources to emit.
     * @since 5.0.0
     * @deprecated since 5.6.0. Unused now {@link com.atlassian.webresource.api.assembler.resource.ResourcePhase#INLINE}
     * exists.
     */
    @Deprecated
    default WebResourceSet drainIncludedSyncResources() {
        return drainIncludedResources();
    }
}
