package com.atlassian.webresource.api.assembler;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

/**
 * Interface for clients to require data blobs.
 *
 * @since v3.0
 */
public interface RequiredData {
    /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources() with
     * {@link ResourcePhase#defaultPhase}.
     *
     * @param key     data key
     * @param content JSON data
     * @return this, to support method chaining {@code WRM.data()} JavaScript API
     */
    RequiredData requireData(String key, Jsonable content);

    RequiredData requireData(String key, Jsonable content, ResourcePhase resourcePhase);

    /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources() with
     * {@link ResourcePhase#defaultPhase}.
     *
     * @param key     data key
     * @param content content to render
     * @return this, to support method chaining {@code WRM.data()} JavaScript API
     */
    RequiredData requireData(String key, Number content);

    RequiredData requireData(String key, Number content, ResourcePhase resourcePhase);

    /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources() with
     * {@link ResourcePhase#defaultPhase}.
     *
     * @param key     data key
     * @param content content to render
     * @return this, to support method chaining {@code WRM.data()} JavaScript API
     */
    RequiredData requireData(String key, String content);

    RequiredData requireData(String key, String content, ResourcePhase resourcePhase);

    /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources() with
     * {@link ResourcePhase#defaultPhase}.
     *
     * @param key     data key
     * @param content content to render
     * @return this, to support method chaining {@code WRM.data()} JavaScript API
     */
    RequiredData requireData(String key, Boolean content);

    RequiredData requireData(String key, Boolean content, ResourcePhase resourcePhase);
}
