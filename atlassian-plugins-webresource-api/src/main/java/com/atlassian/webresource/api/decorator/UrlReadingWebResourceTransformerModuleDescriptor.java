package com.atlassian.webresource.api.decorator;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

public interface UrlReadingWebResourceTransformerModuleDescriptor
        extends ModuleDescriptor<WebResourceTransformerFactory> {}
