package com.atlassian.webresource.api.assembler;

import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.defaultPhase;

/**
 * Marker interface for resources returned from the web resource system. This includes javascript, css and data
 * resources.
 *
 * @since v3.0
 */
public interface WebResource {

    /**
     * Retrieves the web resource phase.
     *
     * @return The web resource phase.
     */
    default ResourcePhase getResourcePhase() {
        return defaultPhase();
    }
}
