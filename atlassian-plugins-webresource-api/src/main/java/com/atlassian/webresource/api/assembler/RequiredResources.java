package com.atlassian.webresource.api.assembler;

import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

/**
 * Interface for clients to require web resources.
 *
 * @see WebResourceAssembler
 * @see PageBuilderService
 * @since v3.0
 */
public interface RequiredResources {

    /**
     * Specifies that resources for the given module should be included on the next call to drainIncludedResources()
     * with {@link ResourcePhase#defaultPhase}.
     *
     * @param moduleCompleteKey key of webresource module
     * @return this, to support method chaining
     */
    @Nonnull
    RequiredResources requireWebResource(@Nonnull String moduleCompleteKey);

    /**
     * Specifies that resources for the given module should be included on the next call to drainIncludedResources(),
     * taking into consideration the resource phase.
     *
     * @param resourcePhase     The module resourcePhase
     * @param moduleCompleteKey key of webresource module
     * @return this, to support method chaining
     * @since 5.0.0
     */
    @Nonnull
    RequiredResources requireWebResource(@Nonnull ResourcePhase resourcePhase, @Nonnull String moduleCompleteKey);

    /**
     * Specifies that resources for the given webresource context should be included on the next call to
     * drainIncludedResources() with {@link ResourcePhase#defaultPhase}.
     *
     * @param context webresource context to include
     * @return this, to support method chaining
     */
    @Nonnull
    RequiredResources requireContext(@Nonnull String context);

    /**
     * Specifies that resources for the given webresource context should be included on the next call to
     * drainIncludedResources(), taking into consideration the resource phase.
     *
     * @param resourcePhase The context resourcePhase
     * @param context       webresource context to include
     * @return this, to support method chaining
     * @since 5.0.0
     */
    @Nonnull
    RequiredResources requireContext(@Nonnull ResourcePhase resourcePhase, @Nonnull String context);

    /**
     * Specifies that the given resources should be excluded from all future calls to drainIncludedResources(). This is
     * equivalent to requiring the given webresources and contexts then calling drain().
     * <p>
     * Any currently un-drained resources/contexts will remain and be included in the next drain.
     * <p>
     * If this WebResourceAssembler includes implicit resources (e.g. "superbatch"), this call will also exclude all
     * such implicit resources from future calls to drainIncludedResources().
     *
     * @param webResources webresource keys to exclude.
     * @param contexts     contexts to exclude.
     * @return this, to support method chaining
     */
    @Nonnull
    RequiredResources exclude(@Nullable Set<String> webResources, @Nullable Set<String> contexts);

    /**
     * <p>
     * Includes all contexts and resources specified in the root-page into the {@link WebResourceAssembler} with
     * {@link ResourcePhase#defaultPhase}.
     * </p>
     *
     * @param key key of the root page
     * @return this, to support method chaining
     * @since 3.5.22
     */
    @Nonnull
    RequiredResources requirePage(@Nonnull String key);

    /**
     * <p>
     * Includes all contexts and resources specified in the root-page into the {@link WebResourceAssembler} taking into
     * consideration the resource phase.
     * </p>
     *
     * @param resourcePhase The page resourcePhase
     * @param key           key of the root page
     * @return this, to support method chaining
     * @since 5.0.0
     */
    @Nonnull
    RequiredResources requirePage(@Nonnull ResourcePhase resourcePhase, @Nonnull String key);

    /**
     * Exclude superbatch context.
     *
     * @return this, to support method chaining
     * @since 5.4.0
     */
    @Nonnull
    RequiredResources excludeSuperbatch();

    /**
     * <p>
     * Sets resource phase for superbatch context. Keep in mind that if superbatch is disabled globally this function
     * does not have an effect.
     * </p>
     *
     * @param resourcePhase The superbatch resourcePhase
     * @return this, to support method chaining
     * @since 5.4.0
     */
    @Nonnull
    RequiredResources requireSuperbatch(@Nonnull final ResourcePhase resourcePhase);
}
