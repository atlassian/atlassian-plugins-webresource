package com.atlassian.webresource.api.descriptor;

import java.util.List;
import java.util.Set;

import com.atlassian.plugin.ModuleDescriptor;

/**
 * Missing the following; getting the composite condition, getting the transformations, getting the data providers, and
 * getting the deprecation details. If any of those sound like something you need, let us know, you can comment on
 * Developer Community or on <a href="https://ecosystem.atlassian.net/browse/PLUGWEB-765">this ticket</a>.
 *
 * @since 7.0.0
 */
public interface WebResourceModuleDescriptor extends ModuleDescriptor<Void> {
    /**
     * @return the web-resource contexts this resource is associated with.
     */
    Set<String> getContexts();

    /**
     * Returns the set of contexts that this web-resource depends on, this is only relevant is this web-resource is
     * defining a root page ({@link #isRootPage()})
     *
     * @return the set of contexts that this resource depends on
     */
    Set<String> getContextDependencies();

    /**
     * Returns a list of dependencies on other web-resources.
     *
     * @return a list of module complete keys
     */
    List<String> getDependencies();

    /**
     * Declares if this web-resource is defining the root page, e.g. 'issue view page', 'dashboard page'
     *
     * @return an Option of the RootPage object
     */
    boolean isRootPage();

    /**
     * @return <code>true</code> if the web-resource is deprecated, <code>false</code> otherwise.
     */
    boolean isDeprecated();

    /**
     * @return <code>true</code> if resource minification should be skipped, <code>false</code> otherwise.
     */
    boolean isDisableMinification();
}
