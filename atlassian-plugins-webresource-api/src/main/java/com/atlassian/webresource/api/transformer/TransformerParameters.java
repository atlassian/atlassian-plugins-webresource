package com.atlassian.webresource.api.transformer;

import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

/**
 * Transformer parameters to be passed to {@link WebResourceTransformerFactory}'s methods.
 *
 * @since 6.3.0
 */
public interface TransformerParameters {

    /**
     * @return the key of the plugin containing resources to be transformed
     */
    String getPluginKey();

    /**
     * @return the key of the web-resource module containing resources to be transformed
     */
    String getModuleKey();
}
