package com.atlassian.webresource.api;

/**
 * Provides access to querystring params.
 *
 * @since 6.3.0
 */
public interface QueryParams {

    String get(String key);
}
