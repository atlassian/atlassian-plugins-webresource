package com.atlassian.webresource.api.assembler;

/**
 * Factory for creating web resource builders.
 *
 * @since v3.0
 */
public interface WebResourceAssemblerFactory {
    /**
     * Creates a new WebResourceAssemblerParams for constructing a builder.
     *
     * @return builder instance
     */
    WebResourceAssemblerBuilder create();
}
