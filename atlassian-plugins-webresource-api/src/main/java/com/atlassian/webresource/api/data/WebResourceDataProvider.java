package com.atlassian.webresource.api.data;

import java.util.function.Supplier;

import com.atlassian.json.marshal.Jsonable;

/**
 * Interface for providing JSON data to a client.
 * <p>
 * Instances of this class are constructed at plugin enablement time.
 *
 * @since v3.0
 */
public interface WebResourceDataProvider extends Supplier<Jsonable> {}
