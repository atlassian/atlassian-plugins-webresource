package com.atlassian.webresource.api.assembler.resource;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Nullable;

/**
 * Defines the current resource phases.
 *
 * <b>THE ORDER OF THE ENUMERABLES MATTERS. BE CAREFUL WHEN ADDING NEW TYPES.</b>
 *
 * <b>Ordered from earliest to the latest phase</b>
 */
public enum ResourcePhase {
    /**
     * Code is rendered directly to the HTTP response. When rendering to HTML, code will render inside {@code <style>}
     * and {@code <script>} tags. In other rendering contexts, the text content will be returned.
     */
    INLINE("inline"),

    /**
     * Code should be loaded and run immediately as the client encounters it, blocking further HTML parsing.
     * <p>
     * When rendering to HTML, code will be referenced by URL, accessed via {@code <script src="">} or
     * {@code <link href="">} as appropriate.
     */
    REQUIRE("require"),

    /**
     * Code should start loading when the client encounters it, but should not execute until code in the {@link #INLINE}
     * and {@link #REQUIRE} phases has executed and the initial HTTP response has completed. In a browser context, this
     * means when the closing {@code </html>} tag is encountered.
     * <p>
     * When rendering to HTML, code will be referenced by URL, accessed via {@code <script defer src="">} or
     * {@code <link href="">} as appropriate.
     */
    DEFER("defer"),

    /**
     * Intended for page’s non-critical, dynamic behaviours. Code only runs after all page render scripts completed.
     */
    INTERACTION("interaction");

    private final String label;
    private static final Map<String, ResourcePhase> BY_LABEL = new HashMap<>();

    ResourcePhase(String label) {
        this.label = label;
    }

    static {
        for (ResourcePhase resourcePhase : values()) {
            BY_LABEL.put(resourcePhase.label, resourcePhase);
        }
    }

    public static ResourcePhase getPhaseOrDefault(@Nullable final String phase) {
        final String key =
                Optional.ofNullable(phase).map(p -> p.toLowerCase(Locale.ROOT)).orElse(null);
        return BY_LABEL.getOrDefault(key, defaultPhase());
    }

    /**
     * Method responsible for managing the default phase.
     *
     * @return Returns the current default phase.
     */
    public static ResourcePhase defaultPhase() {
        return REQUIRE;
    }
}
