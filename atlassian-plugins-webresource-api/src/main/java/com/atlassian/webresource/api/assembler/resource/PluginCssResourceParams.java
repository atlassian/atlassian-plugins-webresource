package com.atlassian.webresource.api.assembler.resource;

/**
 * Parameters for a CSS webresource
 *
 * @since v3.0
 */
public interface PluginCssResourceParams extends PluginUrlResourceParams {
    /**
     * @return this resource's media param, otherwise null
     */
    String media();
}
