package com.atlassian.webresource.api.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadableResource;

/**
 * Describes a resource for transformation
 *
 * @since 6.3.0
 */
public interface TransformableResource {

    /**
     * The original resource location
     */
    ResourceLocation location();

    /**
     * The original resource
     */
    DownloadableResource nextResource();
}
