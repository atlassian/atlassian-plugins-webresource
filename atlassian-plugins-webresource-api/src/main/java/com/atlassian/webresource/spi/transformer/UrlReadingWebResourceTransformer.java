package com.atlassian.webresource.spi.transformer;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformableResource;

/**
 * The part of a transformer that reads the config and url params and does the actual resource transformation.
 *
 * @since 6.3.0
 */
public interface UrlReadingWebResourceTransformer {
    /**
     * Transforms the downloadable resource by returning a new one.  This is invoked when the resource is being served.
     *
     * @param transformableResource Information describing the resource to transform
     * @param params query params
     * @return The new resource representing the transformed resource
     */
    DownloadableResource transform(TransformableResource transformableResource, QueryParams params);
}
