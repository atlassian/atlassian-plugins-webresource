package com.atlassian.webresource.spi.transformer;

import com.atlassian.webresource.api.url.UrlBuilder;

/**
 * The part of a transformer that contributes to the URL
 *
 * @since 6.3.0
 */
public interface TransformerUrlBuilder {
    /**
     * Called when constructing the URL as the hosting HTML page is being served. Can add parameters to the query
     * string and alter the resource hash.
     *
     * @param urlBuilder interface for contributing to the URL
     */
    void addToUrl(UrlBuilder urlBuilder);
}
