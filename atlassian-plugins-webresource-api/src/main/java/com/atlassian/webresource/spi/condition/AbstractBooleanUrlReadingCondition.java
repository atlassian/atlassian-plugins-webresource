package com.atlassian.webresource.spi.condition;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;

/**
 * Simple abstract implementation for {@link UrlReadingCondition} implementations.
 * <p>
 * Handles encoding a single boolean into the url. The convention here is that if the condition is true, it is added
 * the querystring; if false it is not added to the querystring.
 *
 * @since 6.3.0
 */
public abstract class AbstractBooleanUrlReadingCondition implements UrlReadingCondition {

    protected static final String TRUE = Boolean.TRUE.toString();

    @Override
    public void init(Map<String, String> params) throws PluginParseException {}

    @Override
    public void addToUrl(UrlBuilder urlBuilder) {
        if (isConditionTrue()) {
            urlBuilder.addToQueryString(queryKey(), TRUE);
        }
    }

    @Override
    public boolean shouldDisplay(QueryParams params) {
        return Boolean.parseBoolean(params.get(queryKey()));
    }

    /**
     * @return true if this condition passes. This is evaluated at url generation time.
     */
    protected abstract boolean isConditionTrue();

    /**
     * Should be unique to avoid clashes
     *
     * @return the key to append to the query string
     */
    protected abstract String queryKey();
}
