package com.atlassian.webresource.spi.condition;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;

/**
 * Condition that contributes to the URL.
 * <p>
 * UrlReadingConditions are included in batches. When an HTML page is served, the UrlReadingCondition is asked to
 * contribute to the URL in {@link #addToUrl(UrlBuilder)}. At resource serve time,
 * the condition evaluates whether it should display based on the querystring.
 *
 * @since 6.3.0
 */
public interface UrlReadingCondition {
    /**
     * Called after creation and autowiring.
     *
     * @param params The optional map of parameters specified in XML.
     */
    void init(Map<String, String> params) throws PluginParseException;

    /**
     * Called when constructing the URL as the hosting HTML page is being served. Can add parameters to the query
     * string and alter the resource hash.
     *
     * @param urlBuilder interface for contributing to the URL
     */
    void addToUrl(UrlBuilder urlBuilder);

    /**
     * Determine whether the web fragment should be displayed. This method should only read values from its config
     * and the query params.
     *
     * @param params query params
     * @return true if the user should see the fragment, false otherwise
     */
    boolean shouldDisplay(QueryParams params);
}
