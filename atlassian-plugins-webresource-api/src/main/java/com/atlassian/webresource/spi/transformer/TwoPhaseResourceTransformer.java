package com.atlassian.webresource.spi.transformer;

import java.io.InputStream;
import java.util.function.Function;

import com.atlassian.plugin.elements.ResourceLocation;

/**
 * Allows implementors to receive metadata captured from build-time processing steps
 * and to decide whether to use an alternate transformation strategy based on that metadata.
 *
 * Use in conjunction with other transformer interfaces to reduce costs when
 * transforming content at product runtime.
 */
public interface TwoPhaseResourceTransformer {
    /**
     * For a given file to be transformed, find and load any build-time metadata associated with it.
     *
     * @param resourceLocation the locality of a resource within a classpath (e.g., plugin, application container, etc.)
     * @param loadFromFile a function that can take a relative filepath and return an inputstream for the contents at that filepath.
     *                     this function should be locked to an appropriate classpath.
     */
    void loadTwoPhaseProperties(ResourceLocation resourceLocation, Function<String, InputStream> loadFromFile);

    /**
     * Allows a transformer to decide whether it has enough data to run an alternate transformation strategy
     * using the data loaded when {@link #loadTwoPhaseProperties(ResourceLocation, Function)} was called.
     *
     * @return true if an alternate transformation strategy should be used, false otherwise
     */
    boolean hasTwoPhaseProperties();
}
