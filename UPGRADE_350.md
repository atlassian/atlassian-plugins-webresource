## Host application upgrade notes

### Cross-tenant CDN and pre-baking

With 3.5.0, several implementations related to cross-tenant CDN and pre-baking resources were introduced.

In order to pre-bake resources from products some interfaces were added and new methods added to others.
The main new concept introduced is that all contributors to the content of a resource should be **dimension aware**.
It means they should provide all dimensions, i.e., all possible states they can assume.

**First**, all transformer factories should implement the interface `DimensionAwareWebResourceTransformerFactory` as its
new method `computeDimensions`. A simple implementation of this method whose dimensions are the locales is as follows:

```java
String QUERY_KEY = "locale";

@Override
public Dimensions computeDimensions() {
    List<String> locales = StreamSupport.stream(webResourceIntegration.getSupportedLocales().spliterator(), false)
            .map(LocaleUtils::serialize).collect(Collectors.toList());
    return Dimensions.empty().andExactly(QUERY_KEY, locales);
}}
```

It means the content of this transformer changes according to the locale only. This is closely related to the query
parameter modifying the URL, so the dimensions would be the set of values that query parameter can assume,
including the omission of the parameter.

**Second**, as transformers must contribute to the URL in order to avoid caching issues, `TransformUrlBuilders` should
also implement a new method to contribute to the URL during the pre-baking process. So all URL builders should implement
`DimensionAwareTransformerUrlBuilder` and its method `addToUrl(UrlBuilder urlBuilder, Coordinate coord)`. The ordinary
`addToUrl` is something like this:

```java
@Override
public void addToUrl(UrlBuilder urlBuilder)
{
    String locale = LocaleUtils.serialize(webResourceIntegration.getLocale());
    addToUrl(urlBuilder, locale);
}
```

The new method has the same nature, however the current "state" it not retrieved from the environment,
but from a `Coordinate`. This way the pre-baker can build all URLs it needs. For instance:

```java
@Override
public void addToUrl(UrlBuilder urlBuilder, Coordinate coord)
{
    String locale = coord.get("locale");
    addToUrl(urlBuilder, locale);
}
```

In the code above `locale` is name of the coordinate that affects the URL, similar to a query parameter.

**Third**, all conditions should also provide its dimensions and contribute to the URL implementing the method
`addToUrl` as in the URL builder. So, all classes implementing `UrlReadingCondition` should now implement
`DimensionAwareUrlReadingCondition`.

The good news is that if a condition is based on `SimpleUrlReadingCondition` it'll inherit dimension-aware behaviour
and no additional work is needed. Otherwise you should implement `computeDimensions` and the new `addToUrl`.

**Fourth**, the interface `WebResourceIntegration` has a new method named `getSupportedLocales`. Obviously the product
implementing the interface should return all supported locales.

In JIRA, `JiraWebResourceIntegration` simply returns a set using its internal API:

```java
@Override
public Iterable<Locale> getSupportedLocales() {
    return localeManager.getInstalledLocales();
}
```

**Fifth**, the interface `CDNStrategy` has a new method named `getPrebakeConfig` that can be used to activate mapping
of resources. It means if your product has resources pre-baked in the CDN and you provide the proper mapping file
Web Resource will transparently serve the user with the CDN URLs. Note that `PrebakeConfig` is also a new interface
and has a factory method in itself.

**Sixth**, some components from Web Resource now implement new interfaces. One example is
`DefaultWebResourceAssemblerFactory` which now implements `PrebakeWebResourceAssemblerFactory`. If one product makes
use of it with a Dependency Injection container like Pico, it should provide this component for the new interfaces.
Example:

```java
MultipleKeyRegistrant.registrantFor(DefaultWebResourceAssemblerFactory.class)
        .implementing(PrebakeWebResourceAssemblerFactory.class)
        .implementing(WebResourceAssemblerFactory.class)
        .registerWith(PROVIDED, register);
```

Other new interfaces are `PrebakeWebResourceAssembler` and `PrebakeWebResourceAssemblerBuilder`.

**Seventh**, remove the implementation for `WebResourceIntegration.getCacheFactory` method, it's not used anymore.

**Eight**, constructor for `StaticTransformersSupplier` has been changed, you may need to update its IoC registration
code.

### Root pages

"Root pages" are a declared set of contexts and resources that can be required during a request just like a normal Context
or Web Resource.

You can see the original RFC [here](https://extranet.atlassian.com/pages/viewpage.action?pageId=2664442104), just be aware 
that page hasn't been polished.

#### How it works

A definition example from JIRA Dashboard:

```xml
<web-resource key="dashboard-page">
    <root-page/>
    <dependencies>
        <web-resource>com.atlassian.gadgets.dashboard:dashboard</web-resource>
        <web-resource>com.atlassian.gadgets.dashboard:gadget-dashboard-resources</web-resource>
        <web-resource>com.atlassian.jira.jira-tzdetect-plugin:tzdetect-lib</web-resource>
        <web-resource>jira.webresources:calendar-en</web-resource>
        <web-resource>jira.webresources:calendar-localisation-moment</web-resource>
        <web-resource>jira.webresources:calendar</web-resource>
        <web-resource>jira.webresources:global-static</web-resource>
        <web-resource>jira.webresources:header</web-resource>
        <web-resource>jira.webresources:jira-fields</web-resource>
        <web-resource>jira.webresources:key-commands</web-resource>
        <context>atl.dashboard</context>
        <context>atl.general</context>
        <context>atl.global</context>
        <context>jira.dashboard</context>
        <context>jira.general</context>
        <context>jira.global</context>
    </dependencies>
</web-resource>
```

And the way it's required:

```java
assembler.resources().requirePage("com.atlassian.jira.gadgets:dashboard-page");
```

So all items declared above would be included for that request.

#### Rationale

The concept of "root pages" is to provide a declarative approach which would leverage browser caching and allow the 
CT-CDN pre-baker to know beforehand what a page requires and put that resource in the CDN. 

In an ideal world, all pages would require one and only one "root page" with no additional contexts and resources. If 
something need to be dynamic, a `SimpleUrlReadingCondition` would be a good solution, which adds a query parameter
to distinguish between one batch content and another.

Unfortunately, there are limitations referring to plugin's ability to add arbitrary contexts and resources to pages. 
If this is the case, resources will miss CT-CDN and browser cache, falling back probably to Per-Tenant CDN. 
You can read more [here](https://extranet.atlassian.com/display/APDEX/RFC%3A+Root+Page+API) and
[here](https://extranet.atlassian.com/display/JCP/RFC+pre-bake+analyzer).
 
Thus "root pages" are a limited solution which brings some value mainly when the code implementing a page could avoid
being "too dynamic"