# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

# [7.3.0]

## Added

- [DCA11Y-1211] Provide default Spring wiring

## Fixed

- [JSEV-3707] `AJS.I18n.getText()` correctly handles empty strings as values 

# [7.2.0]

## Changed
- [PLUGWEB-775] [PLUGWEB-776] [PLUGWEB-777] [PLUGWEB-778] Remove dead code including unused methods from WebResourceIntegration

# [7.1.0]

## Added
- [PLUGWEB-773]: Add support for a new `source` parameter of `pluginSharedHome` for web-resources to allow loading them from product home filesystem locations. 

## [7.0.3]

### Fixed
- [DCPL-1440] ProfilingMetricsTest fixed by adding back the noisy-neighbour-plugin as bundled artifact to WRM test module.

## [7.0.2]

### Changed

- [PLUGWEB-759] Expose missing batch configuration properties from DefaultResourceBatchingConfiguration

### Fixed
- [BSP-5978] Upgrade the tomcat-embed-core dependency to 9.0.83

## [7.0.1]

### Added
- Optimization SPI allUsedQueryParameters to WebResourceTransformerFactory and ContentTransformerFactory

### Fixed
- [DCA11Y-994] PLUGWEB-702: Remove dangling IE code
- [DCA11Y-1004] Ensure query parameters influencing transformations are part of the WRM transform caching keys

## [7.0.0]

### Added

- [PLUGWEB-725] [PLUGWEB-726] Public API version of `UrlReadingWebResourceTransformerModuleDescriptor`
- [PLUGWEB-725] [PLUGWEB-726] Public API version of `WebResourceModuleDescriptor`

### Fixed
- [BSP-5255] Bumped platform to 6.0.36 and transitive spring to 5.3.29
- [PLUGWEB-674] Clientside will now throw an error if adding resource of unknown type
- [PLUGWEB-682] `data-collector-async` no longer requires `aui-core` and `js-event` dependencies to be provided explicitly
- [PLUGWEB-697] Remove old SPI types
- [PLUGWEB-717] Remove SimpleUrlReadingCondition
- [PLUGWEB-694] Remove org.dom4j from API
- [PLUGWEB-700] Remove curl web-resource
- [PLUGWEB-703] Remove deprecated WebResourceAssemblerFactory#clearCache method
- [PLUGWEB-724] Remove migrated API types
- [PLUGWEB-706] Remove resource compiler
- [PLUGWEB-696] Remove prebake from the API module
- [PLUGWEB-712] Remove PluginResourceLocator#temporaryWayToGetGlobalsDoNotUseIt
- [PLUGWEB-764] Remove IE from the API
- [PLUGWEB-729] Remove support for legacy conditions and transformers
- [PLUGWEB-701] remove AMD from the API
- [PLUGWEB-767] Improve handling of non-ascii encoded resources

## [6.3.0]

### Changed
- [PLUGWEB-714] Remove the @ExperimentalApi annotation from what we're committing to
- [PLUGWEB-698] Replace dom4j in what will be in API
- [DCPL-525] Deprecate com.google.common API in atlassian-plugins-webresource
- [PLUGWEB-716] Extract SimpleUrlReadingCondition without the prebake stuff into SPI
- [PLUGWEB-719] Deprecate #executeInNewContext from API
- [PLUGWEB-722] Replace Guava in the API

## [6.2.0]

### Changed
- [PLUGWEB-672] The minimum phase for clientside inclusions is now DEFER

### Fixed
- [PLUGWEB-670] Clientside inclusions can execute prior to their serverside dependencies
- [PLUGWEB-675] Duplicate inclusions when clientside drains before the document is finished parsing
- [PLUGWEB-688] Expose setting the log level as an AMD module
- [BSP-5852] Updated velocity to atlassian fork version

## [6.1.1]

### Added
- [PLUGWEB-671] Add warning log for when i18n is missing.

## [6.1.0]

### Fixed
- [BSP-5263] Updated jsoup to 1.15.3
- [BSP-5255] Bumped platform to 6.0.36 and transitive spring to 5.3.29
- [SPFE-908] Completed removal of JAXB
- [BSP-3837] Upgrade platform version to 5.2.11 and spring to 5.3.19
- [BSP-3912] Moved the ResourceUrlTest class to WRM test package
- [CONFSRVDEV-23734] Updated confluence version to 7.18.1
- [CONFSRVDEV-23734] Updated atlassian.profiling.version to 4.6.1
- [CONFSERVER-79610] Fix ClassCastException with Struts2
- [BSP-4539] Upgrade the commons-text dependency to 1.10.0
- [BSP-5264] replacing rollup-plugin-terser  (since it's deprecated) to @rollup/plugin-terser

## [6.0.6]
### Fixed
- [BSP-5263] Updated jsoup to 1.15.3
- [BSP-5264] replacing rollup-plugin-terser  (since it's deprecated) to @rollup/plugin-terser
- [PLUGWEB-528] Allow use of whitespace in number and choice syntax within formatted strings for pluralising internationalisation strings.
- Fixed behaviour of limits in choice syntax to no longer include exact matches when using `<`.
- [PLUGWEB-671] Add warning log for when i18n is missing.
- [BSP-5871] Downgraded tomcat-embed-core to 9.0.81.


## [6.0.5]

### Fixed
- [BSP-5255] Bumped platform to 6.0.36 and transitive spring to 5.3.29
- [BSP-4539] Upgrade the commons-text dependency to 1.10.0

## [6.0.4]

### Fixed
- [SPFE-908] Completed removal of JAXB

## [6.0.3]

### Fixed
- [BSP-3700] Updated prompts version to 2.4.2
- [BSP-3782] Updated spring-beans to 5.3.18
- [BSP-3837] Upgrade platform version to 5.2.11 and spring to 5.3.19

## [6.0.2]

### Fixed
- [BSP-3674] Upgrade json-schema version to 0.4.0
- [BSP-3693] Audit plugin error while upgrading to version 1.14.0

## [5.6.7]

### Fixed
- [BSP-5263] Updated jsoup to 1.15.3
- [BSP-5264] replacing rollup-plugin-terser  (since it's deprecated) to @rollup/plugin-terser
- [BSP-5255] Bumped platform to 5.3.35 and transitive spring to 5.3.29
- [BSP-4539] Upgrade the commons-text dependency to 1.10.0

## [5.6.6]

### Fixed
- [PLUGWEB-667] WRM DarkFeature check fails with NoSuchElementException
- [BSP-3837] Upgrade platform version to 5.2.11 and spring to 5.3.19

## [5.6.5]

### Fixed
- [BSP-3700] Updated prompts version to 2.4.2
- [BSP-3782] Updated spring-beans to 5.3.18
- [BSP-3693] Audit plugin error while upgrading to version 1.14.0

## [5.6.4]

### Fixed
- [BSP-3674] Upgrade json-schema version to 0.4.0

## [5.6.2]

### Fixed
- [PLUGWEB-661] WRM 5 resource stats do not collect data for CSS assets
- [PLUGWEB-662] Do not throw NPE when a context is included and excluded at the same time
- [PLUGWEB-644] Double `''` apostrophes rendered for an i18n translation when two-phases i18n mechanism is used and the translation is not parametrised

## [5.6.0]

### Added
- [PLUGWEB-536] added new `INLINE` resource phase. Resources required using this phase will return their content
  directly and synchronously; no additional lookups (e.g., URLs) are needed.
- [ITAS-174] added JMX metrics for web resource conditions which will emit the time and number of times a `Condition#shouldDisplay` is evaluated.
- [SPFE-769](https://ecosystem.atlassian.net/browse/SPFE-769) Revert the breaking changes that caused creating a fork version of WRM for the Bitbucket product.

  We are adding back the Bitbucket specific change related to CDN feature. For more details check [BBSDEV-20883](https://bulldog.internal.atlassian.com/browse/BBSDEV-20796).

### Changed
- The `_sync` batch is now loaded via the `INLINE` resource phase.
- Server-side resource requests using the `INTERACTION` resource phase will yield a `<script defer>` HTML tag instead of a blocking `<script>` tag.
  This means the earliest that an interaction resource request will be discovered & requested client-side is just before `DomContentLoaded`.
  The execution time for `INTERACTION` resources is unchanged.

### Fixed
- [PLUGWEB-660] Include WRM resources in the same phase as superbatch
- [PLUGWEB-662] Do not throw NPE when a context is included and excluded at the same time

## [5.5.12]

### Fixed
- [BSP-4539] Upgrade the commons-text dependency to 1.10.0

## [5.5.11]

### Fixed
- [PLUGWEB-667] WRM DarkFeature check fails with NoSuchElementException
- [BSP-3837] Upgrade platform version to 5.2.11, refapp version to 5.4.4

## [5.5.10]

### Fixed
- [BSP-3700] Updated prompts version to 2.4.2
- [BSP-3782] Updated spring-beans to 5.3.18
- [BSP-3693] Audit plugin error while upgrading to version 1.14.0

## [5.5.9]

### Fixed
- [BSP-3674] Upgrade json-schema version to 0.4.0
- [BSP-3693] Audit plugin error while upgrading to version 1.14.0

## [5.5.7]

### Fixed
- [PLUGWEB-660] Include WRM resources in the same phase as superbatch
- [PLUGWEB-662] Do not throw NPE when a context is included and excluded at the same time
- [PLUGWEB-661] WRM 5 resource stats do not collect data for CSS assets
- [PLUGWEB-664] Double `''` apostrophes rendered for an i18n translation when two-phases i18n mechanism is used and the translation is not parametrised

## [5.5.6]

### Fixed
- [PLUGWEB-652] Avoid setting invalid `Last-Modified` values for batch files.

## [5.5.5]

### Fixed
- [PLUGWEB-654] web-resources in superbatch will not be double-served in dev mode if another context loads an overlapping content set.

## [5.5.4]

### Changed
- [PLUGWEB-640] web-resource and context inclusion order via `/wrm/2.0/resources` endpoint is significant.
  This behaviour should not be depended upon. Use web-resource `<dependency>` declarations to ensure code load order.
- [PLUGWEB-640] web-resource and context inclusion order via Java APIs is significant.
  This behaviour should not be depended upon. Use web-resource `<dependency>` declarations to ensure code load order.
- [PLUGWEB-640] contextbatch URLs are no longer commutative. `/contextbatch/js/AAA,BBB/batch.js` will return different content to `/contextbatch/js/BBB,AAA/batch.js`.
- [PLUGWEB-663] bump versions of Atlassian Plugins, Atlassian REST and platform-poms to update to [Platform 6 Milestone 2](https://hello.atlassian.net/wiki/spaces/AB/pages/1413652184/Platform+6+-+Milestone+02)

### Fixed
- [PLUGWEB-649] if multiple contextbatch URLS are generated in a single drain, they will not double-load resources from previous URLs.
- [PLUGWEB-641] if the default phase for the superbatch is changed, it will not double-load resources loaded in previous phases.
- [PLUGWEB-648] content inclusion order is now the same across all batching modes.

## [5.5.3]

### Fixed
- [PLUGWEB-635] contextbatch URLs will list the smallest possible set of keys to exclude, keeping total URL length down.

## [5.5.2]

### Changed

- [PLUGWEB-634] `_super` and `_sync` will only be subtracted from contextbatch URLs when they are non-empty and have previously been served.
- Performance analytics are only included when a `WebResourceSet` is built and only
  if `WebResourceAssemblerBuilder#autoIncludeFrontendRuntime(boolean)` was set to `true`.

### Fixed

- `/rest/wrm/2.0/requestables/_super/dependency-graph` will return all items in the superbatch.
- `/rest/wrm/2.0/requestables/_sync/dependency-graph` will return all items in the sync batch.
- Fixed bug where all items were considered part of the superbatch.

## [5.5.1]

### Fixed

- [PLUGWEB-631] added temporary patch to avoid omitting required content in contextbatch URLs.
    - This patch may trigger [PLUGWEB-440] in more instances, as the URL parts will get very long.
      A more robust patch will be made in a subsequent 5.5.x patch release.
- [PLUGWEB-632] Resources requested in the `interaction` phase or via `WRM.requireLazily` will resolve if made before the `DomContentLoaded` event fires.

## [5.5.0]

### Added

- [PLUGWEB-547] support changing phase for the superbatch by `RequiredResources#requireSuperbatch`. This gives the ability to change
  superbatch phase per page. As well as excluding superbatch per page by `RequiredResources#excludeSuperbatch` method.
- [PLUGWEB-550] REST endpoint `/rest/wrm/2.0/requestables` provides metadata from WRM's constructed dependency graph.
  Given a `web-resource` or context key, these endpoints will return information about it.
  - [PLUGWEB-554] `/rest/wrm/2.0/requestables/{requestableKey}/consumer-graph` returns all direct and transitive
    consumers of a given `requestableKey` as a list of edges. Useful for discovering the ways a specific web-resource
    can be loaded.
  - [PLUGWEB-555] `/rest/wrm/2.0/requestables/{requestableKey}/dependency-graph` returns all direct and transitive
    dependencies discoverable from `requestableKey` as a list of edges. Useful for determining the breadth of content
    that will be included whenever the `requestableKey` is loaded.
- [PLUGWEB-627] Added `WebResourceAssemblerBuilder#includeSyncbatchResources(boolean)` so page authors may choose to omit the sync batch. It is enabled by default.
- [PLUGWEB-627] Added `WebResourceAssemblerBuilder#autoIncludeFrontendRuntime(boolean)` so page authors may choose to omit frontend runtime components. It is enabled by default.

### Changed

- [PLUGWEB-546] Context batch URL generation will attempt to exclude individual web-resources when it would result in fewer URLs overall.
- [PLUGWEB-627] `WRM.require` and `WRM.requireLazily` will only auto-load if at least one thing was requested in the `INTERACTION` resource phase and `WebResourceAssemblerBuilder#autoIncludeFrontendRuntime` is enabled.

### Fixed

- [PLUGWEB-546] Loading contexts with an overlap multiple phases will use context batch URLs instead of hundreds of individual web-resource batch URLs.

### Removed

- Attribute `type="text/javascript"` is no longer available in generated `script` tags.
- Attribute `type="text/css"` is no longer available in generated `link` tags.

### Deprecated

- [PLUGWEB-547] `WebResourceAssemblerBuilder#includeSuperbatchResources` was deprecated. Use `RequiredResources#excludeSuperbatch` instead.

## [5.4.15]

### Fixed
- [BSP-3700] Updated prompts version to 2.4.2
- [BSP-3782] Updated spring-beans to 5.3.18
- [BSP-3837] Upgrade platform version to 5.2.11, refapp version to 5.4.4

## [5.4.14]

### Fixed
- [BSP-3674] Upgrade json-schema version to 0.4.0
- [BSP-3693] Audit plugin error while upgrading to version 1.14.0

## [5.4.13]

### Fixed
- [PLUGWEB-661] WRM 5 resource stats do not collect data for CSS assets
- [PLUGWEB-662] Do not throw NPE when a context is included and excluded at the same time
- [PLUGWEB-664] Double `''` apostrophes rendered for an i18n translation when two-phases i18n mechanism is used and the translation is not parametrised

## [5.4.12]

### Fixed
- [PLUGWEB-652] Avoid setting invalid `Last-Modified` values for batch files.

## [5.4.11]

### Changed
- [PLUGWEB-640] web-resource and context inclusion order via `/wrm/2.0/resources` endpoint is significant.
  This behaviour should not be depended upon. Use web-resource `<dependency>` declarations to ensure code load order.

### Fixed
- [PLUGWEB-648] content inclusion order is now the same across all batching modes.

## [5.4.10]

### Changed
- [PLUGWEB-640] web-resource and context inclusion order via Java APIs is significant.
  This behaviour should not be depended upon. Use web-resource `<dependency>` declarations to ensure code load order.
- [PLUGWEB-640] contextbatch URLs are no longer commutative. `/contextbatch/js/AAA,BBB/batch.js` will return different content to `/contextbatch/js/BBB,AAA/batch.js`.

## [5.4.9]

### Fixed
- [PLUGWEB-632] Resources requested in the `interaction` phase or via `WRM.requireLazily` will resolve if made before the `DomContentLoaded` event fires.

## [5.4.8]

### Changed
- [PLUGWEB-627] Products can opt out of bundlehash validation on a per-request basis by setting an `_wrm-integrity` attribute to `no-validate` on a `HttpRequest`.

## [5.4.7]

### Fixed
- [DCNG-1352] Bundle hash validation accounts for subtracted contexts regardless of request order.
- [PLUGWEB-625] Bundle hash validation no longer causes some conditional content to be omitted.

## [5.4.6]

## Changed
- `WRM.require` and `WRM.requireLazily` are explicitly configured to work on `same-origin` and to not cache responses.

## Fixed
- Older browsers correctly send credentials in `WRM.require` and `WRM.requireLazily` calls.

## [5.4.5]

### Fixed
- [PLUGWEB-621] Bundle hash validation accounts for subtracted contexts.
- [PLUGWEB-624] Bundle hash validation accounts for shared dependencies where they are conditional on one side.

## [5.4.4]

### Fixed
- [PLUGWEB-623] IE-only CSS is no longer emitted when loading resources via the client-side.

## [5.4.3]

### Changed
- Introduced `onDeepFilterFail` parameter to `BundleFinder`.
- Bundle hash now reflects full bundle tree state regardless of filtering results.

### Fixed
- [PLUGWEB-621] Bundle hash values reflect every dependency traversable for a given request, regardless of conditions.
- [PLUGWEB-622] Resources provided in `WebResourceIntegration#getSyncWebResourceKeys` are now filtered by predicates
  passed to `WebResourceSet#writeHtmlTags(Writer, UrlMode, Predicate)`.

## [5.4.2]

### Highlights

Batch URLs got reviewed and long-standing issues were fixed:

- Context batch URLs are long-term cacheable on CDN.
- Source maps now work for context batch and web-resource batch URLs.

### Fixed

- [PLUGWEB-447] Update to sourcemap v2 which introduces support for `sourcesContent`.

## [5.4.1]

Release failed.

## [5.4.0]

Release failed.

## [5.3.3]

### Fixed
- [PLUGWEB-632] Resources requested in the `interaction` phase or via `WRM.requireLazily` will resolve if made before the `DomContentLoaded` event fires.

## [5.3.2]

- [PLUGWEB-610] For testing purposes WRM client consumes `window.WRM.__localeOverride` property which allows to override the locale used to load resources

### Removed
- [PLUGWEB-618] Remove unnecessary `jgrapht` library

## [5.3.1]

### Highlights
- `JsI18nTransformer` is now ~20x faster after optimisation.

### Added
- [PLUGWEB-567] New `TwoPhaseResourceTransformer` interface allows for implementors to receive metadata captured from
  build-time processing steps and to decide whether to use an alternate transformation strategy based on that metadata.

### Changed
- [PLUGWEB-568] `wrm/i18n` javascript module has `WRM.I18n.km` property, which is a `Map` of i18n keys to their
  locale-specific translations.
- [PLUGWEB-568] `JsI18nTransformer` implements `TwoPhaseResourceTransformer`. When plugin authors provide
  a `.i18n.properties` file for each JS file in their plugin, the transformer will write used translations to
  the `WRM.I18n.km` map rather than using the more expensive regex-based substitution strategy.
- [PLUGWEB-422] `JsI18nTransformer` reduced the length of the identifier prior to the `I18n.getText(` from 1001 to 101
  characters for performance
- [PLUGWEB-611] The `bundlehash` value in context-batch URLs is now based on all bundles that contribute to the batch,
  including conditional bundles whose content would not be included in the final output. This results in a value that
  does not change when URL-reading conditions (and their GET parameters) change values.
- [PLUGWEB-611] Changed property `atlassian.webresource.disable.resource.counter.hash.validation`
  to `atlassian.webresource.enable.bundle.hash.validation`

## 5.3.0

Broken release. Do not use.

## [5.2.2]

### Fixed
- [PLUGWEB-608] Disable caching web resource files explicitly excluded from batching.
- [PLUGWEB-607] Improve robustness of performance monitoring frontend code.

## [5.2.1]

### Fixed
- [PLUGWEB-562] Resources endpoints (1.0, 2.0) are now able to return `unparsedData` when context call usage.
- [PLUGWEB-560] Add missing event attribute `cacheableRequest` to the analytics allowlist for the `wrm.caching.request.server` event.
- [PLUGWEB-559] Fixed AJS loading while adding WRM as mandatory dependency for all pages.
- [PLUGWEB-564] Fixed broken resource loading while loaded from plugin module.

## [5.2.0]

### Added
- analytics to track effectiveness of resource processing caching and plugin system state caching.

## [5.1.2]

### Fixed
- Fixed AJS loading while adding WRM as mandatory dependency for all pages.
- Fixed broken resource loading while loaded from plugin module.
- [PLUGWEB-560] Add missing event attribute `cacheableRequest` to the analytics allowlist for the `wrm.caching.request.server` event.

## [5.1.1]

### Changed

- Batch resources hash validation is now disabled by default. Changed
  property `atlassian.webresource.disable.resource.counter.hash.validation`
  to `atlassian.webresource.enable.resource.counter.hash.validation`.

## [5.1.0]

### Added
- Analytics to track how effective existing resource caching is

## [5.0.3]

### Fixed
- [PLUGWEB-559] - Fixed AJS loading while adding WRM as mandatory dependency for all pages.
- [PLUGWEB-562] - Resources endpoints (1.0, 2.0) are now able to return `unparsedData` when context call usage.
- [PLUGWEB-564] - Fixed broken resource loading while loaded from plugin module.

## [5.0.2]

### Changed
- Batch resources hash validation is now disabled by default.
  Changed property `atlassian.webresource.disable.resource.counter.hash.validation` to `atlassian.webresource.enable.resource.counter.hash.validation`.
- [PLUGWEB-88] - All logging in `DefaultResourceDependencyResolver#resolveDependencies` is now `DEBUG` level.
- [PLUGWEB-290] - All logging in `DefaultResourceDependencyResolver#resolveDependencies` includes the web-resource dependency chain info.
  This can help determine which web-resource(s) are mis-configured.

## [5.0.1]

### Fixed
- [SPFE-527] - Reverted previous API of WebResourceTransformer and TransformableResource.

## [5.0.0]

### Highlights
- Dropped support for IE 11.
- WRM has no dependency on `jQuery` anymore!
- WRM has resource phases to adjust when web-resources are loaded.

### Added
- [PLUGWEB-513] - Resource phases for loading web-resources:
  - `require` phase: code loads and runs immediately when the browser parses the `<script>` or `<link>` tag.
  - `defer` phase: code loads asynchronously and runs when the page has finished parsing
  - `interaction` phase: code loads and runs after all `require` and `defer` phase scripts have completed.
- [PLUGWEB-543] - Private REST API at `/wrm/2.0/resources` for requesting resources with phases.
- [PLUGWEB-543] - Private Swagger definitions for REST APIs.

### Changed
- [PLUGWEB-502] - `JavaScriptWebResource#matches` only returns `true` for `.js` files. Any `.soy` files will not be matched.
- [PLUGWEB-502] - `CssWebResource#matches` only returns `true` for `.css` files. Any `.less` files will not be matched.
- [SPFE-423] - Updated atlassian-html-encoder to v1.5, up from v1.4
- `WRM.require` returns a `Promise` instead of a `jQuery.Deferred` object.
- `WRM.require` requests data via `fetch` instead of `jQuery.ajax`.

### Deprecated
- [SPFE-476] - `WRM.curl` is unused internally. It has moved to its own `com.atlassian.plugins.atlassian-plugins-webresource-rest:curl` web-resource and is deprecated.
- [SPFE-476] - `/webResources/1.0/` REST APIs are all deprecated.
- [SPFE-503] - `WebResourceIntegration#isDeferJsAttributeEnabled` method is deprecated in favour of `DEFER` phase.

### Removed
- Prebake packages to discover and crawl web-resources at build-time have been removed.
- [PLUGWEB-502] - Removed dormant "async" dependencies from web-resource descriptors.
- [PLUGWEB-502] - Removed dormant `web-modules` feature.
- [SPFE-460] - Removed `AMD` flags and associated module loader behaviours.
- [SPFE-460] - `WRM.InOrderLoader` has been removed.
- [SPFE-460] - `WRM.contextPath._reset` has been removed.

## [4.5.4]

### Fixed
- [BSP-5263] Updated jsoup to 1.15.3
- [BSP-3673] Remove the jackson-mapper-asl dependency
- [BSP-3674] Upgrade json-schema version to 0.4.0
- [BSP-3700] Upgrade prompts to version 2.4.2 or higher
- [BSP-3777] Upgrade minimist to version 1.2.6 or higher
- [BSP-3837] Upgrade platform version to 5.2.11 and atlassian-plugins to 5.5.6

## [4.5.3]

### Fixed
- [PLUGWEB-661] WRM 5 resource stats do not collect data for CSS assets
- [PLUGWEB-662] Do not throw NPE when a context is included and excluded at the same time
- [PLUGWEB-644] Double `''` apostrophes rendered for an i18n translation when two-phases i18n mechanism is used and the translation is not parametrised

## [4.5.2]

### Fixed
[PLUGWEB-652] Avoid setting invalid `Last-Modified` values for batch files.

## [4.5.1]

## Fixed
- [PLUGWEB-648] content inclusion order is now the same across all batching modes.

## [4.5.0]

- [SPFE-769] Revert the breaking changes that caused creating a fork version of WRM for the Bitbucket product.
- [BBSDEV-20883] We are adding back the Bitbucket specific change related to CDN feature.

## [4.4.9]

### Fixed
- [BSP-3673] Remove the jackson-mapper-asl dependency
- [BSP-3674] Upgrade json-schema version to 0.4.0
- [BSP-3700] Upgrade prompts to version 2.4.2 or higher
- [BSP-3777] Upgrade minimist to version 1.2.6 or higher
- [BSP-3837] Upgrade platform version to 5.2.11 and atlassian-plugins to 5.5.6
- [BSP-4539] Remove the commons-text dependency

## [4.4.8]

### Fixed
- [PLUGWEB-661] WRM 5 resource stats do not collect data for CSS assets
- [PLUGWEB-662] Do not throw NPE when a context is included and excluded at the same time
- [PLUGWEB-644] Double `''` apostrophes rendered for an i18n translation when two-phases i18n mechanism is used and the translation is not parametrised

## [4.4.4]

## Fixed
- [PLUGWEB-648] content inclusion order is now the same across all batching modes.

## [4.4.3]

### Fixed
- [DCNG-1352] Bundle hash validation accounts for subtracted contexts regardless of request order.
- [PLUGWEB-625] Bundle hash validation no longer causes some conditional content to be omitted.

## [4.4.2]

### Fixed
- [PLUGWEB-621] Bundle hash validation accounts for subtracted contexts.
- [PLUGWEB-624] Bundle hash validation accounts for shared dependencies where they are conditional on one side.

## [4.4.1]

### Changed
- Introduced `onDeepFilterFail` parameter to `BundleFinder`.
- Bundle hash now reflects full bundle tree state regardless of filtering results.

### Fixed
- [PLUGWEB-621] Bundle hash values reflect every dependency traversable for a given request, regardless of conditions.
- [PLUGWEB-622] Resources provided in `WebResourceIntegration#getSyncWebResourceKeys` are now filtered by predicates
  passed to `WebResourceSet#writeHtmlTags(Writer, UrlMode, Predicate)`.

## [4.4.0]

### Highlights

Batch URLs got reviewed and long-standing issues were fixed:

- Context batch URLs are long-term cacheable on CDN.
- Source maps now work for context batch and web-resource batch URLs.

### Changed
- [PLUGWEB-447] Update to sourcemap v2 which introduces support for `sourcesContent`.
- [PLUGWEB-611] Set `atlassian.webresource.enable.bundle.hash.validation` to `true` by default.

## [4.3.12]

### Fixed
[PLUGWEB-652] Avoid setting invalid `Last-Modified` values for batch files.

## [4.3.11]

## Fixed
- [PLUGWEB-648] content inclusion order is now the same across all batching modes.

## [4.3.10]

### Fixed
- [DCNG-1352] Bundle hash validation accounts for subtracted contexts regardless of request order.
- [PLUGWEB-625] Bundle hash validation no longer causes some conditional content to be omitted.

## [4.3.9]

### Fixed
- [PLUGWEB-621] Bundle hash validation accounts for subtracted contexts.
- [PLUGWEB-624] Bundle hash validation accounts for shared dependencies where they are conditional on one side.

## [4.3.8]

### Changed
- Introduced `onDeepFilterFail` parameter to `BundleFinder`.
- Bundle hash now reflects full bundle tree state regardless of filtering results.

### Fixed
- [PLUGWEB-621] Bundle hash values reflect every dependency traversable for a given request, regardless of conditions.

## [4.3.7]

### Changed
- [PLUGWEB-611] Changed property `atlassian.webresource.disable.resource.counter.hash.validation`
    to `atlassian.webresource.enable.bundle.hash.validation`
- [PLUGWEB-611] Refactored bundle hash calculation.

## [4.3.6]

### Changed
- [PLUGWEB-611] Batch resources hash validation is now disabled by default.
  Changed property `atlassian.webresource.disable.resource.counter.hash.validation` to `atlassian.webresource.enable.resource.counter.hash.validation`.

## [4.3.5]

### Added
- [DCNG-1299] Resource counter hash is now validated when requesting batch resource.

### Changed
- [PLUGWEB-88] - All logging in `DefaultResourceDependencyResolver#resolveDependencies` is now `DEBUG` level.
- [PLUGWEB-290] - All logging in `DefaultResourceDependencyResolver#resolveDependencies` includes the web-resource dependency chain info.
  This can help determine which web-resource(s) are mis-configured.

## [4.3.4]

### Changed
- Remove Platform POM import and replace with direct dependencies.

## [4.3.3]

### Changed
- Forward-port changes from 4.1.7

## [4.3.2]

### Changed
- Forward-port changes from 4.1.6

## [4.3.1]

### Fixed
- Images and files served relative to a web-resource or context will work as expected
  when incremental cache is enabled.

## [4.3.0]

### Added
- Plugin authors can now omit the plugin key when declaring web-resource `dependency` blocks.
  e.g., `<dependency>:local-web-resource</dependency>` will resolve to a
  `<web-resource key="local-web-resource">` in the same plugin.

## [4.2.9]

### Fixed
- [PLUGWEB-652] Avoid setting invalid `Last-Modified` values for batch files.

## [4.2.8]

### Changed
- Forward-port changes from 4.1.7

## [4.2.7]

### Changed
- Forward-port changes from 4.1.6

## [4.2.6]

### Fixed
- Images and files served relative to a web-resource or context will work as expected
  when incremental cache is enabled.

## [4.2.3]

### Fixed
- The `com.atlassian.plugins.atlassian-plugins-webresource-plugin:amd-loader` web-resource retrieves
  an appropriate resource in Confluence.

## [4.2.2]

### Added
- Added a `com.atlassian.plugins.atlassian-plugins-webresource-plugin:amd-loader` web-resource.
  This does not provide an AMD loader itself; instead, it maps to a web-resource in each product
  where an AMD loader is provided.
  Plugin authors can depend on this to guarantee a `define` and `require` function exist at runtime.

### Fixed
- The "i18n" web-resource depends on the "root" correctly.

## [4.2.1]

### Changed
- Dropped the support for the on-the-fly conversion of relative URLs into absolute URLs in CSS files
- Bad requests to the `/resources` REST endpoint no longer output a full stack trace.

### Fixed
- The "root" web-resource `key` was changed to avoid find-and-replace logic that treats
  double-underscores as the start of a variable name.

## [4.2.0]

### Added
- [PLUGWEB-456] - `WRM.I18n.getText` JavaScript function and `wrm/i18n` modules available from
  the `com.atlassian.plugins.atlassian-plugins-webresource-plugin:i18n` web-resource.

## [4.1.8]

### Fixed
- [PLUGWEB-652] Avoid setting invalid `Last-Modified` values for batch files.

## [4.1.7]

### Changed
- Add missing default implementation for the `rebuildResourceUrlPrefix`
  method added in 4.1.6.

## [4.1.6]

### Changed
- Provide a new `rebuildResourceUrlPrefix` method in `WebResourceIntegration` SPI
  to trigger product-side system hash recalculation.
- Deprecate `getSystemCounter` on `WebResourceIntegration` SPI in favour of new
  `getResourceUrlPrefix` method.
- Add `encodeConfigurationState` method to `CDNStrategy`

## [4.1.5]

### Changed
- Bumped atlassian-plugins version to 5.3.8

## [4.1.4]

### Fixed
- Images and files served relative to a web-resource or context will work as expected
  when incremental cache is enabled.

## [4.1.3]

### Changed
- Deprecation log events will now only log at WARN level if the atlassian.dev.mode property is
  set, otherwise they will be logged at DEBUG level.
- Bad requests to the `/resources` REST endpoint no longer output a full stack trace.

### Fixed
- Callbacks for `WRM.require` calls to unrelated resources will resolve correctly once all resources for that
  specific require call have been loaded.
- `WRM.require` calls that result in an empty set of resources will no longer make a remote request, and their
  callbacks will execute immediately.
- The "root" web-resource `key` was changed to avoid find-and-replace logic that treats
  double-underscores as the start of a variable name.
- [PLUGWEB-454] - The `JsI18nTransformer` will find and transform javascript translation calls in the format of
  `variableName["I18n"].getText(...)` (this format is typically output by Babel 7 and Webpack 4).
- [PLUGWEB-453] - Deleted resource temp files no longer cause permanent resource compilation.

## [4.1.2]

### Changed
- resources can now be flagged with "allow-public-use" param when you want them served with a CORS header

### Fixed
- Fixed a problem where queued callbacks for `WRM.require` would occasionally resolve before their
  required scripts had been loaded.

## [4.1.0]

### Changed
 - Bunch of APIs are not experimental anymore, for example
 `UrlReadingWebResourceTransformer`, `UrlReadingContentTransformer`, `QueryParams`, `UrlBuilder`, `UrlMode`.

### Fixed
- Callbacks for `WRM.require` calls to unrelated resources will resolve correctly once all resources for that
  specific require call have been loaded.
- `WRM.require` calls that result in an empty set of resources will no longer make a remote request, and their
  callbacks will execute immediately.

## [4.0.10]

### Fixed
- [PLUGWEB-652] Avoid setting invalid `Last-Modified` values for batch files.

## [4.0.9]

### Fixed
- [PLUGWEB-453] - Deleted resource temp files no longer cause permanent resource compilation.

## [4.0.8]

### Changed
- Deprecation log events will now only log at WARN level if the atlassian.dev.mode property is
  set, otherwise they will be logged at DEBUG level.
- Bad requests to the `/resources` REST endpoint no longer output a full stack trace.

### Fixed
- Callbacks for `WRM.require` calls to unrelated resources will resolve correctly once all resources for that
  specific require call have been loaded.
- `WRM.require` calls that result in an empty set of resources will no longer make a remote request, and their
  callbacks will execute immediately.
- The "root" web-resource `key` was changed to avoid find-and-replace logic that treats
  double-underscores as the start of a variable name.

## [4.0.7]

### Fixed
- The `JsI18nTransformer` will find and transform javascript translation calls in the format of
  `variableName["I18n"].getText(...)` (this format is typically output by Babel 7 and Webpack 4).

## [4.0.6]

### Changed
- resources can now be flagged with "allow-public-use" param when you want them served with a CORS header

## [4.0.3]

### Changed
- Deprecation log events will now only log at WARN level if the atlassian.dev.mode property is
  set, otherwise they will be logged at DEBUG level.

## [4.0.2]

### Changed
- Tomcat's `ClientAbortException`, thrown when the end-user closes connection abruptly,
  will be swallowed by the plugin and not propagated to the main application.

## [4.0.1]
- `WRM.require` will continue to work if the version of jQuery used does not return a `jQuery.Deferred` object
  in its `jQuery.ajax` method.

## [4.0.0]

### Changed
- Adds support for Java 11.
- Now depends on Platform 5 being provided by the host product.

### Fixed
- The superbatch is only excluded from requests automatically if it was drained previously.
- Async requests for resources can now pull in resources if they are referenced through the superbatch.

### Removed
- The WRM has removed support for IE 10 and older.
- The ability to conditionally output web-resources to Internet Explorer has been removed.
  `<resource>` blocks with either an `ieOnly` or `conditionalComment` param will not be rendered at all.

## [3.7.4]

### Fixed
- [PLUGWEB-453] - Deleted resource temp files no longer cause permanent resource compilation.

## [3.7.3]

### Changed
- Bad requests to the `/resources` REST endpoint no longer output a full stack trace.

### Fixed
- The "root" web-resource `key` was changed to avoid find-and-replace logic that treats
  double-underscores as the start of a variable name.

## [3.7.2]

### Fixed
- The `JsI18nTransformer` will find and transform javascript translation calls in the format of
  `variableName["I18n"].getText(...)` (this format is typically output by Babel 7 and Webpack 4).

## [3.7.1]

### Fixed
- Fixed a problem where queued callbacks for `WRM.require` would occasionally resolve before their
  required scripts had been loaded.

## [3.7.0]

### Changed
- Deprecation log events will now only log at WARN level if the atlassian.dev.mode property is
  set, otherwise they will be logged at DEBUG level.

### Fixed
- Callbacks for `WRM.require` calls to unrelated resources will resolve correctly once all resources for that
  specific require call have been loaded.
- `WRM.require` calls that result in an empty set of resources will no longer make a remote request, and their
  callbacks will execute immediately.

## [3.6.7]

### Fixed
- [PLUGWEB-453] - Deleted resource temp files no longer cause permanent resource compilation.

## [3.6.6]

### Fixed
- The `JsI18nTransformer` will find and transform javascript translation calls in the format of
  `variableName["I18n"].getText(...)` (this format is typically output by Babel 7 and Webpack 4).

## [3.6.5]

### Changed
- Tomcat's `ClientAbortException`, thrown when the end-user closes connection abruptly,
  will be swallowed by the plugin and not propagated to the main application.

## [3.6.4]

### Fixed
- `WRM.require` will continue to work if the version of jQuery used does not return a `jQuery.Deferred` object
  in its `jQuery.ajax` method.

## [3.6.3]

### Changed
- Replaced usage of jQuery for DOM element creation with native equivalents.
- Use of `jQuery.Deferred#then` was replaced with `jQuery.Deferred#done` and `jQuery.Deferred#fail`, so that
  behaviour will remain synchronous irrespective of what version of jQuery is used.

## [3.6.1]

### Fixed

- The change to `JsI18nTransformer` in [3.6.0] was reverted;
  translation substitutions will preserve the original variable where the `I18n` object was found.
    - This allows unit tests that stub or mock the original variable to continue working.
- Usages of `AJS.log` have been replaced with `console.log`.

## [3.6.0]

### Added
- `web-resource` module descriptors can be marked as deprecated by adding a `<deprecated/>` child element to them.
  The following attributes and values are accepted:
  `<deprecation since="version" remove="version" alternative="webresource:key">a detailed reason</deprecation>`
- `WRM.format` JavaScript function available from
  the `com.atlassian.plugins.atlassian-plugins-webresource-plugin:format` web-resource

### Changed
- `JsI18nTransformer` will always rewrite translations with substitutions
  to use `WRM.format` -- for instance, `AJS.I18n.getText("foo.bar", 1, 2, 3)` will become
  `WRM.format("value of 'foo.bar' translation key", 1, 2)`.
- Requiring the `com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager`
  web-resource no longer pulls in the `com.atlassian.auiplugin:aui-core` web-resource from AUI.

### Fixed
- The WRM consumes `jQuery` via `window` instead of via `AJS.$`.

## [3.5.45]

### Fixed
- [PLUGWEB-453] Deleted resource temp files no longer cause permanent resource compilation (fixes PLUGWEB-453).

## [3.5.44]

### Fixed
- The `JsI18nTransformer` will find and transform javascript translation calls in the format of
  `variableName["I18n"].getText(...)` (this format is typically output by Babel 7 and Webpack 4).

[Unreleased]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/HEAD%0Datlassian-plugins-webresource-7.2.0

[7.2.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-7.2.0%0Datlassian-plugins-webresource-7.1.2
[7.1.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-7.1.1%0Datlassian-plugins-webresource-7.1.1
[7.1.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-7.1.1%0Datlassian-plugins-webresource-7.1.0
[7.1.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-7.1.0%0Datlassian-plugins-webresource-7.0.3
[7.0.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-7.0.3%0Datlassian-plugins-webresource-7.0.2
[7.0.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-7.0.2%0Datlassian-plugins-webresource-7.0.1
[7.0.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-7.0.1%0Datlassian-plugins-webresource-7.0.0
[7.0.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-7.0.0%0Datlassian-plugins-webresource-6.2.0
[6.2.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-6.2.0%0Datlassian-plugins-webresource-6.1.1
[6.1.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-6.1.1%0Datlassian-plugins-webresource-6.1.0
[6.1.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-6.1.0%0Datlassian-plugins-webresource-6.0.3
[6.0.6]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-6.0.6%0Datlassian-plugins-webresource-6.0.5
[6.0.5]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-6.0.5%0Datlassian-plugins-webresource-6.0.4
[6.0.4]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-6.0.4%0Datlassian-plugins-webresource-6.0.3
[6.0.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-6.0.3%0Datlassian-plugins-webresource-6.0.2
[6.0.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-6.0.2%0Datlassian-plugins-webresource-6.0.1
[6.0.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-6.0.1%0Datlassian-plugins-webresource-6.0.0
[6.0.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-6.0.0%0Drelease%2F5.6.x

[5.6.5]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.6.5%0Datlassian-plugins-webresource-5.6.4
[5.6.4]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.6.4%0Datlassian-plugins-webresource-5.6.3
[5.6.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.6.3%0Datlassian-plugins-webresource-5.6.2
[5.6.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.6.2%0Datlassian-plugins-webresource-5.6.1
[5.6.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.6.1%0Datlassian-plugins-webresource-5.6.0
[5.6.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.6.0%0Datlassian-plugins-webresource-5.5.10

[5.5.10]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.5.10%0Datlassian-plugins-webresource-5.5.9
[5.5.9]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.5.9%0Datlassian-plugins-webresource-5.5.7
[5.5.7]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.5.7%0Datlassian-plugins-webresource-5.5.6
[5.5.6]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.5.6%0Datlassian-plugins-webresource-5.5.5
[5.5.5]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.5.5%0Datlassian-plugins-webresource-5.5.4
[5.5.4]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.5.4%0Datlassian-plugins-webresource-5.5.3
[5.5.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.5.3%0Datlassian-plugins-webresource-5.5.2
[5.5.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.5.2%0Datlassian-plugins-webresource-5.5.1
[5.5.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.5.1%0Datlassian-plugins-webresource-5.5.0
[5.5.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.5.0%0Datlassian-plugins-webresource-5.4.11

[5.4.15]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.15%0Datlassian-plugins-webresource-5.4.14
[5.4.14]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.14%0Datlassian-plugins-webresource-5.4.13
[5.4.13]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.13%0Datlassian-plugins-webresource-5.4.12
[5.4.12]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.12%0Datlassian-plugins-webresource-5.4.11
[5.4.11]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.11%0Datlassian-plugins-webresource-5.4.10
[5.4.10]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.10%0Datlassian-plugins-webresource-5.4.9
[5.4.9]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.9%0Datlassian-plugins-webresource-5.4.8
[5.4.8]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.8%0Datlassian-plugins-webresource-5.4.7
[5.4.7]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.7%0Datlassian-plugins-webresource-5.4.6
[5.4.6]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.6%0Datlassian-plugins-webresource-5.4.5
[5.4.5]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.5%0Datlassian-plugins-webresource-5.4.4
[5.4.4]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.4%0Datlassian-plugins-webresource-5.4.3
[5.4.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.3%0Datlassian-plugins-webresource-5.4.2
[5.4.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.2%0Datlassian-plugins-webresource-5.3.3
[5.4.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.1%0Datlassian-plugins-webresource-5.4.0
[5.4.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.4.0%0Datlassian-plugins-webresource-5.3.3

[5.3.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.3.3%0Datlassian-plugins-webresource-5.3.2
[5.3.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.3.2%0Datlassian-plugins-webresource-5.3.1
[5.3.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.3.1%0Datlassian-plugins-webresource-5.2.2
[5.3.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.3.0%0Datlassian-plugins-webresource-5.2.2

[5.2.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.2.2%0Datlassian-plugins-webresource-5.2.1
[5.2.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.2.1%0Datlassian-plugins-webresource-5.2.0
[5.2.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.2.0%0Datlassian-plugins-webresource-5.1.2

[4.4.8]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.4.8%0Datlassian-plugins-webresource-4.4.4
[5.1.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.1.2%0Datlassian-plugins-webresource-5.1.1
[5.1.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.1.1%0Datlassian-plugins-webresource-5.1.0
[5.1.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.1.0%0Datlassian-plugins-webresource-5.0.1

[5.0.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.0.3%0Datlassian-plugins-webresource-5.0.2
[5.0.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.0.2%0Datlassian-plugins-webresource-5.0.1
[5.0.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.0.1%0Datlassian-plugins-webresource-5.0.0
[5.0.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-5.0.0%0Datlassian-plugins-webresource-4.4.4

[4.5.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.5.3%0Datlassian-plugins-webresource-4.4.8

[4.4.8]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.4.8%0Datlassian-plugins-webresource-4.4.4
[4.4.8]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.4.8%0Datlassian-plugins-webresource-4.4.4
[4.4.4]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.4.4%0Datlassian-plugins-webresource-4.4.3
[4.4.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.4.3%0Datlassian-plugins-webresource-4.4.2
[4.4.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.4.2%0Datlassian-plugins-webresource-4.4.1
[4.4.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.4.1%0Datlassian-plugins-webresource-4.4.0
[4.4.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.4.0%0Datlassian-plugins-webresource-4.3.11

[4.3.11]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.11%0Datlassian-plugins-webresource-4.3.10
[4.3.10]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.10%0Datlassian-plugins-webresource-4.3.9
[4.3.9]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.9%0Datlassian-plugins-webresource-4.3.8
[4.3.8]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.8%0Datlassian-plugins-webresource-4.3.7
[4.3.7]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.7%0Datlassian-plugins-webresource-4.3.6
[4.3.6]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.6%0Datlassian-plugins-webresource-4.3.5
[4.3.5]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.5%0Datlassian-plugins-webresource-4.3.4
[4.3.4]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.4%0Datlassian-plugins-webresource-4.3.3
[4.3.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.3%0Datlassian-plugins-webresource-4.3.2
[4.3.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.2%0Datlassian-plugins-webresource-4.3.1
[4.3.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.1%0Datlassian-plugins-webresource-4.3.0
[4.3.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.3.0%0Datlassian-plugins-webresource-4.2.3

[4.2.8]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.2.8%0Datlassian-plugins-webresource-4.2.7
[4.2.7]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.2.7%0Datlassian-plugins-webresource-4.2.6
[4.2.6]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.2.6%0Datlassian-plugins-webresource-4.2.3
[4.2.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.2.3%0Datlassian-plugins-webresource-4.2.2
[4.2.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.2.2%0Datlassian-plugins-webresource-4.2.1
[4.2.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.2.1%0Datlassian-plugins-webresource-4.2.0
[4.2.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.2.0%0Datlassian-plugins-webresource-4.1.7

[4.1.7]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.1.7%0Datlassian-plugins-webresource-4.0.6
[4.1.6]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.1.6%0Datlassian-plugins-webresource-4.0.5
[4.1.5]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.1.5%0Datlassian-plugins-webresource-4.0.4
[4.1.4]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.1.4%0Datlassian-plugins-webresource-4.0.3
[4.1.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.1.3%0Datlassian-plugins-webresource-4.0.2
[4.1.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.1.2%0Datlassian-plugins-webresource-4.0.0
[4.1.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.1.0%0Datlassian-plugins-webresource-4.0.9
[4.1.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.1.0%0Datlassian-plugins-webresource-4.0.9

[4.0.9]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.0.9%0Datlassian-plugins-webresource-4.0.8
[4.0.8]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.0.8%0Datlassian-plugins-webresource-4.0.7
[4.0.7]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.0.7%0Datlassian-plugins-webresource-4.0.6
[4.0.6]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.0.6%0Datlassian-plugins-webresource-4.0.3
[4.0.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.0.3%0Datlassian-plugins-webresource-4.0.2
[4.0.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.0.2%0Datlassian-plugins-webresource-4.0.1
[4.0.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.0.1%0Datlassian-plugins-webresource-4.0.0
[4.0.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-4.0.0%0Datlassian-plugins-webresource-3.7.4

[3.7.4]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.7.4%0Datlassian-plugins-webresource-3.7.3
[3.7.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.7.3%0Datlassian-plugins-webresource-3.7.2
[3.7.2]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.7.2%0Datlassian-plugins-webresource-3.7.1
[3.7.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.7.1%0Datlassian-plugins-webresource-3.7.0
[3.7.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.7.0%0Datlassian-plugins-webresource-3.6.7

[3.6.7]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.6.7%0Datlassian-plugins-webresource-3.6.6
[3.6.6]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.6.6%0Datlassian-plugins-webresource-3.6.5
[3.6.5]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.6.5%0Datlassian-plugins-webresource-3.6.4
[3.6.4]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.6.4%0Datlassian-plugins-webresource-3.6.3
[3.6.3]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.6.3%0Datlassian-plugins-webresource-3.6.1
[3.6.1]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.6.1%0Datlassian-plugins-webresource-3.6.0
[3.6.0]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.6.0%0Datlassian-plugins-webresource-3.5.45

[3.5.45]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.5.45%0Datlassian-plugins-webresource-3.5.44
[3.5.44]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/compare/atlassian-plugins-webresource-3.5.44%0Datlassian-plugins-webresource-3.5.43

[PLUGWEB-778]: https://ecosystem.atlassian.net/browse/PLUGWEB-778
[PLUGWEB-777]: https://ecosystem.atlassian.net/browse/PLUGWEB-777
[PLUGWEB-776]: https://ecosystem.atlassian.net/browse/PLUGWEB-776
[PLUGWEB-775]: https://ecosystem.atlassian.net/browse/PLUGWEB-775
[PLUGWEB-688]: https://ecosystem.atlassian.net/browse/PLUGWEB-688
[PLUGWEB-675]: https://ecosystem.atlassian.net/browse/PLUGWEB-675
[PLUGWEB-673]: https://ecosystem.atlassian.net/browse/PLUGWEB-673
[PLUGWEB-672]: https://ecosystem.atlassian.net/browse/PLUGWEB-672
[PLUGWEB-671]: https://ecosystem.atlassian.net/browse/PLUGWEB-671
[PLUGWEB-670]: https://ecosystem.atlassian.net/browse/PLUGWEB-670
[PLUGWEB-664]: https://ecosystem.atlassian.net/browse/PLUGWEB-664
[PLUGWEB-662]: https://ecosystem.atlassian.net/browse/PLUGWEB-662
[PLUGWEB-661]: https://ecosystem.atlassian.net/browse/PLUGWEB-661
[PLUGWEB-663]: https://ecosystem.atlassian.net/browse/PLUGWEB-663
[PLUGWEB-660]: https://ecosystem.atlassian.net/browse/PLUGWEB-660
[PLUGWEB-649]: https://ecosystem.atlassian.net/browse/PLUGWEB-649
[PLUGWEB-652]: https://ecosystem.atlassian.net/browse/PLUGWEB-652
[PLUGWEB-648]: https://ecosystem.atlassian.net/browse/PLUGWEB-648
[PLUGWEB-644]: https://ecosystem.atlassian.net/browse/PLUGWEB-644
[PLUGWEB-641]: https://ecosystem.atlassian.net/browse/PLUGWEB-641
[PLUGWEB-640]: https://ecosystem.atlassian.net/browse/PLUGWEB-640
[PLUGWEB-632]: https://ecosystem.atlassian.net/browse/PLUGWEB-632
[PLUGWEB-631]: https://ecosystem.atlassian.net/browse/PLUGWEB-631
[PLUGWEB-627]: https://ecosystem.atlassian.net/browse/PLUGWEB-627
[PLUGWEB-626]: https://ecosystem.atlassian.net/browse/PLUGWEB-626
[PLUGWEB-625]: https://ecosystem.atlassian.net/browse/PLUGWEB-625
[PLUGWEB-624]: https://ecosystem.atlassian.net/browse/PLUGWEB-624
[PLUGWEB-623]: https://ecosystem.atlassian.net/browse/PLUGWEB-623
[PLUGWEB-622]: https://ecosystem.atlassian.net/browse/PLUGWEB-622
[PLUGWEB-621]: https://ecosystem.atlassian.net/browse/PLUGWEB-621
[PLUGWEB-618]: https://ecosystem.atlassian.net/browse/PLUGWEB-618
[PLUGWEB-613]: https://ecosystem.atlassian.net/browse/PLUGWEB-613
[PLUGWEB-612]: https://ecosystem.atlassian.net/browse/PLUGWEB-612
[PLUGWEB-611]: https://ecosystem.atlassian.net/browse/PLUGWEB-611
[PLUGWEB-610]: https://ecosystem.atlassian.net/browse/PLUGWEB-610
[PLUGWEB-608]: https://ecosystem.atlassian.net/browse/PLUGWEB-608
[PLUGWEB-607]: https://ecosystem.atlassian.net/browse/PLUGWEB-607
[PLUGWEB-568]: https://ecosystem.atlassian.net/browse/PLUGWEB-568
[PLUGWEB-567]: https://ecosystem.atlassian.net/browse/PLUGWEB-567
[PLUGWEB-564]: https://ecosystem.atlassian.net/browse/PLUGWEB-564
[PLUGWEB-562]: https://ecosystem.atlassian.net/browse/PLUGWEB-562
[PLUGWEB-560]: https://ecosystem.atlassian.net/browse/PLUGWEB-560
[PLUGWEB-559]: https://ecosystem.atlassian.net/browse/PLUGWEB-559
[PLUGWEB-555]: https://ecosystem.atlassian.net/browse/PLUGWEB-555
[PLUGWEB-554]: https://ecosystem.atlassian.net/browse/PLUGWEB-554
[PLUGWEB-550]: https://ecosystem.atlassian.net/browse/PLUGWEB-550
[PLUGWEB-547]: https://ecosystem.atlassian.net/browse/PLUGWEB-547
[PLUGWEB-543]: https://ecosystem.atlassian.net/browse/PLUGWEB-543
[PLUGWEB-528]: https://ecosystem.atlassian.net/browse/PLUGWEB-528
[PLUGWEB-513]: https://ecosystem.atlassian.net/browse/PLUGWEB-513
[PLUGWEB-502]: https://ecosystem.atlassian.net/browse/PLUGWEB-502
[PLUGWEB-456]: https://ecosystem.atlassian.net/browse/PLUGWEB-456
[PLUGWEB-454]: https://ecosystem.atlassian.net/browse/PLUGWEB-454
[PLUGWEB-453]: https://ecosystem.atlassian.net/browse/PLUGWEB-453
[PLUGWEB-447]: https://ecosystem.atlassian.net/browse/PLUGWEB-447
[PLUGWEB-423]: https://ecosystem.atlassian.net/browse/PLUGWEB-423
[PLUGWEB-422]: https://ecosystem.atlassian.net/browse/PLUGWEB-422
[PLUGWEB-290]: https://ecosystem.atlassian.net/browse/PLUGWEB-290
[PLUGWEB-88]: https://ecosystem.atlassian.net/browse/PLUGWEB-88

[SPFE-908]: https://ecosystem.atlassian.net/browse/SPFE-908
[SPFE-769]: https://ecosystem.atlassian.net/browse/SPFE-769
[SPFE-527]: https://ecosystem.atlassian.net/browse/SPFE-527
[SPFE-503]: https://ecosystem.atlassian.net/browse/SPFE-503
[SPFE-476]: https://ecosystem.atlassian.net/browse/SPFE-476
[SPFE-460]: https://ecosystem.atlassian.net/browse/SPFE-460
[SPFE-423]: https://ecosystem.atlassian.net/browse/SPFE-423

[BSP-3782]: https://bulldog.internal.atlassian.com/browse/BSP-3782
[BSP-3777]: https://bulldog.internal.atlassian.com/browse/BSP-3777
[BSP-3700]: https://bulldog.internal.atlassian.com/browse/BSP-3700
[BSP-3693]: https://bulldog.internal.atlassian.com/browse/BSP-3693
[BSP-3674]: https://bulldog.internal.atlassian.com/browse/BSP-3674
[BSP-3673]: https://bulldog.internal.atlassian.com/browse/BSP-3673

[BBSDEV-20883]: https://bulldog.internal.atlassian.com/browse/BBSDEV-20796

[CONFSRVDEV-23734]: https://jira.atlassian.com/browse/CONFSRVDEV-23734
[CONFSERVER-79610]: https://jira.atlassian.com/browse/CONFSERVER-79610

[DCA11Y-1211]: https://hello.jira.atlassian.cloud/browse/DCA11Y-1211

[JSEV-3707]: https://bulldog.internal.atlassian.com/browse/JSEV-3707
