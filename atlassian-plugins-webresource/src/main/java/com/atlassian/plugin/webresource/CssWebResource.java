package com.atlassian.plugin.webresource;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.lang.String.join;
import static java.util.Arrays.asList;
import static java.util.Optional.ofNullable;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

import static com.atlassian.plugin.webresource.impl.config.Config.CSS_EXTENSION;
import static com.atlassian.plugin.webresource.impl.config.Config.MEDIA_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.config.Config.WRM_BATCH_TYPE_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.config.Config.WRM_KEY_PARAM_NAME;

public class CssWebResource extends AbstractWebResourceFormatter {
    private static final String ATTRIBUTE_JOINER = " ";
    private static final String ATTRIBUTE_MEDIAL_ALL = " media=\"all\"";

    private static final List<String> STYLESHEET_PARAMETERS_HANDLED =
            asList("title", MEDIA_PARAM_NAME, "charset", WRM_KEY_PARAM_NAME, WRM_BATCH_TYPE_PARAM_NAME);
    private static final String STYLESHEET_TAG_BEGIN = "<link rel=\"stylesheet\" href=\"";
    private static final String STYLESHEET_TAG_EMPTY = "";
    private static final String STYLESHEET_TAG_END = ">\n";

    public CssWebResource() {}

    @Override
    public boolean matches(@Nullable final String name) {
        return ofNullable(name).filter(value -> name.endsWith(CSS_EXTENSION)).isPresent();
    }

    @Nonnull
    @Override
    public String formatResource(final String url, final Map<String, String> attributes) {
        if (isValid(attributes)) {
            final StringBuilder buffer = new StringBuilder()
                    .append(STYLESHEET_TAG_BEGIN)
                    .append(escapeHtml4(url))
                    .append('"');

            final Collection<String> tokens = getParametersAsAttributes(attributes);
            if (isNotEmpty(tokens)) {
                buffer.append(ATTRIBUTE_JOINER).append(join(ATTRIBUTE_JOINER, tokens));
            }

            final String mediaAttribute = ofNullable(attributes.get(MEDIA_PARAM_NAME))
                    .map(attribute -> STYLESHEET_TAG_EMPTY)
                    .orElse(ATTRIBUTE_MEDIAL_ALL);

            return buffer.append(mediaAttribute).append(STYLESHEET_TAG_END).toString();
        }
        return STYLESHEET_TAG_EMPTY;
    }

    @Override
    protected List<String> getAttributeParameters() {
        return STYLESHEET_PARAMETERS_HANDLED;
    }
}
