package com.atlassian.plugin.webresource.legacy;

import java.util.HashSet;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Creates a batch of all like-typed resources that are declared as "super-batch="true"" in their plugin
 * definitions.
 * <p>
 * The URL for batch resources is /download/superbatch/&lt;type&gt;/batch.&lt;type. The additional type part in the path
 * is simply there to make the number of path-parts identical with other resources, so relative URLs will still work
 * in CSS files.
 */
public class SuperBatchPluginResource extends AbstractPluginResource {
    private final SortedSet<String> excludedKeys;

    protected SuperBatchPluginResource() {
        super(new HashSet<>());
        this.excludedKeys = new TreeSet<>();
    }

    public void addBatchedWebResourceDescriptor(final String key) {
        completeKeys.add(key);
    }

    public void addExcludedContext(final String key) {
        excludedKeys.add(key);
    }

    /**
     * Used to ensure superbatch does not double-serve resources loaded in
     * an earlier {@link com.atlassian.webresource.api.assembler.resource.ResourcePhase}.
     * @return the contexts that have already been served and should be subtracted from the superbatch.
     */
    public Iterable<String> getExcludedContexts() {
        return excludedKeys;
    }

    public boolean isEmpty() {
        return completeKeys.isEmpty();
    }
}
