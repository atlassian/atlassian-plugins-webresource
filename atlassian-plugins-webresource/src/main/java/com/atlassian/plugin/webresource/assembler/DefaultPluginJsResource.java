package com.atlassian.plugin.webresource.assembler;

import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResourceParams;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

/**
 * Implementation of PluginUrlResource
 *
 * @since 3.0
 */
public class DefaultPluginJsResource extends DefaultPluginUrlResource<PluginJsResourceParams>
        implements PluginJsResource {

    public DefaultPluginJsResource(@Nonnull final ResourceUrl resourceUrl) {
        super(resourceUrl);
    }

    public DefaultPluginJsResource(@Nonnull final ResourceUrl resourceUrl, @Nonnull final ResourcePhase resourcePhase) {
        super(resourceUrl, resourcePhase);
    }

    @Override
    public PluginJsResourceParams getParams() {
        return new DefaultPluginJsResourceParams(
                resourceUrl.getParams(), resourceUrl.getKey(), resourceUrl.getBatchType());
    }
}
