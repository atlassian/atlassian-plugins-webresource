package com.atlassian.plugin.webresource.condition;

import com.atlassian.webresource.api.QueryParams;

/**
 * Implementation of DecoratingCompositeCondition that fits the new UrlReadingCondition interface
 *
 * @since v3.0
 */
class DecoratingOrCompositeCondition extends DecoratingCompositeCondition {
    @Override
    public boolean shouldDisplay(QueryParams params) {
        for (DecoratingCondition condition : conditions) {
            if (condition.shouldDisplay(params)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public DecoratingCondition invertCondition() {
        DecoratingAndCompositeCondition andCondition = new DecoratingAndCompositeCondition();
        for (DecoratingCondition condition : conditions) {
            andCondition.addCondition(condition.invertCondition());
        }
        return andCondition;
    }
}
