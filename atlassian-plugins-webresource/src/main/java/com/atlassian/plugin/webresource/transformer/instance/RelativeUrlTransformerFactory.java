package com.atlassian.plugin.webresource.transformer.instance;

import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.atlassian.util.concurrent.Lazy;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.plugin.servlet.AbstractFileServerServlet;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.api.transformer.TransformableResource;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

import static com.atlassian.plugin.servlet.AbstractFileServerServlet.PATH_SEPARATOR;

/**
 * Transformer that converts relative urls in CSS resources into absolute urls.
 * <p>
 * This will only transform relative urls. It will not transform urls that are:
 * <ul>
 * <li>absolute to the domain, eg /my-resource</li>
 * <li>absolute to another domain, eg http://blah.com/my-resource or https://blah.com/my-resource</li>
 * <li>data urls</li>
 * </ul>
 *
 * @since v3.1.0
 */
public class RelativeUrlTransformerFactory implements WebResourceTransformerFactory {
    /**
     * @deprecated since 4.2.1 without a replacement. Used only for deprecated relative to absolute resource URL conversions.
     */
    @Deprecated
    public static final String RELATIVE_URL_QUERY_KEY = "relative-url";

    private static final Pattern CSS_URL_PATTERN = Pattern.compile("url\\s*\\(\\s*+([\"'])?+(?!/|https?://|data:)");

    private final WebResourceIntegration webResourceIntegration;
    private final WebResourceUrlProvider webResourceUrlProvider;
    private final CdnResourceUrlTransformer cdnResourceUrlTransformer;
    private final boolean usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins;

    public RelativeUrlTransformerFactory(
            WebResourceIntegration webResourceIntegration,
            WebResourceUrlProvider webResourceUrlProvider,
            CdnResourceUrlTransformer cdnResourceUrlTransformer) {
        this.webResourceIntegration = webResourceIntegration;
        this.webResourceUrlProvider = webResourceUrlProvider;
        this.cdnResourceUrlTransformer = cdnResourceUrlTransformer;
        // It is not allowed to change `usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins` dynamically, once set
        // it can't be changed.
        this.usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins =
                webResourceIntegration.usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins();
    }

    @Override
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        return new RelativeUrlTransformerUrlBuilder();
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters parameters) {
        return new RelativeUrlTransformer(parameters);
    }

    @Override
    public Set<String> allUsedQueryParameters() {
        return Set.of(RELATIVE_URL_QUERY_KEY);
    }

    public static class CdnStrategyChangedException extends RuntimeException {
        public CdnStrategyChangedException(String message) {
            super(message);
        }
    }

    class RelativeUrlTransformerUrlBuilder implements TransformerUrlBuilder {
        @Override
        public void addToUrl(UrlBuilder urlBuilder) {
            if (webResourceIntegration.getCDNStrategy() != null
                    && webResourceIntegration.getCDNStrategy().supportsCdn()) {
                addRelativeUrlQueryKey(urlBuilder);
            }
        }

        private void addRelativeUrlQueryKey(UrlBuilder urlBuilder) {
            // The resources inside this CSS resource can be served via CDN
            // Note that we do not need to know whether the CSS resource being transformed by this resource
            // is tainted or not. It may be tainted, and not be served via CDN; yet the resources inside it
            // (eg icon fonts) are still able to be served via CDN.
            urlBuilder.addToQueryString(RELATIVE_URL_QUERY_KEY, String.valueOf(true));
        }
    }

    class RelativeUrlTransformer implements UrlReadingWebResourceTransformer {
        private final TransformerParameters parameters;

        RelativeUrlTransformer(final TransformerParameters parameters) {
            this.parameters = parameters;
        }

        @Override
        public DownloadableResource transform(TransformableResource transformableResource, QueryParams params) {
            boolean requestCdnUrl = Boolean.parseBoolean(params.get(RELATIVE_URL_QUERY_KEY));
            final Supplier<String> urlPrefix = createUrlPrefixRef(cdnResourceUrlTransformer, requestCdnUrl);
            Function<Matcher, CharSequence> replacer =
                    matcher -> new StringBuilder(matcher.group()).append(urlPrefix.get());

            return new CharSequenceDownloadableResource(transformableResource.nextResource()) {
                @Override
                protected CharSequence transform(CharSequence originalContent) {
                    final Matcher matcher = CSS_URL_PATTERN.matcher(originalContent);
                    final StringBuffer output = new StringBuffer();
                    while (matcher.find()) {
                        final CharSequence sequence = replacer.apply(matcher);
                        matcher.appendReplacement(output, "");
                        output.append(sequence);
                    }
                    matcher.appendTail(output);
                    return output;
                }
            };
        }

        private Supplier<String> createUrlPrefixRef(
                final CdnResourceUrlTransformer cdnResourceUrlTransformer, final boolean requestCdnUrl) {
            return Lazy.supplier(() -> {
                // This transform's only "variable" is the context path, and hence is not even variable since the
                // context path is already in the url path. Therefore we don't need to contribute anything additional
                // to the hash - this transform is a "constant" and other transformers / conditions don't apply to data
                // uri's or images.
                final String version = Config.getPluginVersionOrInstallTime(
                        webResourceIntegration.getPluginAccessor().getPlugin(parameters.getPluginKey()),
                        usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins);
                String resourcePrefix = PATH_SEPARATOR
                        + AbstractFileServerServlet.SERVLET_PATH
                        + PATH_SEPARATOR
                        + AbstractFileServerServlet.RESOURCE_URL_PREFIX;
                String localRelativeUrl = webResourceUrlProvider.getStaticResourcePrefix(version, UrlMode.RELATIVE)
                        + resourcePrefix + PATH_SEPARATOR + parameters.getPluginKey() + ":" + parameters.getModuleKey()
                        + PATH_SEPARATOR;
                if (!requestCdnUrl) {
                    return localRelativeUrl;
                }
                final CDNStrategy cdnStrategy = webResourceIntegration.getCDNStrategy();
                if (cdnStrategy == null || !cdnStrategy.supportsCdn()) {
                    // This will throw an exception if we have the CDN parameter in the URL, but
                    // webResourceIntegration.getCdnStrategy() returns null. This situation will occur
                    // if the cdn strategy is changed (eg via dark feature) between url-generation time and
                    // resource-fetch time. In this situation, it is correct to throw an exception - the alternative
                    // is to poison the CDN-side cache with a resource that has not been transformed to CDN.
                    throw new CdnStrategyChangedException(
                            "CDN strategy has changed between url generation time and resource fetch time");
                }
                final String resourceCdnPrefix = cdnResourceUrlTransformer.getResourceCdnPrefix(localRelativeUrl);
                // we do not want to prefix the resources with the scheme/hostname. And to be completely honest, we
                // don't want to prefix it with
                // anything at all. With the code below, accessing resources directly using .. dot notation will work,
                // changing the CDN scheme/hostname/port
                // will work, but changing the CDN path will not - the old resources will keep referring to the old path
                // until upgrade or addon change.
                return removeComponentsBeforePath(resourceCdnPrefix);
            });
        }
    }

    @VisibleForTesting
    static String removeComponentsBeforePath(final String resourceCdnPrefix) {
        final int hostnameStart = resourceCdnPrefix.indexOf("//") + 2;
        final int pathStart = resourceCdnPrefix.indexOf("/", hostnameStart);
        if (pathStart == -1) {
            return "";
        }
        return resourceCdnPrefix.substring(pathStart);
    }
}
