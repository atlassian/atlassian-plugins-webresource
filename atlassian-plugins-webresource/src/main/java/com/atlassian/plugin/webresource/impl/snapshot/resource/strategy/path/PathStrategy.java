package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.path;

public interface PathStrategy {
    String getPath();
}
