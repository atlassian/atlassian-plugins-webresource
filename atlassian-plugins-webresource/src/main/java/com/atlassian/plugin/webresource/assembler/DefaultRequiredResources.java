package com.atlassian.plugin.webresource.assembler;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.models.Requestable;
import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toSet;

import static com.atlassian.plugin.webresource.impl.config.Config.CONTEXT_PREFIX;
import static com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers.resolveExcluded;
import static com.atlassian.plugin.webresource.impl.support.Support.LOGGER;
import static com.atlassian.plugin.webresource.util.WebResourceKeyHelper.isWebResourceKey;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.defaultPhase;

class DefaultRequiredResources implements RequiredResources {
    private final RequestState requestState;

    DefaultRequiredResources(@Nonnull final RequestState requestState) {
        this.requestState =
                requireNonNull(requestState, "The request state object is mandatory to build a required resources.");
    }

    @Nonnull
    @Override
    @Deprecated
    public RequiredResources requireWebResource(@Nonnull final String completeKey) {
        return requireWebResource(defaultPhase(), completeKey);
    }

    @Nonnull
    @Override
    public RequiredResources requireWebResource(
            @Nonnull final ResourcePhase resourcePhase, @Nonnull final String completeKey) {
        if (!isWebResourceKey(completeKey)) {
            LOGGER.warn(
                    "requiring something that doesn't look like the web resource \"{}\", it will be ignored.",
                    completeKey);
            return this;
        }
        requestState.getRawRequest().include(resourcePhase, new WebResourceKey(completeKey));
        return this;
    }

    @Nonnull
    @Override
    @Deprecated
    public RequiredResources requireContext(@Nonnull final String context) {
        return requireContext(defaultPhase(), context);
    }

    @Nonnull
    @Override
    public RequiredResources excludeSuperbatch() {
        requestState.getSuperbatchConfiguration().setEnabled(false);

        return this;
    }

    @Nonnull
    @Override
    public RequiredResources requireSuperbatch(@Nonnull final ResourcePhase resourcePhase) {
        requestState.getSuperbatchConfiguration().setEnabled(true);
        requestState.getSuperbatchConfiguration().setResourcePhase(resourcePhase);

        return this;
    }

    @Nonnull
    @Override
    public RequiredResources requireContext(@Nonnull final ResourcePhase resourcePhase, @Nonnull final String context) {
        requestState.getRawRequest().include(resourcePhase, new WebResourceContextKey(context));
        return this;
    }

    @Nonnull
    @Override
    public RequiredResources exclude(@Nullable Set<String> excludeWebResources, @Nullable Set<String> excludeContexts) {
        final Set<Requestable> requestablesToExclude = new HashSet<>();

        ofNullable(excludeWebResources)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .filter(StringUtils::isNotBlank)
                .map(WebResourceKey::new)
                .forEach(requestablesToExclude::add);

        ofNullable(excludeContexts)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .filter(StringUtils::isNotBlank)
                .map(WebResourceContextKey::new)
                .forEach(requestablesToExclude::add);

        // todo PLUGWEB-631 change strategy to avoid the entire calculateBatches process.
        // todo PLUGWEB-631 need to match how the ResourceRequirer crawls and culls the dependency graph.
        {
            final Collection<String> unresolvedExcludedKeys =
                    requestablesToExclude.stream().map(Requestable::toLooseType).collect(toCollection(HashSet::new));

            final Collection<String> resolvedExcludedKeys = resolveExcluded(
                    requestState.getRequestCache(),
                    requestState.getGlobals().getUrlCache(),
                    requestState.getUrlStrategy(),
                    unresolvedExcludedKeys,
                    requestState.getExcluded());

            final Set<Requestable> resolvedExcludedRequestables = resolvedExcludedKeys.stream()
                    .map(key -> key.contains(CONTEXT_PREFIX) ? new WebResourceContextKey(key) : new WebResourceKey(key))
                    .collect(toSet());

            requestState.getRawRequest().clearExcluded();
            requestState.getRawRequest().exclude(resolvedExcludedRequestables);
        }

        return this;
    }

    @Nonnull
    @Override
    @Deprecated
    public RequiredResources requirePage(@Nonnull final String key) {
        return requirePage(defaultPhase(), key);
    }

    @Nonnull
    @Override
    public RequiredResources requirePage(@Nonnull final ResourcePhase resourcePhase, @Nonnull final String key) {
        final Collection<Requestable> includedResources =
                requestState.getSnapshot().getRootPage(key).getWebResource().getDependencies().stream()
                        .map(WebResourceKey::new)
                        .collect(toCollection(LinkedHashSet::new));
        requestState.getRawRequest().include(resourcePhase, includedResources);
        return this;
    }
}
