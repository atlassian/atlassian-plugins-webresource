package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.web.baseconditions.BaseCondition;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;

/**
 * A condition interface that wraps {@link com.atlassian.webresource.spi.condition.UrlReadingCondition}.
 *
 * @since v3.0
 */
public interface DecoratingCondition extends BaseCondition {
    /**
     * Called when constructing the URL as the hosting HTML page is being served. Can add parameters to the query
     * string and alter the resource hash.
     *
     * @param urlBuilder interface for contributing to the URL
     */
    void addToUrl(UrlBuilder urlBuilder, UrlBuildingStrategy urlBuilderStrategy);

    /**
     * Determine whether the web fragment should be displayed. This method should only read values from its config
     * and the query params.
     *
     * @param params query params
     * @return true if the user should see the fragment, false otherwise
     */
    boolean shouldDisplay(QueryParams params);

    /**
     * @return a version of this condition with inverted boolean logic.
     */
    DecoratingCondition invertCondition();
}
