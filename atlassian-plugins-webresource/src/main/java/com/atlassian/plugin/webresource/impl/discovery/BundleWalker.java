package com.atlassian.plugin.webresource.impl.discovery;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Predicate;

import com.google.common.base.Predicates;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.plugin.webresource.impl.support.Support;

import static com.atlassian.plugin.webresource.impl.config.Config.isContextKey;
import static com.atlassian.plugin.webresource.impl.config.Config.virtualContextKeyToWebResourceKey;

/**
 * Walks the dependency graph of {@link Bundle} objects and reduces the graph to an ordered list
 * of their keys, ensuring all upstream dependencies occur before the things depending on them.
 */
public class BundleWalker {
    /**
     * Makes it easier to see the places in our traversal algorithm where we
     * want to force our execution to follow a specific logic path.
     */
    private static final Predicate<Bundle> alwaysFailingDeepFilter = Predicates.alwaysFalse();

    private final Snapshot snapshot;

    /**
     * Keeps track of all keys discovered during a traversal. Discovery order is not preserved.
     * Used to quickly check whether a given key has been traversed before or not,
     * and whether it has already been marked "included".
     */
    private Map<String, Found.State> discovered;
    /**
     * Keeps track of all bundles in discovery order when traversing.
     * Items are deliberately not checked for uniqueness; it is valid for an item to appear
     * multiple times in this list. This allows us to determine two discrete orders:
     * 1) discovery order, determinable by filtering on key, then taking the first instance
     * 2) content inclusion order, determinable by filtering on "included" state, then filtering on key, then taking the first instance
     */
    private List<Found.Item> resolved;
    /**
     * Keeps track of current traversal path from an entrypoint and through the graph.
     * If a key exists here and is rediscovered, that indicates a cycle in the dependency graph.
     */
    private LinkedHashSet<String> stack;

    public BundleWalker(Snapshot snapshot) {
        this.snapshot = snapshot;
    }

    /**
     * Returns an ordered set of web-resource keys, based on the dependency graph
     * of the included keys, and whether each web-resource discovered meets the conditions
     * specified in the search.
     *
     * Keys will be omitted from the returned set if they appear in the excluded keys.
     *
     * Exclusions take precedence over included keys.
     *
     * In cases where the dependency graph does not dictate an order,
     * keys will appear in the order they were requested (i.e., the order they appear
     * in the included keys).
     *
     * @param included unique, ordered web-resource keys to include in the search.
     * @param excluded unique web-resource keys to omit from the result if found.
     * @return ordered keys of resolved Web Resources.
     */
    public Found find(
            final LinkedHashSet<String> included,
            final Set<String> excluded,
            final Predicate<Bundle> conditionsForExcluded,
            final boolean shouldExpandExcludedKeys,
            final Predicate<Bundle> deep,
            final Predicate<Bundle> deepFilter,
            final PredicateFailStrategy deepFilterFailStrategy,
            final Predicate<Bundle> shallowFilter) {
        if (included.isEmpty()) {
            return Found.EMPTY;
        }

        // Resolving excluded. The final exclusion list is sorted for predictability; useful for caches and URLs.
        final SortedSet<String> reducedExclusions = new TreeSet<>();
        final Set<String> excludedResolved;
        if (shouldExpandExcludedKeys) {
            Found found = new BundleWalker(snapshot)
                    .find(
                            new LinkedHashSet<>(excluded),
                            new LinkedHashSet<>(),
                            conditionsForExcluded,
                            false,
                            Predicates.alwaysTrue(),
                            conditionsForExcluded,
                            deepFilterFailStrategy,
                            Predicates.alwaysTrue());
            excludedResolved = new LinkedHashSet<>(found.getFound());
            reducedExclusions.addAll(found.getReducedInclusions());
        } else {
            excludedResolved = excluded;
            reducedExclusions.addAll(excluded);
        }

        // Initialise state for tracking already-discovered keys, keys in discovery order, and current graph traversal
        // stack
        resolved = new ArrayList<>();
        stack = new LinkedHashSet<>();
        final Map<String, Found.State> previouslyDiscovered = new HashMap<>();

        // Iterate through the inclusion list in order, keeping track of what keys have been processed.
        // The included list is a set subtype only to indicate its uniqueness, but we care more about the order here.
        final List<String> items = new ArrayList<>(included);
        final List<String> reducedInclusions = new ArrayList<>(included);
        for (int i = 0; i < items.size(); i++) {
            final String key = items.get(i);

            // before traversal, check whether this subtree has already been explored and included.
            if (Found.State.INCLUDED.equals(previouslyDiscovered.get(key))) {
                // we've found this subtree before, so its inclusion is redundant.
                reducedInclusions.remove(key);
                // not really much point in exploring this subtree again.
                continue;
            }

            // perform traversal
            discovered = new HashMap<>();
            findItChangesArguments(
                    key,
                    excludedResolved,
                    deep,
                    deepFilter,
                    deepFilterFailStrategy,
                    shallowFilter,
                    EnumSet.noneOf(TraversalOption.class));

            // after traversal, check whether any previously-traversed keys were discovered.
            for (int j = 0; j < i; j++) {
                final String previousKey = items.get(j);
                if (Found.State.INCLUDED.equals(discovered.get(previousKey))) {
                    reducedInclusions.remove(previousKey);
                }
            }

            // merge discoveries, being careful to avoid ignoring content that should be included.
            discovered.forEach((item, state) -> {
                // avoid altering state for any items that are explicitly included.
                if (Found.State.INCLUDED.equals(previouslyDiscovered.get(item))) {
                    return;
                }
                // add or update item state
                previouslyDiscovered.put(item, state);
            });
        }

        // Return the ordered list of found bundles
        return new Found(resolved, reducedInclusions, reducedExclusions);
    }

    /**
     * Resolve dependencies for {@link Bundle} while avoiding any cyclic dependencies, excluded dependencies, or
     * dependencies that should be ignored because they do not pass the configured predicates.
     */
    private void findItChangesArguments(
            final String key,
            final Set<String> excluded,
            final Predicate<Bundle> deep,
            final Predicate<Bundle> deepFilter,
            final PredicateFailStrategy deepFilterFailStrategy,
            final Predicate<Bundle> shallowFilter,
            final EnumSet<TraversalOption> traversalOptions) {
        // If bundle was already discovered and categorised
        if (discovered.containsKey(key)) {
            // If the bundle was skipped but we expect that the `skipped` or `ignored` value might change this
            // iteration, let it continue. Otherwise interrupt.
            final Found.State state = discovered.get(key);
            final boolean disableBundleContribution = traversalOptions.contains(TraversalOption.RECORD_FAILED);
            if (disableBundleContribution && !Found.State.INCLUDED.equals(state)
                    || !disableBundleContribution && Found.State.INCLUDED.equals(state)) {
                return;
            }
        }
        if (excluded.contains(key)) {
            // If we're looking for the complete potential graph, we need to continue traversing
            // but mark everything from here down as excluded.
            // this helps us determine the overlap between the current and previously requested items.
            if (deepFilterFailStrategy == PredicateFailStrategy.CONTINUE) {
                final EnumSet<TraversalOption> newOptions = EnumSet.copyOf(traversalOptions);
                newOptions.add(TraversalOption.RECORD_FAILED);
                final Bundle bundle = getBundle(key);
                if (bundle != null) {
                    stack.add(key);
                    advanceDeeperIfAllowed(
                            bundle,
                            excluded,
                            deep,
                            alwaysFailingDeepFilter,
                            deepFilterFailStrategy,
                            shallowFilter,
                            newOptions);
                    recordDiscoveredItem(key, Found.State.IGNORED);
                    stack.remove(key);
                }
                // If bundle not found it could be a virtual context (e.g., a web-resource masquerading as a context)
                else if (isContextKey(key)) {
                    findItChangesArguments(
                            virtualContextKeyToWebResourceKey(key),
                            excluded,
                            deep,
                            deepFilter,
                            deepFilterFailStrategy,
                            shallowFilter,
                            traversalOptions);
                }
            }
            return;
        }
        if (stack.contains(key)) {
            Support.LOGGER.warn(
                    "cyclic plugin resource dependency has been detected with: {}, stack trace: {}", key, stack);
            return;
        }
        stack.add(key);

        final Bundle bundle = getBundle(key);
        if (bundle != null) {
            if (deepFilter.test(bundle)) {
                advanceDeeperIfAllowed(
                        bundle, excluded, deep, deepFilter, deepFilterFailStrategy, shallowFilter, traversalOptions);

                if (shallowFilter.test(bundle)) {
                    recordDiscoveredItem(key, Found.State.INCLUDED);
                } else {
                    recordDiscoveredItem(key, Found.State.SKIPPED);
                }
            } else {
                /*
                 * Even though deepFilter returned false, we still need to traverse deep to collect bundles for bundle hash calculation.
                 * We expect all dependencies of current node on this branch to be added as skipped or ignored (to avoid condition execution and caching).
                 */

                if (deepFilterFailStrategy == PredicateFailStrategy.CONTINUE) {
                    final EnumSet<TraversalOption> newOptions = EnumSet.copyOf(traversalOptions);
                    newOptions.add(TraversalOption.RECORD_FAILED);
                    advanceDeeperIfAllowed(
                            bundle,
                            excluded,
                            deep,
                            alwaysFailingDeepFilter,
                            deepFilterFailStrategy,
                            shallowFilter,
                            newOptions);
                }

                final Found.State state;
                if (traversalOptions.contains(TraversalOption.RECORD_FAILED)) {
                    state = Found.State.IGNORED;
                } else {
                    state = Found.State.SKIPPED;
                }
                recordDiscoveredItem(key, state);
            }
        }
        // If bundle not found it could be a virtual context (e.g., a web-resource masquerading as a context)
        else if (isContextKey(key)) {
            findItChangesArguments(
                    virtualContextKeyToWebResourceKey(key),
                    excluded,
                    deep,
                    deepFilter,
                    deepFilterFailStrategy,
                    shallowFilter,
                    traversalOptions);
        }
        // If it's still not found, that's very odd...
        else if (Support.LOGGER.isDebugEnabled()) {
            Support.LOGGER.debug("Attempted to resolve bundle for {}, but it was null. stack trace: {}", key, stack);
        }

        stack.remove(key);
    }

    /**
     * Adds item to discovered or updates its {@link Found.State} value. Behaviour is specific to meet requirements of
     * bundle tree traversal. It only allows one-way change from "not included" to included.
     * If an item was declared as included, it won't be altered for subsequent calls other states.
     *
     * @param item to be stored
     * @param state the item's relevance in this traversal
     */
    private void recordDiscoveredItem(final String item, final Found.State state) {
        // avoid altering state for any items that are explicitly included.
        if (Found.State.INCLUDED.equals(discovered.get(item))) {
            return;
        }
        // add or update item state
        discovered.put(item, state);
        // add item entry
        resolved.add(new Found.Item(item, state));
    }

    private void advanceDeeperIfAllowed(
            final Bundle bundle,
            final Set<String> excluded,
            final Predicate<Bundle> deep,
            final Predicate<Bundle> deepFilter,
            final PredicateFailStrategy deepFilterFailStrategy,
            final Predicate<Bundle> shallowFilter,
            final EnumSet<TraversalOption> traversalOptions) {
        if (deep.test(bundle)) {
            List<String> bundleDependencies = bundle.getDependencies();
            for (String dependencyKey : bundleDependencies) {
                findItChangesArguments(
                        dependencyKey,
                        excluded,
                        deep,
                        deepFilter,
                        deepFilterFailStrategy,
                        shallowFilter,
                        traversalOptions);
            }
        }
    }

    protected Bundle getBundle(String key) {
        return snapshot.get(key);
    }
}
