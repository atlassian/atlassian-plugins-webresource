package com.atlassian.plugin.webresource.assembler;

import java.util.Map;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.assembler.resource.PluginCssResourceParams;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;

/**
 * @since v3.0
 */
class DefaultPluginCssResourceParams extends DefaultPluginUrlResourceParams implements PluginCssResourceParams {
    public DefaultPluginCssResourceParams(
            Map<String, String> params, String key, PluginUrlResource.BatchType batchType) {
        super(params, key, batchType);
    }

    @Override
    public String media() {
        return params.get(Config.MEDIA_PARAM_NAME);
    }
}
