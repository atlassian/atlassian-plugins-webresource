package com.atlassian.plugin.webresource.analytics.events;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.event.api.AsynchronousPreferred;
import com.atlassian.plugin.webresource.impl.support.http.ServingType;

/**
 * Analytic event to capture how effective the request serving cache is
 * <p>
 * Added during PLPI work to determine if improving or avoiding cold-cache scenarios is worth anything.
 *
 * @see <a href="https://ecosystem.atlassian.net/browse/PLUGWEB-560">PLUGWEB-560</a>
 * @since 5.2.0
 */
@AsynchronousPreferred
@EventName("wrm.caching.request.server")
public class RequestServingCacheEvent {
    private static final int EVENT_VERSION = 1;
    private final boolean cacheableRequest;
    private final boolean cacheHit;
    private final boolean cachingEnabled;
    private final boolean isSourceMap;
    private final ServingType servingType;
    private final long sizeInBytes;

    public RequestServingCacheEvent(
            final boolean cacheableRequest,
            final boolean cacheHit,
            final boolean cachingEnabled,
            final boolean isSourceMap,
            final ServingType servingType,
            final long sizeInBytes) {
        this.cacheableRequest = cacheableRequest;
        this.cacheHit = cacheHit;
        this.cachingEnabled = cachingEnabled;
        this.isSourceMap = isSourceMap;
        this.servingType = servingType;
        this.sizeInBytes = sizeInBytes;
    }

    public int getEventVersion() {
        return EVENT_VERSION;
    }

    public boolean getCacheableRequest() {
        return cacheableRequest;
    }

    public boolean getCacheHit() {
        return cacheHit;
    }

    public boolean getCachingEnabled() {
        return cachingEnabled;
    }

    public boolean getIsSourceMap() {
        return isSourceMap;
    }

    public String getServingType() {
        return servingType.toString();
    }

    public long getSizeInBytes() {
        return sizeInBytes;
    }
}
