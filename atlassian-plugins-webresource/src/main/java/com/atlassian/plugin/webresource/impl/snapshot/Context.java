package com.atlassian.plugin.webresource.impl.snapshot;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

/**
 * Context.
 *
 * @since v3.4.4
 */
public class Context extends Bundle {
    public Context(
            Snapshot snapshot,
            String key,
            List<String> dependencies,
            Date updatedAt,
            String version,
            boolean isTransformable) {
        super(snapshot, key, dependencies, updatedAt, version, isTransformable);
    }

    @Override
    // @todo: why did this only return resources for js modules..? is this a web-resource "context" or something else..?
    public LinkedHashMap<String, Resource> getResources(RequestCache cache) {
        LinkedHashMap<String, Resource> resources = new LinkedHashMap<>();
        return resources;
    }
}
