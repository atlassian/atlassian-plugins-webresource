package com.atlassian.plugin.webresource.impl.snapshot;

import javax.annotation.Nonnull;

/**
 * Holds info about a deprecated web resource module descriptor.
 *
 * @since v3.6.0
 */
public class Deprecation {
    private final String moduleCompleteKey;
    private String sinceVersion;
    private String removeInVersion;
    private String alternative;
    private String extraInfo;

    public Deprecation(@Nonnull String moduleCompleteKey) {
        this.moduleCompleteKey = moduleCompleteKey;
    }

    public String getSinceVersion() {
        return sinceVersion;
    }

    public void setSinceVersion(String sinceVersion) {
        this.sinceVersion = clean(sinceVersion);
    }

    public String getRemoveInVersion() {
        return removeInVersion;
    }

    public void setRemoveInVersion(String removeInVersion) {
        this.removeInVersion = clean(removeInVersion);
    }

    public String getAlternative() {
        return alternative;
    }

    public void setAlternative(String alternative) {
        this.alternative = clean(alternative);
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = clean(extraInfo);
    }

    public String buildLogMessage() {
        StringBuilder sb = new StringBuilder();
        sb.append("[DEPRECATED] \"");
        sb.append(moduleCompleteKey);
        sb.append("\" has been deprecated");
        if (getSinceVersion() != null) {
            sb.append(" since ");
            sb.append(getSinceVersion());
        }
        sb.append(" and will be removed in ");
        if (getRemoveInVersion() != null) {
            sb.append(getRemoveInVersion());
        } else {
            sb.append("a future release");
        }
        sb.append(".");
        if (getAlternative() != null) {
            sb.append(" Use ");
            sb.append(getAlternative());
            sb.append(" instead.");
        }
        if (getExtraInfo() != null) {
            sb.append(" ");
            sb.append(getExtraInfo());
        }
        return sb.toString();
    }

    private String clean(final String input) {
        final String val = input != null ? input.trim().replaceAll("\\s+", " ") : "";
        return val.length() > 0 ? val : null;
    }
}
