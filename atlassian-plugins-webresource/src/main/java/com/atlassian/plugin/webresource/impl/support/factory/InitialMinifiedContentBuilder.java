package com.atlassian.plugin.webresource.impl.support.factory;

import java.io.InputStream;
import java.util.Collection;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.impl.support.InitialContent;
import com.atlassian.sourcemap.ReadableSourceMap;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

import static com.atlassian.plugin.webresource.impl.support.Support.getSourceMap;

/**
 * Represents a class responsible for building a {@link InitialContent} based on the minified version of a {@link Resource}.
 *
 * @since 5.0.0
 */
class InitialMinifiedContentBuilder {
    /**
     * Pattern used to build an alternate version of minified extension.
     */
    private static final String ALTERNATE_MINIFIED_PATTERN_EXTENSION = ".min.%s";

    /**
     * Represents an empty string.
     */
    private static final String EMTPY = "";

    /**
     * Pattern used to build an extension.
     */
    private static final String EXTENSION_PATTERN = ".%s";

    /**
     * Pattern used to build a minified extension.
     */
    private static final String MINIFIED_PATTERN_EXTENSION = "-min.%s";

    /**
     * All possible file extensions that could be minified.
     */
    private static final Collection<String> MINIFIABLE_EXTENSIONS = asList("css", "js");

    private final Config config;
    private final Router router;
    private final InitialSourceContentBuilder initialSourceContentBuilder;

    private InputStream content;
    private String path;
    private ReadableSourceMap sourceMap;

    InitialMinifiedContentBuilder(
            @Nonnull final Globals globals, @Nonnull final InitialSourceContentBuilder initialSourceContentBuilder) {
        this.config = requireNonNull(
                globals.getConfig(),
                "The global configuration is mandatory for the creation of a minified content builder.");
        this.initialSourceContentBuilder =
                requireNonNull(initialSourceContentBuilder, "The initial content source builder is mandatory.");
        this.router = requireNonNull(
                globals.getRouter(),
                "The router information is mandatory for the creation of a minified content builder.");
    }

    /**
     * Build a minified path containing a suffix '.min' based on a resource path.
     *
     * @param path The resource path.
     * @return The minified path.
     */
    private static String getAlternatePath(final String path) {
        final int lastDot = path.lastIndexOf(".");
        return path.substring(0, lastDot) + ".min" + path.substring(lastDot);
    }

    /**
     * Build a minified path containing a suffix '-min' based on a resource path.
     *
     * @param path The resource path.
     * @return The minified path.
     */
    private static String getPath(final String path) {
        final int lastDot = path.lastIndexOf(".");
        return path.substring(0, lastDot) + "-min" + path.substring(lastDot);
    }

    @Nonnull
    InitialMinifiedContentBuilder withContent(@Nonnull final Resource resource) {
        if (isMinificationEnabled(resource)) {
            if (isNull(content)) {
                final String path = getPath(resource.getPath());
                content = resource.getStreamFor(path);
            }
            if (isNull(content)) {
                final String path = getAlternatePath(resource.getPath());
                content = resource.getStreamFor(path);
            }
        }
        return this;
    }

    @Nonnull
    InitialMinifiedContentBuilder withPath(@Nonnull final Resource resource) {
        if (isMinificationEnabled(resource)) {
            path = withContent(resource)
                    .build()
                    .getContent()
                    .map(minifiedStream -> getPath(resource.getPath()))
                    .orElseGet(() -> getAlternatePath(resource.getPath()));
        }
        return this;
    }

    @Nonnull
    InitialMinifiedContentBuilder withSourceMap(@Nonnull final Resource resource) {
        final String sourcePath = initialSourceContentBuilder
                .withContent(resource)
                .withPath(router, resource)
                .build()
                .getPath()
                .orElse(null);
        sourceMap = getSourceMap(path, resource, sourcePath);
        return this;
    }

    @Nonnull
    InitialContent build() {
        return new InitialMinifiedContent(content, path, sourceMap);
    }

    /**
     * Verifies whether the minification is enabled for a certain resource
     *
     * @param resource The resource to be verified.
     * @return <p>{@code true}: The resource is minifiable.</p>
     * <p>{@code false}: The resource not is minifiable.</p>
     */
    private boolean isMinificationEnabled(final Resource resource) {
        final boolean isMinificationEnabled =
                resource.getParent().isMinificationEnabled() && config.isMinificationEnabled();
        if (isMinificationEnabled) {
            return MINIFIABLE_EXTENSIONS.stream().anyMatch(minifiableExtension -> {
                final String path =
                        ofNullable(resource.getPath()).map(String::toLowerCase).orElse(EMTPY);
                final String extension = format(EXTENSION_PATTERN, minifiableExtension);
                final String minifiedExtension = format(MINIFIED_PATTERN_EXTENSION, minifiableExtension);
                final String alternateMinifiedExtension =
                        format(ALTERNATE_MINIFIED_PATTERN_EXTENSION, minifiableExtension);
                return path.endsWith(extension)
                        && !(path.endsWith(minifiedExtension) || path.endsWith(alternateMinifiedExtension));
            });
        }
        return isMinificationEnabled;
    }
}
