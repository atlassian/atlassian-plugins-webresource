package com.atlassian.plugin.webresource.transformer;

import java.util.Set;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerFactory;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.transformer.TwoPhaseResourceTransformer;

/**
 * Applies a fixed set of transformers to all resources by type.
 * These transformers are always applied after pluggable transformers.
 *
 * @since v3.1.0
 */
public interface StaticTransformers extends TwoPhaseResourceTransformer {
    // TODO refactor it into the interface of the transformer.
    //
    // It's a temporary way to get list of parameters used by static transformers, it should be refactored
    // into proper transformer interface.
    String[] PARAMETERS_USED = new String[] {RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY};

    /**
     * Contributes to the URL for all static transformers matching the given type
     *
     * @param locationType          type of resource (eg js, css, less)
     * @param transformerParameters parameters to pass to transformers
     * @param urlBuilder            url building to contribute to
     */
    void addToUrl(
            String locationType,
            TransformerParameters transformerParameters,
            UrlBuilder urlBuilder,
            UrlBuildingStrategy urlBuildingStrategy);

    /**
     * Performs transformation for all static transformers for the given type.
     *
     * @param content               content to transform
     * @param transformerParameters the {@link TransformerParameters} object
     * @param resourceLocation      resource location
     * @param queryParams           url querystring containing information for transformers
     * @param sourceUrl             url of original source file
     * @return a DownloadableResource representing the completed transformation
     */
    Content transform(
            Content content,
            TransformerParameters transformerParameters,
            ResourceLocation resourceLocation,
            QueryParams queryParams,
            String sourceUrl);

    /**
     * Get list of param keys used by this transformers.
     *
     * @since 3.5.27
     */
    public Set<String> getParamKeys();
}
