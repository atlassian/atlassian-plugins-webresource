package com.atlassian.plugin.webresource.filter.rewrite;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.tuckey.web.filters.urlrewrite.extend.RewriteMatch;
import org.tuckey.web.filters.urlrewrite.extend.RewriteRule;

import com.atlassian.plugin.servlet.ResourceDownloadUtils;

import static com.atlassian.plugin.webresource.ResourceUtils.STATIC_HASH;

/**
 * This Tuckey URL {@link RewriteRule} will normalize a URL and then make sure that it does
 * not attempt to use directory traversal to access the WEB-INF directory.
 * <p>
 * This {@link RewriteRule} will also add the required caching headers
 * {@link com.atlassian.plugin.servlet.ResourceDownloadUtils#addPublicCachingHeaders(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}.
 * <p>
 * Note: This implementation was lifted with some interpretation from JIRA (com.atlassian.jira.plugin.webresource.CachingResourceDownloadRewriteRule) ).
 */
public class ResourceDownloadRewriteRule extends RewriteRule {
    private static final Pattern NON_WEB_INF_RESOURCES_URI_PATTERN = Pattern.compile("^/s/(.*)/_/((?i)(?!WEB-INF).*)");

    @Override
    public RewriteMatch matches(HttpServletRequest request, HttpServletResponse response) {
        final String normalisedRequestUriPath;

        try {
            // Apply URI normalisation to the incoming request, to avoid path traversal into WEB-INF resources
            normalisedRequestUriPath = getNormalisedPathFrom(request);
        } catch (URISyntaxException invalidUriInRequest) {
            return null;
        }

        final Matcher nonWebInfResourcesPatternMatcher =
                NON_WEB_INF_RESOURCES_URI_PATTERN.matcher(normalisedRequestUriPath);

        if (!nonWebInfResourcesPatternMatcher.matches()) {
            return null;
        }

        final String rewrittenUriPath = "/" + nonWebInfResourcesPatternMatcher.group(2);

        final String rewrittenUrl = request.getContextPath() + rewrittenUriPath;

        return new RewriteMatch() {

            @Override
            public String getMatchingUrl() {
                return rewrittenUrl;
            }

            @Override
            public boolean execute(HttpServletRequest request, HttpServletResponse response)
                    throws ServletException, IOException {
                ResourceDownloadUtils.addPublicCachingHeaders(request, response);
                request.setAttribute(STATIC_HASH, nonWebInfResourcesPatternMatcher.group(1));
                request.getRequestDispatcher(rewrittenUriPath).forward(request, response);

                return true;
            }
        };
    }

    private String getNormalisedPathFrom(final HttpServletRequest request) throws URISyntaxException {
        return new URI(stripContextFrom(request)).normalize().toString();
    }

    private String stripContextFrom(final HttpServletRequest request) {
        return request.getRequestURI().substring(request.getContextPath().length());
    }
}
