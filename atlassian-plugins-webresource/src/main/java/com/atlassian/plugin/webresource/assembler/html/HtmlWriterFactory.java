package com.atlassian.plugin.webresource.assembler.html;

import java.io.Writer;
import java.util.EnumMap;
import java.util.Map;
import java.util.function.BiFunction;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static java.util.Objects.requireNonNull;

import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.DEFER;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INLINE;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.defaultPhase;

public class HtmlWriterFactory {
    private final Map<ResourcePhase, BiFunction<Writer, UrlMode, HtmlTagWriter>> htmlWriterBuilderByPhase;

    public HtmlWriterFactory(@Nonnull final Config config, @Nonnull final RequestState requestState) {
        requireNonNull(config, "The config is mandatory.");
        requireNonNull(requestState, "The requestState is mandatory.");
        htmlWriterBuilderByPhase = new EnumMap<>(ResourcePhase.class);
        htmlWriterBuilderByPhase.put(
                INLINE, (writer, urlMode) -> new InlineHtmlTagWriter(config, requestState, writer, urlMode));
        htmlWriterBuilderByPhase.put(
                REQUIRE, (writer, urlMode) -> new DefaultHtmlTagWriter(config, requestState, writer, urlMode));
        htmlWriterBuilderByPhase.put(DEFER, (writer, urlMode) -> new DeferHtmlTagWriter(requestState, writer, urlMode));
        htmlWriterBuilderByPhase.put(
                INTERACTION, (writer, urlMode) -> new InteractiveHtmlTagWriter(requestState, writer, urlMode));
    }

    @Nonnull
    public HtmlTagWriter get(
            @Nonnull final ResourcePhase resourcePhase, @Nonnull final Writer writer, @Nonnull final UrlMode urlMode) {
        requireNonNull(resourcePhase, "The resource phase is mandatory.");
        requireNonNull(writer, "The writer is mandatory.");
        requireNonNull(urlMode, "The urlMode is mandatory.");
        final BiFunction<Writer, UrlMode, HtmlTagWriter> defaultWriterBuilder =
                htmlWriterBuilderByPhase.get(defaultPhase());
        return htmlWriterBuilderByPhase
                .getOrDefault(resourcePhase, defaultWriterBuilder)
                .apply(writer, urlMode);
    }
}
