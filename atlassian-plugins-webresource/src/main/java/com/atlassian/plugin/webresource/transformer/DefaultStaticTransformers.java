package com.atlassian.plugin.webresource.transformer;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformableResource;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.transformer.TwoPhaseResourceTransformer;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

/**
 * {@link com.atlassian.plugin.webresource.transformer.StaticTransformers} implementation.
 *
 * @since v3.1.0
 */
public class DefaultStaticTransformers implements StaticTransformers {
    private final StaticTransformersSupplier staticTransformersSupplier;
    private Set<String> paramKeys = new HashSet<>();
    private Function<String, InputStream> loader;
    private ResourceLocation loaderResourceLocation;

    public DefaultStaticTransformers(StaticTransformersSupplier staticTransformersSupplier) {
        this.staticTransformersSupplier = staticTransformersSupplier;
    }

    @Override
    public void addToUrl(
            String locationType,
            TransformerParameters transformerParameters,
            UrlBuilder urlBuilder,
            UrlBuildingStrategy urlBuildingStrategy) {
        for (WebResourceTransformerFactory transformerFactory : transformersForType(locationType)) {
            urlBuildingStrategy.addToUrl(transformerFactory.makeUrlBuilder(transformerParameters), urlBuilder);
        }
    }

    @Override
    public void loadTwoPhaseProperties(ResourceLocation resourceLocation, Function<String, InputStream> loadFromFile) {
        this.loader = loadFromFile;
        this.loaderResourceLocation = resourceLocation;
    }

    @Override
    public boolean hasTwoPhaseProperties() {
        return false;
    }

    @Override
    public Content transform(
            Content content,
            TransformerParameters transformerParameters,
            ResourceLocation resourceLocation,
            QueryParams queryParams,
            String sourceUrl) {
        for (WebResourceTransformerFactory transformerFactory : transformersForLocation(resourceLocation)) {
            TransformableResource tr = new DefaultTransformableResource(
                    resourceLocation, ResourceServingHelpers.asDownloadableResource(content));
            UrlReadingWebResourceTransformer transformer =
                    transformerFactory.makeResourceTransformer(transformerParameters);
            if (loader != null
                    && resourceLocation.equals(loaderResourceLocation)
                    && transformer instanceof TwoPhaseResourceTransformer) {
                ((TwoPhaseResourceTransformer) transformer).loadTwoPhaseProperties(resourceLocation, loader);
            }
            content = ResourceServingHelpers.asContent(transformer.transform(tr, queryParams), null, true);
        }
        return content;
    }

    @Override
    public Set<String> getParamKeys() {
        return paramKeys;
    }

    private Iterable<WebResourceTransformerFactory> transformersForType(final String locationType) {
        return staticTransformersSupplier.get(locationType);
    }

    private Iterable<WebResourceTransformerFactory> transformersForLocation(final ResourceLocation location) {
        return staticTransformersSupplier.get(location);
    }
}
