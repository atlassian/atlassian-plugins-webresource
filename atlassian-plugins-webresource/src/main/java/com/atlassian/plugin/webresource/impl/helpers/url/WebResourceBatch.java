package com.atlassian.plugin.webresource.impl.helpers.url;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.ResourceKey;
import com.atlassian.plugin.webresource.impl.ResourceKeysSupplier;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

import static com.atlassian.plugin.webresource.impl.RequestCache.toResourceKeys;
import static com.atlassian.plugin.webresource.impl.config.Config.BATCH_TYPES;
import static com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers.resourcesOfType;

/**
 * Data structure representing Web Resource Batch, it is used in cache and shouldn't contain any references.
 */
public class WebResourceBatch {
    private final String key;
    private final List<SubBatch> subBatches;
    private final Map<String, ResourceKeysSupplier> standaloneResourceKeysByType = new HashMap<>();

    public WebResourceBatch(
            final String key, final List<SubBatch> subBatches, final List<Resource> standaloneResources) {
        this.key = key;
        this.subBatches = subBatches;
        // Instead of storing the reference to the resources storing its keys.
        for (String type : BATCH_TYPES) {
            final List<Resource> resourcesOfType = resourcesOfType(standaloneResources, type);
            final List<ResourceKey> resourceKeys = toResourceKeys(resourcesOfType);
            final ResourceKeysSupplier resourceKeysSupplier = new ResourceKeysSupplier(resourceKeys);
            standaloneResourceKeysByType.put(type, resourceKeysSupplier);
        }
    }

    public String getKey() {
        return key;
    }

    public List<Resource> getStandaloneResourcesOfType(final RequestCache requestCache, final String type) {
        return requestCache.getCachedResources(standaloneResourceKeysByType.get(type));
    }

    public List<SubBatch> getSubBatches() {
        return subBatches;
    }
}
