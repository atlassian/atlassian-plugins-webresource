package com.atlassian.plugin.webresource.condition;

import com.atlassian.webresource.api.QueryParams;

/**
 * Implementation of DecoratingCompositeCondition that fits the new UrlReadingCondition interface.
 *
 * @since v3.0
 */
public class DecoratingAndCompositeCondition extends DecoratingCompositeCondition {
    @Override
    public boolean shouldDisplay(QueryParams params) {
        for (DecoratingCondition condition : conditions) {
            if (!condition.shouldDisplay(params)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public DecoratingCondition invertCondition() {
        DecoratingOrCompositeCondition orCondition = new DecoratingOrCompositeCondition();
        for (DecoratingCondition condition : conditions) {
            orCondition.addCondition(condition.invertCondition());
        }
        return orCondition;
    }
}
