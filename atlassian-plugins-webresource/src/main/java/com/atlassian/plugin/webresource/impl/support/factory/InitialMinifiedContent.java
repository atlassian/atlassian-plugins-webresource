package com.atlassian.plugin.webresource.impl.support.factory;

import java.io.InputStream;
import java.io.OutputStream;
import javax.annotation.Nullable;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.InitialContent;
import com.atlassian.sourcemap.ReadableSourceMap;

import static com.atlassian.plugin.webresource.impl.support.Support.copy;

/**
 * <p>Represents a minified file content looked up from the file system.</p>
 * <p>E.g: A minified version of a raw javascript file.</p>
 *
 * @since 5.0.0
 */
class InitialMinifiedContent extends InitialContent {

    InitialMinifiedContent(
            @Nullable final InputStream content,
            @Nullable final String path,
            @Nullable final ReadableSourceMap sourceMap) {
        super(content, path, sourceMap);
    }

    @NonNull
    static InitialMinifiedContentBuilder builder(@NonNull final Globals globals) {
        return new InitialMinifiedContentBuilder(globals, InitialSourceContent.builder());
    }

    @NonNull
    @Override
    public Content toContent(@NonNull final Content originalContent) {
        return new Content() {
            @Override
            public ReadableSourceMap writeTo(
                    final OutputStream originalContentStream, final boolean isSourceMapEnabled) {
                getContent().ifPresent(content -> copy(content, originalContentStream));
                return getSourceMap().filter(sourceMap -> isSourceMapEnabled).orElse(null);
            }

            @Override
            public String getContentType() {
                return originalContent.getContentType();
            }

            @Override
            public boolean isTransformed() {
                return false;
            }
        };
    }
}
