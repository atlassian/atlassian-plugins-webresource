package com.atlassian.plugin.webresource.impl;

import java.util.Optional;
import javax.annotation.Nonnull;

import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static java.util.Objects.requireNonNull;

/**
 * Keeps the superbatch configuration in one place.
 */
public class SuperbatchConfiguration {
    private boolean enabled;
    private ResourcePhase resourcePhase;

    public SuperbatchConfiguration(boolean enabled) {
        this.enabled = enabled;
    }

    public ResourcePhase getResourcePhase() {
        return Optional.ofNullable(this.resourcePhase).orElseGet(ResourcePhase::defaultPhase);
    }

    public SuperbatchConfiguration setResourcePhase(@Nonnull ResourcePhase resourcePhase) {
        this.resourcePhase = requireNonNull(resourcePhase, "The resourcePhase is required");

        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public SuperbatchConfiguration setEnabled(boolean enabled) {
        this.enabled = enabled;

        return this;
    }
}
