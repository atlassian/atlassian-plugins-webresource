package com.atlassian.plugin.webresource.legacy;

import java.util.Set;

/**
 * Represents all web resources and contexts that have already been served
 * to the client and should not be served again.
 *
 * @since v3.1
 */
public class InclusionState {
    /**
     * Has the superbatch been included
     */
    public boolean superbatch;
    /**
     * Webresources that have been included in previous calls to includeResources, and all the individual resources
     * in included contexts
     */
    public final Set<String> webresources;
    /**
     * Webresource contexts that have been included in previous calls to includeResources
     */
    public final Set<String> contexts;
    /**
     * Webresources and contexts that were explicitly excluded from the request.
     * All items in the {@link #webresources} and {@link #contexts} sets should be
     * discoverable through the dependency graphs of these items.
     */
    public final Set<String> topLevel;

    public InclusionState(boolean superbatch, Set<String> webresources, Set<String> contexts, Set<String> topLevel) {
        this.superbatch = superbatch;
        this.webresources = webresources;
        this.contexts = contexts;
        this.topLevel = topLevel;
    }
}
