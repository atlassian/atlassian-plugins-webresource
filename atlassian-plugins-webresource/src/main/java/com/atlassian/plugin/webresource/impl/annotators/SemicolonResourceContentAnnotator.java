package com.atlassian.plugin.webresource.impl.annotators;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashSet;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

/**
 * Prepend and append a semicolon.
 * <p>
 * If a JavaScript file follows another that doesn't end with a semicolon, when it's batched, it's possible for the
 * semantics of the JavaScript to change. Consider the following:
 * <pre>// FILE ONE.js
 * console.log("Loading...")
 *
 * // FILE TWO.js
 * (function () {})()</pre>
 * When batched, it's turned into:
 * <pre>console.log("Loading...")(function () {})()</pre>
 * (Notice that the result of `console.log` is being *executed* with `function () {}` passed in as an argument. This
 * means you'll get an exception like this:
 * <pre>TypeError: undefined is not a function</pre>
 * This is a stateless implementation of the {@link com.atlassian.plugin.webresource.impl.annotators.ResourceContentAnnotator} that will write a semicolon before and
 * after each resource.
 *
 * @since 3.1.1
 */
public class SemicolonResourceContentAnnotator extends ResourceContentAnnotator {
    @Override
    public int beforeResourceInBatch(
            LinkedHashSet<String> requiredResources,
            Resource resource,
            final Map<String, String> params,
            OutputStream stream)
            throws IOException {
        stream.write(';');
        stream.write('\n');
        return 1;
    }

    @Override
    public void afterResourceInBatch(
            LinkedHashSet<String> requiredResources,
            Resource resource,
            final Map<String, String> params,
            OutputStream stream)
            throws IOException {
        stream.write(';');
    }

    @Override
    public int hashCode() {
        return getClass().getName().hashCode();
    }
}
