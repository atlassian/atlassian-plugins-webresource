package com.atlassian.plugin.webresource.impl.helpers.url;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.plugin.webresource.ResourceUrl;

import static java.util.Collections.emptyList;

public class Resolved {
    private final Set<String> excludedResolved;
    private final List<ResourceUrl> urls;

    public Resolved(final Set<String> excludedResolved) {
        this(emptyList(), excludedResolved);
    }

    public Resolved(final List<ResourceUrl> urls, final Set<String> excludedResolved) {
        this.urls = new ArrayList<>(urls);
        this.excludedResolved = new HashSet<>(excludedResolved);
    }

    public List<ResourceUrl> getUrls() {
        return urls;
    }

    public Set<String> getExcludedResolved() {
        return excludedResolved;
    }
}
