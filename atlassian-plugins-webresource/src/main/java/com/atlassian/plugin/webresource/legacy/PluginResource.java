package com.atlassian.plugin.webresource.legacy;

/**
 * Represents a plugin resource.
 * This object represents a page-time resource. It is used build a URL to the underlying resource,
 * but does not know how to get the contents of that resource itself.
 *
 * @since 2.2
 */
public interface PluginResource {}
