package com.atlassian.plugin.webresource.assembler;

import java.io.Writer;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.plugin.webresource.assembler.html.HtmlTagWriter;
import com.atlassian.plugin.webresource.assembler.html.HtmlWriterFactory;
import com.atlassian.plugin.webresource.assembler.html.PrefetchHtmlTagWriter;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResource;
import com.atlassian.webresource.api.assembler.WebResourceSet;

import static com.google.common.base.Predicates.instanceOf;
import static com.google.common.collect.Iterables.filter;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

/**
 * Implementation of WebResourceSet
 *
 * @since v3.0
 */
public class DefaultWebResourceSet implements WebResourceSet {
    private final Config config;
    private final HtmlWriterFactory htmlWriterFactory;
    private final RequestState requestState;
    private final Collection<WebResourceInformation> webResourceInformation;

    public DefaultWebResourceSet(
            @Nonnull final Config config,
            @Nonnull final RequestState requestState,
            @Nonnull final Collection<WebResourceInformation> webResourceInformation) {
        this.config = config;
        this.requestState = requestState;
        this.webResourceInformation = webResourceInformation;
        htmlWriterFactory = new HtmlWriterFactory(config, requestState);
    }

    @Nonnull
    @Override
    public Iterable<WebResource> getResources() {
        final Collection<WebResource> webResources = webResourceInformation.stream()
                .map(WebResourceInformation::getData)
                .flatMap(Collection::stream)
                .collect(toCollection(LinkedList::new));
        webResources.addAll(webResourceInformation.stream()
                .map(WebResourceInformation::getResourceUrls)
                .flatMap(Collection::stream)
                .map(ResourceUrls::getPluginUrlResource)
                .collect(toCollection(LinkedList::new)));
        return webResources;
    }

    @Nonnull
    @Override
    public <T extends WebResource> Iterable<T> getResources(@Nonnull final Class<T> clazz) {
        return (Iterable<T>) filter(getResources(), instanceOf(clazz));
    }

    @Nonnull
    public List<ResourceUrl> getResourceUrls() {
        return webResourceInformation.stream()
                .map(WebResourceInformation::getUrls)
                .flatMap(Collection::stream)
                .collect(toList());
    }

    @Override
    public void writeHtmlTags(@Nonnull final Writer writer, @Nonnull final UrlMode urlMode) {
        writeHtmlTags(writer, urlMode, webResource -> true);
    }

    @Override
    public void writeHtmlTags(
            @Nonnull final Writer writer,
            @Nonnull final UrlMode urlMode,
            @Nonnull final Predicate<WebResource> predicate) {
        writeHtmlTags(writer, urlMode, predicate, resourceUrl -> true);
    }

    @Override
    public void writePrefetchLinks(@Nonnull final Writer writer, @Nonnull final UrlMode urlMode) {
        /*
         * Note we don't write data providers when generating prefetch links, only URLs.
         * Prefetch links make the same <link> HTML regardless of resource type, so we only have a single formatter.
         */
        final HtmlTagWriter htmlTagWriter = new PrefetchHtmlTagWriter(config, requestState, writer, urlMode);
        webResourceInformation.stream()
                .map(WebResourceInformation::getResourceUrls)
                .forEach(htmlTagWriter::writeHtmlTag);
    }

    public void writeHtmlTags(
            @Nonnull final Writer writer,
            @Nonnull final UrlMode urlMode,
            @Nonnull final Predicate<WebResource> predicate,
            @Nonnull final Predicate<ResourceUrl> legacyPredicate) {
        // Write phased resources
        webResourceInformation.forEach(information -> {
            /*
             * Pick an appropriate HTML rendering strategy based on phase. If the phase is somehow invalid,
             * we pick up the strategy for the default phase.
             */
            final HtmlTagWriter htmlTagWriter = htmlWriterFactory.get(information.getResourcePhase(), writer, urlMode);
            htmlTagWriter.writeHtmlTag(information, predicate, legacyPredicate);
        });
    }
}
