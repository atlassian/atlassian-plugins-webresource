package com.atlassian.plugin.webresource.impl.helpers.url;

import java.util.LinkedHashSet;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Data structure representing Web Resource Batch Key.
 */
public class ContextBatchKey {
    private final List<String> included;
    private final LinkedHashSet<String> excluded;

    public ContextBatchKey(final List<String> included, final LinkedHashSet<String> excluded) {
        this.included = included;
        this.excluded = excluded;
    }

    public List<String> getIncluded() {
        return included;
    }

    public LinkedHashSet<String> getExcluded() {
        return excluded;
    }

    @Override
    public String toString() {
        final StringBuilder buffer = new StringBuilder();
        buffer.append('[').append(StringUtils.join(included, ", "));
        if (!excluded.isEmpty()) {
            buffer.append('-');
            buffer.append(StringUtils.join(excluded, ", "));
        }
        return buffer.append(']').toString();
    }
}
