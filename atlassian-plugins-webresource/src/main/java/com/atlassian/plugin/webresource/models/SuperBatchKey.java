package com.atlassian.plugin.webresource.models;

import static com.atlassian.plugin.webresource.impl.config.Config.SUPER_BATCH_CONTEXT_KEY;

/**
 * Represents the special "super-batch" web-resource context.
 *
 * @since 5.0.0
 */
public class SuperBatchKey extends WebResourceContextKey {

    private static final SuperBatchKey KEY = new SuperBatchKey();

    private SuperBatchKey() {
        super(SUPER_BATCH_CONTEXT_KEY);
    }

    public static SuperBatchKey getInstance() {
        return KEY;
    }

    @Override
    public String toString() {
        return "<wrc!" + SUPER_BATCH_CONTEXT_KEY + ">";
    }
}
