package com.atlassian.plugin.webresource.impl;

import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;

/**
 *
 */
public abstract class UrlBuildingStrategy {
    private UrlBuildingStrategy() {}

    public static UrlBuildingStrategy normal() {
        return new NormalStrategy();
    }

    public abstract void addToUrl(UrlReadingCondition condition, UrlBuilder urlBuilder);

    public abstract void addToUrl(TransformerUrlBuilder transformer, UrlBuilder urlBuilder);

    private static class NormalStrategy extends UrlBuildingStrategy {
        @Override
        public void addToUrl(UrlReadingCondition condition, UrlBuilder urlBuilder) {
            condition.addToUrl(urlBuilder);
        }

        @Override
        public void addToUrl(TransformerUrlBuilder transformer, UrlBuilder urlBuilder) {
            transformer.addToUrl(urlBuilder);
        }
    }
}
