package com.atlassian.plugin.webresource.legacy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

/**
 * Performs a calculation on many referenced contexts, and produces an set of intermingled batched-contexts and residual
 * (skipped) resources. Some of the input contexts may have been merged into cross-context batches.
 * The batches are constructed in such a way that no batch is dependent on another.
 * The output batches and resources may be intermingled so as to preserve the input order as much as possible.
 *
 * @since 2.9.0
 */
public class ContextBatchBuilder {
    private static final Logger log = LoggerFactory.getLogger(ContextBatchBuilder.class);

    private final ResourceDependencyResolver dependencyResolver;

    private final List<String> allIncludedResources = new ArrayList<>();
    private final Set<String> skippedResources = new HashSet<>();
    private final boolean isSuperBatchingEnabled;
    private final boolean includeDependenciesForFailedUrlReadingConditions;

    public ContextBatchBuilder(
            ResourceDependencyResolver dependencyResolver,
            boolean isSuperBatchingEnabled,
            boolean includeDependenciesForFailedUrlReadingConditions) {
        this.dependencyResolver = dependencyResolver;
        this.isSuperBatchingEnabled = isSuperBatchingEnabled;
        this.includeDependenciesForFailedUrlReadingConditions = includeDependenciesForFailedUrlReadingConditions;
    }

    public Iterable<PluginResource> buildBatched(
            final RequestCache requestCache,
            final UrlBuildingStrategy urlBuildingStrategy,
            final List<String> includedContexts,
            final InclusionState excludedThings) {
        final WebResourceKeysToContextBatches includedBatches = WebResourceKeysToContextBatches.create(
                requestCache,
                urlBuildingStrategy,
                includedContexts,
                dependencyResolver,
                isSuperBatchingEnabled,
                includeDependenciesForFailedUrlReadingConditions);

        skippedResources.addAll(includedBatches.getSkippedResources());

        if (includedBatches.isEmpty()) {
            return emptyList();
        }

        final WebResourceKeysToContextBatches excludedBatches;
        // Add contexts and top-level exclusions to the map,
        // allowing us to find and subtract overlapping entrypoints.
        {
            final Set<String> excludedContexts = new HashSet<>();
            if (excludedThings.contexts != null) {
                excludedContexts.addAll(excludedThings.contexts);
            }
            if (excludedThings.topLevel != null) {
                excludedContexts.addAll(excludedThings.topLevel);
            }
            excludedBatches = WebResourceKeysToContextBatches.create(
                    requestCache,
                    urlBuildingStrategy,
                    excludedContexts,
                    dependencyResolver,
                    isSuperBatchingEnabled,
                    includeDependenciesForFailedUrlReadingConditions);
        }

        // There are three levels to consider here. In order:
        // 1. Type (CSS/JS)
        // 2. Parameters (media, etc)
        // 3. Context
        final List<ContextBatch> batches = new ArrayList<>();

        // This working list will be reduced as each context is handled.
        final List<ContextBatch> batchesToProcess = new ArrayList<>(includedBatches.getContextBatches());

        final ContextBatchOperations contextBatchOperations = new ContextBatchOperations();

        while (!batchesToProcess.isEmpty()) {
            ContextBatch contextBatch = batchesToProcess.remove(0);
            Set<ContextBatch> alreadyProcessedBatches = new HashSet<>();
            alreadyProcessedBatches.add(contextBatch);

            // First, determine if any of the requested contexts can be merged
            Iterator<ModuleDescriptorStub> resourceIterator =
                    contextBatch.getResources().iterator();
            while (resourceIterator.hasNext()) {
                ModuleDescriptorStub contextResource = resourceIterator.next();
                String resourceKey = contextResource.getCompleteKey();
                // check for an overlap with the other batches (take into account only the batches not yet processed).
                List<ContextBatch> additionalContexts =
                        includedBatches.getAdditionalContextsForResourceKey(resourceKey, alreadyProcessedBatches);

                if (CollectionUtils.isNotEmpty(additionalContexts)) {
                    if (log.isDebugEnabled()) {
                        for (ContextBatch additional : additionalContexts) {
                            log.debug(
                                    "Context: {} shares a resource with {}: {}",
                                    contextBatch.getKey(),
                                    additional.getKey(),
                                    contextResource.getCompleteKey());
                        }
                    }

                    List<ContextBatch> contextsToMerge = new ArrayList<>(1 + additionalContexts.size());
                    contextsToMerge.add(contextBatch);
                    contextsToMerge.addAll(additionalContexts);
                    contextBatch = contextBatchOperations.merge(contextsToMerge);

                    // remove the merged batches from those to be processed
                    batchesToProcess.removeAll(additionalContexts);
                    alreadyProcessedBatches.addAll(additionalContexts);

                    // As a new overlapping context is merged, restart the resource iterator so we can check for new
                    // resources
                    // that may have been added via the merge.
                    resourceIterator = contextBatch.getResources().iterator();
                }
            }

            // Now that all potential merges have occurred, we can get a reference
            // to the full set of resources that would be served at this URL.
            final Set<String> contextResourceKeys = new HashSet<>();
            contextBatch.getResourceKeys().forEach(contextResourceKeys::add);

            // Next, determine whether the request might be empty after accounting for all
            // resource exclusions or previously-served resources.
            final SortedSet<String> subtractedWebResources = new TreeSet<>();

            // Check whether there is a perfect overlap between the webresource sets
            // to include in and exclude from this request.
            if (excludedThings.webresources != null && !excludedThings.webresources.isEmpty()) {
                // If 100% of inclusions are present in the exclusion list, we want to serve nothing.
                if (excludedThings.webresources.containsAll(contextResourceKeys)) {
                    log.debug("Context: {} is completely served by previous resource requests", contextBatch.getKey());
                    continue;
                }
            }

            // If any excluded web-resources are discoverable in the included contexts,
            // make sure we add them to the excluded batches list so that they're
            // subtracted from the context batch URL.
            if (excludedThings.topLevel != null) {
                excludedThings.topLevel.stream()
                        .filter(contextResourceKeys::contains)
                        .forEach(subtractedWebResources::add);
            }

            // We separate the search for excluded batches since we want to perform the subtraction
            // after all the merging has been done. If you do a subtraction then a merge you cannot
            // ensure (with ContextBatch as it is currently implemented) that the merge will not
            // bring back in previously excluded resources.
            if (!excludedBatches.isEmpty()) {
                resourceIterator = contextBatch.getResources().iterator();
                while (resourceIterator.hasNext()) {
                    ModuleDescriptorStub contextResource = resourceIterator.next();
                    String resourceKey = contextResource.getCompleteKey();

                    List<ContextBatch> excludeContexts = excludedBatches.getContextsForResourceKey(resourceKey);
                    if (!excludeContexts.isEmpty()) {
                        contextBatch = contextBatchOperations.subtract(contextBatch, excludeContexts);
                    }
                }

                skippedResources.removeAll(excludedBatches.getSkippedResources());

                // No need to subtract any web-resources discoverable through excluded contexts
                excludedBatches.resourceToContextBatches.keySet().forEach(subtractedWebResources::remove);
            }

            // Subtract already-included web resources. To do this, we pretend
            // that each web-resource is actually a context.
            final List<ContextBatch> subtractedWebResourcesAsContexts = subtractedWebResources.stream()
                    .map(key -> new ContextBatch(singletonList(key), emptyList(), emptyList(), isSuperBatchingEnabled))
                    .collect(toList());
            contextBatch = contextBatchOperations.subtract(contextBatch, subtractedWebResourcesAsContexts);

            // check that we still have resources in this batch - if not, the batch is not required.
            if (!contextBatch.isEmpty()) {
                Iterables.addAll(allIncludedResources, contextBatch.getResourceKeys());
                batches.add(contextBatch);
            } else if (log.isDebugEnabled()) {
                log.debug("The context batch {} contains no resources so will be dropped.", contextBatch.getKey());
            }
        }

        // Build the batch resources
        return concat(transform(batches, new Function<ContextBatch, Iterable<PluginResource>>() {
            public Iterable<PluginResource> apply(final ContextBatch batch) {
                return batch.buildPluginResources();
            }
        }));
    }

    public Iterable<String> getAllIncludedResources() {
        return allIncludedResources;
    }

    public Iterable<String> getSkippedResources() {
        return skippedResources;
    }

    private static class WebResourceKeysToContextBatches {
        private final Map<String, List<ContextBatch>> resourceToContextBatches;
        private final List<ContextBatch> knownBatches;
        private final Set<String> skippedResources;

        private WebResourceKeysToContextBatches(
                Map<String, List<ContextBatch>> resourceKeyToContext,
                List<ContextBatch> allBatches,
                Set<String> skippedResources) {
            this.resourceToContextBatches = resourceKeyToContext;
            this.knownBatches = allBatches;
            this.skippedResources = skippedResources;
        }

        /**
         * Create a Map of {@link WebResourceModuleDescriptor} key to the contexts that includes them. If a
         * {@link WebResourceModuleDescriptor} exists in multiple contexts then each context will be
         * referenced. The ContextBatches created at this point are pure - they do not take into account
         * overlaps or exclusions, they simply contain all the resources for their context name.
         *
         * @param contexts           the contexts to create a mapping for
         * @param dependencyResolver used to construct the identified contexts
         * @return a WebResourceToContextsMap containing the required mapping.
         */
        static WebResourceKeysToContextBatches create(
                RequestCache requestCache,
                UrlBuildingStrategy urlBuildingStrategy,
                final Iterable<String> contexts,
                final ResourceDependencyResolver dependencyResolver,
                boolean isSuperBatchingEnabled,
                boolean includeDependenciesForFailedUrlReadingConditions) {
            final Map<String, List<ContextBatch>> resourceKeyToContext = new HashMap<>();
            final List<ContextBatch> batches = new ArrayList<>();
            final Set<String> skippedResources = new HashSet<>();

            for (String context : contexts) {
                Iterable<ModuleDescriptorStub> dependencies = dependencyResolver.getDependenciesInContext(
                        requestCache,
                        urlBuildingStrategy,
                        context,
                        skippedResources,
                        includeDependenciesForFailedUrlReadingConditions);

                ContextBatch batch =
                        new ContextBatch(singletonList(context), null, dependencies, isSuperBatchingEnabled);
                for (ModuleDescriptorStub moduleDescriptor : dependencies) {
                    String key = moduleDescriptor.getCompleteKey();

                    if (!resourceKeyToContext.containsKey(key)) {
                        resourceKeyToContext.put(key, new ArrayList<>());
                    }

                    resourceKeyToContext.get(key).add(batch);

                    if (!batches.contains(batch)) {
                        batches.add(batch);
                    }
                }
            }

            return new WebResourceKeysToContextBatches(resourceKeyToContext, batches, skippedResources);
        }

        boolean isEmpty() {
            return knownBatches.isEmpty();
        }

        /**
         * @param key the resource key to find contexts for
         * @return all contexts the specified resource is included in. An empty List is returned if none.
         */
        List<ContextBatch> getContextsForResourceKey(String key) {
            return getAdditionalContextsForResourceKey(key, null);
        }

        /**
         * @param key           the resource key to be mapped to contexts
         * @param knownContexts the contexts we already know about (may be null)
         * @return a List of any additional contexts that the identified resource key can be found in. If there
         * are no additional contexts then an empty List is returned.
         */
        List<ContextBatch> getAdditionalContextsForResourceKey(String key, Collection<ContextBatch> knownContexts) {
            List<ContextBatch> allContexts = resourceToContextBatches.get(key);
            if (CollectionUtils.isEmpty(allContexts)) {
                return emptyList();
            }

            LinkedHashSet<ContextBatch> contexts = new LinkedHashSet<>(allContexts);
            if (CollectionUtils.isNotEmpty(knownContexts)) {
                contexts.removeAll(knownContexts);
            }

            return new ArrayList<>(contexts);
        }

        /**
         * @return all the ContextBatches referenced in this class.
         */
        List<ContextBatch> getContextBatches() {
            return new ArrayList<>(knownBatches);
        }

        /**
         * @return the conditional resources that could not be mapped
         */
        public Set<String> getSkippedResources() {
            return skippedResources;
        }
    }
}
