package com.atlassian.plugin.webresource.impl.discovery;

public enum TraversalOption {
    /**
     * Use this flag when a node fails a predicate test,
     * but the traversal through the full graph should continue.
     */
    RECORD_FAILED
}
