package com.atlassian.plugin.webresource.assembler.html;

import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.CssWebResource;
import com.atlassian.plugin.webresource.assembler.ResourceUrls;
import com.atlassian.webresource.api.UrlMode;

import static java.util.Objects.requireNonNull;

/**
 * Wrapper around {@link CssWebResource} responsible for the transformation of a {@link ResourceUrls} into a css import tag.
 *
 * @since 5.0.0
 */
final class CssTagFormatter implements HtmlTagFormatter {
    private final CssWebResource formatter;
    private final UrlMode urlMode;

    CssTagFormatter(@Nonnull final UrlMode urlMode) {
        formatter = new CssWebResource();
        this.urlMode = requireNonNull(urlMode, "The url mode is mandatory for the creation of CssTagFormatter.");
    }

    @Nonnull
    @Override
    public String format(@Nonnull final ResourceUrls resourceUrls) {
        requireNonNull(resourceUrls, "The resource urls are mandatory for the creation of the script tag");
        return formatter.formatResource(
                resourceUrls.getPluginUrlResource().getStaticUrl(urlMode),
                resourceUrls.getPluginUrlResource().getParams().all());
    }

    @Override
    public boolean matches(@Nonnull final String resourceName) {
        requireNonNull(resourceName, "The resource name is mandatory for the comparison.");
        return formatter.matches(resourceName);
    }
}
