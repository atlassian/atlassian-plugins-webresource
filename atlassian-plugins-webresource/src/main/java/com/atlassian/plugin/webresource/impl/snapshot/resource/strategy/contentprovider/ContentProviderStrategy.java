package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contentprovider;

import com.atlassian.plugin.webresource.impl.support.Content;

public interface ContentProviderStrategy {
    Content getContent();
}
