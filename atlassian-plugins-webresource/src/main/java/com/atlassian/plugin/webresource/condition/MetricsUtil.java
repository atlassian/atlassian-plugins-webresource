package com.atlassian.plugin.webresource.condition;

import javax.annotation.Nonnull;

import com.atlassian.util.profiling.Ticker;

import static com.atlassian.util.profiling.Metrics.metric;

public final class MetricsUtil {

    private static final String METRIC_KEY = "web.resource.condition";
    private static final String CONDITION_CLASS_NAME = "conditionClassName";

    private MetricsUtil() {}

    public static Ticker startWebConditionProfilingTimer(
            @Nonnull String pluginKey, @Nonnull String conditionClassName) {
        return metric(METRIC_KEY)
                .fromPluginKey(pluginKey)
                .tag(CONDITION_CLASS_NAME, conditionClassName)
                .withAnalytics()
                .startTimer();
    }
}
