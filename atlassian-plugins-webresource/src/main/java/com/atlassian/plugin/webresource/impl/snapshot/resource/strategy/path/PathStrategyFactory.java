package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.path;

import com.atlassian.plugin.elements.ResourceLocation;

public class PathStrategyFactory {
    public PathStrategyFactory() {}

    public PathStrategy createPath(ResourceLocation resourceLocation) {
        return new DefaultPathStrategy(resourceLocation);
    }

    public PathStrategy createRelativePath(ResourceLocation resourceLocation, String relativePath) {
        return new RelativePathStrategyDecorator(new DefaultPathStrategy(resourceLocation), relativePath);
    }
}
