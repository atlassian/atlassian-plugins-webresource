package com.atlassian.plugin.webresource.transformer;

import java.util.Set;

import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;

/**
 * Factory to create URL aware web resource transformers with Source Map.
 *
 * @since v3.3
 */
public interface ContentTransformerFactory {
    /**
     * Return the URL builder for this transform
     *
     * @param parameters transformer parameters
     * @return an builder that contributes parameters to the URL
     */
    TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters);

    /**
     * Return the transformer for this transform
     *
     * @param parameters transformer parameters
     * @return a transformer that reads values from the url and transforms a webresource
     */
    UrlReadingContentTransformer makeResourceTransformer(TransformerParameters parameters);

    /**
     * Return the complete set of query parameters that this transformer could ever use.
     * <p>
     * <b>NOTE:</b> Every query parameter is important to avoid cache positioning issues.
     * </p>
     * This should always return the same query parameters.
     * A null will disable all optimisations and only this default implementation is allowed to return it.
     *
     * @since 7.0.1
     */
    default Set<String> allUsedQueryParameters() {
        return null;
    }
}
