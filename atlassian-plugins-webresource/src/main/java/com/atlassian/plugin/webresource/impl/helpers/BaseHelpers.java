package com.atlassian.plugin.webresource.impl.helpers;

import java.util.Map;
import java.util.function.Predicate;

import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;

/**
 * Stateless helper functions providing basic support for resolving dependencies for resources.
 *
 * @since v3.3
 */
public class BaseHelpers {
    /**
     * Filter by web resource conditions evaluated against params.
     */
    public static Predicate<Bundle> isConditionsSatisfied(
            final RequestCache requestCache, final Map<String, String> params) {
        return bundle -> {
            CachedCondition condition = bundle.getCondition();
            return (condition == null) || (condition.evaluateSafely(requestCache, params));
        };
    }

    /**
     * Filter by web resource conditions evaluated immediately.
     */
    public static Predicate<Bundle> isConditionsSatisfied(
            final RequestCache requestCache, UrlBuildingStrategy urlBuilderStrategy) {
        return bundle -> {
            CachedCondition condition = bundle.getCondition();
            return (condition == null) || (condition.evaluateSafely(requestCache, urlBuilderStrategy));
        };
    }
}
