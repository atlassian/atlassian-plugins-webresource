package com.atlassian.plugin.webresource.models;

import javax.annotation.Nonnull;

/**
 * Represents a special web-resource key used by a developer to reference every single web-resource
 * and web-resource context a given page in the application should load when requested.
 */
public class RootPageKey extends WebResourceKey {
    public RootPageKey(@Nonnull String key) {
        super(key);
    }

    @Override
    public String toString() {
        return String.format("<root-page!%s>", getKey());
    }
}
