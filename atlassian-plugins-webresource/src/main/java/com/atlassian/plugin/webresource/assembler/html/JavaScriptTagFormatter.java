package com.atlassian.plugin.webresource.assembler.html;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.JavascriptWebResource;
import com.atlassian.plugin.webresource.assembler.ResourceUrls;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;

import static java.util.Objects.requireNonNull;

import static com.atlassian.plugin.webresource.impl.config.Config.DEFER_SCRIPT_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.config.Config.INITIAL_RENDERED_SCRIPT_PARAM_NAME;

/**
 * Class responsible for the transformation of a {@link ResourceUrls} into a javascript import tag.
 *
 * @since 5.0.0
 */
class JavaScriptTagFormatter implements HtmlTagFormatter {
    private final JavascriptWebResource formatter;
    private final boolean isDeferEnabled;
    private final UrlMode urlMode;

    JavaScriptTagFormatter(@Nonnull final UrlMode urlMode, final boolean deferEnabled) {
        formatter = new JavascriptWebResource();
        isDeferEnabled = deferEnabled;
        this.urlMode = requireNonNull(urlMode, "The url mode is mandatory for the creation of JavaScriptTagFormatter.");
    }

    @Nonnull
    @Override
    public String format(@Nonnull final ResourceUrls resourceUrls) {
        requireNonNull(resourceUrls, "The resource urls are mandatory for the creation of the script tag");

        final PluginUrlResource<?> urlResource = resourceUrls.getPluginUrlResource();
        final Map<String, String> attributes =
                new LinkedHashMap<>(urlResource.getParams().all());

        attributes.put(INITIAL_RENDERED_SCRIPT_PARAM_NAME, "");

        if (isDeferEnabled) {
            attributes.put(DEFER_SCRIPT_PARAM_NAME, "");
        }

        return formatter.formatResource(urlResource.getStaticUrl(urlMode), attributes);
    }

    @Override
    public boolean matches(@Nonnull final String resourceName) {
        requireNonNull(resourceName, "The resource name is mandatory for the comparison.");
        return formatter.matches(resourceName);
    }
}
