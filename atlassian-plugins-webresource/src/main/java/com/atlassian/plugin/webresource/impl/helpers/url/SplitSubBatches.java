package com.atlassian.plugin.webresource.impl.helpers.url;

import java.util.List;

import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

/**
 * Temporary data structure used during splitting batches into sub batches.
 */
class SplitSubBatches {
    private List<SubBatch> contextSubBatches;
    private List<Resource> contextStandaloneResources;
    private List<WebResourceBatch> legacyWebResources;
    private boolean isAdditionalSortingRequired;

    public List<SubBatch> getContextSubBatches() {
        return contextSubBatches;
    }

    public void setContextSubBatches(final List<SubBatch> contextSubBatches) {
        this.contextSubBatches = contextSubBatches;
    }

    public List<Resource> getContextStandaloneResources() {
        return contextStandaloneResources;
    }

    public void setContextStandaloneResources(final List<Resource> contextStandaloneResources) {
        this.contextStandaloneResources = contextStandaloneResources;
    }

    public List<WebResourceBatch> getLegacyWebResources() {
        return legacyWebResources;
    }

    public void setLegacyWebResources(final List<WebResourceBatch> legacyWebResources) {
        this.legacyWebResources = legacyWebResources;
    }

    public boolean isAdditionalSortingRequired() {
        return isAdditionalSortingRequired;
    }

    public void setAdditionalSortingRequired(final boolean additionalSortingRequired) {
        isAdditionalSortingRequired = additionalSortingRequired;
    }
}
