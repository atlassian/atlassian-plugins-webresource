package com.atlassian.plugin.webresource;

import java.util.List;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;

/**
 * An adapter between the current URL Generation code and previous URL Output code.
 *
 * @since v3.3
 */
public abstract class ResourceUrl {

    public abstract String getName();

    public abstract String getKey();

    public abstract String getType();

    public abstract String getUrl(boolean isAbsolute);

    public abstract Map<String, String> getParams();

    public abstract PluginUrlResource.BatchType getBatchType();

    public abstract List<Resource> getResources(RequestCache requestCache);
}
