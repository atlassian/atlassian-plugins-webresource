package com.atlassian.plugin.webresource.assembler.html;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.plugin.webresource.assembler.ResourceUrls;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.support.Content;

import static java.util.Collections.emptySet;
import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.text.StringEscapeUtils.escapeHtml4;

import static com.atlassian.plugin.webresource.impl.config.Config.INITIAL_RENDERED_SCRIPT_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.config.Config.WRM_BATCH_TYPE_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.config.Config.WRM_KEY_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.transform;
import static com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType.CONTEXT;

class JavaScriptSyncResourceWriter {
    private static final String CONTEXT_BATCH_TYPE = CONTEXT.name().toLowerCase();
    private static final String JAVASCRIPT_EXTENSION = "js";
    private static final String LINE_SEPARATOR = "\n";

    private final RequestCache cache;
    private final Globals globals;
    private final Writer writer;

    JavaScriptSyncResourceWriter(@Nonnull final RequestState requestState, @Nonnull final Writer writer) {
        requireNonNull(requestState, "The request state is mandatory for the creation of the SyncResourceWriter.");
        this.writer = requireNonNull(writer, "The writer is mandatory for the creation of the SyncResourceWriter.");
        cache = requireNonNull(requestState.getRequestCache());
        globals = requireNonNull(requestState.getGlobals());
    }

    void write(@Nonnull final Collection<ResourceUrls> resources) {
        requireNonNull(resources, "The resource set is mandatory for to generate the data to be written.");

        try {
            final Map<String, Set<String>> urlParams = new LinkedHashMap<>();
            urlParams.put(WRM_KEY_PARAM_NAME, new HashSet<>());
            urlParams.put(WRM_BATCH_TYPE_PARAM_NAME, new HashSet<>());

            final Collection<Content> contents = resources.stream()
                    .map(ResourceUrls::getResourceUrl)
                    .flatMap(urls -> {
                        urls.getParams().forEach((k, v) -> {
                            urlParams.putIfAbsent(k, new HashSet<>());
                            urlParams.get(k).add(v);
                        });
                        urlParams.get(WRM_KEY_PARAM_NAME).add(urls.getKey());
                        urlParams
                                .get(WRM_BATCH_TYPE_PARAM_NAME)
                                .add(urls.getBatchType().name().toLowerCase());
                        return urls.getResources(cache).stream();
                    })
                    .filter(resource -> JAVASCRIPT_EXTENSION.equals(resource.getNameType()))
                    .map(resource -> transform(
                            globals,
                            new LinkedHashSet<>(
                                    singletonList(resource.getParent().getKey())),
                            null,
                            resource,
                            resource.getParams(),
                            true))
                    .collect(toList());

            urlParams.put(INITIAL_RENDERED_SCRIPT_PARAM_NAME, emptySet());

            // note: if there are both `context` and `resource` batch types present, we will pretend everything is a
            // context.
            // this is equivalent to context batching behaviour changes introduced in PLUGWEB-546.
            if (urlParams.get(WRM_BATCH_TYPE_PARAM_NAME).contains(CONTEXT_BATCH_TYPE)) {
                urlParams.put(WRM_BATCH_TYPE_PARAM_NAME, new HashSet<>(singletonList(CONTEXT_BATCH_TYPE)));
            }

            if (isNotEmpty(contents)) {
                final OutputStream outputStream = new SyncOutputStream(writer);
                writer.write("<script");
                for (Map.Entry<String, Set<String>> entry : urlParams.entrySet()) {
                    String attr = entry.getKey();
                    String val = String.join(",", entry.getValue());
                    writer.write(" ");
                    writer.write(attr);
                    if (StringUtils.isNotBlank(val)) {
                        writer.write("=\"");
                        writer.write(escapeHtml4(val));
                        writer.write("\"");
                    }
                }
                writer.write(">");
                writer.write(LINE_SEPARATOR);
                for (final Content content : contents) {
                    content.writeTo(outputStream, false);
                    writer.write(LINE_SEPARATOR);
                }
                writer.write("</script>");
                writer.write(LINE_SEPARATOR);
            }
        } catch (final IOException exception) {
            throw new IllegalStateException(exception);
        }
    }

    private static final class SyncOutputStream extends OutputStream {
        private final Writer writer;

        private SyncOutputStream(final Writer writer) {
            this.writer = writer;
        }

        @Override
        public void write(final int data) throws IOException {
            writer.write(data);
        }
    }
}
