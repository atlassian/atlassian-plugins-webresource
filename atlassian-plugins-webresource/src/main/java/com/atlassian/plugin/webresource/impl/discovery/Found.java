package com.atlassian.plugin.webresource.impl.discovery;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;

/**
 * Represents the result of walking the dependency graph via {@link BundleWalker}, where every
 * item is in <a href="https://mathworld.wolfram.com/TopologicalSort.html">is in topological sort order</a>.
 * The items captured will vary based on how the graph walk was configured in
 * {@link BundleFinder} or {@link BundleWalker}.
 */
public class Found {
    public static final Found EMPTY = new Found(emptyList(), emptyList(), emptySet());

    private final List<Item> items;
    private final List<String> reducedInclusions;
    private final Set<String> reducedExclusions;

    public Found(final List<Item> items, final List<String> reducedInclusions, final Set<String> reducedExclusions) {
        this.items = items;
        this.reducedInclusions = ImmutableList.copyOf(reducedInclusions);
        this.reducedExclusions = ImmutableSet.copyOf(reducedExclusions);
    }

    /**
     * @return a list of {@link com.atlassian.plugin.webresource.models.Requestable} keys
     * whose content should be served to the client.
     */
    public List<String> getFound() {
        return items.stream()
                .filter(item -> State.INCLUDED.equals(item.getState()))
                .map(Item::getKey)
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * @return a list of {@link com.atlassian.plugin.webresource.models.Requestable} keys that failed a deep filter
     * test while traversing the graph (such as a {@link com.atlassian.webresource.spi.condition.UrlReadingCondition}
     * returning false) and should not be served to the client.
     *
     * Note that these are the exact items that failed a predicate test. Any child items would be ignored by the
     * graph walk. If you need a complete list that includes subtrees of skipped items, configure
     * {@link BundleFinder#onDeepFilterFail(PredicateFailStrategy)} to continue when a deep filter fails.
     */
    public List<String> getSkipped() {
        return items.stream()
                .filter(item -> State.SKIPPED.equals(item.getState()))
                .map(Item::getKey)
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * @return all {@link com.atlassian.plugin.webresource.models.Requestable} keys
     * discovered while performing this graph traversal. The list will include all skipped items,
     * and may include ignored subtrees depending on how {@link BundleFinder#onDeepFilterFail(PredicateFailStrategy)}
     * was configured.
     */
    public List<String> getAll() {
        return items.stream().map(Item::getKey).distinct().collect(Collectors.toList());
    }

    /**
     * @return the minimal set of graph entry points required to discover all the {@link #getFound()} items
     * after omitting subtrees discoverable via {@link #getReducedExclusions()}.
     * Can be used to minimise encoding cost when expressing the graph query and avoid redundant graph walks.
     */
    public List<String> getReducedInclusions() {
        return reducedInclusions;
    }

    /**
     * @return the minimal set of graph entry points required to avoid serving content the client should not be given.
     * Can be used to minimise encoding cost when expressing the graph query and avoid redundant graph walks.
     */
    public Set<String> getReducedExclusions() {
        return reducedExclusions;
    }

    enum State {
        /**
         * Items with this state contribute to response contents, URLs, caches, etc.
         */
        INCLUDED,
        /**
         * Used to mark items that fail filter predicates.
         * Items with this state DO NOT contribute to response contents but DO contribute to URLs, caches, etc.
         * Specifically, they will be used to determine what UrlReadingCondition should affect the URL when
         * batches and sub-batches are calculated.
         */
        SKIPPED,
        /**
         * Used to mark items that would have formed a part of this dependency graph, but were
         * omitted due to it or a parent being explicitly excluded (i.e., it has been served
         * in a previous request, or its parent's condition failed, etc).
         * Items with this state DO NOT contribute to response contents, caches, etc. but DO contribute to URLs.
         */
        IGNORED
    }

    /**
     * Tuple of a {@link com.atlassian.plugin.webresource.models.Requestable} key and its contribution {@link State}
     * to the response for a client request.
     */
    public static class Item {
        private final String key;
        private final State state;

        Item(final @Nonnull String key, @Nonnull final State state) {
            Objects.requireNonNull(key);
            this.key = key;
            this.state = state;
        }

        public String getKey() {
            return key;
        }

        private State getState() {
            return state;
        }
    }
}
