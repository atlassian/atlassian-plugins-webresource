package com.atlassian.plugin.webresource.assembler.html;

import java.io.Writer;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.assembler.ResourceUrls;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.UrlMode;

import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;

import static com.atlassian.plugin.webresource.BatchResourceContentsWebFormatter.insertBatchResourceContents;

/**
 * Writer wrapper responsible for writing HTML import tags with urls as prefetch.
 *
 * @since 5.0.0
 */
public class PrefetchHtmlTagWriter extends HtmlTagWriter {

    private final Config configuration;

    public PrefetchHtmlTagWriter(
            @Nonnull final Config configuration,
            @Nonnull final RequestState requestState,
            @Nonnull final Writer writer,
            @Nonnull final UrlMode urlMode) {
        super(requestState, writer, singletonList(PrefetchHtmlFormatter.getInstance(urlMode)));
        this.configuration = requireNonNull(
                configuration, "The configuration is mandatory for the creation of PrefetchHtmlTagWriter.");
    }

    @Nonnull
    @Override
    String generateHtmlTag(@Nonnull final ResourceUrls resource, @Nonnull final HtmlTagFormatter formatter) {
        if (configuration.isBatchContentTrackingEnabled()) {
            final String formattedResource = formatter.format(resource);
            // Insert the dependency information into the formatted resource.
            return insertBatchResourceContents(resource, formattedResource);
        }
        return formatter.format(resource);
    }
}
