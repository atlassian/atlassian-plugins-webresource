package com.atlassian.plugin.webresource.assembler.html;

import java.io.Writer;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.assembler.ResourceUrls;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.webresource.api.UrlMode;

import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

/**
 * Represents a HTML tag writer for "DEFER" phase which uses both {@link CssTagFormatter} and {@link JavaScriptTagFormatter}
 * for the generation of the HTML import tags.
 *
 * @since 5.0.0
 */
final class DeferHtmlTagWriter extends HtmlTagWriter {
    DeferHtmlTagWriter(
            @Nonnull final RequestState requestState, @Nonnull final Writer writer, @Nonnull final UrlMode urlMode) {
        super(
                requestState,
                writer,
                asList(
                        // Default way of loading CSS as in the
                        // com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE
                        new CssTagFormatter(urlMode), new JavaScriptTagFormatter(urlMode, true)));
    }

    @Nonnull
    @Override
    String generateHtmlTag(@Nonnull final ResourceUrls resourceUrls, @Nonnull final HtmlTagFormatter formatter) {
        requireNonNull(resourceUrls, "The resource urls are mandatory for the creation of the script tag.");
        requireNonNull(formatter, "The formatter is mandatory for generating the tags.");
        return formatter.format(resourceUrls);
    }
}
