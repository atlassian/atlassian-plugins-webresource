package com.atlassian.plugin.webresource;

import java.util.Map;
import java.util.Objects;

/**
 * Provides access to querystring params.
 *
 * @since v3.0
 */
public class QueryParams implements com.atlassian.webresource.api.QueryParams {
    private final Map<String, String> map;

    private QueryParams(Map<String, String> map) {
        this.map = map;
    }

    public static QueryParams of(Map<String, String> map) {
        return new QueryParams(map);
    }

    public String get(String key) {
        return map.get(key);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryParams that = (QueryParams) o;
        return Objects.equals(map, that.map);
    }

    @Override
    public int hashCode() {
        return Objects.hash(map);
    }

    @Override
    public String toString() {
        return "QueryParams{" + "map=" + map + '}';
    }
}
