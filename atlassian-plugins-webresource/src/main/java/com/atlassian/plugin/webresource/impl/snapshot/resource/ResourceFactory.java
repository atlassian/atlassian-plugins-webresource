package com.atlassian.plugin.webresource.impl.snapshot.resource;

import javax.annotation.Nonnull;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contentprovider.ContentProviderStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contentprovider.ContentProviderStrategyFactory;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contenttype.ContentTypeStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contenttype.ContentTypeStrategyFactory;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.path.PathStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.path.PathStrategyFactory;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream.StreamStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream.StreamStrategyFactory;

import static java.util.Objects.requireNonNull;

public class ResourceFactory {
    private final StreamStrategyFactory streamStrategyFactory;
    private final PathStrategyFactory pathStrategyFactory;
    private final ContentProviderStrategyFactory contentProviderStrategyFactory;
    private final ContentTypeStrategyFactory contentTypeStrategyFactory;

    public ResourceFactory(ServletContextFactory servletContextFactory, WebResourceIntegration webResourceIntegration) {
        this.streamStrategyFactory = new StreamStrategyFactory(servletContextFactory, webResourceIntegration);
        this.pathStrategyFactory = new PathStrategyFactory();
        this.contentProviderStrategyFactory = new ContentProviderStrategyFactory();
        this.contentTypeStrategyFactory = new ContentTypeStrategyFactory();
    }

    public Resource createResource(
            @Nonnull Bundle parent,
            @Nonnull ResourceLocation resourceLocation,
            @Nonnull String nameType,
            @Nonnull String locationType) {
        requireNonNull(parent);
        requireNonNull(resourceLocation);
        requireNonNull(nameType);
        requireNonNull(locationType);

        ContentTypeStrategy contentTypeStrategy =
                contentTypeStrategyFactory.createDefaultContentTypeStrategy(resourceLocation);
        StreamStrategy streamStrategy =
                streamStrategyFactory.createStandardModuleStreamStrategy(parent, resourceLocation);
        PathStrategy pathStrategy = pathStrategyFactory.createPath(resourceLocation);
        ContentProviderStrategy contentProviderStrategy =
                contentProviderStrategyFactory.createStreamContentProviderStrategy(
                        streamStrategy, contentTypeStrategy, pathStrategy);

        return new Resource(
                parent,
                resourceLocation,
                nameType,
                locationType,
                contentTypeStrategy,
                streamStrategy,
                pathStrategy,
                contentProviderStrategy);
    }

    /*
     * I have no idea why this path needs to be overridden.
     * The methods using this are deprecated.
     * TODO: Investigate for further refactor
     */
    @Deprecated
    public Resource createResourceWithRelativePath(
            @Nonnull Bundle parent,
            @Nonnull ResourceLocation resourceLocation,
            @Nonnull String nameType,
            @Nonnull String locationType,
            @Nonnull String overriddenPath) {
        requireNonNull(parent);
        requireNonNull(resourceLocation);
        requireNonNull(nameType);
        requireNonNull(locationType);
        requireNonNull(overriddenPath);

        ContentTypeStrategy contentTypeStrategy =
                contentTypeStrategyFactory.createDefaultContentTypeStrategy(resourceLocation);
        StreamStrategy streamStrategy =
                streamStrategyFactory.createStandardModuleStreamStrategy(parent, resourceLocation);
        PathStrategy pathStrategy = pathStrategyFactory.createRelativePath(resourceLocation, overriddenPath);

        ContentProviderStrategy contentProviderStrategy =
                contentProviderStrategyFactory.createStreamContentProviderStrategy(
                        streamStrategy, contentTypeStrategy, pathStrategy);

        return new Resource(
                parent,
                resourceLocation,
                nameType,
                locationType,
                contentTypeStrategy,
                streamStrategy,
                pathStrategy,
                contentProviderStrategy);
    }
}
