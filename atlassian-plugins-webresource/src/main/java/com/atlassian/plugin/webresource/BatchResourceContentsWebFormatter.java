package com.atlassian.plugin.webresource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.plugin.webresource.assembler.ResourceUrls;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * The WebResourceFormatter should take a PluginResource and add the BatchResourceContents dependency information to
 * the output as required.
 */
public final class BatchResourceContentsWebFormatter {
    /**
     * Match the opening of an HTML tag up to the first attribute or a tag with no attributes at
     * all (i.e. followed immediately with the &gt; symbol).
     */
    private static final Pattern TAG_NAME_PATTERN = Pattern.compile("(<\\w+)[\\s|>]");

    /**
     * Modify the supplied formattedResource to contain any necessary data attributes required to carry additional
     * information about the PluginResource being written.
     */
    public static String insertBatchResourceContents(ResourceUrls resource, String formattedResource) {
        Map<String, String> dependencyAttributes = createDependencyAttributes(resource);
        if (dependencyAttributes.isEmpty()) {
            return formattedResource;
        }

        List<String> nameValues = new ArrayList<>(dependencyAttributes.size());
        for (Entry<String, String> attr : dependencyAttributes.entrySet()) {
            String nameValuePair = escapeHtml4(attr.getKey()) + "=\"" + escapeHtml4(attr.getValue()) + '"';
            nameValues.add(nameValuePair);
        }

        String nameValuesStr = StringUtils.join(nameValues, ' ');

        Matcher matcher = TAG_NAME_PATTERN.matcher(formattedResource);
        if (matcher.find()) {
            StringBuilder builder = new StringBuilder(formattedResource);
            builder.insert(matcher.end(1), ' ' + nameValuesStr);
            return builder.toString();
        }

        return formattedResource;
    }

    private static Map<String, String> createDependencyAttributes(ResourceUrls resource) {
        Map<String, String> dependencyAttributes = new HashMap<>();

        if (resource.getResourceUrl() instanceof WebResourceSubBatchUrl) {
            WebResourceSubBatchUrl webResourceBatchUrl = (WebResourceSubBatchUrl) resource.getResourceUrl();
            Bundle bundle = webResourceBatchUrl.getBundle();
            List<String> dependencyValues = new ArrayList<>(1);
            String attributeValue = bundle.getKey() + '[' + bundle.getVersion() + ']';
            dependencyValues.add(attributeValue);

            if (!dependencyValues.isEmpty()) {
                dependencyAttributes.put(
                        "data-atlassian-webresource-contents", StringUtils.join(dependencyValues, ','));
            }
        }

        if (resource.getResourceUrl() instanceof ContextSubBatchResourceUrl) {
            ContextSubBatchResourceUrl contextBatchResourceUrl = (ContextSubBatchResourceUrl) resource.getResourceUrl();
            List<Bundle> batchedBundles = contextBatchResourceUrl.getBatchedBundles();
            List<String> dependencyValues = new ArrayList<>(batchedBundles.size());
            for (Bundle descriptor : batchedBundles) {
                String attributeValue = descriptor.getKey() + '[' + descriptor.getVersion() + ']';
                dependencyValues.add(attributeValue);
            }

            if (!dependencyValues.isEmpty()) {
                dependencyAttributes.put(
                        "data-atlassian-webresource-contents", StringUtils.join(dependencyValues, ','));
            }

            Iterator<String> contextsIterator =
                    contextBatchResourceUrl.getIncludedContexts().iterator();
            if (contextsIterator.hasNext()) {
                dependencyAttributes.put(
                        "data-atlassian-webresource-contexts", StringUtils.join(contextsIterator, ','));
            }

            Iterator<String> excludedContextsIterator =
                    contextBatchResourceUrl.getExcludedContexts().iterator();
            if (excludedContextsIterator.hasNext()) {
                dependencyAttributes.put(
                        "data-atlassian-webresource-excluded-contexts",
                        StringUtils.join(excludedContextsIterator, ','));
            }
        }

        return dependencyAttributes;
    }
}
