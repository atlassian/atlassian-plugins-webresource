package com.atlassian.plugin.webresource.graph;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.hash;
import static java.util.Objects.requireNonNull;

/**
 * Responsible for defining the dependency between a source and target vertex in a {@code DependencyGraph}.
 *
 * @param <T> The type of the vertices.
 */
public class DependencyEdge<T> {
    private T source;
    private T target;

    @Nonnull
    public T getSource() {
        return source;
    }

    void setSource(@Nonnull final T source) {
        this.source = requireNonNull(source, "The source vertex is mandatory.");
    }

    @Nonnull
    public T getTarget() {
        return target;
    }

    void setTarget(@Nonnull final T target) {
        this.target = requireNonNull(target, "The target vertex is mandatory.");
    }

    @Override
    public boolean equals(@Nullable final Object other) {
        if (other instanceof DependencyEdge) {
            final DependencyEdge otherDependency = (DependencyEdge) other;
            return otherDependency.source.equals(source) && otherDependency.target.equals(target);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return hash(source, target);
    }

    @Override
    public String toString() {
        return "(" + source + " : " + target + ')';
    }
}
