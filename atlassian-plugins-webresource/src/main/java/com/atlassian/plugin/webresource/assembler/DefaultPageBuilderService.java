package com.atlassian.plugin.webresource.assembler;

import java.util.Map;

import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;

/**
 * Default implementation of PageBuilderService
 *
 * @since v3.0
 */
public class DefaultPageBuilderService implements LegacyPageBuilderService, PageBuilderService {
    private static final String REQUEST_CACHE_REQUIRED_RESOURCES = "plugin.webresource.required.resources";

    private final WebResourceIntegration webResourceIntegration;
    private final WebResourceAssemblerFactory webResourceAssemblerFactory;

    public DefaultPageBuilderService(
            WebResourceIntegration webResourceIntegration, WebResourceAssemblerFactory webResourceAssemblerFactory) {
        this.webResourceIntegration = webResourceIntegration;
        this.webResourceAssemblerFactory = webResourceAssemblerFactory;
    }

    @Override
    public WebResourceAssembler assembler() {
        return cachedAssembler();
    }

    @Override
    public void seed(WebResourceAssembler assembler) {
        final Map<String, Object> cache = webResourceIntegration.getRequestCache();
        if (cache.containsKey(REQUEST_CACHE_REQUIRED_RESOURCES)) {
            throw new IllegalStateException("Request-local WebResourceAssembler has already been initialised");
        }
        cache.put(REQUEST_CACHE_REQUIRED_RESOURCES, assembler);
    }

    private WebResourceAssembler cachedAssembler() {
        final Map<String, Object> cache = webResourceIntegration.getRequestCache();
        @SuppressWarnings("unchecked")
        WebResourceAssembler assembler = (WebResourceAssembler) cache.get(REQUEST_CACHE_REQUIRED_RESOURCES);
        if (assembler == null) {
            assembler = webResourceAssemblerFactory.create().build();
            cache.put(REQUEST_CACHE_REQUIRED_RESOURCES, assembler);
        }
        return assembler;
    }

    @Override
    public void clearRequestLocal() {
        webResourceIntegration.getRequestCache().remove(REQUEST_CACHE_REQUIRED_RESOURCES);
    }
}
