package com.atlassian.plugin.webresource.models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.plugin.webresource.impl.config.Config.CONTEXT_PREFIX;

/**
 * Represents a web-resource context key.
 *
 * @since 5.0.0
 */
public class WebResourceContextKey extends Requestable {
    private static final Pattern EXTRANEOUS_CONTEXT_PATTERN = Pattern.compile("^(?:" + CONTEXT_PREFIX + "[:]?)+");
    private static final Logger LOGGER = LoggerFactory.getLogger(WebResourceContextKey.class);

    public WebResourceContextKey(@Nonnull final String key) {
        super(constructKey(key));
    }

    private static String constructKey(@Nonnull final String key) {
        final Matcher matcher = EXTRANEOUS_CONTEXT_PATTERN.matcher(key);
        if (matcher.find()) {
            LOGGER.debug("Provided key '{}' already contains '{}' prefix.", key, CONTEXT_PREFIX);
            matcher.reset();
            return matcher.replaceAll("");
        }
        return key;
    }

    @Override
    @Deprecated
    public String toLooseType() {
        return String.format("%s:%s", CONTEXT_PREFIX, getKey());
    }

    @Override
    public String toString() {
        return String.format("<wrc!%s>", getKey());
    }
}
