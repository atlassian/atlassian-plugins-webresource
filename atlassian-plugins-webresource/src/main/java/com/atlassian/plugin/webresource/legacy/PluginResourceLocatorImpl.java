package com.atlassian.plugin.webresource.legacy;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import io.atlassian.fugue.Option;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;

import static java.util.Collections.emptyList;

// import static com.atlassian.plugin.webresource.legacy.AbstractBatchResourceBuilder.skipBatch;

/**
 * Default implementation of {@link PluginResourceLocator}.
 *
 * @since 2.2
 */
public class PluginResourceLocatorImpl implements PluginResourceLocator {
    private static final Logger log = LoggerFactory.getLogger(PluginResourceLocatorImpl.class);

    private final PluginAccessor pluginAccessor;

    public PluginResourceLocatorImpl(WebResourceIntegration webResourceIntegration) {
        pluginAccessor = webResourceIntegration.getPluginAccessor();
    }

    @Override
    public List<PluginResource> getPluginResources(final String moduleCompleteKey) {
        if (moduleCompleteKey.contains(":")) {
            // If it's the web resource.
            Option<WebResourceModuleDescriptor> option = getDescriptor(moduleCompleteKey);
            if (option.isEmpty()) {
                return emptyList();
            }

            WebResourceModuleDescriptor wrmd = option.get();
            final Set<PluginResource> resources = new LinkedHashSet<>();

            for (final ResourceDescriptor resourceDescriptor : wrmd.getResourceDescriptors()) {
                resources.add(new BatchPluginResource(moduleCompleteKey, wrmd.getCompleteKey()));
            }
            return ImmutableList.copyOf(resources);
        } else {
            // If it's the module.
            List<PluginResource> resources = new ArrayList<>();
            resources.add(new BatchPluginResource(moduleCompleteKey, moduleCompleteKey));
            return resources;
        }
    }

    private Option<WebResourceModuleDescriptor> getDescriptor(String moduleCompleteKey) {
        final ModuleDescriptor<?> moduleDescriptor = pluginAccessor.getEnabledPluginModule(moduleCompleteKey);
        if ((moduleDescriptor == null) || !(moduleDescriptor instanceof WebResourceModuleDescriptor)) {
            log.error("Error loading resource \"{}\". Resource is not a Web Resource Module", moduleCompleteKey);
            return Option.none();
        }
        return Option.some((WebResourceModuleDescriptor) moduleDescriptor);
    }
}
