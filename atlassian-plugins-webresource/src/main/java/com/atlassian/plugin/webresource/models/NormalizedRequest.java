package com.atlassian.plugin.webresource.models;

/**
 * Represents a {@link RawRequest} whose {@link Requestable} members have been expanded and normalised
 * in to {@link WebResourceKey} values.
 */
public class NormalizedRequest {}
