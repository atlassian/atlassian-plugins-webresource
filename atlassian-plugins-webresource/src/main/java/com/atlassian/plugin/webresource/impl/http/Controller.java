package com.atlassian.plugin.webresource.impl.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.net.HttpHeaders;

import com.atlassian.plugin.cache.filecache.Cache;
import com.atlassian.plugin.cache.filecache.impl.PassThroughCache;
import com.atlassian.plugin.servlet.util.LastModifiedHandler;
import com.atlassian.plugin.webresource.analytics.events.InvalidBundleHashEvent;
import com.atlassian.plugin.webresource.analytics.events.RequestServingCacheEvent;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.discovery.BundleFinder;
import com.atlassian.plugin.webresource.impl.discovery.ResourceFinder;
import com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.impl.support.LineCountingProxyOutputStream;
import com.atlassian.plugin.webresource.impl.support.NullOutputStream;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.impl.support.http.BaseController;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;
import com.atlassian.plugin.webresource.impl.support.http.ServingType;
import com.atlassian.plugin.webresource.models.LooselyTypedRequestExpander;
import com.atlassian.plugin.webresource.models.RawRequest;
import com.atlassian.plugin.webresource.models.Requestable;
import com.atlassian.plugin.webresource.models.WebResourceKey;
import com.atlassian.sourcemap.ReadableSourceMap;
import com.atlassian.sourcemap.Util;
import com.atlassian.sourcemap.WritableSourceMap;

import static java.util.Collections.emptyMap;

import static com.atlassian.plugin.webresource.ResourceUtils.STATIC_HASH;
import static com.atlassian.plugin.webresource.ResourceUtils.shouldValidateRequest;
import static com.atlassian.plugin.webresource.analytics.EventFiringHelper.publishIfEventPublisherNonNull;
import static com.atlassian.plugin.webresource.analytics.EventFiringHelper.publishedThrottledEventIfEventPublisherNonNull;
import static com.atlassian.plugin.webresource.impl.config.Config.isBundleHashValidationEnabled;
import static com.atlassian.plugin.webresource.impl.helpers.BaseHelpers.isConditionsSatisfied;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.getResource;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.shouldBeIncludedInBatch;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.transform;
import static com.atlassian.plugin.webresource.impl.http.Router.sourceMapUrlToUrl;
import static com.atlassian.plugin.webresource.impl.support.Support.copy;
import static com.atlassian.sourcemap.ReadableSourceMap.toWritableSourceMap;
import static com.atlassian.sourcemap.Util.generateSourceMapComment;

/**
 * Handles HTTP requests.
 * <p>
 * Intended to be used with one instance per request to handle
 *
 * @since 3.3
 */
public class Controller extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(Controller.class);

    private final RequestCache requestCache;

    public Controller(Globals globals, Request request, Response response) {
        super(globals, request, response);
        this.requestCache = new RequestCache(globals);
    }

    /**
     * Serves single Resource.
     */
    public void serveResource(String completeKey, String resourceName, final ServingType servingType) {
        RawRequest raw = new RawRequest();
        raw.include(new WebResourceKey(completeKey));
        request.setRequestedResources(raw);
        request.setServingType(servingType);
        Resource resource = getResource(requestCache, completeKey, resourceName);
        serveResource(raw, resource, true, false);
    }

    /**
     * Serves Source Map for single Resource.
     */
    public void serveResourceSourceMap(String completeKey, String resourceName, final ServingType servingType) {
        RawRequest raw = new RawRequest();
        raw.include(new WebResourceKey(completeKey));
        request.setRequestedResources(raw);
        request.setServingType(servingType);
        Resource resource = getResource(requestCache, completeKey, resourceName);
        serveSourceMap(raw, resource);
    }

    /**
     * Serves Batch (all types of Batch - Web Resource Batch, Super Batch, Context Batch). Supports bundle hash verification.
     * If verification fails, empty resource list is returned that results in empty content response and 404 code.
     *
     * @param raw                       all included and excluded {@link Requestable}.
     * @param type                      type of Batch.
     * @param resolveDependencies       if dependencies should be resolved.
     * @param verifyBundleHash if bundle hash in the request url should be verified against what we currently have in the system
     */
    public void serveBatch(
            final RawRequest raw,
            final ServingType servingType,
            final String type,
            final boolean resolveDependencies,
            final boolean withLegacyConditions,
            final boolean isCachingEnabled,
            final boolean verifyBundleHash) {
        request.setRequestedResources(raw);
        request.setServingType(servingType);

        serveResources(
                raw,
                () -> {
                    if (shouldValidateBundleHash(verifyBundleHash) && !isBundleHashValid(raw)) {
                        publishIfEventPublisherNonNull(globals.getEventPublisher(), new InvalidBundleHashEvent());
                        return Collections.emptyList();
                    }

                    return getBatchResources(raw, type, resolveDependencies, withLegacyConditions);
                },
                isCachingEnabled);
    }

    private boolean shouldValidateBundleHash(boolean perRequestFlag) {
        return perRequestFlag && isBundleHashValidationEnabled() && shouldValidateRequest(request.getParams());
    }

    private boolean isBundleHashValid(final RawRequest raw) {
        final String bundleHashParam = request.getParams().get(STATIC_HASH);

        if (bundleHashParam == null) {
            // Unable to verify as STATIC_HASH param is not present, allow to continue
            return true;
        }

        final String hash = bundleHashParam.substring(bundleHashParam.lastIndexOf("/") + 1);

        return UrlGenerationHelpers.calculateBundleHash(raw, requestCache).equals(hash);
    }

    /**
     * Serves Source Map for Batch.
     *
     * @param raw                 all included and excluded {@link Requestable}.
     * @param type                type of Batch.
     * @param resolveDependencies if dependencies should be resolved.
     */
    public void serveBatchSourceMap(
            final RawRequest raw,
            final ServingType servingType,
            final String type,
            final boolean resolveDependencies,
            final boolean withLegacyConditions) {
        request.setRequestedResources(raw);
        request.setServingType(servingType);

        serveResourcesSourceMap(raw, () -> getBatchResources(raw, type, resolveDependencies, withLegacyConditions));
    }

    /**
     * Get Resources for Batch (all types of Batch - Web Resource Batch, Super Batch, Context Batch).
     *
     * @param raw                 all included and excluded {@link Requestable}.
     * @param type                type of Batch.
     * @param resolveDependencies if dependencies should be resolved.
     */
    protected List<Resource> getBatchResources(
            RawRequest raw, String type, boolean resolveDependencies, boolean withLegacyConditions) {
        LooselyTypedRequestExpander resources = new LooselyTypedRequestExpander(raw);
        // Exclude sync resources
        LinkedHashSet<String> included = new LinkedHashSet<>(resources.getIncluded());
        LinkedHashSet<String> excludedAndSync = new LinkedHashSet<>(resources.getExcluded());
        Bundle syncContext = requestCache.getSnapshot().get(Config.SYNCBATCH_KEY);
        if (null != syncContext) {
            excludedAndSync.addAll(syncContext.getDependencies());
        }

        final BundleFinder bundles = new BundleFinder(requestCache.getSnapshot())
                .included(included)
                .excluded(excludedAndSync, isConditionsSatisfied(requestCache, request.getParams()))
                .deep(resolveDependencies)
                .deepFilter(isConditionsSatisfied(requestCache, request.getParams()));
        return new ResourceFinder(bundles, requestCache)
                .filter(shouldBeIncludedInBatch(type, request.getParams()))
                .end();
    }

    /**
     * Serves Resource relative to Batch.
     *
     * @param raw                 all included and excluded {@link Requestable}.
     * @param resourceName        name attribute of &lt;resource&gt; to be served from within a requested web-resource.
     * @param resolveDependencies if dependencies should be resolved.
     */
    public void serveResourceRelativeToBatch(
            RawRequest raw,
            String resourceName,
            final ServingType servingType,
            boolean resolveDependencies,
            boolean withLegacyConditions) {
        request.setRelativeResourceName(resourceName);
        request.setRequestedResources(raw);
        request.setServingType(servingType);
        Resource resourceRelativeToBatch =
                getResourceRelativeToBatch(raw, resourceName, resolveDependencies, withLegacyConditions);
        serveResource(raw, resourceRelativeToBatch, true, false);
    }

    /**
     * Serves Source Map for Resource relative to Batch.
     */
    public void serveResourceRelativeToBatchSourceMap(
            RawRequest raw,
            String resourceName,
            final ServingType servingType,
            boolean resolveDependencies,
            boolean withLegacyConditions) {
        request.setRelativeResourceName(resourceName);
        request.setRequestedResources(raw);
        request.setServingType(servingType);
        Resource resourceRelativeToBatch =
                getResourceRelativeToBatch(raw, resourceName, resolveDependencies, withLegacyConditions);
        serveSourceMap(raw, resourceRelativeToBatch);
    }

    /**
     * Get Resource relative to Batch.
     */
    protected Resource getResourceRelativeToBatch(
            RawRequest raw, String resourceName, boolean resolveDependencies, boolean withLegacyConditions) {
        LooselyTypedRequestExpander resources = new LooselyTypedRequestExpander(raw);
        List<String> bundles = new BundleFinder(requestCache.getSnapshot())
                .included(resources.getIncluded())
                .excluded(resources.getExcluded(), isConditionsSatisfied(requestCache, request.getParams()))
                .deep(resolveDependencies)
                .deepFilter(isConditionsSatisfied(requestCache, request.getParams()))
                .end();
        return getResource(requestCache, bundles, resourceName);
    }

    /**
     * Serves Source Code of the Resource.
     */
    public void serveSource(final String completeKey, String resourceName, final ServingType servingType) {
        final boolean IS_CACHING_ENABLED = false;

        RawRequest raw = new RawRequest();
        raw.include(new WebResourceKey(completeKey));
        request.setRequestedResources(raw);
        request.setServingType(servingType);

        if (Resource.isPrebuiltSourceName(resourceName)) {
            resourceName = Resource.getResourceNameFromPrebuiltSourceName(resourceName);
        }

        final Resource resource = getResource(requestCache, completeKey, resourceName);
        if (handleNotFoundRedirectAndNotModified(resource)) {
            return;
        }

        sendCached(
                new ContentImpl(resource.getContentType(), false) {
                    @Override
                    public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                        String prebuildSourceName = Resource.getPrebuiltSourcePath(resource.getLocation());
                        InputStream sourceStream = resource.getStreamFor(prebuildSourceName);
                        if (sourceStream != null) {
                            copy(sourceStream, out);
                        } else {
                            resource.getContent().writeTo(out, isSourceMapEnabled);
                        }
                        return null;
                    }
                },
                resource.getParams(),
                IS_CACHING_ENABLED);
    }

    /**
     * Causes the content inside resource to be served in response.
     *
     * @param raw      all included and excluded {@link Requestable}.
     * @param resource the tangible {@link Resource} whose content should be served and should exist somewhere in raw.
     */
    protected void serveResource(RawRequest raw, Resource resource) {
        final boolean IS_CACHING_ENABLED = true;
        final boolean APPLY_ANNOTATORS = true;

        if (handleNotFoundRedirectAndNotModified(resource)) {
            return;
        }

        Content content = transform(
                globals, requiredResources(raw), request.getUrl(), resource, request.getParams(), APPLY_ANNOTATORS);
        sendCached(content, resource.getParams(), IS_CACHING_ENABLED);
    }

    protected void serveResource(
            RawRequest raw, Resource resource, boolean applyTransformations, boolean isCachingEnabled) {
        if (handleNotFoundRedirectAndNotModified(resource)) {
            return;
        }
        Content content = applyTransformations
                ? transform(globals, requiredResources(raw), request.getUrl(), resource, request.getParams(), true)
                : resource.getContent();
        sendCached(content, resource.getParams(), isCachingEnabled);
    }

    /**
     * Causes all content inside every {@link Resource} provided by resources to be served in response.
     *
     * @param raw       all included and excluded {@link Requestable}.
     * @param resources an ordered list of {@link Resource} whose content should be served.
     *                  Each should be reachable via one of the {@link Requestable} referenced in raw.
     */
    protected void serveResources(RawRequest raw, Supplier<Collection<Resource>> resources, boolean isCachingEnabled) {
        LinkedHashSet<String> requiredResources = requiredResources(raw);
        if (log.isDebugEnabled()) {
            log.debug(
                    "Serving requiredResources {} and resources with names {}",
                    String.join("|", requiredResources),
                    resources.get().stream().map(Resource::getFullName).collect(Collectors.joining("|")));
        }
        Content content = transform(
                globals, requiredResources, request.getUrl(), request.getType(), resources, request.getParams());
        sendCached(content, emptyMap(), isCachingEnabled);
    }

    /**
     * Serves Source Map.
     */
    protected void serveSourceMap(RawRequest raw, Resource resource) {
        final boolean IS_CACHING_ENABLED = false;
        final boolean APPLY_ANNOTATORS = true;

        if (handleNotFoundRedirectAndNotModified(resource)) {
            return;
        }

        Content content = transform(
                globals,
                requiredResources(raw),
                sourceMapUrlToUrl(request.getUrl()),
                resource,
                request.getParams(),
                APPLY_ANNOTATORS);
        sendCached(content, resource.getParams(), IS_CACHING_ENABLED);
    }

    /**
     * Serves Source Map for Batch of Resources.
     */
    private void serveResourcesSourceMap(RawRequest raw, Supplier<Collection<Resource>> resources) {
        final boolean IS_CACHING_ENABLED = true;

        String resourcePath = sourceMapUrlToUrl(request.getPath());
        String type = Request.getType(resourcePath);
        Content content = transform(
                globals,
                requiredResources(raw),
                sourceMapUrlToUrl(request.getUrl()),
                type,
                resources,
                request.getParams());
        sendCached(content, emptyMap(), IS_CACHING_ENABLED);
    }

    /**
     * these values are *only* used by ModuleAnnotator#after,
     * and only if the module being written happens to be one explicitly required by the user. It's meant to match
     * the call in ModuleAnnotator#before and immediately
     * execute (i.e., `require(modulekey)`) the module that was just defined (i.e., `define(modulekey)`).
     * @deprecated unused. was created in v5 branch to facilitate refactoring. remove before 5.0.0
     */
    @Deprecated
    private LinkedHashSet<String> requiredResources(RawRequest raw) {
        LooselyTypedRequestExpander resources = new LooselyTypedRequestExpander(raw);
        LinkedHashSet<String> onlyUsedForAnnotatingExplicitlyRequiredWebModules =
                new LinkedHashSet<>(resources.getIncluded());
        return onlyUsedForAnnotatingExplicitlyRequiredWebModules;
    }

    /**
     * Handle not found resources, redirects and not modified resources.
     *
     * @return true if request has been handled and no further processing needed.
     */
    protected boolean handleNotFoundRedirectAndNotModified(Resource resource) {
        if (resource == null) {
            response.sendError(404);
            return true;
        }
        if (checkIfCachedAndNotModified(resource.getParent().getUpdatedAt())) {
            return true;
        }
        if (resource.isRedirect()) {
            response.sendRedirect(resource.getLocation(), resource.getContentType());
            return true;
        }
        return false;
    }

    /**
     * Check if resource is not modified and replies with not-modified response if so.
     *
     * @param updatedAt when resource has been updated.
     * @return true if Resources is not modified and no further processing is needed.
     */
    protected boolean checkIfCachedAndNotModified(Date updatedAt) {
        LastModifiedHandler lastModifiedHandler = new LastModifiedHandler(updatedAt);
        return request.isCacheable() && response.checkRequestHelper(lastModifiedHandler);
    }

    protected void sendCached(Content content, Map<String, String> params, boolean isCachingEnabled) {
        if (!content.isPresent() && Config.isBundleHashValidationEnabled()) {
            response.sendError(404);
            return;
        }

        if (Boolean.TRUE.toString().equals(params.get(Config.ALLOW_PUBLIC_USE_PARAM_NAME))) {
            response.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
        }

        final boolean cacheHit;
        if (isSourceMapEnabled()
                && content.isTransformed()
                && globals.getConfig().optimiseSourceMapsForDevelopment()) {
            cacheHit = sendCachedInDevelopment(content, isCachingEnabled);
        } else {
            cacheHit = sendCachedInProduction(content, isCachingEnabled);
        }

        log.debug(
                "Called sendCached on the resource with request URL {} and response status code {}",
                request.getPath(),
                response.getStatus());

        if (globals.getConfig().isPerformanceTrackingEnabled()) {
            publishedThrottledEventIfEventPublisherNonNull(
                    globals.getEventPublisher(),
                    new RequestServingCacheEvent(
                            request.isCacheable(),
                            cacheHit,
                            isCachingEnabled,
                            request.isSourceMap(),
                            request.getServingType(),
                            response.numBytesWritten()));
        }
    }

    private boolean sendCachedInDevelopment(Content content, boolean isCachingEnabled) {
        final boolean cacheHit;
        // Setting content type.
        String contentType = content.getContentType() != null ? content.getContentType() : request.getContentType();
        response.setContentTypeIfNotBlank(contentType);

        Cache cache = isCachingEnabled && request.isCacheable() ? globals.getContentCache() : new PassThroughCache();

        // Detecting content type of the resource.
        final String resourceContentType;

        if (request.isSourceMap()) {
            String resourcePath = sourceMapUrlToUrl(request.getPath());
            resourceContentType = content.getContentType() != null
                    ? content.getContentType()
                    : globals.getConfig().getContentType(resourcePath);
        } else {
            resourceContentType = contentType;
        }

        // Using two stream provider to cache both resource and its source map.
        Cache.TwoStreamProvider twoStreamProvider = (out1, out2) -> {
            final boolean sourceMapEnabled = true;
            LineCountingProxyOutputStream lineCountingStream = new LineCountingProxyOutputStream(out1);
            ReadableSourceMap sourceMap = content.writeTo(lineCountingStream, sourceMapEnabled);

            String sourceMapUrl = globals.getRouter().sourceMapUrl(request.getPath(), request.getParams());
            try {
                out1.write(("\n" + generateSourceMapComment(sourceMapUrl, resourceContentType)).getBytes());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            // For some resources source map could not be generated, in such case generating 1 to 1 source map.
            generateOneToOneSourceMap(out2, lineCountingStream, toWritableSourceMap(sourceMap), request);
        };

        // Sending data to browser.
        OutputStream out = response.getOutputStream();
        String cacheKey = buildCacheKey();
        if (request.isSourceMap()) {
            cacheHit = cache.cacheTwo("http", cacheKey, null, out, twoStreamProvider);
        } else {
            cacheHit = cache.cacheTwo("http", cacheKey, out, null, twoStreamProvider);
        }
        return cacheHit;
    }

    private boolean sendCachedInProduction(Content content, boolean isCachingEnabled) {
        final boolean cacheHit;
        String contentType = content.getContentType() != null ? content.getContentType() : request.getContentType();
        response.setContentTypeIfNotBlank(contentType);

        Cache cache = isCachingEnabled && request.isCacheable() ? globals.getContentCache() : new PassThroughCache();
        String cacheKey = buildCacheKey();

        if (request.isSourceMap()) {
            // Serving Source Map.
            if (globals.getConfig().isSourceMapEnabled()) {
                cacheHit = cache.cache("http", cacheKey, response.getOutputStream(), producerOut -> {
                    LineCountingProxyOutputStream lineCountingStream =
                            new LineCountingProxyOutputStream(new NullOutputStream());
                    ReadableSourceMap sourceMap = content.writeTo(lineCountingStream, true);

                    // For some resources source map could not be generated, in such case generating 1 to 1 source map.
                    generateOneToOneSourceMap(producerOut, lineCountingStream, toWritableSourceMap(sourceMap), request);
                });
            } else {
                cacheHit = false;
                response.sendError(503);
            }
        } else {
            // Serving Resources.
            cacheHit = cache.cache(
                    "http", cacheKey, response.getOutputStream(), producerOut -> content.writeTo(producerOut, false));
            if (isSourceMapEnabled() && content.isTransformed()) {
                String sourceMapUrl = globals.getRouter().sourceMapUrl(request.getPath(), request.getParams());
                try {
                    response.getOutputStream()
                            .write(("\n" + generateSourceMapComment(sourceMapUrl, contentType)).getBytes());
                } catch (RuntimeException | IOException e) {
                    Support.LOGGER.error("can't generate source map comment", e);
                }
            }
        }
        return cacheHit;
    }

    private static void generateOneToOneSourceMap(
            OutputStream out,
            LineCountingProxyOutputStream lineCountingStream,
            WritableSourceMap sourceMap,
            Request request) {
        if (sourceMap == null) {
            String resourceUrl = sourceMapUrlToUrl(request.getUrl());
            sourceMap = Util.create1to1SourceMap(lineCountingStream.getLinesCount(), resourceUrl);
        }
        try {
            out.write(sourceMap.generate().getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates cache key for resource or batch.
     */
    protected String buildCacheKey() {
        return request.getRequestHash();
    }

    /**
     * If Source Map is enabled for the current Request.
     */
    protected boolean isSourceMapEnabled() {
        // The `isSourceMapEnabled` used to choose single stream or double stream cache,
        // and as soon the cache should be chosen
        // before of the resolution of the resource, it can't use any data from resource (so it can't use `resource
        // .getType()`).
        // The decision should be based purely on data available in the request.
        return globals.getConfig().isSourceMapEnabledFor(request.getType());
    }
}
