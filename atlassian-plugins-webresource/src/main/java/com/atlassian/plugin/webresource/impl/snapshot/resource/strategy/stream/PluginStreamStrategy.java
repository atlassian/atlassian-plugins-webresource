package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream;

import java.io.InputStream;

import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;

public class PluginStreamStrategy implements StreamStrategy {
    private final WebResourceIntegration integration;
    private final Bundle bundle;

    PluginStreamStrategy(WebResourceIntegration integration, Bundle bundle) {
        this.integration = integration;
        this.bundle = bundle;
    }

    @Override
    public InputStream getInputStream(String path) {
        return integration.getPluginAccessor().getEnabledPlugin(bundle.getKey()).getResourceAsStream(path);
    }
}
