package com.atlassian.plugin.webresource.legacy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.Iterables;
import io.atlassian.util.concurrent.ResettableLazyReference;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.contains;
import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableMap;

public class DefaultResourceDependencyResolver implements ResourceDependencyResolver {
    private static final Logger log = LoggerFactory.getLogger(DefaultResourceDependencyResolver.class);

    private final WebResourceIntegration webResourceIntegration;
    private final Cache cached = new Cache();
    private final boolean isSuperBatchingEnabled;
    private final List<String> superBatchModuleCompleteKeys;
    private final Globals globals;

    public DefaultResourceDependencyResolver(
            Globals globals,
            final WebResourceIntegration webResourceIntegration,
            boolean isSuperBatchingEnabled,
            List<String> superBatchModuleCompleteKeys) {
        this.globals = globals;
        this.webResourceIntegration = webResourceIntegration;
        this.isSuperBatchingEnabled = isSuperBatchingEnabled;
        this.superBatchModuleCompleteKeys = superBatchModuleCompleteKeys;
    }

    public Iterable<ModuleDescriptorStub> getSuperBatchDependencies() {
        return cached.resourceMap().values();
    }

    private Iterable<String> getSuperBatchDependencyKeys() {
        return cached.resourceMap().keySet();
    }

    public Iterable<ModuleDescriptorStub> getDependencies(
            RequestCache requestCache,
            UrlBuildingStrategy urlBuildingStrategy,
            final String moduleKey,
            final boolean excludeSuperBatchedResources,
            boolean includeDependenciesForFailedUrlReadingConditions) {
        final LinkedHashMap<String, ModuleDescriptorStub> orderedResources = new LinkedHashMap<>();
        final Iterable<String> superBatchResources =
                excludeSuperBatchedResources ? getSuperBatchDependencyKeys() : Collections.<String>emptyList();
        resolveDependencies(
                requestCache,
                urlBuildingStrategy,
                moduleKey,
                orderedResources,
                superBatchResources,
                new Stack<String>(),
                null,
                includeDependenciesForFailedUrlReadingConditions);
        return orderedResources.values();
    }

    public Iterable<ModuleDescriptorStub> getDependenciesInContext(
            RequestCache requestCache,
            UrlBuildingStrategy urlBuildingStrategy,
            final String context,
            final Set<String> skippedResources,
            boolean includeDependenciesForFailedUrlReadingConditions) {
        return getDependenciesInContext(
                requestCache,
                urlBuildingStrategy,
                context,
                true,
                skippedResources,
                includeDependenciesForFailedUrlReadingConditions);
    }

    public Iterable<ModuleDescriptorStub> getDependenciesInContext(
            RequestCache requestCache,
            UrlBuildingStrategy urlBuildingStrategy,
            final String context,
            boolean excludeSuperBatchedResources,
            final Set<String> skippedResources,
            boolean includeDependenciesForFailedUrlReadingConditions) {
        final Set<ModuleDescriptorStub> contextResources = new LinkedHashSet<>();
        final Class<WebResourceModuleDescriptor> clazz = WebResourceModuleDescriptor.class;
        List<String> contextDependencies = new ArrayList<>();
        for (final WebResourceModuleDescriptor descriptor :
                webResourceIntegration.getPluginAccessor().getEnabledModuleDescriptorsByClass(clazz)) {
            if (descriptor != null && descriptor.getContexts().contains(context)) {
                contextDependencies.add(descriptor.getCompleteKey());
            }
        }

        // Adding modules.
        Bundle bundle = requestCache.getSnapshot().get(Config.CONTEXT_PREFIX + ":" + context);
        if (bundle != null) {
            contextDependencies.addAll(bundle.getDependencies());
        }

        for (String key : contextDependencies) {
            final LinkedHashMap<String, ModuleDescriptorStub> dependencies = new LinkedHashMap<>();
            final Iterable<String> superBatchResources =
                    excludeSuperBatchedResources ? getSuperBatchDependencyKeys() : Collections.<String>emptyList();
            resolveDependencies(
                    requestCache,
                    urlBuildingStrategy,
                    key,
                    dependencies,
                    superBatchResources,
                    new Stack<String>(),
                    skippedResources,
                    includeDependenciesForFailedUrlReadingConditions);
            for (final ModuleDescriptorStub dependency : dependencies.values()) {
                contextResources.add(dependency);
            }
        }

        return unmodifiableCollection(contextResources);
    }

    /**
     * Adds the resources as well as its dependencies in order to the given ordered set. This method uses recursion
     * to add a resouce's dependent resources also to the set. You should call this method with a new stack
     * passed to the last parameter.
     * <p>
     * Note that resources already in the given super batch will be excluded when resolving dependencies. You
     * should pass in an empty set for the super batch to include super batch resources.
     *
     * @param moduleKey                                        the module complete key to add as well as its dependencies
     * @param orderedResourceKeys                              an ordered list set where the resources are added in order
     * @param superBatchResources                              the set of super batch resources to exclude when resolving dependencies
     * @param stack                                            where we are in the dependency tree
     * @param skippedResources                                 if not null, all resources with conditions are skipped and added to this set.
     * @param includeDependenciesForFailedUrlReadingConditions if dependencies resolved for context batch we need to
     *                                                         include all dependencies for UrlReadingConditions even if it failed, all those dependencies would be
     *                                                         be cached. Later, those failed dependencies would be removed at the time when content served.
     */
    private void resolveDependencies(
            RequestCache requestCache,
            UrlBuildingStrategy urlBuildingStrategy,
            String moduleKey,
            Map<String, ModuleDescriptorStub> orderedResourceKeys,
            Iterable<String> superBatchResources,
            Stack<String> stack,
            Set<String> skippedResources,
            boolean includeDependenciesForFailedUrlReadingConditions) {
        if (contains(superBatchResources, moduleKey)) {
            log.debug(
                    "Not requiring web-resource `{}` because it is part of a super-batch\nInclusion stack: {}",
                    moduleKey,
                    stack);
            return;
        }
        if (stack.contains(moduleKey)) {
            log.debug("Cyclic web-resource dependency detected with `{}`\nInclusion stack: {}", moduleKey, stack);
            return;
        }

        final ModuleDescriptor<?> moduleDescriptor;
        try {
            moduleDescriptor = webResourceIntegration.getPluginAccessor().getEnabledPluginModule(moduleKey);
        } catch (IllegalArgumentException e) {
            // this can happen if the module key is an invalid format (e.g. has no ":")
            log.debug("Cannot find web-resource with key `{}`\nInclusion stack: {}", moduleKey, stack);
            return;
        }
        if (moduleDescriptor == null) {
            return;
        }
        if (!(moduleDescriptor instanceof WebResourceModuleDescriptor)) {
            if (log.isDebugEnabled()) {
                if (webResourceIntegration.getPluginAccessor().getPluginModule(moduleKey) != null) {
                    log.debug(
                            "Cannot include disabled web-resource module `{}`\nInclusion stack: {}", moduleKey, stack);
                } else {
                    log.debug("Cannot find web-resource module for `{}`\nInclusion stack: {}", moduleKey, stack);
                }
            }
            return;
        }

        final WebResourceModuleDescriptor webResourceModuleDescriptor = (WebResourceModuleDescriptor) moduleDescriptor;

        // If the webresource cannot encode its state into the URL, it must be broken out of the batch and served
        // separately. This is normally due to the presence of a DecoratingLegacyCondition
        boolean skipDependencies = false;
        if (!includeDependenciesForFailedUrlReadingConditions) {
            Bundle bundle = requestCache.getSnapshot().get(webResourceModuleDescriptor.getCompleteKey());
            if (bundle != null) {
                CachedCondition condition = bundle.getCondition();
                if (condition != null) {
                    final boolean conditionResult = !condition.evaluateSafely(requestCache, urlBuildingStrategy);
                    skipDependencies = conditionResult;
                    log.debug(
                            "Condition for web-resource {} evaluated; updating skipDependencies from {} to {}",
                            webResourceModuleDescriptor.getCompleteKey(),
                            skipDependencies,
                            conditionResult);
                }
            }
        }

        final List<String> dependencies = webResourceModuleDescriptor.getDependencies();
        log.debug(
                "About to add web-resource `{}` and its dependencies: {} and skipDependencies is {}\nInclusion stack: {}",
                moduleKey,
                dependencies,
                skipDependencies,
                stack);
        stack.push(moduleKey);
        try {
            if (!skipDependencies) {
                for (final String dependency : dependencies) {
                    if (orderedResourceKeys.get(dependency) == null) {
                        resolveDependencies(
                                requestCache,
                                urlBuildingStrategy,
                                dependency,
                                orderedResourceKeys,
                                superBatchResources,
                                stack,
                                skippedResources,
                                includeDependenciesForFailedUrlReadingConditions);
                    }
                }
            }
        } finally {
            log.debug("Finished adding web-resource `{}` and its dependencies\nInclusion stack: {}", moduleKey, stack);
            stack.pop();
        }
        orderedResourceKeys.put(moduleKey, new ModuleDescriptorStub(webResourceModuleDescriptor));
    }

    static final class SuperBatch {
        final String version;
        final Map<String, ModuleDescriptorStub> resources;

        SuperBatch(final String version, final Map<String, ModuleDescriptorStub> resources) {
            this.version = checkNotNull(version);
            this.resources = checkNotNull(resources);
        }
    }

    final class Cache {
        ResettableLazyReference<SuperBatch> lazy = new ResettableLazyReference<SuperBatch>() {
            @Override
            protected SuperBatch create() throws Exception {
                // The linked hash map ensures that order is preserved
                final String version = webResourceIntegration.getSuperBatchVersion();
                return new SuperBatch(version, loadDescriptors(superBatchModuleCompleteKeys));
            }

            Map<String, ModuleDescriptorStub> loadDescriptors(final Iterable<String> moduleKeys) {
                if (Iterables.isEmpty(moduleKeys)) {
                    return emptyMap();
                }
                final Map<String, ModuleDescriptorStub> resources = new LinkedHashMap<>();
                for (final String moduleKey : moduleKeys) {
                    RequestCache requestCache = new RequestCache(globals);
                    UrlBuildingStrategy normalStrategy = UrlBuildingStrategy.normal();
                    resolveDependencies(
                            requestCache,
                            normalStrategy,
                            moduleKey,
                            resources,
                            Collections.<String>emptyList(),
                            new Stack<String>(),
                            null,
                            true);
                }
                return unmodifiableMap(resources);
            }
        };

        Map<String, ModuleDescriptorStub> resourceMap() {
            if (!isSuperBatchingEnabled) {
                log.debug("Super batching not enabled, but getSuperBatchDependencies() called. Returning empty.");
                return emptyMap();
            }
            while (true) {
                final SuperBatch batch = lazy.get();
                if (batch.version.equals(webResourceIntegration.getSuperBatchVersion())) {
                    return batch.resources;
                }

                // The super batch has been updated so recreate the batch
                lazy.reset();
            }
        }
    }
}
