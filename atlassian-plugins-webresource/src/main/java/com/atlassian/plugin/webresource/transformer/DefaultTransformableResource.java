package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.webresource.api.transformer.TransformableResource;

public final class DefaultTransformableResource implements TransformableResource {
    private final ResourceLocation location;
    private final DownloadableResource nextResource;

    public DefaultTransformableResource(ResourceLocation location, DownloadableResource nextResource) {
        this.location = location;
        this.nextResource = nextResource;
    }

    public ResourceLocation location() {
        return this.location;
    }

    public DownloadableResource nextResource() {
        return nextResource;
    }
}
