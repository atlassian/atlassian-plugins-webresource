package com.atlassian.plugin.webresource.impl.discovery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

/**
 * Query builder for retrieving ordered lists of {@link Resource} objects based on what the user is requesting
 * and what has previously been requested.
 *
 * The order ensures that:
 *
 * <ul>
 *     <li>All &lt;resource&gt; blocks inside one &lt;web-resource&gt; occur in the order listed.</li>
 *     <li>Dependencies will be placed before anything that depends on them.</li>
 * </ul>
 */
public class ResourceFinder {
    private final BundleFinder bundleFinder;
    private final RequestCache requestCache;
    private Predicate<Resource> filter;

    public ResourceFinder(BundleFinder bundleFinder, RequestCache requestCache) {
        this.bundleFinder = bundleFinder;
        this.requestCache = requestCache;
    }

    /**
     * Filter resources.
     */
    public ResourceFinder filter(Predicate<Resource> filter) {
        this.filter = filter;
        return this;
    }

    /**
     * Run query and get found resources.
     */
    public List<Resource> end() {
        List<String> keys = bundleFinder.end();
        List<Resource> resources = getResources(keys);
        if (filter != null) {
            return resources.stream().filter(filter).collect(Collectors.toList());
        }
        return resources;
    }

    /**
     * Get Resources for list of Web Resources.
     *
     * @param keys keys of Web Resources
     */
    public List<Resource> getResources(Collection<String> keys) {
        List<Resource> resources = new ArrayList<>();
        for (String key : keys) {
            Bundle bundle = requestCache.getSnapshot().get(key);
            if (bundle == null) {
                continue;
            }
            resources.addAll(bundle.getResources(requestCache).values());
        }
        return resources;
    }
}
