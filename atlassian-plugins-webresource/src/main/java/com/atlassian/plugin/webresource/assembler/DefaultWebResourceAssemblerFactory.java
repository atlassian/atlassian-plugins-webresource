package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerBuilder;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;

/**
 * Default implementation of WebResourceAssemblerFactory
 *
 * @since v3.0
 */
public class DefaultWebResourceAssemblerFactory implements WebResourceAssemblerFactory {
    private final Globals globals;

    public DefaultWebResourceAssemblerFactory(Globals globals) {
        this.globals = globals;
    }

    @Override
    public WebResourceAssemblerBuilder create() {
        return new DefaultWebResourceAssemblerBuilder(globals);
    }
}
