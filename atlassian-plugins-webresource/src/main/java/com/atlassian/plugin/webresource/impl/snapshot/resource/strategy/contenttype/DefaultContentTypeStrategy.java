package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contenttype;

import com.atlassian.plugin.elements.ResourceLocation;

public class DefaultContentTypeStrategy implements ContentTypeStrategy {
    private ResourceLocation resourceLocation;

    DefaultContentTypeStrategy(ResourceLocation resource) {
        this.resourceLocation = resource;
    }

    @Override
    public String getContentType() {
        return resourceLocation.getContentType();
    }
}
