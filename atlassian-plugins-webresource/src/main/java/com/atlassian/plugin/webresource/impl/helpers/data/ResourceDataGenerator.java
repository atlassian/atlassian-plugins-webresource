package com.atlassian.plugin.webresource.impl.helpers.data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.plugin.webresource.data.DefaultPluginDataResource;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.discovery.BundleFinder;
import com.atlassian.plugin.webresource.impl.helpers.ResourceGenerationInfo;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.api.data.PluginDataResource;

import static com.atlassian.plugin.webresource.impl.helpers.BaseHelpers.isConditionsSatisfied;

public class ResourceDataGenerator {

    public Set<PluginDataResource> generate(ResourceGenerationInfo info) {
        final Set<PluginDataResource> dataResources = new HashSet<>();
        if (info.getResourcePhase().isPresent()) {
            final RequestState requestState = info.getData();
            final ResourcePhase resourcePhase = info.getResourcePhase().get();

            // TODO SPFE-392: Refactor to use GraphWalker instead of BundleFinder - hey maybe we should do that in other
            // places? Maybe that should be a ticket
            final List<String> webResourceKeysWithData = new BundleFinder(requestState.getSnapshot())
                    .included(requestState.getIncluded(resourcePhase))
                    .excluded(
                            requestState.getExcluded(),
                            isConditionsSatisfied(requestState.getRequestCache(), requestState.getUrlStrategy()))
                    .deepFilter(isConditionsSatisfied(requestState.getRequestCache(), requestState.getUrlStrategy()))
                    .end();

            // Adding implicitly required data.
            requestState.getSnapshot().toBundles(webResourceKeysWithData).forEach(bundle -> {
                bundle.getData()
                        .forEach((key, value) ->
                                dataResources.add(new DefaultPluginDataResource(bundle.getKey() + '.' + key, value)));
            });

            // Adding explicitly required data.
            requestState.getIncludedData(resourcePhase).entrySet().stream()
                    .filter(entry -> !requestState.getExcludedData().contains(entry.getKey()))
                    .forEach(data -> {
                        dataResources.add(new DefaultPluginDataResource(data.getKey(), data.getValue()));
                    });
        }

        return dataResources;
    }
}
