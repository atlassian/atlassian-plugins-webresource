package com.atlassian.plugin.webresource.impl.support;

import java.io.InputStream;
import java.util.Optional;
import javax.annotation.Nullable;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.atlassian.sourcemap.ReadableSourceMap;

import static java.util.Optional.ofNullable;

/**
 * <p>This class goal is to allow the load of the most appropriate source of content for the file.</p>
 * <p>It does this by abstracting over a plugin’s filesystem and choosing the most appropriate tangible file to pull content from.</p>
 *
 * @since 5.0.0
 */
public abstract class InitialContent {
    private final InputStream content;
    private final String path;
    private final ReadableSourceMap sourceMap;

    public InitialContent(
            @Nullable final InputStream content,
            @Nullable final String path,
            @Nullable final ReadableSourceMap sourceMap) {
        this.content = content;
        this.path = path;
        this.sourceMap = sourceMap;
    }

    @NonNull
    public Optional<InputStream> getContent() {
        return ofNullable(content);
    }

    @NonNull
    public Optional<String> getPath() {
        return ofNullable(path);
    }

    @NonNull
    public Optional<ReadableSourceMap> getSourceMap() {
        return ofNullable(sourceMap);
    }

    /**
     * Converts the current {@link InitialContent} to {@link Content}.
     *
     * @param originalContent The original content to be transformed.
     * @return The build {@link Content}
     */
    @NonNull
    public abstract Content toContent(@NonNull final Content originalContent);
}
