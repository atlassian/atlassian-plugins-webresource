package com.atlassian.plugin.webresource.impl.helpers.url;

import java.util.List;
import java.util.Set;

/**
 * DTO representing three collections:
 *
 * 1) ordered list of context batches to serve,
 * 2) ordered list of web resource batches to serve, and
 * 3) unordered "excluded resolved" list, which is a combination of all web resources and contexts
 *    discovered in the dependency graph while constructing the first two lists, whose content will either be
 *    served in these batch files or was omitted from them.
 *
 * When serving, context batches must be served first, followed by web-resource batches.
 * The content from any item in the "excluded resolved" list must be omitted from future requests made by this client.
 *
 * @see com.atlassian.plugin.webresource.legacy.LegacyUrlGenerationHelpers.Resolved
 */
public class CalculatedBatches {
    private final List<ContextBatch> contextBatches;
    private final List<WebResourceBatch> webResourceBatches;
    private final Set<String> excludedResolved;

    public CalculatedBatches(
            final List<ContextBatch> contextBatches,
            final List<WebResourceBatch> webResourceBatches,
            final Set<String> excludedResolved) {
        this.contextBatches = contextBatches;
        this.webResourceBatches = webResourceBatches;
        this.excludedResolved = excludedResolved;
    }

    /**
     * @return ordered list of context batches to serve.
     */
    public List<ContextBatch> getContextBatches() {
        return contextBatches;
    }

    /**
     * @return ordered list of web resource batches to serve. Be sure to serve after {@link #getContextBatches()}
     * to preserve expected code execution order.
     */
    public List<WebResourceBatch> getWebResourceBatches() {
        return webResourceBatches;
    }

    /**
     * @return a combination of all web resources and contexts discovered in the dependency graph while constructing
     * the {@link #getContextBatches()} and {@link #getWebResourceBatches()} lists, whose content will either be
     * served in these batch files or was omitted from them.
     *
     * The content from any item contained here must be omitted from future requests made by this client.
     */
    public Set<String> getExcludedResolved() {
        return excludedResolved;
    }
}
