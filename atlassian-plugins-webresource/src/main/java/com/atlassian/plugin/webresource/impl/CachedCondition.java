package com.atlassian.plugin.webresource.impl;

import java.util.Map;

import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

/**
 * Cached condition.
 * <p>
 * This class is interned, see {@link com.atlassian.plugin.webresource.impl.support.ConditionInstanceCache}.
 *
 * @since v3.3
 */
public class CachedCondition {

    private final DecoratingCondition condition;

    public CachedCondition(DecoratingCondition condition) {
        this.condition = condition;
    }

    /**
     * The {@link UrlReadingCondition} will be asked to generate parameters and evaluate it.
     * If error will be thrown it will be intercepted and condition evaluated to false.
     */
    public boolean evaluateSafely(RequestCache requestCache, UrlBuildingStrategy urlBuilderStrategy) {
        Boolean result = requestCache.getCachedConditionsEvaluation().get(this);
        if (result == null) {
            DefaultUrlBuilder defaultUrlBuilder = new DefaultUrlBuilder();
            addToUrlSafely(requestCache, defaultUrlBuilder, urlBuilderStrategy);
            result = evaluateSafely(requestCache, defaultUrlBuilder.buildParams());
            requestCache.getCachedConditionsEvaluation().put(this, result);
        }
        return result;
    }

    /**
     * The {@link UrlReadingCondition} will be asked to generate parameters and evaluate it.
     * If error will be thrown it will be intercepted and condition evaluated to false.
     */
    public boolean evaluateSafely(RequestCache requestCache, Map<String, String> params) {
        Boolean result = requestCache.getCachedConditionsEvaluation().get(this);
        if (result == null) {
            try {
                result = condition.shouldDisplay(QueryParams.of(params));
            } catch (RuntimeException e) {
                Support.LOGGER.warn("exception thrown in `shouldDisplay` during condition evaluation", e);
                return false;
            }
            requestCache.getCachedConditionsEvaluation().put(this, result);
        }
        return result;
    }

    /**
     * Add parameters of condition to url.
     * If error will be thrown it will be intercepted and nothing will be added to url.
     */
    public void addToUrlSafely(
            RequestCache requestCache, UrlBuilder urlBuilder, UrlBuildingStrategy urlBuilderStrategy) {
        DefaultUrlBuilder result = requestCache.getCachedConditionsParameters().get(this);
        if (result == null) {
            result = new DefaultUrlBuilder();
            try {
                condition.addToUrl(result, urlBuilderStrategy);
            } catch (RuntimeException e) {
                Support.LOGGER.warn("exception thrown in `addToUrl` during condition evaluation", e);
            }
            requestCache.getCachedConditionsParameters().put(this, result);
        }
        result.applyTo(urlBuilder);
    }
}
