package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.path;

import com.atlassian.plugin.elements.ResourceLocation;

public class DefaultPathStrategy implements PathStrategy {
    private final ResourceLocation resourceLocation;

    DefaultPathStrategy(ResourceLocation resourceLocation) {
        this.resourceLocation = resourceLocation;
    }

    @Override
    public String getPath() {
        return resourceLocation.getLocation();
    }
}
