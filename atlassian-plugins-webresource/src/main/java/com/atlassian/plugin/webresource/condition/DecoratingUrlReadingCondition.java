package com.atlassian.plugin.webresource.condition;

import java.util.Map;

import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

import static com.atlassian.plugin.webresource.condition.MetricsUtil.startWebConditionProfilingTimer;

/**
 * Instance of a DecoratingCondition that wraps a UrlReadingCondition
 *
 * @since v3.0
 */
public class DecoratingUrlReadingCondition implements DecoratingCondition {
    protected final UrlReadingCondition urlReadingCondition;
    protected final Map<String, String> params;
    private final String pluginKey;
    private final String conditionClassName;

    public DecoratingUrlReadingCondition(
            UrlReadingCondition urlReadingCondition,
            Map<String, String> params,
            String pluginKey,
            String conditionClassName) {
        this.urlReadingCondition = urlReadingCondition;
        this.params = params;
        this.pluginKey = pluginKey;
        this.conditionClassName = conditionClassName;
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder, UrlBuildingStrategy urlBuilderStrategy) {
        urlBuilderStrategy.addToUrl(urlReadingCondition, urlBuilder);
    }

    @Override
    public boolean shouldDisplay(QueryParams params) {
        try (Ticker ignored = startWebConditionProfilingTimer(pluginKey, conditionClassName)) {
            return urlReadingCondition.shouldDisplay(params);
        }
    }

    @Override
    public DecoratingCondition invertCondition() {
        // It's never used, maybe it would be better to throw not implemented exception.
        return new DecoratingCondition() {
            @Override
            public void addToUrl(UrlBuilder urlBuilder, UrlBuildingStrategy urlBuilderStrategy) {
                DecoratingUrlReadingCondition.this.addToUrl(urlBuilder, urlBuilderStrategy);
            }

            @Override
            public boolean shouldDisplay(QueryParams params) {
                return !DecoratingUrlReadingCondition.this.shouldDisplay(params);
            }

            @Override
            public DecoratingCondition invertCondition() {
                return DecoratingUrlReadingCondition.this;
            }
        };
    }

    public Map<String, String> getParams() {
        return params;
    }

    public UrlReadingCondition getUrlReadingCondition() {
        return urlReadingCondition;
    }
}
