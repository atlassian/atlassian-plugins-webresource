package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contenttype;

public interface ContentTypeStrategy {
    String getContentType();
}
