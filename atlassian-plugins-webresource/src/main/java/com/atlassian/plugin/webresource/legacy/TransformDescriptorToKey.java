package com.atlassian.plugin.webresource.legacy;

import com.google.common.base.Function;

public class TransformDescriptorToKey implements Function<ModuleDescriptorStub, String> {
    public String apply(final ModuleDescriptorStub resource) {
        return resource.getCompleteKey();
    }
}
