package com.atlassian.plugin.webresource.impl.support.http;

import java.io.IOException;
import java.io.OutputStream;
import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.CountingOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.servlet.util.LastModifiedHandler;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Probably would be removed after the refactoring, currently needed to hide some implementation details.
 *
 * @since 3.3
 */
public class Response {
    private static final Logger log = LoggerFactory.getLogger(Response.class);

    private final Request request;
    private final HttpServletResponse originalResponse;
    private final CountingOutputStream decoratedOutputStream;

    public Response(final Request request, final HttpServletResponse response) {
        this.request = request;
        this.originalResponse = response;
        try {
            this.decoratedOutputStream = new CountingOutputStream(response.getOutputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void addHeader(final String name, final String value) {
        originalResponse.addHeader(name, value);
    }

    /**
     * DEPRECATED - just like the underlying method.
     * <p>
     * Only exists because WRM common has been left behind accidentally in Atlassian Plugins repo and we can't get both
     * upgraded at the same time AND we don't want to expose the underlying HttpServletResponse
     *
     * @param lastModifiedHandler last modified handler to checkRequest with
     * @return lastModifiedHandler#checkRequest
     */
    @Deprecated // TODO - Remove once WRM common is in the same repo https://ecosystem.atlassian.net/browse/PLUGWEB-561
    public boolean checkRequestHelper(@Nonnull final LastModifiedHandler lastModifiedHandler) {
        return lastModifiedHandler.checkRequest(request.getOriginalRequest(), originalResponse);
    }

    public void sendRedirect(String location, String contentType) {
        setContentTypeIfNotBlank(contentType);
        try {
            request.getOriginalRequest()
                    .getRequestDispatcher(location)
                    .forward(request.getOriginalRequest(), originalResponse);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ServletException e2) {
            throw new RuntimeException(e2);
        }
    }

    public void sendError(int code) {
        log.debug("Sending error code {} for response to request with URL {}", code, request.getPath());
        try {
            originalResponse.sendError(code);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void setContentType(String contentType) {
        originalResponse.setContentType(contentType);
    }

    public void setCharacterEncoding(String characterEncoding) {
        originalResponse.setCharacterEncoding(characterEncoding);
    }

    public OutputStream getOutputStream() {
        return decoratedOutputStream;
    }

    public void setContentTypeIfNotBlank(String contentType) {
        if (isNotBlank(contentType)) {
            setContentType(contentType);
        }
    }

    public void setStatus(int status) {
        originalResponse.setStatus(status);
    }

    public int getStatus() {
        return originalResponse.getStatus();
    }

    /***
     * Returns the total number of bytes written to the output stream.
     * NOTE: this is not necessarily the size of the Response.
     *
     * @return total number of bytes written to the output stream
     */
    public long numBytesWritten() {
        return decoratedOutputStream.getByteCount();
    }
}
