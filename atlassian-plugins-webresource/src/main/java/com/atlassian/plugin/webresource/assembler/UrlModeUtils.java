package com.atlassian.plugin.webresource.assembler;

import com.atlassian.webresource.api.UrlMode;

/**
 * Utilities for converting between {@link com.atlassian.webresource.api.UrlMode} and
 * {@link com.atlassian.webresource.api.UrlMode}
 *
 * @since v3.0
 */
public class UrlModeUtils {
    public static UrlMode convert(UrlMode urlMode) {
        switch (urlMode) {
            case ABSOLUTE:
                return UrlMode.ABSOLUTE;
            case RELATIVE:
                return UrlMode.RELATIVE;
            case AUTO:
                return UrlMode.AUTO;
            default:
                throw new IllegalArgumentException("Unrecognised UrlMode: " + urlMode);
        }
    }
}
