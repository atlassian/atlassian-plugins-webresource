package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream;

import java.io.InputStream;

import com.atlassian.plugin.servlet.ServletContextFactory;

public class TomcatStreamStrategy implements StreamStrategy {
    private final ServletContextFactory servletContextFactory;

    TomcatStreamStrategy(ServletContextFactory servletContextFactory) {
        this.servletContextFactory = servletContextFactory;
    }

    @Override
    public InputStream getInputStream(String path) {
        String pathWithSlash = path.startsWith("/") ? path : "/" + path;
        return servletContextFactory.getServletContext().getResourceAsStream(pathWithSlash);
    }
}
