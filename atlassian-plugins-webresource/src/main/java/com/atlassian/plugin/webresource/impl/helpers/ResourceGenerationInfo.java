package com.atlassian.plugin.webresource.impl.helpers;

import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

public class ResourceGenerationInfo {
    private final ResourcePhase resourcePhase;
    private final RequestState data;

    public ResourceGenerationInfo(@Nullable final ResourcePhase resourcePhase, @Nonnull final RequestState data) {
        this.resourcePhase = resourcePhase;
        this.data = requireNonNull(data);
    }

    @Nonnull
    public Optional<ResourcePhase> getResourcePhase() {
        return ofNullable(resourcePhase);
    }

    @Nonnull
    public RequestState getData() {
        return data;
    }
}
