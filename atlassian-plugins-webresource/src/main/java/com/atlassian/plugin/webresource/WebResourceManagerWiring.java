package com.atlassian.plugin.webresource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.spring.AvailableToPlugins;
import com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.assembler.LegacyPageBuilderService;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformerImpl;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.servlet.PluginResourceDownload;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformers;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformersSupplier;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.StaticTransformersSupplier;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.webresource.api.WebResourceManager;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;

@Configuration
public class WebResourceManagerWiring {

    @Bean
    public CdnResourceUrlTransformer cdnResourceUrlTransformer(@Lazy Config config) {
        return new CdnResourceUrlTransformerImpl(config);
    }

    @AvailableToPlugins(PageBuilderService.class)
    @Bean
    public LegacyPageBuilderService pageBuilderService(
            WebResourceIntegration webResourceIntegration, WebResourceAssemblerFactory webResourceAssemblerFactory) {
        return new DefaultPageBuilderService(webResourceIntegration, webResourceAssemblerFactory);
    }

    @Bean
    public PluginResourceDownload pluginResourceDownload(Globals globals, ContentTypeResolver contentTypeResolver) {
        return new PluginResourceDownload(globals, contentTypeResolver);
    }

    @AvailableToPlugins(PluginResourceLocator.class)
    @Bean
    public PluginResourceLocatorImpl pluginResourceLocator(PluginEventManager pluginEventManager, Globals globals) {
        return new PluginResourceLocatorImpl(pluginEventManager, globals);
    }

    @Bean
    public StaticTransformersSupplier staticTransformersSupplier(
            WebResourceIntegration webResourceIntegration,
            WebResourceUrlProvider webResourceUrlProvider,
            @Lazy CdnResourceUrlTransformer cdnResourceUrlTransformer) {
        return new DefaultStaticTransformersSupplier(
                webResourceIntegration, webResourceUrlProvider, cdnResourceUrlTransformer);
    }

    @Bean
    public StaticTransformers staticTransformers(StaticTransformersSupplier staticTransformersSupplier) {
        return new DefaultStaticTransformers(staticTransformersSupplier);
    }

    @Bean
    public TransformerCache transformerCache(PluginEventManager pluginEventManager, PluginAccessor pluginAccessor) {
        return new TransformerCache(pluginEventManager, pluginAccessor);
    }

    @Bean
    public WebResourceBatchingStateCounter webResourceBatchingStateCounter(PluginEventManager pluginEventManager) {
        return new WebResourceBatchingStateCounterImpl(pluginEventManager);
    }

    @AvailableToPlugins(WebResourceAssemblerFactory.class)
    @Bean
    public WebResourceAssemblerFactory webResourceAssemblerFactory(Globals globals) {
        return new DefaultWebResourceAssemblerFactory(globals);
    }

    @Bean
    public Config webResourceConfig(
            ResourceBatchingConfiguration resourceBatchingConfiguration,
            WebResourceIntegration webResourceIntegration,
            WebResourceUrlProvider webResourceUrlProvider,
            ServletContextFactory servletContextFactory,
            TransformerCache transformerCache,
            StaticTransformers staticTransformers) {
        var config = new Config(
                resourceBatchingConfiguration,
                webResourceIntegration,
                webResourceUrlProvider,
                servletContextFactory,
                transformerCache);
        config.setStaticTransformers(staticTransformers);
        return config;
    }

    @Bean
    public Globals webResourceGlobals(
            Config config, EventPublisher eventPublisher, PluginEventManager pluginEventManager) {
        return new Globals(config, eventPublisher, pluginEventManager);
    }

    @AvailableToPlugins(WebResourceManager.class)
    @Bean
    public WebResourceManager webResourceManager(
            WebResourceAssemblerFactory webResourceAssemblerFactory,
            LegacyPageBuilderService pageBuilderService,
            PluginResourceLocator pluginResourceLocator,
            WebResourceUrlProvider webResourceUrlProvider,
            ResourceBatchingConfiguration resourceBatchingConfiguration) {
        return new WebResourceManagerImpl(
                webResourceAssemblerFactory,
                pageBuilderService,
                pluginResourceLocator,
                webResourceUrlProvider,
                resourceBatchingConfiguration);
    }

    @AvailableToPlugins(WebResourceUrlProvider.class)
    @Bean
    public WebResourceUrlProvider webResourceUrlProvider(WebResourceIntegration webResourceIntegration) {
        return new WebResourceUrlProviderImpl(webResourceIntegration);
    }
}
