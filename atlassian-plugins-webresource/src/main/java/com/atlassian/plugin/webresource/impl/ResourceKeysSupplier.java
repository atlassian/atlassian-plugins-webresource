package com.atlassian.plugin.webresource.impl;

import java.util.List;

/**
 * Container storing resource keys.
 */
public class ResourceKeysSupplier {
    private final List<ResourceKey> keys;

    public ResourceKeysSupplier(final List<ResourceKey> keys) {
        this.keys = keys;
    }

    public List<ResourceKey> getKeys() {
        return keys;
    }
}
