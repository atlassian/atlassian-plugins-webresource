package com.atlassian.plugin.webresource.analytics;

import java.util.Optional;
import java.util.SplittableRandom;
import javax.annotation.Nullable;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.webresource.analytics.events.RequestServingCacheEvent;

public class EventFiringHelper {
    private static final SplittableRandom splittableRandom = new SplittableRandom();
    private static final int THROTTLE_LOWER_BOUND = 0;
    private static final int THROTTLE_SERVER_REQUEST_UPPER_BOUND = 20;

    private EventFiringHelper() {}

    public static void publishIfEventPublisherNonNull(@Nullable EventPublisher optionalEventPublisher, Object event) {
        Optional.ofNullable(optionalEventPublisher).ifPresent(eventPublisher -> eventPublisher.publish(event));
    }

    public static void publishedThrottledEventIfEventPublisherNonNull(
            @Nullable EventPublisher optionalEventPublisher, RequestServingCacheEvent event) {
        // Update the analytic events versions if these values change, it won't be obvious that the amount has changed
        // due to the amount fired rather than any behaviour change.
        if (splittableRandom.nextInt(THROTTLE_LOWER_BOUND, THROTTLE_SERVER_REQUEST_UPPER_BOUND)
                == THROTTLE_LOWER_BOUND) {
            publishIfEventPublisherNonNull(optionalEventPublisher, event);
        }
    }
}
