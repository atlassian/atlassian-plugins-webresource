package com.atlassian.plugin.webresource;

import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.cache.filecache.Cache;
import com.atlassian.plugin.cache.filecache.impl.PassThroughCache;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.analytics.events.ServerResourceCacheInvalidationCause;
import com.atlassian.plugin.webresource.analytics.events.ServerResourceCacheInvalidationEvent;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.http.Controller;
import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformers;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformersSupplier;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.sourcemap.ReadableSourceMap;
import com.atlassian.webresource.api.WebResourceUrlProvider;

import static com.atlassian.plugin.webresource.analytics.EventFiringHelper.publishIfEventPublisherNonNull;
import static com.atlassian.plugin.webresource.analytics.events.ServerResourceCacheInvalidationCause.PLUGIN_DISABLED_EVENT;
import static com.atlassian.plugin.webresource.analytics.events.ServerResourceCacheInvalidationCause.PLUGIN_ENABLED_EVENT;
import static com.atlassian.plugin.webresource.analytics.events.ServerResourceCacheInvalidationCause.PLUGIN_WEBRESOURCE_MODULE_DISABLED;
import static com.atlassian.plugin.webresource.analytics.events.ServerResourceCacheInvalidationCause.PLUGIN_WEBRESOURCE_MODULE_ENABLED;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.asDownloadableResource;

/**
 * Default implementation of {@link PluginResourceLocator}.
 *
 * @since 2.2
 * @deprecated since 3.3.2
 */
@Deprecated
public class PluginResourceLocatorImpl implements PluginResourceLocator {
    private volatile Globals globals;

    public PluginResourceLocatorImpl(
            final WebResourceIntegration webResourceIntegration,
            final ServletContextFactory servletContextFactory,
            final WebResourceUrlProvider webResourceUrlProvider,
            final ResourceBatchingConfiguration batchingConfiguration,
            final PluginEventManager pluginEventManager) {
        Config config = new Config(
                batchingConfiguration,
                webResourceIntegration,
                webResourceUrlProvider,
                servletContextFactory,
                new TransformerCache(pluginEventManager, webResourceIntegration.getPluginAccessor()));
        StaticTransformers staticTransformers = new DefaultStaticTransformers(new DefaultStaticTransformersSupplier(
                webResourceIntegration, webResourceUrlProvider, config.getCdnResourceUrlTransformer()));
        config.setStaticTransformers(staticTransformers);
        initialize(pluginEventManager, config, webResourceIntegration.getEventPublisher());
    }

    public PluginResourceLocatorImpl(
            final WebResourceIntegration webResourceIntegration,
            final ServletContextFactory servletContextFactory,
            final WebResourceUrlProvider webResourceUrlProvider,
            final ResourceBatchingConfiguration batchingConfiguration,
            final PluginEventManager pluginEventManager,
            final StaticTransformers staticTransformers) {
        Config config = new Config(
                batchingConfiguration,
                webResourceIntegration,
                webResourceUrlProvider,
                servletContextFactory,
                new TransformerCache(pluginEventManager, webResourceIntegration.getPluginAccessor()));
        config.setStaticTransformers(staticTransformers);
        initialize(pluginEventManager, config, webResourceIntegration.getEventPublisher());
    }
    //

    public PluginResourceLocatorImpl(
            PluginEventManager pluginEventManager, Config config, EventPublisher eventPublisher) {
        initialize(pluginEventManager, config, eventPublisher);
    }

    /**
     * Added for PLUGWEB-612 as a way for anybody who created their own {@code Globals} object,
     * via Spring or otherwise. In future, WRM's consumers should not know about nor have access to
     * {@code Globals} or {@code Config}, as they are internal implementation details.
     *
     * This constructor will be superceded by a more humane single entrypoint in to constructing
     * the WRM library in PLUGWEB-619.
     */
    public PluginResourceLocatorImpl(PluginEventManager pluginEventManager, Globals globals) {
        initialize(pluginEventManager, globals);
    }

    /**
     * Initialises WRM's behaviour, creating a new {@code Globals} instance using provided {@code Config}
     * and binding to the plugin lifecycle, so it can update and invalidate its caches as the system
     * state changes.
     *
     * @param pluginEventManager the plugin system in which to listen for events
     * @param config configuration for the WRM. the WRM will construct a new {@code Globals} object from
     *               this, which might mean having multiple of them in your system. This would be
     *               a very bad thing, so be cautious when calling this method!
     */
    protected void initialize(PluginEventManager pluginEventManager, Config config, EventPublisher eventPublisher) {
        initialize(pluginEventManager, new Globals(config, eventPublisher, pluginEventManager));
    }

    /**
     * Initialises WRM's behaviour, binding {@code Globals} to the plugin framework events, so it can
     * update and invalidate its caches as the system state changes.
     *
     * This method will be superceded by a more humane single entrypoint in to constructing
     * the WRM library in PLUGWEB-619. WRM's consumers should not know about nor have access to
     * {@code Globals} or {@code Config}, as they are internal implementation details.
     *
     * The registration and listening for plugin framework events does not belong inside this deprecated class.
     * That responsibility will move elsewhere to a more centralised, non-deprecated location.
     *
     * @param pluginEventManager the plugin system in which to listen for events
     * @param globals a pre-constructed WRM state container.
     */
    protected void initialize(final PluginEventManager pluginEventManager, @Nonnull final Globals globals) {
        this.globals = globals;

        pluginEventManager.register(this);
    }

    @PluginEventListener
    public void onPluginDisabled(final PluginDisabledEvent event) {
        globals.triggerStateChange();
        fireServerResourceCacheInvalidationEvent(PLUGIN_DISABLED_EVENT);
    }

    @PluginEventListener
    public void onPluginEnabled(final PluginEnabledEvent event) {
        globals.triggerStateChange();
        fireServerResourceCacheInvalidationEvent(PLUGIN_ENABLED_EVENT);
    }

    @PluginEventListener
    public void onPluginModuleEnabled(final PluginModuleEnabledEvent event) {
        if (event.getModule() instanceof WebResourceModuleDescriptor) {
            globals.triggerStateChange();
            fireServerResourceCacheInvalidationEvent(PLUGIN_WEBRESOURCE_MODULE_ENABLED);
        }
    }

    @PluginEventListener
    public void onPluginModuleDisabled(final PluginModuleDisabledEvent event) {
        if (event.getModule() instanceof WebResourceModuleDescriptor) {
            globals.triggerStateChange();
            fireServerResourceCacheInvalidationEvent(PLUGIN_WEBRESOURCE_MODULE_DISABLED);
        }
    }

    @Override
    public boolean matches(final String url) {
        return globals.getRouter().canDispatch(url);
    }

    @Override
    public DownloadableResource getDownloadableResource(final String url, Map<String, String> queryParams) {
        // This code should be deleted when Confluence would be updated and stop using
        // PluginResourceLocator.getDownloadableResource
        // See https://ecosystem.atlassian.net/browse/PLUGWEB-193

        // For unknown reason Confluence could pass null as query params.
        if (queryParams == null) {
            queryParams = new HashMap<>();
        }

        final DownloadableResource[] downloadableResource = new DownloadableResource[] {null};
        Router router = new Router(globals) {
            @Override
            protected Controller createController(
                    final Globals globals, final Request request, final Response response) {
                return new Controller(globals, request, response) {
                    @Override
                    protected void sendCached(
                            final Content content, Map<String, String> params, final boolean isCachingEnabled) {
                        downloadableResource[0] = asDownloadableResource(
                                new ContentImpl(content.getContentType(), content.isTransformed()) {
                                    @Override
                                    public ReadableSourceMap writeTo(
                                            final OutputStream out, final boolean isSourceMapEnabled) {
                                        Cache cache =
                                                isCachingEnabled ? globals.getContentCache() : new PassThroughCache();
                                        cache.cache(
                                                "http",
                                                request.getUrl(),
                                                out,
                                                producerOut -> content.writeTo(producerOut, false));
                                        return null;
                                    }
                                });
                    }

                    @Override
                    protected boolean checkIfCachedAndNotModified(final Date updatedAt) {
                        return false;
                    }
                };
            }
        };
        router.dispatch(new Request(globals, url, queryParams), null);
        return downloadableResource[0];
    }

    public Globals getGlobals() {
        return globals;
    }

    private void fireServerResourceCacheInvalidationEvent(
            @Nonnull final ServerResourceCacheInvalidationCause eventCause) {
        if (globals.getConfig().isPerformanceTrackingEnabled()) {
            publishIfEventPublisherNonNull(
                    globals.getEventPublisher(), new ServerResourceCacheInvalidationEvent(eventCause));
        }
    }
}
