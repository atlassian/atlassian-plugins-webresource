package com.atlassian.plugin.webresource.impl;

import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

/**
 * Key to identify Resource.
 */
public class ResourceKey {
    private final String key;
    private final String name;

    public ResourceKey(final Resource resource) {
        this(resource.getKey(), resource.getName());
    }

    public ResourceKey(final String key, final String name) {
        this.key = key;
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }
}
