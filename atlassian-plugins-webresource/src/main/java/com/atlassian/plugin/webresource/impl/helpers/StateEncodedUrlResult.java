package com.atlassian.plugin.webresource.impl.helpers;

import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;

public class StateEncodedUrlResult {
    private final DefaultUrlBuilder urlBuilder;

    public StateEncodedUrlResult(final DefaultUrlBuilder urlBuilder) {
        this.urlBuilder = urlBuilder;
    }

    public DefaultUrlBuilder getUrlBuilder() {
        return urlBuilder;
    }
}
