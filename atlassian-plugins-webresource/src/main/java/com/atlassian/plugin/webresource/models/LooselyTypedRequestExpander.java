package com.atlassian.plugin.webresource.models;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toCollection;

/**
 * @deprecated A short-term refactoring measure that puts a strong type around poor ones.
 * Any usage of this class should be replaced with {@link NormalizedRequest}.
 */
public class LooselyTypedRequestExpander {
    private final RawRequest request;

    public LooselyTypedRequestExpander(RawRequest r) {
        request = r;
    }

    /**
     * Interprets the request and determines a smaller equivalent request to make.
     *
     * e.g., if context A and B are requested, but context A is also excluded, the request will normalise to just B.
     *
     * At the moment, this logic DOES NOT expand requestable items in to smaller equivalents.
     * e.g., if context `A` houses web-resources `foo` and `bar`, a request for A will not be considered
     * equivalent to a request for `foo,bar` or `bar,foo`.
     */
    public LooselyTypedRequestExpander normalise() {
        /*
         @todo move this normalisation logic in to {@link NormalizedRequest} class. pay attention to
               how names of contexts or root pages are expanded in to more base types.
        */
        RawRequest raw = new RawRequest();
        request.getIncluded().stream()
                .filter(Objects::nonNull)
                .filter(item -> !request.getExcluded().contains(item))
                .forEach(raw::include);

        request.getExcluded().stream()
                .filter(Objects::nonNull)
                .filter(item -> !request.getIncluded().contains(item))
                .forEach(raw::exclude);

        return new LooselyTypedRequestExpander(raw);
    }

    public Set<String> getIncluded() {
        return request.getIncluded().stream().map(this::convert).collect(toCollection(LinkedHashSet::new));
    }

    public Set<String> getExcluded() {
        return request.getExcluded().stream().map(this::convert).collect(toCollection(LinkedHashSet::new));
    }

    private String convert(Requestable r) {
        return r.toLooseType();
    }
}
