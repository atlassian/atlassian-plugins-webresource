package com.atlassian.plugin.webresource.util;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;

import com.atlassian.webresource.api.assembler.resource.CompleteWebResourceKey;

public class WebResourceKeyHelper {
    static final Pattern simpleWebResourceKeyPattern = Pattern.compile("[^:]*:[^:]+");

    public static boolean isWebResourceKey(String key) {
        return key != null
                && simpleWebResourceKeyPattern
                        .matcher(key)
                        .find(); // Please note the plugin key may be empty to mean "the same as me" in a "simple" key
    }

    public static Optional<CompleteWebResourceKey> createWebResourceKey(@Nonnull String completeKey) {
        Objects.requireNonNull(completeKey, "Complete key cannot be null");

        String[] parts = completeKey.split(":");
        var isValid = parts.length == 2 && !parts[0].isBlank() && !parts[1].isBlank();

        return isValid ? Optional.of(new CompleteWebResourceKey(parts[0], parts[1])) : Optional.empty();
    }
}
