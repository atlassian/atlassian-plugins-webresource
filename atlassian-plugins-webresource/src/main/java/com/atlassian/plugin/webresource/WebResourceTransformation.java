package com.atlassian.plugin.webresource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.transformer.ContentTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.DefaultTransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.UrlReadingContentTransformer;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.TransformationDto;
import com.atlassian.webresource.spi.TransformerDto;
import com.atlassian.webresource.spi.transformer.TwoPhaseResourceTransformer;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Collections.emptyMap;
import static java.util.Objects.requireNonNull;

import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.asContent;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.asDownloadableResource;

/**
 * Represents a set of transformer invocations for a specific web resource set and extension.  Transformers are retrieved
 * from the plugin system on request, not plugin initialisation, since plugin start order is indeterminate.
 *
 * @since 2.5.0
 */
public class WebResourceTransformation {
    private final String extension;
    private final String type;
    private final Map<String, Element> transformerElements;
    private final Iterable<String> transformerKeys;
    private final Logger log = LoggerFactory.getLogger(WebResourceTransformation.class);

    public WebResourceTransformation(TransformationDto transformationDto) {
        requireNonNull(transformationDto.extension);

        type = transformationDto.extension;
        extension = "." + type;

        List<String> keys = new ArrayList<>();
        for (TransformerDto transformerDto : transformationDto.transformers) {
            keys.add(transformerDto.key);
        }
        transformerKeys = keys;
        transformerElements = emptyMap();
    }

    public WebResourceTransformation(Element element) {
        checkArgument(element.attributeValue("extension") != null, "extension");

        this.type = element.attributeValue("extension");
        this.extension = "." + type;

        LinkedHashMap<String, Element> transformers = new LinkedHashMap<>();
        for (Element transformElement : element.elements("transformer")) {
            transformers.put(transformElement.attributeValue("key"), transformElement);
        }
        transformerElements = Collections.unmodifiableMap(transformers);
        transformerKeys = transformerElements.keySet();
    }

    // TODO there are two matches - one that accepts ResourceLocation and another that accepts the type (as String).
    // And, they not exactly the same, because there are cases when transformers matched against
    //     "some-name.public.js".matches("public.js")
    // so, how it works isn't very obvious, maybe we should deprecate or change it.
    // See https://ecosystem.atlassian.net/browse/PLUGWEB-195

    public boolean matches(ResourceLocation location) {
        String loc = location.getLocation();
        if (loc == null || "".equals(loc.trim())) {
            loc = location.getName();
        }
        return loc.endsWith(extension);
    }

    public boolean matches(String locationType) {
        return locationType.equals(type);
    }

    public void addTransformParameters(
            TransformerCache transformerCache,
            TransformerParameters transformerParameters,
            UrlBuilder urlBuilder,
            UrlBuildingStrategy urlBuildingStrategy) {
        for (String key : transformerKeys) {
            Object descriptorAsObject = transformerCache.getDescriptor(key);
            if ((descriptorAsObject != null)) {
                if (descriptorAsObject instanceof UrlReadingWebResourceTransformerModuleDescriptor descriptor) {
                    urlBuildingStrategy.addToUrl(
                            descriptor.getModule().makeUrlBuilder(transformerParameters), urlBuilder);
                } else if (descriptorAsObject instanceof ContentTransformerModuleDescriptor descriptor) {
                    urlBuildingStrategy.addToUrl(
                            descriptor.getModule().makeUrlBuilder(transformerParameters), urlBuilder);
                } else {
                    throw new RuntimeException(
                            "invalid usage, transformer descriptor expected but got " + descriptorAsObject);
                }
            }
        }
    }

    public Content transform(
            final CdnResourceUrlTransformer cdnResourceUrlTransformer,
            final TransformerCache transformerCache,
            Resource resource,
            final Content content,
            ResourceLocation resourceLocation,
            final QueryParams params,
            final String sourceUrl) {
        Content lastContent = content;

        for (String transformerKey : transformerKeys) {
            Object descriptorAsObject = transformerCache.getDescriptor(transformerKey);
            if (descriptorAsObject != null) {
                if (descriptorAsObject instanceof ContentTransformerModuleDescriptor descriptor) {
                    UrlReadingContentTransformer transformer = descriptor
                            .getModule()
                            .makeResourceTransformer(resource.getParent().getTransformerParameters());
                    // Provide properties to transformers that can do something with them
                    if (transformer instanceof TwoPhaseResourceTransformer) {
                        ((TwoPhaseResourceTransformer) transformer)
                                .loadTwoPhaseProperties(resourceLocation, resource::getStreamFor);
                    }
                    // Perform the content transform
                    lastContent = transformer.transform(
                            cdnResourceUrlTransformer, lastContent, resourceLocation, params, sourceUrl);
                } else {
                    Element configElement = transformerElements.get(transformerKey);
                    if (descriptorAsObject instanceof UrlReadingWebResourceTransformerModuleDescriptor descriptor) {
                        DefaultTransformableResource transformableResource =
                                new DefaultTransformableResource(resourceLocation, asDownloadableResource(lastContent));
                        UrlReadingWebResourceTransformer transformer = descriptor
                                .getModule()
                                .makeResourceTransformer(resource.getParent().getTransformerParameters());
                        // Provide properties to transformers that can do something with them
                        if (transformer instanceof TwoPhaseResourceTransformer) {
                            ((TwoPhaseResourceTransformer) transformer)
                                    .loadTwoPhaseProperties(resourceLocation, resource::getStreamFor);
                        }
                        // Perform the web-resource transform
                        lastContent = asContent(transformer.transform(transformableResource, params), null, true);
                    } else {
                        throw new RuntimeException(
                                "invalid usage, transformer descriptor expected but got " + descriptorAsObject);
                    }
                }
            } else {
                log.warn(
                        "Web resource transformer {} not found for resource {}, skipping",
                        transformerKey,
                        resourceLocation.getName());
            }
        }
        return lastContent;
    }

    public String toString() {
        return getClass().getSimpleName() + " with " + transformerKeys.toString() + " transformers";
    }

    public boolean addAllUsedQueryParameters(Set<String> paramKeys, TransformerCache transformerCache) {
        for (String key : transformerKeys) {
            Object descriptorAsObject = transformerCache.getDescriptor(key);
            if ((descriptorAsObject != null)) {
                Set<String> transformerParamKeys;

                if (descriptorAsObject instanceof UrlReadingWebResourceTransformerModuleDescriptor descriptor) {
                    transformerParamKeys = descriptor.getModule().allUsedQueryParameters();
                } else if (descriptorAsObject instanceof ContentTransformerModuleDescriptor descriptor) {
                    transformerParamKeys = descriptor.getModule().allUsedQueryParameters();
                } else {
                    throw new RuntimeException(
                            "invalid usage, transformer descriptor expected but got " + descriptorAsObject);
                }

                if (transformerParamKeys != null) {
                    paramKeys.addAll(transformerParamKeys);
                } else {
                    return false;
                }
            }
        }

        return true;
    }
}
