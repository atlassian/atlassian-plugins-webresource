package com.atlassian.plugin.webresource.assembler.html;

import java.io.IOException;
import java.io.Writer;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.assembler.ResourceUrls;

import static java.util.Objects.requireNonNull;

import static com.atlassian.plugin.webresource.impl.support.Support.LOGGER;

class SwallowErrorsWriter {
    private static final String ERROR_TAG_BEGIN = "<!-- Error loading resource \"";
    private static final String ERROR_TAG_END = "<!-- Error loading resource \"";
    private static final String NOT_FOUND_ERROR_MESSAGE = "\".  No resource formatter matches \"";

    private final Writer writer;

    SwallowErrorsWriter(@Nonnull final Writer writer) {
        this.writer = requireNonNull(writer, "The writer is mandatory for the creation of SwallowErrorsWriter.");
    }

    void write(@Nonnull final ResourceUrls resourceUrls) {
        requireNonNull(resourceUrls, "The resource urls are mandatory for the creation of the tags.");
        write(
                ERROR_TAG_BEGIN,
                resourceUrls.getResourceUrl().getKey(),
                NOT_FOUND_ERROR_MESSAGE,
                resourceUrls.getResourceUrl().getName(),
                ERROR_TAG_END);
    }

    void write(@Nonnull final String... contents) {
        requireNonNull(contents, "The contents are mandatory.");
        try {
            for (final String content : contents) {
                writer.write(content);
            }
        } catch (final IOException exception) {
            LOGGER.error("IOException encountered rendering resource", exception);
        }
    }
}
