package com.atlassian.plugin.webresource.analytics.events;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.event.api.AsynchronousPreferred;

@AsynchronousPreferred
@EventName("wrm.caching.resource.invalidation.server")
public class ServerResourceCacheInvalidationEvent {
    private static final int EVENT_VERSION = 1;

    private final ServerResourceCacheInvalidationCause cacheInvalidationCause;

    public ServerResourceCacheInvalidationEvent(ServerResourceCacheInvalidationCause resourceCacheInvalidationCause) {
        cacheInvalidationCause = resourceCacheInvalidationCause;
    }

    public int getEventVersion() {
        return EVENT_VERSION;
    }

    public String getCause() {
        return cacheInvalidationCause.toString();
    }
}
