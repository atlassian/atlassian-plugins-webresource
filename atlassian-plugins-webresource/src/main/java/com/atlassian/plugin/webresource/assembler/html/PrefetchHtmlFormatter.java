package com.atlassian.plugin.webresource.assembler.html;

import java.util.EnumMap;
import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.assembler.ResourceUrls;
import com.atlassian.webresource.api.UrlMode;

import static java.util.Objects.requireNonNull;

import static com.atlassian.plugin.webresource.PrefetchLinkWebResource.FORMATTER;
import static com.atlassian.webresource.api.UrlMode.values;

/**
 * Wrapper responsible for providing the right {@link PrefetchHtmlFormatter} according
 *
 * to each URL type and generating an URL as {@code <link rel="prefetch" href="theurl">}.
 *
 * @since 5.0.0
 */
public final class PrefetchHtmlFormatter implements HtmlTagFormatter {

    private static final Map<UrlMode, PrefetchHtmlFormatter> FORMATTERS = new EnumMap<>(UrlMode.class);

    static {
        for (final UrlMode urlMode : values()) {
            FORMATTERS.put(urlMode, new PrefetchHtmlFormatter(urlMode));
        }
    }

    private final UrlMode urlMode;

    private PrefetchHtmlFormatter(@Nonnull final UrlMode urlMode) {
        this.urlMode = requireNonNull(urlMode, "The url mode is mandatory to build a prefetch formatter.");
    }

    @Nonnull
    public static PrefetchHtmlFormatter getInstance(@Nonnull final UrlMode urlMode) {
        requireNonNull(
                urlMode, "The url mode is mandatory for the retriaval of the instance of PrefetchHtmlFormatter.");
        return FORMATTERS.get(urlMode);
    }

    @Nonnull
    @Override
    public String format(@Nonnull final ResourceUrls resourceUrls) {
        requireNonNull(resourceUrls, "The resource urls are mandatory for the creation of the script tag");
        return FORMATTER.formatResource(
                resourceUrls.getPluginUrlResource().getStaticUrl(urlMode),
                resourceUrls.getPluginUrlResource().getParams().all());
    }

    @Override
    public boolean matches(@Nonnull final String resourceName) {
        requireNonNull(resourceName, "The resource name is mandatory for the comparison.");
        return FORMATTER.matches(resourceName);
    }
}
