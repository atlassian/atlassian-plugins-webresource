package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream;

import java.io.InputStream;

public interface StreamStrategy {
    InputStream getInputStream(String path);
}
