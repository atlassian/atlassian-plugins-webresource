package com.atlassian.plugin.webresource.impl.config;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.MoreObjects;
import io.atlassian.util.concurrent.ResettableLazyReference;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.AbstractFileServerServlet;
import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.PluginResourceContainer;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.ResourceUtils;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformerImpl;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.graph.DependencyGraph;
import com.atlassian.plugin.webresource.graph.DependencyGraphBuilder;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.annotators.ListOfAnnotators;
import com.atlassian.plugin.webresource.impl.annotators.LocationContentAnnotator;
import com.atlassian.plugin.webresource.impl.annotators.ResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.annotators.SemicolonResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.annotators.TryCatchJsResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Context;
import com.atlassian.plugin.webresource.impl.snapshot.Deprecation;
import com.atlassian.plugin.webresource.impl.snapshot.RootPage;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.plugin.webresource.impl.snapshot.WebResource;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.impl.snapshot.resource.ResourceFactory;
import com.atlassian.plugin.webresource.impl.support.ConditionInstanceCache;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.models.Requestable;
import com.atlassian.plugin.webresource.models.SuperBatchKey;
import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.transformer.DefaultTransformerParameters;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.util.HashBuilder;
import com.atlassian.sourcemap.Util;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.api.assembler.resource.CompleteWebResourceKey;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.atlassian.webresource.api.transformer.TransformerParameters;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.sort;

import static com.atlassian.plugin.webresource.Flags.getFileCacheSize;
import static com.atlassian.plugin.webresource.Flags.isDevMode;
import static com.atlassian.plugin.webresource.Flags.isFileCacheEnabled;
import static com.atlassian.plugin.webresource.impl.support.http.BaseRouter.joinWithSlashWithoutEmpty;
import static com.atlassian.plugin.webresource.util.WebResourceKeyHelper.isWebResourceKey;
import static com.atlassian.sal.api.features.DarkFeatureManager.ATLASSIAN_DARKFEATURE_PREFIX;

/**
 * Unites all the configurations in one place. Not exposed as API, so it is possible to change it and store settings
 * that should not be exposed publicly.
 *
 * @since 3.3
 */
public class Config {

    public static final String SOURCE_PARAM_NAME = "source";
    public static final String BATCH_PARAM_NAME = "batch";
    public static final String MEDIA_PARAM_NAME = "media";
    public static final String CONTENT_TYPE_PARAM_NAME = "content-type";
    public static final String CACHE_PARAM_NAME = "cache";
    public static final String ALLOW_PUBLIC_USE_PARAM_NAME = "allow-public-use";
    public static final String ASYNC_SCRIPT_PARAM_NAME = "async";
    public static final String DEFER_SCRIPT_PARAM_NAME = "defer";
    public static final String INITIAL_RENDERED_SCRIPT_PARAM_NAME = "data-initially-rendered";
    public static final String WRM_KEY_PARAM_NAME = "data-wrm-key";
    public static final String WRM_BATCH_TYPE_PARAM_NAME = "data-wrm-batch-type";

    public static final String DOWNLOAD_PARAM_VALUE = "download";

    public static final String JS_TYPE = "js";
    public static final String JS_CONTENT_TYPE = "application/javascript";
    public static final String CSS_TYPE = "css";
    public static final String CSS_CONTENT_TYPE = "text/css";
    public static final String LESS_TYPE = "less";
    public static final String SOY_TYPE = "soy";
    public static final String CSS_EXTENSION = ".css";
    public static final String LESS_EXTENSION = ".less";
    public static final String JS_EXTENSION = ".js";
    public static final String SOY_EXTENSION = ".soy";
    public static final String[] BATCH_TYPES = new String[] {CSS_TYPE, JS_TYPE};
    public static final int GET_URL_MAX_LENGTH = 8 * 1024;
    public static final String[] HTTP_PARAM_NAMES = {
        MEDIA_PARAM_NAME, CONTENT_TYPE_PARAM_NAME, CACHE_PARAM_NAME, ALLOW_PUBLIC_USE_PARAM_NAME
    };
    public static final List<String> PARAMS_SORT_ORDER = asList(CACHE_PARAM_NAME, MEDIA_PARAM_NAME);
    public static final String CONTEXT_PREFIX = "_context";
    public static final String SUPER_BATCH_CONTEXT_KEY = "_super";
    public static final String SUPERBATCH_KEY = Config.CONTEXT_PREFIX + ":" + SUPER_BATCH_CONTEXT_KEY;
    public static final String SYNCBATCH_CONTEXT_KEY = "_sync";
    public static final String SYNCBATCH_KEY = Config.CONTEXT_PREFIX + ":" + SYNCBATCH_CONTEXT_KEY;
    public static final String INCREMENTAL_CACHE_SIZE = "plugin.webresource.incrementalcache.size";
    private static final String DISABLE_MINIFICATION = "atlassian.webresource.disable.minification";
    private static final String DISABLE_URL_CACHING = "atlassian.webresource.disable.url.caching";
    private static final String STATIC_CONTEXT_ORDER_ENABLED = "atlassian.webresource.stable.contexts.enable";

    @VisibleForTesting
    public static final String ENABLE_BUNDLE_HASH_VALIDATION = "atlassian.webresource.enable.bundle.hash.validation";

    @VisibleForTesting
    // Also used on some of WRM's web-resource definitions
    public static final String DISABLE_PERFORMANCE_TRACKING = "atlassian.webresource.performance.tracking.disable";

    private static final String COMPILED_RESOURCE_LOCATION_PARAMETER = "compiledLocation";
    private static final Pattern SNAPSHOT_PLUGIN_REGEX = Pattern.compile("SNAPSHOT", Pattern.CASE_INSENSITIVE);
    private static final Logger log = LoggerFactory.getLogger(Config.class);
    private final WebResourceIntegration integration;
    private final ResourceBatchingConfiguration batchingConfiguration;
    private final WebResourceUrlProvider urlProvider;
    private final boolean isContentCacheEnabled;
    private final int contentCacheSize;
    private final Integer incrementalCacheSize;
    private final boolean usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins;
    private final TransformerCache transformerCache;
    private final ResourceFactory resourceFactory;
    private static ResettableLazyReference<DependencyGraph<Requestable>> dependencyGraph;

    private final CdnResourceUrlTransformer cdnResourceUrlTransformer;
    private final int urlCacheSize;
    private final boolean isUrlCachingEnabled;
    private Logger supportLogger = Support.LOGGER;
    private ContentTypeResolver contentTypeResolver;
    private StaticTransformers staticTransformers;
    private Optional<Boolean> syncbatchCreated = Optional.empty();
    private Optional<Boolean> superbatchCreated = Optional.empty();

    public Config(
            ResourceBatchingConfiguration batchingConfiguration,
            WebResourceIntegration integration,
            WebResourceUrlProvider urlProvider,
            ServletContextFactory servletContextFactory,
            TransformerCache transformerCache) {
        this.batchingConfiguration = batchingConfiguration;
        this.integration = integration;
        this.urlProvider = urlProvider;
        this.transformerCache = transformerCache;
        this.isContentCacheEnabled = isFileCacheEnabled();
        this.contentCacheSize = getFileCacheSize(1000);
        this.isUrlCachingEnabled = !Boolean.getBoolean(DISABLE_URL_CACHING);
        this.urlCacheSize = getFileCacheSize(200);
        this.incrementalCacheSize = Integer.getInteger(INCREMENTAL_CACHE_SIZE, 1000);
        cdnResourceUrlTransformer = new CdnResourceUrlTransformerImpl(this);
        this.usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins =
                integration.usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins();
        this.resourceFactory = new ResourceFactory(servletContextFactory, integration);
        dependencyGraph = new ResettableLazyReference<DependencyGraph<Requestable>>() {
            @Override
            protected DependencyGraph<Requestable> create() throws Exception {
                return DependencyGraph.builder().build();
            }
        };
    }

    /**
     * If the key is context or web resource key.
     */
    public static boolean isContextKey(String key) {
        return key.contains(CONTEXT_PREFIX);
    }

    /**
     * Convert virtual context key to web resource key.
     */
    public static String virtualContextKeyToWebResourceKey(String virtualContextKey) {
        return virtualContextKey.replace(CONTEXT_PREFIX + ":", "");
    }

    protected static List<ResourceLocation> getResourceLocations(
            WebResourceModuleDescriptor webResourceModuleDescriptor, boolean compiledResourcesEnabled) {
        List<ResourceLocation> resourceDescriptors = new ArrayList<>();
        for (ResourceDescriptor resourceDescriptor : webResourceModuleDescriptor.getResourceDescriptors()) {
            if (DOWNLOAD_PARAM_VALUE.equals(resourceDescriptor.getType())) {
                ResourceLocation resourceLocation = resourceDescriptor.getResourceLocationForName(null);

                // Perform boolean check before checking for XML property.
                if (compiledResourcesEnabled) {
                    String compiledLocation = resourceDescriptor.getParameter(COMPILED_RESOURCE_LOCATION_PARAMETER);
                    if (compiledLocation != null) {
                        ResourceLocation newLocation = new ResourceLocation(
                                compiledLocation,
                                resourceLocation.getName(),
                                resourceLocation.getType(),
                                resourceLocation.getContentType(),
                                resourceLocation.getContent(),
                                resourceLocation.getParams());
                        resourceLocation = newLocation;
                    }
                }
                resourceDescriptors.add(resourceLocation);
            }
        }
        return resourceDescriptors;
    }

    /**
     * Determines if batch URLs contexts' are sorted to ensure that the same URL is generated no matter what order the
     * contexts are required in.
     *
     * @since 3.6.0
     */
    public static boolean isStaticContextOrderEnabled() {
        return Boolean.getBoolean(STATIC_CONTEXT_ORDER_ENABLED);
    }

    /**
     * For SNAPSHOT plugins will return install time instead of the version.
     * Needed to simplify working with SNAPSHOT plugins and flushing cache when new SNAPSHOT plugin installed
     * with same version but different content.
     */
    public static String getPluginVersionOrInstallTime(
            Plugin plugin, boolean usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins) {
        PluginInformation pluginInfo = plugin.getPluginInformation();
        // Seems like we need to guard against pluginInfo being null, this check has been taken from
        // `WebResourceUrlProviderImpl.getStaticPluginResourceUrl` in WRM@3.5.9.
        final String version = pluginInfo != null ? pluginInfo.getVersion() : "unknown";
        if (usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins
                && SNAPSHOT_PLUGIN_REGEX.matcher(version).find()) {
            final Date loadedAt = plugin.getDateLoaded() == null ? new Date() : plugin.getDateLoaded();
            return version + "-" + loadedAt.getTime();
        } else {
            return version;
        }
    }

    /**
     * Needed because `contentTypeResolver` isn't available at the time Config is created and set up a bit later.
     */
    public void setContentTypeResolver(ContentTypeResolver contentTypeResolver) {
        if (this.contentTypeResolver != null) {
            throw new RuntimeException("content type resolver already set!");
        }
        this.contentTypeResolver = contentTypeResolver;
    }

    /**
     * @return if cache should be enabled.
     */
    public boolean isContentCacheEnabled() {
        return isContentCacheEnabled;
    }

    /**
     * @return amount of files to be stored in cache.
     */
    public int getContentCacheSize() {
        return contentCacheSize;
    }

    /**
     * @return amount of files to be stored in cache.
     */
    public int getIncrementalCacheSize() {
        return incrementalCacheSize;
    }

    /**
     * @return directory where cache content would be stored as files.
     */
    public File getCacheDirectory() {
        return integration.getTemporaryDirectory();
    }

    @Nonnull
    public static DependencyGraph<Requestable> getDependencyGraph() {
        return dependencyGraph.get();
    }

    /**
     * @param typeOrContentType type or content type.
     * @return if source map enabled for this type or content type.
     */
    public boolean isSourceMapEnabledFor(String typeOrContentType) {
        return isSourceMapEnabled() && Util.isSourceMapSupportedBy(typeOrContentType);
    }

    /**
     * @return if source map enabled.
     */
    public boolean isSourceMapEnabled() {
        return batchingConfiguration.isSourceMapEnabled();
    }

    /**
     * @return if CDN is globally enabled.
     */
    public boolean isCdnEnabled() {
        return null != integration.getCDNStrategy()
                && integration.getCDNStrategy().supportsCdn();
    }

    /**
     * @return base url, where web resources is mounted to.
     */
    public String getBaseUrl() {
        return getBaseUrl(true);
    }

    /**
     * @param isAbsolute if url should be absolute or relative.
     * @return base url, where web resources is mounted to.
     */
    public String getBaseUrl(boolean isAbsolute) {
        try {
            return joinWithSlashWithoutEmpty(
                    urlProvider.getBaseUrl(isAbsolute ? UrlMode.ABSOLUTE : UrlMode.RELATIVE),
                    AbstractFileServerServlet.SERVLET_PATH);
        } catch (AssertionError e) {
            // For unknown reason some links in Confluence (like /confluence/setup/setupstart.action ) doesn't
            // support the absolute base url, catching such cases and trying to use relative base url.
            if (isAbsolute && e.getMessage().contains("Unsupported URLMode")) {
                return getBaseUrl(false);
            } else {
                throw e;
            }
        }
    }

    /**
     * @return url prefix for resource, including base url.
     */
    public String getResourceUrlPrefix(String hash, String bundleHash, boolean isAbsolute) {
        return urlProvider.getStaticResourcePrefix(hash, bundleHash, (isAbsolute ? UrlMode.ABSOLUTE : UrlMode.RELATIVE))
                + "/" + AbstractFileServerServlet.SERVLET_PATH;
    }

    /**
     * @return url prefixed with CDN prefix.
     */
    public String getResourceCdnPrefix(final String url) {
        return integration.getCDNStrategy().transformRelativeUrl(url);
    }

    /**
     * @return content type for given path.
     */
    public String getContentType(String path) {
        return contentTypeResolver.getContentType(path);
    }

    /**
     * @return content annotators for given type.
     */
    public ResourceContentAnnotator getContentAnnotator(String type) {
        List<ResourceContentAnnotator> annotators = new ArrayList<>();
        if (JS_TYPE.equals(type)) {
            annotators.add(new SemicolonResourceContentAnnotator());
            if (isJavaScriptTryCatchWrappingEnabled()) {
                annotators.add(new TryCatchJsResourceContentAnnotator());
            }
            annotators.add(new LocationContentAnnotator());
        } else if (CSS_TYPE.equals(type)) {
            annotators.add(new LocationContentAnnotator());
        }
        return new ListOfAnnotators(annotators);
    }

    /**
     * @since 3.5.27
     */
    private List<CompleteWebResourceKey> getSyncWebResourceKeys() {
        return new ArrayList<>(MoreObjects.firstNonNull(integration.getSyncWebResourceKeys(), emptyList()));
    }

    /**
     * @return static transformers.
     */
    public StaticTransformers getStaticTransformers() {
        return staticTransformers;
    }

    public void setStaticTransformers(StaticTransformers staticTransformers) {
        if (this.staticTransformers != null) {
            throw new RuntimeException("static transformers already set!");
        }
        this.staticTransformers = staticTransformers;
    }

    /**
     * @return true if sync context has been created (is not empty)
     * @throws RuntimeException if called before {@link #getWebResourcesWithoutCache()}.
     * @since 3.5.27
     */
    public boolean isSyncContextCreated() {
        return syncbatchCreated.orElseThrow(() -> new RuntimeException("Called before getWebResourcesWithoutCache()"));
    }

    /**
     * @return true if superbatch context has been created (is not empty)
     * @throws RuntimeException if called before {@link #getWebResourcesWithoutCache()}.
     * @since 5.5.0
     */
    public boolean isSuperbatchCreated() {
        return superbatchCreated.orElseThrow(() -> new RuntimeException("Called before getWebResourcesWithoutCache()"));
    }

    /**
     * Analyzes current state of the plugin system and return web resources.
     */
    public Snapshot getWebResourcesWithoutCache() {
        final Map<WebResource, CachedTransformers> webResourcesTransformations = new HashMap<>();
        final Map<String, Bundle> cachedBundles = new HashMap<>();
        final Map<String, RootPage> rootPages = new HashMap<>();
        final Map<WebResource, CachedCondition> webResourcesCondition = new HashMap<>();
        final Map<String, Deprecation> webResourceDeprecationWarnings = new HashMap<>();
        final Set<WebResource> webResourcesWithLegacyConditions = new HashSet<>();
        final Snapshot snapshot = new Snapshot(
                this,
                cachedBundles,
                rootPages,
                webResourcesTransformations,
                webResourcesCondition,
                webResourcesWithLegacyConditions);

        final Map<String, IntermediaryContextData> intermediaryContexts = new HashMap<>();
        final ConditionInstanceCache conditionInstanceCache = new ConditionInstanceCache();

        // Collecting web resource descriptors.
        final List<WebResourceModuleDescriptor> webResourceDescriptors =
                integration.getPluginAccessor().getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class);

        // Collect deprecation details before processing all descriptors further.
        // We need to collect all deprecation details beforehand, otherwise some usages of
        // deprecated code will not be discovered.
        for (WebResourceModuleDescriptor webResourceDescriptor : webResourceDescriptors) {
            if (webResourceDescriptor == null) {
                continue;
            }
            if (webResourceDescriptor.isDeprecated()) {
                final String completeKey = webResourceDescriptor.getCompleteKey();
                webResourceDeprecationWarnings.put(completeKey, webResourceDescriptor.getDeprecation());
            }
        }

        // Processing descriptors and building web resources graph.
        for (WebResourceModuleDescriptor webResourceDescriptor : webResourceDescriptors) {
            if (webResourceDescriptor == null) {
                continue;
            }
            // Checks if the current web-resource is tagged as a root-page
            boolean isRootPage = webResourceDescriptor.isRootPage();

            Plugin plugin = webResourceDescriptor.getPlugin();
            Date updatedAt = (plugin.getDateLoaded() == null) ? new Date() : plugin.getDateLoaded();

            final String completeKey = webResourceDescriptor.getCompleteKey();

            // Detecting location resource types, needed during URL generation to detect all the transformers
            // that should be applied.
            Map<String, Set<String>> locationResourceTypes = new HashMap<>();
            for (ResourceLocation resourceLocation :
                    getResourceLocations(webResourceDescriptor, integration.isCompiledResourceEnabled())) {
                String nameType = ResourceUtils.getType(resourceLocation.getName());
                String locationType = ResourceUtils.getType(resourceLocation.getLocation());
                String nameOrLocationType = nameType.isEmpty() ? locationType : nameType;
                Set<String> list = locationResourceTypes.get(nameOrLocationType);
                if (list == null) {
                    list = new HashSet<>();
                    locationResourceTypes.put(nameOrLocationType, list);
                }
                list.add(locationType);
            }

            // Adding dependencies, forbidding to use module and root-page names in web resource dependencies.
            List<String> dependencies = new ArrayList<>();
            for (final String key : webResourceDescriptor.getDependencies()) {
                if (rootPages.containsKey(key)) {
                    String msg = "invalid dependency found for \"" + completeKey + "\": \""
                            + key + "\" cannot be as a dependency because it is tagged as a root-page and "
                            + "will be ignored.";
                    supportLogger.error(msg);
                    continue;
                }
                if (!isWebResourceKey(key)) {
                    supportLogger.warn("the dependency \"" + key
                            + "\" doesn't look like the key of the web resource and will be ignored.");
                    continue;
                }
                if (webResourceDeprecationWarnings.containsKey(key)) {
                    String msg = webResourceDeprecationWarnings.get(key).buildLogMessage() + " (required by \""
                            + completeKey + "\")";
                    if (isDevMode()) {
                        supportLogger.warn(msg);
                    } else {
                        supportLogger.debug(msg);
                    }
                }
                dependencies.add(key);
            }

            // Adding context dependencies. At the moment this is allowed behaviour only for root pages, but
            // will be expanded to all web-resources once APDEX-923 is complete
            for (String key : webResourceDescriptor.getContextDependencies()) {
                if (!isRootPage) {
                    String msg = "ignoring dependency \"" + key + "\" in \"" + completeKey
                            + "\": context dependencies are only supported in root-pages at the moment";
                    supportLogger.error(msg);
                    continue;
                }
                if (isWebResourceKey(key)) {
                    supportLogger.warn("the context dependency \"" + key
                            + "\" look like the key of the web resource and will be ignored.");
                    continue;
                }
                dependencies.add(CONTEXT_PREFIX + ":" + key);
            }

            // Adding web resource.
            TransformerParameters transformerParameters = new DefaultTransformerParameters(
                    webResourceDescriptor.getPluginKey(), webResourceDescriptor.getKey());
            final WebResource webResource = new WebResource(
                    snapshot,
                    completeKey,
                    dependencies,
                    updatedAt,
                    getPluginVersionOrInstallTime(plugin, usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins),
                    true,
                    transformerParameters,
                    locationResourceTypes);
            cachedBundles.put(completeKey, webResource);

            // Adding root-page
            if (isRootPage) {
                RootPage rootPage = new RootPage(webResource);
                rootPages.put(completeKey, rootPage);
            }

            // Forbidding contexts on <root-page> web-resources, except for the implicit context that corresponds to the
            // resource itself
            if (isRootPage && webResourceDescriptor.getContexts().size() > 1) {
                String msg = "web-resource \"" + completeKey
                        + "\" cannot be added to a context because it's tagged as a root-page";
                supportLogger.error(msg);
                throw new RuntimeException(msg);
            }

            // Collecting dependencies and dates for contexts, we would need this information later when transform
            // contexts into
            // virtual web resources.
            for (String context : webResourceDescriptor.getContexts()) {
                if (!context.equals(completeKey)) {
                    String contextResourceKey = CONTEXT_PREFIX + ":" + context;
                    IntermediaryContextData contextData = intermediaryContexts.get(contextResourceKey);
                    if (contextData == null) {
                        contextData = new IntermediaryContextData();
                        intermediaryContexts.put(contextResourceKey, contextData);
                    }
                    contextData.dependencies.add(completeKey);
                }
            }

            // Adding conditions.
            DecoratingCondition condition = webResourceDescriptor.getCondition();
            if (condition != null) {
                // Trying to optimize conditions and replace multiple instances of the same UrlReadingCondition with
                // one instance.
                CachedCondition cachedCondition = conditionInstanceCache.intern(condition);
                webResourcesCondition.put(webResource, cachedCondition);
            }

            // Adding transformers.
            List<WebResourceTransformation> transformations = webResourceDescriptor.getTransformations();
            if (!transformations.isEmpty()) {
                webResourcesTransformations.put(webResource, new CachedTransformers(transformations));
            }
        }

        // Turning contexts into virtual resources.
        for (Map.Entry<String, IntermediaryContextData> entry : intermediaryContexts.entrySet()) {
            String contextResourceKey = entry.getKey();
            IntermediaryContextData contextData = entry.getValue();
            updateContextData(cachedBundles, contextData);
            cachedBundles.put(
                    contextResourceKey,
                    new Context(
                            snapshot,
                            contextResourceKey,
                            contextData.dependencies,
                            contextData.updatedAt,
                            contextData.version,
                            true));
        }

        syncbatchCreated = Optional.of(constructSyncbatch(snapshot, cachedBundles));
        superbatchCreated = Optional.of(constructSuperbatch(snapshot, cachedBundles));

        // Rebuild the entire dependency graph if there is any plugin change.
        dependencyGraph.reset();
        dependencyGraph = new ResettableLazyReference<DependencyGraph<Requestable>>() {
            @Override
            protected DependencyGraph<Requestable> create() throws Exception {
                final DependencyGraphBuilder builder = DependencyGraph.builder();

                // Handle superbatch context
                if (isSuperbatchCreated()) {
                    final Requestable SUPERBATCH = SuperBatchKey.getInstance();
                    batchingConfiguration
                            .getSuperBatchModuleCompleteKeys()
                            .forEach(dep -> builder.addWebResourceDependency(SUPERBATCH, dep));
                } else {
                    log.debug(
                            "Ignoring super-batch dependencies; super-batching is probably disabled in resource batching configuration.");
                }

                // Handle syncbatch context
                if (isSyncContextCreated()) {
                    final Requestable SYNCBATCH = new WebResourceContextKey(SYNCBATCH_CONTEXT_KEY);
                    getSyncWebResourceKeys().stream()
                            .map(CompleteWebResourceKey::getCompleteKey)
                            .forEach(dep -> builder.addWebResourceDependency(SYNCBATCH, dep));
                }

                // Add normal dependencies
                builder.addDependencies(webResourceDescriptors);

                // Finalize the graph
                return builder.build();
            }
        };

        return snapshot;
    }

    // APDEX-1045: Adding sync batch (always first in the DOM) as virtual resource.
    private boolean constructSyncbatch(final Snapshot snapshot, final Map<String, Bundle> cachedBundles) {
        final List<String> dependencies = getSyncWebResourceKeys().stream()
                .map(CompleteWebResourceKey::getCompleteKey)
                .collect(Collectors.toList());
        if (!dependencies.isEmpty()) {
            addSpecialContext(snapshot, cachedBundles, SYNCBATCH_KEY, dependencies);
            return true;
        }
        return false;
    }

    // Adding super batch as virtual resource.
    private boolean constructSuperbatch(final Snapshot snapshot, final Map<String, Bundle> cachedBundles) {
        final List<String> dependencies = new ArrayList<>();
        if (isSuperBatchingEnabled()) {
            dependencies.addAll(getBeforeAllResources());
            dependencies.addAll(batchingConfiguration.getSuperBatchModuleCompleteKeys());
        }
        if (!dependencies.isEmpty()) {
            addSpecialContext(snapshot, cachedBundles, SUPERBATCH_KEY, dependencies);
            return true;
        }
        return false;
    }

    private void addSpecialContext(
            Snapshot snapshot, Map<String, Bundle> bundles, String key, List<String> dependencies) {
        IntermediaryContextData contextData = new IntermediaryContextData();
        contextData.updatedAt = new Date(0);
        contextData.dependencies = dependencies;
        updateContextData(bundles, contextData);
        bundles.put(key, new Bundle(snapshot, key, dependencies, contextData.updatedAt, contextData.version, true));
    }

    private void updateContextData(Map<String, Bundle> bundles, IntermediaryContextData contextData) {
        for (String dependency : contextData.dependencies) {
            Bundle bundle = bundles.get(dependency);
            if (null == bundle) {
                continue;
            }

            Date updatedAt = bundle.getUpdatedAt();
            String version = bundle.getVersion();

            if (contextData.updatedAt == null || contextData.updatedAt.before(updatedAt)) {
                contextData.updatedAt = updatedAt;
            }
            contextData.version = HashBuilder.buildHash(contextData.version, version);
        }
    }

    /**
     * Queries the plugin system and return list of Resources for Web Resource.
     */
    public LinkedHashMap<String, Resource> getResourcesWithoutCache(Bundle bundle) {
        // Adding resources, it is important to use `LinkedHashMap` because order of resources is important.
        LinkedHashMap<String, Resource> resources = new LinkedHashMap<>();

        WebResourceModuleDescriptor webResourceDescriptor = getWebResourceModuleDescriptor(bundle.getKey());
        if (webResourceDescriptor != null) {
            for (ResourceLocation resourceLocation :
                    getResourceLocations(webResourceDescriptor, integration.isCompiledResourceEnabled())) {
                Resource resource = buildResource(bundle, resourceLocation);
                resources.put(resource.getName(), resource);
            }
        }
        return resources;
    }

    /**
     * Get data resources for web resource.
     */
    public LinkedHashMap<String, Jsonable> getWebResourceData(String key) {
        // Adding resources, it is important to use `LinkedHashMap` because order of resources is important.
        LinkedHashMap<String, Jsonable> data = new LinkedHashMap<>();
        WebResourceModuleDescriptor webResourceDescriptor = getWebResourceModuleDescriptor(key);
        if (webResourceDescriptor != null) {
            for (Map.Entry<String, WebResourceDataProvider> entry :
                    webResourceDescriptor.getDataProviders().entrySet()) {
                data.put(entry.getKey(), entry.getValue().get());
            }
        }
        return data;
    }

    private WebResourceModuleDescriptor getWebResourceModuleDescriptor(String key) {
        ModuleDescriptor<?> moduleDescriptor;
        try {
            moduleDescriptor = integration.getPluginAccessor().getEnabledPluginModule(key);
        } catch (RuntimeException e) {
            moduleDescriptor = null;
        }
        if (moduleDescriptor == null) {
            return null;
        }
        if (!(moduleDescriptor instanceof WebResourceModuleDescriptor)) {
            return null;
        }
        return (WebResourceModuleDescriptor) moduleDescriptor;
    }

    /**
     * If minification enabled.
     */
    public boolean isMinificationEnabled() {
        return !(Boolean.getBoolean(DISABLE_MINIFICATION) || isDevMode());
    }

    /**
     * Helper to hide bunch of integration code and simplify resource creation.
     */
    protected Resource buildResource(Bundle bundle, ResourceLocation resourceLocation) {
        // The code based on `SingleDownloadableResourceFinder.getDownloadablePluginResource`.
        return resourceFactory.createResource(
                bundle,
                resourceLocation,
                ResourceUtils.getType(resourceLocation.getName()),
                ResourceUtils.getType(resourceLocation.getLocation()));
    }

    /**
     * Get not declared Resource.
     */
    public Resource getModuleResource(String completeKey, String name) {
        // Confluence throws error if trying to query module with not complete key (with the plugin key for example).
        if (!isWebResourceKey(completeKey)) {
            return null;
        }
        ModuleDescriptor<?> moduleDescriptor = integration.getPluginAccessor().getEnabledPluginModule(completeKey);
        // In case of virtual context or super batch resource there would be null returned.
        if (moduleDescriptor == null) {
            return null;
        }
        ResourceLocation resourceLocation = moduleDescriptor.getResourceLocation(DOWNLOAD_PARAM_VALUE, name);
        if (resourceLocation == null) {
            return null;
        }
        Plugin plugin = moduleDescriptor.getPlugin();
        Date updatedAt = (plugin.getDateLoaded() == null) ? new Date() : plugin.getDateLoaded();
        PluginResourceContainer resourceContainer = new PluginResourceContainer(
                new Snapshot(this),
                completeKey,
                updatedAt,
                getPluginVersionOrInstallTime(plugin, usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins));
        return buildResource(resourceContainer, resourceLocation);
    }

    /**
     * Get Resource for Plugin.
     */
    public Resource getPluginResource(String pluginKey, String name) {
        Plugin plugin = integration.getPluginAccessor().getPlugin(pluginKey);
        if (plugin == null) {
            return null;
        }
        ResourceLocation resourceLocation = plugin.getResourceLocation(DOWNLOAD_PARAM_VALUE, name);
        if (resourceLocation == null) {
            return null;
        }
        PluginResourceContainer resourceContainer = new PluginResourceContainer(
                new Snapshot(this),
                pluginKey,
                plugin.getDateLoaded(),
                getPluginVersionOrInstallTime(plugin, usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins));
        Resource resource = buildResource(resourceContainer, resourceLocation);
        return resource;
    }

    public boolean optimiseSourceMapsForDevelopment() {
        return batchingConfiguration.optimiseSourceMapsForDevelopment();
    }

    public boolean isSyncBatchingEnabled() {
        return !integration.getSyncWebResourceKeys().isEmpty();
    }

    public boolean isSuperBatchingEnabled() {
        return batchingConfiguration.isSuperBatchingEnabled()
                && !batchingConfiguration.getSuperBatchModuleCompleteKeys().isEmpty();
    }

    public boolean isBatchContentTrackingEnabled() {
        return batchingConfiguration.isBatchContentTrackingEnabled();
    }

    public boolean isContextBatchingEnabled() {
        return batchingConfiguration.isContextBatchingEnabled();
    }

    public boolean isWebResourceBatchingEnabled() {
        return batchingConfiguration.isPluginWebResourceBatchingEnabled();
    }

    /**
     * If incremental cache enabled.
     */
    public boolean isIncrementalCacheEnabled() {
        return integration.isIncrementalCacheEnabled();
    }

    /**
     * Hash code representing the state of config. State of the system could change not only on plugin system changes
     * but also some properties of config could change too. In such cases the cache additionally checked against the
     * version of config it's build for.
     * <p>
     * Strictly speaking this hash should include the full state of config, but it would be too slow, so instead we
     * check only for some of its properties.
     * </p>
     */
    public int partialHashCode() {
        return integration.getSuperBatchVersion().hashCode();
    }

    /**
     * @deprecated since 3.3.2
     */
    @Deprecated
    public WebResourceIntegration getIntegration() {
        return integration;
    }

    /**
     * @deprecated since 3.3.2
     */
    @Deprecated
    public TransformerCache getTransformerCache() {
        return transformerCache;
    }

    public int getUrlCacheSize() {
        return urlCacheSize;
    }

    @Deprecated
    public ResourceBatchingConfiguration getBatchingConfiguration() {
        return batchingConfiguration;
    }

    /**
     * @deprecated since 5.0.0, use ResourcePriority.DEFER instead
     */
    @Deprecated
    public boolean isDeferJsAttributeEnabled() {
        return integration.isDeferJsAttributeEnabled();
    }

    /**
     * Represents product state considering everything that contributes to content of resources.
     *
     * @since v3.5.0
     */
    @VisibleForTesting
    public String computeGlobalStateHash() {
        // This needs to represent all the things that contribute
        // to the content of files or contribute to what
        // requireContext() or requireResource() would result in
        List<String> state = new LinkedList<>();
        state.add("productver");
        state.add(integration.getHostApplicationVersion());

        // annotators impact content of files
        state.add("annotators");
        state.add(String.valueOf(getContentAnnotator(JS_TYPE).hashCode()));
        state.add(String.valueOf(getContentAnnotator(CSS_TYPE).hashCode()));

        state.add("plugins");
        List<Plugin> plugins = new ArrayList<>(integration.getPluginAccessor().getEnabledPlugins());
        sort(plugins, consistentPluginOrder());
        for (Plugin plugin : plugins) {
            state.add(plugin.getKey());
            state.add(getPluginVersionOrInstallTime(plugin, usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins));
        }

        String globalProductStateHash = HashBuilder.buildHash(state);
        log.info("Calculated global state hash {} based on {}", globalProductStateHash, state);

        return globalProductStateHash;
    }

    private Comparator<Plugin> consistentPluginOrder() {
        return Comparator.<Plugin, String>comparing(p -> p.getKey() + "-"
                + getPluginVersionOrInstallTime(p, usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins));
    }

    public List<String> getBeforeAllResources() {
        return emptyList();
    }

    /**
     * Returns a URL transformer to allow clients to map resource URLs as in {@link #getResourceCdnPrefix(String)}.
     *
     * @since v3.5.0
     */
    public CdnResourceUrlTransformer getCdnResourceUrlTransformer() {
        return cdnResourceUrlTransformer;
    }

    /**
     * Determines if bundle hash validation for batch resources should be disabled.
     *
     * @since 4.3.5
     */
    public static boolean isBundleHashValidationEnabled() {
        final String defaultState = Boolean.TRUE.toString();
        final String property = System.getProperty(ENABLE_BUNDLE_HASH_VALIDATION, defaultState);

        return !Boolean.FALSE.toString().equalsIgnoreCase(property);
    }

    public boolean isJavaScriptTryCatchWrappingEnabled() {
        return batchingConfiguration.isJavaScriptTryCatchWrappingEnabled();
    }

    /**
     * @return true if URL caching should be used.
     */
    public boolean isUrlCachingEnabled() {
        return isUrlCachingEnabled;
    }

    void setLogger(Logger logger) {
        supportLogger = logger;
    }

    public ResourceFactory getResourceFactory() {
        return resourceFactory;
    }

    protected static class IntermediaryContextData {
        public List<String> dependencies = new ArrayList<>();
        public Date updatedAt;
        public String version = "";
    }

    /**
     * @return true if it should track performance/caching stats
     */
    public boolean isPerformanceTrackingEnabled() {
        return !getBooleanFromDarkFeatureManagerThenSystemProperty(DISABLE_PERFORMANCE_TRACKING);
    }

    /**
     * Will do its best to return a result from DarkFeatureManager, but will fallback to System properties
     * <p>
     * Exists to de-couple the WRM from how products manage properties without having to add extra integration work
     * during adoption to the {@link WebResourceIntegration} SPI
     *
     * @param name of the system property
     * @return true if flag is on
     */
    @VisibleForTesting
    protected boolean getBooleanFromDarkFeatureManagerThenSystemProperty(String name) {
        // While DarkFeatureManager should NOT be null,
        // it might be due to testing or integration in to the product issues
        return Optional.ofNullable(integration.getDarkFeatureManager())
                .map(darkFeatureManager -> darkFeatureManager.isEnabledForAllUsers(name))
                .flatMap(val -> val)
                .orElseGet(() -> Boolean.getBoolean(name) || Boolean.getBoolean(ATLASSIAN_DARKFEATURE_PREFIX + name));
    }
}
