package com.atlassian.plugin.webresource;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.helpers.url.ContextBatch;
import com.atlassian.plugin.webresource.impl.helpers.url.SubBatch;
import com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;

import static com.atlassian.plugin.webresource.impl.http.Router.encodeContexts;
import static com.atlassian.plugin.webresource.impl.snapshot.resource.Resource.isCacheableStatic;
import static com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType.CONTEXT;

/**
 * An adapter between the current URL Generation code and previous URL Output code.
 *
 * @since v3.3
 */
public class ContextSubBatchResourceUrl extends ResourceUrl {
    private final String key;
    private final Globals globals;
    private final String type;
    private final ContextBatch contextBatch;
    private final SubBatch subBatch;
    private final String hash;
    private final Map<String, String> params;

    public ContextSubBatchResourceUrl(
            final Globals globals,
            final ContextBatch contextBatch,
            final SubBatch subBatch,
            final String type,
            final Map<String, String> params,
            final String hash) {
        this.globals = globals;
        this.contextBatch = contextBatch;
        this.subBatch = subBatch;
        this.hash = hash;
        this.params = params;
        key = encodeContexts(contextBatch.getIncluded(), contextBatch.getExcluded());
        this.type = type;
    }

    public SubBatch getSubBatch() {
        return subBatch;
    }

    public List<Bundle> getBatchedBundles() {
        return subBatch.getBundles();
    }

    public List<String> getIncludedContexts() {
        return contextBatch.getIncluded();
    }

    public LinkedHashSet<String> getExcludedContexts() {
        return contextBatch.getExcluded();
    }

    @Override
    public String getName() {
        return key + '.' + type;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getType() {
        return type;
    }

    public String getUrl(final boolean isAbsolute) {
        return globals.getRouter()
                .cloneWithNewUrlMode(isAbsolute)
                .contextBatchUrl(
                        getKey(),
                        getType(),
                        getParams(),
                        isCacheableStatic(subBatch.getResourcesParams()),
                        true,
                        hash,
                        UrlGenerationHelpers.calculateBundlesHash(subBatch.getAllFoundBundles()));
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

    public BatchType getBatchType() {
        return CONTEXT;
    }

    @Override
    public List<Resource> getResources(final RequestCache requestCache) {
        final List<Resource> resources = new ArrayList<>();
        for (final Bundle bundle : subBatch.getBundles()) {
            resources.addAll(UrlGenerationHelpers.resourcesOfType(
                    bundle.getResources(requestCache).values(), type));
        }
        return resources;
    }
}
