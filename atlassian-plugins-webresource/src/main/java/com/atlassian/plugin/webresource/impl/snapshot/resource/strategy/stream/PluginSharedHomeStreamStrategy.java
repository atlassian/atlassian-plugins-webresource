package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream;

import java.io.InputStream;
import java.nio.file.Path;

import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.helpers.FileOperations;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.util.WebResourceKeyHelper;
import com.atlassian.webresource.api.assembler.resource.CompleteWebResourceKey;

public class PluginSharedHomeStreamStrategy implements StreamStrategy {
    private final FileOperations fileOperations;
    private final WebResourceIntegration webResourceIntegration;
    private final CompleteWebResourceKey bundleKey;

    public PluginSharedHomeStreamStrategy(
            FileOperations fileOperations, WebResourceIntegration webResourceIntegration, Bundle bundle) {
        this.fileOperations = fileOperations;
        this.webResourceIntegration = webResourceIntegration;
        this.bundleKey =
                WebResourceKeyHelper.createWebResourceKey(bundle.getKey()).orElseThrow();
    }

    @Override
    public InputStream getInputStream(String path) {
        Path normalizedResolutionRoot = getResolutionRoot(webResourceIntegration
                        .getPluginAccessor()
                        .getEnabledPlugin(bundleKey.getPluginKey())
                        .getKey())
                .normalize();

        Path resolvedNormalizedPath = normalizedResolutionRoot.resolve(path).normalize();
        if (!resolvedNormalizedPath.startsWith(normalizedResolutionRoot)) {
            throw new IllegalArgumentException("Invalid sharedPluginHome web resource definition");
        }

        path = resolvedNormalizedPath.toString();

        if (fileOperations.exists(path)) {
            try {
                return fileOperations.createInputStream(path);
            } catch (Exception e) {
                throw new RuntimeException(
                        "Unable to read " + path + " within " + normalizedResolutionRoot + " : " + e);
            }
        } else {
            return null;
        }
    }

    private Path getResolutionRoot(String pluginKey) {
        final var applicationProperties = webResourceIntegration.getApplicationProperties();

        final Path designatedHome = applicationProperties
                .getSharedHomeDirectory()
                .orElse(applicationProperties
                        .getLocalHomeDirectory()
                        .orElseThrow(() -> new RuntimeException(
                                "Unable to determine home directory for pluginSharedHome resolution.")));

        return designatedHome.resolve("web-resources").resolve(pluginKey);
    }
}
