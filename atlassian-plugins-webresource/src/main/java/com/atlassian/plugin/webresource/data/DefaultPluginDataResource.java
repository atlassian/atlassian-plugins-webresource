package com.atlassian.plugin.webresource.data;

import java.util.Optional;
import javax.annotation.Nonnull;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.api.data.PluginDataResource;

import static java.util.Optional.of;

import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.defaultPhase;

/**
 * Default PluginDataResource
 *
 * @since v3.0
 */
public class DefaultPluginDataResource implements PluginDataResource {
    private final String key;
    private final Optional<Jsonable> jsonable;
    private final ResourcePhase resourcePhase;

    public DefaultPluginDataResource(@Nonnull final String key, @Nonnull final Jsonable jsonable) {
        this(key, of(jsonable), defaultPhase());
    }

    public DefaultPluginDataResource(
            @Nonnull final String key, @Nonnull final Jsonable jsonable, @Nonnull final ResourcePhase resourcePhase) {
        this(key, of(jsonable), resourcePhase);
    }

    public DefaultPluginDataResource(@Nonnull final String key, @Nonnull final Optional<Jsonable> jsonable) {
        this(key, jsonable, defaultPhase());
    }

    public DefaultPluginDataResource(
            @Nonnull final String key,
            @Nonnull final Optional<Jsonable> jsonable,
            @Nonnull final ResourcePhase resourcePhase) {
        this.key = key;
        this.jsonable = jsonable;
        this.resourcePhase = resourcePhase;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Jsonable getJsonable() {
        return jsonable.get();
    }

    @Override
    public Optional<Jsonable> getData() {
        return jsonable;
    }

    @Nonnull
    @Override
    public ResourcePhase getResourcePhase() {
        return resourcePhase;
    }

    @Override
    public boolean equals(Object thatObject) {
        if (this == thatObject) {
            return true;
        }
        if (!(thatObject instanceof DefaultPluginDataResource)) {
            return false;
        }

        final DefaultPluginDataResource thatDefaultPluginDataResource = (DefaultPluginDataResource) thatObject;

        return key.equals(thatDefaultPluginDataResource.key);
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }
}
