package com.atlassian.plugin.webresource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

abstract class AbstractWebResourceFormatter implements WebResourceFormatter {
    /**
     * Should return a List of parameter name {@link String}s, which the WebResourceFormatter will write out as
     * HTML attributes.
     *
     * @return a {@link List} of parameter names
     */
    protected abstract List<String> getAttributeParameters();

    /**
     * A convenient method to convert the given parameter map into a List of HTML {@link String} attributes.
     * For example, an entry with key 'foo' and value 'bar' is converted to the attribute string, foo="bar".
     * <p>
     * Only parameters that are supported by the WebResourceFormatter are formatted (See {@link #getAttributeParameters()}).
     *
     * @param params a {@link Map} of parameters
     * @return a list of HTML {@link String} attributes
     */
    protected List<String> getParametersAsAttributes(Map params) {
        final List<String> attributes = new ArrayList<>();
        for (Object param : params.entrySet()) {
            Map.Entry entry = (Map.Entry) param;
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            if (StringUtils.isNotBlank(key) && getAttributeParameters().contains(key.toLowerCase())) {
                String attribute = key + (value.equals("") ? "" : "=\"" + value + "\"");
                attributes.add(attribute);
            }
        }
        return attributes;
    }

    boolean isValid(Map<String, String> attributes) {
        return true;
    }
}
