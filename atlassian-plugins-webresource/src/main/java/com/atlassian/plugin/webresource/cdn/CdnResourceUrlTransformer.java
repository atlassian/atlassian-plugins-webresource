package com.atlassian.plugin.webresource.cdn;

/**
 * This interface aims to decouple clients from {@link com.atlassian.plugin.webresource.impl.config.Config} which
 * currently contains the actual implementation.
 *
 * @since v3.5.0
 */
public interface CdnResourceUrlTransformer {

    /**
     * Transforms a relative URL to CDN
     *
     * @param url Local resource URL
     * @return Transfomed URL
     * @see com.atlassian.plugin.webresource.impl.config.Config#getResourceCdnPrefix(String)
     */
    String getResourceCdnPrefix(String url);
}
