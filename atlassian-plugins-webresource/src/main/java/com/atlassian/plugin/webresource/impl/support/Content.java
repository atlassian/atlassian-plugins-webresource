package com.atlassian.plugin.webresource.impl.support;

import java.io.OutputStream;

import com.atlassian.sourcemap.ReadableSourceMap;

/**
 * Binary content of Web Resource.
 *
 * @since 3.3
 */
public interface Content {
    /**
     * Write the resource to the supplied OutputStream and return its Source Map. Note that the OutputStream will not be
     * closed by this method.
     *
     * @param out                the stream to write to
     * @param isSourceMapEnabled should the source mab being generated
     * @return source map of the resource, may return null.
     */
    ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled);

    /**
     * Returns the content type for the resource. May return null if it cannot resolve its own content type.
     */
    String getContentType();

    /**
     * @return if content has been transformed.
     */
    boolean isTransformed();

    /**
     * @return true if content has been written to the output stream, false otherwise
     */
    default boolean isPresent() {
        return true;
    }
}
