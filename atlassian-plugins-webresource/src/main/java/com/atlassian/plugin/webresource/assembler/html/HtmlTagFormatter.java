package com.atlassian.plugin.webresource.assembler.html;

import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.assembler.ResourceUrls;

interface HtmlTagFormatter {

    /**
     * Converts a certain resource into html import tag and format it according to its url extension.
     *
     * @param resourceUrls The resource to be formatted.
     * @return The formatted value.
     */
    @Nonnull
    String format(@Nonnull final ResourceUrls resourceUrls);

    /**
     * Identifies whether the extesion handlded by the formater matches the resource name.
     *
     * @param resourceName The resource name.
     * @return <p>{@code true}: The resouerce name matches the formatter's.</p>
     * <p>{@code false}: The resouerce name does not match the formatter's.</p>
     */
    boolean matches(@Nonnull final String resourceName);
}
