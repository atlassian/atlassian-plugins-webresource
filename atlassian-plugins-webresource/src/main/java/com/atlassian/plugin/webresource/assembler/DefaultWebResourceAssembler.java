package com.atlassian.plugin.webresource.assembler;

import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.webresource.api.assembler.AssembledResources;
import com.atlassian.webresource.api.assembler.RequiredData;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

import static java.util.Objects.requireNonNull;

/**
 * Implementation of WebResourceAssembler.
 *
 * @since v3.0
 */
class DefaultWebResourceAssembler implements WebResourceAssembler {
    private final AssembledResources assembledResources;
    private final Globals globals;
    private final RequestState requestState;
    private final RequiredData requiredData;
    private final RequiredResources requiredResources;

    DefaultWebResourceAssembler(@Nonnull final RequestState requestState, @Nonnull final Globals globals) {
        this.globals = requireNonNull(globals, "The globals is mandatory to build the web resource assembler.");
        this.requestState =
                requireNonNull(requestState, "The request state is mandatory to build the web resource assembler.");
        requiredData = new DefaultRequiredData(requestState);
        requiredResources = new DefaultRequiredResources(requestState);
        assembledResources = new DefaultAssembledResources(requestState);
    }

    @Nonnull
    @Override
    public AssembledResources assembled() {
        return assembledResources;
    }

    @Nonnull
    @Override
    public RequiredResources resources() {
        return requiredResources;
    }

    @Nonnull
    @Override
    public RequiredData data() {
        return requiredData;
    }

    @Nonnull
    @Override
    public WebResourceAssembler copy() {
        return new DefaultWebResourceAssembler(requestState.deepClone(), globals);
    }
}
