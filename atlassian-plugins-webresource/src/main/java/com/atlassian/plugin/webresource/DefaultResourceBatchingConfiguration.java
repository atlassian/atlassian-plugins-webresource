package com.atlassian.plugin.webresource;

import java.util.List;

import static java.util.Collections.emptyList;

import static com.atlassian.plugin.webresource.Flags.isDevMode;

/**
 * Default configuration for the plugin resource locator, for those applications that do not want to perform
 * any super-batching.
 */
public class DefaultResourceBatchingConfiguration implements ResourceBatchingConfiguration {
    public static final String PLUGIN_WEBRESOURCE_BATCHING_OFF = "plugin.webresource.batching.off";
    public static final String PLUGIN_WEB_RESOURCE_BATCH_CONTENT_TRACKING = "plugin.webresource.batch.content.tracking";
    public static final String PLUGIN_WEB_RESOURCE_JAVASCRIPT_TRY_CATCH_WRAPPING =
            "plugin.webresource.javascript.try.catch.wrapping";
    public static final String PLUGIN_WEB_RESOURCE_SOURCE_MAP_ENABLED = "plugin.webresource.source.map.enabled";
    public static final String PLUGIN_WEB_RESOURCE_SOURCE_MAP_OPTIMISED_FOR_DEVELOPMENT =
            "plugin.webresource.source.map.optimised.for.development";
    public static final String PLUGIN_WEB_RESOURCE_SUPER_BATCH_ENABLED = "plugin.webresource.super.batch.enabled";
    public static final String PLUGIN_WEB_RESOURCE_CONTEXT_BATCH_ENABLED = "plugin.webresource.context.batch.enabled";

    public boolean isSuperBatchingEnabled() {
        final String superBatchEnabledSetting = System.getProperty(PLUGIN_WEB_RESOURCE_SUPER_BATCH_ENABLED, "false");
        return Boolean.parseBoolean(superBatchEnabledSetting);
    }

    public List<String> getSuperBatchModuleCompleteKeys() {
        return emptyList();
    }

    public boolean isContextBatchingEnabled() {
        final String contextBatchEnabledSetting =
                System.getProperty(PLUGIN_WEB_RESOURCE_CONTEXT_BATCH_ENABLED, "false");
        return Boolean.parseBoolean(contextBatchEnabledSetting);
    }

    public boolean isPluginWebResourceBatchingEnabled() {
        final String explicitSetting = System.getProperty(PLUGIN_WEBRESOURCE_BATCHING_OFF);
        if (explicitSetting != null) {
            return !Boolean.parseBoolean(explicitSetting);
        } else {
            return !isDevMode();
        }
    }

    /**
     * Currently (as of v2.14) this is false by default. It is likely to become true by
     * default in the future.
     */
    public boolean isJavaScriptTryCatchWrappingEnabled() {
        final String javaScriptTryCatchWrapping = System.getProperty(PLUGIN_WEB_RESOURCE_JAVASCRIPT_TRY_CATCH_WRAPPING);
        return Boolean.parseBoolean(javaScriptTryCatchWrapping);
    }

    @Override
    public boolean isBatchContentTrackingEnabled() {
        final String trackingSetting = System.getProperty(PLUGIN_WEB_RESOURCE_BATCH_CONTENT_TRACKING);
        return Boolean.parseBoolean(trackingSetting);
    }

    @Override
    public boolean isSourceMapEnabled() {
        return Boolean.parseBoolean(System.getProperty(PLUGIN_WEB_RESOURCE_SOURCE_MAP_ENABLED));
    }

    @Override
    public boolean optimiseSourceMapsForDevelopment() {
        return Boolean.parseBoolean(System.getProperty(PLUGIN_WEB_RESOURCE_SOURCE_MAP_OPTIMISED_FOR_DEVELOPMENT));
    }
}
