package com.atlassian.plugin.webresource.impl.helpers.url;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.ResourceKey;
import com.atlassian.plugin.webresource.impl.ResourceKeysSupplier;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

import static com.google.common.collect.Lists.newArrayList;

import static com.atlassian.plugin.webresource.impl.RequestCache.toResourceKeys;
import static com.atlassian.plugin.webresource.impl.config.Config.BATCH_TYPES;

/**
 * Data structure representing Sub Batch of both Web Resource Batch and Context Batch,
 * it is used in cache and shouldn't contain any references.
 */
public class SubBatch {
    /**
     * Bundles can't be inferred from resourceKeys because there could be an empty bundle without any
     * resources but with condition and dependencies.
     * It is ok to have reference to Bundle because it is anyway stored in Snapshot cache.
     */
    private final List<Bundle> filteredBundles;

    private final Map<String, String> resourcesParams;
    private final Map<String, ResourceKeysSupplier> resourceKeysByType;
    private final List<Bundle> allFoundBundles;

    public SubBatch(final Map<String, String> resourcesParams, final Bundle bundle, final List<Resource> resources) {
        this(resourcesParams, newArrayList(bundle), resources, Collections.singletonList(bundle));
    }

    public SubBatch(
            final Map<String, String> resourcesParams,
            final List<Bundle> filteredBundles,
            final List<Resource> resources,
            final List<Bundle> allFoundBundles) {
        this.resourcesParams = resourcesParams;
        this.filteredBundles = filteredBundles;
        resourceKeysByType = new HashMap<>();
        this.allFoundBundles = allFoundBundles;

        // Instead of storing the reference to the resources storing its keys.
        for (final String type : BATCH_TYPES) {
            final List<Resource> resourcesOfType = UrlGenerationHelpers.resourcesOfType(resources, type);
            final List<ResourceKey> resourceKeys = toResourceKeys(resourcesOfType);
            final ResourceKeysSupplier resourceKeysSupplier = new ResourceKeysSupplier(resourceKeys);
            resourceKeysByType.put(type, resourceKeysSupplier);
        }
    }

    /**
     * @return list of filtered bundles based on various bundle criteria.
     */
    public List<Bundle> getBundles() {
        return filteredBundles;
    }

    public List<Resource> getResourcesOfType(final RequestCache requestCache, final String type) {
        return requestCache.getCachedResources(resourceKeysByType.get(type));
    }

    public Map<String, String> getResourcesParams() {
        return resourcesParams;
    }

    /**
     * @since 5.3.0
     *
     * @return list of all found and unfiltered bundles.
     */
    public List<Bundle> getAllFoundBundles() {
        return allFoundBundles;
    }
}
