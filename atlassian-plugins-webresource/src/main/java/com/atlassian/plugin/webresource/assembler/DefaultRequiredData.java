package com.atlassian.plugin.webresource.assembler;

import javax.annotation.Nonnull;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableBoolean;
import com.atlassian.json.marshal.wrapped.JsonableNumber;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.webresource.api.assembler.RequiredData;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static java.util.Objects.requireNonNull;

import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.defaultPhase;

public class DefaultRequiredData implements RequiredData {
    private final RequestState requestState;

    public DefaultRequiredData(@Nonnull final RequestState requestState) {
        this.requestState = requireNonNull(requestState, "The request state is mandatory to build the required data.");
    }

    @Nonnull
    @Override
    public RequiredData requireData(@Nonnull final String key, @Nonnull final Jsonable content) {
        return requireData(key, content, defaultPhase());
    }

    @Nonnull
    @Override
    public RequiredData requireData(
            @Nonnull final String key, @Nonnull final Jsonable content, @Nonnull final ResourcePhase resourcePhase) {
        requestState.getIncludedData(resourcePhase).put(key, content);
        return this;
    }

    @Nonnull
    @Override
    public RequiredData requireData(@Nonnull final String key, @Nonnull final Number content) {
        return requireData(key, content, defaultPhase());
    }

    @Nonnull
    @Override
    public RequiredData requireData(
            @Nonnull final String key, @Nonnull final Number content, @Nonnull final ResourcePhase resourcePhase) {
        requestState.getIncludedData(resourcePhase).put(key, new JsonableNumber(content));
        return this;
    }

    @Nonnull
    @Override
    public RequiredData requireData(@Nonnull final String key, @Nonnull final String content) {
        return requireData(key, content, defaultPhase());
    }

    @Nonnull
    @Override
    public RequiredData requireData(
            @Nonnull final String key, @Nonnull final String content, @Nonnull final ResourcePhase resourcePhase) {
        requestState.getIncludedData(resourcePhase).put(key, new JsonableString(content));
        return this;
    }

    @Nonnull
    @Override
    public RequiredData requireData(@Nonnull final String key, @Nonnull final Boolean content) {
        return requireData(key, content, defaultPhase());
    }

    @Nonnull
    @Override
    public RequiredData requireData(
            @Nonnull final String key, @Nonnull final Boolean content, @Nonnull final ResourcePhase resourcePhase) {
        requestState.getIncludedData(resourcePhase).put(key, new JsonableBoolean(content));
        return this;
    }
}
