package com.atlassian.plugin.webresource.legacy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

import com.atlassian.plugin.servlet.DownloadableResource;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;
import static java.util.Collections.emptyList;

import static com.atlassian.plugin.webresource.impl.http.Router.encodeContexts;

/**
 * An intermediary object used for constructing and merging context batches.
 * This is a bean that holds the different resources and parameters that apply
 * to a particular batch.
 * The batch can both include and exclude one or more contexts
 * Resources are expected to be in dependency order, with no duplicates.
 */
public class ContextBatch {
    private static final String UTF8 = "UTF-8";
    private static final String MD5 = "MD5";
    private static final Ordering<ModuleDescriptorStub> MODULE_KEY_ORDERING =
            Ordering.natural().onResultOf(new TransformDescriptorToKey());

    private final List<String> contexts;
    private final Iterable<String> excludedContexts;
    private final Iterable<ModuleDescriptorStub> resources;
    private final boolean removeSuperResources;

    /**
     * @param contexts         the ordering of contexts is important since it determines the ordering of resources within a batch (which could be
     *                         important for badly written Javascripts).
     * @param excludedContexts the ordering of excluded contexts is not important.
     */
    public ContextBatch(
            final List<String> contexts,
            Iterable<String> excludedContexts,
            final Iterable<ModuleDescriptorStub> resources,
            boolean removeSuperResources) {
        this.contexts = copyOf(contexts);
        if (excludedContexts == null) {
            this.excludedContexts = emptyList();
        } else {
            this.excludedContexts = copyOf(excludedContexts);
        }

        // Note: Ordering is important in producing a consistent hash.
        // But, dependency order is not important when producing the PluginResource,
        // it is only important when we serve the resource. So it is safe to reorder this.
        this.resources = ImmutableSortedSet.copyOf(MODULE_KEY_ORDERING, resources);
        this.removeSuperResources = removeSuperResources;
    }

    private static Iterable<PluginResource> postContextBatchesProcess(
            List<ContextBatchPluginResource> contextBatchResources) {
        List<PluginResource> result = new LinkedList<>();
        result.addAll(contextBatchResources);
        return result;
    }

    public boolean isEmpty() {
        return Iterables.isEmpty(resources);
    }

    public boolean isRemoveSuperResources() {
        return removeSuperResources;
    }

    /**
     * Iterates over the batch parameters for the context and creates a
     * {@link com.atlassian.plugin.webresource.legacy.ContextBatchPluginResource} for each.
     * <p>
     * It should be noted that the created {@link com.atlassian.plugin.webresource.legacy.ContextBatchPluginResource} will not actually contain any
     * {@link DownloadableResource}. The {@link PluginResource} created is primarily representing a
     * URL to be used in constructing a particular resource/download reference.
     *
     * @return ContextBatchPluginResource instances, although containing no {@link DownloadableResource}s.
     */
    public Iterable<PluginResource> buildPluginResources() {
        // add the BatchedWebResourceDescriptors for this ContextBatch to each of the generated
        // ContextBatchPluginResource
        // so that we can return ContextBatches to the client with embedded information about the dependencies
        // contained.
        Set<String> descriptors = new HashSet<>();
        for (ModuleDescriptorStub wrmd : this.getResources()) {
            descriptors.add(wrmd.getCompleteKey());
        }

        List<ContextBatchPluginResource> resources = new ArrayList<>();

        resources.add(new ContextBatchPluginResource(contexts, excludedContexts, descriptors, removeSuperResources));

        return postContextBatchesProcess(resources);
    }

    public List<String> getContexts() {
        return contexts;
    }

    public Iterable<String> getExcludedContexts() {
        return excludedContexts;
    }

    public Iterable<ModuleDescriptorStub> getResources() {
        return resources;
    }

    Iterable<String> getResourceKeys() {
        return transform(resources, ModuleDescriptorStub::getCompleteKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;

        if (!(obj instanceof ContextBatch)) return false;

        if (obj == this) return true;

        ContextBatch other = (ContextBatch) obj;

        return this.contexts.equals(other.contexts) && this.excludedContexts.equals(other.excludedContexts);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(contexts, excludedContexts);
    }

    public String getKey() {
        return encodeContexts(contexts, excludedContexts);
    }
}
