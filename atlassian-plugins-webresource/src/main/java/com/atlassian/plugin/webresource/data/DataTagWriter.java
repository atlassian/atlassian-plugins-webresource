package com.atlassian.plugin.webresource.data;

import java.io.IOException;
import java.io.Writer;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.html.encode.JavascriptEncoder;
import com.atlassian.html.encode.JavascriptEncodingWriter;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.PluginDataResource;

/**
 * Writes data provided by a &lt;data&gt; element to a page.
 *
 * @since v3.0
 */
public class DataTagWriter {
    private static final Logger log = LoggerFactory.getLogger(DataTagWriter.class);

    private static final String PRE =
            "<script>\nwindow.WRM=window.WRM||{};" + "window.WRM._unparsedData=window.WRM._unparsedData||{};"
                    + "window.WRM._unparsedErrors=window.WRM._unparsedErrors||{};\n";
    private static final String POST = "if(window.WRM._dataArrived)window.WRM._dataArrived();</script>\n";

    public void write(final Writer writer, final Iterable<PluginDataResource> data) throws IOException {
        boolean scriptTagWritten = false;
        for (PluginDataResource datum : data) {
            if (!scriptTagWritten) {
                writer.write(PRE);
                scriptTagWritten = true;
            }
            write(writer, datum);
        }
        if (scriptTagWritten) {
            writer.write(POST);
        }
    }

    private void write(final Writer writer, final PluginDataResource data) throws IOException {
        try {
            Optional<Jsonable> result = data.getData();
            if (result.isPresent()) {
                writer.write("WRM._unparsedData[\"");
                JavascriptEncoder.escape(writer, data.getKey());
                writer.write("\"]=\"");
                result.get().write(new JavascriptEncodingWriter(writer));
                writer.write("\";\n");
            } else {
                writer.write("WRM._unparsedErrors[\"");
                JavascriptEncoder.escape(writer, data.getKey());
                writer.write("\"]=\"\";\n");
            }
        } catch (IOException ex) {
            log.error("IOException encountered rendering data resource '{}'", new String[] {data.getKey()}, ex);
        } catch (RuntimeException ex) {
            // Catch plugins throwing random exceptions
            log.error("Exception encountered rendering data resource '{}'", new String[] {data.getKey()}, ex);
        }
    }
}
