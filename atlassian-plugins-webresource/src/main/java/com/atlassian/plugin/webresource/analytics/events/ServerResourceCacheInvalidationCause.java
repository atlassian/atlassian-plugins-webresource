package com.atlassian.plugin.webresource.analytics.events;

public enum ServerResourceCacheInvalidationCause {
    @SuppressWarnings("unused") // this will be in old versions of the WRM before we removed the API
    PROGRAMMATIC_TRIGGER("programmaticTrigger"),
    PLUGIN_DISABLED_EVENT("pluginDisabled"),
    PLUGIN_ENABLED_EVENT("pluginEnabled"),
    PLUGIN_WEBRESOURCE_MODULE_DISABLED("pluginWebResourceModuleDisabled"),
    PLUGIN_WEBRESOURCE_MODULE_ENABLED("pluginWebResourceModuleEnabled");

    private final String value;

    ServerResourceCacheInvalidationCause(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }
}
