package com.atlassian.plugin.webresource.assembler;

import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.webresource.api.assembler.AssembledResources;
import com.atlassian.webresource.api.assembler.WebResourceSet;

class DefaultAssembledResources implements AssembledResources {
    private final DefaultWebResourceSetBuilder builder;

    DefaultAssembledResources(@Nonnull final RequestState requestState) {
        builder = new DefaultWebResourceSetBuilder(requestState);
    }

    @Nonnull
    @Override
    public WebResourceSet drainIncludedResources() {
        return builder.enableCleanUpAfterInclude()
                .enableAdditionOfWebResourceJavascriptApiDependencies()
                .enableSuperbatch()
                .build();
    }

    @Override
    public WebResourceSet drainIncludedSyncResources() {
        return builder.enableCleanUpAfterInclude()
                .disableAdditionOfWebResourceJavascriptApiDependencies()
                .disableSuperbatch()
                .build();
    }

    @Nonnull
    @Override
    public WebResourceSet peek() {
        return builder.disableCleanUpAfterInclude()
                .disableAdditionOfWebResourceJavascriptApiDependencies()
                .enableSuperbatch()
                .build();
    }
}
