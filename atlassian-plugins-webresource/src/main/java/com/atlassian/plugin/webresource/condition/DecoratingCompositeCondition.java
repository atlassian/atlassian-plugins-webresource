package com.atlassian.plugin.webresource.condition;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.webresource.api.url.UrlBuilder;

/**
 * Abstract superclass for composite UrlReadingConditions.
 * <p>
 * Instances may contain child {@link com.atlassian.webresource.spi.condition.UrlReadingCondition}
 * wrapped by {@link DecoratingUrlReadingCondition}. Evaluation is done as follows:
 * <ul>
 * <li>
 * At URL generation time it is included in the batch.
 * Query / hash parameters for UrlReadingConditions are added to the batch's URL.
 * </li>
 * <li>
 * At Resource serving time, conditions for each UrlReadingCondition are evaluated by calling shouldDisplay().
 * This will be evaluated in the batch resource. Regardless of where the resource is included,
 * if the shouldDisplay() evaluates to false then empty content is returned in place where the resource would be.
 * </li>
 * </ul>
 *
 * @since v3.0
 */
abstract class DecoratingCompositeCondition
        implements com.atlassian.plugin.web.baseconditions.CompositeCondition<DecoratingCondition>,
                DecoratingCondition {
    protected List<DecoratingCondition> conditions = new ArrayList<>();

    public DecoratingCompositeCondition() {}

    @Override
    public void addCondition(DecoratingCondition condition) {
        this.conditions.add(condition);
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder, UrlBuildingStrategy urlBuilderStrategy) {
        for (DecoratingCondition condition : conditions) {
            condition.addToUrl(urlBuilder, urlBuilderStrategy);
        }
    }

    public List<DecoratingCondition> getConditions() {
        return conditions;
    }
}
