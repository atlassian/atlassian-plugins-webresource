package com.atlassian.plugin.webresource.legacy;

import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Represents a batch of all resources that declare themselves as part of a given context(s).
 * <p>
 * The URL for batch resources is /download/contextbatch/&lt;type&gt;/&lt;contextname&gt;/batch.&lt;type. The additional type part in the path
 * is simply there to make the number of path-parts identical with other resources, so relative URLs will still work
 * in CSS files.
 * <p>
 * The &lt;contextname&gt; part has the following syntax - <br>
 * &lt;includedContext1&gt;,&lt;includedContext2&gt;,-&lt;excludedContext1&gt;,-&lt;excludedContext2&gt;
 * <p>
 * To clarify, multiple comma separated contexts should be combined with any contexts preceded by a '-' to be excluded.
 *
 * @since 2.9.0
 */
public class ContextBatchPluginResource extends AbstractPluginResource {
    private final List<String> contexts;
    private final Iterable<String> excludedContexts;
    private boolean removeSuperResources;

    public ContextBatchPluginResource(
            List<String> contexts,
            final Iterable<String> excludedContexts,
            Set<String> completeKeys,
            boolean removeSuperResources) {
        super(Collections.unmodifiableSet(completeKeys));
        this.contexts = contexts;
        this.excludedContexts = excludedContexts;
        this.removeSuperResources = removeSuperResources;
    }

    public Iterable<String> getContexts() {
        return contexts;
    }

    public Iterable<String> getExcludedContexts() {
        return excludedContexts;
    }
}
