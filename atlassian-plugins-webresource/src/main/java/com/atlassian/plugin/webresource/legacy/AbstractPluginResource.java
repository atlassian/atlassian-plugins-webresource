package com.atlassian.plugin.webresource.legacy;

import java.util.Set;

/**
 * Abstract base class for plugin resources
 *
 * @since v3.0
 */
public abstract class AbstractPluginResource implements PluginResource {
    protected final Set<String> completeKeys;

    protected AbstractPluginResource(Set<String> completeKeys) {
        this.completeKeys = completeKeys;
    }
}
