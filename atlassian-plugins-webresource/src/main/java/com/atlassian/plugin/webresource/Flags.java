package com.atlassian.plugin.webresource;

import com.atlassian.plugin.internal.util.PluginUtils;

/**
 * A wrapper around the checks for various system properties and values
 * so the remainder of the webresource plugin can be relatively undisturbed
 * by implementation details
 *
 * @since v3.7
 */
public class Flags {
    public static boolean isDevMode() {
        return PluginUtils.isAtlassianDevMode();
    }

    public static boolean isFileCacheEnabled() {
        return !(Boolean.getBoolean(PluginUtils.WEBRESOURCE_DISABLE_FILE_CACHE) || isDevMode());
    }

    public static Integer getFileCacheSize(int defaultValue) {
        return Integer.getInteger(PluginUtils.WEBRESOURCE_FILE_CACHE_SIZE, defaultValue);
    }
}
