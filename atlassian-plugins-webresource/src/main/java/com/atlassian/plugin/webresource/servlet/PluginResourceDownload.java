package com.atlassian.plugin.webresource.servlet;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadStrategy;
import com.atlassian.plugin.servlet.util.LastModifiedHandler;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;

/**
 * A downloadable plugin resource, as described here: http://confluence.atlassian.com/display/JIRA/Downloadable+plugin+resource
 * It supports the download of single plugin resources as well as batching.
 * <p>
 * The URL that it parses for a single resource looks like this: <br>
 * <code>{server root}/download/resources/{plugin key}:{module key}/{resource name}</code>
 * </p>
 * The URL that it parses for a batch looks like this: <br>
 * <code>{server root}/download/batch/{plugin key}:{module key}/batch.css?urlreadingparam=true</code>
 */
public class PluginResourceDownload implements DownloadStrategy {
    private static final Logger log = LoggerFactory.getLogger(PluginResourceDownload.class);

    private Globals globals;
    private String characterEncoding = "UTF-8"; // default to sensible encoding

    public PluginResourceDownload(Globals globals, ContentTypeResolver contentTypeResolver) {
        this.globals = globals;
        globals.getConfig().setContentTypeResolver(contentTypeResolver);
    }

    public boolean matches(String urlPath) {
        return globals.getRouter().canDispatch(urlPath);
    }

    public void serveFile(HttpServletRequest originalRequest, final HttpServletResponse originalResponse)
            throws DownloadException {
        Request request = new Request(globals, originalRequest, characterEncoding);
        Response response = new Response(request, originalResponse);
        response.setCharacterEncoding(characterEncoding);

        log.debug(
                "WRM serving plugin resource before dispatch with request URL {} and original response status code {}",
                request.getPath(),
                response.getStatus());

        // Checking if response required and responding with HEAD if not.
        if (request.isCacheable()) {
            final LastModifiedHandler lastModifiedHandler = getLastModifiedHandler(originalRequest);
            if (response.checkRequestHelper(lastModifiedHandler)) {
                // 304
                return;
            }
        }

        // Dispatching.
        globals.getRouter().dispatch(request, response);
    }

    private LastModifiedHandler getLastModifiedHandler(HttpServletRequest originalRequest) {
        Date lastModifiedDate = null;
        try {
            /*
             * Checking the `if-modified-since` header is a cheeky way for us to check whether a
             * client had previously received this resource. We assume the previous HTTP response
             * had a sensible `last-modified` header value, which would get returned to us in `if-modified-since`.
             * TODO PLUGWEB-657 revisit logic here. Might make sense to store & check last-modified or etag in a cache.
             */
            final long ifModifiedSinceValue = originalRequest.getDateHeader("If-Modified-Since");
            if (ifModifiedSinceValue >= 0L) {
                lastModifiedDate = new Date(ifModifiedSinceValue);
            }
        } catch (IllegalArgumentException e) {
            // noop
        }
        return lastModifiedDate != null ? new LastModifiedHandler(lastModifiedDate) : new LastModifiedHandler();
    }

    public void setCharacterEncoding(String characterEncoding) {
        this.characterEncoding = characterEncoding;
    }

    public void setContentTypeResolver(ContentTypeResolver contentTypeResolver) {
        globals.getConfig().setContentTypeResolver(contentTypeResolver);
    }

    public void setGlobals(Globals globals) {
        this.globals = globals;
    }
}
