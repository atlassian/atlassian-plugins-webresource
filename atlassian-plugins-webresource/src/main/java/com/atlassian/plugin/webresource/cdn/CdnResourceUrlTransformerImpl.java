package com.atlassian.plugin.webresource.cdn;

import com.atlassian.plugin.webresource.impl.config.Config;

/**
 * Wraps CDN prefix method from Config in order to not expose this class.
 *
 * @since v3.5.0
 */
public class CdnResourceUrlTransformerImpl implements CdnResourceUrlTransformer {
    private final Config config;

    public CdnResourceUrlTransformerImpl(final Config config) {
        this.config = config;
    }

    @Override
    public String getResourceCdnPrefix(final String url) {
        return config.getResourceCdnPrefix(url);
    }
}
