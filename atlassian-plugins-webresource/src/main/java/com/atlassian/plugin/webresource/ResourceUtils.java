package com.atlassian.plugin.webresource;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class ResourceUtils {
    /**
     * @since 2.13
     */
    public static final String STATIC_HASH = "_statichash";
    /**
     * @since 4.3
     */
    public static final String WRM_INTEGRITY = "_wrm-integrity";

    private static final List<String> HTTP_ATTRIBUTE_AS_PARAMS = Arrays.asList(STATIC_HASH, WRM_INTEGRITY);

    private ResourceUtils() {}

    /**
     * Determines the type (css/js) of the resource from the given path.
     *
     * @param path - the path to use
     * @return the type of resource
     */
    public static String getType(@Nonnull String path) {
        int index = path.lastIndexOf('.');
        if (index > -1 && index < path.length())
            return path.substring(index + 1).toLowerCase();

        return "";
    }

    /**
     * Determines the base name (name without the extension) of the resource from the given path.
     *
     * @param path - the path to use
     * @return the basename of resource
     * @since 3.3
     */
    public static String getBasename(@Nonnull String path) {
        int index = path.lastIndexOf('.');
        if (index > -1 && index < path.length()) {
            return path.substring(0, index);
        }
        return path;
    }

    /**
     * Returns a Map of query parameters from the request. If there are multiple values for the same
     * query parameter, the first value is used.
     *
     * @see javax.servlet.ServletRequest#getParameterMap()
     * @see javax.servlet.ServletRequest#getAttribute(String)
     */
    public static Map<String, String> getQueryParameters(final HttpServletRequest request) {
        final Map<String, String> result = new TreeMap<>();

        request.getParameterMap().forEach((key, values) -> {
            if (values != null && values.length > 0) {
                result.put(key, values[0]);
            }
        });

        HTTP_ATTRIBUTE_AS_PARAMS.stream()
                .filter(attr -> request.getAttribute(attr) != null)
                .forEach(attr -> result.put(attr, request.getAttribute(attr).toString()));

        return result;
    }

    /**
     * Given a parameter map, determine if a web resource is cacheable.
     * <p>
     * This determines whether we store in the file cache or not, as well as dictating 304 behaviour.
     *
     * @return if the {@link ResourceUtils#STATIC_HASH} is set and cache does not equal false.
     */
    public static boolean canRequestedResourcesContentBeAssumedConstant(final Map<String, String> params) {
        final boolean nocache = "false".equals(params.get("cache"));
        final boolean nohash = !params.containsKey(STATIC_HASH);
        return !(nohash || nocache);
    }

    /**
     * Allows HTTP request filters to opt-out of some WRM URL validation on a per-request basis.
     * <p>
     * This determines whether the WRM may respond to HTTP requests with non-200 values if
     * any validations fail.
     *
     * @param params key-value pairs derived from the HTTP request to check.
     * @return false if the HTTP request has a {@link ResourceUtils#WRM_INTEGRITY} attribute with a `no-validate` value,
     *         otherwise true.
     */
    public static boolean shouldValidateRequest(final Map<String, String> params) {
        final String val = params.get(WRM_INTEGRITY);
        if (isNotEmpty(val)) {
            return !"no-validate".equals(val);
        }
        return true;
    }
}
