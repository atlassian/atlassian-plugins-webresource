package com.atlassian.plugin.webresource.impl.annotators;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashSet;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

/**
 * Add comment with information about the single resource included in batch.
 *
 * @since 3.3
 */
public class LocationContentAnnotator extends ResourceContentAnnotator {
    @Override
    public int beforeResourceInBatch(
            LinkedHashSet<String> requiredResources,
            Resource resource,
            final Map<String, String> params,
            OutputStream out)
            throws IOException {
        out.write(String.format("/* module-key = '%s', location = '%s' */\n", resource.getKey(), resource.getLocation())
                .getBytes());
        return 1;
    }

    @Override
    public int hashCode() {
        return getClass().getName().hashCode();
    }
}
