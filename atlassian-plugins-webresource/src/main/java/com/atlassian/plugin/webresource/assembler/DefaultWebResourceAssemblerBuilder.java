package com.atlassian.plugin.webresource.assembler;

import java.util.Optional;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerBuilder;

/**
 * Implementation of WebResourceAssemblerBuilder
 *
 * @since v3.0
 */
public class DefaultWebResourceAssemblerBuilder implements WebResourceAssemblerBuilder {
    private final Globals globals;
    // Using optional to determine whether includeSuperbatchResources was invoked or not
    private Optional<Boolean> isSuperBatchingEnabled = Optional.empty();
    private Optional<Boolean> isSyncBatchingEnabled = Optional.empty();
    private Optional<Boolean> isAutoIncludeFrontendRuntimeEnabled = Optional.empty();

    public DefaultWebResourceAssemblerBuilder(final Globals globals) {
        this.globals = globals;
    }

    @Override
    public WebResourceAssembler build() {
        final UrlBuildingStrategy urlStrat = UrlBuildingStrategy.normal();
        final boolean autoIncludeFrontendRuntime = isAutoIncludeFrontendRuntimeEnabled.orElse(true);
        final RequestState requestState = new RequestState(globals, urlStrat, autoIncludeFrontendRuntime);

        final DefaultWebResourceAssembler assembler = new DefaultWebResourceAssembler(requestState, globals);

        isSyncBatchingEnabled.ifPresent(requestState::setSyncbatchEnabled);
        isSuperBatchingEnabled.ifPresent(requestState.getSuperbatchConfiguration()::setEnabled);

        return assembler;
    }

    /**
     * @deprecated since 5.5.0. Use {@link com.atlassian.webresource.api.assembler.RequiredResources#excludeSuperbatch()} instead.
     */
    @Deprecated
    @Override
    public WebResourceAssemblerBuilder includeSuperbatchResources(boolean include) {
        this.isSuperBatchingEnabled = Optional.of(include);
        return this;
    }

    @Override
    public WebResourceAssemblerBuilder includeSyncbatchResources(boolean include) {
        this.isSyncBatchingEnabled = Optional.of(include);
        return this;
    }

    @Override
    public WebResourceAssemblerBuilder autoIncludeFrontendRuntime(boolean include) {
        this.isAutoIncludeFrontendRuntimeEnabled = Optional.of(include);
        return this;
    }
}
