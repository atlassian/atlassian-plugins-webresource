package com.atlassian.plugin.webresource.impl.helpers.url;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.ResourceKey;
import com.atlassian.plugin.webresource.impl.ResourceKeysSupplier;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

import static org.apache.commons.lang3.StringUtils.join;

import static com.atlassian.plugin.webresource.impl.RequestCache.toResourceKeys;
import static com.atlassian.plugin.webresource.impl.config.Config.BATCH_TYPES;
import static com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers.resourcesOfType;

/**
 * Data structure representing Context Batch, it is used in cache and shouldn't contain any references.
 */
public class ContextBatch {
    private final List<String> included;
    private final LinkedHashSet<String> excluded;
    private final List<String> excludedWithoutApplyingConditions;
    private final boolean isAdditionalSortingRequired;
    private final List<String> skippedWebResourcesWithUrlReadingConditions;
    private final Map<String, ResourceKeysSupplier> standaloneResourceKeysByType;
    private final List<SubBatch> subBatches;

    public ContextBatch(
            final List<String> included,
            final LinkedHashSet<String> excluded,
            final List<String> skippedWebResourcesWithUrlReadingConditions,
            final List<String> excludedWithoutApplyingConditions,
            final List<SubBatch> subBatches,
            final List<Resource> standaloneResources,
            final boolean isAdditionalSortingRequired) {
        this.included = included;
        this.excluded = excluded;
        this.excludedWithoutApplyingConditions = excludedWithoutApplyingConditions;
        this.skippedWebResourcesWithUrlReadingConditions = skippedWebResourcesWithUrlReadingConditions;
        this.subBatches = subBatches;

        standaloneResourceKeysByType = new HashMap<>();
        // Instead of storing the reference to the resources storing its keys.
        for (final String type : BATCH_TYPES) {
            final List<Resource> resourcesOfType = resourcesOfType(standaloneResources, type);
            final List<ResourceKey> resourcesKey = toResourceKeys(resourcesOfType);
            final ResourceKeysSupplier resourceKeysSupplier = new ResourceKeysSupplier(resourcesKey);
            standaloneResourceKeysByType.put(type, resourceKeysSupplier);
        }
        this.isAdditionalSortingRequired = isAdditionalSortingRequired;
    }

    public List<Resource> getStandaloneResourcesOfType(final RequestCache requestCache, final String type) {
        return requestCache.getCachedResources(standaloneResourceKeysByType.get(type));
    }

    public List<String> getIncluded() {
        return included;
    }

    public LinkedHashSet<String> getExcluded() {
        return excluded;
    }

    public List<String> getSkippedWebResourcesWithUrlReadingConditions() {
        return skippedWebResourcesWithUrlReadingConditions;
    }

    public List<String> getExcludedWithoutApplyingConditions() {
        return excludedWithoutApplyingConditions;
    }

    public List<SubBatch> getSubBatches() {
        return subBatches;
    }

    public boolean isAdditionalSortingRequired() {
        return isAdditionalSortingRequired;
    }

    @Override
    public String toString() {
        final StringBuilder buffer = new StringBuilder();
        buffer.append(join(included, ", "));
        if (!excluded.isEmpty()) {
            buffer.append('-');
            buffer.append(join(excluded, ", "));
        }
        return buffer.toString();
    }
}
