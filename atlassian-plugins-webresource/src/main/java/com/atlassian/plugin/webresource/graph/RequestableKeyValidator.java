package com.atlassian.plugin.webresource.graph;

import java.util.Set;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.webresource.models.WebResourceKey;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * Determines whether a loosely-typed input conforms to the rules of a specific {@code Requestable} sub-type.
 *
 * @since 5.1.0
 */
public class RequestableKeyValidator {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestableKeyValidator.class);

    /**
     * This symbol describes if the a certain {@link String} represents a {@link WebResourceKey}.
     */
    private static final String WEB_RESOURCE_SYMBOL = ":";

    private final Set<String> rootPageKeys;

    public RequestableKeyValidator(@Nonnull final Set<String> rootPageKeys) {
        this.rootPageKeys = requireNonNull(rootPageKeys, "The root page keys are mandatory.");
    }

    private static boolean isWebResourceKey(@Nonnull final String requestableKey) {
        return requestableKey.contains(WEB_RESOURCE_SYMBOL);
    }

    /**
     * Verifies whether a resource key represents a resource context or not.
     *
     * @param requestableDependencyKey The requestable resource key.
     * @return <p>{@code true}: The web resource key is a web resource context.</p>
     * <p>{@code false}: The web resource key is not a web resource context.</p>
     */
    public static boolean isWebResourceContext(@Nonnull final String requestableDependencyKey) {
        requireNonNull(requestableDependencyKey, "The requestable dependency key is mandatory.");
        if (isWebResourceKey(requestableDependencyKey)) {
            final String message = format("Requestable key '%s' looks like a web resource.", requestableDependencyKey);
            LOGGER.debug(message);
            return false;
        }
        return true;
    }

    /**
     * Verifies whether a resource key represents a web resource or not.
     *
     * @param requestableDependencyKey The requestable resource key.
     * @return <p>{@code true}: The web resource key is a web resource context.</p>
     * <p>{@code false}: The web resource key is not a web resource context.</p>
     */
    public boolean isWebResource(@Nonnull final String requestableDependencyKey) {
        requireNonNull(requestableDependencyKey, "The web resource key is mandatory.");
        if (rootPageKeys.contains(requestableDependencyKey)) {
            final String message = format("Requestable key '%s' is tagged as a root-page.", requestableDependencyKey);
            LOGGER.debug(message);
            return false;
        }
        return isWebResourceKey(requestableDependencyKey);
    }
}
