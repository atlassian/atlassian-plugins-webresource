package com.atlassian.plugin.webresource.impl.helpers.url;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Sets;

import com.atlassian.plugin.webresource.impl.config.Config;

import static com.google.common.collect.Iterables.getOnlyElement;

import static com.atlassian.plugin.webresource.impl.config.Config.PARAMS_SORT_ORDER;

/**
 * Comparator for sorting sub batches.
 */
public final class ParamsComparator implements Comparator<Map<String, String>> {
    private static final HashSet<String> PARAMS_SORT_ORDER_SET = new HashSet<>(PARAMS_SORT_ORDER);

    /**
     * The only reason this exists it to preserve backward compatibility when sub-batches where sorted by its urls.
     */
    private boolean isAdditionalSortingRequired;

    /**
     * Keys will be sorted according to {@link Config#PARAMS_SORT_ORDER}.
     */
    @Override
    public int compare(final Map<String, String> params1, final Map<String, String> params2) {
        // The fewer conditions, the high phase
        final Set<String> keys1 = Sets.intersection(params1.keySet(), PARAMS_SORT_ORDER_SET);
        final Set<String> keys2 = Sets.intersection(params2.keySet(), PARAMS_SORT_ORDER_SET);

        if (keys1.size() == 1 && keys2.size() == 1) {
            final String key1 = getOnlyElement(keys1);
            final String key2 = getOnlyElement(keys2);

            final int key1Index = PARAMS_SORT_ORDER.indexOf(key1);
            final int key2Index = PARAMS_SORT_ORDER.indexOf(key2);
            if (key1Index != key2Index) {
                return key1Index - key2Index;
            }
        }

        if (keys1.size() == keys2.size()) {
            /*
             * In old implementation sub batches where sorted by its urls, technically it should be
             * safe to sort it by query string only, as written below, but in order to ease
             * backward compatibility testing temporarily it is disabled and 100% backward compatibility
             * maintained, after some time this code should be uncommented and `isAdditionalSortingRequired`
             * stuff should be removed.
             */
            isAdditionalSortingRequired |= true;
            return 0;
        }
        return keys1.size() - keys2.size();
    }

    public boolean isAdditionalSortingRequired() {
        return isAdditionalSortingRequired;
    }
}
