package com.atlassian.plugin.webresource.impl.snapshot;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.webresource.api.transformer.TransformerParameters;

/**
 * Web Resource.
 */
public class WebResource extends Bundle {
    private final TransformerParameters transformerParameters;
    private final Map<String, Set<String>> locationResourceTypes;

    public WebResource(
            Snapshot snapshot,
            String key,
            List<String> dependencies,
            Date updatedAt,
            String version,
            boolean isTransformable,
            TransformerParameters transformerParameters,
            Map<String, Set<String>> locationResourceTypes) {
        super(snapshot, key, dependencies, updatedAt, version, isTransformable);
        this.transformerParameters = transformerParameters;
        this.locationResourceTypes = locationResourceTypes;
    }

    @Override
    public LinkedHashMap<String, Resource> getResources(RequestCache cache) {
        LinkedHashMap<String, Resource> resources = cache.getCachedResources().get(this);
        if (resources == null) {
            resources = snapshot.config.getResourcesWithoutCache(this);
            cache.getCachedResources().put(this, resources);
        }
        return resources;
    }

    @Override
    public Set<String> getLocationResourceTypesFor(String nameType) {
        Set<String> types = locationResourceTypes.get(nameType);
        if (types == null) {
            types = new HashSet<>();
        }
        return types;
    }

    @Override
    public CachedCondition getCondition() {
        return snapshot.conditions().get(this);
    }

    @Override
    public CachedTransformers getTransformers() {
        return snapshot.transformers().get(this);
    }

    @Override
    public TransformerParameters getTransformerParameters() {
        return transformerParameters;
    }

    @Override
    public LinkedHashMap<String, Jsonable> getData() {
        return snapshot.config.getWebResourceData(getKey());
    }

    /**
     * Instead of storing plugin and web resource kyes and taking memory, evaluating it each time it is used.
     * It used during transformation only.
     *
     * @deprecated since v3.3.2
     */
    @Deprecated
    public String getWebResourceKey() {
        return getKey().split(":")[1];
    }

    /**
     * @deprecated since v3.3.2
     */
    @Deprecated
    public String getPluginKey() {
        return getKey().split(":")[0];
    }
}
