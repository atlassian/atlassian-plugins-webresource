package com.atlassian.plugin.webresource;

import com.atlassian.webresource.api.WebResourceFilter;

/**
 * A Web Resource Filter that allows for css and javascript resources.
 * <p>
 * This is the default filter used by the {@link WebResourceManagerImpl} for include/get resource methods that do
 * not accept a filter as a parameter.
 *
 * @since 2.4
 */
public class DefaultWebResourceFilter implements WebResourceFilter {
    private final JavascriptWebResource javascriptWebResource;
    private final CssWebResource cssWebResource;

    public DefaultWebResourceFilter() {
        this.javascriptWebResource = new JavascriptWebResource();
        this.cssWebResource = new CssWebResource();
    }

    public boolean matches(String resourceName) {
        return javascriptWebResource.matches(resourceName) || cssWebResource.matches(resourceName);
    }
}
