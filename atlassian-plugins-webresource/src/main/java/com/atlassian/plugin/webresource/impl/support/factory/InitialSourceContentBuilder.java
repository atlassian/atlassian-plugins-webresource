package com.atlassian.plugin.webresource.impl.support.factory;

import java.io.IOException;
import java.io.InputStream;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.impl.support.InitialContent;
import com.atlassian.sourcemap.ReadableSourceMap;

import static java.lang.String.format;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import static com.atlassian.plugin.webresource.impl.snapshot.resource.Resource.getPrebuiltSourcePath;
import static com.atlassian.plugin.webresource.impl.support.Support.LOGGER;
import static com.atlassian.plugin.webresource.impl.support.Support.getSourceMap;

/**
 * Represents a class responsible for building a {@link InitialContent} based on the source version of a {@link Resource}.
 *
 * @since 5.0.0
 */
class InitialSourceContentBuilder {
    private InputStream content;
    private String path;
    private ReadableSourceMap sourceMap;

    @Nonnull
    InitialSourceContentBuilder withContent(@Nonnull final Resource resource) {
        if (nonNull(resource.getLocation())) {
            final String prebuildSourcePath = getPrebuiltSourcePath(resource.getLocation());
            try (final InputStream sourceStream = resource.getStreamFor(prebuildSourcePath)) {
                content = sourceStream;
            } catch (final IOException exception) {
                final String message = format(
                        "Error while reading the source file for the resource with location %s.",
                        resource.getLocation());
                LOGGER.warn(message, exception);
            }
        }
        return this;
    }

    @Nonnull
    InitialSourceContentBuilder withPath(@Nonnull final Router router, @Nonnull final Resource resource) {
        if (isNull(content)) {
            withContent(resource); // that is used to guarantee that there was an attempt of content lookup.
        }
        path = isNull(content) ? router.sourceUrl(resource) : router.prebuildSourceUrl(resource);
        return this;
    }

    @Nonnull
    InitialSourceContentBuilder withSourceMap(@Nonnull final Resource resource) {
        sourceMap = getSourceMap(resource.getPath(), resource, path);
        return this;
    }

    @Nonnull
    InitialContent build() {
        return new InitialSourceContent(content, path, sourceMap);
    }
}
