package com.atlassian.plugin.webresource.legacy;

import java.util.List;

/**
 * Assists in locating plugin resources in different ways.
 *
 * @since 2.2
 */
public interface PluginResourceLocator {
    /**
     * Returns a list of {@link com.atlassian.plugin.webresource.legacy.PluginResource}s for a given plugin module's complete key. If
     * the plugin the module belongs to is not enabled or does not exist, an empty list is returned.
     */
    List<PluginResource> getPluginResources(String moduleCompleteKey);
}
