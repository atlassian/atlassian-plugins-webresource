package com.atlassian.plugin.webresource.transformer.instance;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerMatcher;

import static com.atlassian.plugin.webresource.impl.config.Config.CSS_EXTENSION;
import static com.atlassian.plugin.webresource.impl.config.Config.CSS_TYPE;
import static com.atlassian.plugin.webresource.impl.config.Config.LESS_EXTENSION;
import static com.atlassian.plugin.webresource.impl.config.Config.LESS_TYPE;

/**
 * {@link com.atlassian.plugin.webresource.transformer.WebResourceTransformerMatcher} for relative url resources
 *
 * @since v3.1.0
 */
public class RelativeUrlTransformerMatcher implements WebResourceTransformerMatcher {
    @Override
    public boolean matches(String type) {
        if (type != null) {
            final String fileType = type.toLowerCase();
            return fileType.equals(CSS_TYPE) || fileType.equals(LESS_TYPE);
        } else {
            return false;
        }
    }

    @Override
    public boolean matches(ResourceLocation resourceLocation) {
        if (resourceLocation.getName() != null) {
            final String location = resourceLocation.getName().toLowerCase();
            return location.endsWith(CSS_EXTENSION) || location.endsWith(LESS_EXTENSION);
        }

        return false;
    }
}
