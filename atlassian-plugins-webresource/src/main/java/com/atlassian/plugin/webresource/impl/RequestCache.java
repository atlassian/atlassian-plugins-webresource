package com.atlassian.plugin.webresource.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

/**
 * A place to store the results of operations performed during a single request-response lifecycle.
 *
 * The cache's primary goal is to "freeze" application state as it is interrogated, so that values
 * are evaluated once and thus stable for the duration of the request-response lifecycle.
 * Avoiding potentially expensive operations being run multiple times is a nice bonus.
 *
 * This class expects to be constructed per request. It is not an appropriate way to marshal and unmarshal
 * application state across multiple requests from a client.
 *
 * @since v3.3
 */
public class RequestCache {
    private Snapshot cachedSnapshot;
    private final Map<CachedCondition, Boolean> cachedConditionsEvaluation;
    private final Map<CachedCondition, DefaultUrlBuilder> cachedConditionsParameters;
    private final Map<Bundle, LinkedHashMap<String, Resource>> cachedResources;
    private final Map<ResourceKeysSupplier, List<Resource>> cachedResourceLists;
    private final Globals globals;

    public RequestCache(final Globals globals) {
        this.globals = globals;
        cachedResources = new HashMap<>();
        cachedResourceLists = new HashMap<>();
        cachedConditionsEvaluation = new HashMap<>();
        cachedConditionsParameters = new HashMap<>();
    }

    public static List<ResourceKey> toResourceKeys(final List<Resource> resources) {
        return resources.stream().map(ResourceKey::new).collect(toList());
    }

    public Globals getGlobals() {
        return globals;
    }

    /**
     * Needed by WebResource as a storage for Cache.
     */
    public Map<Bundle, LinkedHashMap<String, Resource>> getCachedResources() {
        return cachedResources;
    }

    /**
     * Batches and Sub Batches are cached and as such can't contain references to Resources, this cache helps with that.
     */
    public List<Resource> getCachedResources(final ResourceKeysSupplier resourceKeysSupplier) {
        return ofNullable(cachedResourceLists.get(resourceKeysSupplier)).orElseGet(() -> {
            final List<Resource> resources = resourceKeysSupplier.getKeys().stream()
                    .map(key ->
                            cachedSnapshot.get(key.getKey()).getResources(this).get(key.getName()))
                    .collect(toList());
            cachedResourceLists.put(resourceKeysSupplier, resources);
            return resources;
        });
    }

    public Map<CachedCondition, Boolean> getCachedConditionsEvaluation() {
        return cachedConditionsEvaluation;
    }

    public Map<CachedCondition, DefaultUrlBuilder> getCachedConditionsParameters() {
        return cachedConditionsParameters;
    }

    /**
     * Get all bundles.
     * <p>
     * It is another layer of cache over the `globals.getBundles()` because it is used very
     * heavily, to avoid any performance drawbacks of atomic reference in the `globals.getBundles()`.
     * </p>
     */
    public Snapshot getSnapshot() {
        cachedSnapshot = ofNullable(cachedSnapshot).orElseGet(globals::getSnapshot);
        return cachedSnapshot;
    }
}
