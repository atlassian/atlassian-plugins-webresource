package com.atlassian.plugin.webresource.impl.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class FileOperations {
    public boolean exists(String path) {
        return new File(path).exists();
    }

    public InputStream createInputStream(String path) throws FileNotFoundException {
        return new FileInputStream(path);
    }
}
