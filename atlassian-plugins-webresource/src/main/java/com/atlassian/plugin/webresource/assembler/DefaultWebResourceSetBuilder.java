package com.atlassian.plugin.webresource.assembler;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Set;
import javax.annotation.Nonnull;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.SuperbatchConfiguration;
import com.atlassian.plugin.webresource.impl.helpers.ResourceGenerationInfo;
import com.atlassian.plugin.webresource.impl.helpers.data.ResourceDataGenerator;
import com.atlassian.plugin.webresource.impl.helpers.url.Resolved;
import com.atlassian.plugin.webresource.impl.helpers.url.ResourceUrlGenerator;
import com.atlassian.plugin.webresource.models.SuperBatchKey;
import com.atlassian.plugin.webresource.models.SyncBatchKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.api.data.PluginDataResource;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Stream.of;

import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INLINE;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;

public class DefaultWebResourceSetBuilder {
    @VisibleForTesting
    static final WebResourceKey WEB_RESOURCE_MANAGER_RESOURCE =
            new WebResourceKey("com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager");

    @VisibleForTesting
    static final WebResourceKey DATA_COLLECTOR_OBSERVER_RESOURCE =
            new WebResourceKey("com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-perf-observer");

    @VisibleForTesting
    static final WebResourceKey DATA_COLLECTOR_ASYNC_RESOURCE =
            new WebResourceKey("com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async");

    private final RequestState requestState;
    private boolean cleanUpAfterInclude;
    private boolean addWebResourceJavascriptApiDependencies;
    private boolean isSuperbatchEnabled = false;

    public DefaultWebResourceSetBuilder(@Nonnull final RequestState requestState) {
        this.requestState = requireNonNull(
                requestState, "The request state is mandatory for building the DefaultWebResourceSetBuilder.");
        cleanUpAfterInclude = false;
        addWebResourceJavascriptApiDependencies = true;
    }

    @Nonnull
    public DefaultWebResourceSetBuilder disableCleanUpAfterInclude() {
        cleanUpAfterInclude = false;
        return this;
    }

    @Nonnull
    public DefaultWebResourceSetBuilder enableCleanUpAfterInclude() {
        cleanUpAfterInclude = true;
        return this;
    }

    @Nonnull
    public DefaultWebResourceSetBuilder disableAdditionOfWebResourceJavascriptApiDependencies() {
        addWebResourceJavascriptApiDependencies = false;
        return this;
    }

    @Nonnull
    public DefaultWebResourceSetBuilder enableAdditionOfWebResourceJavascriptApiDependencies() {
        addWebResourceJavascriptApiDependencies = true;
        return this;
    }

    @Nonnull
    public DefaultWebResourceSetBuilder enableSuperbatch() {
        isSuperbatchEnabled = true;

        return this;
    }

    @Nonnull
    public DefaultWebResourceSetBuilder disableSuperbatch() {
        isSuperbatchEnabled = false;

        return this;
    }

    @Nonnull
    public DefaultWebResourceSet build() {
        // Add superbatch
        SuperbatchConfiguration superbatchConfiguration = requestState.getSuperbatchConfiguration();
        if (isSuperbatchEnabled && superbatchConfiguration.isEnabled()) {
            requestState
                    .getRawRequest()
                    .includeFirst(superbatchConfiguration.getResourcePhase(), SuperBatchKey.getInstance());
        }

        // Add syncbatch
        if (requestState.isSyncbatchEnabled()) {
            requestState.getRawRequest().includeFirst(INLINE, SyncBatchKey.getInstance());
        }

        // Add frontend runtime modules as needed
        if (addWebResourceJavascriptApiDependencies && requestState.isAutoIncludeFrontendRuntimeEnabled()) {
            // TODO context batches get served before web-resource batches. If anything in superbatch needs this, it
            // will load too late.
            //      we need that bootstrap phase.

            // This part must be before checking if any interaction will be used, because it uses interaction phase.
            if (requestState.getGlobals().getConfig().isPerformanceTrackingEnabled()) {
                requestState.getRawRequest().include(INLINE, DATA_COLLECTOR_OBSERVER_RESOURCE);
                requestState.getRawRequest().include(INTERACTION, DATA_COLLECTOR_ASYNC_RESOURCE);
            }

            // include WRM.require / WRM.requireLazily if the interaction phase will be used.
            if (requestState.getRawRequest().hasAny(ResourcePhase.INTERACTION)) {
                requestState
                        .getRawRequest()
                        .includeFirst(superbatchConfiguration.getResourcePhase(), WEB_RESOURCE_MANAGER_RESOURCE);
            }
        }

        // From earliest to latest resource phase, convert the request state in to topologically sorted lists
        // of either the batch content to serve or the URLs that will retrieve them later.
        final Deque<WebResourceInformation> webResourceInformation =
                of(ResourcePhase.values()).map(this::buildDependenciesByPhase).collect(toCollection(ArrayDeque::new));
        return new DefaultWebResourceSet(requestState.getGlobals().getConfig(), requestState, webResourceInformation);
    }

    /**
     * Retrieves all the dependencies based on all the included resources for a certain phase.
     *
     * @param resourcePhase The resource phase to be used to retrieve the included resources.
     * @return The object containing the information of all dependencies.
     */
    private WebResourceInformation buildDependenciesByPhase(final ResourcePhase resourcePhase) {

        final ResourceGenerationInfo resourceGenerationInfo = new ResourceGenerationInfo(resourcePhase, requestState);

        final Resolved resolved =
                new ResourceUrlGenerator(requestState.getGlobals().getUrlCache()).generate(resourceGenerationInfo);

        // Assembling data resources.
        final Set<PluginDataResource> pluginDataResources =
                new ResourceDataGenerator().generate(resourceGenerationInfo);

        if (cleanUpAfterInclude) {
            requestState.clearIncludedAndUpdateExcluded(resourcePhase, resolved.getExcludedResolved());
        }

        return new WebResourceInformation(pluginDataResources, resourcePhase, resolved.getUrls());
    }
}
