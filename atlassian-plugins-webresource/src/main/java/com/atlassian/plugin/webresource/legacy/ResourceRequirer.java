package com.atlassian.plugin.webresource.legacy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Iterables;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;

import static com.google.common.collect.Iterables.transform;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.3
 */
public class ResourceRequirer {
    public final PluginResourceLocator pluginResourceLocator;
    private final ResourceDependencyResolver dependencyResolver;
    private final boolean isSuperBatchingEnabled;
    private final boolean includeDependenciesForFailedUrlReadingConditions;

    public ResourceRequirer(
            PluginResourceLocator pluginResourceLocator,
            ResourceDependencyResolver dependencyResolver,
            boolean isSuperBatchingEnabled,
            boolean includeDependenciesForFailedUrlReadingConditions) {
        this.pluginResourceLocator = pluginResourceLocator;
        this.dependencyResolver = dependencyResolver;
        this.isSuperBatchingEnabled = isSuperBatchingEnabled;
        this.includeDependenciesForFailedUrlReadingConditions = includeDependenciesForFailedUrlReadingConditions;
    }

    public Collection<PluginResource> includeResources(
            final RequestCache requestCache,
            final UrlBuildingStrategy urlBuildingStrategy,
            final Set<String> requiredWebResources,
            final Set<String> requiredContexts,
            final InclusionState inclusion) {
        final List<PluginResource> resourcesToInclude = new LinkedList<>();

        // Add superbatch
        addSuperBatchResources(resourcesToInclude, inclusion);

        // Add contexts
        addContextBatchDependencies(
                requestCache,
                urlBuildingStrategy,
                resourcesToInclude,
                requiredWebResources,
                requiredContexts,
                inclusion);

        // Add webresources
        Iterable<String> dependencyModuleKeys =
                getAllModuleKeysDependencies(requestCache, urlBuildingStrategy, requiredWebResources);
        addModuleResources(resourcesToInclude, dependencyModuleKeys, inclusion.webresources);

        // Add contexts, webresources & data to inclusion state
        Iterables.addAll(inclusion.contexts, requiredContexts);
        Iterables.addAll(inclusion.webresources, dependencyModuleKeys);

        return resourcesToInclude;
    }

    private void addSuperBatchResources(List<PluginResource> resourcesToInclude, InclusionState inclusion) {
        if (inclusion.superbatch || !isSuperBatchingEnabled) {
            return;
        }
        inclusion.superbatch = true;
        Iterables.addAll(
                resourcesToInclude,
                new SuperBatchBuilder(dependencyResolver, pluginResourceLocator, inclusion).build());
    }

    private void addContextBatchDependencies(
            RequestCache requestCache,
            UrlBuildingStrategy urlBuildingStrategy,
            List<PluginResource> resourcesToInclude,
            Set<String> requiredWebResources,
            Set<String> requiredContexts,
            InclusionState inclusion) {
        final ContextBatchBuilder builder = new ContextBatchBuilder(
                dependencyResolver, isSuperBatchingEnabled, includeDependenciesForFailedUrlReadingConditions);

        Iterable<PluginResource> contextResources =
                builder.buildBatched(requestCache, urlBuildingStrategy, new ArrayList<>(requiredContexts), inclusion);

        Iterables.addAll(resourcesToInclude, contextResources);
        Iterables.addAll(inclusion.webresources, builder.getAllIncludedResources());
        Iterables.addAll(requiredWebResources, builder.getSkippedResources());
    }

    private Iterable<String> getAllModuleKeysDependencies(
            RequestCache requestCache, UrlBuildingStrategy urlBuildingStrategy, Iterable<String> moduleCompleteKeys) {
        final Set<String> dependencyModuleCompleteKeys = new LinkedHashSet<>();
        for (final String moduleCompleteKey : moduleCompleteKeys) {
            final Iterable<String> dependencies = toModuleKeys(dependencyResolver.getDependencies(
                    requestCache,
                    urlBuildingStrategy,
                    moduleCompleteKey,
                    isSuperBatchingEnabled,
                    includeDependenciesForFailedUrlReadingConditions));
            Iterables.addAll(dependencyModuleCompleteKeys, dependencies);
        }
        return dependencyModuleCompleteKeys;
    }

    private void addModuleResources(
            final List<PluginResource> resourcesToInclude,
            final Iterable<String> dependencyModuleCompleteKeys,
            final Set<String> excludeModuleKeys) {
        for (final String moduleKey : dependencyModuleCompleteKeys) {
            if (excludeModuleKeys.contains(moduleKey)) {
                // skip this resource if it is already visited
                continue;
            }

            for (final PluginResource moduleResource : pluginResourceLocator.getPluginResources(moduleKey)) {
                resourcesToInclude.add(moduleResource);
            }
        }
    }

    private Iterable<String> toModuleKeys(final Iterable<ModuleDescriptorStub> descriptors) {
        return transform(descriptors, new TransformDescriptorToKey());
    }
}
