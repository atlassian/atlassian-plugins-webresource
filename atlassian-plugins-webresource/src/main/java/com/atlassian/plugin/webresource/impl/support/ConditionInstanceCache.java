package com.atlassian.plugin.webresource.impl.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.plugin.webresource.condition.DecoratingAndCompositeCondition;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.condition.DecoratingUrlReadingCondition;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

/**
 * Instance cache, needed to have only one instance of UrlReadingCondition for same UrlReadingConditon class and
 * initialization parameters.
 *
 * @since v3.3
 */
public class ConditionInstanceCache {
    public static final boolean CONDITION_INTERN_ENABLED =
            !Boolean.getBoolean("com.atlassian.plugin.webresource.disableconditionintern");

    Map<Set<Tuple<Class<? extends UrlReadingCondition>, Map<String, String>>>, CachedCondition> instances =
            new HashMap<>();

    public CachedCondition intern(DecoratingCondition condition) {
        // Conditions other than DecoratingAndCompositeCondition are ignored and not optimized.
        //
        // Not that the `instanceof` can't be used, the strong equality should be used, otherwise we would
        // mistakenly cache different implementations as if they are the same.
        if (CONDITION_INTERN_ENABLED && (DecoratingAndCompositeCondition.class.equals(condition.getClass()))) {
            List<DecoratingCondition> conditions = ((DecoratingAndCompositeCondition) condition).getConditions();
            List<DecoratingUrlReadingCondition> urlReadingConditions = new ArrayList<>();
            for (DecoratingCondition c : conditions) {
                // Conditions other than DecoratingUrlReadingCondition are ignored and not optimized.
                if (c instanceof DecoratingUrlReadingCondition) {
                    urlReadingConditions.add((DecoratingUrlReadingCondition) c);
                }
            }

            // Can be interned only if all conditions are UrlReadingCondition.
            if (urlReadingConditions.size() == conditions.size()) {
                // Building the key, conditions with the same key are the same and could be interned.
                Set<Tuple<Class<? extends UrlReadingCondition>, Map<String, String>>> key = new HashSet<>();
                for (DecoratingUrlReadingCondition c : urlReadingConditions) {
                    key.add(new Tuple<>(c.getUrlReadingCondition().getClass(), c.getParams()));
                }

                // Replacing multiple instances of conditions with one.
                CachedCondition cachedCondition = instances.get(key);
                if (cachedCondition == null) {
                    cachedCondition = new CachedCondition(condition);
                    instances.put(key, cachedCondition);
                }
                return cachedCondition;
            } else {
                return new CachedCondition(condition);
            }
        } else {
            return new CachedCondition(condition);
        }
    }
}
