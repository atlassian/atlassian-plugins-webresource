package com.atlassian.plugin.webresource.legacy;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;

/**
 * Stub to plug modules into existing WRM system.
 *
 * @since v3.4.2
 */
public class ModuleDescriptorStub extends AbstractModuleDescriptor<Void> {
    private String completeKey;

    public ModuleDescriptorStub(WebResourceModuleDescriptor descriptor) {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
        this.completeKey = descriptor.getCompleteKey();
    }

    @Override
    public String getCompleteKey() {
        return completeKey;
    }

    @Override
    public Void getModule() {
        throw new UnsupportedOperationException("Not implemented");
    }
}
