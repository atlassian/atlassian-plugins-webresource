package com.atlassian.plugin.webresource.models;

import javax.annotation.Nonnull;

/**
 * Represents a single web-resource key.
 *
 * @since 5.0.0
 */
public class WebResourceKey extends Requestable {

    public WebResourceKey(@Nonnull final String key) {
        super(key);
    }

    @Override
    public String toString() {
        return String.format("<wr!%s>", getKey());
    }
}
