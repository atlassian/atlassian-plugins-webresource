package com.atlassian.plugin.webresource.impl.discovery;

/**
 * What the bundle walker should do when a deep filter predicate fails.
 * @since 5.4.3
 */
public enum PredicateFailStrategy {
    /**
     * The walker should stop traversing the graph at this node.
     */
    STOP,
    /**
     * The walker should continue to traverse the graph from this node.
     */
    CONTINUE
}
