package com.atlassian.plugin.webresource;

import java.io.File;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.api.assembler.resource.CompleteWebResourceKey;

/**
 * The integration points between the Web Resource layer, and specific applications (eg JIRA, Confluence).
 *
 * @see com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService#DefaultPageBuilderService(WebResourceIntegration, com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory)
 * @see WebResourceManagerImpl#WebResourceManagerImpl(com.atlassian.plugin.webresource.impl.Globals, WebResourceIntegration, WebResourceUrlProvider)
 */
public interface WebResourceIntegration {
    /**
     * Applications must implement this method to get access to the application's ApplicationProperties
     */
    ApplicationProperties getApplicationProperties();

    /**
     * Applications must implement this method to get access to the application's PluginAccessor
     */
    PluginAccessor getPluginAccessor();

    /**
     * This must be a thread-local cache that will be accessible from both the page, and the decorator
     */
    Map<String, Object> getRequestCache();

    /**
     * Represents the unique string for this system, which when updated will flush the cache.
     *
     * @return A string representing the count
     * @deprecated since 5.0 implement {@link #getResourceUrlPrefix()} instead
     */
    @Deprecated
    default String getSystemCounter() {
        return getResourceUrlPrefix();
    }

    /**
     * Represents the resource URL prefix used to construct URLs to resources. A correctly constructed prefix should change
     * when the content served under that URL changes. Some examples of configuration items that should result in the URL prefix
     * getting changed:
     * Look and Feel update in Jira (reason: this has impact on the generated CSS files), CDN URL change (reason: absolute link to the
     * CDN is stored in the CSS files).
     *
     * @return A string used as resource URL prefix
     */
    default String getResourceUrlPrefix() {
        return getSystemCounter();
    }

    /**
     * Represents the last time the system was updated.  This is generally obtained from BuildUtils or similar.
     */
    String getSystemBuildNumber();

    /**
     * The version number of the host application, for example "7.0.0-OD-07" or "5.6". It is intended to identify
     * if two instances of an application are the same version (the versions of plugins are checked separately).
     * This differs from {@link #getSystemBuildNumber()}, which does not necessarily change when the application changes
     * (at least in JIRA this represents the database build number, not the application version number).
     *
     * @since 3.4.7
     */
    String getHostApplicationVersion();

    /**
     * Returns the base URL for this application.  This method may return either an absolute or a relative URL.
     * Implementations are free to determine which mode to use based on any criteria of their choosing. For example, an
     * implementation may choose to return a relative URL if it detects that it is running in the context of an HTTP
     * request, and an absolute URL if it detects that it is not.  Or it may choose to always return an absolute URL, or
     * always return a relative URL.  Callers should only use this method when they are sure that either an absolute or
     * a relative URL will be appropriate, and should not rely on any particular observed behavior regarding how this
     * value is interpreted, which may vary across different implementations.
     * <p>
     * In general, the behavior of this method should be equivalent to calling {@link
     * #getBaseUrl(UrlMode)} with a {@code urlMode} value of {@link
     * UrlMode#AUTO}.
     *
     * @return the string value of the base URL of this application
     */
    String getBaseUrl();

    /**
     * Returns the base URL for this application in either relative or absolute format, depending on the value of {@code
     * urlMode}.
     * <p>
     * If {@code urlMode == {@link UrlMode#ABSOLUTE}}, this method returns an absolute URL, with URL
     * scheme, hostname, port (if non-standard for the scheme), and context path.
     * <p>
     * If {@code urlMode == {@link UrlMode#RELATIVE}}, this method returns a relative URL containing
     * just the context path.
     * <p>
     * If {@code urlMode == {@link UrlMode#AUTO}}, this method may return either an absolute or a
     * relative URL.  Implementations are free to determine which mode to use based on any criteria of their choosing.
     * For example, an implementation may choose to return a relative URL if it detects that it is running in the
     * context of an HTTP request, and an absolute URL if it detects that it is not.  Or it may choose to always return
     * an absolute URL, or always return a relative URL.  Callers should only use {@code
     * WebResourceManager.UrlMode#AUTO} when they are sure that either an absolute or a relative URL will be
     * appropriate, and should not rely on any particular observed behavior regarding how this value is interpreted,
     * which may vary across different implementations.
     *
     * @param urlMode specifies whether to use absolute URLs, relative URLs, or allow the concrete implementation to
     * decide
     * @return the string value of the base URL of this application
     * @since 2.3.0
     */
    String getBaseUrl(UrlMode urlMode);

    /**
     * This version number is used for caching URL generation, and needs to be incremented every time the contents
     * of the superbatch may have changed. Practically this means updating every time the plugin system state changes
     *
     * @return a version number
     */
    String getSuperBatchVersion();

    /**
     * The locale hash that should be inserted into static resource urls for the current request, if appropriate.
     * This method should return the hash for i18n-contributing plugins without the current locale attached to it.
     *
     * @return null if the url should not have a locale component.
     * @since 3.5.22
     */
    String getI18nStateHash();

    /**
     * A reference to the temporary directory the application want the plugin system to use. The temporary directory can
     * be cleared at any time by the application and can be used by the plugin system to cache things like batches or
     * other things that can easily be re-generated. It is recommended that this directory be /apphome/tmp/webresources.
     * The plugin system can delete any or all files it sees fit that exists under this directory at any time.
     * The directory does not need to exist.
     *
     * @return a File reference to the temporary directory. This can not return null.
     * @since 2.9.0
     */
    File getTemporaryDirectory();

    /**
     * Returns the CDNStrategy for serving resources via CDN. This may return null, in which case no resources should
     * be served via CDN.
     *
     * @return CDN strategy
     */
    CDNStrategy getCDNStrategy();

    /**
     * @return the current user's locale. Must return a default locale if none is found.
     */
    Locale getLocale();

    /**
     * @return the set of locales supported by this application. Used to pre-bake resources.
     * @since 3.4.7
     */
    Iterable<Locale> getSupportedLocales();

    /**
     * Retrieve the unformatted message text associated with this key.
     *
     * @param locale locale in which to look up keys
     * @param key key for the i18ned message
     * @return the unformatted message text if key is found for the given locale, otherwise returns key itself.
     */
    String getI18nRawText(Locale locale, String key);

    /**
     * Retrieve the message text associated with this key.
     *
     * @param locale locale in which to look up keys
     * @param key key for the i18ned message
     * @return the message text if key is found for the given locale, otherwise returns key itself.
     */
    String getI18nText(Locale locale, String key);

    /**
     * If incremental cache should be used. It's the experimental feature that potentially could slow down
     * the instance and should be disabled by default.
     * <p>
     * Please note that if it's enabled the size of the cache should be big enough. It caches the
     * transformed content of the individual JS / CSS files, and there are lots of such files. The size defined by
     * `plugin.webresource.incrementalcache.size` system property.
     * <p>
     * See https://extranet.atlassian.com/display/~apetrushin/Incremental+build+Cache+for+Context+Batch for details.
     *
     * @since 3.4.0
     */
    default boolean isIncrementalCacheEnabled() {
        return false;
    }

    /**
     * CONFDEV-35445 Should WRM enable the defer attribute in its JS tags?
     * Experimental Api
     *
     * @since 3.4.0
     * @deprecated since 5.0.0, use ResourcePriority.DEFER instead
     */
    @Deprecated
    default boolean isDeferJsAttributeEnabled() {
        return false;
    }

    /**
     * Synchronous resources will be inlined in the HTML response when
     * {@link com.atlassian.webresource.api.assembler.WebResourceSet#writeHtmlTags(Writer, com.atlassian.webresource.api.UrlMode)} gets called.
     * For example, analytics or AMD loader should be available for every other scripts.
     * Important note. Only JS files will be taken from specified web-resources and
     * other types of resources will be ignored. Warning will be logged in such case.
     * Any dependencies of specified web-resources will be also treated as sync and will be included
     * before those web-resources so be careful.
     * It's guaranteed that sync web-resources will be included only once, hence they will be also
     * excluded from other batches.
     *
     * @return list of complete resource keys
     * @since 3.5.27
     */
    default List<CompleteWebResourceKey> getSyncWebResourceKeys() {
        return new ArrayList<>();
    }

    /**
     * If true the plugin install time will be used instead of the plugin version for the SNAPSHOT plugins.
     * The plugin version is used when building the cache hash for URLs. for details see https://ecosystem.atlassian.net/browse/PLUGWEB-304
     * <p>
     * It's needed to simplify development and flush the cache when updated SNAPSHOT plugin installed.
     *
     * @since 3.5.9
     */
    default boolean usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins() {
        return false;
    }

    /**
     * Returns whether "compiled resources" are enabled. This will cause WRM to look for a 'compiledLocation' property
     * on a downloadable web resource. If the property is present, and this method returns true, then WRM will serve the
     * resource located on the path provided by 'compiledLocation' instead of at the path provided by 'location'.
     */
    default boolean isCompiledResourceEnabled() {
        return false;
    }

    /**
     * Applications must implement this method to get access to the analytic event publisher
     *
     * @since 5.2.0
     */
    @Nonnull
    EventPublisher getEventPublisher();

    /**
     * Used to separate the WRM from the details of how products deal with system properties
     *
     * @since 5.2.0
     */
    @Nonnull
    DarkFeatureManager getDarkFeatureManager();
}
