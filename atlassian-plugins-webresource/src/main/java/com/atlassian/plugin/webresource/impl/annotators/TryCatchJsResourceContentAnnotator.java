package com.atlassian.plugin.webresource.impl.annotators;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashSet;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

/**
 * This should only be used for annotating Javascript resources. It will wrap each Javascript resource
 * in a try/catch block with console logging in the catch.
 * <p>
 * This implementation is stateless.
 */
public class TryCatchJsResourceContentAnnotator extends ResourceContentAnnotator {
    public static final String CATCH_BLOCK =
            "WRMCB=function(e){var c=console;if(c&&c.log&&c.error){c.log('Error running batched script.');c.error(e);}}\n";
    private static final byte[] BEFORE_CHUNK = "try {\n".getBytes();
    // The catch block should start with the newline, because otherwise if the last line ends with the comment
    // it will be commented out too.
    private static final byte[] AFTER_CHUNK = "\n}catch(e){WRMCB(e)}".getBytes();

    @Override
    public int beforeResourceInBatch(
            LinkedHashSet<String> requiredResources,
            Resource resource,
            final Map<String, String> params,
            OutputStream stream)
            throws IOException {
        stream.write(BEFORE_CHUNK);
        return 1;
    }

    @Override
    public void afterResourceInBatch(
            LinkedHashSet<String> requiredResources,
            Resource resource,
            final Map<String, String> params,
            OutputStream stream)
            throws IOException {
        stream.write(AFTER_CHUNK);
    }

    @Override
    public int beforeAllResourcesInBatch(
            LinkedHashSet<String> requiredResources, String url, final Map<String, String> params, OutputStream stream)
            throws IOException {
        stream.write(CATCH_BLOCK.getBytes());
        return 1;
    }

    @Override
    public int hashCode() {
        return getClass().getName().hashCode();
    }
}
