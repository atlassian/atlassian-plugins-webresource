package com.atlassian.plugin.webresource.graph;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.models.Requestable;
import com.atlassian.plugin.webresource.models.RootPageKey;
import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;

@SuppressWarnings("UnusedReturnValue")
public class DependencyGraphBuilder {
    private static final Logger LOGGER = LoggerFactory.getLogger(DependencyGraphBuilder.class);

    private final Set<String> rootPageKeys;
    private final DependencyGraph<Requestable> resourceDependencyGraph;

    DependencyGraphBuilder() {
        resourceDependencyGraph = new DependencyGraph<>(Requestable.class);
        rootPageKeys = new HashSet<>();
    }

    @Nonnull
    public DependencyGraphBuilder addDependencies(
            @Nonnull final WebResourceModuleDescriptor webResourceModuleDescriptor) {
        requireNonNull(webResourceModuleDescriptor, "The web resource module descriptor is mandatory.");

        final String completeKey = webResourceModuleDescriptor.getCompleteKey();
        final Requestable resourceKey;
        if (webResourceModuleDescriptor.isRootPage()) {
            rootPageKeys.add(completeKey);
            resourceKey = new RootPageKey(completeKey);
            addWebResourceContextDependencies(resourceKey, webResourceModuleDescriptor.getContextDependencies());
        } else {
            resourceKey = new WebResourceKey(completeKey);
        }
        addWebResourceContexts(resourceKey, webResourceModuleDescriptor.getContexts());
        addWebResourceDependencies(resourceKey, webResourceModuleDescriptor.getDependencies());
        return this;
    }

    @Nonnull
    public DependencyGraphBuilder addDependencies(
            @Nonnull final Collection<WebResourceModuleDescriptor> webResourceModuleDescriptors) {
        requireNonNull(webResourceModuleDescriptors, "The web resource module descriptors are mandatory.");
        webResourceModuleDescriptors.stream().filter(Objects::nonNull).forEach(this::addDependencies);
        return this;
    }

    @Nonnull
    public DependencyGraphBuilder addWebResourceDependency(
            @Nonnull final Requestable requestable, @Nonnull final String webResourceDependency) {
        requireNonNull(requestable, "The requestable key is mandatory.");
        requireNonNull(webResourceDependency, "The web resource dependency is mandatory.");
        resourceDependencyGraph.addDependency(requestable, new WebResourceKey(webResourceDependency));
        return this;
    }

    @Nonnull
    public DependencyGraph<Requestable> build() {
        return resourceDependencyGraph;
    }

    private void addWebResourceDependencies(final Requestable requestable, final Collection<String> dependencies) {
        final RequestableKeyValidator webResourceKeyValidator = new RequestableKeyValidator(rootPageKeys);
        final Collection<Requestable> webResourceDependencyKeys = dependencies.stream()
                .filter(webResourceKeyValidator::isWebResource)
                .map(WebResourceKey::new)
                .collect(toSet());
        resourceDependencyGraph.addDependencies(requestable, webResourceDependencyKeys);
    }

    private void addWebResourceContexts(final Requestable requestable, final Collection<String> contexts) {
        if (requestable instanceof WebResourceContextKey) {
            final String message = format(
                    "Ignoring contexts for '%s': a context cannot depend on other contexts.", requestable.getKey());
            LOGGER.debug(message);
            return;
        }

        contexts.stream()
                .filter(RequestableKeyValidator::isWebResourceContext)
                .map(WebResourceContextKey::new)
                .forEach(ctx -> resourceDependencyGraph.addDependency(ctx, requestable));
    }

    private void addWebResourceContextDependencies(
            final Requestable requestable, final Collection<String> contextDependencies) {
        // At the moment this is allowed behaviour only for root pages.
        if (!(requestable instanceof RootPageKey)) {
            final String message = format(
                    "Ignoring context dependencies for '%s': context dependencies are only supported in root-pages at the moment.",
                    requestable.getKey());
            LOGGER.debug(message);
            return;
        }

        final Collection<Requestable> webResourceDependencyKeys = contextDependencies.stream()
                .filter(RequestableKeyValidator::isWebResourceContext)
                .map(WebResourceContextKey::new)
                .collect(toSet());
        resourceDependencyGraph.addDependencies(requestable, webResourceDependencyKeys);
    }
}
