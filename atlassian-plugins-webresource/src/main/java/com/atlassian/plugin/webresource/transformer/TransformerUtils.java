package com.atlassian.plugin.webresource.transformer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.function.Function;

import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;

/**
 * Utility class for transforming resources
 */
public class TransformerUtils {
    public static final Charset UTF8 = StandardCharsets.UTF_8;

    /**
     * Write apply a given transform a resource and then write the transformed content
     * to the supplied OutputStream.
     * Note that the OutputStream will not be closed by this method.
     *
     * @param originalResource - the resource to transform
     * @param encoding         - the encoding to use for writing
     * @param outputStream     - the output stream
     * @param transform        - a function for transforming the content
     * @throws DownloadException - thrown if it is not possible to stream the output
     * @since 2.9.0
     */
    public static void transformAndStreamResource(
            final DownloadableResource originalResource,
            final Charset encoding,
            final OutputStream outputStream,
            final Function<CharSequence, CharSequence> transform)
            throws DownloadException {
        try {
            final ByteArrayOutputStream originalResourceStream = new ByteArrayOutputStream();
            originalResource.streamResource(originalResourceStream);
            originalResourceStream.flush();
            outputStream.write(
                    // PERF NOTE: Using a String is faster than using a StringBuffer
                    transform
                            .apply(originalResourceStream.toString(encoding.name()))
                            .toString()
                            .getBytes(encoding));
        } catch (final IOException e) {
            throw new DownloadException("Unable to stream to the output", e);
        }
    }
}
