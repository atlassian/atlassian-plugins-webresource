package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

/**
 * Supplies static transformers
 *
 * @since v3.1.0
 */
public interface StaticTransformersSupplier {

    /**
     * @param locationType type of resource (eg js / css)
     * @return static transformers to apply to the given type
     */
    Iterable<WebResourceTransformerFactory> get(String locationType);

    /**
     * @param resourceLocation resource location
     * @return static transformers to apply to the given resourceLocation
     */
    Iterable<WebResourceTransformerFactory> get(ResourceLocation resourceLocation);
}
