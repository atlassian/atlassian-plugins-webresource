package com.atlassian.plugin.webresource.impl.snapshot;

/**
 * Container for a root-page. This class wraps a normal {@link WebResource}, and adds an additional flag that
 * specifies the superbatch inclusion behaviour.
 *
 * @since 3.5.9
 */
public final class RootPage {

    private final WebResource webResource;

    public RootPage(WebResource webResource) {
        this.webResource = webResource;
    }

    public WebResource getWebResource() {
        return webResource;
    }
}
