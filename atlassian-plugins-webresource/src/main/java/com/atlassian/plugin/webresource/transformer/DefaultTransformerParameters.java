package com.atlassian.plugin.webresource.transformer;

import com.atlassian.webresource.api.transformer.TransformerParameters;

public class DefaultTransformerParameters implements TransformerParameters {
    private final String pluginKey;
    private final String moduleKey;

    public DefaultTransformerParameters(String pluginKey, String moduleKey) {
        this.pluginKey = pluginKey;
        this.moduleKey = moduleKey;
    }

    public String getPluginKey() {
        return pluginKey;
    }

    public String getModuleKey() {
        return moduleKey;
    }
}
