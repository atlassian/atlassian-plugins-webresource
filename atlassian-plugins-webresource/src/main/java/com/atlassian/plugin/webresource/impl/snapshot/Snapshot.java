package com.atlassian.plugin.webresource.impl.snapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.config.Config;

import static java.util.Optional.ofNullable;

/**
 * Snapshot of the current state of the plugin system, captures list of web resources and its transformers
 * and conditions.
 *
 * @since v3.3
 */
public class Snapshot {
    // In Refactor
    public final Config config;
    // Storing some attribute of Resource outside of it in a separate Set in order to consume less memory
    // because amount of such attributes should be much smaller than the amount of web resources.
    private final Map<WebResource, CachedTransformers> webResourcesTransformations;
    private final Map<WebResource, CachedCondition> webResourcesCondition;

    @Deprecated // Always is created with an empty Set
    final Set<WebResource> webResourcesWithDisabledMinification;

    private final Map<String, Bundle> cachedBundles;
    private final Map<String, RootPage> rootPages;

    public Snapshot(Config config) {
        this(config, new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashSet<>());
    }

    public Snapshot(
            Config config,
            Map<String, Bundle> cachedBundles,
            Map<String, RootPage> rootPages,
            Map<WebResource, CachedTransformers> webResourcesTransformations,
            Map<WebResource, CachedCondition> webResourcesCondition,
            Set<WebResource> webResourcesWithDisabledMinification) {
        this.cachedBundles = cachedBundles;
        this.rootPages = rootPages;
        this.webResourcesTransformations = webResourcesTransformations;
        this.webResourcesCondition = webResourcesCondition;
        this.webResourcesWithDisabledMinification = webResourcesWithDisabledMinification;
        this.config = config;
    }

    public List<Bundle> toBundles(Iterable<String> keys) {
        List<Bundle> bundles = new ArrayList<>();
        for (String key : keys) {
            Bundle bundle = get(key);
            if (bundle != null) {
                bundles.add(bundle);
            }
        }
        return bundles;
    }

    public Bundle get(final String key) {
        return cachedBundles.get(key);
    }

    @Nonnull
    public RootPage getRootPage(final String key) {
        return ofNullable(rootPages.get(key))
                .orElseThrow(() -> new IllegalArgumentException("Root page '" + key + "' does not exist!"));
    }

    public Iterable<RootPage> getAllRootPages() {
        return rootPages.values();
    }

    public Map<WebResource, CachedCondition> conditions() {
        return webResourcesCondition;
    }

    public Map<WebResource, CachedTransformers> transformers() {
        return webResourcesTransformations;
    }
}
