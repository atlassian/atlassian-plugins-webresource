package com.atlassian.plugin.webresource.impl.support;

import com.atlassian.sourcemap.ReadableSourceMap;
import com.atlassian.sourcemap.SourceMapJoiner;
import com.atlassian.sourcemap.WritableSourceMap;

/**
 * Stub for SourceMapJoiner, needed to replace it when Source Map generation not needed.
 */
public class SourceMapJoinerStub extends SourceMapJoiner {
    @Override
    public void add(ReadableSourceMap sourceMap, int length) {}

    @Override
    public void add(ReadableSourceMap sourceMap, int length, int offset) {}

    @Override
    public WritableSourceMap join() {
        return null;
    }
}
