package com.atlassian.plugin.webresource.analytics.events;

import com.atlassian.analytics.api.annotations.EventName;

/**
 * Fired when a batch bundle is requested with wrong / outdated bundle hash in the URL.
 */
@EventName("webresource.plugin.invalid.bundle.hash")
public class InvalidBundleHashEvent {}
