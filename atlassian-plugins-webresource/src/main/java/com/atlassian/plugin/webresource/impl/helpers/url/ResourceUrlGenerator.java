package com.atlassian.plugin.webresource.impl.helpers.url;

import java.util.LinkedHashSet;
import java.util.List;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.helpers.ResourceGenerationInfo;
import com.atlassian.plugin.webresource.impl.support.UrlCache;
import com.atlassian.plugin.webresource.impl.support.UrlCache.IncludedExcludedConditionsAndBatchingOptions;

import static java.util.Objects.requireNonNull;

import static com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers.buildIncludedExcludedConditionsAndBatchingOptions;
import static com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers.calculateBatches;
import static com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers.collectUrlStateAndBuildResourceUrls;

public class ResourceUrlGenerator {

    private final UrlCache cache;

    public ResourceUrlGenerator(@Nonnull final UrlCache cache) {
        this.cache = requireNonNull(cache);
    }

    @Nonnull
    public Resolved generate(@Nonnull final ResourceGenerationInfo information) {

        final RequestState requestState = information.getData();
        final LinkedHashSet<String> allIncluded =
                information.getResourcePhase().map(requestState::getIncluded).orElseGet(requestState::getIncluded);
        final LinkedHashSet<String> allExcluded = new LinkedHashSet<>();
        allExcluded.addAll(requestState.getExcludedResolved());
        allExcluded.addAll(requestState.getExcluded());

        if (allIncluded.isEmpty()) {
            return new Resolved(allExcluded);
        }

        final IncludedExcludedConditionsAndBatchingOptions cacheKey = buildIncludedExcludedConditionsAndBatchingOptions(
                requestState.getRequestCache(), requestState.getUrlStrategy(), allIncluded, requestState.getExcluded());

        // Calculating
        final CalculatedBatches calculatedBatches = cache.getBatches(
                cacheKey,
                key -> calculateBatches(
                        requestState.getRequestCache(),
                        requestState.getUrlStrategy(),
                        allIncluded,
                        allExcluded,
                        key.getExcluded()));

        // Collecting transformer parameters and assembling resource urls.
        final List<ResourceUrl> resourceUrls = collectUrlStateAndBuildResourceUrls(
                requestState,
                requestState.getUrlStrategy(),
                calculatedBatches.getContextBatches(),
                calculatedBatches.getWebResourceBatches());

        return new Resolved(resourceUrls, calculatedBatches.getExcludedResolved());
    }
}
