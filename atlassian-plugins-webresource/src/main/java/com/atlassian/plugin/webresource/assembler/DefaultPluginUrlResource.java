package com.atlassian.plugin.webresource.assembler;

import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResourceParams;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static com.atlassian.webresource.api.UrlMode.ABSOLUTE;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.defaultPhase;

/**
 * Implementation of PluginUrlResource
 *
 * @since 3.0
 */
abstract class DefaultPluginUrlResource<T extends PluginUrlResourceParams> implements PluginUrlResource<T> {
    protected final ResourceUrl resourceUrl;
    private final ResourcePhase resourcePhase;

    DefaultPluginUrlResource(@Nonnull final ResourceUrl resourceUrl, @Nonnull final ResourcePhase resourcePhase) {
        this.resourceUrl = resourceUrl;
        this.resourcePhase = resourcePhase;
    }

    DefaultPluginUrlResource(@Nonnull final ResourceUrl resourceUrl) {
        this(resourceUrl, defaultPhase());
    }

    @Override
    public String getStaticUrl(UrlMode urlMode) {
        return resourceUrl.getUrl(urlMode == ABSOLUTE);
    }

    @Override
    public String toString() {
        return resourceUrl.getKey() + ':' + resourceUrl.getName();
    }

    @Override
    public String getKey() {
        return resourceUrl.getKey();
    }

    @Override
    public BatchType getBatchType() {
        return resourceUrl.getBatchType();
    }

    @Nonnull
    @Override
    public ResourcePhase getResourcePhase() {
        return resourcePhase;
    }
}
