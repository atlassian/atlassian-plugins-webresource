package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contenttype;

import javax.annotation.Nonnull;

import com.atlassian.plugin.elements.ResourceLocation;

import static java.util.Objects.requireNonNull;

public class ContentTypeStrategyFactory {

    public ContentTypeStrategyFactory() {}

    public ContentTypeStrategy createDefaultContentTypeStrategy(@Nonnull ResourceLocation resourceLocation) {
        return new DefaultContentTypeStrategy(requireNonNull(resourceLocation));
    }
}
