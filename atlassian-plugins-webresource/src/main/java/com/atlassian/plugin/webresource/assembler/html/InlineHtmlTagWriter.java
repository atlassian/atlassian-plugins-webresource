package com.atlassian.plugin.webresource.assembler.html;

import java.io.Writer;
import java.util.Collection;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.assembler.ResourceUrls;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.UrlMode;

import static java.util.Collections.emptyList;

public class InlineHtmlTagWriter extends HtmlTagWriter {
    private final JavaScriptSyncResourceWriter jsWriter;

    public InlineHtmlTagWriter(
            final Config config, final RequestState requestState, final Writer writer, final UrlMode urlMode) {
        super(requestState, writer, emptyList());
        this.jsWriter = new JavaScriptSyncResourceWriter(requestState, writer);
    }

    @Override
    public void writeHtmlTag(@Nonnull final Collection<ResourceUrls> resources) {
        jsWriter.write(resources);
    }

    @Nonnull
    @Override
    String generateHtmlTag(@Nonnull ResourceUrls resource, @Nonnull HtmlTagFormatter formatter) {
        throw new RuntimeException("This should never get called.");
    }
}
