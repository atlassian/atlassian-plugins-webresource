package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contentprovider;

import java.io.InputStream;
import java.io.OutputStream;

import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contenttype.ContentTypeStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.path.PathStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream.StreamStrategy;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.sourcemap.ReadableSourceMap;

import static com.atlassian.plugin.webresource.impl.support.Support.copy;

public class StreamContentProviderStrategy implements ContentProviderStrategy {
    private final StreamStrategy streamStrategy;
    private final ContentTypeStrategy contentTypeStrategy;
    private final PathStrategy pathStrategy;

    StreamContentProviderStrategy(
            StreamStrategy streamStrategy, ContentTypeStrategy contentTypeStrategy, PathStrategy pathStrategy) {
        this.streamStrategy = streamStrategy;
        this.contentTypeStrategy = contentTypeStrategy;
        this.pathStrategy = pathStrategy;
    }

    public Content getContent() {
        return new ContentImpl(contentTypeStrategy.getContentType(), false) {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                InputStream is = streamStrategy.getInputStream(pathStrategy.getPath());
                if (is != null) {
                    copy(is, out);
                } else {
                    throw new RuntimeException("Cannot read resource " + pathStrategy.getPath());
                }
                return null;
            }
        };
    }
}
