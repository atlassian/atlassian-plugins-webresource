package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.path;

public class RelativePathStrategyDecorator implements PathStrategy {
    private final PathStrategy pathStrategy;
    private final String relativePath;

    RelativePathStrategyDecorator(PathStrategy pathStrategy, String relativePath) {
        this.pathStrategy = pathStrategy;
        this.relativePath = relativePath;
    }

    @Override
    public String getPath() {
        String baseLocation = pathStrategy.getPath();
        if (baseLocation.endsWith("/")) {
            return baseLocation + relativePath;
        } else {
            return baseLocation + "/" + relativePath;
        }
    }
}
