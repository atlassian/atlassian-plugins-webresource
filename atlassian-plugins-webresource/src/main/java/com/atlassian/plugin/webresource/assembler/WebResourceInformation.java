package com.atlassian.plugin.webresource.assembler;

import java.util.Collection;
import java.util.LinkedList;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;
import com.atlassian.webresource.api.data.PluginDataResource;

import static java.util.stream.Collectors.toCollection;

import static com.atlassian.plugin.webresource.impl.config.Config.CSS_TYPE;
import static com.atlassian.plugin.webresource.impl.config.Config.JS_TYPE;

public class WebResourceInformation {
    private final Collection<ResourceUrls> containers;
    private final Collection<PluginDataResource> data;
    private final ResourcePhase resourcePhase;
    private final Collection<ResourceUrl> urls;

    WebResourceInformation(
            @Nonnull final Collection<PluginDataResource> data,
            @Nonnull final ResourcePhase resourcePhase,
            @Nonnull final Collection<ResourceUrl> urls) {
        this.data = data;
        this.resourcePhase = resourcePhase;
        this.urls = urls;
        containers = urls.stream()
                .map(resourceUrl -> {
                    final String type = resourceUrl.getType();
                    switch (type) {
                        case JS_TYPE: {
                            final PluginUrlResource<?> pluginUrlResource =
                                    new DefaultPluginJsResource(resourceUrl, resourcePhase);
                            return new ResourceUrls(resourceUrl, pluginUrlResource);
                        }
                        case CSS_TYPE: {
                            final PluginUrlResource<?> pluginUrlResource =
                                    new DefaultPluginCssResource(resourceUrl, resourcePhase);
                            return new ResourceUrls(resourceUrl, pluginUrlResource);
                        }
                        default: {
                            throw new RuntimeException("unsupported extension " + type);
                        }
                    }
                })
                .collect(toCollection(LinkedList::new));
    }

    @Nonnull
    public Collection<ResourceUrls> getResourceUrls() {
        return containers;
    }

    @Nonnull
    public Collection<PluginDataResource> getData() {
        return data;
    }

    @Nonnull
    public ResourcePhase getResourcePhase() {
        return resourcePhase;
    }

    @Nonnull
    public Collection<ResourceUrl> getUrls() {
        return urls;
    }
}
