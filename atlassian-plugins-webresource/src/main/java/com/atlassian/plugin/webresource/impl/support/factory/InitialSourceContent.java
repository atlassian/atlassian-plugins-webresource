package com.atlassian.plugin.webresource.impl.support.factory;

import java.io.InputStream;
import java.io.OutputStream;
import javax.annotation.Nullable;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.InitialContent;
import com.atlassian.sourcemap.ReadableSourceMap;

/**
 * <p>Represents a source file content looked up from the file system.</p>
 * <p>E.g: If we have a transpilation from ES6 to ES5, it will contain ES6 source code used for the generation of the ES5 one.</p>
 *
 * @since 5.0.0
 */
class InitialSourceContent extends InitialContent {

    InitialSourceContent(
            @Nullable final InputStream content,
            @Nullable final String path,
            @Nullable final ReadableSourceMap sourceMap) {
        super(content, path, sourceMap);
    }

    @NonNull
    static InitialSourceContentBuilder builder() {
        return new InitialSourceContentBuilder();
    }

    @NonNull
    @Override
    public Content toContent(@NonNull final Content originalContent) {
        return new Content() {
            @Override
            public ReadableSourceMap writeTo(
                    final OutputStream originalContentStream, final boolean isSourceMapEnabled) {
                originalContent.writeTo(originalContentStream, isSourceMapEnabled);
                return getSourceMap().filter(sourceMap -> isSourceMapEnabled).orElse(null);
            }

            @Override
            public String getContentType() {
                return originalContent.getContentType();
            }

            @Override
            public boolean isTransformed() {
                return false;
            }
        };
    }
}
