package com.atlassian.plugin.webresource.impl;

import java.util.List;
import java.util.Set;

import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;

/**
 * Wrapper around transformer to cache it.
 *
 * @since v3.3.2
 */
public class CachedTransformers {
    private final List<WebResourceTransformation> transformations;

    public CachedTransformers(List<WebResourceTransformation> transformations) {
        this.transformations = transformations;
    }

    /**
     * Add transformer parameters to url. If error wil be thrown it will be intercepted and ignored.
     *
     */
    public void addToUrlSafely(
            UrlBuilder urlBuilder,
            UrlBuildingStrategy urlBuildingStrategy,
            String type,
            TransformerCache transformerCache,
            TransformerParameters transformerParameters,
            String webResourceKey) {
        for (WebResourceTransformation transformation : transformations) {
            if (transformation.matches(type)) {
                try {
                    transformation.addTransformParameters(
                            transformerCache, transformerParameters, urlBuilder, urlBuildingStrategy);
                } catch (RuntimeException e) {
                    Support.LOGGER.warn("error thrown in transformer during url generation for " + webResourceKey, e);
                }
            }
        }
    }

    public List<WebResourceTransformation> getTransformations() {
        return transformations;
    }

    /**
     * Get list of param keys used by these transformers.
     *
     * @param paramKeys set to add keys to
     * @param type resource type
     * @return false if there are transformers that don't support allUsedQueryParameters
     */
    public boolean addAllUsedQueryParameters(Set<String> paramKeys, String type, TransformerCache transformerCache) {
        for (WebResourceTransformation transformation : transformations) {
            if (transformation.matches(type)) {
                if (!transformation.addAllUsedQueryParameters(paramKeys, transformerCache)) return false;
            }
        }

        return true;
    }
}
