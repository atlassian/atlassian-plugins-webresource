package com.atlassian.plugin.webresource.legacy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.helpers.url.ContextBatchKey;

import static java.util.Collections.emptySet;

public class LegacyUrlGenerationHelpers {
    /**
     * @deprecated since 5.5.0.
     * Use {@link #calculateBatches(RequestCache, UrlBuildingStrategy, Collection, Collection, Set, boolean)} instead.
     */
    @Deprecated
    public static Resolved calculateBatches(
            final RequestCache requestCache,
            final UrlBuildingStrategy urlBuildingStrategy,
            final Collection<String> topLevelIncluded,
            final Collection<String> excludedResolved,
            boolean includeDependenciesForFailedUrlReadingConditions) {
        return calculateBatches(
                requestCache,
                urlBuildingStrategy,
                topLevelIncluded,
                excludedResolved,
                emptySet(),
                includeDependenciesForFailedUrlReadingConditions);
    }

    /**
     * Groups given web resources and contexts in to batches, taking
     * any content that has already been served in to account.
     *
     * @param requestCache        request cache.
     * @param urlBuildingStrategy how conditions and transformers contribute to generated URLs.
     * @param topLevelIncluded    the "entry point" web resources and contexts the client has requested.
     * @param topLevelExcluded    the "entry point" web resources and contexts that were explicitly excluded
     *                            from the request by the client. Used to encode in batch URLs
     * @param allExcluded         all web resources discoverable through the topLevelExcluded set.
     *                            This should be a superset of topLevelExcluded - that is, all "entry point"
     *                            web-resources and contexts should also be present here. Used to determine what has already been
     *                            served to the client and avoid re-serving the same content.
     * @return a {@link Resolved} that represents the batch groups to serve to the client, along
     *         with a list of all items that should be omitted from subsequent client requests.
     */
    public static Resolved calculateBatches(
            final RequestCache requestCache,
            final UrlBuildingStrategy urlBuildingStrategy,
            final Collection<String> topLevelIncluded,
            final Collection<String> allExcluded,
            final Set<String> topLevelExcluded,
            boolean includeDependenciesForFailedUrlReadingConditions) {
        // Check whether special batches even exist.
        // These flags could be used to save a lot of wasted processing...
        // but I'm being surgical today; I'm not rewriting this entire algorithm just yet.
        final boolean superBatchIsNotEmpty =
                requestCache.getGlobals().getConfig().isSuperbatchCreated();

        // Converting input data to legacy format.
        final String prefix = Config.CONTEXT_PREFIX + ":";
        final Set<String> requiredContexts = new LinkedHashSet<>();
        final Set<String> requiredWebResources = new LinkedHashSet<>();
        boolean isSuperBatchEnabled = false;
        for (String key : topLevelIncluded) {
            if (Config.isContextKey(key)) {
                if (Config.SUPERBATCH_KEY.equals(key)) {
                    isSuperBatchEnabled = true;
                } else {
                    requiredContexts.add(key.replace(prefix, ""));
                }
            } else {
                requiredWebResources.add(key);
            }
        }

        final Set<String> excludedContexts = new LinkedHashSet<>();
        final Set<String> excludedWebResources = new LinkedHashSet<>();
        boolean isSuperBatchHasBeenEscluded = false;
        for (String key : allExcluded) {
            if (Config.SUPERBATCH_KEY.equals(key)) {
                isSuperBatchHasBeenEscluded = true;
            } else {
                if (Config.isContextKey(key)) {
                    excludedContexts.add(key.replace(prefix, ""));
                } else {
                    excludedWebResources.add(key);
                }
            }
        }

        // Preparing legacy interfaces.
        Config config = requestCache.getGlobals().getConfig();
        isSuperBatchEnabled |= isSuperBatchHasBeenEscluded;

        List<String> superBatchKeys = new ArrayList<>(config.getBeforeAllResources());
        superBatchKeys.addAll(config.getBatchingConfiguration().getSuperBatchModuleCompleteKeys());

        com.atlassian.plugin.webresource.legacy.ResourceDependencyResolver legacyDependencyResolver =
                new com.atlassian.plugin.webresource.legacy.DefaultResourceDependencyResolver(
                        requestCache.getGlobals(), config.getIntegration(), isSuperBatchEnabled, superBatchKeys);
        com.atlassian.plugin.webresource.legacy.PluginResourceLocator legacyResourceLocator =
                new com.atlassian.plugin.webresource.legacy.PluginResourceLocatorImpl(config.getIntegration());

        ResourceRequirer resourceRequirer = new ResourceRequirer(
                legacyResourceLocator,
                legacyDependencyResolver,
                isSuperBatchEnabled,
                includeDependenciesForFailedUrlReadingConditions);

        // Calculating dependencies.
        InclusionState inclusionState = new InclusionState(
                isSuperBatchHasBeenEscluded, excludedWebResources, excludedContexts, topLevelExcluded);
        Collection<PluginResource> resources = resourceRequirer.includeResources(
                requestCache, urlBuildingStrategy, requiredWebResources, requiredContexts, inclusionState);

        // Converting output data from legacy format.
        List<ContextBatchKey> contextBatchKeys = new ArrayList<>();
        List<String> webResourceBatchKeys = new ArrayList<>();

        boolean hasSuperbatch = false;
        for (PluginResource pluginResource : resources) {
            if (pluginResource instanceof SuperBatchPluginResource) {
                List<String> included = new ArrayList<>();
                SuperBatchPluginResource contextBatchPluginResource = (SuperBatchPluginResource) pluginResource;
                included.add(Config.SUPERBATCH_KEY);
                final LinkedHashSet<String> excluded = new LinkedHashSet<>();
                for (String key : contextBatchPluginResource.getExcludedContexts()) {
                    excluded.add(Config.CONTEXT_PREFIX + ":" + key);
                }
                contextBatchKeys.add(new ContextBatchKey(included, excluded));
                hasSuperbatch = true;
                break;
            }
        }
        final boolean shouldSubtractSuperbatch =
                superBatchIsNotEmpty && (isSuperBatchHasBeenEscluded || (hasSuperbatch && inclusionState.superbatch));
        for (PluginResource pluginResource : resources) {
            if (pluginResource instanceof ContextBatchPluginResource) {
                List<String> included = new ArrayList<>();
                LinkedHashSet<String> excluded = new LinkedHashSet<>();
                ContextBatchPluginResource contextBatchPluginResource = (ContextBatchPluginResource) pluginResource;
                for (String key : contextBatchPluginResource.getContexts()) {
                    included.add(Config.CONTEXT_PREFIX + ":" + key);
                }
                if (shouldSubtractSuperbatch) {
                    excluded.add(Config.SUPERBATCH_KEY);
                }
                for (String key : contextBatchPluginResource.getExcludedContexts()) {
                    excluded.add(Config.CONTEXT_PREFIX + ":" + key);
                }
                contextBatchKeys.add(new ContextBatchKey(included, excluded));
            }
            if (pluginResource instanceof BatchPluginResource) {
                BatchPluginResource batchPluginResource = (BatchPluginResource) pluginResource;
                webResourceBatchKeys.add(batchPluginResource.getModuleCompleteKey());
            }
        }

        LinkedHashSet<String> excludedResolved = new LinkedHashSet<>();
        if (shouldSubtractSuperbatch) {
            excludedResolved.add(Config.SUPERBATCH_KEY);
        }
        for (String key : inclusionState.contexts) {
            if (!Config.SUPER_BATCH_CONTEXT_KEY.equals(key)) {
                excludedResolved.add(Config.CONTEXT_PREFIX + ":" + key);
            }
        }
        excludedResolved.addAll(inclusionState.webresources);

        return new Resolved(contextBatchKeys, webResourceBatchKeys, excludedResolved);
    }

    /**
     * DTO representing three collections:
     *
     * 1) ordered list of context batches to serve,
     * 2) ordered list of web resource batches to serve, and
     * 3) unordered "excluded resolved" list, which is a combination of all web resources and contexts
     *    discovered in the dependency graph while constructing the first two lists, whose content will either be
     *    served in these batch files or was omitted from them.
     *
     * When serving, context batches must be served first, followed by web-resource batches.
     * The content from any item in the "excluded resolved" list must be omitted from subsequent client requests.
     *
     * @see com.atlassian.plugin.webresource.impl.helpers.url.CalculatedBatches
     */
    public static class Resolved {
        public final List<ContextBatchKey> contextBatchKeys;
        public final List<String> webResourceBatchKeys;
        public final Set<String> excludedResolved;

        public Resolved(
                List<ContextBatchKey> contextBatchKeys,
                List<String> webResourceBatchKeys,
                Set<String> excludedResolved) {
            this.contextBatchKeys = contextBatchKeys;
            this.webResourceBatchKeys = webResourceBatchKeys;
            this.excludedResolved = excludedResolved;
        }
    }
}
