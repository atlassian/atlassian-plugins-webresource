package com.atlassian.plugin.webresource;

import java.util.List;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.helpers.url.SubBatch;
import com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;

import static com.atlassian.plugin.webresource.impl.snapshot.resource.Resource.isCacheableStatic;
import static com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType.RESOURCE;

/**
 * An adapter between the current URL Generation code and previous URL Output code.
 *
 * @since v3.3
 */
public class WebResourceSubBatchUrl extends ResourceUrl {
    private final Globals globals;
    private final String type;
    private final Map<String, String> params;
    private final String hash;
    private final String key;
    private final SubBatch subBatch;

    public WebResourceSubBatchUrl(
            final Globals globals,
            final String key,
            final SubBatch subBatch,
            final String type,
            final Map<String, String> params,
            final String hash) {
        this.globals = globals;
        this.key = key;
        this.subBatch = subBatch;
        this.type = type;
        this.hash = hash;
        this.params = params;
    }

    @Override
    public String getName() {
        return key + '.' + type;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getType() {
        return type;
    }

    public String getUrl(final boolean isAbsolute) {
        boolean isCacheable = isCacheableStatic(subBatch.getResourcesParams());
        return globals.getRouter()
                .cloneWithNewUrlMode(isAbsolute)
                .webResourceBatchUrl(
                        getKey(),
                        getType(),
                        getParams(),
                        isCacheable,
                        true,
                        hash,
                        getBundle().getVersion());
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

    @Override
    public BatchType getBatchType() {
        return RESOURCE;
    }

    public Bundle getBundle() {
        return subBatch.getBundles().get(0);
    }

    @Override
    public List<Resource> getResources(final RequestCache requestCache) {
        return UrlGenerationHelpers.resourcesOfType(
                getBundle().getResources(requestCache).values(), type);
    }
}
