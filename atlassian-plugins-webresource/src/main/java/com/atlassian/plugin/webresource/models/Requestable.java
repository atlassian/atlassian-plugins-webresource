package com.atlassian.plugin.webresource.models;

import java.util.Objects;
import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Represents anything a developer can request or "require" directly to load on the frontend.
 *
 * Requestable items are unique addresses to locations within a running product where a set of resources
 * can be located and served as content to the frontend.
 */
public abstract class Requestable {
    private final String key;

    public Requestable(@Nonnull final String key) {
        this.key = requireNonNull(key, "The key is mandatory for creating the requestable.");
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof Requestable) {
            final Requestable otherRequestable = (Requestable) other;
            return Objects.equals(key, otherRequestable.key);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    public String getKey() {
        return key;
    }

    /**
     * Used during refactoring to show where we lose our strong typing
     *
     * @return The requestable key.
     */
    @Deprecated
    public String toLooseType() {
        return key;
    }
}
