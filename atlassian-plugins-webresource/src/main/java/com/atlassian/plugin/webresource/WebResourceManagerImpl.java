package com.atlassian.plugin.webresource;

import java.io.StringWriter;
import java.io.Writer;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Predicates;

import com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceSet;
import com.atlassian.plugin.webresource.assembler.LegacyPageBuilderService;
import com.atlassian.plugin.webresource.assembler.UrlModeUtils;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.WebResourceFilter;
import com.atlassian.webresource.api.WebResourceManager;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.api.assembler.AssembledResources;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.atlassian.webresource.api.assembler.WebResourceSet;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;

import static com.atlassian.plugin.util.Assertions.notNull;

/**
 * A handy super-class that handles most of the resource management.
 * <p>
 * To use this manager, you need to have the following UrlRewriteFilter code:
 * <pre>
 * &lt;rule&gt;
 * &lt;from&gt;^/s/(.*)/_/(.*)&lt;/from&gt;
 * &lt;run class="com.atlassian.plugin.servlet.ResourceDownloadUtils" method="addCachingHeaders" /&gt;
 * &lt;to type="forward"&gt;/$2&lt;/to&gt;
 * &lt;/rule&gt;
 * </pre>
 * <p>
 * Sub-classes should implement the abstract methods
 */
public class WebResourceManagerImpl implements WebResourceManager {
    protected final LegacyPageBuilderService pageBuilderService;
    private final WebResourceAssemblerFactory webResourceAssemblerFactory;
    private final WebResourceUrlProvider webResourceUrlProvider;

    public WebResourceManagerImpl(
            Globals globals,
            final WebResourceIntegration webResourceIntegration,
            final WebResourceUrlProvider webResourceUrlProvider) {
        this(globals, webResourceIntegration, webResourceUrlProvider, new DefaultResourceBatchingConfiguration());
    }

    public WebResourceManagerImpl(
            final Globals globals,
            final WebResourceIntegration webResourceIntegration,
            final WebResourceUrlProvider webResourceUrlProvider,
            final ResourceBatchingConfiguration batchingConfiguration) {
        this(
                new DefaultWebResourceAssemblerFactory(globals),
                null,
                webResourceIntegration,
                webResourceUrlProvider,
                null);
    }

    public WebResourceManagerImpl(
            final WebResourceAssemblerFactory webResourceAssemblerFactory,
            final PluginResourceLocator pluginResourceLocator,
            final WebResourceIntegration webResourceIntegration,
            final WebResourceUrlProvider webResourceUrlProvider,
            final ResourceBatchingConfiguration batchingConfiguration) {
        this(
                webResourceAssemblerFactory,
                new DefaultPageBuilderService(webResourceIntegration, webResourceAssemblerFactory),
                null,
                webResourceUrlProvider,
                null);
    }

    public WebResourceManagerImpl(
            final WebResourceAssemblerFactory webResourceAssemblerFactory,
            final LegacyPageBuilderService pageBuilderService,
            final PluginResourceLocator pluginResourceLocator,
            final WebResourceUrlProvider webResourceUrlProvider,
            final ResourceBatchingConfiguration batchingConfiguration) {
        this.webResourceAssemblerFactory = notNull("webResourceAssemblerFactory", webResourceAssemblerFactory);
        this.pageBuilderService = notNull("pageBuilderService", pageBuilderService);
        this.webResourceUrlProvider = notNull("webResourceUrlProvider", webResourceUrlProvider);
    }

    @Override
    public void requireResource(final String moduleCompleteKey) {
        getRequestLocalRequiredResources().requireWebResource(moduleCompleteKey);
    }

    @Override
    public void requireResourcesForContext(final String context) {
        getRequestLocalRequiredResources().requireContext(context);
    }

    @Override
    public void includeResources(
            final Iterable<String> moduleCompleteKeys, final Writer writer, final UrlMode urlMode) {
        // Include resources from the super batch as we don't include the super batch itself
        WebResourceAssembler webResourceAssembler = createSuperbatchingDisabledWebResourceAssembler();
        for (final String moduleCompleteKey : moduleCompleteKeys) {
            webResourceAssembler.resources().requireWebResource(moduleCompleteKey);
        }
        final WebResourceSet webResourceSet = webResourceAssembler.assembled().drainIncludedResources();
        webResourceSet.writeHtmlTags(writer, UrlModeUtils.convert(urlMode));
    }

    /**
     * This is the equivalent of of calling {@link #includeResources(Writer, UrlMode, WebResourceFilter)} with
     * the given url mode and a {@link DefaultWebResourceFilter}.
     *
     * @see #includeResources(Writer, UrlMode, WebResourceFilter)
     */
    @Override
    public void includeResources(final Writer writer, final UrlMode urlMode) {
        includeResources(writer, urlMode, new DefaultWebResourceFilter());
    }

    /**
     * Writes out the resource tags to the previously required resources called via requireResource methods for the
     * specified url mode and resource filter. Note that this method will clear the list of previously required resources.
     *
     * @param writer            the writer to write the links to
     * @param urlMode           the url mode to write resource url links in
     * @param webResourceFilter the resource filter to filter resources on
     * @since 2.4
     */
    @Override
    public void includeResources(
            final Writer writer, final UrlMode urlMode, final WebResourceFilter webResourceFilter) {
        writeIncludedResources(writer, urlMode, webResourceFilter);
        clear();
    }

    /**
     * This is the equivalent of calling {@link #getRequiredResources(UrlMode, WebResourceFilter)} with the given url
     * mode and a {@link DefaultWebResourceFilter}.
     *
     * @see #getRequiredResources(UrlMode, WebResourceFilter)
     */
    @Override
    public String getRequiredResources(final UrlMode urlMode) {
        return getRequiredResources(urlMode, new DefaultWebResourceFilter());
    }

    /**
     * Returns a String of the resources tags to the previously required resources called via requireResource methods
     * for the specified url mode and resource filter. Note that this method will NOT clear the list of previously
     * required resources.
     *
     * @param urlMode the url mode to write out the resource tags
     * @param filter  the web resource filter to filter resources on
     * @return a String of the resource tags
     * @since 2.4
     */
    @Override
    public String getRequiredResources(final UrlMode urlMode, final WebResourceFilter filter) {
        return writeIncludedResources(new StringWriter(), urlMode, filter).toString();
    }

    /**
     * Get the resources that have been required but excluding certain resource keys and certain contexts.
     * <p>
     * You should note that the WebResourceManager cannot break context batch resources down beyond the
     * granularity of the context so excluding particular resource keys does not necessary exclude them
     * from a context batch. To safely use this method it is expected that after the initial page load any
     * subsequent "dynamic" requests for resources should be at the granularity of individual resources and
     * not contexts. However you can request contexts if you are confident that there hasn't been an
     * overlap of contexts and dynamically requested macros. A typical safe example is in Confluence when
     * rendering macros the macro rendering process will have requested individual resources which will
     * not exist in any contexts.
     * <p>
     * This method is available for use by sub-classes but is not part of the public interface since it does
     * not form a complete solution for dynamic resource handling.
     *
     * @param urlMode              the url mode to write out the resource tags
     * @param webResourceFilter    the web resource filter to filter resources on
     * @param excludedResourceKeys the complete key of resources to be excluded. These exclusion will not apply to any requested contexts
     *                             so if a context happens to include these resource keys it will still be served (unless excluded as a context).
     * @param excludedContexts     contexts to be excluded when the tags are written
     * @return a String of the resource tags
     * @since 2.12.5
     */
    protected String getRequiredResources(
            UrlMode urlMode,
            WebResourceFilter webResourceFilter,
            Set<String> excludedResourceKeys,
            List<String> excludedContexts) {
        return writeIncludedResources(
                        new StringWriter(), urlMode, webResourceFilter, excludedResourceKeys, excludedContexts)
                .toString();
    }

    /**
     * Write all currently included resources to the given writer.
     */
    private <W extends Writer> W writeIncludedResources(
            final W writer, final UrlMode urlMode, final WebResourceFilter filter) {
        return writeIncludedResources(writer, urlMode, filter, emptySet(), emptyList());
    }

    /**
     * Write the resource tags request to the given writer but exclude certain resources and contexts. A typical use case would
     * be of a single page application which progressively requires further resources as further parts of the application are
     * loaded. The application would use this method and supply the resource keys and contexts it already knows about so that
     * duplicate resources do not end up being served.
     * <p>
     * Note that a context batch cannot be broken down into individual resource keys in the current WebResourceManager
     * implementation so the contents of context batches are not affected by any excluded resource keys. You can only
     * affect the context batches served by specifying excluded contexts. Another implication of this is that is
     * super batching is configured then the super batch will always be returned (for this method to be useful you
     * should have super batching disabled).
     *
     * @param excludedResourceKeys the complete key of resources to be excluded. These exclusion will not apply to any requested contexts
     *                             so if a context happens to include these resource keys it will still be served (unless excluded as a context).
     * @param excludedContexts     contexts to be excluded when the tags are written
     * @return the requested resources formatted as HTML tags with excluded contexts and resources ignored.
     */
    private <W extends Writer> W writeIncludedResources(
            final W writer,
            final UrlMode urlMode,
            final WebResourceFilter filter,
            final Set<String> excludedResourceKeys,
            final List<String> excludedContexts) {
        if (null != excludedResourceKeys
                && !excludedResourceKeys.isEmpty()
                && null != excludedContexts
                && !excludedContexts.isEmpty()) {
            // webResourceAssembler.exclude always excludes superbatch resources, so this will no longer return the
            // superbatch is resourceBatchingConfiguration.isSuperBatchingEnabled returns true. However, this method
            // is specified in the comments to only be called with superbatching disabled - which will cause this
            // method to operate correctly.
            getRequestLocalRequiredResources().exclude(excludedResourceKeys, new HashSet<>(excludedContexts));
        }

        DefaultWebResourceSet webResourceSet =
                (DefaultWebResourceSet) getRequestLocalAssembledResources().peek();

        webResourceSet.writeHtmlTags(
                writer,
                UrlModeUtils.convert(urlMode),
                Predicates.alwaysTrue(),
                input -> filter.matches(input.getName()));
        return writer;
    }

    @Override
    public void requireResource(final String moduleCompleteKey, final Writer writer, final UrlMode urlMode) {
        WebResourceAssembler webResourceAssembler = createSuperbatchingDisabledWebResourceAssembler();
        webResourceAssembler.resources().requireWebResource(moduleCompleteKey);
        final WebResourceSet webResourceSet = webResourceAssembler.assembled().drainIncludedResources();
        webResourceSet.writeHtmlTags(writer, UrlModeUtils.convert(urlMode));
    }

    @Override
    public String getResourceTags(final String moduleCompleteKey, final UrlMode urlMode) {
        final StringWriter writer = new StringWriter();
        requireResource(moduleCompleteKey, writer, urlMode);
        return writer.toString();
    }

    protected AssembledResources getRequestLocalAssembledResources() {
        return pageBuilderService.assembler().assembled();
    }

    protected RequiredResources getRequestLocalRequiredResources() {
        return pageBuilderService.assembler().resources();
    }

    private void clear() {
        pageBuilderService.clearRequestLocal();
    }

    /**
     * Creates a WebResourceAssembler with superbatch always turned off. This is used to build lists of module
     * dependencies including resources that would otherwise be in the superbatch.
     *
     * @return WebResourceAssembler with superbatching turned off.
     */
    private WebResourceAssembler createSuperbatchingDisabledWebResourceAssembler() {
        return webResourceAssemblerFactory
                .create()
                .includeSuperbatchResources(false)
                .build();
    }
}
