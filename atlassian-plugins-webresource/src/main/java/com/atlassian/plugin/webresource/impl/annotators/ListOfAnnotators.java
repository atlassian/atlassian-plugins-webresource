package com.atlassian.plugin.webresource.impl.annotators;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

/**
 * Helper to simplify working with multiple annotators.
 *
 * @since v 3.4.2
 */
public class ListOfAnnotators extends ResourceContentAnnotator {
    private final List<ResourceContentAnnotator> annotators;

    public ListOfAnnotators(List<ResourceContentAnnotator> annotators) {
        this.annotators = annotators;
    }

    @Override
    public int beforeResourceInBatch(
            LinkedHashSet<String> requiredResources,
            Resource resource,
            final Map<String, String> params,
            OutputStream stream)
            throws IOException {
        int offset = 0;
        for (ResourceContentAnnotator annotator : annotators) {
            offset += annotator.beforeResourceInBatch(requiredResources, resource, params, stream);
        }
        return offset;
    }

    @Override
    public void afterResourceInBatch(
            LinkedHashSet<String> requiredResources,
            Resource resource,
            final Map<String, String> params,
            OutputStream stream)
            throws IOException {
        for (int i = annotators.size() - 1; i >= 0; i--) {
            annotators.get(i).afterResourceInBatch(requiredResources, resource, params, stream);
        }
    }

    @Override
    public int beforeAllResourcesInBatch(
            LinkedHashSet<String> requiredResources, String url, final Map<String, String> params, OutputStream stream)
            throws IOException {
        int offset = 0;
        for (ResourceContentAnnotator annotator : annotators) {
            offset += annotator.beforeAllResourcesInBatch(requiredResources, url, params, stream);
        }
        return offset;
    }

    @Override
    public void afterAllResourcesInBatch(
            LinkedHashSet<String> requiredResources, String url, final Map<String, String> params, OutputStream stream)
            throws IOException {
        for (int i = annotators.size() - 1; i >= 0; i--) {
            annotators.get(i).afterAllResourcesInBatch(requiredResources, url, params, stream);
        }
    }

    @Override
    public int beforeResource(
            LinkedHashSet<String> requiredResources,
            String url,
            Resource resource,
            final Map<String, String> params,
            OutputStream stream)
            throws IOException {
        int offset = 0;
        for (ResourceContentAnnotator annotator : annotators) {
            offset += annotator.beforeResource(requiredResources, url, resource, params, stream);
        }
        return offset;
    }

    @Override
    public void afterResource(
            LinkedHashSet<String> requiredResources,
            String url,
            Resource resource,
            final Map<String, String> params,
            OutputStream stream)
            throws IOException {
        for (int i = annotators.size() - 1; i >= 0; i--) {
            annotators.get(i).afterResource(requiredResources, url, resource, params, stream);
        }
    }

    @Override
    public int hashCode() {
        return this.annotators.hashCode();
    }
}
