package com.atlassian.plugin.webresource.util;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ContainerManagedPlugin;

/**
 * Utility for loading a class from a plugin
 *
 * @since v3.0
 */
public class PluginClassLoader {
    public static <T> T create(Plugin plugin, Class<?> callingClass, HostContainer hostContainer, String className)
            throws ClassNotFoundException {
        Class<T> clazz = plugin.loadClass(className, callingClass);
        if (plugin instanceof ContainerManagedPlugin) {
            return ((ContainerManagedPlugin) plugin).getContainerAccessor().createBean(clazz);
        } else {
            return hostContainer.create(clazz);
        }
    }
}
