package com.atlassian.plugin.webresource.impl.http;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

import com.atlassian.plugin.webresource.ResourceUtils;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.discovery.BundleFinder;
import com.atlassian.plugin.webresource.impl.discovery.ResourceFinder;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.impl.support.http.BaseRouter;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;
import com.atlassian.plugin.webresource.impl.support.http.ServingType;
import com.atlassian.plugin.webresource.models.LooselyTypedRequestExpander;
import com.atlassian.plugin.webresource.models.RawRequest;
import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;

import static com.atlassian.plugin.webresource.impl.helpers.BaseHelpers.isConditionsSatisfied;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.shouldBeIncludedInBatch;

/**
 * Generates urls and route requests to proper handlers.
 *
 * @since 3.3
 */
public class Router extends BaseRouter<Controller> {
    public Router(final Globals globals) {
        super(globals);

        // Routes.

        addRoute("/resources/:completeKey/*resourceName.map", new Handler() {
            public void apply(Controller controller, String escapedCompleteKey, String escapedResourceName) {
                controller.serveResourceSourceMap(
                        unescapeSlashes(escapedCompleteKey),
                        unescapeSlashes(escapedResourceName),
                        ServingType.RESOURCES_SINGLE);
            }
        });

        addRoute("/resources/:completeKey/*resourceName", new Handler() {
            public void apply(Controller controller, String escapedCompleteKey, String escapedResourceName) {
                controller.serveResource(
                        unescapeSlashes(escapedCompleteKey),
                        unescapeSlashes(escapedResourceName),
                        ServingType.RESOURCES_SINGLE);
            }
        });

        addRoute("/sources/:completeKey/*resourceName", new Handler() {
            public void apply(Controller controller, String escapedCompleteKey, String escapedResourceName) {
                controller.serveSource(
                        unescapeSlashes(escapedCompleteKey),
                        unescapeSlashes(escapedResourceName),
                        ServingType.SOURCES_SINGLE);
            }
        });

        addRoute("/contextbatch/:type/:encodedContexts/*batchPostfixOrResourceName.map", new Handler() {
            public void apply(
                    Controller controller,
                    String type,
                    String encodedContexts,
                    String escapedBatchPostfixOrResourceName) {
                String batchPostfixOrResourceName = unescapeSlashes(escapedBatchPostfixOrResourceName);
                RawRequest raw = decodeContexts(encodedContexts);
                if (batchPostfixOrResourceName.equals("batch." + type)) {
                    controller.serveBatchSourceMap(raw, ServingType.CONTEXTBATCH, type, true, false);
                } else {
                    controller.serveResourceRelativeToBatchSourceMap(
                            raw, batchPostfixOrResourceName, ServingType.CONTEXTBATCH_RESOURCE, true, false);
                }
            }
        });

        addRoute("/contextbatch/:type/:encodedContexts/*batchPostfixOrResourceName", new Handler() {
            public void apply(
                    Controller controller,
                    String type,
                    String encodedContexts,
                    String escapedBatchPostfixOrResourceName) {
                String batchPostfixOrResourceName = unescapeSlashes(escapedBatchPostfixOrResourceName);
                RawRequest raw = decodeContexts(encodedContexts);
                if (batchPostfixOrResourceName.equals("batch." + type)) {
                    controller.serveBatch(raw, ServingType.CONTEXTBATCH, type, true, false, true, true);
                } else {
                    controller.serveResourceRelativeToBatch(
                            raw, batchPostfixOrResourceName, ServingType.CONTEXTBATCH_RESOURCE, true, false);
                }
            }
        });

        addRoute("/batch/:completeKey/*batchPostfixOrResourceName.map", new Handler() {
            public void apply(
                    Controller controller, String escapedCompleteKey, String escapedBatchPostfixOrResourceName) {
                String completeKey = unescapeSlashes(escapedCompleteKey);
                String batchPostfixOrResourceName = unescapeSlashes(escapedBatchPostfixOrResourceName);
                RawRequest raw = new RawRequest();
                raw.include(new WebResourceKey(completeKey));
                if (completeKey.equals(ResourceUtils.getBasename(batchPostfixOrResourceName))) {
                    // e.g., /batch/com.atlassian.auiplugin:dropdown/com.atlassian.auiplugin:dropdown.js
                    // serving all content inside a web-resource
                    controller.serveBatchSourceMap(
                            raw, ServingType.BATCH, ResourceUtils.getType(batchPostfixOrResourceName), false, true);
                } else {
                    controller.serveResourceRelativeToBatchSourceMap(
                            raw, batchPostfixOrResourceName, ServingType.BATCH_RESOURCE, false, true);
                }
            }
        });

        addRoute("/batch/:completeKey/*batchPostfixOrResourceName", new Handler() {
            public void apply(Request request, Response response, Controller controller, String[] arguments) {
                String completeKey = unescapeSlashes(arguments[0]);
                String batchPostfixOrResourceName = unescapeSlashes(arguments[1]);
                RawRequest raw = new RawRequest();
                raw.include(new WebResourceKey(completeKey));
                String type = ResourceUtils.getType(batchPostfixOrResourceName);

                if (completeKey.equals(ResourceUtils.getBasename(batchPostfixOrResourceName))) {
                    // e.g., /batch/com.atlassian.auiplugin:dropdown/com.atlassian.auiplugin:dropdown.js
                    // serving all content inside a web-resource
                    controller.serveBatch(raw, ServingType.BATCH, type, false, true, false, false);
                } else {
                    // It is impossible to detect if resource is batch or single relative to batch by url only. Because
                    // in previous implementation urls like `/batch/app:page/:anything.css` could be treated as both
                    // batch and
                    // single resource.
                    // So, the only way to detect what it is is to try to build batch and see if it's empty or not.
                    //
                    // TODO it seems that it doesn't work properly, because relative resources still served as
                    // batched (see test case for it shouldServeSingleResourceRealtiveToBatchWithTheSameType).
                    // so, maybe we can remove it and always use only `/batch/:completeKey/:completeKey.:type` for
                    // batch?
                    RequestCache requestCache = new RequestCache(globals);

                    LooselyTypedRequestExpander expander = new LooselyTypedRequestExpander(raw);
                    BundleFinder bundles = new BundleFinder(globals.getSnapshot())
                            .included(expander.getIncluded())
                            .excluded(expander.getExcluded(), isConditionsSatisfied(requestCache, request.getParams()))
                            .deep(false)
                            .deepFilter(isConditionsSatisfied(requestCache, request.getParams()));
                    ResourceFinder resources = new ResourceFinder(bundles, requestCache)
                            .filter(shouldBeIncludedInBatch(type, request.getParams()));

                    if (!resources.end().isEmpty()) {
                        // The already found batch resource not reused here because it's very rare legacy case,
                        // no need to optimize it.
                        controller.serveBatch(raw, ServingType.BATCH, type, false, true, false, false);
                    } else {
                        controller.serveResourceRelativeToBatch(
                                raw, batchPostfixOrResourceName, ServingType.BATCH_RESOURCE, false, true);
                    }
                }
            }
        });
    }

    /**
     * Needed to support legacy API.
     */
    protected Router(Globals globals, List<Route> routes, boolean useAbsoluteUrl) {
        super(globals, routes, useAbsoluteUrl);
    }

    // Needed to support legacy API.
    public static String resourceUrlAsStaticMethod(
            String completeKey, String resourceName, Map<String, String> params) {
        return buildUrl(interpolate("/resources/:completeKey/:resourceName", completeKey, resourceName), params);
    }

    /**
     * Encodes lists of included and excluded contexts into single key like `batch1,batch2,-excludedBatch3,
     * -excludedBatch4`.
     */
    public static String encodeContexts(Collection<String> includedContexts, Iterable<String> excludedContexts) {
        Iterable<String> includedContextsList;
        Iterable<String> excludedContextsList;

        if (Config.isStaticContextOrderEnabled()) {
            includedContextsList = sortContextList(includedContexts);
            excludedContextsList = sortContextList(excludedContexts);
        } else {
            includedContextsList = includedContexts;
            excludedContextsList = excludedContexts;
        }

        String prefix = Config.CONTEXT_PREFIX + ":";
        StringBuilder buff = new StringBuilder();
        for (String context : includedContextsList) {
            buff.append(context.replace(prefix, "")).append(",");
        }
        for (String context : excludedContextsList) {
            buff.append("-").append(context.replace(prefix, "")).append(",");
        }
        buff.deleteCharAt(buff.length() - 1);
        return buff.toString();
    }

    /**
     * Sorts a list of contexts to ensure the same contexts will always result in the same URL
     */
    private static List<String> sortContextList(Iterable<String> contexts) {
        List<String> contextList = Lists.newArrayList(contexts);
        Collections.sort(contextList);
        return contextList;
    }

    /**
     * Decodes context key like `batch1,batch2,-excludedBatch3,-excludedBatch4` into lists of included and excluded
     * contexts.
     */
    public static RawRequest decodeContexts(String encodedContexts) {
        RawRequest request = new RawRequest();
        String[] tokens = encodedContexts.split(",");
        for (String token : tokens) {
            if (token.startsWith("-")) {
                request.exclude(new WebResourceContextKey(token.substring(1)));
            } else {
                request.include(new WebResourceContextKey(token));
            }
        }
        return request;
    }

    /**
     * Expands list of Contexts into all web resources.
     * public static NormalizedRequest decodeContextsAsWebResources(String encodedContexts) {
     * RawRequest raw = decodeContexts(encodedContexts);
     * NormalizedRequest request = new NormalizedRequest();
     * request.include(raw);
     * return request;
     * }
     */

    /**
     * Converting source map url to url, removes `.map` from url.
     */
    public static String sourceMapUrlToUrl(String sourceMapUrl) {
        return sourceMapUrl.replaceAll("\\.map$", "").replaceAll("\\.map\\?", "?");
    }

    public static String escapeSlashes(String string) {
        return string.replaceAll("/", "::");
    }

    public static String unescapeSlashes(String string) {
        return string.replaceAll("::", "/");
    }

    /**
     * Url helpers.
     */
    public String contextBatchUrl(
            String key,
            String type,
            Map<String, String> params,
            boolean isResourceSupportCache,
            boolean isResourceSupportCdn,
            String resourceHash,
            String bundleHash) {
        return buildUrlWithPrefix(
                interpolate("/contextbatch/:type/:key/batch.:type", type, key, type),
                params,
                isResourceSupportCache,
                isResourceSupportCdn,
                resourceHash,
                bundleHash);
    }

    public String contextBatchSourceMapUrl(
            String key,
            String type,
            Map<String, String> params,
            boolean isResourceSupportCache,
            boolean isResourceSupportCdn,
            String resourceHash,
            String bundleHash) {
        return buildUrlWithPrefix(
                interpolate("/contextbatch/:type/:key/batch.:type.map", type, key, type),
                params,
                isResourceSupportCache,
                isResourceSupportCdn,
                resourceHash,
                bundleHash);
    }

    public String resourceUrlRelativeToContextBatch(
            String key,
            String type,
            String resourceName,
            Map<String, String> params,
            boolean isResourceSupportCache,
            boolean isResourceSupportCdn,
            String resourceHash,
            String bundleHash) {
        return buildUrlWithPrefix(
                interpolate("/contextbatch/:type/:key/:resourceName", type, key, resourceName),
                params,
                isResourceSupportCache,
                isResourceSupportCdn,
                resourceHash,
                bundleHash);
    }

    public String resourceUrl(
            String completeKey,
            String resourceName,
            Map<String, String> params,
            boolean isResourceSupportCache,
            boolean isResourceSupportCdn,
            String resourceHash,
            String bundleHash) {
        return buildUrlWithPrefix(
                interpolate("/resources/:completeKey/:resourceName", escapeSlashes(completeKey), resourceName),
                params,
                isResourceSupportCache,
                isResourceSupportCdn,
                resourceHash,
                bundleHash);
    }

    public String resourceSourceMapUrl(
            String completeKey,
            String resourceName,
            Map<String, String> params,
            boolean isResourceSupportCache,
            boolean isResourceSupportCdn,
            String resourceHash,
            String bundleHash) {
        return buildUrlWithPrefix(
                interpolate("/resources/:completeKey/:resourceName.map", escapeSlashes(completeKey), resourceName),
                params,
                isResourceSupportCache,
                isResourceSupportCdn,
                resourceHash,
                bundleHash);
    }

    public String pluginResourceUrl(
            String pluginKey,
            String resourceName,
            Map<String, String> params,
            boolean isResourceSupportCache,
            boolean isResourceSupportCdn,
            String resourceHash,
            String bundleHash) {
        return buildUrlWithPrefix(
                interpolate("/resources/:pluginKey/:resourceName", pluginKey, resourceName),
                params,
                isResourceSupportCache,
                isResourceSupportCdn,
                resourceHash,
                bundleHash);
    }

    // TODO currently sources not cached, because otherwise it would be hard to use it - the hash goes first in
    // the url and it makes sources in Chrome Inspector look like a bunch of folders with random number names.
    // When we change url scheme in such a way that the hash would be a parameter, then it would be possible to add hash
    // to source urls and cache it the same way as other files.
    public String resourceUrlWithoutHash(Resource resource, Map<String, String> params) {
        return buildUrl(
                globals.getConfig().getBaseUrl()
                        + interpolate("/resources/:completeKey/:resourceName", resource.getKey(), resource.getName()),
                params);
    }

    public String sourceUrl(
            String completeKey,
            String resourceName,
            Map<String, String> params,
            boolean isResourceSupportCache,
            boolean isResourceSupportCdn,
            String resourceHash,
            String bundleHash) {
        // TODO currently sources not cached, because otherwise it would be hard to use it - the hash goes first in
        // the url and it makes sources in Chrome Inspector look like a bunch of folders with random number names.
        // When we change url scheme in such a way that the hash would be a parameter,
        // then it would be possible to add hash
        // to source urls and cache it the same way as other files.
        return buildUrl(
                globals.getConfig().getBaseUrl()
                        + interpolate("/sources/:completeKey/:resourceName", escapeSlashes(completeKey), resourceName),
                params);
    }

    public String prebuildSourceUrl(
            String completeKey,
            String resourceName,
            Map<String, String> params,
            boolean isResourceSupportCache,
            boolean isResourceSupportCdn,
            String resourceHash,
            String bundleHash) {
        return sourceUrl(
                completeKey,
                Resource.getPrebuiltSourcePath(resourceName),
                params,
                isResourceSupportCache,
                isResourceSupportCdn,
                resourceHash,
                bundleHash);
    }

    public String sourceUrl(Resource resource) {
        return sourceUrl(resource.getKey(), resource.getName(), new HashMap<>(), true, true, "", resource.getVersion());
    }

    public String prebuildSourceUrl(Resource resource) {
        return prebuildSourceUrl(
                resource.getKey(), resource.getName(), new HashMap<>(), true, true, "", resource.getVersion());
    }

    /**
     * It is important for the url structure to be: 1. the same number of sectioned paths as the SinglePluginResource 2.
     * include the module complete key in the path before the resource name This is due to css resources referencing
     * other resources such as images in relative path forms.
     */
    public String webResourceBatchUrl(
            String completeKey,
            String type,
            Map<String, String> params,
            boolean isResourceSupportCache,
            boolean isResourceSupportCdn,
            String resourceHash,
            String bundleHash) {
        String encodedCompleteKey = escapeSlashes(completeKey);
        return buildUrlWithPrefix(
                interpolate("/batch/:completeKey/:completeKey.:type", encodedCompleteKey, encodedCompleteKey, type),
                params,
                isResourceSupportCache,
                isResourceSupportCdn,
                resourceHash,
                bundleHash);
    }

    public String resourceUrlRelativeToWebResourceBatch(
            String completeKey,
            String resourceName,
            Map<String, String> params,
            boolean isResourceSupportCache,
            boolean isResourceSupportCdn,
            String resourceHash,
            String bundleHash) {
        return buildUrlWithPrefix(
                interpolate("/batch/:completeKey/:resourceName", escapeSlashes(completeKey), resourceName),
                params,
                isResourceSupportCache,
                isResourceSupportCdn,
                resourceHash,
                bundleHash);
    }

    public String sourceMapUrl(String resourceUrl, Map<String, String> generatedParams) {
        return buildUrl(resourceUrl + ".map", generatedParams);
    }

    /**
     * Needed to support legacy API.
     */
    public Router cloneWithNewUrlMode(boolean useAbsoluteUrl) {
        return new Router(globals, routes, useAbsoluteUrl);
    }

    /**
     * Adds prefix to relative url.
     */
    public String buildUrlWithPrefix(
            String url,
            Map<String, String> params,
            boolean isResourceSupportCache,
            boolean isResourceSupportCdn,
            String hash,
            String bundleHash) {
        Object urlWithParams = buildUrl(url, params);
        Config config = globals.getConfig();
        if (isResourceSupportCache) {
            String hashWithCdnMark = hash + (isResourceSupportCdn ? "-CDN" : "-T");
            if (isResourceSupportCdn && config.isCdnEnabled()) {
                String prefix = config.getResourceUrlPrefix(hashWithCdnMark, bundleHash, false);
                return config.getResourceCdnPrefix(prefix + urlWithParams);
            } else {
                String prefix = config.getResourceUrlPrefix(hashWithCdnMark, bundleHash, useAbsoluteUrl);
                return prefix + urlWithParams;
            }
        } else {
            return config.getBaseUrl(useAbsoluteUrl) + urlWithParams;
        }
    }

    @Override
    protected Controller createController(Globals globals, Request request, Response response) {
        return new Controller(globals, request, response);
    }
}
