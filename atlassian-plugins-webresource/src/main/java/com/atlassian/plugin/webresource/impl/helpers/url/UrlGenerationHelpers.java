package com.atlassian.plugin.webresource.impl.helpers.url;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import com.google.common.base.Predicates;

import com.atlassian.plugin.webresource.ContextSubBatchResourceUrl;
import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.plugin.webresource.ResourceUrlImpl;
import com.atlassian.plugin.webresource.WebResourceSubBatchUrl;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.discovery.BundleFinder;
import com.atlassian.plugin.webresource.impl.discovery.Found;
import com.atlassian.plugin.webresource.impl.discovery.PredicateFailStrategy;
import com.atlassian.plugin.webresource.impl.helpers.BaseHelpers;
import com.atlassian.plugin.webresource.impl.helpers.StateEncodedUrlResult;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.atlassian.plugin.webresource.impl.support.UrlCache;
import com.atlassian.plugin.webresource.impl.support.UrlCache.EvaluatedCondition;
import com.atlassian.plugin.webresource.impl.support.UrlCache.IncludedAndExcluded;
import com.atlassian.plugin.webresource.impl.support.UrlCache.IncludedExcludedConditionsAndBatchingOptions;
import com.atlassian.plugin.webresource.impl.support.http.BaseRouter;
import com.atlassian.plugin.webresource.legacy.LegacyUrlGenerationHelpers;
import com.atlassian.plugin.webresource.models.RawRequest;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.plugin.webresource.util.HashBuilder;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;

import static com.atlassian.plugin.webresource.impl.config.Config.BATCH_TYPES;

/**
 * Stateless helper functions providing basic support for resource url generation.
 *
 * @since v3.3
 */
public class UrlGenerationHelpers extends BaseHelpers {
    /**
     * Building the cache key of included, excluded and conditions.
     */
    protected static IncludedExcludedConditionsAndBatchingOptions buildIncludedExcludedConditionsAndBatchingOptions(
            final RequestCache requestCache,
            final UrlBuildingStrategy urlBuilderStrategy,
            final Collection<String> topLevelIncluded,
            final Collection<String> topLevelExcluded) {
        final Set<EvaluatedCondition> evaluatedConditions = new HashSet<>();
        final Predicate<Bundle> conditionEvaluator = bundle -> Optional.ofNullable(bundle.getCondition())
                .map(condition -> {
                    boolean evaluationResult = condition.evaluateSafely(requestCache, urlBuilderStrategy);
                    evaluatedConditions.add(new EvaluatedCondition(condition, evaluationResult));

                    /*
                     * If condition will be evaluated to false, the conditions for its dependencies will be
                     * skipped.
                     */
                    return evaluationResult;
                })
                .orElse(true);

        // Walk the dependency graph, finding all conditions along the way, as well as subtree data.
        // We want to get the smallest possible set of condition coordinates relevant to the included items.
        final Found results = new BundleFinder(requestCache.getSnapshot())
                .included(topLevelIncluded)
                .excluded(topLevelExcluded, Predicates.alwaysTrue())
                .deepFilter(conditionEvaluator)
                .endAndGetResult();

        // Shrink the list of inclusions and exclusions by omitting any entrypoints that reference a subtree of another.
        // todo PLUGWEB-636: shrinking the inclusion list might cause code load+execution order problems... or it could
        // be glorious.
        final LinkedHashSet<String> reducedIncluded = new LinkedHashSet<>(topLevelIncluded);
        final Set<String> reducedExcluded = results.getReducedExclusions();

        // Build the cache key, using the reduced sets of data discovered walking the dependency graph.
        return new IncludedExcludedConditionsAndBatchingOptions(
                new IncludedAndExcluded(reducedIncluded, reducedExcluded), evaluatedConditions);
    }

    /**
     * Uses the set of top-level inclusions and exclusions, as well as a cache of previously-evaluated
     * conditions etc., to determine what content has already been served to the client.
     *
     * @param requestCache request cache. Used to look up previous condition evaluation results.
     * @param topLevelIncluded  the "entry point" web resources and contexts the client has requested.
     *                          conditions will be considered when discovering content via these items.
     * @param topLevelExcluded  the "entry point" web resources and contexts the client has already
     *                          loaded and should not be served again. Conditions will NOT be considered
     *                          when disovering content via these items.
     * @return list of all items that should be omitted from future requests made by this client.
     * @deprecated use {@link com.atlassian.plugin.webresource.graph.DependencyGraph} or {@link BundleFinder} instead
     *             to avoid generating an entire URL set just to traverse the dependency graph.
     */
    @Deprecated
    public static Set<String> resolveExcluded(
            final RequestCache requestCache,
            final UrlCache urlCache,
            final UrlBuildingStrategy urlBuilderStrategy,
            final Collection<String> topLevelIncluded,
            final Collection<String> topLevelExcluded) {

        final IncludedExcludedConditionsAndBatchingOptions cacheKey = buildIncludedExcludedConditionsAndBatchingOptions(
                requestCache, urlBuilderStrategy, topLevelIncluded, topLevelExcluded);
        return urlCache.getResolvedExcluded(cacheKey, key -> LegacyUrlGenerationHelpers.calculateBatches(
                        requestCache, urlBuilderStrategy, key.getIncluded(), key.getExcluded(), false)
                .excludedResolved);
    }

    /**
     * Groups given web resources and contexts in to batches, taking
     * any content that has already been served in to account.
     *
     * @param requestCache        request cache.
     * @param urlBuildingStrategy how conditions and transformers contribute to generated URLs.
     * @param topLevelIncluded    the "entry point" web resources and contexts the client has requested.
     * @param topLevelExcluded    the "entry point" web resources and contexts that were explicitly excluded
     *                            from the request by the client. Used to
     * @param allExcluded         all web resources discoverable through the topLevelExcluded set.
     *                            This should be a superset of topLevelExcluded - that is, all "entry point"
     *                            web-resources and contexts should also be present here. Used to determine what has already been
     *                            served to the client and avoid re-serving the same content.
     * @return a {@link CalculatedBatches} that represents the batch groups to serve to the client, along
     *         with a list of all items that should be omitted from subsequent client requests.
     */
    protected static CalculatedBatches calculateBatches(
            final RequestCache requestCache,
            final UrlBuildingStrategy urlBuildingStrategy,
            final LinkedHashSet<String> topLevelIncluded,
            final LinkedHashSet<String> allExcluded,
            final Set<String> topLevelExcluded) {
        final LegacyUrlGenerationHelpers.Resolved resolved = LegacyUrlGenerationHelpers.calculateBatches(
                requestCache, urlBuildingStrategy, topLevelIncluded, allExcluded, topLevelExcluded, false);

        // Splitting list of batches into list of sub-batches.
        final Tuple<List<ContextBatch>, List<WebResourceBatch>> subBatches = splitIntoSubBatches(
                requestCache,
                urlBuildingStrategy,
                resolved.contextBatchKeys,
                resolved.webResourceBatchKeys,
                allExcluded);
        final List<ContextBatch> contextBatches = subBatches.getFirst();
        final List<WebResourceBatch> webResourceBatches = subBatches.getLast();
        return new CalculatedBatches(contextBatches, webResourceBatches, resolved.excludedResolved);
    }

    /**
     * Collect URL State for Conditions and Transformers and build Resource URLs.
     *
     * @return list of Resource URLs.
     */
    protected static List<ResourceUrl> collectUrlStateAndBuildResourceUrls(
            final RequestState requestState,
            final UrlBuildingStrategy urlBuilderStrategy,
            final List<ContextBatch> contextBatches,
            final List<WebResourceBatch> webResourceBatches) {

        final RequestCache requestCache = requestState.getRequestCache();
        final Globals globals = requestCache.getGlobals();
        final List<ResourceUrl> resourceUrls = new ArrayList<>();

        for (final String type : BATCH_TYPES) {
            // Assembling resource urls for context batches.
            for (final ContextBatch contextBatch : contextBatches) {

                final List<String> excludedResolvedWithoutApplyingConditions = new BundleFinder(
                                requestCache.getSnapshot())
                        .included(contextBatch.getExcludedWithoutApplyingConditions())
                        .end();

                final List<ContextSubBatchResourceUrl> contextBatchResourceUrls = contextBatch.getSubBatches().stream()
                        .filter(subBatch ->
                                !subBatch.getResourcesOfType(requestCache, type).isEmpty())
                        .map(subBatch -> {
                            final StateEncodedUrlResult stateEncodedUrlResult = encodeStateInUrlIfSupported(
                                    requestCache,
                                    urlBuilderStrategy,
                                    type,
                                    subBatch.getResourcesParams(),
                                    subBatch.getBundles(),
                                    requestCache
                                            .getSnapshot()
                                            .toBundles(contextBatch.getSkippedWebResourcesWithUrlReadingConditions()),
                                    requestCache.getSnapshot().toBundles(excludedResolvedWithoutApplyingConditions));
                            // Creating resource url.
                            final DefaultUrlBuilder urlBuilder = stateEncodedUrlResult.getUrlBuilder();
                            return new ContextSubBatchResourceUrl(
                                    requestCache.getGlobals(),
                                    contextBatch,
                                    subBatch,
                                    type,
                                    urlBuilder.buildParams(),
                                    urlBuilder.buildHash());
                        })
                        .collect(toList());
                /*
                 * Sorting batch by urls, it's not needed and exists only for backward compatibility,
                 * because for some unknown reason in the previous version sub-batches where sorted by its urls.
                 * It exists only to ease backward compatibility testing, after some time this code should be removed.
                 */
                if (contextBatch.isAdditionalSortingRequired()) {
                    final ParamsComparator paramsComparator = new ParamsComparator();
                    contextBatchResourceUrls.sort((url1, url2) -> {
                        int result = paramsComparator.compare(url1.getParams(), url2.getParams());
                        // Only the part of the url with parameters should be compared, because hashes could be
                        // different and change sorting.
                        final String aUrl = BaseRouter.buildUrl("", url1.getParams());
                        final String bUrl = BaseRouter.buildUrl("", url2.getParams());
                        return result == 0 ? aUrl.compareTo(bUrl) : result;
                    });
                }

                if (globals.getConfig().isContextBatchingEnabled()) {
                    resourceUrls.addAll(contextBatchResourceUrls);
                } else {
                    // Destructing context batch into web resource batches or into individual resources.
                    for (final ContextSubBatchResourceUrl contextSubBatchResourceUrl : contextBatchResourceUrls) {
                        final Map<String, String> contextBatchResourceParams =
                                contextSubBatchResourceUrl.getSubBatch().getResourcesParams();
                        final SubBatch subBatch = contextSubBatchResourceUrl.getSubBatch();
                        final List<Resource> contextBatchResources = subBatch.getResourcesOfType(requestCache, type);

                        // Getting list of bundles.
                        final Map<Bundle, List<Resource>> bundles = new LinkedHashMap<>();
                        for (final Resource resource : contextBatchResources) {
                            bundles.computeIfAbsent(resource.getParent(), key -> new ArrayList<>())
                                    .add(resource);
                        }

                        // Calculating web resource batches.
                        for (final Entry<Bundle, List<Resource>> entry : bundles.entrySet()) {
                            final Bundle bundle = entry.getKey();
                            final List<Resource> resources = entry.getValue();

                            // Calculating parameters for web batch.
                            final StateEncodedUrlResult stateEncodedUrlResult = encodeStateInUrlIfSupported(
                                    requestCache,
                                    urlBuilderStrategy,
                                    type,
                                    contextBatchResourceParams,
                                    bundle,
                                    new ArrayList<>(),
                                    new ArrayList<>());

                            final DefaultUrlBuilder urlBuilder = stateEncodedUrlResult.getUrlBuilder();
                            final Map<String, String> webResourceBatchHttpParams = urlBuilder.buildParams();

                            if (globals.getConfig().isWebResourceBatchingEnabled()) {
                                // Creating web resource batch url.
                                final SubBatch webResourceSubBatch =
                                        new SubBatch(webResourceBatchHttpParams, bundle, resources);
                                resourceUrls.add(new WebResourceSubBatchUrl(
                                        requestCache.getGlobals(),
                                        bundle.getKey(),
                                        webResourceSubBatch,
                                        type,
                                        webResourceBatchHttpParams,
                                        urlBuilder.buildHash()));
                            } else {
                                // Creating resources url.
                                for (final Resource resource : resources) {
                                    resourceUrls.add(new ResourceUrlImpl(
                                            globals, resource, webResourceBatchHttpParams, urlBuilder.buildHash()));
                                }
                            }
                        }
                    }
                }
                resourceUrls.addAll(createResourceUrlsForRedirectResources(
                        requestCache,
                        urlBuilderStrategy,
                        contextBatch.getStandaloneResourcesOfType(requestCache, type)));
            }

            // Assembling resource urls for all web resources, usually it's resources with legacy stuff or special
            // legacy case when the web-resource has been excluded.
            for (final WebResourceBatch webResourceBatch : webResourceBatches) {
                for (final SubBatch subBatch : webResourceBatch.getSubBatches()) {
                    final List<Resource> resources = subBatch.getResourcesOfType(requestCache, type);
                    if (!resources.isEmpty()) {
                        final StateEncodedUrlResult stateEncodedUrlResult = encodeStateInUrlIfSupported(
                                requestCache,
                                urlBuilderStrategy,
                                type,
                                subBatch.getResourcesParams(),
                                subBatch.getBundles(),
                                new ArrayList<>(),
                                new ArrayList<>());

                        final DefaultUrlBuilder urlBuilder = stateEncodedUrlResult.getUrlBuilder();
                        if (globals.getConfig().isWebResourceBatchingEnabled()) {
                            resourceUrls.add(new WebResourceSubBatchUrl(
                                    requestCache.getGlobals(),
                                    webResourceBatch.getKey(),
                                    subBatch,
                                    type,
                                    urlBuilder.buildParams(),
                                    urlBuilder.buildHash()));
                        } else {
                            for (final Resource resource : resources) {
                                resourceUrls.add(new ResourceUrlImpl(
                                        globals, resource, urlBuilder.buildParams(), urlBuilder.buildHash()));
                            }
                        }
                    }
                }
                resourceUrls.addAll(createResourceUrlsForRedirectResources(
                        requestCache,
                        urlBuilderStrategy,
                        webResourceBatch.getStandaloneResourcesOfType(requestCache, type)));
            }
        }
        return resourceUrls;
    }

    /**
     * Split batches into sub batches grouped by URL parameters and set of standalone resources.
     *
     * @param contextBatchKeys     keys of context batches
     * @param webResourceBatchKeys keys of web resource batches.
     * @return context batches and web resource batches split into sub batches.
     */
    public static Tuple<List<ContextBatch>, List<WebResourceBatch>> splitIntoSubBatches(
            final RequestCache requestCache,
            final UrlBuildingStrategy urlBuilderStrategy,
            final List<ContextBatchKey> contextBatchKeys,
            final List<String> webResourceBatchKeys,
            final Set<String> allExcluded) {
        // Processing context batches.
        final List<ContextBatch> contextBatches = new ArrayList<>();
        final Set<String> alreadyIncluded = new HashSet<>(allExcluded);
        for (final ContextBatchKey key : contextBatchKeys) {
            final Found found = new BundleFinder(requestCache.getSnapshot())
                    .included(key.getIncluded())
                    .excludedResolved(alreadyIncluded)
                    .deepFilter(isConditionsSatisfied(requestCache, urlBuilderStrategy))
                    .onDeepFilterFail(PredicateFailStrategy.CONTINUE)
                    .endAndGetResult();

            // Only UrlReadingConditions should be in Context Batch, removing any legacy conditions.
            final List<String> skippedWebResourcesWithUrlReadingConditions = new ArrayList<>();
            for (final Bundle bundle : requestCache.getSnapshot().toBundles(found.getSkipped())) {
                if (bundle.getCondition() != null) {
                    skippedWebResourcesWithUrlReadingConditions.add(bundle.getKey());
                }
            }

            // We need to add url parameters for excluded resources too, see PLUGWEB-339.
            final LinkedHashSet<String> excludedWithoutApplyingConditions = key.getExcluded();
            final SplitSubBatches result = splitBatchIntoSubBatches(requestCache, found, true);
            contextBatches.add(new ContextBatch(
                    key.getIncluded(),
                    key.getExcluded(),
                    skippedWebResourcesWithUrlReadingConditions,
                    new ArrayList<>(excludedWithoutApplyingConditions),
                    result.getContextSubBatches(),
                    result.getContextStandaloneResources(),
                    result.isAdditionalSortingRequired()));

            alreadyIncluded.addAll(found.getFound());
        }

        // Processing web resource batches.
        final List<WebResourceBatch> webResourceBatches = new ArrayList<>();

        // Splitting into sub batches.
        for (final String key : webResourceBatchKeys) {
            final List<String> keys = new ArrayList<>();
            keys.add(key);
            final Found found = new BundleFinder(requestCache.getSnapshot())
                    .included(keys)
                    .deep(false)
                    .deepFilter(isConditionsSatisfied(requestCache, urlBuilderStrategy))
                    .endAndGetResult();

            final SplitSubBatches result = splitBatchIntoSubBatches(requestCache, found, false);

            if (!result.getContextSubBatches().isEmpty()
                    && result.getLegacyWebResources().isEmpty()) {
                webResourceBatches.add(new WebResourceBatch(
                        key, result.getContextSubBatches(), result.getContextStandaloneResources()));
            } else if (result.getContextSubBatches().isEmpty()
                    && !result.getLegacyWebResources().isEmpty()) {
                if (!result.getContextStandaloneResources().isEmpty()) {
                    throw new RuntimeException("single web resource cannot have context standalone resources!");
                }
                if (result.getLegacyWebResources().size() > 1) {
                    throw new RuntimeException("single web resource cannot split into multiple web resources!");
                }
                webResourceBatches.add(result.getLegacyWebResources().get(0));
            } else if (!contextBatchKeys.isEmpty()
                    && !result.getLegacyWebResources().isEmpty()) {
                throw new RuntimeException(
                        "single web resource batch could be either legacy or not, but not both at the same time!");
            }
        }
        return new Tuple<>(contextBatches, webResourceBatches);
    }

    /**
     * Calculates URL resource hashes for given request.
     *
     * @param rawRequest raw request.
     * @param requestCache    request cache.
     * @return calculated resource hashes.
     */
    public static String calculateBundleHash(final RawRequest rawRequest, final RequestCache requestCache) {
        return calculateBundlesHash(getAllBundlesForContext(rawRequest, requestCache));
    }

    public static String calculateBundlesHash(final List<Bundle> bundles) {
        final HashBuilder hashBuilder = new HashBuilder();

        bundles.forEach(bundle -> {
            hashBuilder.add(bundle.getKey());
            hashBuilder.add(bundle.getVersion());
        });

        return hashBuilder.build();
    }

    /**
     * Split batch into sub batches and standalone resources, same code used to split both context and resource batches.
     *
     * @param found list of complete keys that the batch contains.
     * @param doSorting    some legacy stuff, in some cases it sorted in some not.
     * @return batch split into sub batches and
     */
    protected static SplitSubBatches splitBatchIntoSubBatches(
            final RequestCache requestCache, final Found found, final boolean doSorting) {
        final String IS_STANDALONE = "_isStandalone";
        final List<Bundle> bundles = requestCache.getSnapshot().toBundles(found.getFound());
        final List<Bundle> bundlesForHashCalculation =
                requestCache.getSnapshot().toBundles(found.getAll());

        /*
         * During splitting into sub-batches the order of standalone resources would be lost,
         * this comparator needed to restore the right order.
         */
        final Map<Resource, Integer> allResourcesOrdered = new HashMap<>();
        int i = 0;
        Comparator<Resource> RESOURCE_COMPARATOR = comparingInt(allResourcesOrdered::get);

        // Getting list of unique params combinations, each combination would produce one sub-batch.
        final Map<Map<String, String>, List<Resource>> uniqueParams = new LinkedHashMap<>();
        final LinkedHashMap<Bundle, Map<Map<String, String>, List<Resource>>> legacyUniqueParams =
                new LinkedHashMap<>();
        for (final Bundle bundle : bundles) {
            for (final Resource resource : bundle.getResources(requestCache).values()) {
                allResourcesOrdered.put(resource, i);
                i += 1;

                Map<String, String> params = resource.getUrlParams();
                if (!resource.isBatchable()) {
                    params.put(IS_STANDALONE, "true");
                }

                uniqueParams.computeIfAbsent(params, key -> new ArrayList<>()).add(resource);
            }
        }

        // Sorting list of unique params combination, the sub-batches would be sorted in the same way.
        final SplitSubBatches result = new SplitSubBatches();
        final List<Map<String, String>> uniqueParamsSorted = new ArrayList<>(uniqueParams.keySet());
        final ParamsComparator paramsComparator = new ParamsComparator();
        if (doSorting) {
            uniqueParamsSorted.sort(paramsComparator);
        }
        result.setAdditionalSortingRequired(paramsComparator.isAdditionalSortingRequired());

        // Sorting list of unique params combination for legacy web resources, the sub-batches would be sorted in the
        // same way.
        final Map<Bundle, List<Map<String, String>>> legacyUniqueParamsSorted = new LinkedHashMap<>();
        for (final Entry<Bundle, Map<Map<String, String>, List<Resource>>> entry : legacyUniqueParams.entrySet()) {
            final List<Map<String, String>> webResourceUniqueParamsSorted =
                    new ArrayList<>(entry.getValue().keySet());
            legacyUniqueParamsSorted.put(entry.getKey(), webResourceUniqueParamsSorted);
        }

        // Assembling sub batches.
        result.setContextSubBatches(new ArrayList<>());
        result.setContextStandaloneResources(new ArrayList<>());
        for (final Map<String, String> params : uniqueParamsSorted) {
            if (params.containsKey(IS_STANDALONE)) {
                result.getContextStandaloneResources().addAll(uniqueParams.get(params));
            } else {
                result.getContextSubBatches()
                        .add(new SubBatch(params, bundles, uniqueParams.get(params), bundlesForHashCalculation));
            }
        }
        result.getContextStandaloneResources().sort(RESOURCE_COMPARATOR);

        // Assembling legacy sub batches, it should be last and should be re-grouped into web resource batches.
        result.setLegacyWebResources(new ArrayList<>());
        for (final Entry<Bundle, Map<Map<String, String>, List<Resource>>> entry : legacyUniqueParams.entrySet()) {

            final List<Resource> webResourceStandaloneResources = new ArrayList<>();
            final List<SubBatch> webResourceSubBatches = new ArrayList<>();
            for (final Map<String, String> params : legacyUniqueParamsSorted.get(entry.getKey())) {
                if (params.containsKey(IS_STANDALONE)) {
                    webResourceStandaloneResources.addAll(uniqueParams.get(params));
                } else {
                    final List<Resource> resources =
                            legacyUniqueParams.get(entry.getKey()).get(params);
                    webResourceSubBatches.add(new SubBatch(params, entry.getKey(), resources));
                }
            }

            webResourceStandaloneResources.sort(RESOURCE_COMPARATOR);
            result.getLegacyWebResources()
                    .add(new WebResourceBatch(
                            entry.getKey().getKey(), webResourceSubBatches, webResourceStandaloneResources));
        }
        return result;
    }

    /**
     * Get resources of given type.
     */
    public static List<Resource> resourcesOfType(final Collection<Resource> resources, final String type) {
        final List<Resource> result = new ArrayList<>();
        for (Resource resource : resources) {
            if (type.equals(resource.getNameOrLocationType())) {
                result.add(resource);
            }
        }
        return result;
    }

    /**
     * Collect and encode URL state for given Resources, it collects resource params, condition params and transformer params.
     */
    protected static StateEncodedUrlResult encodeStateInUrlIfSupported(
            final RequestCache requestCache,
            final UrlBuildingStrategy urlBuilderStrategy,
            final String type,
            final Map<String, String> params,
            final Bundle bundle,
            final List<Bundle> skipped,
            final List<Bundle> excludedWithoutApplyingConditions) {
        final List<Bundle> bundles = new ArrayList<>();
        bundles.add(bundle);
        return encodeStateInUrlIfSupported(
                requestCache, urlBuilderStrategy, type, params, bundles, skipped, excludedWithoutApplyingConditions);
    }

    /**
     * Collect and encode URL state for given Resources, it collects resource params, condition params and transformer params.
     */
    protected static StateEncodedUrlResult encodeStateInUrlIfSupported(
            final RequestCache requestCache,
            final UrlBuildingStrategy urlBuilderStrategy,
            final String type,
            final Map<String, String> params,
            final List<Bundle> bundles,
            final List<Bundle> skipped,
            final List<Bundle> excludedWithoutApplyingConditions) {
        // Adding resource params.
        final DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        for (final Entry<String, String> entry : params.entrySet()) {
            urlBuilder.addToQueryString(entry.getKey(), entry.getValue());
        }

        // Adding parameters for UrlReadingConditions. The url parameters from failed and excluded UrlReadingConditions
        // also should be added.
        final List<Bundle> bundlesWithSkippedAndExcluded = new ArrayList<>(bundles);
        bundlesWithSkippedAndExcluded.addAll(skipped);
        bundlesWithSkippedAndExcluded.addAll(excludedWithoutApplyingConditions);

        for (final Bundle bundle : bundlesWithSkippedAndExcluded) {
            final CachedCondition condition = bundle.getCondition();
            if (condition != null) {
                condition.addToUrlSafely(requestCache, urlBuilder, urlBuilderStrategy);
            }
        }

        // Adding transformer params.
        for (final Bundle bundle : bundles) {
            final CachedTransformers transformers = bundle.getTransformers();
            if (transformers != null) {
                for (final String locationType : bundle.getLocationResourceTypesFor(type)) {
                    transformers.addToUrlSafely(
                            urlBuilder,
                            urlBuilderStrategy,
                            locationType,
                            requestCache.getGlobals().getConfig().getTransformerCache(),
                            bundle.getTransformerParameters(),
                            bundle.getKey());
                }
            }
            for (final String locationType : bundle.getLocationResourceTypesFor(type)) {
                requestCache
                        .getGlobals()
                        .getConfig()
                        .getStaticTransformers()
                        .addToUrl(locationType, bundle.getTransformerParameters(), urlBuilder, urlBuilderStrategy);
            }
        }
        return new StateEncodedUrlResult(urlBuilder);
    }

    /**
     * Creates Resource URLs for redirect resources.
     */
    protected static List<ResourceUrl> createResourceUrlsForRedirectResources(
            final RequestCache requestCache,
            final UrlBuildingStrategy urlBuilderStrategy,
            final List<Resource> resources) {

        final List<ResourceUrl> resourceUrls = new ArrayList<>();
        for (final Resource resource : resources) {
            // Calculating params and hash.
            final DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
            for (final Entry<String, String> entry : resource.getParams().entrySet()) {
                urlBuilder.addToQueryString(entry.getKey(), entry.getValue());
            }

            final Bundle webResource = resource.getParent();
            final CachedCondition condition = webResource.getCondition();
            if (condition != null) {
                condition.addToUrlSafely(requestCache, urlBuilder, urlBuilderStrategy);
            }

            final CachedTransformers transformers = webResource.getTransformers();
            if (transformers != null) {
                transformers.addToUrlSafely(
                        urlBuilder,
                        urlBuilderStrategy,
                        resource.getLocationType(),
                        requestCache.getGlobals().getConfig().getTransformerCache(),
                        webResource.getTransformerParameters(),
                        webResource.getKey());
            }

            requestCache
                    .getGlobals()
                    .getConfig()
                    .getStaticTransformers()
                    .addToUrl(
                            resource.getLocationType(),
                            webResource.getTransformerParameters(),
                            urlBuilder,
                            urlBuilderStrategy);

            resourceUrls.add(new ResourceUrlImpl(
                    requestCache.getGlobals(), resource, urlBuilder.buildParams(), urlBuilder.buildHash()));
        }
        return resourceUrls;
    }

    private static List<Bundle> getAllBundlesForContext(final RawRequest raw, final RequestCache requestCache) {
        final Found found = new BundleFinder(requestCache.getSnapshot())
                .included(raw.getIncludedAsLooseType())
                .excluded(raw.getExcludedAsLooseType(), Predicates.alwaysTrue())
                .onDeepFilterFail(PredicateFailStrategy.CONTINUE)
                .endAndGetResult();

        return requestCache.getGlobals().getSnapshot().toBundles(found.getAll());
    }
}
