package com.atlassian.plugin.webresource.transformer;

import javax.annotation.Nonnull;

import io.atlassian.util.concurrent.ResettableLazyReference;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;

/**
 * Defines a module descriptor for a {@link ContentTransformerFactory}.
 *
 * @since 3.3
 */
public class ContentTransformerModuleDescriptor extends AbstractModuleDescriptor<ContentTransformerFactory> {
    private final ResettableLazyReference<ContentTransformerFactory> moduleLazyReference =
            new ResettableLazyReference<ContentTransformerFactory>() {
                @Override
                protected ContentTransformerFactory create() throws Exception {
                    return moduleFactory.createModule(moduleClassName, ContentTransformerModuleDescriptor.this);
                }
            };
    private String aliasKey;

    public ContentTransformerModuleDescriptor(ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        this.aliasKey = element.attributeValue("alias-key");
        super.init(plugin, element);
    }

    @Override
    public void disabled() {
        moduleLazyReference.reset();
        super.disabled();
    }

    @Override
    public ContentTransformerFactory getModule() {
        return moduleLazyReference.get();
    }

    public String getAliasKey() {
        return aliasKey;
    }
}
