package com.atlassian.plugin.webresource.assembler;

import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;

import static java.util.Objects.requireNonNull;

/**
 * Represents a wrapper for resources urls, since there are several ways of retrieving resources urls.
 *
 * @since 5.0.0
 */
public class ResourceUrls {
    private final ResourceUrl resourceUrl;
    private final PluginUrlResource<?> pluginUrlResource;

    public ResourceUrls(@Nonnull final ResourceUrl resourceUrl, @Nonnull final PluginUrlResource<?> pluginUrlResource) {
        this.resourceUrl = requireNonNull(resourceUrl, "The resource url is mandatory.");
        this.pluginUrlResource = requireNonNull(pluginUrlResource, "The plugin url resource is mandatory.");
    }

    @Nonnull
    public PluginUrlResource<?> getPluginUrlResource() {
        return pluginUrlResource;
    }

    @Nonnull
    public ResourceUrl getResourceUrl() {
        return resourceUrl;
    }
}
