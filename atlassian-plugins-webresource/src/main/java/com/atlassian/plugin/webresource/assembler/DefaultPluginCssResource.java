package com.atlassian.plugin.webresource.assembler;

import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginCssResourceParams;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

/**
 * Implementation of PluginUrlResource
 *
 * @since 3.0
 */
public class DefaultPluginCssResource extends DefaultPluginUrlResource<PluginCssResourceParams>
        implements PluginCssResource {
    public DefaultPluginCssResource(@Nonnull final ResourceUrl resourceUrl) {
        super(resourceUrl);
    }

    public DefaultPluginCssResource(
            @Nonnull final ResourceUrl resourceUrl, @Nonnull final ResourcePhase resourcePhase) {
        super(resourceUrl, resourcePhase);
    }

    @Override
    public PluginCssResourceParams getParams() {
        return new DefaultPluginCssResourceParams(
                resourceUrl.getParams(), resourceUrl.getKey(), resourceUrl.getBatchType());
    }
}
