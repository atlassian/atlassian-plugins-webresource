package com.atlassian.plugin.webresource.impl.snapshot.resource;

import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Files;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contentprovider.ContentProviderStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.contenttype.ContentTypeStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.path.PathStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream.StreamStrategy;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.Support;

import static java.util.Arrays.asList;

/**
 * Resource of Web Resource or Plugin.
 *
 * @since 3.3
 */
public class Resource {
    private static final Set<String> HTTP_PARAM_NAMES_SET = new HashSet<>(asList(Config.HTTP_PARAM_NAMES));

    @VisibleForTesting
    final StreamStrategy streamStrategy;

    private final Bundle parent;
    private final String nameType;
    private final String locationType;
    private final ResourceLocation resourceLocation;
    private final ContentTypeStrategy contentTypeStrategy;
    private final PathStrategy pathStrategy;
    private final ContentProviderStrategy contentProviderStrategy;

    Resource(
            Bundle parent,
            ResourceLocation resourceLocation,
            String nameType,
            String locationType,
            ContentTypeStrategy contentTypeStrategy,
            StreamStrategy streamStrategy,
            PathStrategy pathStrategy,
            ContentProviderStrategy contentProviderStrategy) {
        this.parent = parent;
        this.nameType = nameType;
        this.locationType = locationType;
        this.resourceLocation = sanitizeResourceLocation(resourceLocation);
        this.contentTypeStrategy = contentTypeStrategy;
        this.streamStrategy = streamStrategy;
        this.pathStrategy = pathStrategy;
        this.contentProviderStrategy = contentProviderStrategy;
    }

    public static Map<String, String> getUrlParamsStatic(final Map<String, String> params) {
        final Map<String, String> urlParams = new HashMap<>();
        for (final Map.Entry<String, String> entry : params.entrySet()) {
            if (entry.getKey().equals(Config.ALLOW_PUBLIC_USE_PARAM_NAME)
                    && entry.getValue().equals(Boolean.FALSE.toString())) {
                // false is the default, we don't want to propagate it out to the URL (or risk unnecessary batch
                // splitting
                continue;
            }
            if (HTTP_PARAM_NAMES_SET.contains(entry.getKey())) {
                urlParams.put(entry.getKey(), entry.getValue());
            }
        }
        return urlParams;
    }

    public static boolean isCacheableStatic(Map<String, String> params) {
        return !"false".equalsIgnoreCase(params.get(Config.CACHE_PARAM_NAME));
    }

    public static String getPrebuiltSourcePath(String resourceName) {
        String fileExtension = Files.getFileExtension(resourceName);
        String baseName = resourceName.substring(0, resourceName.length() - fileExtension.length() - 1);
        return baseName + "-source." + fileExtension;
    }

    public static boolean isPrebuiltSourceName(String name) {
        return name.contains("-source.");
    }

    public static String getResourceNameFromPrebuiltSourceName(String prebuiltSourcePath) {
        return prebuiltSourcePath.replace("-source.", ".");
    }

    /**
     * PLUGWEB-429 - HACK - We need to sanitize the params we retrieve from a resource location, so that
     * we can de-duplicate "alike" values before any batching logic would create multiple discrete URLs
     * for them.
     */
    static ResourceLocation sanitizeResourceLocation(ResourceLocation res) {
        Map<String, String> newParams = res.getParams();
        res = new ResourceLocation(
                res.getLocation(),
                res.getName(),
                res.getType(),
                res.getContentType(),
                res.getContent(),
                ImmutableMap.copyOf(newParams));
        return res;
    }

    /**
     * Get Parent - Web Resource or Plugin.
     */
    public Bundle getParent() {
        return parent;
    }

    public String getName() {
        return resourceLocation.getName();
    }

    public String getFullName() {
        return getParent().getKey() + "/" + resourceLocation.getName();
    }

    public String getLocation() {
        return resourceLocation.getLocation();
    }

    public boolean isRedirect() {
        return "webContext".equalsIgnoreCase(resourceLocation.getParameter(Config.SOURCE_PARAM_NAME));
    }

    public String getNameType() {
        return nameType;
    }

    public String getNameOrLocationType() {
        return nameType.isEmpty() ? locationType : nameType;
    }

    public String getLocationType() {
        return locationType;
    }

    public String getContentType() {
        return this.contentTypeStrategy.getContentType();
    }

    public boolean isBatchable() {
        return !isRedirect() && !"false".equalsIgnoreCase(resourceLocation.getParameter(Config.BATCH_PARAM_NAME));
    }

    public InputStream getStreamFor(String path) {
        return streamStrategy.getInputStream(path);
    }

    public String getPath() {
        return pathStrategy.getPath();
    }

    /**
     * Should it be included in Batch with given Parameters?
     */
    public boolean isBatchable(Map<String, String> batchParams) {
        if (!isBatchable()) {
            return false;
        }
        for (final String key : Config.HTTP_PARAM_NAMES) {
            if (!Support.equals(batchParams.get(key), getParams().get(key))) {
                return false;
            }
        }
        return true;
    }

    public Map<String, String> getParams() {
        return resourceLocation.getParams();
    }

    /**
     * Get only those parameters that would be output to url.
     */
    public Map<String, String> getUrlParams() {
        return getUrlParamsStatic(getParams());
    }

    public boolean isTransformable() {
        return parent.isTransformable();
    }

    public String getVersion() {
        return parent.getVersion();
    }

    public String getKey() {
        return parent.getKey();
    }

    public boolean isCacheable() {
        return isCacheableStatic(this.getUrlParams());
    }

    public Content getContent() {
        return this.contentProviderStrategy.getContent();
    }

    /**
     * @deprecated since 3.3.2
     */
    @Deprecated
    public ResourceLocation getResourceLocation() {
        return resourceLocation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if ((o == null) || (getClass() != o.getClass())) {
            return false;
        }
        Resource resource = (Resource) o;
        return parent.equals(resource.parent) && getName().equals(resource.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(parent, getName());
    }

    @Override
    public String toString() {
        return "{" + getName() + (!isBatchable() ? " isNotBatchable" : "") + "}";
    }
}
