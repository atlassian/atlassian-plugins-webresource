package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.helpers.FileOperations;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;

import static com.atlassian.plugin.webresource.util.WebResourceKeyHelper.isWebResourceKey;

public class StreamStrategyFactory {
    public static String WEB_CONTEXT_STATIC = "webContextStatic";
    public static String PLUGIN_SHARED_HOME = "pluginSharedHome";

    private final ServletContextFactory servletContextFactory;
    private final WebResourceIntegration webResourceIntegration;

    private static final FileOperations fileOperations = new FileOperations();

    public StreamStrategyFactory(
            ServletContextFactory servletContextFactory, WebResourceIntegration webResourceIntegration) {
        this.servletContextFactory = servletContextFactory;
        this.webResourceIntegration = webResourceIntegration;
    }

    public StreamStrategy createStandardModuleStreamStrategy(Bundle parent, ResourceLocation resourceLocation) {
        String sourceParam = resourceLocation.getParameter(Config.SOURCE_PARAM_NAME);
        final boolean isWebContextStatic = WEB_CONTEXT_STATIC.equalsIgnoreCase(sourceParam);
        final boolean isSourceSharedPluginHome = PLUGIN_SHARED_HOME.equalsIgnoreCase(sourceParam);

        if (isSourceSharedPluginHome) {
            if (!isWebResourceKey(
                    parent.getKey())) { // TODO NEXT STEPs: Override WebResourceModuleDerscription init, do validation
                // there,
                // and then drop the check for isKeyWebResourceIdentifier here since it will be
                // guaranteed to be ok
                throw new IllegalArgumentException("Invalid pluginSharedHome resource: " + resourceLocation);
            }

            return new PluginSharedHomeStreamStrategy(fileOperations, webResourceIntegration, parent);
        } else if (isWebContextStatic) {
            return new TomcatStreamStrategy(servletContextFactory);
        } else {
            if (isWebResourceKey(parent.getKey())) {
                return new WebResourceStreamStrategy(webResourceIntegration, parent);
            } else {
                return new PluginStreamStrategy(webResourceIntegration, parent);
            }
        }
    }
}
