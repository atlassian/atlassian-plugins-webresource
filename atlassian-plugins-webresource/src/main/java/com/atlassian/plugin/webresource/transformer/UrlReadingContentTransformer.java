package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.webresource.api.QueryParams;

/**
 * The part of a transformer that reads the config and url params and does the actual resource transformation and
 * generates the Source Map.
 *
 * @since 3.3
 */
public interface UrlReadingContentTransformer {
    /**
     * Transforms the downloadable resource by returning a new one.  This is invoked when the resource is being served.
     *
     * @param cdnResourceUrlTransformer transforms relative URLs into CDN URLs
     * @param content                   content to transform
     * @param location                  resource details
     * @param params                    query params
     * @param sourceUrl                 url of source code for resource.
     * @return The new resource representing the transformed resource and the source map.
     */
    Content transform(
            CdnResourceUrlTransformer cdnResourceUrlTransformer,
            Content content,
            ResourceLocation location,
            QueryParams params,
            String sourceUrl);
}
