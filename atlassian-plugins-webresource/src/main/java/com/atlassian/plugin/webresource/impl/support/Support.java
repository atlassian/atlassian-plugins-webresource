package com.atlassian.plugin.webresource.impl.support;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.List;
import java.util.function.Predicate;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Predicates;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.sourcemap.ReadableSourceMap;

import static com.atlassian.sourcemap.ReadableSourceMapImpl.fromSource;

/**
 * Basic utility functions.
 *
 * @since v6.3
 */
public class Support {
    public static final Logger LOGGER = LoggerFactory.getLogger("webresource");

    /**
     * Join list of predicates with AND operand.
     */
    public static Predicate<Bundle> efficientAndPredicate(List<Predicate<Bundle>> predicates) {
        if (predicates.size() == 0) {
            return Predicates.alwaysTrue();
        } else if (predicates.size() == 1) {
            return predicates.get(0);
        } else {
            return predicates.stream().reduce(Predicate::and).get();
        }
    }

    /**
     * Helper to check for equality without bothering to handle nulls.
     */
    public static boolean equals(Object a, Object b) {
        return a == null ? b == null : a.equals(b);
    }

    /**
     * Copy in into out and close streams safely.
     */
    public static void copy(final InputStream in, final OutputStream out) {
        try {
            IOUtils.copy(in, out);
        } catch (final IOException e) {
            logIOException(e);
        } finally {

            IOUtils.closeQuietly(in);
            try {
                out.flush();
            } catch (final IOException e) {
                Support.LOGGER.debug("Error flushing output stream", e);
            }
        }
    }

    public static void logIOException(IOException e) {
        if (e instanceof SocketException && "Broken pipe".equals(e.getMessage())) {
            Support.LOGGER.trace("Client Abort error", e);
        } else {
            Support.LOGGER.debug("IO Exception", e);
        }
    }

    public static ReadableSourceMap getSourceMap(
            final String resourcePath, final Resource resource, final String sourceUrl) {
        // Loading source map.
        String pathForSourceMap = resourcePath + ".map";
        InputStream sourceMapAsStream = resource.getStreamFor(pathForSourceMap);
        if (sourceMapAsStream == null) {
            // Currently almost no one plugin has source maps, there will be too many
            // warnings in the log. Maybe it will be enabled later.
            // Support.LOGGER.warn("no source map for " + resource.getKey() + "/" + resource.getName());
            return null;
        } else {
            return fromSource(sourceMapAsStream);
        }
    }
}
