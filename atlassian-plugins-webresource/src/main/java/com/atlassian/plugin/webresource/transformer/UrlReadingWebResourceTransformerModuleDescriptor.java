package com.atlassian.plugin.webresource.transformer;

import javax.annotation.Nonnull;

import io.atlassian.util.concurrent.ResettableLazyReference;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

/**
 * Defines a module descriptor for a {@link WebResourceTransformerFactory}.
 *
 * @since 3.0
 */
public class UrlReadingWebResourceTransformerModuleDescriptor
        extends AbstractModuleDescriptor<WebResourceTransformerFactory>
        implements com.atlassian.webresource.api.decorator.UrlReadingWebResourceTransformerModuleDescriptor {
    private final ResettableLazyReference<WebResourceTransformerFactory> moduleLazyReference =
            new ResettableLazyReference<WebResourceTransformerFactory>() {
                @Override
                protected WebResourceTransformerFactory create() throws Exception {
                    return moduleFactory.createModule(
                            moduleClassName, UrlReadingWebResourceTransformerModuleDescriptor.this);
                }
            };
    private String aliasKey;

    public UrlReadingWebResourceTransformerModuleDescriptor(ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        this.aliasKey = element.attributeValue("alias-key");
        super.init(plugin, element);
    }

    @Override
    public void disabled() {
        moduleLazyReference.reset();
        super.disabled();
    }

    @Override
    public WebResourceTransformerFactory getModule() {
        return moduleLazyReference.get();
    }

    public String getAliasKey() {
        return aliasKey;
    }
}
