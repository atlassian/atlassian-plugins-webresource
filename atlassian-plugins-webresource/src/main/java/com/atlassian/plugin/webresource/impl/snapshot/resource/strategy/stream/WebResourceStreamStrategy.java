package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream;

import java.io.InputStream;

import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;

public class WebResourceStreamStrategy implements StreamStrategy {
    private final WebResourceIntegration webResourceIntegration;
    private final Bundle bundle;

    WebResourceStreamStrategy(WebResourceIntegration webResourceIntegration, Bundle bundle) {
        this.webResourceIntegration = webResourceIntegration;
        this.bundle = bundle;
    }

    @Override
    public InputStream getInputStream(String path) {
        return webResourceIntegration
                .getPluginAccessor()
                .getEnabledPluginModule(this.bundle.getKey())
                .getPlugin()
                .getResourceAsStream(path);
    }
}
