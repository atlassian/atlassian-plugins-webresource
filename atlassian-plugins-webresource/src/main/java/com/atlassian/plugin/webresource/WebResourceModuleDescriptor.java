package com.atlassian.plugin.webresource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;
import io.atlassian.fugue.Option;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.baseconditions.AbstractConditionElementParser;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.condition.UrlReadingConditionElementParser;
import com.atlassian.plugin.webresource.data.WebResourceDataProviderParser;
import com.atlassian.plugin.webresource.impl.snapshot.Deprecation;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import static io.atlassian.fugue.Option.option;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;

import static com.atlassian.plugin.webresource.util.WebResourceKeyHelper.isWebResourceKey;

/**
 * A way of linking to web 'resources', such as javascript or css.  This allows us to include resources once
 * on any given page, as well as ensuring that plugins can declare resources, even if they are included
 * at the bottom of a page.
 */
public class WebResourceModuleDescriptor extends AbstractModuleDescriptor<Void>
        implements com.atlassian.webresource.api.descriptor.WebResourceModuleDescriptor {
    private final HostContainer hostContainer;
    private boolean isRootPage;
    private List<String> dependencies = emptyList();
    private Set<String> contextDependencies = emptySet();
    private Set<String> contexts = emptySet();
    private boolean disableMinification;
    private WebResourceDataProviderParser dataProviderParser;
    private Deprecation deprecation;
    private Map<String, WebResourceDataProvider> dataProviders = emptyMap();
    private List<WebResourceTransformation> webResourceTransformations = emptyList();
    private UrlReadingConditionElementParser conditionElementParser;
    private Element element;
    private DecoratingCondition condition;

    /**
     * @deprecated Since 3.0.0 - use {@link #WebResourceModuleDescriptor(com.atlassian.plugin.module.ModuleFactory, com.atlassian.plugin.hostcontainer.HostContainer)}
     */
    @Deprecated
    public WebResourceModuleDescriptor(final HostContainer hostContainer) {
        this(ModuleFactory.LEGACY_MODULE_FACTORY, hostContainer);
    }

    public WebResourceModuleDescriptor(ModuleFactory moduleFactory, final HostContainer hostContainer) {
        super(moduleFactory);
        this.conditionElementParser = new UrlReadingConditionElementParser(hostContainer);
        this.hostContainer = hostContainer;
    }

    /**
     * Expands any local web-resource dependency to include the appropriate plugin key,
     * so that consumers can correctly locate the dependency.
     */
    private static String resolveLocalDependency(String rawKey, Plugin plugin) {
        if (isWebResourceKey(rawKey) && rawKey.startsWith(":")) {
            return plugin.getKey() + rawKey;
        } else {
            return rawKey;
        }
    }

    private static List<String> resolveLocalDependencyList(List<String> dependencies, Plugin plugin) {
        return Collections.unmodifiableList(dependencies.stream()
                .map(key -> resolveLocalDependency(key, plugin))
                .collect(Collectors.toList()));
    }

    private static Deprecation parseDeprecation(Element element, @Nonnull String key) {
        final Element depEl = element.element("deprecated");
        Deprecation depNotice = null;
        if (depEl != null) {
            depNotice = new Deprecation(key);
            depNotice.setSinceVersion(depEl.attributeValue("since"));
            depNotice.setRemoveInVersion(depEl.attributeValue("remove"));
            depNotice.setAlternative(depEl.attributeValue("alternative"));
            depNotice.setExtraInfo(depEl.getText());
        }
        return depNotice;
    }

    /**
     * Parse transformations.
     */
    public static List<WebResourceTransformation> parseTransformations(Element element) {
        List<Element> transformations = (List<Element>) element.elements("transformation");
        if (!transformations.isEmpty()) {
            final List<WebResourceTransformation> trans = new ArrayList<>(transformations.size());
            for (Element e : transformations) {
                trans.add(new WebResourceTransformation(e));
            }
            return ImmutableList.copyOf(trans);
        }
        return emptyList();
    }

    public static DecoratingCondition parseCondition(
            UrlReadingConditionElementParser conditionElementParser, Plugin plugin, Element element) {
        try {
            return conditionElementParser.makeConditions(
                    plugin, element, AbstractConditionElementParser.CompositeType.AND);
        } catch (final PluginParseException e) {
            // is there a better exception to throw?
            throw new RuntimeException("Unable to enable web resource due to issue processing condition", e);
        }
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException {
        super.init(plugin, element);

        this.dependencies =
                resolveLocalDependencyList(parseDependencies(element, "web-resource", "dependency"), plugin);

        this.contextDependencies = parseDependencySet(element, "dependencies", "context");

        this.isRootPage = element.element("root-page") != null;
        final LinkedHashSet<String> ctxs = new LinkedHashSet<>(contexts.size());
        ctxs.add(getCompleteKey()); // every <web-resource> has an implicit context based on its name

        List<Element> contexts = element.elements("context");
        if (!contexts.isEmpty()) {
            for (Element contextElement : contexts) {
                ctxs.add(contextElement.getTextTrim());
            }
        }
        this.contexts = Collections.unmodifiableSet(ctxs);

        webResourceTransformations = parseTransformations(element);
        deprecation = parseDeprecation(element, getCompleteKey());

        List<Element> dataElements = element.elements("data");
        dataProviderParser = new WebResourceDataProviderParser(hostContainer, dataElements);

        final String minifiedAttribute = element.attributeValue("disable-minification");
        disableMinification = Boolean.parseBoolean(minifiedAttribute);
        this.element = element;
    }

    /**
     * Parses dependencies of the given type form the XML and returns them as a set.
     */
    private Set<String> parseDependencySet(
            final Element element, final String parentElementName, final String subElementName) {
        return Collections.unmodifiableSet((Set<String>) Stream.of(element.element(parentElementName))
                .filter(e -> e != null)
                .flatMap(parentElement -> (Stream<Element>) parentElement.elements(subElementName).stream())
                .map(Element::getTextTrim)
                .collect(Collectors.toCollection(LinkedHashSet::new)));
    }

    /**
     * Parses dependencies of the given types from the XML and returns them as a combined list.
     *
     * @param element    the XML root element
     * @param name       the element name of the dependency (used in the dependencies block)
     * @param legacyName the legacy name of the dependency (used outside the dependencies block)
     */
    private List<String> parseDependencies(final Element element, final String name, final String legacyName) {
        final List<Element> webResourceDependencyElement = element.elements(legacyName);
        List<String> webResourceDependencies =
                webResourceDependencyElement.stream().map(Element::getTextTrim).collect(Collectors.toList());

        final Option<Element> dependenciesElementOpt = option(element.element("dependencies"));
        List<String> dependenciesElementWebResources = dependenciesElementOpt
                .map(dependenciesElement -> {
                    final List<Element> webResources = dependenciesElement.elements(name);
                    return webResources.stream().map(Element::getTextTrim).collect(Collectors.toList());
                })
                .getOrElse(new ArrayList<>());

        List<String> allWebResourceDependencies =
                new ArrayList<>(webResourceDependencies.size() + dependenciesElementWebResources.size());
        allWebResourceDependencies.addAll(webResourceDependencies);
        allWebResourceDependencies.addAll(dependenciesElementWebResources);

        return ImmutableList.copyOf(allWebResourceDependencies);
    }

    /**
     * As this descriptor just handles resources, you should never call this
     */
    @Override
    public Void getModule() {
        throw new UnsupportedOperationException("There is no module for Web Resources");
    }

    @Override
    public void enabled() {
        super.enabled();
        condition = parseCondition(conditionElementParser, plugin, element);
        try {
            dataProviders = dataProviderParser.createDataProviders(plugin, this.getClass());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Unable to enable web resource due to an issue processing data-provider", e);
        } catch (final PluginParseException e) {
            throw new RuntimeException("Unable to enable web resource due to an issue processing data-provider", e);
        }
    }

    @Override
    public void disabled() {
        super.disabled();
        condition = null;
        dataProviders = emptyMap();
    }

    public boolean isDeprecated() {
        return deprecation != null;
    }

    public Deprecation getDeprecation() {
        return deprecation;
    }

    /**
     * Returns the web resource contexts this resource is associated with.
     *
     * @return the web resource contexts this resource is associated with.
     * @since 2.5.0
     */
    public Set<String> getContexts() {
        return contexts;
    }

    /**
     * Returns a list of dependencies on other web resources.
     *
     * @return a list of module complete keys
     */
    public List<String> getDependencies() {
        return dependencies;
    }

    /**
     * Returns an Option of the RootPage object.
     *
     * @return an Option of the RootPage object
     */
    public boolean isRootPage() {
        return isRootPage;
    }

    /**
     * Returns the set of contexts that this resource depends on.
     *
     * @return the set of contexts that this resource depends on
     * @since 3.5.9
     */
    public Set<String> getContextDependencies() {
        return contextDependencies;
    }

    public List<WebResourceTransformation> getTransformations() {
        return webResourceTransformations;
    }

    public DecoratingCondition getCondition() {
        return condition;
    }

    /**
     * @return <code>true</code> if resource minification should be skipped, <code>false</code> otherwise.
     */
    public boolean isDisableMinification() {
        return disableMinification;
    }

    /**
     * @return data providers specified by this web-resource
     * @since 3.0
     */
    public Map<String, WebResourceDataProvider> getDataProviders() {
        return dataProviders;
    }
}
