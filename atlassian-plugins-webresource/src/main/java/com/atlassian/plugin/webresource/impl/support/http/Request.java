package com.atlassian.plugin.webresource.impl.support.http;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.plugin.webresource.ResourceUtils;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.models.LooselyTypedRequestExpander;
import com.atlassian.plugin.webresource.models.RawRequest;
import com.atlassian.plugin.webresource.models.Requestable;
import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;

import static com.atlassian.plugin.webresource.impl.http.Router.buildUrl;
import static com.atlassian.plugin.webresource.impl.http.Router.parseWithRe;

/**
 * Possibly would be replaced by original request, currently original request is hard to use because parsed `path` and
 * `params` not available on it, it also hides some other implementation details that wont be needed after the
 * refactoring.
 * <p>
 * Also after the refactoring it would contain the `state` object - the only thing that will change during the request,
 * all other resources would be stateless.
 *
 * @since 3.3
 */
public class Request {
    private final String path;
    private final Map<String, String> params;
    private final HttpServletRequest originalRequest;
    private final boolean isCacheable;
    private final Globals globals;
    private final String url;
    private boolean sourcemap = false;
    private String filetype = "";
    private String resourceName = "";
    private ServingType servingType = ServingType.UNKNOWN;
    private RawRequest requested;

    public Request(Globals globals, String path, Map<String, String> params) {
        this.globals = globals;
        originalRequest = null;
        this.params = params;
        this.path = path;
        isCacheable = ResourceUtils.canRequestedResourcesContentBeAssumedConstant(getParams());
        this.url = buildUrl(path, params);
        setFiletype(path);
    }

    public Request(Globals globals, HttpServletRequest request, String encoding) {
        this.globals = globals;
        originalRequest = request;
        params = ResourceUtils.getQueryParameters(request);
        try {
            path = URLDecoder.decode(request.getRequestURI(), encoding);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

        isCacheable = ResourceUtils.canRequestedResourcesContentBeAssumedConstant(getParams());
        url = buildUrl(path, params);
        setFiletype(path);
    }

    /**
     * @param path path of HTTP request.
     * @return type of request, `js`, `css`, determined same as file type / extension.
     */
    public static String getType(String path) {
        List<String> parsed = parseWithRe(path, "\\.([^\\.]+)$");
        return parsed.size() > 0 ? parsed.get(0).toLowerCase() : null;
    }

    private void setFiletype(String path) {
        final String type = getType(path);
        if ("map".equals(type)) {
            this.sourcemap = true;
            List<String> matches = parseWithRe(path, "\\.([^\\.]+).map$");
            if (matches.size() > 0) {
                this.filetype = matches.get(0);
            }
        } else {
            this.sourcemap = false;
            this.filetype = type;
        }
    }

    /**
     * @return path of HTTP request, part of URL without query string.
     */
    public String getPath() {
        return path;
    }

    /**
     * @return HTTP params.
     */
    public Map<String, String> getParams() {
        return params;
    }

    public HttpServletRequest getOriginalRequest() {
        return originalRequest;
    }

    /**
     * @return type of request, `js`, `css`, determined same as file type / extension.
     */
    public String getType() {
        return filetype;
    }

    public boolean isSourceMap() {
        return sourcemap;
    }

    public String getContentType() {
        return globals.getConfig().getContentType(getPath());
    }

    /**
     * @return URL of HTTP request, with query string.
     */
    public String getUrl() {
        return url;
    }

    // Calculating downloadableResource.isResourceModified can be moderately expensive. Avoiding if possible.
    public boolean isCacheable() {
        return isCacheable;
    }

    public Globals getGlobals() {
        return globals;
    }

    public ServingType getServingType() {
        return servingType;
    }

    public void setServingType(ServingType servingType) {
        this.servingType = servingType;
    }

    public void setRequestedResources(RawRequest request) {
        // @todo: allow merging two RawRequest objects together so we can avoid potential NPEs.
        this.requested = request;
    }

    private Stream<Requestable> normalised(Collection<String> items) {
        return (items != null ? items : Collections.<String>emptyList())
                .stream()
                        .filter(StringUtils::isNotBlank)
                        .map(String::toLowerCase)
                        .map(key -> key.startsWith(Config.CONTEXT_PREFIX)
                                ? new WebResourceContextKey(key.substring(Config.CONTEXT_PREFIX.length()))
                                : new WebResourceKey(key));
    }

    public String getRelativeResourceName() {
        return resourceName;
    }

    public void setRelativeResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    /**
     * Build a string that represents the request. If two requests are logically equivalent,
     * it will return the same value. This is useful for caches.
     *
     * @return a hashed string representing the unique aspects of the request.
     */
    public String getRequestHash() {
        final ArrayList<String> key = new ArrayList<>();
        final String sortedParamsAsString = getParams().entrySet().stream()
                .map(e -> e.getKey() + "=" + e.getValue())
                .sorted()
                .collect(Collectors.joining(","));
        final LooselyTypedRequestExpander resources = new LooselyTypedRequestExpander(requested).normalise();

        // Currently minified and non-minified resources use same url, it would be better to use different urls.
        key.add("min=" + globals.getConfig().isMinificationEnabled());

        key.add("urltype=" + getServingType());
        key.add("included=" + String.join(",", resources.getIncluded()));
        key.add("excluded=" + String.join(",", resources.getExcluded()));
        key.add("resource=" + getRelativeResourceName());
        key.add("filetype=" + filetype);
        key.add("sourcemap=" + sourcemap);
        key.add("params={{" + sortedParamsAsString + "}}");

        return String.join("|", key);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return getRequestHash().equals(request.getRequestHash());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRequestHash());
    }
}
