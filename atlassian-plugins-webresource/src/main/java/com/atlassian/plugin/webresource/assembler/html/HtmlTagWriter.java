package com.atlassian.plugin.webresource.assembler.html;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.function.Predicate;
import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.plugin.webresource.assembler.ResourceUrls;
import com.atlassian.plugin.webresource.assembler.WebResourceInformation;
import com.atlassian.plugin.webresource.data.DataTagWriter;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.webresource.api.assembler.WebResource;
import com.atlassian.webresource.api.data.PluginDataResource;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

import static com.atlassian.plugin.webresource.impl.support.Support.LOGGER;

/**
 * Represents a {@link Writer} wrapper responsible for writing {@link WebResourceInformation#getResourceUrls()} as HTML tags, according to their extension.
 *
 * @since 5.0.0
 */
public abstract class HtmlTagWriter {
    private final Collection<HtmlTagFormatter> formatters;
    private final RequestState requestState;
    private final SwallowErrorsWriter swallowErrorsWriter;
    private final Writer writer;

    public HtmlTagWriter(
            @Nonnull final RequestState requestState,
            @Nonnull final Writer writer,
            @Nonnull final Collection<HtmlTagFormatter> formatters) {
        this.requestState =
                requireNonNull(requestState, "The request state is mandatory for the creation of HtmlTagWriter.");
        this.writer = requireNonNull(writer, "The writer is mandatory for the creation of HtmlTagWriter.");
        this.formatters = formatters;
        swallowErrorsWriter = new SwallowErrorsWriter(writer);
    }

    /**
     * Generates HTML import tags for each {@link WebResourceInformation#getResourceUrls()}
     * according to its URL extension type and write it into the {@link HtmlTagWriter#writer}.
     *
     * @param information     The information used to generate the import tags.
     * @param predicate       The predicate used to filter the resources based on the {@link WebResource} information.
     * @param legacyPredicate The predicate used to filter the resources based on the {@link ResourceUrl} information.
     */
    public void writeHtmlTag(
            @Nonnull final WebResourceInformation information,
            @Nonnull final Predicate<WebResource> predicate,
            @Nonnull final Predicate<ResourceUrl> legacyPredicate) {
        requireNonNull(information, "The information of the resources is mandatory to write html tags.");
        requireNonNull(
                predicate, "The predicate for the WebResource is mandatory for the filtering of tags to be written");
        requireNonNull(
                legacyPredicate,
                "The predicate for the ResourceUrl is mandatory for the filtering of tags to be written");

        writeResourceData(information, predicate);

        final Collection<ResourceUrls> resources = information.getResourceUrls().stream()
                .filter(container -> predicate.test(container.getPluginUrlResource()))
                .filter(container -> legacyPredicate.test(container.getResourceUrl()))
                .collect(toCollection(LinkedList::new));
        writeHtmlTag(resources);
    }

    /**
     * Generates HTML tags for each {@link ResourceUrls} according to its URL extension type and write it into the {@link HtmlTagWriter#writer}.
     *
     * @param resources The resources containing the URL information to be used to generate HTML import tag.
     */
    public void writeHtmlTag(@Nonnull final Collection<ResourceUrls> resources) {
        requireNonNull(resources, "The resource urls are mandatory for the creation the HTML tags.");

        for (final HtmlTagFormatter formatter : formatters) {
            for (final Iterator<ResourceUrls> iterator = resources.iterator(); iterator.hasNext(); ) {
                final ResourceUrls resource = iterator.next();
                if (formatter.matches(resource.getResourceUrl().getName())) {
                    writeHtmlTag(generateHtmlTag(resource, formatter));
                    iterator.remove();
                }
            }
        }
        resources.forEach(swallowErrorsWriter::write);
    }

    /**
     * Flushes an HTML string to the writer. Use when you need to optimise how HTML is rendered for
     * a collection of resources rather than a single one.
     *
     * @param html The HTML contents to output.
     */
    protected void writeHtmlTag(@Nonnull String... html) {
        swallowErrorsWriter.write(html);
    }

    /**
     * Generates an HTML tag applying an {@link HtmlTagFormatter} to a {@link ResourceUrls} resource url.
     *
     * @param resource  The resource containing the url to be used for the tag generation.
     * @param formatter The formatter which will be applied to the url.
     * @return The final HTML tag.
     */
    @Nonnull
    abstract String generateHtmlTag(@Nonnull final ResourceUrls resource, @Nonnull final HtmlTagFormatter formatter);

    private void writeResourceData(final WebResourceInformation information, final Predicate<WebResource> predicate) {
        try {
            final Collection<PluginDataResource> resourcesData =
                    information.getData().stream().filter(predicate).collect(toList());
            new DataTagWriter().write(writer, resourcesData);
        } catch (final IOException exception) {
            LOGGER.error("IOException encountered rendering data tags", exception);
        }
    }
}
