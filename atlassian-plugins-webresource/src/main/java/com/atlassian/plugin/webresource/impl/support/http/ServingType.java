package com.atlassian.plugin.webresource.impl.support.http;

/**
 * Represents the various unique modes of serving content at a URL.
 * <p>
 * These should have a 1:1 correspondence to the URL paths the WRM responds to,
 * with the exception of source maps, which re-use the same serving type as their base URL.
 */
public enum ServingType {
    /**
     * The "nothing was set" content type.
     */
    UNKNOWN,
    /**
     * Serve a single tangible {@link com.atlassian.plugin.webresource.impl.snapshot.resource.Resource} at this URL.
     * The resource lives in the {@link com.atlassian.plugin.webresource.impl.snapshot.WebResource} referenced in
     * this request.
     */
    RESOURCES_SINGLE,
    /**
     * Serve the raw, unmodified source for a single tangible
     * {@link com.atlassian.plugin.webresource.impl.snapshot.resource.Resource} at this URL.
     * The resource lives in the {@link com.atlassian.plugin.webresource.impl.snapshot.WebResource} referenced in
     * this request.
     */
    SOURCES_SINGLE,
    /**
     * Serve an ordered collection of {@link com.atlassian.plugin.webresource.impl.snapshot.resource.Resource} content
     * from a single {@link com.atlassian.plugin.webresource.impl.snapshot.Bundle} at this URL.
     */
    BATCH,
    /**
     * Serve the content of a single tangible {@link com.atlassian.plugin.webresource.impl.snapshot.resource.Resource}
     * at this URL.
     * The resource lives in the single {@link com.atlassian.plugin.webresource.impl.snapshot.Bundle} referenced
     * in this request.
     */
    BATCH_RESOURCE,
    /**
     * Serve an ordered collection of {@link com.atlassian.plugin.webresource.impl.snapshot.resource.Resource} content
     * at this URL, for every {@link com.atlassian.plugin.webresource.impl.snapshot.Bundle} included by at least one of
     * the {@link com.atlassian.plugin.webresource.impl.snapshot.Context} referenced in this request.
     */
    CONTEXTBATCH,
    /**
     * Serve a single tangible {@link com.atlassian.plugin.webresource.impl.snapshot.resource.Resource} at this URL.
     * The resource lives in one of the {@link com.atlassian.plugin.webresource.impl.snapshot.Bundle}s included
     * by at least one of the {@link com.atlassian.plugin.webresource.impl.snapshot.Context} referenced in this request.
     */
    CONTEXTBATCH_RESOURCE,
}
