package com.atlassian.plugin.webresource.impl.support.factory;

import javax.annotation.Nonnull;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.impl.support.InitialContent;

import static java.util.Objects.requireNonNull;

/**
 * Factory used to build a {@link InitialContent}.
 *
 * @since 5.0.0
 */
public class InitialContentFactory {
    private final Globals globals;
    private final Router router;

    public InitialContentFactory(@Nonnull final Globals globals) {
        this.globals = requireNonNull(
                globals, "The globals information is mandatory for the creation of a initial content factory.");
        this.router = requireNonNull(
                globals.getRouter(),
                "The router information is mandatory for the creation of a initial content factory.");
    }

    /**
     * Look up for a certain {@link InitialContent} based on a {@link Resource}.
     *
     * @param resource The resource used as base for the creation of the initial content.
     * @return The found initial content.
     */
    @NonNull
    public InitialContent lookup(@NonNull final Resource resource) {
        final InitialContent minifiedContent = InitialMinifiedContent.builder(globals)
                .withContent(resource)
                .withPath(resource)
                .withSourceMap(resource)
                .build();
        if (minifiedContent.getContent().isPresent()) {
            return minifiedContent;
        }
        return InitialSourceContent.builder()
                .withContent(resource)
                .withPath(router, resource)
                .withSourceMap(resource)
                .build();
    }
}
