package com.atlassian.plugin.webresource.models;

import static com.atlassian.plugin.webresource.impl.config.Config.SYNCBATCH_CONTEXT_KEY;

/**
 * Represents the special synchronous or "sync batch" web-resource context.
 * Everything in this context should be rendered to the body of the current HTTP response.
 *
 * @since 5.6.0
 */
public class SyncBatchKey extends WebResourceContextKey {

    private static final SyncBatchKey KEY = new SyncBatchKey();

    private SyncBatchKey() {
        super(SYNCBATCH_CONTEXT_KEY);
    }

    public static SyncBatchKey getInstance() {
        return KEY;
    }

    @Override
    public String toString() {
        return "<wrc!" + SYNCBATCH_CONTEXT_KEY + ">";
    }
}
