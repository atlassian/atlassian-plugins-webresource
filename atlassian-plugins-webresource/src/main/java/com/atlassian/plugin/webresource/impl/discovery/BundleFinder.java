package com.atlassian.plugin.webresource.impl.discovery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Predicates;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;

import static com.google.common.base.Predicates.alwaysFalse;
import static com.google.common.base.Predicates.alwaysTrue;
import static java.util.Optional.ofNullable;

import static com.atlassian.plugin.webresource.impl.support.Support.efficientAndPredicate;

/**
 * Query builder for retrieving ordered lists of web-resource keys based on what the user is requesting
 * and what has previously been requested.
 */
public class BundleFinder {
    private final Snapshot snapshot;

    /**
     * Unique keys to include in the query.
     * The order they are added may be important to the order that results are returned in.
     */
    private final LinkedHashSet<String> included;

    private final List<Predicate<Bundle>> deepFilters;
    private PredicateFailStrategy deepFilterFailStrategy = PredicateFailStrategy.STOP;
    /**
     * Unique keys to ignore if found during the query.
     */
    private final Set<String> excluded;

    private final List<Predicate<Bundle>> shallowFilters;

    private Predicate<Bundle> conditionsForExcluded;
    private boolean deep;
    private boolean resolveExcluded;

    public BundleFinder(final Snapshot snapshot) {
        deep = true;
        deepFilters = new ArrayList<>();
        excluded = new HashSet<>();
        included = new LinkedHashSet<>();
        resolveExcluded = true;
        this.snapshot = snapshot;
        shallowFilters = new ArrayList<>();
    }

    public BundleFinder included(final Collection<String> keys) {
        included.clear();
        included.addAll(keys);
        return this;
    }

    public BundleFinder excluded(
            @Nonnull final Collection<String> keys, @Nullable final Predicate<Bundle> conditionsForExcluded) {
        excluded.clear();
        excluded.addAll(keys);
        this.conditionsForExcluded = ofNullable(conditionsForExcluded).orElseGet(Predicates::alwaysTrue);
        resolveExcluded = true;
        return this;
    }

    public BundleFinder excludedResolved(final Collection<String> keys) {
        excluded.clear();
        excluded.addAll(keys);
        conditionsForExcluded = alwaysTrue();
        resolveExcluded = false;
        return this;
    }

    /**
     * If it should resolve full tree of dependencies recursively, `true` by default.
     */
    public BundleFinder deep(final boolean deep) {
        this.deep = deep;
        return this;
    }

    /**
     * Remove element and all its dependencies if filter fail.
     */
    public BundleFinder deepFilter(final Predicate<Bundle> filter) {
        deepFilters.add(filter);
        return this;
    }

    /**
     * What the walker should do when a deep filter fails.
     * @param failStrategy
     * @return
     */
    public BundleFinder onDeepFilterFail(PredicateFailStrategy failStrategy) {
        this.deepFilterFailStrategy = failStrategy;
        return this;
    }

    public Found endAndGetResult() {
        final Predicate<Bundle> deepPredicate = deep ? alwaysTrue() : alwaysFalse();
        return new BundleWalker(snapshot)
                .find(
                        included,
                        excluded,
                        conditionsForExcluded,
                        resolveExcluded,
                        deepPredicate,
                        efficientAndPredicate(deepFilters),
                        deepFilterFailStrategy,
                        efficientAndPredicate(shallowFilters));
    }

    /**
     * Run query and get found Bundles.
     */
    public List<String> end() {
        return endAndGetResult().getFound();
    }
}
