package com.atlassian.plugin.webresource.assembler;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResourceParams;

/**
 * @since v3.0
 */
abstract class DefaultPluginUrlResourceParams implements PluginUrlResourceParams {
    protected final Map<String, String> params;

    public DefaultPluginUrlResourceParams(Map<String, String> params, String key, BatchType batchType) {
        this.params = new LinkedHashMap<>(params);
        this.params.put(Config.WRM_KEY_PARAM_NAME, key);
        this.params.put(Config.WRM_BATCH_TYPE_PARAM_NAME, batchType.name().toLowerCase());
    }

    @Override
    public Map<String, String> all() {
        return Collections.unmodifiableMap(params);
    }
}
