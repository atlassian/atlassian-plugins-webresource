package com.atlassian.plugin.webresource;

import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;

import static java.util.Arrays.asList;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

import static com.atlassian.plugin.webresource.impl.config.Config.ASYNC_SCRIPT_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.config.Config.DEFER_SCRIPT_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.config.Config.INITIAL_RENDERED_SCRIPT_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.config.Config.JS_EXTENSION;
import static com.atlassian.plugin.webresource.impl.config.Config.WRM_BATCH_TYPE_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.config.Config.WRM_KEY_PARAM_NAME;

public class JavascriptWebResource extends AbstractWebResourceFormatter {
    private static final String ATTRIBUTE_JOINER = " ";

    private static final List<String> HANDLED_PARAMETERS = asList(
            "charset",
            WRM_KEY_PARAM_NAME,
            WRM_BATCH_TYPE_PARAM_NAME,
            INITIAL_RENDERED_SCRIPT_PARAM_NAME,
            ASYNC_SCRIPT_PARAM_NAME,
            DEFER_SCRIPT_PARAM_NAME);
    private static final String JAVASCRIPT_TAG_BEGIN = "<script src=\"";
    private static final String JAVASCRIPT_TAG_EMPTY = "";
    private static final String JAVASCRIPT_TAG_END = "></script>\n";

    public JavascriptWebResource() {}

    @Override
    public boolean matches(@Nullable final String name) {
        return ofNullable(name).filter(value -> name.endsWith(JS_EXTENSION)).isPresent();
    }

    @Override
    public String formatResource(final String url, final Map<String, String> attributes) {
        if (isValid(attributes)) {
            final StringBuilder scriptTagBuilder = new StringBuilder(JAVASCRIPT_TAG_BEGIN)
                    .append(escapeHtml4(url))
                    .append("\" ");
            return scriptTagBuilder
                    .append(StringUtils.join(
                            getParametersAsAttributes(attributes).iterator(), ATTRIBUTE_JOINER))
                    .append(JAVASCRIPT_TAG_END)
                    .toString();
        }
        return JAVASCRIPT_TAG_EMPTY;
    }

    @Override
    protected List<String> getAttributeParameters() {
        return HANDLED_PARAMETERS;
    }

    @Override
    boolean isValid(Map<String, String> attributes) {
        // A note to future developers: if both `async` and `defer` are in attributes, it is valid!
        // The w3c spec explicitly states that async wins when both are present.
        // The intent is to allow older browsers to fall back to a defer behaviour if async is unsupported.
        // https://www.w3.org/TR/2011/WD-html5-author-20110809/the-script-element.html#attr-script-async
        // We might choose to inform developers if they are adding both, but it's not actually "wrong".
        return super.isValid(attributes);
    }
}
