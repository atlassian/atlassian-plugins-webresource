package com.atlassian.plugin.webresource.condition;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.loaders.LoaderUtils;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.baseconditions.AbstractConditionElementParser;
import com.atlassian.plugin.web.baseconditions.CompositeCondition;
import com.atlassian.plugin.web.conditions.ConditionLoadingException;
import com.atlassian.plugin.webresource.util.PluginClassLoader;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

/**
 * Implementation of AbstractConditionElementParser for UrlReadingCondition.
 * <p>
 * The "class" attribute may represent a {@link UrlReadingCondition} or a legacy {@link Condition}. We offered "class2"
 * for migration purposes, to avoid changing behaviour we still check that first. However, if "class" is provided and
 * it doesn't implement {@link UrlReadingCondition} we refuse to load the module to bring this to the engineer's
 * attention
 * </p>
 *
 * @since v3.0
 */
public class UrlReadingConditionElementParser extends AbstractConditionElementParser<DecoratingCondition> {
    private final HostContainer hostContainer;

    public UrlReadingConditionElementParser(HostContainer hostContainer) {
        this.hostContainer = hostContainer;
    }

    @Override
    protected DecoratingCondition makeConditionImplementation(Plugin plugin, Element element)
            throws PluginParseException {
        try {
            String overrideConditionClassName = element.attributeValue("class2");
            String conditionClassName =
                    null != overrideConditionClassName ? overrideConditionClassName : element.attributeValue("class");
            if (conditionClassName == null) {
                throw new PluginParseException("Condition element must specify a class attribute");
            }
            Map<String, String> params = LoaderUtils.getParams(element);
            return create(plugin, conditionClassName, params);
        } catch (final ClassCastException e) {
            throw new PluginParseException(
                    "Configured condition class does not implement the UrlReadingCondition interface", e);
        } catch (final ConditionLoadingException cle) {
            throw new PluginParseException("Unable to load the module's display conditions: " + cle.getMessage(), cle);
        }
    }

    private DecoratingCondition create(Plugin plugin, String className, Map<String, String> params)
            throws ConditionLoadingException {
        Object o = createObject(plugin, className);
        if (o instanceof UrlReadingCondition) {
            UrlReadingCondition urlReadingCondition = (UrlReadingCondition) o;
            // Protecting against condition being able to change params.
            HashMap<String, String> paramsCopy = new HashMap<>(params);
            urlReadingCondition.init(params);
            return new DecoratingUrlReadingCondition(urlReadingCondition, paramsCopy, plugin.getKey(), className);
        } else {
            throw new ConditionLoadingException(
                    "Cannot load condition class: " + className
                            + ". Only classes extended com.atlassian.webresource.spi.condition.UrlReadingCondition are supported."
                            + " Please migrate your conditions to the com.atlassian.webresource.spi.condition.UrlReadingCondition class."
                            + " https://developer.atlassian.com/server/framework/atlassian-sdk/stateless-web-resource-transforms-and-conditions");
        }
    }

    private <T> T createObject(Plugin plugin, String className) throws ConditionLoadingException {
        try {
            return PluginClassLoader.create(plugin, this.getClass(), hostContainer, className);
        } catch (ClassNotFoundException e) {
            throw new ConditionLoadingException("Cannot load condition class: " + className, e);
        }
    }

    @Override
    protected DecoratingCondition invert(DecoratingCondition condition) {
        return condition.invertCondition();
    }

    @Override
    protected CompositeCondition<DecoratingCondition> createAndCompositeCondition() {
        return new DecoratingAndCompositeCondition();
    }

    @Override
    protected CompositeCondition<DecoratingCondition> createOrCompositeCondition() {
        return new DecoratingOrCompositeCondition();
    }
}
