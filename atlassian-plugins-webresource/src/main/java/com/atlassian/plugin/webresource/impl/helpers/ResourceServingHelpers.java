package com.atlassian.plugin.webresource.impl.helpers;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.annotators.ResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.WebResource;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.impl.support.LineCountingProxyOutputStream;
import com.atlassian.plugin.webresource.impl.support.SourceMapJoinerStub;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.impl.support.factory.InitialContentFactory;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.sourcemap.ReadableSourceMap;
import com.atlassian.sourcemap.SourceMapJoiner;
import com.atlassian.sourcemap.Util;
import com.atlassian.webresource.api.assembler.resource.CompleteWebResourceKey;
import com.atlassian.webresource.spi.transformer.TwoPhaseResourceTransformer;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.stream.Collectors.toMap;

import static com.atlassian.plugin.webresource.impl.support.Support.logIOException;
import static com.atlassian.plugin.webresource.impl.support.http.BaseRouter.buildUrl;
import static com.atlassian.plugin.webresource.util.WebResourceKeyHelper.createWebResourceKey;
import static com.atlassian.sourcemap.WritableSourceMap.toReadableSourceMap;

/**
 * Stateless helper functions providing support for serving resources.
 *
 * @since v6.3
 */
public class ResourceServingHelpers extends UrlGenerationHelpers {
    /**
     * Get resource for Web Resource or Plugin, also resolves relative paths.
     *
     * @param completeKey  key of web resource or plugin.
     * @param resourceName name of Resource.
     */
    public static Resource getResource(RequestCache requestCache, String completeKey, String resourceName) {
        Resource resource = getWebResourceResource(requestCache, completeKey, resourceName);
        if (resource == null) {
            resource = getResourceRelativeToWebResource(requestCache, completeKey, resourceName);
        }
        if (resource == null) {
            resource = getFromOSGiPluginModuleResource(requestCache.getGlobals(), completeKey, resourceName);
        }
        if (resource == null) {
            resource = getPluginResource(requestCache.getGlobals(), completeKey, resourceName);
        }
        if (resource == null) {
            resource = getResourceRelativeToPlugin(requestCache.getGlobals(), completeKey, resourceName);
        }
        return resource;
    }

    /**
     * Get Resource for one of Web Resources.
     *
     * @param bundles      list of keys of Web Resources.
     * @param resourceName name of Resource.
     */
    public static Resource getResource(RequestCache requestCache, Collection<String> bundles, String resourceName) {
        Resource resource = null;
        for (String key : bundles) {
            if ((resource = getResource(requestCache, key, resourceName)) != null) {
                break;
            }
        }
        return resource;
    }

    /**
     * Apply conditions and transformations to list of Resources and generate resulting Batch.
     *
     * @param requiredResources only used by ModuleAnnotator#after and should be removed.
     *                          see Controller#requiredResources for more details.
     * @param resourcesSupplier list of Resources supplier
     * @param params            http params.
     * @return resulting content.
     */
    public static Content transform(
            final Globals globals,
            LinkedHashSet<String> requiredResources,
            final String url,
            final String type,
            final Supplier<Collection<Resource>> resourcesSupplier,
            final Map<String, String> params) {
        final Collection<Resource> resources = resourcesSupplier.get();

        return new ContentImpl(null, true) {
            @Override
            public boolean isPresent() {
                return !resources.isEmpty();
            }

            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                ResourceContentAnnotator annotator = globals.getConfig().getContentAnnotator(type);
                try {
                    SourceMapJoiner sourceMapJoiner =
                            isSourceMapEnabled ? new SourceMapJoiner() : new SourceMapJoinerStub();

                    boolean isFirst = true;

                    for (Resource resource : resources) {

                        if (!isFirst) {
                            // Resources should be delimited by new line, it's needed for source map to work properly.
                            out.write('\n');
                        }

                        // Byte counting proxy needed to calculate the length of the resource,
                        // needed for the source map generation.
                        LineCountingProxyOutputStream lineCountingStream = new LineCountingProxyOutputStream(out);
                        OutputStream outOrLineCountingStream = isSourceMapEnabled ? lineCountingStream : out;

                        int offset = 0;
                        if (isFirst) {
                            // Adding before all annotators.
                            offset += annotator.beforeAllResourcesInBatch(
                                    requiredResources, url, params, outOrLineCountingStream);
                        }

                        // Adding before annotators.
                        offset += annotator.beforeResourceInBatch(
                                requiredResources, resource, params, outOrLineCountingStream);

                        Content content = transformSafely(globals, requiredResources, url, resource, params, false);
                        ReadableSourceMap sourceMap = content.writeTo(outOrLineCountingStream, isSourceMapEnabled);

                        int resourceLength = lineCountingStream.getLinesCount() - offset;

                        // Apply the after annotators in reverse order
                        annotator.afterResourceInBatch(requiredResources, resource, params, outOrLineCountingStream);

                        // If there's no source map generated by transformers,
                        // the 1to1 source map should be generated because
                        // the source map needed to generate the batch source map.
                        // And, instead of the source url the url of transformed single resource should be used (because
                        // transformers without source map support may be already applied).
                        if (isSourceMapEnabled && sourceMap == null) {
                            String singleResourceUrl = globals.getRouter().resourceUrlWithoutHash(resource, params);
                            sourceMap =
                                    toReadableSourceMap(Util.create1to1SourceMap(resourceLength, singleResourceUrl));
                        }

                        // Adding the resource source map to the batch source map.
                        sourceMapJoiner.add(sourceMap, lineCountingStream.getLinesCount(), offset);

                        if (isFirst) {
                            isFirst = false;
                        }
                    }

                    // Adding after all annotators.
                    annotator.afterAllResourcesInBatch(requiredResources, url, params, out);

                    return toReadableSourceMap(sourceMapJoiner.join());
                } catch (final IOException e) {
                    logIOException(e);
                    return null;
                }
            }
        };
    }

    /**
     * Transform given Resource by applying Transformers and Static Transformers.
     *
     * @param resource the resource.
     * @param params   http params.
     * @return resulting content.
     */
    public static Content transform(
            final Globals globals,
            LinkedHashSet<String> requiredResources,
            String url,
            final Resource resource,
            final Map<String, String> params,
            boolean applyAnnotators) {
        final Content content =
                transformWithoutCache(globals, requiredResources, url, resource, params, applyAnnotators);
        // It is possible to cache in case of enabled source map too, but it could be costly because the source map
        // needs to be serialized and de-serialized.
        if (globals.getConfig().isSourceMapEnabledFor(resource.getNameOrLocationType())) {
            return content;
        } else {
            return new ContentImpl(content.getContentType(), content.isTransformed()) {
                @Override
                public ReadableSourceMap writeTo(final OutputStream out, final boolean isSourceMapEnabled) {
                    if (isSourceMapEnabled) {
                        Support.LOGGER.warn(
                                "internal error, source map could not be used with incremental transformer cache!");
                    }

                    String key = buildKey(globals, resource, params);
                    globals.getTemporaryIncrementalCache()
                            .cache("transformer", key, out, out1 -> content.writeTo(out1, isSourceMapEnabled));
                    return null;
                }
            };
        }
    }

    /**
     * The `params` contains all the params from url, so if we build the key using all params it will
     * be correct but inefficient for the caching. It's inefficient because not all params are used by transformers for
     * the given resource.
     * <p>
     * If we can detect the subset of params that's actually used by transformers
     * fot the given resource we would be able to provide better cache hit ratio.
     * This method calculates the key in the smart way, using only those parameters that are actually used.
     */
    public static String buildKey(Globals globals, Resource resource, Map<String, String> params) {
        /*
         * The base key needs to account for the resources' path, so it will account for indirect resource
         * references like in {@link #getWebResourceResource(RequestCache, String, String)} or
         * {@link #getResourceRelativeToWebResource(RequestCache, String, String)}}... typically these are
         * called to handle relative image paths, which will be embedded in CSS files, or called in older
         * template files retrieving static URLs for resources.
         *
         * @see {@link com.atlassian.plugin.webresource.impl.http.Controller#serveResourceRelativeToBatch(Collection, LinkedHashSet, String, boolean, boolean)}
         * @see {@link com.atlassian.webresource.api.WebResourceUrlProvider#getStaticPluginResourceUrl(String, String, UrlMode)}
         */
        final String baseKey = resource.getKey() + ":" + resource.getPath();

        // Check if there's anything about this resource that might alter its content, such as transforms
        if (resource.getParent() instanceof WebResource webResource) {
            // Calculating parameters used by transformers.
            Set<String> usedParameterKeys =
                    new HashSet<>(globals.getConfig().getStaticTransformers().getParamKeys());

            var resourceTransformers = webResource.getTransformers();
            if (resourceTransformers != null) {
                if (!resourceTransformers.addAllUsedQueryParameters(
                        usedParameterKeys,
                        resource.getLocationType(),
                        globals.getConfig().getTransformerCache())) {
                    // Use all query parameters in case there's a transformer that does not provide it's potentially
                    // used query keys
                    // This is a path to be deprecated at some point
                    return buildUrl(baseKey, params);
                }
            }

            Map<String, String> usedParameters =
                    usedParameterKeys.stream().filter(params::containsKey).collect(toMap(key -> key, params::get));

            // Creating key using only params actually used by transformer.
            return buildUrl(baseKey, usedParameters);
        } else {
            // If it's not a web resource there's no transformers and it's always the same.
            return baseKey;
        }
    }

    /**
     * Transform given Resource by applying Transformers and Static Transformers.
     *
     * @param resource the resource.
     * @param params   http params.
     * @return resulting content.
     */
    public static Content transformWithoutCache(
            final Globals globals,
            LinkedHashSet<String> requiredResources,
            final String url,
            final Resource resource,
            final Map<String, String> params,
            boolean applyAnnotators) {
        checkArgument(!resource.isRedirect(), "can't transform redirect resource!");

        Content content = resource.getContent();
        if (!resource.isTransformable()) {
            return content;
        }

        content = new InitialContentFactory(globals).lookup(resource).toContent(content);

        String resourceAsSourceUrl = globals.getRouter().sourceUrl(resource);
        content = applyTransformers(globals, resource, content, params, resourceAsSourceUrl);
        content = applyStaticTransformers(globals, resource, content, params, resourceAsSourceUrl);

        if (applyAnnotators) {
            // Applying annotators.
            final Content immutableContent = content;
            return new ContentImpl(content.getContentType(), true) {
                @Override
                public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                    ResourceContentAnnotator annotator =
                            globals.getConfig().getContentAnnotator(resource.getNameOrLocationType());
                    try {
                        // Adding before all annotators.
                        int offset = annotator.beforeResource(requiredResources, url, resource, params, out);

                        // Updating source map if exist.
                        ReadableSourceMap sourceMap = immutableContent.writeTo(out, isSourceMapEnabled);
                        if (isSourceMapEnabled && sourceMap != null) {
                            sourceMap.addOffset(offset);
                        }

                        // Adding after all annotators.
                        annotator.afterResource(requiredResources, url, resource, params, out);

                        return sourceMap;
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            };
        } else {
            return content;
        }
    }

    /**
     * Transform given Resource by applying Transformers and Static Transformers.
     * In case of any error it will be intercepted and empty content returned.
     *
     * @param resource the resource.
     * @param params   http params.
     * @return resulting content.
     */
    public static Content transformSafely(
            Globals globals,
            LinkedHashSet<String> requiredResources,
            String url,
            Resource resource,
            Map<String, String> params,
            boolean applyAnnotators) {
        Content content;
        try {
            content = transform(globals, requiredResources, url, resource, params, applyAnnotators);
        } catch (RuntimeException e) {
            Support.LOGGER.warn("can't transform resource " + resource.getKey() + ":" + resource.getName(), e);
            content = buildEmptyContent(null);
        }
        return buildSafeContent(content, resource.getFullName());
    }

    /**
     * Select Resources that should be included in batch.
     *
     * @param type   type of batch.
     * @param params http params of batch.
     * @return list of selected Resources.
     */
    public static Predicate<Resource> shouldBeIncludedInBatch(final String type, final Map<String, String> params) {
        return resource -> type.equals(resource.getNameOrLocationType()) && resource.isBatchable(params);
    }

    /**
     * Adapter, turns Content into DownloadableResource.
     */
    public static DownloadableResource asDownloadableResource(final Content content) {
        return new DownloadableResource() {
            @Override
            public boolean isResourceModified(HttpServletRequest request, HttpServletResponse response) {
                throw new RuntimeException("not supported for content wrapper!");
            }

            @Override
            public void serveResource(HttpServletRequest request, HttpServletResponse response) {
                throw new RuntimeException("not supported for content wrapper!");
            }

            @Override
            public void streamResource(OutputStream out) {
                content.writeTo(out, false);
            }

            @Override
            public String getContentType() {
                return content.getContentType();
            }
        };
    }

    /**
     * Adapter, turns DownloadableResource into Content.
     */
    public static Content asContent(
            final DownloadableResource downloadableResource, final ReadableSourceMap sourceMap, boolean isTransformed) {
        return new ContentImpl(downloadableResource.getContentType(), isTransformed) {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                try {
                    downloadableResource.streamResource(out);
                } catch (DownloadException e) {
                    Support.LOGGER.debug("Error while serving file: DownloadException");
                }
                return sourceMap;
            }
        };
    }

    public static Content buildEmptyContent(String contentType) {
        return new ContentImpl(contentType, false) {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                return null;
            }
        };
    }

    /**
     * Intercept any exception and ignores it.
     */
    public static Content buildSafeContent(final Content content, final String fullResourceName) {
        return new Content() {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                try {
                    return content.writeTo(out, isSourceMapEnabled);
                } catch (RuntimeException e) {
                    Support.LOGGER.warn("error in `Content.writeTo` for " + fullResourceName, e);
                    return null;
                }
            }

            @Override
            public String getContentType() {
                try {
                    return content.getContentType();
                } catch (RuntimeException e) {
                    Support.LOGGER.warn("error in `Content.getContentType` for " + fullResourceName, e);
                    return null;
                }
            }

            @Override
            public boolean isTransformed() {
                try {
                    return content.isTransformed();
                } catch (RuntimeException e) {
                    Support.LOGGER.warn("error in `Content.isTransformed` for " + fullResourceName, e);
                    return false;
                }
            }
        };
    }

    /**
     * Apply Transformers to Resource.
     *
     * @param resource  resource.
     * @param content   content of resource.
     * @param params    http params.
     * @param sourceUrl url of source code for resource.
     * @return resulting content.
     */
    protected static Content applyTransformers(
            final Globals globals,
            final Resource resource,
            final Content content,
            final Map<String, String> params,
            final String sourceUrl) {
        if (resource.getParent() instanceof WebResource webResource) {
            CachedTransformers transformers = webResource.getTransformers();
            if (transformers == null) {
                return content;
            }

            Content lastContent = content;
            for (WebResourceTransformation transformation : transformers.getTransformations()) {
                lastContent = repairSourceMapChain(lastContent, innerLastContent -> {
                    // There's another version of `matches` that matches against extension,
                    // but it can't be used because sometimes
                    // transformers are matched against things like "some-name.public.js".matches("public.js") so if it
                    // would be
                    // changed to matching the extension it wouldn't work.
                    if (transformation.matches(resource.getResourceLocation())) {
                        if (transformation instanceof TwoPhaseResourceTransformer) {
                            ((TwoPhaseResourceTransformer) transformation)
                                    .loadTwoPhaseProperties(resource.getResourceLocation(), resource::getStreamFor);
                        }
                        innerLastContent = transformation.transform(
                                globals.getConfig().getCdnResourceUrlTransformer(),
                                globals.getConfig().getTransformerCache(),
                                resource,
                                innerLastContent,
                                resource.getResourceLocation(),
                                params::get,
                                sourceUrl);
                    }
                    return innerLastContent;
                });
            }
            return lastContent;
        } else {
            return content;
        }
    }

    /**
     * Some transformers doesn't support source map, the result is that the source map chain is broken and
     * not working.
     * <p>
     * We can try to fix it, if some transformer doesn't support source map - instead of breaking source map
     * chain we'll use the previous source map.
     */
    protected static Content repairSourceMapChain(Content input, RepairSourceMapChainCallback cb) {
        // Storing the first source map.
        final boolean[] isInputSourceMapEnabled = new boolean[1];
        final ReadableSourceMap[] inputSourceMap = new ReadableSourceMap[1];
        ContentImpl wrapper = new ContentImpl(input.getContentType(), input.isTransformed()) {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                inputSourceMap[0] = input.writeTo(out, isInputSourceMapEnabled[0]);
                return inputSourceMap[0];
            }
        };

        Content transformed = cb.apply(wrapper);

        // If transformer doesn't support source map using the first source map.
        return new ContentImpl(transformed.getContentType(), transformed.isTransformed()) {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                isInputSourceMapEnabled[0] = isSourceMapEnabled;
                ReadableSourceMap sourceMap = transformed.writeTo(out, isSourceMapEnabled);
                // Hack to fix some transformers not supporting source map. In such case we use the previous
                // source map and hoping for the best.
                return sourceMap != null ? sourceMap : inputSourceMap[0];
            }
        };
    }

    /**
     * Apply Static Transformers to Resource.
     *
     * @param resource  resource.
     * @param content   content of resource.
     * @param params    http params.
     * @param sourceUrl url of source code for resource.
     * @return resulting content.
     */
    protected static Content applyStaticTransformers(
            final Globals globals, Resource resource, Content content, Map<String, String> params, String sourceUrl) {
        if (resource.getParent() instanceof WebResource) {
            return repairSourceMapChain(content, innerContent -> {
                StaticTransformers staticTransformers = globals.getConfig().getStaticTransformers();
                staticTransformers.loadTwoPhaseProperties(resource.getResourceLocation(), resource::getStreamFor);
                return staticTransformers.transform(
                        innerContent,
                        resource.getParent().getTransformerParameters(),
                        resource.getResourceLocation(),
                        params::get,
                        sourceUrl);
            });
        } else {
            return content;
        }
    }

    /**
     * Get Resource for Web Resource.
     *
     * @param completeKey  complete key of Web Resource.
     * @param resourceName name of Resource.
     */
    protected static Resource getWebResourceResource(
            RequestCache requestCache, String completeKey, String resourceName) {
        Bundle bundle = requestCache.getSnapshot().get(completeKey);
        if (bundle == null) {
            return null;
        }
        return bundle.getResources(requestCache).get(resourceName);
    }

    /**
     * Get Resource from an OSGi plugin module.
     *
     * @param completeKeyOrPluginKey complete key or plugin key.
     * @param resourceName           name of Resource.
     */
    public static Resource getFromOSGiPluginModuleResource(
            Globals globals, String completeKeyOrPluginKey, String resourceName) {
        return globals.getConfig().getModuleResource(completeKeyOrPluginKey, resourceName);
    }

    /**
     * Get Resource relative to Web Resource.
     *
     * @deprecated since v3.3.2
     */
    @Deprecated
    protected static Resource getResourceRelativeToWebResource(
            RequestCache requestCache, String completeKey, String resourceName) {
        Bundle bundle = requestCache.getSnapshot().get(completeKey);
        if (bundle == null) {
            return null;
        }
        String filePath = "";
        Resource resource = null;
        while (resource == null) {
            String[] parts = splitLastPathPart(resourceName);
            if (parts == null) {
                return null;
            }
            resourceName = parts[0];
            filePath = parts[1] + filePath;
            resource = bundle.getResources(requestCache).get(resourceName);
        }

        final String finalFilePath = filePath;
        return requestCache
                .getGlobals()
                .getConfig()
                .getResourceFactory()
                .createResourceWithRelativePath(
                        resource.getParent(),
                        resource.getResourceLocation(),
                        resource.getNameType(),
                        resource.getLocationType(),
                        finalFilePath);
    }

    /**
     * Get Resource for Plugin.
     *
     * @param completeKeyOrPluginKey complete key or plugin key.
     * @param resourceName           name of Resource.
     */
    protected static Resource getPluginResource(Globals globals, String completeKeyOrPluginKey, String resourceName) {
        return globals.getConfig().getPluginResource(getPluginKey(completeKeyOrPluginKey), resourceName);
    }

    /**
     * Get Resource relative to Plugin.
     *
     * @deprecated since v3.3.2
     */
    @Deprecated
    protected static Resource getResourceRelativeToPlugin(
            Globals globals, String completeKeyOrPluginKey, String resourceName) {
        String pluginKey = getPluginKey(completeKeyOrPluginKey);
        String filePath = "";
        Resource resource = null;
        while (resource == null) {
            String[] parts = splitLastPathPart(resourceName);
            if (parts == null) {
                return null;
            }
            resourceName = parts[0];
            filePath = parts[1] + filePath;
            resource = globals.getConfig().getPluginResource(pluginKey, resourceName);
        }
        final String finalFilePath = filePath;
        return globals.getConfig()
                .getResourceFactory()
                .createResourceWithRelativePath(
                        resource.getParent(),
                        resource.getResourceLocation(),
                        resource.getNameType(),
                        resource.getLocationType(),
                        finalFilePath);
    }

    /**
     * Split path into parent folder and name.
     *
     * @deprecated since v3.3.2
     */
    @Deprecated
    public static String[] splitLastPathPart(String resourcePath) {
        int indexOfSlash = resourcePath.lastIndexOf('/');
        // skip over the trailing slash
        if (resourcePath.endsWith("/")) {
            indexOfSlash = resourcePath.lastIndexOf('/', indexOfSlash - 1);
        }
        if (indexOfSlash < 0) {
            return null;
        }
        return new String[] {resourcePath.substring(0, indexOfSlash + 1), resourcePath.substring(indexOfSlash + 1)};
    }

    /**
     * In case of Plugin Key - returns the same key, in case of Web Resource key - extracts Plugin key from it.
     */
    protected static String getPluginKey(String completeKeyOrPluginKey) {
        Optional<CompleteWebResourceKey> key = createWebResourceKey(completeKeyOrPluginKey);
        return key.isPresent() ? key.get().getPluginKey() : completeKeyOrPluginKey;
    }

    protected interface RepairSourceMapChainCallback {
        Content apply(Content content);
    }
}
