package com.atlassian.plugin.webresource.impl.snapshot;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.webresource.api.transformer.TransformerParameters;

/**
 * Web Resource.
 *
 * @since 3.3
 */
public class Bundle {
    private final String key;
    private final String version;
    private final boolean isTransformable;
    private final List<String> dependencies;
    private final Date updatedAt;
    protected Snapshot snapshot;

    public Bundle(
            Snapshot snapshot,
            String key,
            List<String> dependencies,
            Date updatedAt,
            String version,
            boolean isTransformable) {
        this.snapshot = snapshot;
        this.key = key;
        this.dependencies = dependencies;
        this.updatedAt = updatedAt;
        this.version = version;
        this.isTransformable = isTransformable;
    }

    public String getKey() {
        return key;
    }

    /**
     * List of dependencies.
     *
     * @return list of Web Resources keys.
     */
    public List<String> getDependencies() {
        return dependencies;
    }

    /**
     * Get last update time.
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public String getVersion() {
        return version;
    }

    public boolean isTransformable() {
        return isTransformable;
    }

    // in Refactor
    public Snapshot getSnapshot() {
        return snapshot;
    }

    /**
     * Get Resources of this Web Resource.
     */
    public LinkedHashMap<String, Resource> getResources(RequestCache cache) {
        return new LinkedHashMap<>();
    }

    /**
     * Get data resources.
     */
    public LinkedHashMap<String, Jsonable> getData() {
        return new LinkedHashMap<>();
    }

    /**
     * Returns condition if bundle has any.
     */
    public CachedCondition getCondition() {
        return null;
    }

    /**
     * Return transformers if bundle has any.
     */
    public CachedTransformers getTransformers() {
        return null;
    }

    /**
     * Transformer parameters needed for transformations.
     */
    public TransformerParameters getTransformerParameters() {
        return null;
    }

    /**
     * Resources can have different name and location types. This mapping returns location types for given resource
     * name type.
     */
    public Set<String> getLocationResourceTypesFor(String nameType) {
        return new HashSet<>();
    }

    /**
     * Returns if minification is enabled.
     *
     * Deprecated Snapshot#webResourcesWithDisabledMinification is always created with an empty Set and it will always return false.
     */
    @Deprecated
    public boolean isMinificationEnabled() {
        return !snapshot.webResourcesWithDisabledMinification.contains(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if ((o == null) || (getClass() != o.getClass())) {
            return false;
        }
        return key.equals(((Bundle) o).key);
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public String toString() {
        return "{" + key
                + (dependencies.isEmpty() ? "" : (", dependencies: " + StringUtils.join(dependencies, "," + ""))) + "}";
    }
}
