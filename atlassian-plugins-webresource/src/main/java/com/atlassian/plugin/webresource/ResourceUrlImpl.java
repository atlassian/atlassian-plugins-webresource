package com.atlassian.plugin.webresource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;

/**
 * An adapter between the current URL Generation code and previous URL Output code.
 *
 * @since v6.3
 */
public class ResourceUrlImpl extends ResourceUrl {
    private final Globals globals;
    private final Resource resource;
    private final String hash;
    private final Map<String, String> params;

    public ResourceUrlImpl(Globals globals, Resource resource, final Map<String, String> params, String hash) {
        this.globals = globals;
        this.resource = resource;
        this.params = params;
        this.hash = hash;
    }

    public String getName() {
        return resource.getName();
    }

    public String getKey() {
        return resource.getKey();
    }

    public String getType() {
        return resource.getNameOrLocationType();
    }

    public String getUrl(final boolean isAbsolute) {
        return globals.getRouter()
                .cloneWithNewUrlMode(isAbsolute)
                .resourceUrl(
                        getKey(), getName(), getParams(), resource.isCacheable(), true, hash, resource.getVersion());
    }

    public Map<String, String> getParams() {
        return params;
    }

    public PluginUrlResource.BatchType getBatchType() {
        return PluginUrlResource.BatchType.RESOURCE;
    }

    @Override
    public List<Resource> getResources(RequestCache requestCache) {
        List<Resource> resources = new ArrayList<>();
        resources.add(resource);
        return resources;
    }
}
