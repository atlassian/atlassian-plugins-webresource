package com.atlassian.plugin.webresource.transformer;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import com.google.common.base.Predicate;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerFactory;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerMatcher;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

/**
 * Implementation of {@link com.atlassian.plugin.webresource.transformer.StaticTransformersSupplier}
 *
 * @since v3.1.0
 */
public class DefaultStaticTransformersSupplier implements StaticTransformersSupplier {
    private final Collection<DescribedTransformer> describedTransformers;

    public DefaultStaticTransformersSupplier(
            WebResourceIntegration webResourceIntegration,
            WebResourceUrlProvider urlProvider,
            CdnResourceUrlTransformer cdnResourceUrlTransformer) {
        RelativeUrlTransformerFactory relativeUrlTransformerFactory =
                new RelativeUrlTransformerFactory(webResourceIntegration, urlProvider, cdnResourceUrlTransformer);
        RelativeUrlTransformerMatcher relativeUrlTransformerMatcher = new RelativeUrlTransformerMatcher();
        describedTransformers =
                Arrays.asList(new DescribedTransformer(relativeUrlTransformerMatcher, relativeUrlTransformerFactory));
    }

    private static Iterable<WebResourceTransformerFactory> toTransformerFactories(
            Collection<DescribedTransformer> describedTransformers, Predicate<DescribedTransformer> predicate) {
        return describedTransformers.stream()
                .filter(predicate)
                .map(input -> input.transformerFactory)
                .collect(Collectors.toList());
    }

    @Override
    public Iterable<WebResourceTransformerFactory> get(final String locationType) {
        return toTransformerFactories(describedTransformers, input -> input.matcher.matches(locationType));
    }

    @Override
    public Iterable<WebResourceTransformerFactory> get(final ResourceLocation resourceLocation) {
        return toTransformerFactories(describedTransformers, input -> input.matcher.matches(resourceLocation));
    }

    private static final class DescribedTransformer {
        private final WebResourceTransformerMatcher matcher;
        private final WebResourceTransformerFactory transformerFactory;

        private DescribedTransformer(
                WebResourceTransformerMatcher matcher, WebResourceTransformerFactory transformerFactory) {
            this.matcher = matcher;
            this.transformerFactory = transformerFactory;
        }
    }
}
