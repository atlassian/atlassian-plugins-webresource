package com.atlassian.plugin.webresource;

import java.util.concurrent.atomic.AtomicLong;

import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;

/**
 * A ready-to-use implementation of batching state counter. Ready to be plugged into {@link WebResourceIntegration}.
 *
 * @since 2.11.0
 */
public class WebResourceBatchingStateCounterImpl implements WebResourceBatchingStateCounter {
    private final PluginEventManager pluginEventManager;
    private final AtomicLong counter;
    private volatile boolean active = false;

    public WebResourceBatchingStateCounterImpl(PluginEventManager pluginEventManager) {
        this.pluginEventManager = pluginEventManager;
        this.counter = new AtomicLong(0L);

        pluginEventManager.register(this);
    }

    /**
     * Closes the instance.
     */
    public void close() {
        pluginEventManager.unregister(this);
    }

    @PluginEventListener
    public void onPluginFrameworkStarted(final PluginFrameworkStartedEvent event) {
        active = true;
        incrementCounterIfActive();
    }

    @PluginEventListener
    public void onPluginFrameworkPluginFrameworkShutdown(final PluginFrameworkShutdownEvent event) {
        active = false;
    }

    @PluginEventListener
    public void onPluginModuleEnabled(final PluginModuleEnabledEvent event) {
        incrementCounterIfActive();
    }

    @PluginEventListener
    public void onPluginModuleDisabled(final PluginModuleDisabledEvent event) {
        incrementCounterIfActive();
    }

    @Override
    public long getBatchingStateCounter() {
        return counter.get();
    }

    @Override
    public void incrementCounter() {
        incrementCounterIfActive();
    }

    private void incrementCounterIfActive() {
        if (active) {
            counter.incrementAndGet();
        }
    }
}
