package com.atlassian.plugin.webresource.legacy;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import com.google.common.collect.Iterables;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

/**
 * Builds superbatch resources
 *
 * @since v3.0
 */
public class SuperBatchBuilder {
    private final ResourceDependencyResolver dependencyResolver;
    private final PluginResourceLocator pluginResourceLocator;
    private final InclusionState excludedThings;

    public SuperBatchBuilder(
            ResourceDependencyResolver dependencyResolver, PluginResourceLocator pluginResourceLocator) {
        this(dependencyResolver, pluginResourceLocator, null);
    }

    public SuperBatchBuilder(
            ResourceDependencyResolver dependencyResolver,
            PluginResourceLocator pluginResourceLocator,
            InclusionState inclusionState) {
        this.dependencyResolver = dependencyResolver;
        this.pluginResourceLocator = pluginResourceLocator;
        this.excludedThings = inclusionState;
    }

    /**
     * Get all super-batch resources that match the given filter.
     */
    public Iterable<PluginResource> build() {
        final Predicate<String> alreadyIncluded = excludedThings.webresources::contains;
        final Predicate<String> notIncludedYet = alreadyIncluded.negate();

        final List<String> superBatchModuleKeys = stream(
                        dependencyResolver.getSuperBatchDependencies().spliterator(), false)
                .map(ModuleDescriptorStub::getCompleteKey)
                .collect(toList());

        final SuperBatchPluginResource superBatchPluginResource = new SuperBatchPluginResource();

        // todo: same key optimisation done in ContextBatchBuilder. I'm sure we can extract the logic or, better yet,
        //       use or re-use our graph walk data to do this faster.
        excludedThings.topLevel.forEach(superBatchPluginResource::addExcludedContext);

        superBatchModuleKeys.stream()
                .filter(notIncludedYet)
                .map(pluginResourceLocator::getPluginResources)
                .flatMap(Collection::stream)
                .filter(BatchPluginResource.class::isInstance)
                .map(BatchPluginResource.class::cast)
                .map(BatchPluginResource::getModuleCompleteKey)
                .forEachOrdered(superBatchPluginResource::addBatchedWebResourceDescriptor);

        if (superBatchPluginResource.isEmpty()) {
            return emptyList();
        }

        Iterables.addAll(excludedThings.webresources, superBatchModuleKeys);
        return singletonList(superBatchPluginResource);
    }
}
