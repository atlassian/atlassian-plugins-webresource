package com.atlassian.plugin.webresource;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.atlassian.plugin.webresource.impl.config.Config;

import static java.util.Arrays.asList;

/**
 * Outputs all URLs as {@code <link rel="prefetch" href="theurl">}.
 *
 * @see com.atlassian.webresource.api.assembler.WebResourceSet#writePrefetchLinks
 * @since 3.6.0
 */
public class PrefetchLinkWebResource extends AbstractWebResourceFormatter {
    public static final WebResourceFormatter FORMATTER = new PrefetchLinkWebResource();

    // crossorigin is supported for prefetch in Firefox: https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types
    private static final List<String> HANDLED_PARAMETERS = asList("title", Config.MEDIA_PARAM_NAME, "crossorigin");

    public boolean matches(String name) {
        return name != null
                && (name.endsWith(Config.JS_EXTENSION)
                        || name.endsWith(Config.CSS_EXTENSION)
                        || name.endsWith(Config.LESS_EXTENSION)
                        || name.endsWith(Config.SOY_EXTENSION));
    }

    public String formatResource(String url, Map<String, String> attributes) {
        if (!isValid(attributes)) {
            return "";
        }
        StringBuilder buffer = new StringBuilder("<link rel=\"prefetch\" href=\"");
        buffer.append(StringEscapeUtils.escapeHtml4(url)).append("\"");

        List attributeTokens = getParametersAsAttributes(attributes);
        if (attributes.size() > 0) {
            buffer.append(" ").append(StringUtils.join(attributeTokens.iterator(), " "));
        }

        buffer.append(">\n");

        return buffer.toString();
    }

    protected List<String> getAttributeParameters() {
        return HANDLED_PARAMETERS;
    }
}
