package com.atlassian.plugin.webresource.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.plugin.webresource.models.RawRequest;
import com.atlassian.plugin.webresource.models.Requestable;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static java.util.stream.Collectors.toCollection;
import static org.apache.commons.lang3.StringUtils.join;

/**
 * State maintained during single request.
 *
 * @since 3.3
 */
public class RequestState {
    private final Globals globals;
    private final RawRequest rawRequest;
    private final RequestCache requestCache;
    private final UrlBuildingStrategy urlBuildingStrategy;

    private final SuperbatchConfiguration superbatchConfiguration;
    private boolean syncbatchEnabled = true;
    private final boolean autoIncludeFrontendRuntime;
    /**
     * As the request is being served, we need to walk the full dependency graph
     * to discover all the content to serve, as well as all the content to avoid serving.
     * Keeping this cache avoids incurring that cost in a few scenarios. It also means
     * {@link RawRequest} can be untainted by this class' state and the intermediary steps
     * involved in serving the request.
     */
    private final Set<String> excludedResolvedCache;

    public RequestState(
            final Globals globals,
            final UrlBuildingStrategy urlBuildingStrategy,
            final boolean autoIncludeFrontendRuntime) {
        this(
                new RequestCache(globals),
                globals,
                urlBuildingStrategy,
                new RawRequest(),
                new SuperbatchConfiguration(globals.getConfig().isSuperBatchingEnabled()),
                globals.getConfig().isSyncBatchingEnabled(),
                autoIncludeFrontendRuntime);
    }

    public RequestState(final RequestState other) {
        this(
                other.requestCache,
                other.globals,
                other.urlBuildingStrategy,
                other.rawRequest,
                other.superbatchConfiguration,
                other.syncbatchEnabled,
                other.autoIncludeFrontendRuntime);
    }

    private RequestState(
            final RequestCache requestCache,
            final Globals globals,
            final UrlBuildingStrategy urlBuildingStrategy,
            final RawRequest rawRequest,
            final SuperbatchConfiguration superbatchConfiguration,
            final boolean syncbatchEnabled,
            final boolean autoIncludeFrontendRuntime) {
        this.requestCache = requestCache;
        this.globals = globals;
        this.urlBuildingStrategy = urlBuildingStrategy;
        this.rawRequest = rawRequest.deepClone();
        this.superbatchConfiguration = superbatchConfiguration;
        this.syncbatchEnabled = syncbatchEnabled;
        this.excludedResolvedCache = new HashSet<>();
        this.autoIncludeFrontendRuntime = autoIncludeFrontendRuntime;
    }

    /**
     * Called after generating urls, to clear current state and remember already included resources.
     */
    public void clearIncludedAndUpdateExcluded(
            @Nonnull final ResourcePhase resourcePhase, @Nonnull final Set<String> excludedResolved) {
        this.rawRequest.setPhaseCompleted(resourcePhase);
        this.excludedResolvedCache.addAll(excludedResolved);
    }

    @Nonnull
    public RequestState deepClone() {
        return new RequestState(this);
    }

    @Nonnull
    public RequestCache getRequestCache() {
        return requestCache;
    }

    /**
     * @see RawRequest#getExcludedAsLooseType()
     */
    @Nonnull
    @Deprecated
    public LinkedHashSet<String> getExcluded() {
        return rawRequest.getExcludedAsLooseType();
    }

    @Nonnull
    public Set<String> getExcludedData() {
        return rawRequest.getExcludedData();
    }

    /**
     * @see RawRequest#getIncludedAsLooseType()
     */
    @Nonnull
    @Deprecated
    public LinkedHashSet<String> getIncluded() {
        return rawRequest.getIncludedAsLooseType();
    }

    /**
     * @see RawRequest#getIncludedAsLooseType()
     */
    @Nonnull
    @Deprecated
    public LinkedHashSet<String> getIncluded(@Nonnull final ResourcePhase phaseType) {
        return rawRequest.getIncluded(phaseType).stream()
                .map(Requestable::toLooseType)
                .collect(toCollection(LinkedHashSet::new));
    }

    @Nonnull
    public LinkedHashMap<String, Jsonable> getIncludedData() {
        return rawRequest.getIncludedData();
    }

    @Nonnull
    public LinkedHashMap<String, Jsonable> getIncludedData(@Nonnull final ResourcePhase resourcePhase) {
        return rawRequest.getIncludedData(resourcePhase);
    }

    /**
     * Most UI code is not written to be idempotent, so content for each {@link Requestable}
     * should only ever be served to the UI once.
     *
     * Use this list to avoid incurring the full graph traversal cost each time you need
     * the complete list of "already used" content, such as:
     * <ul>
     *     <li>Bailing faster in graph traversal if an item in this list is encountered,</li>
     *     <li>Identifying content sets that would cause something to be served twice,</li>
     *     <li>Avoiding serving "empty" content sets once accounting for this list,</li>
     *     <li>and so on.</li>
     * </ul>
     * @since 5.5
     * @see com.atlassian.plugin.webresource.impl.discovery.BundleFinder#excludedResolved(Collection)
     * @see com.atlassian.plugin.webresource.impl.helpers.url.Resolved
     * @see com.atlassian.plugin.webresource.impl.helpers.url.CalculatedBatches#getExcludedResolved()
     * @see com.atlassian.webresource.api.assembler.RequiredResources#exclude(Set, Set)
     */
    @Nonnull
    public Set<String> getExcludedResolved() {
        return ImmutableSet.copyOf(excludedResolvedCache);
    }

    /**
     * @deprecated since 5.0.0. There is no replacement. Don't use god objects.
     */
    @Nonnull
    @Deprecated
    public Globals getGlobals() {
        return globals;
    }

    public RawRequest getRawRequest() {
        return rawRequest;
    }

    /**
     * Get all bundles.
     * <p>
     * It is another layer of cache over the 'globals.getBundles()' because it is used very heavily,
     * to avoid any performance drawbacks of atomic reference in the 'globals.getBundles()'.
     * </p>
     */
    @Nonnull
    public Snapshot getSnapshot() {
        return requestCache.getSnapshot();
    }

    /**
     * Whether to automatically include frontend runtime modules (e.g., WRM.require, WRM.data) automatically
     * when the feature is used.
     * @return true if modules should be automatically included; false if they should be manually required.
     * @since 5.5.0
     */
    public boolean isAutoIncludeFrontendRuntimeEnabled() {
        return autoIncludeFrontendRuntime;
    }

    public UrlBuildingStrategy getUrlStrategy() {
        return urlBuildingStrategy;
    }

    public SuperbatchConfiguration getSuperbatchConfiguration() {
        return superbatchConfiguration;
    }

    public void setSyncbatchEnabled(boolean enabled) {
        syncbatchEnabled = enabled;
    }

    public boolean isSyncbatchEnabled() {
        return syncbatchEnabled;
    }

    @Override
    public String toString() {
        return '[' + join(rawRequest.getIncludedAsLooseType(), ", ") + "] - ["
                + join(rawRequest.getExcludedAsLooseType(), ", ") + ']';
    }
}
