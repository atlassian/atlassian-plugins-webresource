package com.atlassian.plugin.webresource;

import java.util.ArrayList;
import java.util.Date;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;

/**
 * If the Resource belongs to Plugin - represents that Plugin, mostly needed as a stub.
 *
 * @since 3.3
 */
public class PluginResourceContainer extends Bundle {
    public PluginResourceContainer(Snapshot snapshot, String pluginKey, Date updatedAt, String version) {
        super(snapshot, pluginKey, new ArrayList<>(), updatedAt, version, false);
    }
}
