package com.atlassian.plugin.webresource.impl.support;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableSortedSet;

import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.helpers.url.CalculatedBatches;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Cache for URL Generation, or more exactly three Caches for:
 * - Batches for given included and excluded.
 * - Legacy resources for given included and excluded.
 * - Resolved excluded for given include and excluded.
 *
 * @since v3.3
 */
public interface UrlCache {
    /**
     * Get batches for given included and excluded.
     *
     * @return context batches, web resource batches, resolved excluded resources.
     */
    CalculatedBatches getBatches(IncludedExcludedConditionsAndBatchingOptions key, BatchesProvider provider);

    /**
     * Get resolved excluded for given include and excluded.
     *
     * @return resolved excluded resources
     */
    Set<String> getResolvedExcluded(
            IncludedExcludedConditionsAndBatchingOptions key, ResolvedExcludedProvider provider);

    void clear();

    public static interface BatchesProvider {
        CalculatedBatches get(IncludedExcludedConditionsAndBatchingOptions key);
    }

    public static interface ResolvedExcludedProvider {
        Set<String> get(IncludedExcludedConditionsAndBatchingOptions key);
    }

    public static class IncludedAndExcluded extends Tuple<LinkedHashSet<String>, Set<String>> {
        public IncludedAndExcluded(final LinkedHashSet<String> included, final Set<String> excluded) {
            super(included, ImmutableSortedSet.copyOf(excluded));
        }

        public LinkedHashSet<String> getIncluded() {
            return getFirst();
        }

        public Set<String> getExcluded() {
            return getLast();
        }
    }

    /**
     * Result of evaluated condition with the condition itself.
     */
    public static class EvaluatedCondition extends Tuple<CachedCondition, Boolean> {
        public EvaluatedCondition(CachedCondition cachedCondition, Boolean evaluationResult) {
            super(cachedCondition, evaluationResult);
        }
    }

    public static class IncludedExcludedConditionsAndBatchingOptions {
        private final IncludedAndExcluded inludedAndExcluded;
        private final Set<EvaluatedCondition> evaluatedConditions;

        public IncludedExcludedConditionsAndBatchingOptions(
                IncludedAndExcluded includedAndExcluded, Set<EvaluatedCondition> evaluatedConditions) {
            this.inludedAndExcluded = checkNotNull(includedAndExcluded);
            this.evaluatedConditions = checkNotNull(evaluatedConditions);
        }

        public LinkedHashSet<String> getIncluded() {
            return inludedAndExcluded.getIncluded();
        }

        public Set<String> getExcluded() {
            return inludedAndExcluded.getExcluded();
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof IncludedExcludedConditionsAndBatchingOptions)) {
                return false;
            }

            final IncludedExcludedConditionsAndBatchingOptions that = (IncludedExcludedConditionsAndBatchingOptions) o;

            if (!evaluatedConditions.equals(that.evaluatedConditions)) {
                return false;
            }
            if (!inludedAndExcluded.equals(that.inludedAndExcluded)) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = inludedAndExcluded.hashCode();
            result = 31 * result + evaluatedConditions.hashCode();
            return result;
        }
    }

    /**
     * Implementation.
     */
    public static class Impl implements UrlCache {
        private Cache<IncludedAndExcluded, List<CachedCondition>> cachedConditions;
        private Cache<IncludedExcludedConditionsAndBatchingOptions, CalculatedBatches> cachedBatches;
        private Cache<IncludedExcludedConditionsAndBatchingOptions, Set<String>> cachedResolvedExcluded;

        public Impl(long size) {
            cachedConditions = CacheBuilder.newBuilder().maximumSize(size).build();
            cachedBatches = CacheBuilder.newBuilder().maximumSize(size).build();
            cachedResolvedExcluded = CacheBuilder.newBuilder().maximumSize(size).build();
        }

        @Override
        public CalculatedBatches getBatches(
                final IncludedExcludedConditionsAndBatchingOptions key, final BatchesProvider provider) {
            try {
                return cachedBatches.get(key, () -> provider.get(key));
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Set<String> getResolvedExcluded(
                final IncludedExcludedConditionsAndBatchingOptions key, final ResolvedExcludedProvider provider) {
            try {
                return cachedResolvedExcluded.get(key, () -> provider.get(key));
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void clear() {
            cachedConditions.invalidateAll();
            cachedBatches.invalidateAll();
            cachedResolvedExcluded.invalidateAll();
        }
    }

    /**
     * No caching implementation.
     */
    public static class PassThrough implements UrlCache {
        @Override
        public CalculatedBatches getBatches(
                IncludedExcludedConditionsAndBatchingOptions key, BatchesProvider provider) {
            return provider.get(key);
        }

        @Override
        public Set<String> getResolvedExcluded(
                IncludedExcludedConditionsAndBatchingOptions key, ResolvedExcludedProvider provider) {
            return provider.get(key);
        }

        @Override
        public void clear() {}
    }
}
