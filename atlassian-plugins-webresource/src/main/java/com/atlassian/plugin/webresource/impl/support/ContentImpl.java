package com.atlassian.plugin.webresource.impl.support;

/**
 * Helper to simplify implementation of Content class.
 *
 * @since 3.3
 */
public abstract class ContentImpl implements Content {
    private String contentType;
    private boolean isTransformed;

    public ContentImpl(String contentType, boolean isTransformed) {
        this.contentType = contentType;
        this.isTransformed = isTransformed;
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public boolean isTransformed() {
        return isTransformed;
    }
}
