package com.atlassian.plugin.cache.filecache.impl;

import java.io.OutputStream;

import com.atlassian.plugin.cache.filecache.Cache;

import static com.atlassian.plugin.cache.filecache.impl.FileCacheImpl.ensureNotNull;

/**
 * Always streams each resource from the given input
 *
 * @since v2.13
 */
public class PassThroughCache implements Cache {
    private static final boolean CACHE_WAS_HIT = false; // This implementation will never cache, so it'll always miss

    @Override
    public boolean cache(String bucket, String key, OutputStream out, StreamProvider provider) {
        provider.write(out);
        return CACHE_WAS_HIT;
    }

    @Override
    public boolean cacheTwo(
            String bucket, String key, OutputStream out1, OutputStream out2, TwoStreamProvider provider) {
        provider.write(ensureNotNull(out1), ensureNotNull(out2));
        return CACHE_WAS_HIT;
    }

    @Override
    public void clear() {}
}
