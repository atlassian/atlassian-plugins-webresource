package com.atlassian.plugin.cache.filecache;

import java.io.OutputStream;

/**
 * Represents a file cache.
 *
 * @since 2.13
 */
public interface Cache {

    /**
     * Stream the contents identified by the key to the destination stream. Should the contents not exist in the cache
     * a new entry should be created if the implementation is a caching implementation.
     *
     * @param bucket   bucket to group keys
     * @param key      can not be null
     * @param out      where to write the cached item to
     * @param provider provides the underlying item on a cache-miss
     * @return if it was a cache-hit
     */
    boolean cache(String bucket, String key, OutputStream out, StreamProvider provider);

    /**
     * Stream two contents identified by the key to the destination stream. Should the contents not exist in the cache
     * a new entry should be created if the implementation is a caching implementation.
     *
     * @param bucket   bucket to group keys
     * @param key      can not be null
     * @param out1     where to write the first cached item to
     * @param out2     where to write the second cached item to
     * @param provider provides the underlying item on a cache-miss
     * @return if it was a cache-hit
     * @since 3.3
     */
    boolean cacheTwo(String bucket, String key, OutputStream out1, OutputStream out2, TwoStreamProvider provider);

    /**
     * Remove all entries in the cache.
     */
    void clear();

    /**
     * Interface used by the file caching system. Items wishing to participate in file caching will need to
     * implement this interface. This interface gives the file cache a means to get hold of the contents that will be cached.
     *
     * @since v2.13
     */
    interface StreamProvider {
        /**
         * Produce the complete stream and write to the designated output stream. Classes implementing this method should not
         * close the output stream.
         *
         * @param out designated output stream.
         */
        void write(OutputStream out);
    }

    /**
     * Interface used by the file caching system. Items wishing to participate in file caching will need to
     * implement this interface. This interface gives the file cache a means to get hold of two contents that will be cached.
     *
     * @since v3.3
     */
    interface TwoStreamProvider {
        /**
         * Produce the complete stream and write to the designated output stream. Classes implementing this method should not
         * close the output stream.
         *
         * @param out1 first designated output stream.
         * @param out2 second designated output stream.
         */
        void write(OutputStream out1, OutputStream out2);
    }
}
