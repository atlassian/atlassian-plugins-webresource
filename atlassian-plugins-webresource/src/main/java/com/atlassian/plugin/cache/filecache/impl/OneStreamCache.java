package com.atlassian.plugin.cache.filecache.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import com.atlassian.plugin.cache.filecache.Cache;

/**
 * Caches the contents of a stream, and deletes the cache when asked to do so.
 * This class encapsulates the lifecycle of a cached file, from as-yet-uncached, to cached, to needs-deletion, to deleted.
 * <p>
 * Calls to stream() always succeed, even if the cache was deleted.
 * The initial call to stream() will block other callers to stream() and deleteWhenPossible().
 * <p>
 * Subsequent calls to stream() don't block other calls.
 * <p>
 * Subsequent calls to deleteWhenPossible() don't block.
 * <p>
 * A call to deleteWhenPossible() always results in the file being (eventually) deleted.
 * <p>
 * Both stream() and deleteWhenPossible() can be called multiple times, in any order, with any
 * level of concurrency.
 *
 * @since v3.3
 */
public class OneStreamCache extends StreamsCache {
    protected final File tmpFile;

    public OneStreamCache(File tmpFile) {
        this.tmpFile = tmpFile;
    }

    public void stream(OutputStream out, final Cache.StreamProvider provider) {
        boolean fromCache = doEnter(
                () -> {
                    try {
                        streamToCache(provider);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                },
                tmpFile::isFile);
        try {
            if (fromCache) {
                streamFromFile(tmpFile, out);
            } else {
                provider.write(out);
            }
        } finally {
            doExit(tmpFile::delete);
        }
    }

    public void streamTwo(OutputStream out1, OutputStream out2, Cache.TwoStreamProvider provider) {
        throw new RuntimeException("one stream cache doesn't support streamTwo method!");
    }

    public void deleteWhenPossible() {
        deleteWhenPossible(tmpFile::delete);
    }

    protected void streamToCache(Cache.StreamProvider input) throws IOException {
        OutputStream cacheout = null;
        try {
            cacheout = new BufferedOutputStream(createWriteStream(tmpFile));
            input.write(cacheout);
        } finally {
            if (cacheout != null) {
                cacheout.close(); // NB: if this fails, we don't want to silently ignore it
            }
        }
    }
}
