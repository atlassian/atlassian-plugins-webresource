package com.atlassian.plugin.cache.filecache.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.CyclicBarrier;

import org.slf4j.Logger;

import junit.framework.TestCase;

import static org.mockito.Mockito.mock;

import static com.atlassian.plugin.webresource.TestUtils.getField;

public class TestTwoStreamsCache extends TestCase {
    public void testNormalOperation() throws Exception {
        File tmp1 = File.createTempFile("TestCachedFile", "test");
        tmp1.delete();
        File tmp2 = File.createTempFile("TestCachedFile", "test");
        tmp2.delete();

        final MockTwoStreamsProvider streamProvider = new MockTwoStreamsProvider("foo", "boo");
        final ByteArrayOutputStream out1 = new ByteArrayOutputStream();
        final ByteArrayOutputStream out2 = new ByteArrayOutputStream();

        TwoStreamsCache cf = new TwoStreamsCache(tmp1, tmp2);
        assertFalse(tmp1.exists());
        assertFalse(tmp2.exists());

        cf.streamTwo(out1, out2, streamProvider);
        assertTrue(tmp1.exists());
        assertTrue(tmp2.exists());
        assertEquals(1, streamProvider.callCount);
        assertEquals("foo", out1.toString());
        assertEquals("boo", out2.toString());

        out1.reset();
        out2.reset();
        cf.streamTwo(out1, out2, streamProvider);
        assertTrue(tmp1.exists());
        assertTrue(tmp2.exists());
        assertEquals(1, streamProvider.callCount); // second time round, it should use the cache, not the streamProvider
        assertEquals("foo", out1.toString());
        assertEquals("boo", out2.toString());

        cf.deleteWhenPossible();
        assertFalse(tmp1.exists());
        assertFalse(tmp2.exists());

        out1.reset();
        out2.reset();
        cf.streamTwo(out1, out2, streamProvider);
        assertFalse(tmp1.exists());
        assertFalse(tmp2.exists());
        assertEquals(2, streamProvider.callCount); // this time, it will have to use the streamProvider
        assertEquals("foo", out1.toString());
        assertEquals("boo", out2.toString());
    }

    public void testImmediateDelete() throws Exception {
        File tmp1 = File.createTempFile("TestCachedFile", "test");
        tmp1.delete();
        File tmp2 = File.createTempFile("TestCachedFile", "test");
        tmp2.delete();

        final MockTwoStreamsProvider streamProvider = new MockTwoStreamsProvider("foo", "boo");
        final ByteArrayOutputStream out1 = new ByteArrayOutputStream();
        final ByteArrayOutputStream out2 = new ByteArrayOutputStream();

        TwoStreamsCache cf = new TwoStreamsCache(tmp1, tmp2);
        assertFalse(tmp1.exists());
        assertFalse(tmp2.exists());

        cf.deleteWhenPossible();
        assertFalse(tmp1.exists());
        assertFalse(tmp2.exists());

        cf.streamTwo(out1, out2, streamProvider);
        assertFalse(tmp1.exists());
        assertFalse(tmp2.exists());
        assertEquals(1, streamProvider.callCount);
        assertEquals("foo", out1.toString());
        assertEquals("boo", out2.toString());
    }

    public void testConcurrentReaders() throws Exception {
        File tmp1 = File.createTempFile("TestCachedFile", "test");
        tmp1.delete();
        File tmp2 = File.createTempFile("TestCachedFile", "test");
        tmp2.delete();

        final TwoStreamsCache cf = new TwoStreamsCache(tmp1, tmp2);

        final CyclicBarrier start = new CyclicBarrier(3);

        class ReaderThread extends Thread {
            final MockTwoStreamsProvider streamProvider = new MockTwoStreamsProvider("foo", "boo");
            final ByteArrayOutputStream out1 = new ByteArrayOutputStream();
            final ByteArrayOutputStream out2 = new ByteArrayOutputStream();
            Exception caught;

            @Override
            public void run() {
                try {
                    start.await();

                    cf.streamTwo(out1, out2, streamProvider);
                } catch (Exception e) {
                    caught = e;
                }
            }
        }

        ReaderThread r1 = new ReaderThread();
        ReaderThread r2 = new ReaderThread();
        r1.start();
        r2.start();
        start.await();
        r1.join();
        r2.join();

        assertNull(r1.caught);
        assertNull(r2.caught);

        // should have been called once, by one of the threads.
        assertEquals(1, r1.streamProvider.callCount + r2.streamProvider.callCount);
        assertTrue(tmp1.exists());
        assertTrue(tmp2.exists());
    }

    public void testConcurrentDeleteDuringRead() throws Exception {
        File tmp1 = File.createTempFile("TestCachedFile", "test");
        tmp1.delete();
        File tmp2 = File.createTempFile("TestCachedFile", "test");
        tmp2.delete();

        final TwoStreamsCache cf = new TwoStreamsCache(tmp1, tmp2);

        final CyclicBarrier start = new CyclicBarrier(3);

        class ReaderThread extends Thread {
            final MockTwoStreamsProvider streamProvider = new MockTwoStreamsProvider("foo", "boo");
            final ByteArrayOutputStream out1 = new ByteArrayOutputStream();
            final ByteArrayOutputStream out2 = new ByteArrayOutputStream();
            Exception caught;

            @Override
            public void run() {
                try {
                    start.await();

                    cf.streamTwo(out1, out2, streamProvider);
                } catch (Exception e) {
                    caught = e;
                }
            }
        }

        ReaderThread r1 = new ReaderThread();
        ReaderThread r2 = new ReaderThread();
        r1.start();
        r2.start();
        start.await();
        r1.join();
        r2.join();

        assertNull(r1.caught);
        assertNull(r2.caught);

        // should have been called once, by one of the threads.
        assertEquals(1, r1.streamProvider.callCount + r2.streamProvider.callCount);
        assertTrue(tmp1.exists());
        assertTrue(tmp2.exists());
    }

    public void testExceptionDuringClosingOfCachedFile() throws Exception {
        File tmp1 = File.createTempFile("TestCachedFile", "test");
        tmp1.delete();
        File tmp2 = File.createTempFile("TestCachedFile", "test");
        tmp2.delete();

        TwoStreamsCache cf = new TwoStreamsCache(tmp1, tmp2) {
            protected OutputStream createWriteStream(File file) throws FileNotFoundException {
                return new FileOutputStream(file) {
                    public void close() throws IOException {
                        throw new RuntimeException("re-close");
                    }
                };
            }
        };
        cf.setLogger(mock(Logger.class));

        MockTwoStreamsProvider streamProvider = new MockTwoStreamsProvider("foo", "boo");
        ByteArrayOutputStream out1 = new ByteArrayOutputStream();
        ByteArrayOutputStream out2 = new ByteArrayOutputStream();

        cf.streamTwo(out1, out2, streamProvider);
        // exceptions writing to cache aren't propagated
        assertEquals(TwoStreamsCache.State.UNCACHED, getField(cf, "state"));
    }

    public void testExceptionDuringInitialCaching() throws Exception {
        File tmp1 = File.createTempFile("TestCachedFile", "test");
        tmp1.delete();
        File tmp2 = File.createTempFile("TestCachedFile", "test");
        tmp2.delete();

        final TwoStreamsCache cf = new TwoStreamsCache(tmp1, tmp2);
        cf.setLogger(mock(Logger.class));

        final MockTwoStreamsProvider streamProvider =
                new MockTwoStreamsProvider("foo", "boo", new NullPointerException("bar"));
        final ByteArrayOutputStream out1 = new ByteArrayOutputStream();
        final ByteArrayOutputStream out2 = new ByteArrayOutputStream();

        try {
            cf.streamTwo(out1, out2, streamProvider);
            fail("expected a NPE");
        } catch (NullPointerException e) {
            assertEquals("bar", e.getMessage());
        }
        assertEquals(TwoStreamsCache.State.UNCACHED, getField(cf, "state"));
    }

    public void testAcceptNullAsStream() throws Exception {
        File tmp1 = File.createTempFile("TestCachedFile", "test");
        tmp1.delete();
        File tmp2 = File.createTempFile("TestCachedFile", "test");
        tmp2.delete();

        final MockTwoStreamsProvider streamProvider = new MockTwoStreamsProvider("foo", "boo");
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        TwoStreamsCache cf = new TwoStreamsCache(tmp1, tmp2);

        cf.streamTwo(out, null, streamProvider);
        assertEquals("foo", out.toString());

        out.reset();
        cf.streamTwo(null, out, streamProvider);
        assertEquals("boo", out.toString());
    }
}
