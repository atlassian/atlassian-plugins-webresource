package com.atlassian.plugin.cache.filecache.impl;

import java.io.IOException;
import java.io.OutputStream;

import com.atlassian.plugin.cache.filecache.Cache;

public class MockTwoStreamsProvider implements Cache.TwoStreamProvider {
    final String content1;
    final String content2;
    final RuntimeException re;
    int callCount = 0;

    public MockTwoStreamsProvider(String content1, String content2) {
        this(content1, content2, null);
    }

    public MockTwoStreamsProvider(String content1, String content2, RuntimeException re) {
        this.content1 = content1;
        this.content2 = content2;
        this.re = re;
    }

    @Override
    public void write(OutputStream out1, OutputStream out2) {
        callCount++;
        try {
            out1.write(content1.getBytes());
            out2.write(content2.getBytes());
            if (re != null) {
                throw re;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
