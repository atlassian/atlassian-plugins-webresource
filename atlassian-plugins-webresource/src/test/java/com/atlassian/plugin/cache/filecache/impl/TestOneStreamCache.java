package com.atlassian.plugin.cache.filecache.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.CyclicBarrier;

import org.slf4j.Logger;

import junit.framework.TestCase;

import static org.mockito.Mockito.mock;

import static com.atlassian.plugin.webresource.TestUtils.getField;

public class TestOneStreamCache extends TestCase {
    public void testNormalOperation() throws Exception {
        File tmp = File.createTempFile("TestCachedFile", "test");
        tmp.delete();

        final MockStreamProvider streamProvider = new MockStreamProvider("foo");
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        OneStreamCache cf = new OneStreamCache(tmp);
        assertFalse(tmp.exists());

        cf.stream(out, streamProvider);
        assertTrue(tmp.exists());
        assertEquals(1, streamProvider.callCount);
        assertEquals("foo", out.toString());

        out.reset();
        cf.stream(out, streamProvider);
        assertTrue(tmp.exists());
        assertEquals(1, streamProvider.callCount); // second time round, it should use the cache, not the streamProvider
        assertEquals("foo", out.toString());

        cf.deleteWhenPossible();
        assertFalse(tmp.exists());

        out.reset();
        cf.stream(out, streamProvider);
        assertFalse(tmp.exists());
        assertEquals(2, streamProvider.callCount); // this time, it will have to use the streamProvider
        assertEquals("foo", out.toString());
    }

    public void testImmediateDelete() throws Exception {
        File tmp = File.createTempFile("TestCachedFile", "test");
        tmp.delete();

        final MockStreamProvider streamProvider = new MockStreamProvider("foo");
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        OneStreamCache cf = new OneStreamCache(tmp);
        assertFalse(tmp.exists());

        cf.deleteWhenPossible();
        assertFalse(tmp.exists());

        cf.stream(out, streamProvider);
        assertFalse(tmp.exists());
        assertEquals(1, streamProvider.callCount);
        assertEquals("foo", out.toString());
    }

    public void testConcurrentReaders() throws Exception {
        File tmp = File.createTempFile("TestCachedFile", "test");
        tmp.delete();

        final OneStreamCache cf = new OneStreamCache(tmp);

        final CyclicBarrier start = new CyclicBarrier(3);

        class ReaderThread extends Thread {
            final MockStreamProvider streamProvider = new MockStreamProvider("foo");
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            Exception caught;

            @Override
            public void run() {
                try {
                    start.await();

                    cf.stream(out, streamProvider);
                } catch (Exception e) {
                    caught = e;
                }
            }
        }

        ReaderThread r1 = new ReaderThread();
        ReaderThread r2 = new ReaderThread();
        r1.start();
        r2.start();
        start.await();
        r1.join();
        r2.join();

        assertNull(r1.caught);
        assertNull(r2.caught);

        // should have been called once, by one of the threads.
        assertEquals(1, r1.streamProvider.callCount + r2.streamProvider.callCount);
        assertTrue(tmp.exists());
    }

    public void testConcurrentDeleteDuringRead() throws Exception {
        File tmp = File.createTempFile("TestCachedFile", "test");
        tmp.delete();

        final OneStreamCache cf = new OneStreamCache(tmp);

        final CyclicBarrier start = new CyclicBarrier(3);

        class ReaderThread extends Thread {
            final MockStreamProvider streamProvider = new MockStreamProvider("foo");
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            Exception caught;

            @Override
            public void run() {
                try {
                    start.await();

                    cf.stream(out, streamProvider);
                } catch (Exception e) {
                    caught = e;
                }
            }
        }

        ReaderThread r1 = new ReaderThread();
        ReaderThread r2 = new ReaderThread();
        r1.start();
        r2.start();
        start.await();
        r1.join();
        r2.join();

        assertNull(r1.caught);
        assertNull(r2.caught);

        // should have been called once, by one of the threads.
        assertEquals(1, r1.streamProvider.callCount + r2.streamProvider.callCount);
        assertTrue(tmp.exists());
    }

    public void testExceptionDuringClosingOfCachedFile() throws Exception {
        File tmp = File.createTempFile("TestCachedFile", "test");
        tmp.delete();

        OneStreamCache cf = new OneStreamCache(tmp) {
            protected OutputStream createWriteStream(File file) throws FileNotFoundException {
                return new FileOutputStream(file) {
                    public void close() throws IOException {
                        throw new RuntimeException("re-close");
                    }
                };
            }
        };
        cf.setLogger(mock(Logger.class));

        MockStreamProvider streamProvider = new MockStreamProvider("foo");
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        cf.stream(out, streamProvider);
        // exceptions writing to cache aren't propagated
        assertEquals(OneStreamCache.State.UNCACHED, getField(cf, "state"));
    }

    public void testExceptionDuringInitialCaching() throws Exception {
        File tmp = File.createTempFile("TestCachedFile", "test");
        tmp.delete();

        final OneStreamCache cf = new OneStreamCache(tmp);
        cf.setLogger(mock(Logger.class));

        final MockStreamProvider streamProvider = new MockStreamProvider("foo", new NullPointerException("bar"));
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            cf.stream(out, streamProvider);
            fail("expected a NPE");
        } catch (NullPointerException e) {
            assertEquals("bar", e.getMessage());
        }
        assertEquals(OneStreamCache.State.UNCACHED, getField(cf, "state"));
    }

    public void testDeletedFile() throws Exception {
        File tmp = File.createTempFile("TestCachedFile", "test");

        final MockStreamProvider streamProvider = new MockStreamProvider("foo");
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final OneStreamCache cache = new OneStreamCache(tmp);

        // First hit - should call streamProvider.write
        cache.stream(out, streamProvider);
        tmp.delete();
        // Second hit - file has gone missing - we should cache resource once again
        cache.stream(out, streamProvider);
        // File is already cached - should not call streamProvider.write
        cache.stream(out, streamProvider);

        assertEquals(2, streamProvider.callCount);
    }
}
