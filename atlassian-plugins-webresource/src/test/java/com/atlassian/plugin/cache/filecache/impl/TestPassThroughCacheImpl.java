package com.atlassian.plugin.cache.filecache.impl;

import java.io.ByteArrayOutputStream;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestPassThroughCacheImpl {
    final boolean CACHE_WAS_HIT = true;

    @Test
    public void passesThroughForOneStream() {
        PassThroughCache cache = new PassThroughCache();
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        MockStreamProvider in1 = new MockStreamProvider("a");
        boolean cacheHit = cache.cache("bucket", "key1", out, in1);
        assertNotEquals(cacheHit, CACHE_WAS_HIT);
        assertEquals(1, in1.callCount);
        assertEquals("a", out.toString());

        out.reset();
        cacheHit = cache.cache("bucket", "key1", out, in1);
        assertNotEquals(cacheHit, CACHE_WAS_HIT);
        assertEquals(2, in1.callCount);
        assertEquals("a", out.toString());

        MockStreamProvider in2 = new MockStreamProvider("b");
        out.reset();
        cacheHit = cache.cache("bucket", "key2", out, in2);
        assertNotEquals(cacheHit, CACHE_WAS_HIT);
        assertEquals(1, in2.callCount);
        assertEquals("b", out.toString());
    }

    @Test
    public void passesThroughForTwoStreams() {
        PassThroughCache cache = new PassThroughCache();
        final ByteArrayOutputStream out1 = new ByteArrayOutputStream();
        final ByteArrayOutputStream out2 = new ByteArrayOutputStream();

        MockTwoStreamsProvider in1 = new MockTwoStreamsProvider("a", "b");
        boolean cacheHit = cache.cacheTwo("bucket", "key1", out1, out2, in1);
        assertNotEquals(cacheHit, CACHE_WAS_HIT);
        assertEquals(1, in1.callCount);
        assertEquals("a", out1.toString());
        assertEquals("b", out2.toString());

        out1.reset();
        out2.reset();
        cacheHit = cache.cacheTwo("bucket", "key1", out1, out2, in1);
        assertNotEquals(cacheHit, CACHE_WAS_HIT);
        assertEquals(2, in1.callCount);
        assertEquals("a", out1.toString());
        assertEquals("b", out2.toString());

        MockTwoStreamsProvider in2 = new MockTwoStreamsProvider("c", "d");
        out1.reset();
        out2.reset();
        cacheHit = cache.cacheTwo("bucket", "key2", out1, out2, in2);
        assertNotEquals(cacheHit, CACHE_WAS_HIT);
        assertEquals(1, in2.callCount);
        assertEquals("c", out1.toString());
        assertEquals("d", out2.toString());
    }
}
