package com.atlassian.plugin.webresource.mocks;

import javax.annotation.Nonnull;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

import static org.mockito.Mockito.mockingDetails;

public class MockBuilder {

    private MockBuilder() {}

    public static MockBuilder builder() {
        return new MockBuilder();
    }

    @Nonnull
    public BundleMockBuilder withMockedBundle(@Nonnull final Bundle bundle) {
        if (mockingDetails(bundle).isMock()) {
            return new BundleMockBuilder(this, bundle);
        }
        throw new IllegalArgumentException("The bundle object must be a mock.");
    }

    @Nonnull
    public ConfigMockBuilder withMockedConfig(@Nonnull final Config config) {
        if (mockingDetails(config).isMock()) {
            return new ConfigMockBuilder(this, config);
        }
        throw new IllegalArgumentException("The config object must be a mock.");
    }

    @Nonnull
    public GlobalsMockBuilder withMockedGlobals(@Nonnull final Globals globals) {
        if (mockingDetails(globals).isMock()) {
            return new GlobalsMockBuilder(this, globals);
        }
        throw new IllegalArgumentException("The globals object must be a mock.");
    }

    @Nonnull
    public ResourceMockBuilder withMockedResource(@Nonnull final Resource resource) {
        if (mockingDetails(resource).isMock()) {
            return new ResourceMockBuilder(this, resource);
        }
        throw new IllegalArgumentException("The resources object must be a mock.");
    }

    @Nonnull
    public RouterMockBuilder withMockedRouter(@Nonnull final Router router) {
        if (mockingDetails(router).isMock()) {
            return new RouterMockBuilder(this, router);
        }
        throw new IllegalArgumentException("The router object must be a mock.");
    }

    @Nonnull
    public ResourceLocationMockBuilder withMockedResourceLocation(@Nonnull final ResourceLocation resourceLocation) {
        if (mockingDetails(resourceLocation).isMock()) {
            return new ResourceLocationMockBuilder(this, resourceLocation);
        }
        throw new IllegalArgumentException("The resource location object must be a mock.");
    }
}
