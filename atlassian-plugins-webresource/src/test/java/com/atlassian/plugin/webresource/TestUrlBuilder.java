package com.atlassian.plugin.webresource;

import org.junit.Test;

import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;

public class TestUrlBuilder {
    @Test
    public void shouldAcceptNullAsHashName() {
        (new DefaultUrlBuilder()).addToHash("", null);
    }
}
