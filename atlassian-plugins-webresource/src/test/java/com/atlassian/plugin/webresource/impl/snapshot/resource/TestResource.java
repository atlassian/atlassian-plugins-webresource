package com.atlassian.plugin.webresource.impl.snapshot.resource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestResource {
    private static String SAMPLE_CONTENT = "(new function(){})();;";
    private static String PLUGIN_KEY = "plugin";
    private static String PATH_TO_RESOURCE = "path/to/resource.js";

    @Mock
    private Bundle bundle;

    @Mock
    private ResourceLocation resourceLocation;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private WebResourceIntegration webResourceIntegration;

    @InjectMocks
    private ResourceFactory resourceFactory;

    @Test
    public void shouldReturnCorrectContent() {
        when(bundle.getKey()).thenReturn("plugin");
        when(resourceLocation.getLocation()).thenReturn(PATH_TO_RESOURCE);
        when(webResourceIntegration
                        .getPluginAccessor()
                        .getEnabledPlugin(PLUGIN_KEY)
                        .getResourceAsStream(PATH_TO_RESOURCE))
                .thenReturn(new ByteArrayInputStream(SAMPLE_CONTENT.getBytes()));
        Resource resource = resourceFactory.createResource(bundle, resourceLocation, "", "");

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        resource.getContent().writeTo(byteArrayOutputStream, false);

        assertThat(byteArrayOutputStream.toString(), equalTo(SAMPLE_CONTENT));
    }
}
