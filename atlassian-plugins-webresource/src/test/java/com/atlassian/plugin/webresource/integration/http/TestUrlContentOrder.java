package com.atlassian.plugin.webresource.integration.http;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.stringContainsInOrder;

import static com.atlassian.plugin.webresource.impl.config.Config.ENABLE_BUNDLE_HASH_VALIDATION;

public class TestUrlContentOrder {
    static final String ATLASSIAN_CACHES_ENABLED_PROPERTY = "atlassian.disable.caches";

    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    private WebResource wr;
    private Router r;

    @Before
    public void setUp() throws Exception {
        System.setProperty(ENABLE_BUNDLE_HASH_VALIDATION, Boolean.FALSE.toString());
        System.setProperty(ATLASSIAN_CACHES_ENABLED_PROPERTY, Boolean.FALSE.toString());
        wr = new WebResourceImpl();

        wr.configure()
                .plugin("plugin.key")
                .webResource("feature-a")
                .context("foo")
                .resource("feature-a.js")
                .webResource("feature-b")
                .context("foo")
                .context("bar")
                .resource("feature-b.js")
                .webResource("feature-c")
                .context("bar")
                .resource("feature-c.js")
                .end();

        r = wr.getGlobals().getRouter();
    }

    /**
     * Added to cover a behaviour relied upon by Confluence. Remove in WRM v6.
     * @see <a href="https://ecosystem.atlassian.net/browse/PLUGWEB-640">PLUGWEB-640</a> for patch to satisfy Confluence.
     * @see <a href="https://ecosystem.atlassian.net/browse/PLUGWEB-495">PLUGWEB-495</a> for long-term solution.
     */
    @Test
    public void given_cachesEnabled_when_loadingContextBatch_then_apperanceOrderMatters() {
        final String fooFirst = r.contextBatchUrl("foo,bar", "js", emptyMap(), true, true, "123", "456");
        final String barFirst = r.contextBatchUrl("bar,foo", "js", emptyMap(), true, true, "123", "456");

        final String fooFirstContent = wr.getContent(fooFirst);
        final String barFirstContent = wr.getContent(barFirst);

        assertThat(fooFirstContent, not(equalTo(barFirstContent)));
        assertThat(fooFirstContent, stringContainsInOrder(asList("feature-a", "feature-b", "feature-c")));
        assertThat(barFirstContent, stringContainsInOrder(asList("feature-b", "feature-c", "feature-a")));
    }
}
