package com.atlassian.plugin.webresource.integration.stub;

import java.io.File;
import java.io.IOException;

import com.atlassian.plugin.webresource.WebResourceIntegration;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

public class WebResourceIntegrationImpl {

    public static WebResourceIntegration create() {
        WebResourceIntegration integration = mock(WebResourceIntegration.class);
        lenient().when(integration.getTemporaryDirectory()).thenReturn(getTempDir());
        lenient().when(integration.getSystemBuildNumber()).thenReturn("system-build-number");
        lenient().when(integration.getSystemCounter()).thenReturn("system-build-counter");
        lenient().when(integration.getSuperBatchVersion()).thenReturn("super-batch-version");
        lenient().when(integration.getHostApplicationVersion()).thenReturn("product-version");
        return integration;
    }

    public static File getTempDir() {
        try {
            return File.createTempFile("foo", "foo").getParentFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
