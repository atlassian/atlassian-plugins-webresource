package com.atlassian.plugin.webresource.graph;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.models.Requestable;

import static java.util.Objects.requireNonNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Class used to build a mocked version of {@link DependencyGraph}
 *
 * @since 5.3.0
 */
public class DependencyGraphMock {
    private final Collection<Dependency> dependencies;

    @JsonCreator
    public DependencyGraphMock(@JsonProperty("dependencies") @Nonnull final Collection<Dependency> dependencies) {
        this.dependencies =
                requireNonNull(dependencies, "The dependencies are mandatory to build the dependency graph");
    }

    @Nonnull
    public DependencyGraph<Requestable> toDependencyGraph() {
        final DependencyGraph<Requestable> dependencyGraph = new DependencyGraph<>(Requestable.class);
        for (final Dependency dependency : dependencies) {
            dependencyGraph.addDependencies(
                    dependency.getSourceRequestableKey(), dependency.getContextDependencyRequestableKeys());
        }
        return dependencyGraph;
    }

    public static final class Dependency {
        private final Requestable sourceRequestableKey;
        private final Set<String> dependencyRequestableKeys;
        private final Set<String> contextDependencyRequestableKeys;
        private final boolean root;

        @JsonCreator
        public Dependency(
                @JsonProperty("sourceRequestableKey") @Nonnull final String sourceRequestableKey,
                @JsonProperty("dependencyRequestableKeys") @Nonnull final Set<String> dependencyRequestableKeys,
                @JsonProperty("contextDependencyRequestableKeys") @Nonnull Set<String> contextDependencyRequestableKeys,
                @JsonProperty("root") final boolean root) {
            requireNonNull(sourceRequestableKey, "The source requestable key is mandatory.");
            requireNonNull(dependencyRequestableKeys, "The context dependency requestable keys are mandatory.");
            requireNonNull(contextDependencyRequestableKeys, "The dependency requestable keys are mandatory.");
            this.dependencyRequestableKeys = dependencyRequestableKeys;
            this.sourceRequestableKey = new GenericRequestable(sourceRequestableKey);
            this.contextDependencyRequestableKeys = contextDependencyRequestableKeys;
            this.root = root;
        }

        @Nonnull
        public Requestable getSourceRequestableKey() {
            return sourceRequestableKey;
        }

        @Nonnull
        public Collection<Requestable> getContextDependencyRequestableKeys() {
            return contextDependencyRequestableKeys.stream()
                    .map(GenericRequestable::new)
                    .collect(Collectors.toList());
        }

        @Nonnull
        public Collection<Requestable> getDependencyRequestableKeys() {
            return dependencyRequestableKeys.stream()
                    .map(GenericRequestable::new)
                    .collect(Collectors.toList());
        }

        @Nonnull
        public WebResourceModuleDescriptor toWebResourceModuleDescriptor() {
            final WebResourceModuleDescriptor webResourceModuleDescriptor = mock(WebResourceModuleDescriptor.class);
            when(webResourceModuleDescriptor.getCompleteKey()).thenReturn(sourceRequestableKey.getKey());
            when(webResourceModuleDescriptor.getContextDependencies()).thenReturn(contextDependencyRequestableKeys);
            when(webResourceModuleDescriptor.isRootPage()).thenReturn(root);
            return webResourceModuleDescriptor;
        }
    }

    public static class GenericRequestable extends Requestable {
        public GenericRequestable(@Nonnull String key) {
            super(key);
        }

        @Override
        public String toString() {
            return String.format("<fake!%s>", getKey());
        }
    }
}
