package com.atlassian.plugin.webresource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.mockito.ArgumentMatchers;
import org.mockito.stubbing.Answer;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.integration.stub.RouterWithoutHashes;
import com.atlassian.plugin.webresource.integration.stub.WebResourceIntegrationImpl;
import com.atlassian.plugin.webresource.transformer.ContentTransformerFactory;
import com.atlassian.plugin.webresource.transformer.ContentTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.sourcemap.ReadableSourceMap;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUtils {

    public static Globals createGlobals() {
        return createGlobals(createWebResourceIntegration(), RouterWithoutHashes.class);
    }

    public static Globals createGlobals(
            WebResourceIntegration integration, WebResourceUrlProvider urlProvider, Class<? extends Router> klass) {
        Config config = createConfig(integration, urlProvider);
        Globals globals = new Globals(config, null, null);
        return setRouter(globals, klass);
    }

    public static Globals createGlobals(WebResourceIntegration integration, Class<? extends Router> routerClass) {
        Config config = createConfig(integration, null);
        Globals globals = new Globals(config, null, null);
        return setRouter(globals, routerClass);
    }

    private static Config createConfig(
            WebResourceIntegration integration, @Nullable WebResourceUrlProvider urlProvider) {
        TransformerCache transformerCache =
                new TransformerCache(mock(PluginEventManager.class), mock(PluginAccessor.class));
        ResourceBatchingConfiguration batchingConfig = mock(DefaultResourceBatchingConfiguration.class);
        Config config = new Config(
                batchingConfig,
                integration,
                urlProvider == null ? new WebResourceUrlProviderImpl(integration) : urlProvider,
                mock(ServletContextFactory.class),
                transformerCache);
        config.setStaticTransformers(mock(StaticTransformers.class));
        return config;
    }

    private static Globals setRouter(Globals globals, Class<? extends Router> routerClass) {
        Router router;
        try {
            Constructor<? extends Router> routerConstructor = routerClass.getDeclaredConstructor(Globals.class);
            router = routerConstructor.newInstance(globals);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        setField(globals, "router", router);

        return globals;
    }

    public static WebResourceIntegration createWebResourceIntegration() {
        PluginInformation pluginInformation = mock(PluginInformation.class);
        lenient().when(pluginInformation.getVersion()).thenReturn("0");

        Plugin plugin = mock(Plugin.class);
        lenient().when(plugin.getPluginInformation()).thenReturn(pluginInformation);

        ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        lenient().when(moduleDescriptor.getPlugin()).thenReturn(plugin);

        PluginAccessor pluginAccessor = mock(PluginAccessor.class);
        lenient().when(pluginAccessor.getEnabledPluginModule(any(String.class))).thenReturn(moduleDescriptor);

        WebResourceIntegration integration = WebResourceIntegrationImpl.create();
        lenient().when(integration.getPluginAccessor()).thenReturn(pluginAccessor);
        lenient().when(integration.getSuperBatchVersion()).thenReturn("super");
        return integration;
    }

    static WebResourceModuleDescriptor createWebResourceModuleDescriptor(final String completeKey, final Plugin p) {
        return createWebResourceModuleDescriptor(completeKey, p, emptyList(), emptyList());
    }

    static WebResourceModuleDescriptor createWebResourceModuleDescriptor(
            final String completeKey, final Plugin p, final List<ResourceDescriptor> resourceDescriptors) {
        return createWebResourceModuleDescriptor(completeKey, p, resourceDescriptors, emptyList());
    }

    public static WebResourceModuleDescriptor createWebResourceModuleDescriptor(
            final String completeKey,
            final Plugin p,
            final List<ResourceDescriptor> resourceDescriptors,
            final List<String> dependencies) {
        return createWebResourceModuleDescriptor(completeKey, p, resourceDescriptors, dependencies, emptySet());
    }

    public static WebResourceModuleDescriptor createWebResourceModuleDescriptor(
            final String completeKey,
            final Plugin p,
            final List<ResourceDescriptor> resourceDescriptors,
            final List<String> dependencies,
            final Set<String> contexts) {
        return createWebResourceModuleDescriptor(
                completeKey, p, resourceDescriptors, dependencies, contexts, emptyList());
    }

    public static WebResourceModuleDescriptor createWebResourceModuleDescriptor(
            final String completeKey,
            final Plugin p,
            final List<ResourceDescriptor> resourceDescriptors,
            final List<String> dependencies,
            final Set<String> contexts,
            final List<WebResourceTransformation> transformations) {
        return createWebResourceModuleDescriptor(
                completeKey, p, resourceDescriptors, dependencies, contexts, transformations, () -> null, emptyMap());
    }

    public static WebResourceModuleDescriptor createWebResourceModuleDescriptor(
            final String completeKey,
            final Plugin p,
            final List<ResourceDescriptor> resourceDescriptors,
            final List<String> dependencies,
            final Set<String> contexts,
            final List<WebResourceTransformation> transformations,
            final Supplier<DecoratingCondition> condition,
            final Map<String, WebResourceDataProvider> dataProviders) {
        return createWebResourceModuleDescriptor(
                completeKey,
                p,
                resourceDescriptors,
                dependencies,
                new HashSet<>(),
                contexts,
                transformations,
                condition,
                dataProviders,
                false);
    }

    public static WebResourceModuleDescriptor createWebResourceModuleDescriptor(
            final String completeKey,
            final Plugin p,
            final List<ResourceDescriptor> resourceDescriptors,
            final List<String> dependencies,
            final Set<String> contextDependencies,
            final Set<String> contexts,
            final List<WebResourceTransformation> transformations,
            final Supplier<DecoratingCondition> condition,
            final Map<String, WebResourceDataProvider> dataProviders,
            boolean isRootPage) {
        return new TestWebResourceModuleDescriptor(
                ModuleFactory.LEGACY_MODULE_FACTORY,
                new DefaultHostContainer(),
                completeKey,
                resourceDescriptors,
                p,
                dependencies,
                contextDependencies,
                contexts,
                transformations,
                condition,
                dataProviders,
                isRootPage);
    }

    public static List<ResourceDescriptor> createResourceDescriptors(String... resourceNames) {
        return createResourceDescriptors(asList(resourceNames));
    }

    public static List<ResourceDescriptor> createResourceDescriptors(List<String> resourceNames) {
        List<ResourceDescriptor> resourceDescriptors = new ArrayList<>();
        for (String resourceName : resourceNames) {
            resourceDescriptors.add(createResourceDescriptor(resourceName));
        }
        return resourceDescriptors;
    }

    public static ResourceDescriptor createResourceDescriptor(String resourceName) {
        return createResourceDescriptor(resourceName, new TreeMap<String, String>());
    }

    public static ResourceDescriptor createResourceDescriptor(String resourceName, Map<String, String> parameters) {
        try {
            String xml = "<resource type=\"download\" name=\"" + resourceName + "\" location=\"" + resourceName
                    + "\">\n" + "<param name=\"source\" value=\"webContextStatic\"/>\n";

            for (String key : parameters.keySet()) {
                xml += "<param name=\"" + escapeXMLCharacters(key) + "\" value=\""
                        + escapeXMLCharacters(parameters.get(key)) + "\"/>\n";
            }

            xml += "</resource>";
            return new ResourceDescriptor(
                    new Dom4jDelegatingElement(DocumentHelper.parseText(xml).getRootElement()));
        } catch (DocumentException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static List<String> createNamedHashes(String... values) {
        if (values.length % 2 != 0) {
            throw new IllegalArgumentException("Must provide an even number of parameters");
        }
        List<String> hashes = new LinkedList<>();
        Iterator<String> iter = asList(values).iterator();
        while (iter.hasNext()) {
            hashes.add(iter.next());
        }
        return Collections.unmodifiableList(hashes);
    }

    public static void mockTransformers(
            PluginAccessor pluginAccessor, Map<String, ? extends WebResourceTransformerFactory> transformers) {
        Iterable<UrlReadingWebResourceTransformerModuleDescriptor> descriptors =
                Iterables.transform(transformers.entrySet(), (Function<
                                Map.Entry<String, ? extends WebResourceTransformerFactory>,
                                UrlReadingWebResourceTransformerModuleDescriptor>)
                        input -> {
                            UrlReadingWebResourceTransformerModuleDescriptor descriptor =
                                    mock(UrlReadingWebResourceTransformerModuleDescriptor.class);
                            when(descriptor.getKey()).thenReturn(input.getKey());
                            when(descriptor.getModule()).thenReturn(input.getValue());
                            return descriptor;
                        });

        List<UrlReadingWebResourceTransformerModuleDescriptor> descriptorList = Lists.newArrayList(descriptors);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(UrlReadingWebResourceTransformerModuleDescriptor.class))
                .thenReturn(descriptorList);
    }

    public static UrlReadingWebResourceTransformerModuleDescriptor mockTransformer(
            String key, String aliasKey, Class klass) {
        try {
            UrlReadingWebResourceTransformerModuleDescriptor descriptor =
                    mock(UrlReadingWebResourceTransformerModuleDescriptor.class);
            when(descriptor.getKey()).thenReturn(key);
            when(descriptor.getAliasKey()).thenReturn(aliasKey);
            when(descriptor.getModule()).thenReturn((WebResourceTransformerFactory) klass.newInstance());
            return descriptor;
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static ContentTransformerModuleDescriptor mockContentTransformer(String key, Class klass) {
        try {
            ContentTransformerModuleDescriptor descriptor = mock(ContentTransformerModuleDescriptor.class);
            when(descriptor.getKey()).thenReturn(key);
            when(descriptor.getModule()).thenReturn((ContentTransformerFactory) klass.newInstance());
            return descriptor;
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static void mockTransformerAlias(PluginAccessor pluginAccessor, final String key, final String alias) {
        List<UrlReadingWebResourceTransformerModuleDescriptor> descriptors =
                pluginAccessor.getEnabledModuleDescriptorsByClass(
                        UrlReadingWebResourceTransformerModuleDescriptor.class);
        List<UrlReadingWebResourceTransformerModuleDescriptor> transformed =
                Lists.transform(descriptors, urlReadingWebResourceTransformerModuleDescriptor -> {
                    if (key.equals(urlReadingWebResourceTransformerModuleDescriptor.getKey())) {
                        when(urlReadingWebResourceTransformerModuleDescriptor.getAliasKey())
                                .thenReturn(alias);
                    }
                    return urlReadingWebResourceTransformerModuleDescriptor;
                });
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(UrlReadingWebResourceTransformerModuleDescriptor.class))
                .thenReturn(transformed);
    }

    /**
     * Creates a web resource transformation that adds the given queryParams to the querystring and the given hashes to the hash
     *
     * @param queryParams Map of query params
     * @param hashes List of hashes
     */
    public static WebResourceTransformation createWebResourceTransformation(
            final Map<String, String> queryParams, final Iterable<String> hashes, final String transformationType) {
        return createWebResourceTransformation(() -> queryParams, () -> hashes, transformationType);
    }

    /**
     * Creates a web resource transformation that adds the given queryParams to the querystring and the given hashes to the hash
     *
     * @param queryParams Map of query params
     * @param hashes List of hashes
     */
    public static WebResourceTransformation createWebResourceTransformation(
            final Supplier<Map<String, String>> queryParams,
            final Supplier<Iterable<String>> hashes,
            final String transformationType) {
        WebResourceTransformation webResourceTransformation = mock(WebResourceTransformation.class);
        doAnswer((Answer<Object>) invocation -> {
                    Object[] args = invocation.getArguments();
                    UrlBuilder urlBuilder = (UrlBuilder) args[2];
                    for (Map.Entry<String, String> entry : queryParams.get().entrySet()) {
                        urlBuilder.addToQueryString(entry.getKey(), entry.getValue());
                    }
                    for (String hash : hashes.get()) {
                        urlBuilder.addToHash("", hash);
                    }
                    return null;
                })
                .when(webResourceTransformation)
                .addTransformParameters(
                        any(TransformerCache.class),
                        any(TransformerParameters.class),
                        any(DefaultUrlBuilder.class),
                        any(UrlBuildingStrategy.class));
        doAnswer((Answer<Object>) invocation -> {
                    Object[] args = invocation.getArguments();
                    String type = (String) args[0];
                    return transformationType.equals(type);
                })
                .when(webResourceTransformation)
                .matches(any(String.class));
        return webResourceTransformation;
    }

    private static String escapeXMLCharacters(String input) {
        return input.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt");
    }

    public static Plugin createTestPlugin() {
        return createTestPlugin("test.atlassian", "1");
    }

    public static Plugin createTestPlugin(String pluginKey, String version, Class... loadableClasses) {
        try {
            final Plugin plugin = mock(Plugin.class);
            PluginInformation pluginInfo = new PluginInformation();
            pluginInfo.setVersion(version);
            when(plugin.getPluginInformation()).thenReturn(pluginInfo);
            when(plugin.getKey()).thenReturn(pluginKey);
            when(plugin.getDateLoaded()).thenReturn(new Date());
            for (Class loadableClass : loadableClasses) {
                when(plugin.loadClass(ArgumentMatchers.eq(loadableClass.getName()), (Class<?>) any()))
                        .thenReturn(
                                (Class<Object>) TestUtils.class.getClassLoader().loadClass(loadableClass.getName()));
            }
            return plugin;
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static StaticTransformers emptyStaticTransformers() {
        return new StaticTransformers() {

            @Override
            public void loadTwoPhaseProperties(
                    ResourceLocation resourceLocation, java.util.function.Function<String, InputStream> loadFromFile) {}

            @Override
            public boolean hasTwoPhaseProperties() {
                return false;
            }

            @Override
            public void addToUrl(
                    String locationType,
                    TransformerParameters transformerParameters,
                    UrlBuilder urlBuilder,
                    UrlBuildingStrategy urlBuildingStrategy) {}

            @Override
            public Content transform(
                    Content content,
                    TransformerParameters transformerParameters,
                    ResourceLocation resourceLocation,
                    QueryParams queryParams,
                    String sourceUrl) {
                return content;
            }

            @Override
            public Set<String> getParamKeys() {
                return new HashSet<>();
            }
        };
    }

    public static Map<String, String> emptyParams() {
        return emptyMap();
    }

    public static Map<String, String> cacheableRequestParams() {
        return cacheableRequestParams(emptyParams());
    }

    public static Map<String, String> cacheableRequestParams(Map<String, String> others) {
        Map<String, String> params = Collections.singletonMap("_statichash", "yes");
        return Stream.concat(params.entrySet().stream(), others.entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public static void setupSuperBatch(ResourceBatchingConfiguration batchingConfiguration) {
        when(batchingConfiguration.isSuperBatchingEnabled()).thenReturn(true);
        when(batchingConfiguration.isContextBatchingEnabled()).thenReturn(true);
        when(batchingConfiguration.getSuperBatchModuleCompleteKeys())
                .thenReturn(asList(
                        "test.atlassian:superbatch", "test.atlassian:superbatch2", "test.atlassian:missing-plugin"));
    }

    public static QueryParams emptyQueryParams() {
        Map<String, String> map = emptyMap();
        return map::get;
    }

    public static WebResourceDataProvider emptyDataProvider() {
        return stringDataProvider("");
    }

    public static WebResourceDataProvider stringDataProvider(final String str) {
        return () -> jsonableOf(str);
    }

    public static Jsonable jsonableOf(final String str) {
        return writer -> writer.write("\"" + str + "\"");
    }

    public static String toString(DownloadableResource downloadableResource) {
        ByteArrayOutputStream buff = new ByteArrayOutputStream();
        try {
            downloadableResource.streamResource(buff);
        } catch (DownloadException e) {
            throw new RuntimeException(e);
        }
        return buff.toString();
    }

    public static Object getField(Object obj, String name) {
        try {
            return getFieldAndEnsureItsAccessible(obj, name).get(obj);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static void setField(Object obj, String name, Object value) {
        try {
            getFieldAndEnsureItsAccessible(obj, name).set(obj, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private static Field getFieldAndEnsureItsAccessible(Object obj, String name) {
        Class klass = obj instanceof Class ? (Class) obj : obj.getClass();
        Field field = null;
        do {
            try {
                field = klass.getDeclaredField(name);
            } catch (NoSuchFieldException e) {
            }
            klass = klass.getSuperclass();
            if (klass == null) {
                throw new NoSuchFieldError();
            }
        } while (field == null);

        if (!field.isAccessible()) {
            field.setAccessible(true);
        }
        Field.class.getDeclaredMethods();
        Method getDeclaredFields0 = null;
        try {
            getDeclaredFields0 = Class.class.getDeclaredMethod("getDeclaredFields0", boolean.class);
            getDeclaredFields0.setAccessible(true);
            Field[] fields = (Field[]) getDeclaredFields0.invoke(Field.class, false);
            Field modifiers = null;
            for (Field each : fields) {
                if ("modifiers".equals(each.getName())) {
                    modifiers = each;
                    break;
                }
            }
            if (modifiers != null) {
                modifiers.setAccessible(true);
                modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }

        return field;
    }

    /**
     * @param keysAndValues a list of strings in an order of key, value, key, value, etc.
     * @return mutable map of values provided
     */
    @Nonnull
    public static Map<String, String> buildMap(@Nullable String... keysAndValues) {
        final Map<String, String> map = new HashMap<>();
        if (keysAndValues != null) {
            for (int i = 0; i < keysAndValues.length; ) {
                map.put(keysAndValues[i], keysAndValues[i + 1]);
                i += 2;
            }
        }
        return map;
    }

    /**
     * @param seedValues seed entries
     * @return mutable map of values provided
     */
    @Nonnull
    public static Map<String, String> buildMap(@Nullable Map<String, String> seedValues) {
        final Map<String, String> map = new HashMap<>();
        if (seedValues != null) {
            map.putAll(seedValues);
        }
        return map;
    }

    public static List<String> buildList(String... values) {
        return asList(values);
    }

    public static Content asContent(final String string) {
        return new ContentImpl(null, false) {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                try {
                    out.write(string.getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                return null;
            }
        };
    }

    public static String removeWebResourceLogs(String html) {
        return Pattern.compile("\\n*<!-- WebResource logs[\\s\\S]*?-->\\n*|\\n*\\Z", Pattern.MULTILINE)
                .matcher(html)
                .replaceAll("");
    }

    public static String removeTrailingSpaces(String str) {
        return str.replaceAll("[\\n\\s]*\\Z", "");
    }

    public static class TestWebResourceModuleDescriptor extends WebResourceModuleDescriptor {
        private final String completeKey;
        private final List<ResourceDescriptor> resourceDescriptors;
        private final Plugin p;
        private final List<String> dependencies;
        private final Set<String> contextDependencies;
        private final Set<String> contexts;
        private final List<WebResourceTransformation> transformations;
        private final Supplier<DecoratingCondition> condition;
        private final Map<String, WebResourceDataProvider> dataProviders;
        private final boolean isRootPage;

        public TestWebResourceModuleDescriptor(
                ModuleFactory legacyModuleFactory,
                DefaultHostContainer defaultHostContainer,
                String completeKey,
                List<ResourceDescriptor> resourceDescriptors,
                Plugin p,
                List<String> dependencies,
                Set<String> contextDependencies,
                Set<String> contexts,
                List<WebResourceTransformation> transformations,
                Supplier<DecoratingCondition> condition,
                Map<String, WebResourceDataProvider> dataProviders,
                boolean isRootPage) {
            super(legacyModuleFactory, defaultHostContainer);
            this.completeKey = completeKey;
            this.resourceDescriptors = resourceDescriptors;
            this.p = p;
            this.dependencies = dependencies;
            this.contextDependencies = contextDependencies;
            this.contexts = contexts;
            this.transformations = transformations;
            this.condition = condition;
            this.dataProviders = dataProviders;
            this.isRootPage = isRootPage;
        }

        @Override
        public String getCompleteKey() {
            return completeKey;
        }

        @Override
        public List<ResourceDescriptor> getResourceDescriptors() {
            return resourceDescriptors;
        }

        @Override
        public String getPluginKey() {
            return p.getKey();
        }

        @Override
        public Plugin getPlugin() {
            return p;
        }

        @Override
        public List<String> getDependencies() {
            return dependencies;
        }

        @Override
        public Set<String> getContextDependencies() {
            return contextDependencies;
        }

        @Override
        public boolean isRootPage() {
            return isRootPage;
        }

        @Override
        public Set<String> getContexts() {
            return contexts;
        }

        @Override
        public List<WebResourceTransformation> getTransformations() {
            return transformations;
        }

        @Override
        public DecoratingCondition getCondition() {
            return condition.get();
        }

        @Override
        public ResourceLocation getResourceLocation(String type, String name) {
            if ("download".equals(type)) {
                return new ResourceLocation("", name, type, "", "", emptyMap());
            }
            return super.getResourceLocation(type, name);
        }

        @Override
        public Map<String, WebResourceDataProvider> getDataProviders() {
            return dataProviders;
        }
    }

    public static class StubUrlReadingCondition implements UrlReadingCondition {
        @Override
        public void addToUrl(UrlBuilder urlBuilder) {}

        @Override
        public boolean shouldDisplay(QueryParams params) {
            return true;
        }

        @Override
        public void init(Map<String, String> params) throws PluginParseException {}
    }
}
