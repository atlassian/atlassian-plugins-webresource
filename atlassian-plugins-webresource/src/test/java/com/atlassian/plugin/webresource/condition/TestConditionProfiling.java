package com.atlassian.plugin.webresource.condition;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import javax.annotation.Nonnull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.util.profiling.MetricKey;
import com.atlassian.util.profiling.MetricTag;
import com.atlassian.util.profiling.MetricsConfiguration;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThan;

public class TestConditionProfiling extends TestCase {

    private static final Duration SHOULD_DISPLAY_SLEEP_TIME = Duration.ofMillis(400);
    private static final Map<String, String> SHOULD_DISPLAY_SLEEP_TIME_PARAMS =
            TestUtils.buildMap("shouldDisplaySleepTimeMilli", String.valueOf(SHOULD_DISPLAY_SLEEP_TIME.toMillis()));
    private TestableStrategy metricStrategy;

    @Before
    public void setup() {
        metricStrategy = new TestableStrategy();
        StrategiesRegistry.addMetricStrategy(metricStrategy);
    }

    @After
    public void tearDown() {
        StrategiesRegistry.removeMetricStrategy(metricStrategy);
    }

    @Test
    public void callingResourceWithUrlReadingCondition_shouldEmitMetricsCorrectly() {
        wr.configure()
                .disableContextBatching()
                .disableWebResourceBatching()
                .plugin("plugin")
                .webResource("a")
                .condition(SlowUrlReadingCondition.class, SHOULD_DISPLAY_SLEEP_TIME_PARAMS)
                .context("myContext")
                .resource("a1.js")
                .end();

        wr.requireContext("myContext");
        wr.paths();

        verifyMetricHasBeenEmittedWithCorrectValues(1, SlowUrlReadingCondition.class);
    }

    @Test
    public void callingResourceWithMultipleUrlReadingConditions_shouldEmitMetricsCorrectly() {
        wr.configure()
                .disableContextBatching()
                .disableWebResourceBatching()
                .plugin("plugin")
                .webResource("a")
                .condition(SlowUrlReadingCondition.class, SHOULD_DISPLAY_SLEEP_TIME_PARAMS)
                .condition(SlowUrlReadingCondition.class, SHOULD_DISPLAY_SLEEP_TIME_PARAMS)
                .context("myContext")
                .resource("a1.js")
                .end();

        wr.requireContext("myContext");
        wr.paths();

        verifyMetricHasBeenEmittedWithCorrectValues(2, SlowUrlReadingCondition.class);
    }

    private void verifyMetricHasBeenEmittedWithCorrectValues(
            int expectedNumberOfEmittedMetrics, Class<?> expectedConditionClassName) {
        assertThat(metricStrategy.getStartedTimers().size(), equalTo(expectedNumberOfEmittedMetrics));
        IntStream.range(0, expectedNumberOfEmittedMetrics).forEach(i -> {
            MetricKey metricKey = metricStrategy.getStartedTimers().get(i);
            assertThat(metricKey.getMetricName(), is("web.resource.condition"));
            assertThat(
                    metricKey.getTags(),
                    hasItems(
                            MetricTag.of("fromPluginKey", "plugin"),
                            MetricTag.of("atl-analytics", true),
                            MetricTag.of("conditionClassName", expectedConditionClassName.getName())));
        });

        long expectedTimeTaken = expectedNumberOfEmittedMetrics * SHOULD_DISPLAY_SLEEP_TIME.toNanos();
        long expectedTimeTakenWithComputationalOffset = expectedTimeTaken + TimeUnit.MILLISECONDS.toNanos(20);
        assertThat(
                metricStrategy.getTotalElapsedTime().toNanos(),
                allOf(greaterThanOrEqualTo(expectedTimeTaken), lessThan(expectedTimeTakenWithComputationalOffset)));
    }

    private static class TestableStrategy implements MetricStrategy {

        private final Map<MetricKey, Duration> startedMetricsAndElapsedTime = new IdentityHashMap<>();

        public List<MetricKey> getStartedTimers() {
            return new ArrayList<>(startedMetricsAndElapsedTime.keySet());
        }

        public Duration getTotalElapsedTime() {
            return startedMetricsAndElapsedTime.values().stream()
                    .reduce(Duration::plus)
                    .orElseThrow(() -> new RuntimeException("No started timers found"));
        }

        @Override
        public void setConfiguration(@Nonnull MetricsConfiguration configuration) {}

        @Nonnull
        @Override
        public Ticker startTimer(String ignored) {
            throw new UnsupportedOperationException("not implemented yet");
        }

        @Nonnull
        @Override
        public Ticker startTimer(@Nonnull MetricKey metricKey) {
            Instant started = Instant.now();
            startedMetricsAndElapsedTime.put(metricKey, Duration.ZERO);
            return () -> startedMetricsAndElapsedTime.replace(metricKey, Duration.between(started, Instant.now()));
        }

        @Override
        public void updateHistogram(@Nonnull String metricName, long value) {}

        @Override
        public void updateTimer(@Nonnull String metricName, long time, @Nonnull TimeUnit timeUnit) {}
    }
}
