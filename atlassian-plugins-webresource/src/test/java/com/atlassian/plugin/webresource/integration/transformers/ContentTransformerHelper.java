package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.transformer.ContentTransformerFactory;
import com.atlassian.plugin.webresource.transformer.UrlReadingContentTransformer;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;

public abstract class ContentTransformerHelper implements ContentTransformerFactory {
    @Override
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        return urlBuilder -> {};
    }

    @Override
    public UrlReadingContentTransformer makeResourceTransformer(TransformerParameters parameters) {
        final ContentTransformerHelper _this = this;
        return (cdnResourceUrlTransformer, content, resourceLocation, params, sourceUrl) ->
                _this.transform(content, resourceLocation, params, sourceUrl);
    }

    public abstract Content transform(
            Content content, ResourceLocation resourceLocation, QueryParams params, String sourceUrl);
}
