package com.atlassian.plugin.webresource.integration.transformers;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashSet;
import java.util.Map;

import com.atlassian.plugin.webresource.impl.annotators.ResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.integration.BaseTestResourceContentAnnotator;

public class OffsetContentAnnotatorsWithSourceMap {
    public static ResourceContentAnnotator[] build() {
        return new ResourceContentAnnotator[] {
            new BaseTestResourceContentAnnotator() {
                @Override
                public int beforeResourceInBatch(
                        LinkedHashSet<String> requiredResources,
                        Resource resource,
                        final Map<String, String> params,
                        OutputStream stream)
                        throws IOException {
                    stream.write("first-annotator\n".getBytes());
                    return 1;
                }

                @Override
                public void afterResourceInBatch(
                        LinkedHashSet<String> requiredResources,
                        Resource resource,
                        final Map<String, String> params,
                        OutputStream stream)
                        throws IOException {
                    stream.write("\nfirst-annotator".getBytes());
                }
            },
            new BaseTestResourceContentAnnotator() {
                @Override
                public int beforeResourceInBatch(
                        LinkedHashSet<String> requiredResources,
                        Resource resource,
                        final Map<String, String> params,
                        OutputStream stream)
                        throws IOException {
                    stream.write("second-annotator\n".getBytes());
                    return 1;
                }

                @Override
                public void afterResourceInBatch(
                        LinkedHashSet<String> requiredResources,
                        Resource resource,
                        final Map<String, String> params,
                        OutputStream stream)
                        throws IOException {
                    stream.write("\nsecond-annotator".getBytes());
                }
            }
        };
    }
}
