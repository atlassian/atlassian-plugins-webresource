package com.atlassian.plugin.webresource.integration;

import java.util.Random;

import org.junit.Test;

import com.atlassian.plugin.webresource.AlwaysFalseCondition;
import com.atlassian.plugin.webresource.AlwaysTrueCondition;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.matchesPattern;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.doReturn;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;

// Tests for base use cases, happy path.
public class TestSpecialCases extends TestCase {
    @Test
    public void shouldNotCommentOutCatchKeywordInBatchGuards_CONFDEV_38634() {
        wr.configure(config -> doReturn(true).when(config).isJavaScriptTryCatchWrappingEnabled())
                .plugin("plugin")
                .webResource("a")
                .context("general")
                .resource("a1.js", "last line // some comment")
                .end();

        wr.requireContext("general");

        assertThat(
                wr.getContent(wr.drain()),
                matches(
                        // Checking for the newline
                        "last line // some comment\n}catch"));
    }

    @Test
    public void shouldHandleLongUrlsSafelyWithoutConsumingTooMuchCPU_PLUGWEB_316() {
        wr.configure().end();
        StringBuilder url90k = new StringBuilder();

        Random random = new Random();
        for (int i = 0; i < 90 * 8 * 1024; i++) {
            url90k.append((char) (random.nextInt(25) + 97));
        }

        Exception ex = assertThrows(RuntimeException.class, () -> wr.getContent(url90k.toString()));
        assertThat(ex.getMessage(), matchesPattern(".*no route.*"));
    }

    @Test
    public void shouldHandleInvertedCondition() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .context("general")
                .condition(AlwaysTrueCondition.class)
                .resource("a1.js")
                .webResource("b")
                .context("general")
                .invertedCondition(AlwaysTrueCondition.class)
                .resource("b1.js")
                .end();

        wr.requireContext("general");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(contextBatchUrl("general", "js", buildMap("always-true", "true"))));

        assertThat(wr.getContent(wr.lastDrain()), allOf(matches("content of a1"), not(matches("content of b1"))));
    }

    @Test
    public void shouldApplyConditionsForExcluded() {
        wr.configure()
                .plugin("plugin")
                // It's needed to connect and merge alpha and beta in the same batch.
                .webResource("shared")
                .context("alpha")
                .context("beta")
                .resource("shared.js")
                .webResource("a")
                .context("alpha")
                .dependency("plugin:c")
                .resource("a1.js")
                .webResource("b")
                .condition(AlwaysFalseCondition.class)
                .context("beta")
                .dependency("plugin:c")
                .resource("b1.js")
                .webResource("c")
                .resource("c1.js")
                .end();

        wr.requireContext("alpha");
        wr.excludeContext("beta");

        assertThat(wr.paths(), matches(contextBatchUrl("alpha,-beta", "js", buildMap("always-false", "true"))));

        assertThat(wr.getContent(), matches("content of c1", "content of a1"));
    }

    @Test
    public void shouldEscapeUrl() {
        wr.configure()
                .disableWebResourceBatching()
                .plugin("plugin")
                .webResource("shared")
                .resource("something?=special.js")
                .end();

        wr.requireResource("plugin:shared");

        assertThat(
                "'?' should be encoded since it can denote the end of the path, but '/' and '=' are valid as-is",
                wr.paths().get(0),
                matches("/something%3F=special.js"));
    }
}
