package com.atlassian.plugin.webresource.assembler;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import com.atlassian.plugin.webresource.integration.stub.WebResourceIntegrationImpl;
import com.atlassian.webresource.api.assembler.AssembledResources;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultPageBuilderService {
    private PageBuilderService create() {
        Map<String, Object> requestCache = new HashMap<>();
        WebResourceIntegration mockWebResourceIntegration = WebResourceIntegrationImpl.create();
        when(mockWebResourceIntegration.getRequestCache()).thenReturn(requestCache);

        WebResourceImpl wr = new WebResourceImpl();
        wr.configure().end();
        DefaultWebResourceAssemblerFactory webResourceAssemblerFactory =
                new DefaultWebResourceAssemblerFactory(wr.getGlobals());

        return new DefaultPageBuilderService(mockWebResourceIntegration, webResourceAssemblerFactory);
    }

    @Test
    public void testAssembledResources() {
        assertNotNull(create().assembler());
    }

    @Test
    public void testAssembledResourcesIsSameInstanceOnMultipleCalls() {
        PageBuilderService pageBuilderService = create();
        AssembledResources assembledResources = pageBuilderService.assembler().assembled();

        assertTrue(assembledResources == pageBuilderService.assembler().assembled());
    }

    @Test
    public void testRequiredResources() {
        assertNotNull(create().assembler().resources());
    }

    @Test
    public void testRequiredResourcesIsSameInstanceOnMultipleCalls() {
        PageBuilderService pageBuilderService = create();
        RequiredResources requiredResources = pageBuilderService.assembler().resources();

        assertTrue(requiredResources == pageBuilderService.assembler().resources());
    }

    @Test
    public void testSeed() {
        WebResourceAssembler assembler = mock(WebResourceAssembler.class);
        PageBuilderService pageBuilderService = create();

        pageBuilderService.seed(assembler);

        assertTrue(assembler == pageBuilderService.assembler());
    }

    @Test(expected = IllegalStateException.class)
    public void testSeedFailsIfAlreadySeeded() {
        PageBuilderService pageBuilderService = create();
        pageBuilderService.seed(mock(WebResourceAssembler.class));
        pageBuilderService.seed(mock(WebResourceAssembler.class));
    }

    @Test(expected = IllegalStateException.class)
    public void testSeedFailsIfAlreadyRequested() {
        PageBuilderService pageBuilderService = create();
        pageBuilderService.assembler();
        pageBuilderService.seed(mock(WebResourceAssembler.class));
    }
}
