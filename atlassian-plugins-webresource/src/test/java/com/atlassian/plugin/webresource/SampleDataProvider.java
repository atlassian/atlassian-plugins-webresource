package com.atlassian.plugin.webresource;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

public class SampleDataProvider implements WebResourceDataProvider {
    @Override
    public Jsonable get() {
        return new JsonableString("value of sample data provider");
    }
}
