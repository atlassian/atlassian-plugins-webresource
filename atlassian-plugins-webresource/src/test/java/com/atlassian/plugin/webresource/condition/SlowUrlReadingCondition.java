package com.atlassian.plugin.webresource.condition;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

import static java.lang.Thread.sleep;

public class SlowUrlReadingCondition implements UrlReadingCondition {

    private long shouldDisplaySleepTimeMilli = 90;

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
        this.shouldDisplaySleepTimeMilli = Long.parseLong(params.get("shouldDisplaySleepTimeMilli"));
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder) {}

    @Override
    public boolean shouldDisplay(QueryParams params) {
        try {
            sleep(shouldDisplaySleepTimeMilli);
        } catch (InterruptedException ignored) {
        }
        return true;
    }
}
