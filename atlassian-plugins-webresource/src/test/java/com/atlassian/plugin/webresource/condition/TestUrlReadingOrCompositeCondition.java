package com.atlassian.plugin.webresource.condition;

import java.util.Map;

import org.junit.Test;

import com.atlassian.plugin.webresource.AlwaysFalseCondition;
import com.atlassian.plugin.webresource.AlwaysTrueCondition;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static com.atlassian.plugin.webresource.TestUtils.emptyQueryParams;
import static com.atlassian.plugin.webresource.condition.ConditionTestUtils.decorate;

public class TestUrlReadingOrCompositeCondition {
    final UrlBuildingStrategy normalStrategy = UrlBuildingStrategy.normal();

    @Test
    public void testPasses() {
        DecoratingCompositeCondition condition = new DecoratingOrCompositeCondition();
        condition.addCondition(decorate(new AlwaysTrueCondition()));
        condition.addCondition(decorate(new AlwaysFalseCondition()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testFails() {
        DecoratingCompositeCondition condition = new DecoratingOrCompositeCondition();
        condition.addCondition(decorate(new AlwaysFalseCondition()));
        condition.addCondition(decorate(new AlwaysFalseCondition()));
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        condition.addToUrl(urlBuilder, normalStrategy);
        Map<String, String> params = urlBuilder.buildParams();
        assertFalse(condition.shouldDisplay(params::get));
    }
}
