package com.atlassian.plugin.webresource.transformer.instance;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import com.google.common.collect.ImmutableMap;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.integration.stub.WebResourceIntegrationImpl;
import com.atlassian.plugin.webresource.transformer.DefaultTransformableResource;
import com.atlassian.plugin.webresource.transformer.DefaultTransformerParameters;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.api.transformer.TransformableResource;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.servlet.AbstractFileServerServlet.PATH_SEPARATOR;
import static com.atlassian.plugin.webresource.TestUtils.asContent;
import static com.atlassian.plugin.webresource.TestUtils.emptyQueryParams;

public class TestRelativeUrlTransformerFactory {
    public static final String STATIC_PREFIX = "/test/s/en_GB/1/10/1000/_";
    public static final String MODULE_KEY = "test-resource";
    public static final String TRANSFORM_PREFIX =
            STATIC_PREFIX + "/download/resources" + PATH_SEPARATOR + "test.atlassian:" + MODULE_KEY + PATH_SEPARATOR;
    private static final String CDN_PATH = "/very.cdn.wow";
    public static final String CDN_PREFIX = "//much.cloudfront.com" + CDN_PATH;

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private WebResourceUrlProvider mockWebResourceUrlProvider;

    @Mock
    private PluginAccessor mockPluginAccessor;

    @Mock
    private CdnResourceUrlTransformer cdnResourceUrlTransformer;

    private Plugin testPlugin;

    @Before
    public void setUp() {
        testPlugin = TestUtils.createTestPlugin();
        when(mockWebResourceUrlProvider.getStaticResourcePrefix("1", UrlMode.RELATIVE))
                .thenReturn(STATIC_PREFIX);
        when(cdnResourceUrlTransformer.getResourceCdnPrefix(any()))
                .then(invocation -> CDN_PREFIX + invocation.getArguments()[0]);
        when(mockPluginAccessor.getPlugin(eq(testPlugin.getKey()))).thenReturn(testPlugin);
    }

    @Test
    public void removesResourceCdnPrefixCorrectly() {
        assertThat(RelativeUrlTransformerFactory.removeComponentsBeforePath("http://cdn"), CoreMatchers.equalTo(""));
        assertThat(
                RelativeUrlTransformerFactory.removeComponentsBeforePath("https://cdn:8443"), CoreMatchers.equalTo(""));
        assertThat(
                RelativeUrlTransformerFactory.removeComponentsBeforePath("https://cdn:8443/somepath"),
                CoreMatchers.equalTo("/somepath"));
        assertThat(
                RelativeUrlTransformerFactory.removeComponentsBeforePath("https://cdn:8443/mycdn/jira1"),
                CoreMatchers.equalTo("/mycdn/jira1"));
        assertThat(
                RelativeUrlTransformerFactory.removeComponentsBeforePath("https://cdn/somepath"),
                CoreMatchers.equalTo("/somepath"));
    }

    @Test
    public void testAddToUrlNoCdn() {
        TransformerParameters transformerParameters = new DefaultTransformerParameters(testPlugin.getKey(), MODULE_KEY);

        WebResourceTransformerFactory transformerFactory = new RelativeUrlTransformerFactory(
                mockWebResourceIntegration(false), mockWebResourceUrlProvider, cdnResourceUrlTransformer);

        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        transformerFactory.makeUrlBuilder(transformerParameters).addToUrl(urlBuilder);

        assertFalse(urlBuilder.buildParams().containsKey(RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY));
    }

    @Test
    public void testAddToUrlCdn() {
        TransformerParameters transformerParameters = new DefaultTransformerParameters(testPlugin.getKey(), MODULE_KEY);

        WebResourceTransformerFactory transformerFactory = new RelativeUrlTransformerFactory(
                mockWebResourceIntegration(true), mockWebResourceUrlProvider, cdnResourceUrlTransformer);

        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        transformerFactory.makeUrlBuilder(transformerParameters).addToUrl(urlBuilder);

        assertEquals("true", urlBuilder.buildParams().get(RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY));
    }

    @Test
    public void testReplaceRelative() {
        assertTransformWorked(
                TRANSFORM_PREFIX + "../relative.png",
                CDN_PATH + TRANSFORM_PREFIX + "../relative.png",
                "../relative.png");
    }

    @Test
    public void testReplaceAbsoluteSkipped() {
        assertTransformWorked("/absolute.png", "/absolute.png");
    }

    @Test
    public void testReplaceAbsoluteSkippedFQ() {
        assertTransformWorked("http://atlassian.com/test/absolute.png", "http://atlassian.com/test/absolute.png");
    }

    @Test
    public void testReplaceAbsoluteHttpsSkipped() {
        assertTransformWorked("https://atlassian.com/test/absolute.png", "https://atlassian.com/test/absolute.png");
    }

    @Test
    public void testReplaceDataUriSkipped() {
        assertTransformWorked(
                "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAARCAYAAAA",
                "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAARCAYAAAA");
    }

    @Test
    public void testReplaceAbsoluteWithFunkyCharacters() {
        assertTransformWorked("/f\u00F8tex/absolute.png", "/f\u00F8tex/absolute.png");
    }

    @Test
    public void testReplaceAbsoluteWithFunkyCharactersFQ() {
        assertTransformWorked("http://www.f\u00F8tex.dk", "http://www.f\u00F8tex.dk");
    }

    @Test
    public void testReplaceRelativeWithFunkyCharactersTxPrefix() {
        assertTransformWorked(
                TRANSFORM_PREFIX + "f\u00F8tex.png", CDN_PATH + TRANSFORM_PREFIX + "f\u00F8tex.png", "f\u00F8tex.png");
    }

    @Test
    public void testReplaceAbsoluteWithNumbers() {
        assertTransformWorked("/1/absolute.png", "/1/absolute.png");
    }

    @Test
    public void testReplaceAbsoluteWithNumbersFQ() {
        assertTransformWorked("http://192.168.1.1:8080/test/absolute.png", "http://192.168.1.1:8080/test/absolute.png");
    }

    @Test
    public void testReplaceAbsoluteWithNumbersTxPrefix() {
        assertTransformWorked(
                TRANSFORM_PREFIX + "../relative1.png",
                CDN_PATH + TRANSFORM_PREFIX + "../relative1.png",
                "../relative1.png");
    }

    /**
     * Tests that an exception is thrown if a resource is fetched via CDN, but no CDN strategy is present
     */
    @Test(expected = RuntimeException.class)
    public void testCdnStrategyChangingBetweenUrlGenerationAndResourceFetchTime() {
        WebResourceTransformerFactory transformerFactory = new RelativeUrlTransformerFactory(
                mockWebResourceIntegration(false), mockWebResourceUrlProvider, cdnResourceUrlTransformer);

        TransformerParameters transformerParameters = new DefaultTransformerParameters(testPlugin.getKey(), MODULE_KEY);
        Content content = asContent(".test { blahblah url(../relative1.png) }");
        TransformableResource resource = new DefaultTransformableResource(
                mock(ResourceLocation.class), ResourceServingHelpers.asDownloadableResource(content));
        DownloadableResource transformed = transformerFactory
                .makeResourceTransformer(transformerParameters)
                .transform(resource, cdnQueryParams());
        downloadableResourceToString(transformed);
    }

    private void assertTransformWorked(final String expectedUrl, final String originalUrl) {
        assertTransformWorked(expectedUrl, expectedUrl, originalUrl);
    }

    private void assertTransformWorked(
            final String expectedUrl, final String expectedCdnUrl, final String originalUrl) {
        WebResourceTransformerFactory transformerFactory = new RelativeUrlTransformerFactory(
                mockWebResourceIntegration(false), mockWebResourceUrlProvider, cdnResourceUrlTransformer);
        assertTransformWorked(transformerFactory, expectedUrl, originalUrl, emptyQueryParams());

        transformerFactory = new RelativeUrlTransformerFactory(
                mockWebResourceIntegration(true), mockWebResourceUrlProvider, cdnResourceUrlTransformer);
        assertTransformWorked(transformerFactory, expectedCdnUrl, originalUrl, cdnQueryParams());
    }

    private void assertTransformWorked(
            WebResourceTransformerFactory transformerFactory,
            String expectedUrl,
            String originalUrl,
            QueryParams queryParams) {
        assertTransformWorked(transformerFactory, "url(%s)", expectedUrl, originalUrl, queryParams);
        assertTransformWorked(transformerFactory, "url( %s )", expectedUrl, originalUrl, queryParams);
        assertTransformWorked(transformerFactory, "url (%s)", expectedUrl, originalUrl, queryParams);
        assertTransformWorked(transformerFactory, "url(\"%s\")", expectedUrl, originalUrl, queryParams);
        assertTransformWorked(transformerFactory, "url( \"%s\")", expectedUrl, originalUrl, queryParams);
        assertTransformWorked(transformerFactory, "url (\"%s\")", expectedUrl, originalUrl, queryParams);
        assertTransformWorked(transformerFactory, "url('%s')", expectedUrl, originalUrl, queryParams);
        assertTransformWorked(transformerFactory, "url( '%s' )", expectedUrl, originalUrl, queryParams);
        assertTransformWorked(transformerFactory, "url ('%s' )", expectedUrl, originalUrl, queryParams);
    }

    private void assertTransformWorked(
            WebResourceTransformerFactory transformerFactory,
            final String format,
            final String expectedUrl,
            final String originalUrl,
            QueryParams queryParams) {
        final String original = String.format(".test { blahblah %s }", String.format(format, originalUrl));
        final String expected = String.format(".test { blahblah %s }", String.format(format, expectedUrl));
        TransformerParameters transformerParameters = new DefaultTransformerParameters(testPlugin.getKey(), MODULE_KEY);
        //        TransformableResource transformableResource = new TransformableResource(null, null, new
        // TestTransformerUtils.MockDownloadableResource(original));
        Content content = asContent(original);
        TransformableResource resource = new DefaultTransformableResource(
                mock(ResourceLocation.class), ResourceServingHelpers.asDownloadableResource(content));
        DownloadableResource transformed = transformerFactory
                .makeResourceTransformer(transformerParameters)
                .transform(resource, queryParams);
        assertEquals(expected, downloadableResourceToString(transformed));
    }

    private WebResourceIntegration mockWebResourceIntegration(boolean supportsCdn) {
        WebResourceIntegration mockWebResourceIntegration = WebResourceIntegrationImpl.create();
        when(mockWebResourceIntegration.getPluginAccessor()).thenReturn(mockPluginAccessor);
        when(mockWebResourceIntegration.getCDNStrategy()).thenReturn(supportsCdn ? createMockCdnStrategy() : null);
        return mockWebResourceIntegration;
    }

    private CDNStrategy createMockCdnStrategy() {
        return new CDNStrategy() {
            @Override
            public boolean supportsCdn() {
                return true;
            }

            @Override
            public String transformRelativeUrl(String url) {
                return CDN_PREFIX + url;
            }
        };
    }

    private QueryParams cdnQueryParams() {
        ImmutableMap<String, String> map =
                ImmutableMap.of(RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY, String.valueOf("true"));
        return map::get;
    }

    private String downloadableResourceToString(DownloadableResource resource) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            resource.streamResource(out);
            return out.toString("UTF-8");
        } catch (DownloadException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
