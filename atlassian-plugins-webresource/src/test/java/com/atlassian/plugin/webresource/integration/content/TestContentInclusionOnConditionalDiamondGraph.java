package com.atlassian.plugin.webresource.integration.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.fixtures.ConditionalDiamond;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.mockito.Mockito.doReturn;

// PLUGWEB-648
@RunWith(Parameterized.class)
public class TestContentInclusionOnConditionalDiamondGraph extends TestCase {
    private final boolean contextBatchingEnabled;
    private final boolean webresourceBatchingEnabled;
    private List<String> urls;

    @Parameterized.Parameters(name = "context-batching: {0}, webresource-batching: {1}")
    public static Collection<Object[]> configure() {
        return asList(new Object[][] {
            {true, true},
            {true, false},
            {false, true},
            {false, false}
        });
    }

    public TestContentInclusionOnConditionalDiamondGraph(
            final boolean contextBatchingEnabled, final boolean webresourceBatchingEnabled) {
        this.contextBatchingEnabled = contextBatchingEnabled;
        this.webresourceBatchingEnabled = webresourceBatchingEnabled;
    }

    @Before
    public void setUp() throws Exception {
        ConditionalDiamond.configure(wr, config -> {
            doReturn(contextBatchingEnabled).when(config).isContextBatchingEnabled();
            doReturn(webresourceBatchingEnabled).when(config).isWebResourceBatchingEnabled();
            doReturn(false).when(config).isSuperBatchingEnabled();
            doReturn(false).when(config).isPerformanceTrackingEnabled();
        });
        urls = new ArrayList<>();
    }

    @Test
    public void given_featureAEnabled_when_ContextSubBatchRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);

        // when
        wr.requireContext("foo");
        urls.addAll(wr.paths());
        wr.requireContext("bar");
        urls.addAll(wr.paths());

        final String content = getAllContentInOrder(urls);

        // then
        assertThat(
                content,
                stringContainsInOrder(newArrayList(
                        "content of lib.js",
                        "content of feature-a.js",
                        "content of feature-s.js",
                        "content of feature-b.js")));
    }

    @Test
    public void given_featureADisabled_when_ContextSubBatchRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);

        // when
        wr.requireContext("foo");
        urls.addAll(wr.paths());
        wr.requireContext("bar");
        urls.addAll(wr.paths());

        final String content = getAllContentInOrder(urls);

        // then
        assertThat(
                content,
                stringContainsInOrder(
                        newArrayList("content of feature-s.js", "content of lib.js", "content of feature-b.js")));
    }

    @Test
    public void given_featureAEnabled_when_FooAndBarRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);

        // when
        wr.requireContext("foo");
        wr.requireContext("bar");
        urls.addAll(wr.paths());

        final String content = getAllContentInOrder(urls);

        // then
        assertThat(
                content,
                stringContainsInOrder(newArrayList(
                        "content of lib.js",
                        "content of feature-a.js",
                        "content of feature-s.js",
                        "content of feature-b.js")));
    }

    @Test
    public void given_featureADisabled_when_FooAndBarRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);

        // when
        wr.requireContext("foo");
        wr.requireContext("bar");
        urls.addAll(wr.paths());

        final String content = getAllContentInOrder(urls);

        // then
        assertThat(
                content,
                stringContainsInOrder(
                        newArrayList("content of feature-s.js", "content of lib.js", "content of feature-b.js")));
    }

    @Test
    public void given_featureAEnabled_when_BarAndFooRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);

        // when
        wr.requireContext("bar");
        wr.requireContext("foo");
        urls.addAll(wr.paths());

        final String content = getAllContentInOrder(urls);

        // then
        assertThat(
                content,
                stringContainsInOrder(newArrayList(
                        "content of lib.js",
                        "content of feature-b.js",
                        "content of feature-s.js",
                        "content of feature-a.js")));
    }

    @Test
    public void given_featureADisabled_when_BarAndFooRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);

        // when
        wr.requireContext("bar");
        wr.requireContext("foo");
        urls.addAll(wr.paths());

        final String content = getAllContentInOrder(urls);

        // then
        assertThat(
                content,
                stringContainsInOrder(
                        newArrayList("content of lib.js", "content of feature-b.js", "content of feature-s.js")));
    }

    private String getAllContentInOrder(final List<String> urls) {
        return urls.stream().map(wr::getContent).collect(Collectors.joining("\n"));
    }
}
