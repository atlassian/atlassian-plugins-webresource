package com.atlassian.plugin.webresource.integration.special;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.doReturn;

import static com.atlassian.plugin.webresource.impl.config.Config.DISABLE_PERFORMANCE_TRACKING;
import static com.atlassian.plugin.webresource.impl.config.Config.ENABLE_BUNDLE_HASH_VALIDATION;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchSubsetUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;

public class TestSuperbatchAcrossPhases {
    static final String ATLASSIAN_CACHES_ENABLED_PROPERTY = "atlassian.disable.caches";

    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    private WebResource wr;
    private WebResourceAssembler assembler;

    @Before
    public void setUp() {
        System.setProperty(ENABLE_BUNDLE_HASH_VALIDATION, FALSE.toString());
        System.setProperty(ATLASSIAN_CACHES_ENABLED_PROPERTY, FALSE.toString());
        System.setProperty(DISABLE_PERFORMANCE_TRACKING, TRUE.toString());

        wr = new WebResourceImpl();
        wr.configure(config -> {
                    doReturn(true).when(config).isContextBatchingEnabled();
                    doReturn(true).when(config).isSuperBatchingEnabled();
                    doReturn(true).when(config).isWebResourceBatchingEnabled();
                })
                .addToSuperbatch("test.atlassian:first-feature", "test.atlassian:different-thing")
                .plugin("test.atlassian")
                .webResource("first-feature")
                .context("first")
                .context("all")
                .resource("first-1.js")
                .resource("first-2.js")
                .resource("first-1.css")
                .resource("first-2.css")
                .webResource("second-feature")
                .context("second")
                .context("all")
                .resource("second-1.css")
                .resource("second-1.js")
                .resource("second-2.css")
                .resource("second-2.js")
                .webResource("third-feature")
                .dependency("test.atlassian:first-feature")
                .dependency("test.atlassian:second-feature")
                .context("third")
                .context("all")
                .resource("third-1.css")
                .resource("third-1.js")
                .webResource("fourth-feature")
                .dependency("test.atlassian:first-feature")
                .dependency("test.atlassian:second-feature")
                .context("fourth")
                .context("all")
                .resource("fourth-1.css")
                .resource("fourth-1.js")
                .webResource("fifth-feature")
                .dependency("test.atlassian:first-feature")
                .dependency("test.atlassian:second-feature")
                .context("fifth")
                .context("all")
                .resource("fifth-1.css")
                .resource("fifth-1.js")
                .webResource("different-thing")
                .resource("different.css")
                .resource("different.js")
                .end();
        assembler = wr.getPageBuilderService().assembler();
    }

    @Test
    public void
            given_superbatchInRequirePhase_when_requiringOverlappingContextsInRequirePhase_then_noSuperbatchSubtractions() {
        // given
        assembler.resources().requireContext(ResourcePhase.REQUIRE, "third");
        assembler.resources().requireContext(ResourcePhase.REQUIRE, "fourth");
        assembler.resources().requireSuperbatch(ResourcePhase.REQUIRE);

        // when
        final List<String> urls = wr.paths();

        // then
        assertThat(
                urls,
                contains(
                        containsString(contextBatchUrl("_super", "css")),
                        containsString(contextBatchUrl("third,fourth,-_super", "css")),
                        containsString(contextBatchUrl("_super", "js")),
                        containsString(contextBatchUrl("third,fourth,-_super", "js"))));
    }

    @Test
    public void
            given_superbatchInDeferPhase_when_requiringOverlappingContextsInRequirePhase_then_allRequirePhaseResourcesSubtractedFromSuperbatch() {
        // given
        assembler.resources().requireContext(ResourcePhase.REQUIRE, "third");
        assembler.resources().requireContext(ResourcePhase.REQUIRE, "fourth");
        assembler.resources().requireSuperbatch(ResourcePhase.DEFER);

        // when
        final List<String> urls = wr.paths();

        // then
        assertThat(
                urls,
                contains(
                        containsString(contextBatchUrl("third,fourth", "css")),
                        containsString(contextBatchUrl("third,fourth", "js")),
                        // note: exclusions are in alphabetical order
                        containsString(contextBatchSubsetUrl("_super", asList("fourth", "third"), "css")),
                        containsString(contextBatchSubsetUrl("_super", asList("fourth", "third"), "js"))));
    }

    @Test
    public void
            given_superbatchInDeferPhase_when_requiringOverlappingWebResourcesInRequirePhase_then_allRequirePhaseResourcesSubtractedFromSuperbatch() {
        // given
        assembler.resources().requireWebResource(ResourcePhase.REQUIRE, "test.atlassian:third-feature");
        assembler.resources().requireWebResource(ResourcePhase.REQUIRE, "test.atlassian:fourth-feature");
        assembler.resources().requireSuperbatch(ResourcePhase.DEFER);

        // when
        final List<String> urls = wr.paths();

        // then

        // todo: optimise to remove as few keys as necessary.
        //       the only key in superbatch is `first-feature`, so no need to subtract higher subtrees.
        final List<String> expectedSuperbatchSubtractions =
                asList("test.atlassian:fourth-feature", "test.atlassian:third-feature");

        assertThat(
                urls,
                contains(
                        containsString(webResourceBatchUrl("test.atlassian:first-feature", "css")),
                        containsString(webResourceBatchUrl("test.atlassian:second-feature", "css")),
                        containsString(webResourceBatchUrl("test.atlassian:third-feature", "css")),
                        containsString(webResourceBatchUrl("test.atlassian:fourth-feature", "css")),
                        containsString(webResourceBatchUrl("test.atlassian:first-feature", "js")),
                        containsString(webResourceBatchUrl("test.atlassian:second-feature", "js")),
                        containsString(webResourceBatchUrl("test.atlassian:third-feature", "js")),
                        containsString(webResourceBatchUrl("test.atlassian:fourth-feature", "js")),
                        containsString(contextBatchSubsetUrl("_super", expectedSuperbatchSubtractions, "css")),
                        containsString(contextBatchSubsetUrl("_super", expectedSuperbatchSubtractions, "js"))));
    }
}
