package com.atlassian.plugin.webresource;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

public class AlwaysFalseCondition implements UrlReadingCondition {
    @Override
    public void init(Map<String, String> params) throws PluginParseException {}

    @Override
    public void addToUrl(UrlBuilder urlBuilder) {
        urlBuilder.addToQueryString("always-false", "true");
    }

    @Override
    public boolean shouldDisplay(QueryParams params) {
        return !"true".equals(params.get("always-false"));
    }
}
