package com.atlassian.plugin.webresource.impl.support.factory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.impl.support.InitialContent;
import com.atlassian.sourcemap.ReadableSourceMap;

import static java.util.Objects.nonNull;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.webresource.mocks.MockBuilder.builder;
import static com.atlassian.sourcemap.ReadableSourceMap.toWritableSourceMap;
import static com.atlassian.sourcemap.ReadableSourceMapImpl.fromSource;

@RunWith(MockitoJUnitRunner.Silent.class)
public class InitialMinifiedContentBuilderTest {
    private static final String JAVASCRIPT_ALTERNATE_MINIFIED_FILE_NAME = "/x/y/z.min.js";
    private static final String JAVASCRIPT_FILE_NAME = "/x/y/z.js";

    private static final String JAVASCRIPT_MINIFIED_INPUT = "var test='Hello World';console.log(test);";
    private static final String JAVASCRIPT_MINIFIED_FILE_MAP_NAME = "/x/y/z.min.js.map";
    private static final String JAVASCRIPT_MINIFIED_FILE_MAP_OUTPUT =
            "{ version : 3, file: '/x/y/z.js', sourceRoot : '', sources: ['/x/y/z.js'], names: ['statement', 'window.console.log'], mappings: 'AAgBC,SAAQ,CAAEA' }";
    private static final String JAVASCRIPT_MINIFIED_FILE_NAME = "/x/y/z-min.js";

    @Mock
    private Bundle bundle;

    @Mock
    private Globals globals;

    @Mock
    private Config config;

    @Mock
    private Router router;

    @Mock
    private Resource resource;

    @Spy
    private final InitialSourceContentBuilder initialSourceContentBuilder = new InitialSourceContentBuilder();

    private InitialMinifiedContentBuilder underTest;

    @Before
    public void setUp() {
        builder().withMockedGlobals(globals).router(router).config(config);
        underTest = new InitialMinifiedContentBuilder(globals, initialSourceContentBuilder);
    }

    /**
     * WHEN resource = /x/y/z.js
     * AND global minification is not enabled
     * AND there is file for minified path. E.g: /x/y/z-min.js
     * THEN return resource minified version.
     */
    @Test
    public void
            when_JavaScriptFileIsNotMinified_And_GlobalMinificationIsNotEnabled_And_ThereIsMinifiedVersionForMinifiedPath_Then_ReturnMinifiedContent() {
        final Callable mocker = () -> builder()
                .withMockedConfig(config)
                .minificationEnabled(true)
                .and()
                .withMockedBundle(bundle)
                .minificationEnabled(true)
                .and()
                .withMockedResource(resource)
                .parent(bundle)
                .path(JAVASCRIPT_FILE_NAME)
                .streamFor(JAVASCRIPT_MINIFIED_FILE_NAME, JAVASCRIPT_MINIFIED_INPUT);
        testContent(mocker, null);
    }

    /**
     * WHEN resource = /x/y/z.js
     * AND global minification is not enabled
     * AND there is file for alternate minified path. E.g: /x/y/z.min.js
     * THEN return resource minified version.
     */
    @Test
    public void
            when_JavaScriptFileIsNotMinified_And_GlobalMinificationIsNotEnabled_And_ThereIsMinifiedVersionForAlternateMinifiedPath_Then_ReturnMinifiedContent() {
        final Callable mocker = () -> builder()
                .withMockedConfig(config)
                .minificationEnabled(true)
                .and()
                .withMockedBundle(bundle)
                .minificationEnabled(true)
                .and()
                .withMockedResource(resource)
                .parent(bundle)
                .path(JAVASCRIPT_FILE_NAME)
                .streamFor(JAVASCRIPT_ALTERNATE_MINIFIED_FILE_NAME, JAVASCRIPT_MINIFIED_INPUT);
        testContent(mocker, null);
    }

    /**
     * WHEN resource = /x/y/z.js
     * AND global minification is not enabled
     * AND there is file for minified path. E.g: /x/y/z-min.js
     * THEN return minified path. E.g: /x/y/z-min.js
     */
    @Test
    public void
            when_JavaScriptFileIsNotMinified_And_GlobalMinificationIsNotEnabled_And_ThereIsMinifiedVersionForMinifiedPath_Then_ReturnMinifiedPath() {
        final Callable mocker = () -> builder()
                .withMockedConfig(config)
                .minificationEnabled(true)
                .and()
                .withMockedBundle(bundle)
                .minificationEnabled(true)
                .and()
                .withMockedResource(resource)
                .parent(bundle)
                .path(JAVASCRIPT_FILE_NAME)
                .streamFor(JAVASCRIPT_ALTERNATE_MINIFIED_FILE_NAME, JAVASCRIPT_MINIFIED_INPUT);
        testPath(mocker, JAVASCRIPT_MINIFIED_FILE_NAME);
    }

    /**
     * WHEN resource = /x/y/z.js
     * AND global minification is not enabled
     * AND there is file for alternate minified path. E.g: /x/y/z.min.js
     * THEN return minified path. E.g: /x/y/z-min.js
     */
    @Test
    public void
            when_JavaScriptFileIsNotMinified_And_GlobalMinificationIsNotEnabled_And_ThereIsMinifiedVersionForAlternateMinifiedPath_Then_ReturnMinifiedPath() {
        final Callable mocker = () -> builder()
                .withMockedConfig(config)
                .minificationEnabled(true)
                .and()
                .withMockedBundle(bundle)
                .minificationEnabled(true)
                .and()
                .withMockedResource(resource)
                .parent(bundle)
                .path(JAVASCRIPT_FILE_NAME)
                .streamFor(JAVASCRIPT_ALTERNATE_MINIFIED_FILE_NAME, JAVASCRIPT_MINIFIED_INPUT);
        testPath(mocker, JAVASCRIPT_MINIFIED_FILE_NAME);
    }

    /**
     * WHEN resource = /x/y/z.js
     * AND global minification is not enabled
     * AND there is not any minified version.
     * THEN return minified path. E.g: /x/y/z-min.js
     */
    @Test
    public void
            when_JavaScriptFileIsNotMinified_And_GlobalMinificationIsNotEnabled_And_ThereIsNotAnyMinifiedVersion_Then_ReturnAlternateMinifiedPath() {
        final Callable mocker = () -> builder()
                .withMockedConfig(config)
                .minificationEnabled(true)
                .and()
                .withMockedBundle(bundle)
                .minificationEnabled(true)
                .and()
                .withMockedResource(resource)
                .parent(bundle)
                .path(JAVASCRIPT_FILE_NAME);
        testPath(mocker, JAVASCRIPT_ALTERNATE_MINIFIED_FILE_NAME);
    }

    @Test
    public void
            when_JavaScriptFileIsNotMinified_And_GlobalMinificationIsNotEnabled_And_ThereIsNotAnyPreBuildSource_Then_ReturnSourceMapForRawContent() {
        // given
        final InitialContent mockedInitialContent = mock(InitialContent.class);
        when(mockedInitialContent.getPath()).thenReturn(of(JAVASCRIPT_FILE_NAME));
        when(initialSourceContentBuilder.build()).thenReturn(mockedInitialContent);

        builder()
                .withMockedConfig(config)
                .minificationEnabled(true)
                .and()
                .withMockedBundle(bundle)
                .minificationEnabled(true)
                .and()
                .withMockedResource(resource)
                .parent(bundle)
                .path(JAVASCRIPT_FILE_NAME)
                .streamFor(JAVASCRIPT_MINIFIED_FILE_MAP_NAME, JAVASCRIPT_MINIFIED_FILE_MAP_OUTPUT);
        // when
        final InitialContent actualInitialContent =
                underTest.withPath(resource).withSourceMap(resource).build();
        // then
        final String expectedSourceMapRawContent =
                "{ version : 3, file: '/x/y/z.js', sourceRoot : '', sources: ['/x/y/z.js'], names: ['statement', 'window.console.log'], mappings: 'AAgBC,SAAQ,CAAEA' }";
        final ReadableSourceMap expectedSourceMap = fromSource(expectedSourceMapRawContent);

        assertTrue(actualInitialContent.getSourceMap().isPresent());
        actualInitialContent
                .getSourceMap()
                .ifPresent(actualSourceMap -> assertEquals(
                        toWritableSourceMap(expectedSourceMap).generateForHumans(),
                        toWritableSourceMap(actualSourceMap).generateForHumans()));
    }

    /**
     * WHEN resource = /x/y/z.js
     * AND global minification is not enabled
     * AND there is minified version for minified path. E.g: /x/y/z-min.js
     * AND there is minified version generated by resource compiler.
     * THEN opt for minified content.
     */
    @Test
    public void
            when_JavaScriptFileIsNotMinified_And_GlobalMinificationIsNotEnabled_And_ThereIsMinifiedVersionForMinifiedPath_And_ThereIsFileGeneratedByResourceCompiler_Then_OptForMinifiedContent() {
        final Callable mocker = () -> builder()
                .withMockedConfig(config)
                .minificationEnabled(true)
                .and()
                .withMockedBundle(bundle)
                .minificationEnabled(true)
                .and()
                .withMockedResource(resource)
                .parent(bundle)
                .path(JAVASCRIPT_FILE_NAME)
                .streamFor(JAVASCRIPT_FILE_NAME, JAVASCRIPT_MINIFIED_INPUT)
                .streamFor(JAVASCRIPT_MINIFIED_FILE_NAME, JAVASCRIPT_MINIFIED_INPUT);
        final Callable verifier = () -> {
            verify(resource, never()).getStreamFor(JAVASCRIPT_ALTERNATE_MINIFIED_FILE_NAME);
        };
        testContent(mocker, verifier);
    }

    /**
     * Executes a certain test and verify if the actual content is equals to the expected one.
     * @param mocker The {@link Callable} responsible for performing the mocking.
     * @param verifier The {@link Callable} responsible for performing extra verifications.
     */
    private void testContent(final Callable mocker, final Callable verifier) {
        // given
        if (nonNull(mocker)) {
            mocker.call();
        }

        // when
        final InitialMinifiedContentBuilder actualInitialContentBuilder = underTest.withContent(resource);
        final InitialContentTest actualInitialContent = new InitialContentTest(actualInitialContentBuilder.build());

        // then
        assertTrue(actualInitialContent.getContentAsString().isPresent());
        actualInitialContent
                .getContentAsString()
                .ifPresent(actualContent -> assertEquals(JAVASCRIPT_MINIFIED_INPUT, actualContent));

        if (nonNull(verifier)) {
            verifier.call();
        }
    }

    /**
     * Executes a certain test and verify if the actual path is equals to the expected one.
     * @param mocker The {@link Callable} responsible for performing the mocking.
     */
    private void testPath(final Callable mocker, final String expectedPath) {
        // given
        if (nonNull(mocker)) {
            mocker.call();
        }
        // when
        final InitialContent initialContent = underTest.withPath(resource).build();
        // then
        assertTrue(initialContent.getPath().isPresent());
        assertEquals(expectedPath, initialContent.getPath().get());
    }

    /**
     * A task that returns a result. Implementors define a single method with no arguments called {@code call}.
     *
     * @since 5.0.0
     */
    private interface Callable {

        /**
         * Computes a result.
         */
        void call();
    }
}
