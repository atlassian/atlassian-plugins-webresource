package com.atlassian.plugin.webresource;

import java.util.Map;

import junit.framework.TestCase;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.helpers.url.ParamsComparator;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;

/**
 * Note, the previous version of comparator used lexicographical order of resource url to sort resources. But, as soon
 * as the urls for sub batches are the same except the querystring parts, it seems that using querystring only
 * should be sufficient to maintain the same order.
 */
public class TestResourceBatchComparator extends TestCase {
    private static final ParamsComparator PARAMS_COMPARATOR = new ParamsComparator();

    private static int compare(Map<String, String> a, Map<String, String> b) {
        return PARAMS_COMPARATOR.compare(a, b);
    }

    public void testResourceBatchComparatorFirstPriorityToUnconditionalResources() {
        assertTrue(compare(buildMap(Config.MEDIA_PARAM_NAME, "print"), buildMap()) > 0);
    }

    public void testResourceBatchComparatorFirstPriorityToUnconditionalResourcesOnly() {
        assertTrue(compare(
                        buildMap("content-type", "application/xml", Config.CACHE_PARAM_NAME, "no-cache"),
                        buildMap(Config.MEDIA_PARAM_NAME, "print"))
                < 0);
    }

    public void testResourceBatchComparatorFewerConditionsComesFirst() {
        assertTrue(compare(
                        buildMap(Config.MEDIA_PARAM_NAME, "blah", Config.CACHE_PARAM_NAME, "true"),
                        buildMap(Config.MEDIA_PARAM_NAME, "print"))
                > 0);
    }

    public void testResourceBatchComparatorOrdersByUrlAmongstUnconditionals() {
        assertTrue(compare(buildMap(Config.MEDIA_PARAM_NAME, "a"), buildMap(Config.MEDIA_PARAM_NAME, "b")) == 0);
    }

    public void testResourceBatchComparatorCacheParamVsLocale() {
        assertTrue(compare(buildMap(Config.CACHE_PARAM_NAME, "true", "locale", "en"), buildMap("locale", "en")) > 0);
    }

    public void testResourceBatchComparatorCacheParam() {
        assertTrue(compare(buildMap(Config.CACHE_PARAM_NAME, "true"), buildMap()) > 0);
    }
}
