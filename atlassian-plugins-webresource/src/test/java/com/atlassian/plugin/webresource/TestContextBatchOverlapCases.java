package com.atlassian.plugin.webresource;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyIterable;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchSubsetUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;

public class TestContextBatchOverlapCases {
    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    private WebResourceImpl wr;

    @Before
    public void setUp() throws Exception {
        wr = new WebResourceImpl();
        System.setProperty("atlassian.darkfeature.atlassian.webresource.performance.tracking.disable", "true");
    }

    @Test
    public void when_ContextSubsetExcluded_then_shouldSubtractFromContextUrl() {
        // same as testSubtractTwoBatchesFromOperand
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .context("foo")
                .resource("a1.css")
                .webResource("b")
                .context("foo")
                .resource("b1.css")
                .webResource("c")
                .context("foo")
                .resource("c1.css")
                .webResource("d")
                .context("foo")
                .resource("d1.css")
                .end();

        wr.exclude("plugin:b", "plugin:d");
        wr.requireContext("foo");

        final List<String> urls = wr.paths();
        assertThat(urls, matches(contextBatchSubsetUrl("foo", asList("plugin:b", "plugin:d"), "css")));
    }

    @Test
    public void when_ContextDirectDependencyIsExcluded_then_shouldSubtractFromContextUrl() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .context("foo")
                .resource("a1.js")
                .webResource("b")
                .context("foo")
                .resource("b1.js")
                .end();

        wr.exclude("plugin:b");
        wr.requireContext("foo");

        final List<String> urls = wr.paths();
        assertThat(urls, matches(contextBatchSubsetUrl("foo", asList("plugin:b"), "js")));
    }

    @Test
    public void when_ContextSubDependencyIsExcluded_then_shouldSubtractFromContextUrl() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .dependency("plugin:b")
                .context("foo")
                .resource("a1.js")
                .webResource("b")
                .resource("b1.js")
                .end();

        wr.exclude("plugin:b");
        wr.requireContext("foo");

        final List<String> urls = wr.paths();
        assertThat(urls, matches(contextBatchSubsetUrl("foo", asList("plugin:b"), "js")));
    }

    @Test
    public void when_ExcludingUncommonDependency_then_shouldSubtractFromRelevantContextUrls() {
        // given
        generateTwoContextsWithOverlappingDependency().end();

        // when
        wr.exclude("plugin:a");
        wr.requireContext("foo");
        wr.requireContext("bar");
        wr.requireResource("plugin:f");
        final List<String> urlsWithFooExclusion = wr.paths();

        wr.reset();

        wr.exclude("plugin:e");
        wr.requireContext("foo");
        wr.requireContext("bar");
        wr.requireResource("plugin:f");
        final List<String> urlsWithBarExclusion = wr.paths();

        // then
        assertThat(
                urlsWithFooExclusion,
                matches(
                        contextBatchSubsetUrl(asList("foo", "bar"), asList("plugin:a"), "js"),
                        webResourceBatchUrl("plugin:f", "js")));
        assertThat(
                urlsWithBarExclusion,
                matches(
                        contextBatchSubsetUrl(asList("foo", "bar"), asList("plugin:e"), "js"),
                        webResourceBatchUrl("plugin:f", "js")));
    }

    @Test
    public void when_ExcludingCommonContext_then_shouldSubtractFromBothContextUrls() {
        // given
        generateTwoContextsWithOverlappingDependency().end();

        // when
        wr.excludeContext("foobar");
        wr.requireContext("foo");
        wr.requireContext("bar");
        wr.requireResource("plugin:f");

        final List<String> urls = wr.paths();

        // then
        assertThat(
                urls,
                matches(
                        contextBatchSubsetUrl(asList("foo", "bar"), asList("foobar"), "js"),
                        webResourceBatchUrl("plugin:f", "js")));
    }

    @Test
    public void when_ExcludingCommonDependencyBetweenTwoContexts_then_shouldSubtractFromBothContextUrls() {
        // given
        generateTwoContextsWithOverlappingDependency().end();

        // when
        wr.exclude("plugin:c");
        wr.requireContext("foo");
        wr.requireContext("bar");
        wr.requireResource("plugin:f");

        final List<String> urls = wr.paths();

        // then
        assertThat(
                urls,
                matches(
                        contextBatchSubsetUrl(asList("foo", "bar"), asList("plugin:c"), "js"),
                        webResourceBatchUrl("plugin:f", "js")));
    }

    @Test
    public void when_BarContextExcludedFromFoo_then_contextUrlShouldSubtractBar() {
        // given
        generateTwoContextsWithOverlappingDependency().end();

        // when
        wr.requireContext("foo");
        wr.excludeContext("bar");

        final List<String> urls = wr.paths();

        // then
        assertThat(urls, matches(contextBatchSubsetUrl(asList("foo"), asList("bar"), "js")));
    }

    @Test
    public void when_FooContextExcludedFromBar_then_contextUrlShouldSubtractFoo() {
        // given
        generateTwoContextsWithOverlappingDependency().end();

        // when
        wr.excludeContext("foo");
        wr.requireContext("bar");

        final List<String> urls = wr.paths();

        // then
        assertThat(urls, matches(contextBatchSubsetUrl(asList("bar"), asList("foo"), "js")));
    }

    @Test
    public void when_FooContextExcludedFromFooSubcontext_then_noUrlShouldBeGenerated() {
        // given
        generateTwoContextsWithOverlappingDependency().end();

        // when
        wr.excludeContext("foo");
        wr.requireContext("mini-foo");

        final List<String> urls = wr.paths();

        // then
        assertThat(urls, is(emptyIterable()));
    }

    @Test
    public void when_FooContextExcludedFromSingleResourceInFoo_then_noUrlShouldBeGenerated() {
        // given
        generateTwoContextsWithOverlappingDependency().end();

        // when
        wr.excludeContext("foo");
        wr.requireContext("plugin:sub-a");

        final List<String> urls = wr.paths();

        // then
        assertThat(urls, is(emptyIterable()));
    }

    @Test
    public void given_ContextSubsetExcludedPreviously_then_shouldSubtractFromUrl() {
        // given
        generateTwoContextsWithOverlappingDependency().end();

        // when
        wr.requireContext("mini-bar");
        final List<String> minibarUrls = wr.paths();

        wr.requireContext("bar");
        final List<String> urls = wr.paths();

        // then
        assertThat(urls, matches(contextBatchSubsetUrl(asList("bar"), asList("mini-bar"), "js")));
    }

    @Test
    public void given_SuperbatchIncludesOverlappingResource_then_shouldSubtractSuperbatchFromUrls() {
        // given
        generateTwoContextsWithOverlappingDependency()
                .configureFurther()
                .addToSuperbatch("plugin:c")
                .end();

        // when
        wr.requireContext("foo");
        wr.requireContext("bar");
        final List<String> urls = wr.paths();

        // then
        assertThat(
                urls,
                matches(
                        contextBatchUrl("_super", "js"),
                        // note: foo and bar could be joined here and that would actually be better.
                        // it doesn't happen yet because the legacy URL generation process treats
                        // superbatch specially rather than like a normal batch, so the algo in
                        // ResourceRequirer#includeResources and ResourceRequirer#addContextBatchDependencies
                        // can't discern web-resources that belong to the superbatch.
                        contextBatchSubsetUrl(asList("foo"), asList("_super"), "js"),
                        contextBatchSubsetUrl(asList("bar"), asList("_super"), "js")));
    }

    @Test
    public void given_InclusionsAreCompleteSubsetOfContext_when_ContextRequested_then_ShouldBeSingleContextBatchUrl() {
        // given
        final List<String> keys = asList("a", "b", "c", "d", "e", "f");
        WebResource.WebResourceChainDsl dsl = wr.configure().plugin("plugin");
        for (final String key : keys) {
            dsl = dsl.webResource(key).context("foo").resource(key + ".js");
        }
        dsl.end();

        // when
        wr.include("plugin:c", "plugin:a");
        wr.exclude("plugin:b", "plugin:d", "plugin:f");
        wr.requireContext("foo");

        final List<String> urls = wr.paths();

        // then
        assertThat(urls, matches(contextBatchSubsetUrl("foo", asList("plugin:b", "plugin:d", "plugin:f"), "js")));
    }

    private WebResource.WebResourceChainDsl generateTwoContextsWithOverlappingDependency() {
        return wr.configure()
                .plugin("plugin")
                .webResource("a")
                .dependency(":sub-a")
                .context("foo")
                .context("mini-foo")
                .resource("a1.js")
                .webResource("b")
                .dependency(":sub-b")
                .context("foo")
                .resource("b1.js")
                .webResource("c")
                .dependency(":sub-c")
                .context("foo")
                .context("bar")
                .context("foobar")
                .resource("c2.js")
                .webResource("d")
                .dependency(":sub-d")
                .context("bar")
                .resource("d1.js")
                .webResource("e")
                .dependency(":sub-e")
                .context("bar")
                .context("mini-bar")
                .resource("e1.js")
                .webResource("f")
                .resource("f1.js")
                .webResource("sub-a")
                .resource("sub-a.js")
                .webResource("sub-b")
                .resource("sub-b.js")
                .webResource("sub-c")
                .resource("sub-c.js")
                .webResource("sub-d")
                .resource("sub-d.js")
                .webResource("sub-e")
                .resource("sub-e.js");
    }
}
