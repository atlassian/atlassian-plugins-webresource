package com.atlassian.plugin.webresource.assembler;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;

import static java.util.Collections.emptyMap;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import static com.atlassian.plugin.webresource.TestUtils.createNamedHashes;
import static com.atlassian.plugin.webresource.TestUtils.createWebResourceTransformation;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.Types.CSS;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.UrlAdderFunction;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.assertPrefixedUrlIs;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.assertResolvedData;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.assertResolvedResources;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.c;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.getHtmlTags;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.m;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.q;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.setOf;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.superbatch;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.u;
import static com.atlassian.plugin.webresource.assembler.DefaultWebResourceSetBuilder.WEB_RESOURCE_MANAGER_RESOURCE;
import static com.atlassian.plugin.webresource.impl.config.Config.SUPER_BATCH_CONTEXT_KEY;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE;

public class TestWebResourceAssembler {
    private AssemblerTestFixture f;

    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    @Before
    public void setUp() throws Exception {
        f = new AssemblerTestFixture();
        System.setProperty("atlassian.darkfeature.atlassian.webresource.performance.tracking.disable", "true");
    }

    @Test
    public void testSingleResourceNoDeps() {
        f.mockPlugin("test.atlassian:nodep-resource").defRes();
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:nodep-resource");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:nodep-resource"));
    }

    @Test
    public void testSingleResourceNoDepsWithMediaParams() {
        f.mockPlugin("test.atlassian:nodep-resource").res("a.css").paramRes("b.css", ImmutableMap.of("media", "print"));
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:nodep-resource");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(
                resources,
                m("test.atlassian:nodep-resource", "css"),
                m("test.atlassian:nodep-resource", "css", "media=print"));
    }

    @Test
    public void testSingleResourceWithDeps() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-a"), m("test.atlassian:resource-b"));
    }

    @Test
    public void testSingleResourceMultiDeps() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-c")
                .defRes()
                .deps("test.atlassian:resource-a", "test.atlassian:resource-b");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-c");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(
                resources,
                m("test.atlassian:resource-a"),
                m("test.atlassian:resource-b"),
                m("test.atlassian:resource-c"));
    }

    @Test
    public void testMultiResourceMultiDeps() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-c").defRes().deps("test.atlassian:resource-a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource(REQUIRE, "test.atlassian:resource-b");
        assembler.resources().requireWebResource(REQUIRE, "test.atlassian:resource-c");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(
                resources,
                m("test.atlassian:resource-a"),
                m("test.atlassian:resource-b"),
                m("test.atlassian:resource-c"));
    }

    @Test
    public void testMultiResourceMultiDepsOrdering() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-c").defRes().deps("test.atlassian:resource-a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-c");
        assembler.resources().requireWebResource("test.atlassian:resource-b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(
                resources,
                m("test.atlassian:resource-a"),
                m("test.atlassian:resource-c"),
                m("test.atlassian:resource-b"));
    }

    @Test
    public void testCascadeResourceDeps() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-c").defRes().deps("test.atlassian:resource-b");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-c");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(
                resources,
                m("test.atlassian:resource-a"),
                m("test.atlassian:resource-b"),
                m("test.atlassian:resource-c"));
    }

    @Test
    public void testSingleContext() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a"));
    }

    @Test
    public void testMultiContext() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        assembler.resources().requireContext("context.b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a"), c("context.b"));
    }

    @Test
    public void testMultiContextOrdering() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.b");
        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.b"), c("context.a"));
    }

    @Test
    public void testMultiContextDependencyOverlap() {
        f.mockPlugin("test.atlassian:resource-base").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-a")
                .defRes()
                .deps("test.atlassian:resource-base")
                .ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b")
                .defRes()
                .deps("test.atlassian:resource-base")
                .ctx("context.b");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        assembler.resources().requireContext("context.b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a", "context.b"));
    }

    @Test
    public void testMultiContextDependencyOverlapMultipleDrain() {
        f.mockPlugin("test.atlassian:base").defRes();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a").deps("test.atlassian:base");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b").deps("test.atlassian:base");
        WebResourceAssembler assembler = f.create();
        assembler.resources().requireContext("context.a");

        assembler.assembled().drainIncludedResources();
        assembler.resources().requireContext("context.b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.b", "-context.a"));
    }

    @Test
    public void testExcludeSingleResource() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        WebResourceAssembler assembler = f.create();

        assembler.resources().exclude(setOf("test.atlassian:resource-a"), setOf());

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeSingleResourceDependency() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().exclude(setOf("test.atlassian:resource-b"), setOf());

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeSingleResourceCascade() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().exclude(setOf("test.atlassian:resource-a"), setOf());

        assembler.resources().requireWebResource("test.atlassian:resource-b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-b"));
    }

    @Test
    public void testExcludeSingleContext() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().exclude(setOf(), setOf("context.a"));

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeSingleContextInWebResource() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().exclude(setOf(), setOf("context.a"));

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeSingleContextInWebResourceDependency() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b")
                .defRes()
                .deps("test.atlassian:resource-a")
                .ctx("context.a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().exclude(setOf(), setOf("context.a"));

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeSingleWebResourceInContext() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().exclude(setOf("test.atlassian:resource-a"), setOf());

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleResource() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleResourceDependency() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-b");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleResourceCascade() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-b"));
    }

    @Test
    public void testRememberSingleContext() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        assembler.assembled().drainIncludedResources();
        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleContextInWebResource() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleContextInWebResourceDependency() {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b")
                .defRes()
                .deps("test.atlassian:resource-a")
                .ctx("context.a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleWebResourceInContext() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-a").defRes();

        WebResourceAssembler assembler = f.create();
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, superbatch());
    }

    @Test
    public void testRememberSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-a").defRes();

        WebResourceAssembler assembler = f.create();
        assembler.assembled().drainIncludedResources();
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberModuleInSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-a").defRes();

        WebResourceAssembler assembler = f.create();
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberModuleInSuperbatchDependency() {
        f.mockSuperbatch("test.atlassian:resource-b");

        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        WebResourceAssembler assembler = f.create();

        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberContextInSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");

        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testContextAndSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        // Added -_super in v.3.3
        assertResolvedResources(resources, superbatch(), c("context.a", "-_super"));
    }

    @Test
    public void testContextAndSuperbatchWithMediaParams() {
        f.mockSuperbatch("test.atlassian:resource-super");
        f.mockPlugin("test.atlassian:resource-super")
                .res("res-one.css")
                .paramRes("res-two.css", ImmutableMap.of("media", "print"));
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        // Added _super in v. 3.3
        assertResolvedResources(resources, superbatch(CSS), superbatch(CSS, "?media=print"), c("context.a", "-_super"));
    }

    @Test
    public void testContextAndPartiallyOverlappingSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, superbatch(), c("context.a", "-_super"));
    }

    @Test
    public void testContextAndCompletelyOverlappingSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, superbatch());
    }

    @Test
    public void testContextPartiallyInSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");

        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a", "-_super"));
    }

    @Test
    public void testExcludeSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");

        f.mockPlugin("test.atlassian:resource-a").defRes();

        WebResourceAssembler assembler = f.create();
        assembler.resources().exclude(setOf(), setOf(SUPER_BATCH_CONTEXT_KEY));
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeModuleInSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");

        f.mockPlugin("test.atlassian:resource-a").defRes();

        WebResourceAssembler assembler = f.create();
        assembler.resources().exclude(setOf(), setOf(SUPER_BATCH_CONTEXT_KEY));

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeModuleInSuperbatchDependency() {
        f.mockSuperbatch("test.atlassian:resource-b");
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();

        assembler.resources().exclude(setOf(), setOf(SUPER_BATCH_CONTEXT_KEY));

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeContextInSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");

        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().exclude(setOf(), setOf(SUPER_BATCH_CONTEXT_KEY));

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeContextPartiallyInSuperbatch() {
        f.mockSuperbatch("test.atlassian:resource-a");

        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().exclude(setOf(), setOf(SUPER_BATCH_CONTEXT_KEY));

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a", "-_super"));
    }

    @Test
    public void testSameResourceExcludedAndIncluded() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-c").defRes().ctx("context.c");

        WebResourceAssembler assembler = f.create();
        assembler.resources().requireWebResource("test.atlassian:resource-a");
        assembler.resources().requireContext("context.b");
        assembler.resources().requireContext("context.c");
        assembler.resources().exclude(setOf("test.atlassian:resource-a"), setOf("context.b"));

        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.c"));
    }

    @Test
    public void testNotIncludeEmptyWebResourceBatch() {
        f.mockPlugin("app:a").defRes();
        WebResourceAssembler assembler = f.create();
        assembler.resources().requireContext("a");
        assembler.resources().exclude(setOf(), setOf("a"));
        WebResourceSet resources = assembler.assembled().drainIncludedResources();
        assertResolvedResources(resources);
    }

    @Test
    public void testNotIncludeEmptyContextBatch() {
        WebResourceAssembler assembler = f.create();
        assembler.resources().requireWebResource("a");
        assembler.resources().exclude(setOf("a"), setOf());
        WebResourceSet resources = assembler.assembled().drainIncludedResources();
        assertResolvedResources(resources);
    }

    @Test
    public void testSingleResourceUnbatched() {
        AssemblerTestFixture f = new AssemblerTestFixture(false, false);
        f.mockPlugin("test.atlassian:a").res("a.js", "b.js");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, u("test.atlassian:a/a"), u("test.atlassian:a/b"));
    }

    @Test
    public void testSingleResourceUnbatchedMultipleTimes() {
        AssemblerTestFixture f = new AssemblerTestFixture(false, false);
        f.mockPlugin("test.atlassian:a").res("a.js", "b.js");
        WebResourceAssembler assembler = f.create();
        assembler.resources().requireWebResource("test.atlassian:a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources);
    }

    @Test
    public void testCloneBasic() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        WebResourceAssembler assembler = f.create();

        WebResourceAssembler clone = assembler.copy();
        clone.resources().requireContext("context.a");
        WebResourceSet resources = clone.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a"));
    }

    @Test
    public void testCloneWithSharedResource() {
        f.mockPlugin("test.atlassian:base").defRes();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a").deps("test.atlassian:base");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b").deps("test.atlassian:base");
        DefaultWebResourceAssemblerFactory factory = f.factory();
        WebResourceAssembler assembler = factory.create().build();
        assembler.resources().requireContext("context.a");
        assembler.assembled().drainIncludedResources();
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));

        WebResourceAssembler clone = assembler.copy();
        clone.resources().requireContext("context.b");
        WebResourceSet resources = clone.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.b", "-context.a"));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(2));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testCloneWithSharedResourceDrainAfterClone() {
        f.mockPlugin("test.atlassian:base").defRes();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a").deps("test.atlassian:base");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b").deps("test.atlassian:base");
        DefaultWebResourceAssemblerFactory factory = f.factory();
        WebResourceAssembler assembler = factory.create().build();
        assembler.resources().requireContext("context.a");

        WebResourceAssembler clone = assembler.copy();
        clone.assembled().drainIncludedResources();
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));

        clone.resources().requireContext("context.b");
        WebResourceSet resources = clone.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.b", "-context.a"));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(2));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testCloneMultipleTimesProducesSameResult() {
        f.mockPlugin("test.atlassian:base").defRes();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a").deps("test.atlassian:base");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b").deps("test.atlassian:base");
        DefaultWebResourceAssemblerFactory factory = f.factory();
        WebResourceAssembler assembler = factory.create().build();
        assembler.resources().requireContext("context.a");

        WebResourceAssembler clone1 = assembler.copy();
        WebResourceSet commonResources1 = clone1.assembled().drainIncludedResources();
        assertResolvedResources(commonResources1, c("context.a"));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));

        clone1.resources().requireContext("context.b");
        WebResourceSet resources1 = clone1.assembled().drainIncludedResources();

        assertResolvedResources(resources1, c("context.b", "-context.a"));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(2));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));

        WebResourceAssembler clone2 = assembler.copy();
        WebResourceSet commonResources2 = clone2.assembled().drainIncludedResources();
        assertResolvedResources(commonResources2, c("context.a"));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(2));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));

        clone2.resources().requireContext("context.b");
        WebResourceSet resources2 = clone2.assembled().drainIncludedResources();

        assertResolvedResources(resources2, c("context.b", "-context.a"));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(2));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testImplicitAutoIncludeFrontendRuntimeModules() {
        final String wrmRequireKey = WEB_RESOURCE_MANAGER_RESOURCE.getKey();
        f.mockPlugin(wrmRequireKey).res("wrm.js");
        f.mockPlugin("plug1:wr1").defRes();
        f.mockPlugin("plug2:wr2").defRes().deps(wrmRequireKey);

        final WebResourceAssembler assembler = f.factory().create().build();

        WebResourceAssembler withNoInteractives = assembler.copy();
        withNoInteractives.resources().requireWebResource(REQUIRE, "plug1:wr1");
        WebResourceSet assembledNoInteractive = withNoInteractives.assembled().drainIncludedResources();
        assertResolvedResources(assembledNoInteractive, m("plug1:wr1"));

        WebResourceAssembler withInteractives = assembler.copy();
        withInteractives.resources().requireWebResource(INTERACTION, "plug1:wr1");
        WebResourceSet assembledInteractive = withInteractives.assembled().drainIncludedResources();
        assertResolvedResources(assembledInteractive, m(wrmRequireKey), m("plug1:wr1"));
    }

    @Test
    public void testDisableAutoIncludeFrontendRuntimeModules() {
        final String wrmRequireKey = WEB_RESOURCE_MANAGER_RESOURCE.getKey();
        f.mockPlugin(wrmRequireKey).res("wrm.js");
        f.mockPlugin("plug1:wr1").defRes();
        f.mockPlugin("plug2:wr2").defRes().deps(wrmRequireKey);

        final WebResourceAssembler assembler =
                f.factory().create().autoIncludeFrontendRuntime(false).build();

        WebResourceAssembler withNoInteractives = assembler.copy();
        withNoInteractives.resources().requireWebResource(REQUIRE, "plug1:wr1");
        WebResourceSet assembledNoInteractive = withNoInteractives.assembled().drainIncludedResources();
        assertResolvedResources(assembledNoInteractive, m("plug1:wr1"));

        WebResourceAssembler withInteractives = assembler.copy();
        withInteractives.resources().requireWebResource(INTERACTION, "plug1:wr1");
        WebResourceSet assembledInteractive = withInteractives.assembled().drainIncludedResources();
        assertResolvedResources(
                assembledInteractive,
                // there should only be the single resource requested,
                // since auto-inclusions were disabled.
                m("plug1:wr1"));
    }

    @Test
    public void testExplicitInclusionOfFrontendRuntimeModulesWhenAutoIncludeDisabled() {
        final String wrmRequireKey = WEB_RESOURCE_MANAGER_RESOURCE.getKey();
        f.mockPlugin(wrmRequireKey).res("wrm.js");
        f.mockPlugin("plug1:wr1").defRes();
        f.mockPlugin("plug2:wr2").defRes().deps(wrmRequireKey);

        final WebResourceAssembler assembler =
                f.factory().create().autoIncludeFrontendRuntime(false).build();

        WebResourceAssembler withInteractives = assembler.copy();
        withInteractives.resources().requireWebResource(REQUIRE, "plug2:wr2");
        WebResourceSet assembledInteractive = withInteractives.assembled().drainIncludedResources();
        assertResolvedResources(assembledInteractive, m(wrmRequireKey), m("plug2:wr2"));
    }

    @Test
    public void testImplicitSyncbatchEnablement() {
        f.mockSyncbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();
        f.mockPlugin("plug2:wr2").defRes();

        final WebResourceSet set1 = f.create().assembled().drainIncludedResources();
        final String html = getHtmlTags(set1);

        assertThat(
                html,
                equalTo(
                        "<script data-wrm-key=\"_sync\" data-wrm-batch-type=\"context\" data-initially-rendered>\nplaceholder content\n</script>\n"));
    }

    @Test
    public void testExplicitSyncbatchDisablement() {
        f.mockSyncbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();
        f.mockPlugin("plug2:wr2").defRes();

        final WebResourceAssembler assembler =
                f.factory().create().includeSyncbatchResources(false).build();
        final WebResourceSet set1 = assembler.assembled().drainIncludedResources();
        final String html = getHtmlTags(set1);

        assertThat(html, isEmptyString());
    }

    @Test
    public void testIncludingSyncbatchResourcesWhenExplicitSyncbatchDisablement() {
        f.mockSyncbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();
        f.mockPlugin("plug2:wr2").defRes();

        final WebResourceAssembler assembler =
                f.factory().create().includeSyncbatchResources(false).build();
        assembler.resources().requireWebResource("plug1:wr1");
        final WebResourceSet set1 = assembler.assembled().drainIncludedResources();
        final String html = getHtmlTags(set1);

        AssemblerTestFixture.assertResolvedResources(set1, m("plug1:wr1"));
    }

    @Test
    public void testImplicitSuperbatchEnablement() {
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();

        WebResourceSet set1 = f.create().assembled().drainIncludedResources();

        AssemblerTestFixture.assertResolvedResources(set1, superbatch());
    }

    @Test
    public void when_SuperbatchIsEnabledByResourceBatchingConfiguration_then_ExplicitSuperbatchEnablementStillWorks() {
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();

        WebResourceAssembler assembler =
                f.factory().create().includeSuperbatchResources(true).build();
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        AssemblerTestFixture.assertResolvedResources(set1, superbatch());
    }

    @Test
    public void
            when_SuperbatchIsDisabledByResourceBatchingConfiguration_then_ExplicitSuperbatchEnablementDoesNotEnableIt() {
        f.mockSuperbatch(false, "plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();

        WebResourceAssembler assembler =
                f.factory().create().includeSuperbatchResources(true).build();
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        AssemblerTestFixture.assertResolvedResources(set1);
    }

    @Test
    public void testExplicitSuperbatchDisablement() {
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();

        WebResourceAssembler assembler =
                f.factory().create().includeSuperbatchResources(false).build();
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        AssemblerTestFixture.assertResolvedResources(set1);
    }

    @Test
    public void testExplicitSuperbatchDisablementWithContext() {
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();
        f.mockPlugin("plug1:wr2").defRes().ctx("context.a");

        WebResourceAssembler assembler =
                f.factory().create().includeSuperbatchResources(false).build();
        assembler.resources().requireContext("context.a");
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        AssemblerTestFixture.assertResolvedResources(set1, c("context.a"));
    }

    @Test
    public void testCdnSafeTransformResources() {
        WebResourceTransformation tx = createWebResourceTransformation(emptyMap(), createNamedHashes(), "js");

        f.prefixUrlsWithHash(true);
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes().transform(tx);
        f.mockPlugin("plug1:wr2").defRes().ctx("context.a").transform(tx);
        f.mockPlugin("plug1:wr3").defRes().transform(tx);

        WebResourceAssembler assembler =
                f.factory().create().includeSuperbatchResources(true).build();
        assembler.resources().requireContext("context.a").requireWebResource("plug1:wr3");
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        ArrayList<PluginUrlResource> resources = Lists.newArrayList(AssemblerTestFixture.getWebUrlResources(set1));
        assertEquals(3, resources.size());
        assertPrefixedUrlIs(resources.get(0), superbatch());
        // Added -_super in v. 3.3
        assertPrefixedUrlIs(resources.get(1), c("context.a", "-_super"));
        assertPrefixedUrlIs(resources.get(2), m("plug1:wr3"));
    }

    @Test
    public void testCdnSafeConditionResources() {
        f.prefixUrlsWithHash(true);
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1")
                .defRes()
                .condition(f.condition().shouldDisplay(true).build());
        f.mockPlugin("plug1:wr2")
                .defRes()
                .ctx("context.a")
                .condition(f.condition().shouldDisplay(true).build());
        f.mockPlugin("plug1:wr3")
                .defRes()
                .condition(f.condition().shouldDisplay(true).build());

        WebResourceAssembler assembler =
                f.factory().create().includeSuperbatchResources(true).build();
        assembler.resources().requireContext("context.a").requireWebResource("plug1:wr3");
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        ArrayList<PluginUrlResource> resources = Lists.newArrayList(AssemblerTestFixture.getWebUrlResources(set1));
        assertEquals(3, resources.size());
        assertPrefixedUrlIs(resources.get(0), superbatch());
        // Added -_super in v. 3.3
        assertPrefixedUrlIs(resources.get(1), c("context.a", "-_super"));
        assertPrefixedUrlIs(resources.get(2), m("plug1:wr3"));
    }

    @Test
    public void testCachingCondition2Resources() {
        final AtomicBoolean setParam = new AtomicBoolean(true);
        UrlAdderFunction urlAdderFunction = new UrlAdderFunction() {
            @Override
            public Void apply(UrlBuilder input) {
                if (setParam.get()) {
                    input.addToQueryString("added", "true");
                }
                return null;
            }
        };
        DecoratingCondition condition =
                f.condition().shouldDisplay(true).urlAdder(urlAdderFunction).build();
        f.mockPlugin("plug1:wr1").defRes().condition(condition);

        DefaultWebResourceAssemblerFactory factory = f.factory();
        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.resources().requireWebResource("plug1:wr1");
        WebResourceSet set1 = assembler1.assembled().drainIncludedResources();

        assertResolvedResources(set1, m("plug1:wr1", "js", q("added", "true")));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));

        // Set the condition to false. The query param should no longer be returned by a WebResourceAssembler.
        setParam.set(false);

        WebResourceAssembler assembler2 = factory.create().build();
        assembler2.resources().requireWebResource("plug1:wr1");
        WebResourceSet set2 = assembler2.assembled().drainIncludedResources();

        assertResolvedResources(set2, m("plug1:wr1", "js"));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testCachingTransformer2Resources() {
        final AtomicReference<String> changingValue = new AtomicReference<String>("original");
        Supplier<Map<String, String>> queryParams = new Supplier<Map<String, String>>() {
            @Override
            public Map<String, String> get() {
                return ImmutableMap.of("chicken", changingValue.get());
            }
        };
        Supplier<Iterable<String>> hashes = Suppliers.<Iterable<String>>ofInstance(createNamedHashes());
        WebResourceTransformation tx = createWebResourceTransformation(queryParams, hashes, "js");
        f.mockPlugin("plug1:wr1").defRes().transform(tx);

        DefaultWebResourceAssemblerFactory factory = f.factory();
        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.resources().requireWebResource("plug1:wr1");
        WebResourceSet set1 = assembler1.assembled().drainIncludedResources();

        assertResolvedResources(set1, m("plug1:wr1", "js", q("chicken", "original")));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));

        // change the query param value. Any assembler should now return the updated query param value
        changingValue.set("crispy");

        WebResourceAssembler assembler2 = factory.create().build();
        assembler2.resources().requireWebResource("plug1:wr1");
        WebResourceSet set2 = assembler2.assembled().drainIncludedResources();

        assertResolvedResources(set2, m("plug1:wr1", "js", q("chicken", "crispy")));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testCachingStaticTransfomers() {
        final AtomicReference<String> changingValue = new AtomicReference<String>("original");
        f.mockPlugin("plug1:wr1").defRes();
        StaticTransformers changingTransformers = new StaticTransformers() {

            @Override
            public void loadTwoPhaseProperties(
                    ResourceLocation resourceLocation, java.util.function.Function<String, InputStream> loadFromFile) {}

            @Override
            public boolean hasTwoPhaseProperties() {
                return false;
            }

            @Override
            public void addToUrl(
                    String locationType,
                    TransformerParameters transformerParameters,
                    UrlBuilder urlBuilder,
                    UrlBuildingStrategy urlBuildingStrategy) {
                urlBuilder.addToQueryString("chicken", changingValue.get());
            }

            @Override
            public Content transform(
                    Content content,
                    TransformerParameters transformerParameters,
                    ResourceLocation resourceLocation,
                    QueryParams queryParams,
                    String sourceUrl) {
                return content;
            }

            @Override
            public Set<String> getParamKeys() {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
        DefaultWebResourceAssemblerFactory factory = f.factory(changingTransformers);
        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.resources().requireWebResource("plug1:wr1");
        WebResourceSet set1 = assembler1.assembled().drainIncludedResources();

        assertResolvedResources(set1, m("plug1:wr1", "js", q("chicken", "original")));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));

        // change the query param value. Any assembler should now return the updated query param value
        changingValue.set("crispy");

        WebResourceAssembler assembler2 = factory.create().build();
        assembler2.resources().requireWebResource("plug1:wr1");
        WebResourceSet set2 = assembler2.assembled().drainIncludedResources();

        assertResolvedResources(set2, m("plug1:wr1", "js", q("chicken", "crispy")));
        //        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
        //        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testContextSubtraction() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b").deps("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().exclude(setOf(), setOf("context.a"));
        assembler.resources().requireContext("context.b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.b", "-context.a"));
    }

    @Test
    public void testExcludingNonOverlappingResource() {
        f.mockPlugin("test.atlassian:resource-z").defRes();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().exclude(setOf("test.atlassian:resource-z"), setOf());
        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a"));
    }

    @Test
    public void testNullableExclude() {
        f.mockPlugin("test.atlassian:resource-z").defRes();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().exclude(null, null);
        assembler.resources().exclude(setOf(null, "", " "), setOf(null, "", " "));
        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a"));
    }

    /*
     * Temporary test case to ensure that the Config class does not allow web-resources with context dependencies
     * until we actually implement context dependency resolution for web-resources. This test must be removed once
     * the feature is in (see APDEX-923).
     */
    @Test
    public void testWebResourceContextDependencyDisallowed() {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-c").defRes();

        f.mockPlugin("test.atlassian:resource-d")
                .defRes()
                .deps("test.atlassian:resource-c")
                .ctxDeps("context.a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().requireWebResource("test.atlassian:resource-c");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-c"));
    }
}
