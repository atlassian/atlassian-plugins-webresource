package com.atlassian.plugin.webresource.integration.stub;

import java.util.List;

import com.atlassian.plugin.webresource.WebResourceManagerImpl;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.WebResourceManager;

// Web Resource stub using deprecated implementation.
public class WebResourceImplDeprecated extends WebResourceImpl {
    private WebResourceManager webResourceManager;

    @Override
    protected void initialize(ConfigurationData configurationData) {
        super.initialize(configurationData);
        webResourceManager = new WebResourceManagerImpl(
                resourceLocator.getGlobals(), integration, urlProvider, batchingConfiguration);
    }

    @Override
    public void requireResource(String key) {
        webResourceManager.requireResource(key);
    }

    @Override
    public void requireContext(String key) {
        webResourceManager.requireResourcesForContext(key);
    }

    @Override
    public void include(List<String> keys) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void exclude(List<String> keys) {
        throw new UnsupportedOperationException("Not implemented");
    }

    // Returns HTML that should be included in the head of the page to include all the required resources.
    @Override
    public String pathsAsHtml() {
        return webResourceManager.getRequiredResources(UrlMode.AUTO);
    }
}
