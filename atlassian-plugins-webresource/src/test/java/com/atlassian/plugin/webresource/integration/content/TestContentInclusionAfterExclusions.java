package com.atlassian.plugin.webresource.integration.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.fixtures.BroadFeatures;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.mockito.Mockito.doReturn;

@RunWith(Parameterized.class)
public class TestContentInclusionAfterExclusions extends TestCase {

    private final boolean contextBatchingEnabled;
    private final boolean webresourceBatchingEnabled;
    private List<String> urls;

    public TestContentInclusionAfterExclusions(
            final boolean contextBatchingEnabled, final boolean webresourceBatchingEnabled) {
        this.contextBatchingEnabled = contextBatchingEnabled;
        this.webresourceBatchingEnabled = webresourceBatchingEnabled;
    }

    @Parameterized.Parameters(name = "context-batching: {0}, webresource-batching: {1}")
    public static Collection<Object[]> configure() {
        return asList(new Object[][] {
            {true, true},
            {true, false},
            {false, true},
            {false, false}
        });
    }

    /**
     *
     */
    @Before
    public void setUp() throws Exception {
        BroadFeatures.configure(wr.configure(config -> {
            doReturn(contextBatchingEnabled).when(config).isContextBatchingEnabled();
            doReturn(webresourceBatchingEnabled).when(config).isWebResourceBatchingEnabled();
            doReturn(false).when(config).isSuperBatchingEnabled();
            doReturn(false).when(config).isPerformanceTrackingEnabled();
        }));
        urls = new ArrayList<>();
    }

    @Test
    public void given_context0Nexcluded_when_context1Nrequested_then_shouldOnlyServeContentFrom1N() {
        // given
        wr.excludeContext("0N");

        // when
        wr.requireContext("1N");
        urls.addAll(wr.paths());

        final String content = getAllContentInOrder(urls);

        // then
        assertThat(
                content,
                stringContainsInOrder(newArrayList(
                        "content of feature-10.js",
                        "content of feature-11.js",
                        "content of feature-12.js",
                        "content of feature-13.js",
                        "content of feature-14.js",
                        "content of feature-15.js",
                        "content of feature-16.js",
                        "content of feature-17.js",
                        "content of feature-18.js",
                        "content of feature-19.js")));
        assertThat(
                content,
                not(anyOf(
                        containsString("content of feature-00.js"),
                        containsString("content of feature-01.js"),
                        containsString("content of feature-02.js"),
                        containsString("content of feature-03.js"),
                        containsString("content of feature-04.js"),
                        containsString("content of feature-05.js"),
                        containsString("content of feature-06.js"),
                        containsString("content of feature-07.js"),
                        containsString("content of feature-08.js"),
                        containsString("content of feature-09.js"))));
    }

    @Test
    public void given_webResourcesFrom0Nexcluded_when_context1Nrequested_then_shouldOnlyServeContentFrom1N() {
        // given
        wr.exclude(
                "plugin.key:feature-00",
                "plugin.key:feature-01",
                "plugin.key:feature-02",
                "plugin.key:feature-03",
                "plugin.key:feature-04",
                "plugin.key:feature-05",
                "plugin.key:feature-06",
                "plugin.key:feature-07",
                "plugin.key:feature-08",
                "plugin.key:feature-09");

        // when
        wr.requireContext("1N");
        urls.addAll(wr.paths());

        final String content = getAllContentInOrder(urls);

        // then
        assertThat(
                content,
                stringContainsInOrder(newArrayList(
                        "content of feature-10.js",
                        "content of feature-11.js",
                        "content of feature-12.js",
                        "content of feature-13.js",
                        "content of feature-14.js",
                        "content of feature-15.js",
                        "content of feature-16.js",
                        "content of feature-17.js",
                        "content of feature-18.js",
                        "content of feature-19.js")));
        assertThat(
                content,
                not(anyOf(
                        containsString("content of feature-00.js"),
                        containsString("content of feature-01.js"),
                        containsString("content of feature-02.js"),
                        containsString("content of feature-03.js"),
                        containsString("content of feature-04.js"),
                        containsString("content of feature-05.js"),
                        containsString("content of feature-06.js"),
                        containsString("content of feature-07.js"),
                        containsString("content of feature-08.js"),
                        containsString("content of feature-09.js"))));
    }

    @Test
    public void given_webResourcesFrom1Nexcluded_when_context2Nrequested_then_shouldOnlyServeContentFrom2N() {
        // given
        wr.exclude(
                "plugin.key:feature-10",
                "plugin.key:feature-11",
                "plugin.key:feature-12",
                "plugin.key:feature-13",
                "plugin.key:feature-14",
                "plugin.key:feature-15",
                "plugin.key:feature-16",
                "plugin.key:feature-17",
                "plugin.key:feature-18",
                "plugin.key:feature-19");

        // when
        wr.requireContext("2N");
        urls.addAll(wr.paths());

        final String content = getAllContentInOrder(urls);

        // then
        assertThat(
                content,
                stringContainsInOrder(newArrayList(
                        "content of feature-20.js",
                        "content of feature-21.js",
                        "content of feature-22.js",
                        "content of feature-23.js",
                        "content of feature-24.js",
                        "content of feature-25.js",
                        "content of feature-26.js",
                        "content of feature-27.js",
                        "content of feature-28.js",
                        "content of feature-29.js")));
        assertThat(
                content,
                not(anyOf(
                        containsString("content of feature-00.js"),
                        containsString("content of feature-01.js"),
                        containsString("content of feature-02.js"),
                        containsString("content of feature-03.js"),
                        containsString("content of feature-04.js"),
                        containsString("content of feature-05.js"),
                        containsString("content of feature-06.js"),
                        containsString("content of feature-07.js"),
                        containsString("content of feature-08.js"),
                        containsString("content of feature-09.js"))));
    }

    @Test
    public void given_webResourcesFrom1NrequestedAlready_when_context2Nrequested_then_shouldOnlyServeContentFrom2N() {
        // given
        wr.requireResource("plugin.key:feature-10");
        wr.requireResource("plugin.key:feature-11");
        wr.requireResource("plugin.key:feature-12");
        wr.requireResource("plugin.key:feature-13");
        wr.requireResource("plugin.key:feature-14");
        wr.requireResource("plugin.key:feature-15");
        wr.requireResource("plugin.key:feature-16");
        wr.requireResource("plugin.key:feature-17");
        wr.requireResource("plugin.key:feature-18");
        wr.requireResource("plugin.key:feature-19");
        wr.paths();

        // when
        wr.requireContext("2N");
        urls.addAll(wr.paths());

        final String content = getAllContentInOrder(urls);

        // then
        assertThat(
                content,
                stringContainsInOrder(newArrayList(
                        "content of feature-20.js",
                        "content of feature-21.js",
                        "content of feature-22.js",
                        "content of feature-23.js",
                        "content of feature-24.js",
                        "content of feature-25.js",
                        "content of feature-26.js",
                        "content of feature-27.js",
                        "content of feature-28.js",
                        "content of feature-29.js")));
        assertThat(
                content,
                not(anyOf(
                        containsString("content of feature-00.js"),
                        containsString("content of feature-01.js"),
                        containsString("content of feature-02.js"),
                        containsString("content of feature-03.js"),
                        containsString("content of feature-04.js"),
                        containsString("content of feature-05.js"),
                        containsString("content of feature-06.js"),
                        containsString("content of feature-07.js"),
                        containsString("content of feature-08.js"),
                        containsString("content of feature-09.js"))));
    }

    private String getAllContentInOrder(final List<String> urls) {
        return urls.stream().map(wr::getContent).collect(Collectors.joining("\n"));
    }
}
