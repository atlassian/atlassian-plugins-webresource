package com.atlassian.plugin.webresource.util;

import org.junit.Test;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class TestHashBuilder {
    @Test
    public void testThatHashBuilderPreservesOrder() {
        String hash1 = HashBuilder.buildHash(asList("abc", "xyz"));

        String hash2 = HashBuilder.buildHash(asList("xyz", "abc"));

        assertThat(hash2, not(equalTo(hash1)));
    }
}
