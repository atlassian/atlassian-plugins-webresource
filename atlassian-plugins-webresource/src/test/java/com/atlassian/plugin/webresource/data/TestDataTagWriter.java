package com.atlassian.plugin.webresource.data;

import java.io.IOException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.Test;

import static java.util.Collections.emptyList;

import static com.atlassian.plugin.webresource.data.DataTestFixture.assertDataTag;

public class TestDataTagWriter {
    @Test
    public void testEmpty() throws IOException {
        assertDataTag(emptyList(), "");
    }

    @Test
    public void testBasic() throws IOException {
        assertDataTag("a", "b", "WRM._unparsedData[\"a\"]=\"b\";");
        StringEscapeUtils.escapeEcmaScript("a");
    }

    @Test
    public void testHtml() throws IOException {
        assertDataTag("<h1>", "<h2>", "WRM._unparsedData[\"\\u003ch1\\u003e\"]=\"\\u003ch2\\u003e\";");
    }

    @Test
    public void testScriptClose() throws IOException {
        assertDataTag(
                "</script>", "</script>", "WRM._unparsedData[\"\\u003c/script\\u003e\"]=\"\\u003c/script\\u003e\";");
    }

    @Test
    public void testQuotes() throws IOException {
        assertDataTag("\"'", "\"'", "WRM._unparsedData[\"\\u0022\\u0027\"]=\"\\u0022\\u0027\";");
    }
}
