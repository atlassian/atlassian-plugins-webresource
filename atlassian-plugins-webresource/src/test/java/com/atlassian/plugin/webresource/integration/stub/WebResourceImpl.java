package com.atlassian.plugin.webresource.integration.stub;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.mockito.ArgumentMatchers;
import com.google.common.collect.Maps;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.webresource.DefaultResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.PluginResourceLocatorImpl;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.ResourceUtils;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptorBuilder;
import com.atlassian.plugin.webresource.WebResourceUrlProviderImpl;
import com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.impl.annotators.ListOfAnnotators;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.servlet.PluginResourceDownload;
import com.atlassian.plugin.webresource.transformer.ContentTransformerFactory;
import com.atlassian.plugin.webresource.transformer.ContentTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformers;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformersSupplier;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;

// Web Resource stub using modern implementation.
public class WebResourceImpl extends WebResource {
    protected PluginResourceLocatorImpl resourceLocator;
    protected PluginAccessor pluginAccessor;
    protected WebResourceIntegration integration;
    protected PageBuilderService pageBuilderService;
    protected ResourceBatchingConfiguration batchingConfiguration;
    protected Config config;
    protected PluginResourceDownload pluginResourceDownload;
    protected WebResourceUrlProviderImpl urlProvider;
    protected StaticTransformers staticTransformers;
    protected Map<String, Object> requestCache;
    private DefaultWebResourceAssemblerFactory webResourceAssemblerFactory;
    private DrainResult lastDrain;

    public static File getTempDir() {
        try {
            return File.createTempFile("foo", "foo").getParentFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void requireResource(String key) {
        pageBuilderService.assembler().resources().requireWebResource(key);
    }

    @Override
    public PageBuilderService getPageBuilderService() {
        return pageBuilderService;
    }

    @Override
    public void requireContext(String key) {
        pageBuilderService.assembler().resources().requireContext(key);
    }

    @Override
    public void include(List<String> keys) {
        for (String key : keys) {
            if (key.contains(Config.CONTEXT_PREFIX)) {
                requireContext(key.replaceAll(Config.CONTEXT_PREFIX + ":", ""));
            } else {
                requireResource(key);
            }
        }
    }

    @Override
    public void exclude(List<String> keys) {
        if (keys.isEmpty()) {
            return;
        }
        LinkedHashSet<String> excludedWebResources = new LinkedHashSet<>();
        LinkedHashSet<String> excludedContexts = new LinkedHashSet<>();
        for (String key : keys) {
            if (key.contains(Config.CONTEXT_PREFIX)) {
                excludedContexts.add(key.replaceAll(Config.CONTEXT_PREFIX + ":", ""));
            } else {
                excludedWebResources.add(key);
            }
        }
        getPageBuilderService().assembler().resources().exclude(excludedWebResources, excludedContexts);
    }

    @Override
    public DrainResult drain() {
        StringWriter out = new StringWriter();
        pageBuilderService.assembler().assembled().drainIncludedResources().writeHtmlTags(out, UrlMode.AUTO);
        lastDrain = new DrainResult(out.toString());
        return lastDrain;
    }

    @Override
    public DrainResult lastDrain() {
        return lastDrain;
    }

    public WebResourceAssemblerFactory getWebResourceAssemblerFactory() {
        return webResourceAssemblerFactory;
    }

    @Override
    public void get(HttpServletRequest request, HttpServletResponse response) {
        try {
            pluginResourceDownload.serveFile(request, response);
        } catch (DownloadException e) {
            throw new RuntimeException(e);
        }
    }

    // Configure web resources according to given XML configuration.
    protected void initialize(ConfigurationData configurationData) {
        // Mocking external parts.
        pluginAccessor = mock(PluginAccessor.class);

        PluginEventManager eventManager = mock(PluginEventManager.class);

        integration = WebResourceIntegrationImpl.create();
        when(integration.getRequestCache()).thenAnswer(invocationOnMock -> requestCache);
        when(integration.getPluginAccessor()).thenReturn(pluginAccessor);
        when(integration.getBaseUrl(com.atlassian.webresource.api.UrlMode.AUTO)).thenReturn("/app");
        when(integration.getI18nStateHash()).thenReturn("stat&");
        when(integration.getCDNStrategy()).thenReturn(configurationData.cdnStrategy);
        when(integration.getSyncWebResourceKeys()).thenReturn(configurationData.syncWebResourceKeys);

        // Batching configuration.
        batchingConfiguration = spy(new DefaultResourceBatchingConfiguration());
        when(batchingConfiguration.isSuperBatchingEnabled()).thenReturn(configurationData.isSuperBatchEnabled);
        when(batchingConfiguration.isContextBatchingEnabled()).thenReturn(configurationData.isContextBatchingEnabled);
        when(batchingConfiguration.isPluginWebResourceBatchingEnabled())
                .thenReturn(configurationData.isWebResourceBatchingEnabled);
        when(batchingConfiguration.isJavaScriptTryCatchWrappingEnabled())
                .thenReturn(configurationData.isJavaScriptTryCatchWrappingEnabled);
        when(batchingConfiguration.isBatchContentTrackingEnabled()).thenReturn(false);
        when(batchingConfiguration.isSourceMapEnabled()).thenReturn(configurationData.isSourceMapEnabled);
        when(batchingConfiguration.optimiseSourceMapsForDevelopment())
                .thenReturn(configurationData.optimiseSourceMapForDevelopment);

        // Url Provider.
        urlProvider = new WebResourceUrlProviderImpl(integration);

        // Static Transformers.
        if (configurationData.staticTransformers != null) {
            this.staticTransformers = configurationData.staticTransformers;
        } else {
            this.staticTransformers = new DefaultStaticTransformers(
                    new DefaultStaticTransformersSupplier(integration, urlProvider, (url) -> url));
        }

        // Config.
        config = new Config(
                batchingConfiguration,
                integration,
                urlProvider,
                null,
                new TransformerCache(eventManager, integration.getPluginAccessor())) {};
        config.setStaticTransformers(staticTransformers);
        config = spy(config);
        doReturn(new ArrayList<>()).when(config).getBeforeAllResources();

        configurationData.configCallbacks.stream().filter(Objects::nonNull).forEachOrdered(cb -> cb.apply(config));

        resourceLocator = new PluginResourceLocatorImpl(eventManager, config, integration.getEventPublisher());

        // Globals.
        globals = resourceLocator.getGlobals();

        // Support for HTTP.
        ContentTypeResolver contentTypeResolver = spy(new ContentTypeResolver() {
            @Override
            public String getContentType(String url) {
                return buildMap("css", "text/css", "js", "application/javascript", "png", "image/png")
                        .get(Request.getType(url));
            }
        });
        pluginResourceDownload = new PluginResourceDownload(globals, contentTypeResolver);

        reset();
    }

    @Override
    public void reset() {
        requestCache = new HashMap<>();
        // Page builder service.
        webResourceAssemblerFactory = new DefaultWebResourceAssemblerFactory(globals);
        pageBuilderService = new DefaultPageBuilderService(integration, webResourceAssemblerFactory);
    }

    @Override
    public void applyConfiguration(final ConfigurationData configurationData) {
        initialize(configurationData);

        List<ContentTransformerModuleDescriptor> contentTransformerDescriptor =
                new ArrayList<ContentTransformerModuleDescriptor>();
        List<UrlReadingWebResourceTransformerModuleDescriptor> transformerDescriptors =
                new ArrayList<UrlReadingWebResourceTransformerModuleDescriptor>();

        // Default transformers.
        for (Map.Entry<String, Class> entry : configurationData.defaultTransformerClasses.entrySet()) {
            transformerDescriptors.add(TestUtils.mockTransformer(entry.getKey(), entry.getKey(), entry.getValue()));
        }

        // Annotators.
        if (configurationData.annotators != null) {
            when(globals.getConfig().getContentAnnotator(any(String.class)))
                    .thenReturn(new ListOfAnnotators(configurationData.annotators));
        }

        // Configuring plugins.
        List<WebResourceModuleDescriptor> listOfWebResourceDescriptors = new ArrayList<>();

        for (PluginData pluginData : configurationData.plugins) {
            // Track content that should be discoverable through this plugin and its module descriptors
            Map<String, String> relativePathsToContent = Maps.newHashMap();

            // Searching for classes.
            List<Class> classes = new ArrayList<>();
            for (WebResourceData webResourceData : pluginData.webResources) {
                for (WebResource.ConditionData condition : webResourceData.conditions) {
                    classes.add(condition.clazz);
                }
            }

            // Creating plugin.
            Plugin plugin = TestUtils.createTestPlugin(pluginData.key, "1", classes.toArray(new Class[classes.size()]));
            when(pluginAccessor.getPlugin(pluginData.key)).thenReturn(plugin);
            when(pluginAccessor.getEnabledPlugin(pluginData.key)).thenReturn(plugin);

            // Plugin resources.
            for (ResourceData resourceData : pluginData.resources) {
                when(plugin.getResourceLocation("download", resourceData.name))
                        .thenReturn(new ResourceLocation(
                                resourceData.location,
                                resourceData.name,
                                ResourceUtils.getType(resourceData.name),
                                resourceData.contentType,
                                resourceData.content,
                                resourceData.params));

                if (resourceData.relativeFiles.isEmpty()) {
                    relativePathsToContent.put(resourceData.location, resourceData.content);
                } else {
                    for (Map.Entry<String, String> file : resourceData.relativeFiles.entrySet()) {
                        final String content =
                                StringUtils.isNotBlank(file.getValue()) ? file.getValue() : resourceData.content;
                        final Path path = Paths.get(resourceData.location, file.getKey());
                        relativePathsToContent.put(path.toString(), content);
                    }
                }
            }

            // Creating web resources.
            for (WebResourceData webResourceData : pluginData.webResources) {
                // Conditions.
                WebResourceModuleDescriptorBuilder descriptorBuilder =
                        new WebResourceModuleDescriptorBuilder(plugin, webResourceData.key);
                for (ConditionData condition : webResourceData.conditions) {
                    descriptorBuilder.addCondition(condition.clazz, condition.params, condition.invert);
                }

                // Resources.
                for (final ResourceData resourceData : webResourceData.resources) {
                    descriptorBuilder.addDescriptor(
                            "download",
                            resourceData.name,
                            resourceData.location,
                            resourceData.contentType,
                            resourceData.params);
                    if (resourceData.relativeFiles.isEmpty()) {
                        relativePathsToContent.put(resourceData.location, resourceData.content);
                    } else {
                        for (Map.Entry<String, String> file : resourceData.relativeFiles.entrySet()) {
                            final String content =
                                    StringUtils.isNotBlank(file.getValue()) ? file.getValue() : resourceData.content;
                            final Path path = Paths.get(resourceData.location, file.getKey());
                            relativePathsToContent.put(path.toString(), content);
                        }
                    }
                }

                // Files.
                for (final FileData fileData : webResourceData.files) {
                    relativePathsToContent.put(fileData.name, fileData.content);
                }

                // Dependencies.
                for (String dependencyKey : webResourceData.dependencies) {
                    descriptorBuilder.addDependency(dependencyKey);
                }

                // Contexts.
                for (String contextKey : webResourceData.contexts) {
                    descriptorBuilder.addContext(contextKey);
                }

                // Tranformations.
                for (Map.Entry<String, List<String>> entry : webResourceData.transformations.entrySet()) {
                    descriptorBuilder.addTransformers(entry.getKey(), entry.getValue());
                }

                // Data providers.
                for (Tuple<String, Class<?>> item : webResourceData.dataProviders) {
                    String key = item.getFirst();
                    Class<?> klass = item.getLast();
                    descriptorBuilder.addDataProvider(key, klass);
                    try {
                        when(plugin.loadClass(ArgumentMatchers.eq(klass.getName()), any()))
                                .thenReturn((Class<Object>) klass);
                    } catch (ClassNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                }

                ModuleDescriptor descriptor = descriptorBuilder.build();
                listOfWebResourceDescriptors.add((WebResourceModuleDescriptor) descriptor);
                when(pluginAccessor.getEnabledPluginModule(pluginData.key + ":" + webResourceData.key))
                        .thenReturn(descriptor);
            }

            // Preparing transformers.
            for (Map.Entry<String, Tuple<String, Class>> entry : pluginData.transformers.entrySet()) {
                String key = entry.getKey();
                String aliasKey = entry.getValue().getFirst();
                Class<?> klass = entry.getValue().getLast();
                if (ContentTransformerFactory.class.isAssignableFrom(klass)) {
                    contentTransformerDescriptor.add(TestUtils.mockContentTransformer(key, klass));
                } else if (WebResourceTransformerFactory.class.isAssignableFrom(klass)) {
                    transformerDescriptors.add(TestUtils.mockTransformer(key, aliasKey, klass));
                } else {
                    throw new RuntimeException("invalid transformer class " + klass.getCanonicalName() + "!");
                }
            }

            // Finally, ensure all content will be discovered and served.
            relativePathsToContent.forEach((path, content) -> {
                // Callback should be used because otherwise resource input stream wouldn't be reset for the new usage.
                when(plugin.getResourceAsStream(path)).thenAnswer(invocationOnMock -> IOUtils.toInputStream(content));
            });
        }

        // Adding web resources.
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
                .thenReturn(listOfWebResourceDescriptors);

        // Adding module keys that should be included in superbatch.
        if (configurationData.addEverythingToSuperbatch) {
            List<String> allWebResourceKeys = new ArrayList<>();
            for (PluginData plugin : configurationData.plugins) {
                for (WebResourceData webResource : plugin.webResources) {
                    allWebResourceKeys.add(plugin.key + ":" + webResource.key);
                }
            }
            when(batchingConfiguration.getSuperBatchModuleCompleteKeys()).thenReturn(allWebResourceKeys);
        } else {
            when(batchingConfiguration.getSuperBatchModuleCompleteKeys())
                    .thenReturn(configurationData.webResourcesToAddToSuperbatch);
        }

        // Adding transformers.
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ContentTransformerModuleDescriptor.class))
                .thenReturn(contentTransformerDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(UrlReadingWebResourceTransformerModuleDescriptor.class))
                .thenReturn(transformerDescriptors);

        // make sure that snapshot is generated
        globals.getSnapshot();
    }

    @Override
    public PluginResourceLocator getResourceLocator() {
        return resourceLocator;
    }

    @Override
    public Config getConfig() {
        return config;
    }
}
