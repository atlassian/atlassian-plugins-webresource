package com.atlassian.plugin.webresource.mocks;

import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;

import static java.util.Objects.requireNonNull;
import static org.mockito.Mockito.when;

public class BundleMockBuilder {
    private final MockBuilder parent;
    private final Bundle bundle;

    public BundleMockBuilder(@Nonnull final MockBuilder parent, @Nonnull final Bundle bundle) {
        this.parent = requireNonNull(parent, "The parent is mandatory for the bundle mock build.");
        this.bundle = requireNonNull(bundle, "The bundle is mandatory for the bundle mock build.");
    }

    @Nonnull
    public MockBuilder and() {
        return parent;
    }

    @Nonnull
    public BundleMockBuilder minificationEnabled(final boolean enabled) {
        when(bundle.isMinificationEnabled()).thenReturn(enabled);
        return this;
    }
}
