package com.atlassian.plugin.webresource.impl.snapshot.resource;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static com.atlassian.plugin.webresource.impl.config.Config.BATCH_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.config.Config.SOURCE_PARAM_NAME;
import static com.atlassian.plugin.webresource.impl.snapshot.resource.Resource.getUrlParamsStatic;
import static com.atlassian.plugin.webresource.mocks.MockBuilder.builder;

@RunWith(MockitoJUnitRunner.class)
public class ResourceTest {

    @Mock
    private Bundle bundle;

    @Mock
    private ResourceLocation resourceLocation;

    @Test
    public void when_UrlAllowPublicUseIsFalse_And_ThereIsAnotherUrl_Then_ReturnNewMapWithoutAllowPublicUseParameter() {
        // when
        final Map<String, String> input = new HashMap<>();
        input.put("allow-public-use", "false");

        final Map<String, String> actual = getUrlParamsStatic(input);

        // then
        final Map<String, String> expected = new HashMap<>();
        assertEquals(expected, actual);
    }

    @Test
    public void when_UrlAllowPublicUseIsTrue_And_ThereIsAnotherUrl_Then_ReturnNewMapWithAllParameters() {
        // when
        final Map<String, String> input = new HashMap<>();
        input.put("allow-public-use", "true");

        final Map<String, String> actual = getUrlParamsStatic(input);

        // then
        final Map<String, String> expected = new HashMap<>();
        expected.put("allow-public-use", "true");
        assertEquals(expected, actual);
    }

    @Test
    public void
            test_IfResourceIsBatchable_When_BatchParamIsFalse_And_IsNotRedirect_Then_ReturnResourceAsNotBatchable() {
        // given
        builder().withMockedResourceLocation(resourceLocation).singletonParams(BATCH_PARAM_NAME, FALSE.toString());
        // when
        final Resource resource = new Resource(bundle, resourceLocation, null, null, null, null, null, null);

        // then
        assertFalse(resource.isBatchable());
    }

    @Test
    public void test_IfResourceIsBatchable_When_BatchParamIsTrue_And_IsNotRedirect_Then_ReturnResourceAsNotBatchable() {
        // given
        final Map<String, String> params = new HashMap<>();
        params.put(BATCH_PARAM_NAME, TRUE.toString());
        params.put(SOURCE_PARAM_NAME, EMPTY);

        builder().withMockedResourceLocation(resourceLocation).params(params);
        // when
        final Resource resource = new Resource(bundle, resourceLocation, null, null, null, null, null, null);

        // then
        assertTrue(resource.isBatchable());
    }

    @Test
    public void test_IfResourceIsBatchable_When_BatchParamIsTrue_And_IsRedirect_Then_ReturnResourceAsNotBatchable() {
        // given
        final Map<String, String> params = new HashMap<>();
        params.put(BATCH_PARAM_NAME, TRUE.toString());
        params.put(SOURCE_PARAM_NAME, "webContext");

        builder().withMockedResourceLocation(resourceLocation).params(params);
        // when
        final Resource resource = new Resource(bundle, resourceLocation, null, null, null, null, null, null);

        // then
        assertFalse(resource.isBatchable());
    }
}
