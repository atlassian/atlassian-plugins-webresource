package com.atlassian.plugin.webresource.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class WebResourceKeyHelperTest {

    @Test
    public void isWebResourceKeyShouldReturnTrueForCompleteKeys() {
        assertTrue(WebResourceKeyHelper.isWebResourceKey("plugin:web-resource"));
    }

    @Test
    public void isWebResourceKeyShouldReturnTrueForSimpleKeys() {
        assertTrue(WebResourceKeyHelper.isWebResourceKey(":web-resource"));
    }

    @Test
    public void isWebResourceKeyShouldReturnFalseForNotKeys() {
        assertFalse(WebResourceKeyHelper.isWebResourceKey("plugin"));
        assertFalse(WebResourceKeyHelper.isWebResourceKey("web-resource"));
    }

    @Test
    public void isWebResourceKeyShouldReturnFalseForNull() {
        assertFalse(WebResourceKeyHelper.isWebResourceKey(null));
    }

    @Test
    public void createWebResourceKeyShouldReturnValidKeysForCompleteKeys() {
        var key = WebResourceKeyHelper.createWebResourceKey("plugin:web-resource");

        assertTrue(key.isPresent());
        assertEquals("plugin", key.get().getPluginKey());
        assertEquals("web-resource", key.get().getWebResourceKey());
    }

    @Test
    public void createWebResourceKeyShouldReturnEmptyForSimpleKeys() {
        assertTrue(WebResourceKeyHelper.createWebResourceKey(":web-resource").isEmpty());
    }

    @Test
    public void createWebResourceKeyShouldReturnEmptyForBadKeys() {
        assertTrue(WebResourceKeyHelper.createWebResourceKey(":web-resource").isEmpty());
        assertTrue(WebResourceKeyHelper.createWebResourceKey("plugin:").isEmpty());
        assertTrue(WebResourceKeyHelper.createWebResourceKey("plugin").isEmpty());
        assertTrue(WebResourceKeyHelper.createWebResourceKey(":").isEmpty());
        assertTrue(WebResourceKeyHelper.createWebResourceKey("").isEmpty());
    }
}
