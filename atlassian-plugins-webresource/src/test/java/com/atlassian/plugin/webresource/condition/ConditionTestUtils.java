package com.atlassian.plugin.webresource.condition;

import java.util.HashMap;

import com.atlassian.webresource.spi.condition.UrlReadingCondition;

public final class ConditionTestUtils {

    public static DecoratingCondition decorate(UrlReadingCondition urlReadingCondition) {
        return new DecoratingUrlReadingCondition(
                urlReadingCondition, new HashMap<>(), "pluginKey", "conditionClassName");
    }
}
