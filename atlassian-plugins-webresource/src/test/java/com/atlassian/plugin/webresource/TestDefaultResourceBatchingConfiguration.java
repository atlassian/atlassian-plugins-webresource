package com.atlassian.plugin.webresource;

import junit.framework.TestCase;

import com.atlassian.plugin.internal.util.PluginUtils;

public class TestDefaultResourceBatchingConfiguration extends TestCase {
    private DefaultResourceBatchingConfiguration batchingConfiguration;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        batchingConfiguration = new DefaultResourceBatchingConfiguration();
    }

    @Override
    public void tearDown() throws Exception {
        batchingConfiguration = null;
        System.clearProperty(DefaultResourceBatchingConfiguration.PLUGIN_WEBRESOURCE_BATCHING_OFF);
        System.clearProperty(DefaultResourceBatchingConfiguration.PLUGIN_WEB_RESOURCE_BATCH_CONTENT_TRACKING);
        System.clearProperty(PluginUtils.ATLASSIAN_DEV_MODE);

        super.tearDown();
    }

    public void testBatchingIsEnabledWhenNotInDevModeAndBatchingIsNotOff() {
        assertTrue(batchingConfiguration.isPluginWebResourceBatchingEnabled());
    }

    public void testBatchingIsDisabledWhenInDevMode() {
        System.clearProperty(DefaultResourceBatchingConfiguration.PLUGIN_WEBRESOURCE_BATCHING_OFF);
        System.setProperty(PluginUtils.ATLASSIAN_DEV_MODE, "true");
        assertFalse(batchingConfiguration.isPluginWebResourceBatchingEnabled());
    }

    public void testBatchingIsDisabledWhenBatchingIsOff() {
        System.setProperty(DefaultResourceBatchingConfiguration.PLUGIN_WEBRESOURCE_BATCHING_OFF, "true");
        System.clearProperty(PluginUtils.ATLASSIAN_DEV_MODE);
        assertFalse(batchingConfiguration.isPluginWebResourceBatchingEnabled());
    }

    public void testBatchingIsEnabledWhenDevModeIsFalse() {
        System.clearProperty(DefaultResourceBatchingConfiguration.PLUGIN_WEBRESOURCE_BATCHING_OFF);
        System.setProperty(PluginUtils.ATLASSIAN_DEV_MODE, "false");
        assertTrue(batchingConfiguration.isPluginWebResourceBatchingEnabled());
    }

    public void testBatchingIsEnabledWhenInDevModeAndBatchingIsExplicitlyNotOff() {
        System.setProperty(DefaultResourceBatchingConfiguration.PLUGIN_WEBRESOURCE_BATCHING_OFF, "false");
        System.setProperty(PluginUtils.ATLASSIAN_DEV_MODE, "true");
        assertTrue(batchingConfiguration.isPluginWebResourceBatchingEnabled());
    }

    public void testBatchContentTrackingEnabled() {
        System.setProperty(DefaultResourceBatchingConfiguration.PLUGIN_WEB_RESOURCE_BATCH_CONTENT_TRACKING, "true");
        assertTrue(batchingConfiguration.isBatchContentTrackingEnabled());
    }

    public void testBatchContentTrackingExplicitlyDisabled() {
        System.setProperty(DefaultResourceBatchingConfiguration.PLUGIN_WEB_RESOURCE_BATCH_CONTENT_TRACKING, "false");
        assertFalse(batchingConfiguration.isBatchContentTrackingEnabled());
    }

    public void testBatchContentTrackingIsDisabledByDefult() {
        System.clearProperty(DefaultResourceBatchingConfiguration.PLUGIN_WEB_RESOURCE_BATCH_CONTENT_TRACKING);
        assertFalse(batchingConfiguration.isBatchContentTrackingEnabled());
    }
}
