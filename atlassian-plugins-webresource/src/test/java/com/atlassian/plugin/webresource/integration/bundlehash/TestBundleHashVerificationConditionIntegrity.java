package com.atlassian.plugin.webresource.integration.bundlehash;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This test verifies that validating a bundlehash does not invoke stateful condition paths.
 */
public class TestBundleHashVerificationConditionIntegrity extends BundleHashTestCase {
    private static UrlReadingCondition mockCondition;

    /**
     * Prepares three web-resources `feature-a`, `feature-b`, and `feature-c`:
     * <ul>
     *     <li>Features A and B are in the `foo` context</li>
     *     <li>Features B and C are in the `bar` context</li>
     * </ul>
     */
    @Override
    protected void setUp() throws Exception {
        mockCondition = mock(UrlReadingCondition.class);
        wr.configure()
                .plugin("plugin.key")
                .webResource("feature-a")
                .context("foo")
                .condition(MockedConditionProxy.class)
                .resource("feature-a.js")
                .webResource("feature-b")
                .context("foo")
                .context("bar")
                .condition(MockedConditionProxy.class)
                .resource("feature-b.js")
                .webResource("feature-c")
                .context("bar")
                .condition(MockedConditionProxy.class)
                .resource("feature-c.js")
                .end();
    }

    @Test
    public void when_requestingContent_then_shouldOnlyInvokeShouldDisplay() {
        // given
        when(mockCondition.shouldDisplay(any())).thenReturn(true);
        wr.requireContext("foo");
        wr.requireContext("bar");
        final List<String> urls = wr.paths();
        clearInvocations(mockCondition);

        // when
        getWithOwnStaticHash(urls.get(0));

        // then
        verify(mockCondition, never()).addToUrl(any());
        verify(mockCondition, atLeastOnce()).shouldDisplay(any());
    }

    public static class MockedConditionProxy implements UrlReadingCondition {
        @Override
        public void init(Map<String, String> params) throws PluginParseException {
            mockCondition.init(params);
        }

        @Override
        public void addToUrl(UrlBuilder urlBuilder) {
            mockCondition.addToUrl(urlBuilder);
        }

        @Override
        public boolean shouldDisplay(QueryParams params) {
            return mockCondition.shouldDisplay(params);
        }
    }
}
