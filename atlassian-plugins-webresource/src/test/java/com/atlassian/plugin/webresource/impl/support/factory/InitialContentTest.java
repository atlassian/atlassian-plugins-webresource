package com.atlassian.plugin.webresource.impl.support.factory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import javax.annotation.Nonnull;

import org.apache.commons.io.IOUtils;
import org.checkerframework.checker.nullness.qual.NonNull;

import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.InitialContent;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.lang3.ObjectUtils.notEqual;

import static com.atlassian.plugin.webresource.impl.support.Support.LOGGER;

public class InitialContentTest extends InitialContent {

    public InitialContentTest(@Nonnull final InitialContent initialContent) {
        super(
                initialContent.getContent().orElse(null),
                initialContent.getPath().orElse(null),
                initialContent.getSourceMap().orElse(null));
    }

    private static byte[] clone(final InputStream originalContent) {
        try {
            originalContent.mark(0);
            final byte[] buffer = new byte[1024];
            final ByteArrayOutputStream clonedContent = new ByteArrayOutputStream();
            for (int readLength = 0; notEqual(readLength, -1); readLength = originalContent.read(buffer)) {
                clonedContent.write(buffer, 0, readLength);
            }
            originalContent.reset();
            clonedContent.flush();
            return clonedContent.toByteArray();
        } catch (final IOException exception) {
            LOGGER.warn("It was not possible to clone the content.", exception);
            return null;
        }
    }

    public Optional<String> getContentAsString() {
        return super.getContent().map(InitialContentTest::clone).map(content -> {
            try (final InputStream copy = new ByteArrayInputStream(content)) {
                return IOUtils.toString(copy, UTF_8);
            } catch (final IOException exception) {
                LOGGER.warn("It was not possible to convert the content to string.", exception);
                return null;
            }
        });
    }

    @Override
    public @NonNull Content toContent(@NonNull final Content originalContent) {
        return null;
    }
}
