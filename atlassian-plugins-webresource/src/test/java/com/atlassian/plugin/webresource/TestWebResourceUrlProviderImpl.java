package com.atlassian.plugin.webresource;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.servlet.AbstractFileServerServlet;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.WebResourceUrlProvider;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class TestWebResourceUrlProviderImpl {
    private static final String ANIMAL_PLUGIN_VERSION = "2";
    private static final String BASEURL = "http://www.foo.com";
    private static final String SYSTEM_COUNTER = "123";
    private static final String SYSTEM_BUILD_NUMBER = "650";

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private WebResourceIntegration mockWebResourceIntegration;

    @Mock
    private PluginAccessor mockPluginAccessor;

    private WebResourceUrlProvider urlProvider;

    @Before
    public void setUp() {
        urlProvider = new WebResourceUrlProviderImpl(mockWebResourceIntegration);

        when(mockWebResourceIntegration.getPluginAccessor()).thenReturn(mockPluginAccessor);
        when(mockWebResourceIntegration.getBaseUrl()).thenReturn(BASEURL);
        when(mockWebResourceIntegration.getBaseUrl(UrlMode.ABSOLUTE)).thenReturn(BASEURL);
        when(mockWebResourceIntegration.getBaseUrl(UrlMode.RELATIVE)).thenReturn("");
        when(mockWebResourceIntegration.getBaseUrl(UrlMode.AUTO)).thenReturn("");
        when(mockWebResourceIntegration.getSystemBuildNumber()).thenReturn(SYSTEM_BUILD_NUMBER);
        when(mockWebResourceIntegration.getSystemCounter()).thenReturn(SYSTEM_COUNTER);
        when(mockWebResourceIntegration.getI18nStateHash()).thenReturn(null);
    }

    @After
    public void tearDown() throws Exception {
        mockWebResourceIntegration = null;
        mockPluginAccessor = null;

        urlProvider = null;
    }

    // testGetStaticResourcePrefix

    @Test
    public void testGetStaticResourcePrefix() {
        final String expectedPrefix = setupGetStaticResourcePrefix(false);
        assertEquals(expectedPrefix, urlProvider.getStaticResourcePrefix(UrlMode.AUTO));
    }

    @Test
    public void testGetStaticResourcePrefixWithAbsoluteUrlMode() {
        testGetStaticResourcePrefix(UrlMode.ABSOLUTE, true);
    }

    @Test
    public void testGetStaticResourcePrefixWithRelativeUrlMode() {
        testGetStaticResourcePrefix(UrlMode.RELATIVE, false);
    }

    @Test
    public void testGetStaticResourcePrefixWithAutoUrlMode() {
        testGetStaticResourcePrefix(UrlMode.AUTO, false);
    }

    private void testGetStaticResourcePrefix(UrlMode urlMode, boolean baseUrlExpected) {
        final String expectedPrefix = setupGetStaticResourcePrefix(baseUrlExpected);
        assertEquals(expectedPrefix, urlProvider.getStaticResourcePrefix(urlMode));
    }

    private String setupGetStaticResourcePrefix(boolean baseUrlExpected) {
        return (baseUrlExpected ? BASEURL : "") + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_PREFIX + "/" + SYSTEM_BUILD_NUMBER
                + "/" + SYSTEM_COUNTER + "/" + WebResourceUrlProviderImpl.STATIC_RESOURCE_SUFFIX;
    }

    // testGetStaticResourcePrefixWithCounter

    @Test
    public void testGetStaticResourcePrefixWithCounter() {
        final String resourceCounter = "456";
        final String expectedPrefix = setupGetStaticResourcePrefixWithCounter(false, resourceCounter);
        assertEquals(expectedPrefix, urlProvider.getStaticResourcePrefix(resourceCounter, UrlMode.AUTO));
    }

    @Test
    public void testGetStaticResourcePrefixWithCounterAndAbsoluteUrlMode() {
        testGetStaticResourcePrefixWithCounter(UrlMode.ABSOLUTE, true);
    }

    @Test
    public void testGetStaticResourcePrefixWithCounterAndRelativeUrlMode() {
        testGetStaticResourcePrefixWithCounter(UrlMode.RELATIVE, false);
    }

    @Test
    public void testGetStaticResourcePrefixWithCounterAndAutoUrlMode() {
        testGetStaticResourcePrefixWithCounter(UrlMode.AUTO, false);
    }

    private void testGetStaticResourcePrefixWithCounter(UrlMode urlMode, boolean baseUrlExpected) {
        final String resourceCounter = "456";
        final String expectedPrefix = setupGetStaticResourcePrefixWithCounter(baseUrlExpected, resourceCounter);
        assertEquals(expectedPrefix, urlProvider.getStaticResourcePrefix(resourceCounter, urlMode));
    }

    private String setupGetStaticResourcePrefixWithCounter(boolean baseUrlExpected, String resourceCounter) {
        return (baseUrlExpected ? BASEURL : "") + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_PREFIX + "/" + SYSTEM_BUILD_NUMBER
                + "/" + SYSTEM_COUNTER + "/" + resourceCounter + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_SUFFIX;
    }

    private String setupGetStaticResourcePrefixWithCounter(
            boolean baseUrlExpected, String contributedHash, String resourceCounter) {
        return (baseUrlExpected ? BASEURL : "") + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_PREFIX + "/" + contributedHash + "/" + SYSTEM_BUILD_NUMBER
                + "/" + SYSTEM_COUNTER + "/" + resourceCounter + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_SUFFIX;
    }

    @Test
    public void testGetStaticResourcePrefixWithEmptyContributedHash() {
        final String resourceCounter = "456";
        final String contributedHash = "";
        final String expectedPrefix = setupGetStaticResourcePrefixWithCounter(false, resourceCounter);
        assertEquals(
                expectedPrefix, urlProvider.getStaticResourcePrefix(contributedHash, resourceCounter, UrlMode.AUTO));
    }

    @Test
    public void testGetStaticResourcePrefixWithContributedHash() {
        final String resourceCounter = "456";
        final String contributedHash = "adfsaf";
        final String expectedPrefix = setupGetStaticResourcePrefixWithCounter(false, contributedHash, resourceCounter);
        assertEquals(
                expectedPrefix, urlProvider.getStaticResourcePrefix(contributedHash, resourceCounter, UrlMode.AUTO));
    }

    // testGetStaticPluginResourcePrefix

    @Test
    public void testGetStaticPluginResourcePrefix() {
        final String moduleKey = "confluence.extra.animal:animal";
        final String resourceName = "foo.js";

        final String expectedPrefix = setupGetStaticPluginResourcePrefix(false, moduleKey, resourceName);

        assertEquals(expectedPrefix, urlProvider.getStaticPluginResourceUrl(moduleKey, resourceName, UrlMode.AUTO));
    }

    @Test
    public void testGetStaticPluginResourcePrefixWithAbsoluteUrlMode() {
        testGetStaticPluginResourcePrefix(UrlMode.ABSOLUTE, true);
    }

    @Test
    public void testGetStaticPluginResourcePrefixWithRelativeUrlMode() {
        testGetStaticPluginResourcePrefix(UrlMode.RELATIVE, false);
    }

    @Test
    public void testGetStaticPluginResourcePrefixWithAutoUrlMode() {
        testGetStaticPluginResourcePrefix(UrlMode.AUTO, false);
    }

    private void testGetStaticPluginResourcePrefix(UrlMode urlMode, boolean baseUrlExpected) {
        final String moduleKey = "confluence.extra.animal:animal";
        final String resourceName = "foo.js";

        final String expectedPrefix = setupGetStaticPluginResourcePrefix(baseUrlExpected, moduleKey, resourceName);

        assertEquals(expectedPrefix, urlProvider.getStaticPluginResourceUrl(moduleKey, resourceName, urlMode));
    }

    private String setupGetStaticPluginResourcePrefix(boolean baseUrlExpected, String moduleKey, String resourceName) {
        final Plugin animalPlugin = new StaticPlugin();
        animalPlugin.setKey("confluence.extra.animal");
        animalPlugin.setPluginsVersion(5);
        animalPlugin.getPluginInformation().setVersion(ANIMAL_PLUGIN_VERSION);

        mockEnabledPluginModule(moduleKey, TestUtils.createWebResourceModuleDescriptor(moduleKey, animalPlugin));

        return (baseUrlExpected ? BASEURL : "") + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_PREFIX + "/" + SYSTEM_BUILD_NUMBER + "/"
                + SYSTEM_COUNTER + "/" + ANIMAL_PLUGIN_VERSION + "/" + WebResourceUrlProviderImpl.STATIC_RESOURCE_SUFFIX
                + "/"
                + AbstractFileServerServlet.SERVLET_PATH + "/" + AbstractFileServerServlet.RESOURCE_URL_PREFIX + "/"
                + moduleKey + "/" + resourceName;
    }

    @Test
    public void testGetStaticPrefixResourceWithI18nHash() {
        testGetStaticPrefixResourceWithI18nHash(UrlMode.ABSOLUTE, true);
        testGetStaticPrefixResourceWithI18nHash(UrlMode.RELATIVE, false);
        testGetStaticPrefixResourceWithI18nHash(UrlMode.AUTO, false);
    }

    private void testGetStaticPrefixResourceWithI18nHash(UrlMode urlMode, boolean baseUrlExpected) {
        String expected = setupGetStaticPrefixResourceWithI18nHash(baseUrlExpected);
        assertEquals(expected, urlProvider.getStaticResourcePrefix(urlMode));
    }

    private String setupGetStaticPrefixResourceWithI18nHash(boolean baseUrlExpected) {
        when(mockWebResourceIntegration.getI18nStateHash()).thenReturn("i18n-ha$h");
        return (baseUrlExpected ? BASEURL : "") + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_PREFIX + "/" + "i18n-ha$h/"
                + SYSTEM_BUILD_NUMBER
                + "/" + SYSTEM_COUNTER + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_SUFFIX;
    }

    @Test
    public void testStaticPluginResourceWithI18nHash() {
        testStaticPluginResourceWithI18nHash(UrlMode.ABSOLUTE, true);
        testStaticPluginResourceWithI18nHash(UrlMode.RELATIVE, false);
        testStaticPluginResourceWithI18nHash(UrlMode.AUTO, false);
    }

    private void testStaticPluginResourceWithI18nHash(UrlMode urlMode, boolean baseUrlExpected) {
        final String moduleKey = "confluence.extra.animal:animal";
        final String resourceName = "foo.js";
        String expected = setupStaticPluginResourceWithI18nHash(moduleKey, resourceName, baseUrlExpected);
        assertEquals(expected, urlProvider.getStaticPluginResourceUrl(moduleKey, resourceName, urlMode));
    }

    private String setupStaticPluginResourceWithI18nHash(
            final String moduleKey, final String resourceName, boolean baseUrlExpected) {
        when(mockWebResourceIntegration.getI18nStateHash()).thenReturn("i18n-ha$h");
        String ver = "2";
        final Plugin animalPlugin = new StaticPlugin();
        animalPlugin.setKey("confluence.extra.animal");
        animalPlugin.setPluginsVersion(5);
        animalPlugin.getPluginInformation().setVersion(ver);

        final ModuleDescriptor moduleDescriptor = TestUtils.createWebResourceModuleDescriptor(moduleKey, animalPlugin);
        when(mockPluginAccessor.getEnabledPluginModule(moduleKey)).thenReturn(moduleDescriptor);

        return (baseUrlExpected ? BASEURL : "") + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_PREFIX + "/i18n-ha$h/" + SYSTEM_BUILD_NUMBER + "/"
                + SYSTEM_COUNTER + "/" + ver + "/" + WebResourceUrlProviderImpl.STATIC_RESOURCE_SUFFIX + "/"
                + AbstractFileServerServlet.SERVLET_PATH + "/" + AbstractFileServerServlet.RESOURCE_URL_PREFIX + "/"
                + moduleKey + "/" + resourceName;
    }

    private void mockEnabledPluginModule(final String key, final ModuleDescriptor md) {
        when(mockPluginAccessor.getEnabledPluginModule(key)).thenReturn(md);
    }
}
