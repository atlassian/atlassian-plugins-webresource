package com.atlassian.plugin.webresource.condition;

import org.junit.Test;

import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import static com.atlassian.plugin.webresource.condition.ConditionTestUtils.decorate;

public class TestUrlReadingCompositeCondition {
    @Test
    public void testAddToUrl() {
        UrlReadingCondition mockCondition = mock(UrlReadingCondition.class);
        DecoratingCompositeCondition condition = createCompositeCondition(decorate(mockCondition));

        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        UrlBuildingStrategy urlBuilderStrategy = UrlBuildingStrategy.normal();
        condition.addToUrl(urlBuilder, urlBuilderStrategy);

        verify(mockCondition).addToUrl(urlBuilder);
    }

    private DecoratingCompositeCondition createCompositeCondition(final DecoratingCondition... conditions) {
        DecoratingCompositeCondition parentCondition = new DecoratingCompositeCondition() {
            @Override
            public boolean shouldDisplay(QueryParams params) {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public DecoratingCondition invertCondition() {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
        for (DecoratingCondition condition : conditions) {
            parentCondition.addCondition(condition);
        }
        return parentCondition;
    }
}
