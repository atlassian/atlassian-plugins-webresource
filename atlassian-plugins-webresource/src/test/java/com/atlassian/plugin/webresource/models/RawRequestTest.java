package com.atlassian.plugin.webresource.models;

import java.util.Collection;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.json.marshal.wrapped.JsonableString;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.DEFER;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE;

public class RawRequestTest {
    private static final Requestable JIRA_WEB_RESOURCE_CRON_EDITOR = new WebResourceKey("jira.webresources:croneditor");
    private static final Requestable CONFLUENCE_EDITOR_EMOTIONS =
            new WebResourceKey("com.atlassian.confluence.editor:emotions-resources");
    private static final Requestable CONFLUENCE_SCRIPTS_FINISHED =
            new WebResourceKey("com.atlassian.confluence.plugins.confluence-scriptsfinished-plugin:scriptsfinished");

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void when_RetrieveAllIncludedResources_Then_ReturnMergedListOfPhaseWithAllResources() {
        // when
        final RawRequest underTest = new RawRequest();
        underTest.include(REQUIRE, JIRA_WEB_RESOURCE_CRON_EDITOR);
        underTest.include(INTERACTION, CONFLUENCE_EDITOR_EMOTIONS);
        underTest.include(INTERACTION, CONFLUENCE_SCRIPTS_FINISHED);
        // then
        assertTrue(underTest.getIncluded().contains(JIRA_WEB_RESOURCE_CRON_EDITOR));
        assertTrue(underTest.getIncluded().contains(CONFLUENCE_EDITOR_EMOTIONS));
        assertTrue(underTest.getIncluded().contains(CONFLUENCE_SCRIPTS_FINISHED));
    }

    @Test
    public void when_RetrieveResourceByPhaseType_Then_ReturnExactlyTheResourcesByTheDefinedPhase() {
        // when
        final RawRequest underTest = new RawRequest();
        underTest.include(REQUIRE, JIRA_WEB_RESOURCE_CRON_EDITOR);
        underTest.include(INTERACTION, CONFLUENCE_EDITOR_EMOTIONS);
        underTest.include(INTERACTION, CONFLUENCE_SCRIPTS_FINISHED);
        // then
        final Collection<Requestable> blockingResources = underTest.getIncluded(REQUIRE);
        assertEquals(1, blockingResources.size());
        final Collection<Requestable> RENDERResources = underTest.getIncluded(INTERACTION);
        assertEquals(2, RENDERResources.size());
    }

    @Test
    public void when_IncludeNullRequestable_Then_ThrowException() {
        // then
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("The resourceToInclude resource is mandatory for the inclusion.");
        // when
        final RawRequest underTest = new RawRequest();
        underTest.include(REQUIRE, (Requestable) null);
    }

    @Test
    public void when_IncludeNullPhaseType_Then_ThrowException() {
        // then
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("The resource resourcePhase type is mandatory for the resource inclusion.");
        // when
        final RawRequest underTest = new RawRequest();
        underTest.include(null, CONFLUENCE_SCRIPTS_FINISHED);
    }

    @Test
    public void when_IncludeRawRequestResourcesToCurrentRawRequest_Then_ReturnRawRequestWithAllResources() {
        // given
        final RawRequest request = new RawRequest();
        request.include(REQUIRE, JIRA_WEB_RESOURCE_CRON_EDITOR);
        request.include(INTERACTION, CONFLUENCE_EDITOR_EMOTIONS);
        // when
        final RawRequest underTest = new RawRequest();
        underTest.include(INTERACTION, CONFLUENCE_SCRIPTS_FINISHED);
        underTest.include(REQUIRE, request);
        // then
        assertTrue(underTest.getIncluded(INTERACTION).contains(CONFLUENCE_SCRIPTS_FINISHED));
        assertTrue(underTest.getIncluded(REQUIRE).contains(JIRA_WEB_RESOURCE_CRON_EDITOR));
        assertTrue(underTest.getIncluded(REQUIRE).contains(CONFLUENCE_EDITOR_EMOTIONS));
    }

    @Test
    public void when_hasAnyOnEmptyPhase_then_ReturnFalse() {
        // given
        final RawRequest request = new RawRequest();
        request.include(REQUIRE, JIRA_WEB_RESOURCE_CRON_EDITOR);
        // then
        assertThat(request.hasAny(INTERACTION), is(false));
        assertThat(request.hasAny(DEFER), is(false));
    }

    @Test
    public void when_hasAnyOnPhaseWithRequestables_then_ReturnTrue() {
        // given
        final RawRequest request = new RawRequest();
        request.include(REQUIRE, JIRA_WEB_RESOURCE_CRON_EDITOR);
        // then
        assertThat(request.hasAny(REQUIRE), is(true));
    }

    @Test
    public void when_hasAnyOnPhaseWithData_then_ReturnTrue() {
        // given
        final RawRequest request = new RawRequest();
        request.getIncludedData(REQUIRE).put("foo", new JsonableString("bar"));
        // then
        assertThat(request.hasAny(REQUIRE), is(true));
    }
}
