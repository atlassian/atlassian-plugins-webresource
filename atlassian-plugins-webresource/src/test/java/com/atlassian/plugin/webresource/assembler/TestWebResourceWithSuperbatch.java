package com.atlassian.plugin.webresource.assembler;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.plugin.webresource.TestUtils.removeWebResourceLogs;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.*;

public class TestWebResourceWithSuperbatch {
    private AssemblerTestFixture fixture;
    private StringWriter writer;

    @Before
    public void setUp() {
        fixture = new AssemblerTestFixture();
        // Resources
        fixture.mockPlugin("test.atlassian:first-feature")
                .res("first-1.js")
                .res("first-2.js")
                .res("first-1.css")
                .res("first-2.css")
                .ctx("first", "all");
        fixture.mockPlugin("test.atlassian:second-feature")
                .res("second-1.css")
                .res("second-1.js")
                .res("second-2.css")
                .res("second-2.js")
                .ctx("second", "all");

        writer = new StringWriter();
    }

    @Test
    public void when_PassedFalseToIncludeSuperbatch_Then_SuperBatchShouldNotBeIncluded() {
        // given
        fixture.mockSuperbatch("test.atlassian:first-feature", "test.atlassian:second-feature");
        WebResourceAssembler assembler = fixture.create();

        // when
        assembler.resources().excludeSuperbatch();
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());

        // then
        assertThat(actual, equalTo(""));
    }

    @Test
    public void
            when_PassedTrueToIncludeSuperbatchResourcesAndDisabledByResourceBatchingConfiguration_Then_SuperBatchShouldDisabled() {
        // given
        fixture.mockSuperbatch(false, "test.atlassian:first-feature", "test.atlassian:second-feature");
        // Superbatch is added to excluded when ResourceBatchingConfiguration#isSuperBatchingEnabled is false
        // So includeSuperbatchResources will not enable it. Method includeSuperbatchResources suggests that it can
        // enable it
        // but this test shows that it is not true
        WebResourceAssembler assembler =
                fixture.factory().create().includeSuperbatchResources(true).build();

        // when
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());

        // then
        assertThat(actual, equalTo(""));
    }

    @Test
    public void when_RequireSuperbatchAndDisabledByResourceBatchingConfiguration_Then_SuperBatchShouldDisabled() {
        // given
        fixture.mockSuperbatch(false, "test.atlassian:first-feature", "test.atlassian:second-feature");
        // Superbatch is added to excluded when ResourceBatchingConfiguration#isSuperBatchingEnabled is false
        WebResourceAssembler assembler = fixture.create();

        // when
        assembler.resources().requireSuperbatch(DEFER);
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());

        // then
        assertThat(actual, equalTo(""));
    }

    @Test
    public void when_RequireSuperbatchWithDeferPhase_Then_SuperBatchShouldContainsDeferAttribute() throws IOException {
        // given
        // Disable sync by disabling performance tracking hardcoded in Config line ~733 by isPerformanceTrackingEnabled
        // usage.
        System.setProperty("atlassian.darkfeature.atlassian.webresource.performance.tracking.disable", "true");
        fixture.mockSuperbatch("test.atlassian:first-feature", "test.atlassian:second-feature");
        WebResourceAssembler assembler = fixture.create();

        // when
        assembler.resources().requireSuperbatch(DEFER);
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());

        // then
        assertOutput(
                actual, "when_RequireSuperbatchWithDeferPhase_Then_SuperBatchShouldContainsDeferAttribute.expected");
    }

    @Test
    public void when_DoNothing_Then_SuperBatchShouldBeIncludedByResourceBatchingConfiguration() throws IOException {
        // given
        // Disable sync by disabling performance tracking hardcoded in Config line ~733 by isPerformanceTrackingEnabled
        // usage.
        System.setProperty("atlassian.darkfeature.atlassian.webresource.performance.tracking.disable", "true");
        fixture.mockSuperbatch("test.atlassian:first-feature", "test.atlassian:second-feature");
        WebResourceAssembler assembler = fixture.create();

        // when
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());

        // then
        assertOutput(actual, "when_DoNothing_Then_SuperBatchShouldBeIncludedByResourceBatchingConfiguration.expected");
    }

    public void assertOutput(String actual, String expectedFileName) throws IOException {
        String expectedFilePath = "test-cases/test-web-resource-with-superbatch/" + expectedFileName;
        File file = new File(
                getClass().getClassLoader().getResource(expectedFilePath).getFile());
        String expectedContent = new String(Files.readAllBytes(file.toPath())).trim();

        assertThat(actual, equalTo(expectedContent));
    }
}
