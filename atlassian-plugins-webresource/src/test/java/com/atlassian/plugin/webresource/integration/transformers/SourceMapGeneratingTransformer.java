package com.atlassian.plugin.webresource.integration.transformers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.sourcemap.ReadableSourceMap;
import com.atlassian.webresource.api.QueryParams;

import static com.atlassian.sourcemap.Util.create1to1SourceMap;
import static com.atlassian.sourcemap.WritableSourceMap.toReadableSourceMap;

public class SourceMapGeneratingTransformer extends ContentTransformerHelper {
    @Override
    public Set<String> allUsedQueryParameters() {
        return Set.of();
    }

    public Content transform(
            final Content content, ResourceLocation resourceLocation, QueryParams params, final String sourceUrl) {
        return new ContentImpl(content.getContentType(), true) {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                if (!isSourceMapEnabled) {
                    return content.writeTo(out, isSourceMapEnabled);
                }

                ByteArrayOutputStream buff = new ByteArrayOutputStream();
                ReadableSourceMap inputSourceMap = content.writeTo(buff, isSourceMapEnabled);
                final String contentAsString = buff.toString();

                if (inputSourceMap != null) {
                    throw new RuntimeException(
                            "invalid usage, source map generating transformer should be the first " + "one!");
                }

                String content = buff.toString();
                try {
                    out.write(contentAsString.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return toReadableSourceMap(create1to1SourceMap(content, sourceUrl));
            }
        };
    }
}
