package com.atlassian.plugin.webresource.impl.support.factory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.impl.support.InitialContent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static com.atlassian.plugin.webresource.mocks.MockBuilder.builder;

@RunWith(MockitoJUnitRunner.class)
public class InitialSourceContentBuilderTest {

    private static final String JAVASCRIPT_FILE_NAME = "/x/y/z.js";

    /**
     * This is the file which will be used as the source for the generation of a transpiled file.
     * If we have a transpilation from ES6 to ES5, it will contain ES6 source code used for the generation of the ES5 one.
     */
    private static final String JAVASCRIPT_SOURCE_FILE =
            "let statement = 'Hello World!';\nwindow.console.log(statement);";

    private static final String JAVASCRIPT_SOURCE_FILE_NAME = "/x/y/z-source.js";

    @Mock
    private Router router;

    @Mock
    private Resource resource;

    /**
     * WHEN javascript file location equals to {@link InitialSourceContentBuilderTest#JAVASCRIPT_FILE_NAME}
     * AND source file does exist
     * THEN return source file content
     */
    @Test
    public void when_JavaScriptLocationIsNotNull_And_SourceFileExists_Then_ShouldReturnSourceFileContent() {
        // given
        builder()
                .withMockedResource(resource)
                .location(JAVASCRIPT_FILE_NAME)
                .streamFor(JAVASCRIPT_SOURCE_FILE_NAME, JAVASCRIPT_SOURCE_FILE);
        // when
        final InitialSourceContentBuilder actualInitialContentBuilder =
                new InitialSourceContentBuilder().withContent(resource);
        final InitialContentTest actualInitialContent = new InitialContentTest(actualInitialContentBuilder.build());
        // then
        assertTrue(actualInitialContent.getContent().isPresent());
        actualInitialContent
                .getContentAsString()
                .ifPresent(actualContent -> assertEquals(JAVASCRIPT_SOURCE_FILE, actualContent));
    }

    /**
     * WHEN javascript file location equals to {@link InitialSourceContentBuilderTest#JAVASCRIPT_FILE_NAME}
     * AND source file does not exist
     * THEN return empty source file content
     */
    @Test
    public void when_JavaScriptLocationIsNotNull_And_SourceFileDoesNotExist_Then_ShouldNotReturnSourceFileContent() {
        // given
        builder().withMockedResource(resource).location(JAVASCRIPT_FILE_NAME);
        // when
        final InitialSourceContentBuilder actualInitialContentBuilder =
                new InitialSourceContentBuilder().withContent(resource);
        final InitialContentTest actualInitialContent = new InitialContentTest(actualInitialContentBuilder.build());
        // then
        assertFalse(actualInitialContent.getContent().isPresent());
    }

    /**
     * WHEN javascript file location equals to {@link InitialSourceContentBuilderTest#JAVASCRIPT_FILE_NAME}
     * AND source file does exist
     * THEN return pre-build source path {@link InitialSourceContentBuilderTest#JAVASCRIPT_SOURCE_FILE_NAME}
     */
    @Test
    public void when_JavaScriptLocationIsNotNull_And_SourceFileExists_Then_ShouldReturnPreBuildSourcePath() {
        // given
        builder()
                .withMockedResource(resource)
                .location(JAVASCRIPT_FILE_NAME)
                .streamFor(JAVASCRIPT_SOURCE_FILE_NAME, JAVASCRIPT_SOURCE_FILE)
                .and()
                .withMockedRouter(router)
                .prebuildSourceUrl(resource, JAVASCRIPT_SOURCE_FILE_NAME);
        // when
        final InitialContent actualInitialContent =
                new InitialSourceContentBuilder().withPath(router, resource).build();
        // then
        assertTrue(actualInitialContent.getPath().isPresent());
        assertEquals(JAVASCRIPT_SOURCE_FILE_NAME, actualInitialContent.getPath().get());
    }

    /**
     * WHEN javascript file location equals to {@link InitialSourceContentBuilderTest#JAVASCRIPT_FILE_NAME}
     * AND source file does not exist
     * THEN return pre-build source path {@link InitialSourceContentBuilderTest#JAVASCRIPT_FILE_NAME}
     */
    @Test
    public void when_JavaScriptLocationIsNotNull_And_SourceFileDoesNotExist_Then_ShouldReturnSourcePath() {
        // given
        builder()
                .withMockedResource(resource)
                .location(JAVASCRIPT_FILE_NAME)
                .and()
                .withMockedRouter(router)
                .sourceUrl(resource, JAVASCRIPT_FILE_NAME);
        // when
        final InitialContent actualInitialContent =
                new InitialSourceContentBuilder().withPath(router, resource).build();
        // then
        assertTrue(actualInitialContent.getPath().isPresent());
        assertEquals(JAVASCRIPT_FILE_NAME, actualInitialContent.getPath().get());
    }
}
