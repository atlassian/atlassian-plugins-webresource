package com.atlassian.plugin.webresource.transformer;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import com.google.common.collect.Lists;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.integration.stub.WebResourceIntegrationImpl;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerFactory;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;

public class TestDefaultStaticTransformersSupplier {
    @Mock
    private WebResourceUrlProvider webResourceUrlProvider;

    private final CdnResourceUrlTransformer cdnResourceUrlTransformer = (url) -> url;
    private StaticTransformersSupplier staticTransformersSupplier;

    @Before
    public void setup() {
        WebResourceIntegration webResourceIntegration = WebResourceIntegrationImpl.create();
        doReturn(false).when(webResourceIntegration).usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins();
        staticTransformersSupplier = new DefaultStaticTransformersSupplier(
                webResourceIntegration, webResourceUrlProvider, cdnResourceUrlTransformer);
    }

    @Test
    public void testCssType() {
        ArrayList<WebResourceTransformerFactory> transformers =
                Lists.newArrayList(staticTransformersSupplier.get("css"));
        assertEquals(1, transformers.size());
        assertTrue(transformers.get(0) instanceof RelativeUrlTransformerFactory);
    }

    @Test
    public void testCssLocation() {
        ResourceLocation resourceLocation = createResourceLocation("my/path/blah.css", "blah.css");
        ArrayList<WebResourceTransformerFactory> transformers =
                Lists.newArrayList(staticTransformersSupplier.get(resourceLocation));
        assertEquals(1, transformers.size());
        assertTrue(transformers.get(0) instanceof RelativeUrlTransformerFactory);
    }

    @Test
    public void testLessLocation() {
        ResourceLocation resourceLocation = createResourceLocation("my/path/blah.less", "blah.css");
        ArrayList<WebResourceTransformerFactory> transformers =
                Lists.newArrayList(staticTransformersSupplier.get(resourceLocation));
        assertEquals(1, transformers.size());
        assertTrue(transformers.get(0) instanceof RelativeUrlTransformerFactory);
    }

    private static ResourceLocation createResourceLocation(String location, String name) {
        return new ResourceLocation(location, name, null, null, null, Collections.<String, String>emptyMap());
    }
}
