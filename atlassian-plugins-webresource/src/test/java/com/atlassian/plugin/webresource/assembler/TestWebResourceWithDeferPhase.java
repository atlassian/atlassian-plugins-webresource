package com.atlassian.plugin.webresource.assembler;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

import static org.apache.commons.io.IOUtils.copy;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.fail;

import static com.atlassian.plugin.webresource.TestUtils.removeWebResourceLogs;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.DEFER;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE;

public class TestWebResourceWithDeferPhase {
    private AssemblerTestFixture fixture;
    private StringWriter writer;

    @Before
    public void setUp() {
        fixture = new AssemblerTestFixture();
        // Resources
        fixture.mockPlugin("test.atlassian:first-feature")
                .res("first-1.js")
                .res("first-2.js")
                .res("first-1.css")
                .res("first-2.css")
                .ctx("first", "all");
        fixture.mockPlugin("test.atlassian:second-feature")
                .res("second-1.css")
                .res("second-1.js")
                .res("second-2.css")
                .res("second-2.js")
                .ctx("second", "all");

        fixture.mockPlugin("test.atlassian:third-feature")
                .res("third-1.css")
                .res("third-1.js")
                .deps("test.atlassian:first-feature", "test.atlassian:second-feature")
                .ctx("third", "all");

        // Page
        fixture.mockPlugin("test.atlassian:first-page", true)
                .deps("test.atlassian:first-feature")
                .deps("test.atlassian:second-feature");

        writer = new StringWriter();
    }

    @Test
    public void testLoadFeaturesWithDefer() throws IOException {
        // given
        WebResourceAssembler assembler = fixture.create();

        // when
        assembler.resources().requireWebResource(DEFER, "test.atlassian:first-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());

        // then
        assertOutput(actual, "testLoadFeaturesWithDefer.expected");
    }

    @Test
    public void testLoadFeaturesWithMixedPhases() throws IOException {
        // given
        WebResourceAssembler assembler = fixture.create();

        // when
        assembler.resources().requireWebResource(REQUIRE, "test.atlassian:first-feature");
        assembler.resources().requireWebResource(DEFER, "test.atlassian:second-feature");
        assembler.resources().requireWebResource(INTERACTION, "test.atlassian:third-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());

        // then
        assertOutput(actual, "testLoadFeaturesWithMixedPhases.expected");
    }

    @Test
    public void testLoadPageWithDefer() throws IOException {
        // given
        WebResourceAssembler assembler = fixture.create();

        // when
        assembler.resources().requirePage(DEFER, "test.atlassian:first-page");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());

        // then
        assertOutput(actual, "testLoadPageWithDefer.expected");
    }

    @Test
    public void testLoadPageWithDeferAndResourceWithInteraction() throws IOException {
        // given
        WebResourceAssembler assembler = fixture.create();

        // when
        assembler.resources().requirePage(DEFER, "test.atlassian:first-page");
        assembler.resources().requireWebResource(INTERACTION, "test.atlassian:third-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());

        // then
        assertOutput(actual, "testLoadPageWithDeferAndResourceWithInteraction.expected");
    }

    @Test
    public void testLoadContextAndWebResourceWithTheSameWebResourceWithMixedPhases() {
        // given
        WebResourceAssembler assembler = fixture.create();

        // when
        assembler.resources().requireWebResource(REQUIRE, "test.atlassian:first-feature");
        assembler.resources().requireContext(DEFER, "all");

        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());

        // then
        assertOutput(actual, "testLoadContextAndWebResourceWithTheSameWebResourceWithMixedPhases.expected");
    }

    public void assertOutput(String actual, String expectedFileName) {
        final String expectedFilePath = "/test-cases/test-web-resource-with-defer-phase/" + expectedFileName;
        try (final InputStream inputStream = getClass().getResourceAsStream(expectedFilePath)) {
            final StringWriter writer = new StringWriter();
            copy(inputStream, writer, "UTF-8");
            final String expectedContent = writer.toString();
            assertThat(actual.trim(), equalTo(expectedContent.trim()));
        } catch (final IOException exception) {
            fail("Could not read the file: " + expectedFileName);
        }
    }
}
