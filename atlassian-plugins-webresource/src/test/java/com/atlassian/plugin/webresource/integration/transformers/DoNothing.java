package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

public class DoNothing implements WebResourceTransformerFactory {
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        return urlBuilder -> {};
    }

    public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters) {
        return (resource, params) -> resource.nextResource();
    }
}
