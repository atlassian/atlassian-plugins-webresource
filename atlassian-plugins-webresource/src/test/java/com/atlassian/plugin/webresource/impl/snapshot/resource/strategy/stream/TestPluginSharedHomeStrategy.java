package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.helpers.FileOperations;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.sal.api.ApplicationProperties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestPluginSharedHomeStrategy {
    private static final String PATH_TO_RESOURCE = "path/to/resource.js";
    private static final String PLUGIN_KEY = "com.starfleet.engines.plugin";
    private static final String WEBRESOURCE_KEY = PLUGIN_KEY + ":web-resource";

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ApplicationProperties applicationProperties;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private WebResourceIntegration webResourceIntegration;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private PluginAccessor pluginAccessor;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Plugin enabledPlugin;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private FileOperations fileOperations;

    @Mock
    private Bundle bundle;

    @Before
    public void setUp() {
        when(bundle.getKey()).thenReturn(WEBRESOURCE_KEY);
        when(webResourceIntegration.getPluginAccessor()).thenReturn(pluginAccessor);
        when(pluginAccessor.getEnabledPlugin(PLUGIN_KEY)).thenReturn(enabledPlugin);
        when(enabledPlugin.getKey()).thenReturn(PLUGIN_KEY);

        when(webResourceIntegration.getApplicationProperties()).thenReturn(applicationProperties);
        when(applicationProperties.getLocalHomeDirectory()).thenReturn(Optional.of(Paths.get("/product-local-home")));
        when(applicationProperties.getSharedHomeDirectory()).thenReturn(Optional.of(Paths.get("/product-shared-home")));
    }

    @After
    public void tearDown() {}

    @Test
    public void
            givenASharedPluginHomeResource_callingGetInputStream_resolvesWithinThePluginsWebResourcesSharedHomeLocation()
                    throws IOException {
        InputStream expectedStream = new InputStream() {
            @Override
            public int read() {
                throw new RuntimeException("Not implemented");
            }
        };
        when(fileOperations.exists("/product-shared-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(true);
        when(fileOperations.createInputStream(
                        "/product-shared-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(expectedStream);
        StreamStrategy streamStrategy =
                new PluginSharedHomeStreamStrategy(fileOperations, webResourceIntegration, bundle);

        var returnedStream = streamStrategy.getInputStream(PATH_TO_RESOURCE);

        assertEquals(expectedStream, returnedStream);
    }

    @Test
    public void
            givenASharedPluginHomeResourceWithRelativePaths_callingGetInputStream_resolvesWithinThePluginsWebResourcesSharedHomeLocation()
                    throws IOException {
        InputStream expectedStream = new InputStream() {
            @Override
            public int read() {
                throw new RuntimeException("Not implemented");
            }
        };
        when(fileOperations.exists("/product-shared-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(true);
        when(fileOperations.createInputStream(
                        "/product-shared-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(expectedStream);
        StreamStrategy streamStrategy =
                new PluginSharedHomeStreamStrategy(fileOperations, webResourceIntegration, bundle);

        var returnedStream =
                streamStrategy.getInputStream("../../web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE);

        assertEquals(expectedStream, returnedStream);
    }

    @Test
    public void givenADuplicateResource_callingGetInputStream_resolvesWithinThePluginsWebResourcesSharedHomeLocation()
            throws IOException {
        InputStream sharedHomeStream = new InputStream() {
            @Override
            public int read() {
                throw new RuntimeException("Not implemented");
            }
        };
        InputStream localHomeStream = new InputStream() {
            @Override
            public int read() {
                throw new RuntimeException("Not implemented");
            }
        };
        when(fileOperations.exists("/product-shared-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(true);
        when(fileOperations.createInputStream(
                        "/product-shared-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(sharedHomeStream);
        lenient()
                .when(fileOperations.exists("/product-local-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(true);
        lenient()
                .when(fileOperations.createInputStream(
                        "/product-local-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(localHomeStream);
        StreamStrategy streamStrategy =
                new PluginSharedHomeStreamStrategy(fileOperations, webResourceIntegration, bundle);

        var returnedStream = streamStrategy.getInputStream(PATH_TO_RESOURCE);

        assertEquals(sharedHomeStream, returnedStream);
    }

    @Test
    public void givenADuplicateResourceWhenSharedHomeIsNotSet_callingGetInputStream_resolvesWithinTheLocalHomeLocation()
            throws IOException {
        when(applicationProperties.getSharedHomeDirectory()).thenReturn(Optional.empty());
        InputStream sharedHomeStream = new InputStream() {
            @Override
            public int read() {
                throw new RuntimeException("Not implemented");
            }
        };
        InputStream localHomeStream = new InputStream() {
            @Override
            public int read() {
                throw new RuntimeException("Not implemented");
            }
        };
        lenient()
                .when(fileOperations.exists(
                        "/product-shared-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(true);
        lenient()
                .when(fileOperations.createInputStream(
                        "/product-shared-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(sharedHomeStream);
        when(fileOperations.exists("/product-local-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(true);
        when(fileOperations.createInputStream(
                        "/product-local-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(localHomeStream);
        StreamStrategy streamStrategy =
                new PluginSharedHomeStreamStrategy(fileOperations, webResourceIntegration, bundle);

        var returnedStream = streamStrategy.getInputStream(PATH_TO_RESOURCE);

        assertEquals(localHomeStream, returnedStream);
    }

    @Test
    public void givenNeitherHomeIsSet_callingGetInputStream_throwsAReasonableException() {
        when(applicationProperties.getLocalHomeDirectory()).thenReturn(Optional.empty());
        when(applicationProperties.getSharedHomeDirectory()).thenReturn(Optional.empty());

        StreamStrategy streamStrategy =
                new PluginSharedHomeStreamStrategy(fileOperations, webResourceIntegration, bundle);

        assertThrows(
                "Unable to determine home directory for pluginSharedHome resolution.",
                RuntimeException.class,
                () -> streamStrategy.getInputStream(PATH_TO_RESOURCE));
    }

    @Test
    public void givenALocalResourceWhenSharedHomeIsSet_callingGetInputStream_returnsNull()
            throws FileNotFoundException {
        InputStream localHomeStream = new InputStream() {
            @Override
            public int read() {
                throw new RuntimeException("Not implemented");
            }
        };
        lenient()
                .when(fileOperations.exists("/product-local-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(true);
        lenient()
                .when(fileOperations.createInputStream(
                        "/product-local-home/web-resources/" + PLUGIN_KEY + "/" + PATH_TO_RESOURCE))
                .thenReturn(localHomeStream);
        StreamStrategy streamStrategy =
                new PluginSharedHomeStreamStrategy(fileOperations, webResourceIntegration, bundle);

        var returnedStream = streamStrategy.getInputStream(PATH_TO_RESOURCE);

        assertNull(returnedStream);
    }

    @Test
    public void givenANonExistingResource_callingGetInputStream_returnsNull() {
        StreamStrategy streamStrategy =
                new PluginSharedHomeStreamStrategy(fileOperations, webResourceIntegration, bundle);

        var returnedStream = streamStrategy.getInputStream(PATH_TO_RESOURCE);

        assertNull(returnedStream);
    }

    @Test
    public void givenAMaliciousResourcePath_callingGetInputStream_throwsAReasonableException() {
        StreamStrategy streamStrategy =
                new PluginSharedHomeStreamStrategy(fileOperations, webResourceIntegration, bundle);

        List.of("/etc/passwd", "../another-plugin/secret.js").forEach(path -> {
            assertThrows(
                    "Invalid sharedPluginHome web resource definition",
                    IllegalArgumentException.class,
                    () -> streamStrategy.getInputStream(path));
        });
    }
}
