package com.atlassian.plugin.webresource;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;

import junit.framework.TestCase;

public class TestCssWebResource extends TestCase {
    private CssWebResource cssWebResource;

    protected void setUp() throws Exception {
        super.setUp();
        cssWebResource = new CssWebResource();
    }

    protected void tearDown() throws Exception {
        cssWebResource = null;
        super.tearDown();
    }

    public void testMatches() {
        assertTrue(cssWebResource.matches("blah.css"));
        assertFalse(cssWebResource.matches("blah.js"));
    }

    public void testFormatResource() {
        final String url = "/confluence/download/resources/confluence.web.resources:master-styles/master.css";

        assertEquals(
                "<link rel=\"stylesheet\" href=\"" + url + "\" media=\"all\">\n",
                cssWebResource.formatResource(url, new HashMap<>()));
    }

    // PLUG-753
    public void testUrlIsEscaped() {
        final String url =
                "/confluence/s/en\"><script>alert(document.cookie)</script>/2153/1/1/_/download/superbatch/css/batch.css";

        assertEquals(
                "<link rel=\"stylesheet\" href=\"" + StringEscapeUtils.escapeHtml4(url) + "\" media=\"all\">\n",
                cssWebResource.formatResource(url, new HashMap<>()));
    }

    public void testFormatResourceWithParameters() {
        final String url = "/confluence/download/resources/confluence.web.resources:master-styles/master.css";
        Map<String, String> params = new LinkedHashMap<>();
        params.put("title", "Confluence Master CSS");
        params.put("charset", "utf-8");
        params.put("foo", "bar"); // test invalid parameter

        assertEquals(
                "<link rel=\"stylesheet\" href=\"" + url + "\" title=\"Confluence Master CSS\""
                        + " charset=\"utf-8\" media=\"all\">\n",
                cssWebResource.formatResource(url, params));
    }
}
