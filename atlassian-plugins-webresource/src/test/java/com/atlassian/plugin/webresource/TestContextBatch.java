package com.atlassian.plugin.webresource;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import com.atlassian.plugin.webresource.integration.transformers.AddLocation;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.superBatchUrl;
import static com.atlassian.plugin.webresource.util.HashBuilder.buildHash;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;

public class TestContextBatch {
    private WebResourceImpl wr;

    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    @Before
    public void setUp() throws Exception {
        wr = new WebResourceImpl();
        System.setProperty("atlassian.darkfeature.atlassian.webresource.performance.tracking.disable", "true");
    }

    @Test
    public void testBuildPluginResources() {
        wr.configure()
                .plugin("a.b.c")
                .webResource("d")
                .context("key")
                .resource("d1.css")
                .resource("d1.js")
                .resource("d2.css")
                .param("media", "print")
                .webResource("e")
                .context("key")
                .resource("e1.js")
                .plugin("f.g.h")
                .webResource("i")
                .context("key")
                .resource("i.js")
                .end();

        wr.requireContext("key");

        assertThat(
                wr.paths(),
                matches(
                        contextBatchUrl("key", "css"),
                        contextBatchUrl("key", "css", buildMap("media", "print")),
                        contextBatchUrl("key", "js")));
    }

    @Test
    public void testBuildPluginResourcesWithTransform() {
        wr.configure()
                .plugin("a.b.c")
                .transformer("AddLocation", AddLocation.class)
                .webResource("d")
                .context("key")
                .transformation("js", "AddLocation")
                .resource("d1.js")
                .end();

        wr.requireContext("key");

        List<String> paths = wr.paths();
        assertThat(paths, matches(contextBatchUrl("key", "js", buildMap("location", "true"))));

        assertThat(paths, matches(buildHash("location")));
        assertThat(paths, matches("-CDN/"));
    }

    @Test
    public void testBuildPluginResourcesWithTransformForMultipleType() {
        wr.configure()
                .plugin("plugin")
                .transformer("cssTransformer", CssTransformer.class)
                .transformer("jsTransformer", JsTransformer.class)
                .webResource("a")
                .transformation("css", "cssTransformer")
                .transformation("js", "jsTransformer")
                .context("key")
                .resource("a1.css")
                .resource("a1.js")
                .end();

        wr.requireContext("key");

        List<String> paths = wr.paths();
        assertThat(
                paths,
                matches(
                        contextBatchUrl("key", "css", buildMap("css-transformer", "true")),
                        contextBatchUrl("key", "js", buildMap("js-transformer", "true"))));

        assertThat(paths.get(0), not(containsString("js")));
        assertThat(paths.get(1), not(containsString("css")));
    }

    // Unspecified behavior, exists only to ensure backward compatibility.
    @Test
    public void shouldChangeDependenciesOrderInWeirdWayAfterSplittingIntoNonOverlappingContextBatches() {
        wr.configure()
                .plugin("p")
                .webResource("a")
                .dependency("p:c")
                .context("c3")
                .resource("resource1.js")
                .webResource("b")
                .context("c2")
                .resource("resource2.js")
                .webResource("c")
                .dependency("p:b")
                .dependency("p:a")
                .context("c1")
                .resource("resource3.js")
                .end();

        wr.include("_context:c1", "_context:c2", "_context:c3");

        assertThat(wr.drain().urls(), matches("/download/contextbatch/js/c1,c3,c2/batch.js"));
    }

    @Test
    public void shouldOrderContextsSameWayAsInPreviousImplementation() {
        wr.configure()
                .plugin("plugin1")
                .webResource("webResource2")
                .dependency("plugin1:webResource3")
                .resource("resource8.js")
                .webResource("webResource3")
                .context("context3")
                .resource("resource3.js")
                .webResource("webResource4")
                .dependency("plugin1:webResource2")
                .context("context1")
                .context("context2")
                .resource("resource4.js")
                .end();

        wr.include("_context:context2", "_context:context3", "_context:context1");

        assertThat(wr.drain().urls(), matches(contextBatchUrl("context2,context1,context3", "js")));
    }

    @Test
    public void shouldRemoveContextsThatHaveBeenAlreadyIncludedInSuperBatch() {
        wr.configure()
                .addToSuperbatch("p:a")
                .plugin("p")
                .webResource("a")
                .context("c1")
                .context("c2")
                .resource("a1.js")
                .webResource("b")
                .context("c2")
                .resource("a1.js")
                .end();

        wr.include("_context:c1", "_context:c2");

        assertThat(wr.drain().urls(), matches(superBatchUrl("js"), contextBatchUrl("c2,-_super", "js")));
    }

    @Test
    public void shouldExcludeDependenciesIncludedInSuperBatchWhenDetectingOverlappedContexts() {
        wr.configure()
                .addToSuperbatch("p:super")
                .plugin("p")
                .webResource("super")
                .resource("super1.js")
                .webResource("b")
                .dependency("p:super")
                .context("c2")
                .resource("b1.js")
                .webResource("d")
                .dependency("p:super")
                .context("c1")
                .resource("d1.js")
                .end();

        wr.include("_context:c1", "_context:c2");

        assertThat(
                wr.drain().urls(),
                matches(superBatchUrl("js"), contextBatchUrl("c1,-_super", "js"), contextBatchUrl("c2,-_super", "js")));
    }

    public static class JsTransformer implements WebResourceTransformerFactory {
        public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
            return new TransformerUrlBuilder() {
                public void addToUrl(UrlBuilder urlBuilder) {
                    urlBuilder.addToQueryString("js-transformer", "true");
                }
            };
        }

        public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters) {
            return null;
        }
    }

    public static class CssTransformer implements WebResourceTransformerFactory {
        public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
            return new TransformerUrlBuilder() {
                public void addToUrl(UrlBuilder urlBuilder) {
                    urlBuilder.addToQueryString("css-transformer", "true");
                }
            };
        }

        public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters) {
            return null;
        }
    }
}
