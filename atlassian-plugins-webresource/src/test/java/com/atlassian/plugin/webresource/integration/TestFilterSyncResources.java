package com.atlassian.plugin.webresource.integration;

import java.io.StringWriter;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;

public class TestFilterSyncResources extends TestCase {
    @Before
    public void setup() {
        wr.configure()
                .addToSyncbatch("plugin:system")
                .plugin("plugin")
                .webResource("foo")
                .resource("foo.js")
                .resource("foo.css")
                .webResource("system")
                .resource("almond.js")
                .resource("system.css")
                .end();
    }

    @Test
    public void when_cssPredicateProvided_then_shouldOnlyOutputCss() {
        // given
        WebResourceAssembler webResourceAssembler = wr.getWebResourceAssemblerFactory()
                .create()
                .includeSuperbatchResources(false)
                .build();
        webResourceAssembler.resources().requireWebResource("plugin:foo");

        // when
        StringWriter cssOut = new StringWriter();
        webResourceAssembler
                .assembled()
                .peek()
                .writeHtmlTags(cssOut, UrlMode.AUTO, PluginCssResource.class::isInstance);
        String cssResult = cssOut.toString();

        // then
        assertThat(
                cssResult,
                allOf(
                        containsString("<link"),
                        containsString("css"),
                        not(containsString("content of almond.js")),
                        not(containsString("js")),
                        not(containsString("<script"))));
    }

    @Test
    public void when_jsPredicateProvided_then_shouldOnlyOutputJs() {
        // given
        WebResourceAssembler webResourceAssembler = wr.getWebResourceAssemblerFactory()
                .create()
                .includeSuperbatchResources(false)
                .build();
        webResourceAssembler.resources().requireWebResource("plugin:foo");

        // when
        StringWriter jsOut = new StringWriter();
        webResourceAssembler.assembled().peek().writeHtmlTags(jsOut, UrlMode.AUTO, PluginJsResource.class::isInstance);
        String jsResult = jsOut.toString();

        // then
        assertThat(
                jsResult,
                allOf(
                        containsString("content of almond.js"),
                        containsString("<script"),
                        not(containsString("css")),
                        not(containsString("<link"))));
    }

    @Test
    public void when_falsePredicateProvided_then_shouldOutputNothing() {
        // given
        WebResourceAssembler webResourceAssembler = wr.getWebResourceAssemblerFactory()
                .create()
                .includeSuperbatchResources(false)
                .build();
        webResourceAssembler.resources().requireWebResource("plugin:foo");

        // when
        StringWriter falseOut = new StringWriter();
        webResourceAssembler.assembled().peek().writeHtmlTags(falseOut, UrlMode.AUTO, webResource -> false);
        String noResult = falseOut.toString();

        // then
        assertThat(noResult, isEmptyString());
    }
}
