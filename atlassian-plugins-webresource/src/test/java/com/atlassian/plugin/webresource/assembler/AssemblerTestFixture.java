package com.atlassian.plugin.webresource.assembler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.ServletContext;

import org.hamcrest.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.PluginResourceLocatorImpl;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.integration.stub.WebResourceIntegrationImpl;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.CompleteWebResourceKey;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.data.PluginDataResource;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.atlassian.webresource.api.url.UrlBuilder;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.apache.commons.io.IOUtils.toInputStream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.endsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.webresource.TestUtils.emptyStaticTransformers;

public class AssemblerTestFixture {
    public static final String SUPERBATCH_VERSION = "1";
    private PluginResourceLocatorImpl pluginResourceLocator;
    private WebResourceIntegration mockWebResourceIntegration;
    private PluginAccessor mockPluginAccessor = mock(PluginAccessor.class);
    private WebResourceUrlProvider webResourceUrlProvider = mock(WebResourceUrlProvider.class);
    private ResourceBatchingConfiguration mockBatchingConfiguration = mock(ResourceBatchingConfiguration.class);
    private PluginEventManager mockEventPublisher = mock(PluginEventManager.class);
    private Plugin testPlugin;
    private boolean useHashInUrls = false;
    private List<WebResourceModuleDescriptor> createdWebResources = new ArrayList<>();
    private ServletContext servletContext = mock(ServletContext.class);
    private ServletContextFactory servletContextFactory = mock(ServletContextFactory.class);

    public AssemblerTestFixture() {
        this(true, true);
    }

    public AssemblerTestFixture(final boolean pluginWebResourceBatchingEnabled, final boolean contextBatchingEnabled) {
        when(servletContextFactory.getServletContext()).thenReturn(servletContext);

        mockWebResourceIntegration = WebResourceIntegrationImpl.create();
        when(mockWebResourceIntegration.getSyncWebResourceKeys()).thenReturn(emptyList());
        when(mockWebResourceIntegration.getPluginAccessor()).thenReturn(mockPluginAccessor);
        when(mockWebResourceIntegration.getSuperBatchVersion()).thenReturn(SUPERBATCH_VERSION);

        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
                .thenReturn(createdWebResources);

        when(mockBatchingConfiguration.isPluginWebResourceBatchingEnabled())
                .thenReturn(pluginWebResourceBatchingEnabled, contextBatchingEnabled);
        when(mockBatchingConfiguration.isContextBatchingEnabled()).thenReturn(true);
        when(mockBatchingConfiguration.isSuperBatchingEnabled()).thenReturn(true);
        when(webResourceUrlProvider.getStaticResourcePrefix(anyString(), anyString(), any(UrlMode.class)))
                .thenAnswer(new Answer<String>() {
                    @Override
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                        if (useHashInUrls) {
                            return invocationOnMock.getArguments()[0].toString();
                        } else {
                            return "";
                        }
                    }
                });
        testPlugin = TestUtils.createTestPlugin();
    }

    public static void assertResolvedResources(WebResourceSet actual, String... expectedResourceNames) {
        assertResolvedResources(getWebUrlResources(actual), expectedResourceNames);
    }

    public static <T extends PluginUrlResource> void assertResolvedResources(
            Iterable<T> resources, String... expectedResourceNames) {
        Iterable<String> actualModuleKeys = Iterables.transform(resources, (Function<PluginUrlResource, String>)
                input -> input.getStaticUrl(com.atlassian.webresource.api.UrlMode.RELATIVE));
        assertEquals(asList(expectedResourceNames), newArrayList(actualModuleKeys));
    }

    public static void assertResolvedData(WebResourceSet resources, String... expectedDataNames) {
        final Iterable<String> actualModuleKeys =
                Iterables.transform(getWebDataResources(resources), PluginDataResource::getKey);
        final String reason = expectedDataNames.length == 0
                ? "No data expected, but some was provided"
                : "Not all expected data was provided";
        assertThat(reason, actualModuleKeys, Matchers.containsInAnyOrder(expectedDataNames));
    }

    public static Iterable<PluginUrlResource> getWebUrlResources(WebResourceSet webResources) {
        return webResources.getResources(PluginUrlResource.class);
    }

    public static Iterable<PluginDataResource> getWebDataResources(WebResourceSet webResources) {
        return webResources.getResources(PluginDataResource.class);
    }

    public static String getHtmlTags(WebResourceSet webResources) {
        StringWriter writer = new StringWriter();
        webResources.writeHtmlTags(writer, com.atlassian.webresource.api.UrlMode.AUTO);
        return writer.toString();
    }

    public static List<String> listOf(String... values) {
        return asList(values);
    }

    public static Set<String> setOf(String... values) {
        return new LinkedHashSet<>(listOf(values));
    }

    public static String superbatch() {
        return superbatch(Types.JS);
    }

    public static String superbatch(Types type) {
        return superbatch(type, null);
    }

    public static String superbatch(String queryString) {
        return superbatch(Types.JS, queryString);
    }

    public static String superbatch(Types type, String queryString) {
        String url = "/download/contextbatch/" + type + "/_super/batch." + type;
        if (null != queryString) {
            url += queryString;
        }
        return url;
    }

    public static String m(String moduleCompleteKey) {
        return m(moduleCompleteKey, "js");
    }

    public static String m(String moduleCompleteKey, String extension) {
        return m(moduleCompleteKey, extension, null);
    }

    public static String q(String key, String value) {
        // TODO URL escape!
        return key + "=" + value;
    }

    public static String m(String moduleCompleteKey, String extension, String queryString) {
        return "/download/batch/" + moduleCompleteKey + "/" + moduleCompleteKey + "." + extension
                + ((queryString != null) ? "?" + queryString : "");
    }

    public static String c(String... contexts) {
        return "/download/contextbatch/js/" + Joiner.on(",").join(contexts) + "/batch.js";
    }

    public static void assertPrefixedUrlIs(PluginUrlResource resource, String expectedTail) {
        String[] actualFullUrl = splitPrefix(resource.getStaticUrl(com.atlassian.webresource.api.UrlMode.ABSOLUTE));
        String actualPrefix = actualFullUrl[0];
        String actualTail = actualFullUrl[1];
        assertEquals(expectedTail, actualTail);

        String expectedCdnSuffix = "-CDN";
        assertTrue("expected " + expectedCdnSuffix + " in " + actualPrefix, actualPrefix.endsWith(expectedCdnSuffix));
    }

    private static String[] splitPrefix(String staticUrl) {
        int i = staticUrl.indexOf('/');
        assertTrue("contains a slash " + staticUrl, i != -1);
        return new String[] {staticUrl.substring(0, i), staticUrl.substring(i, staticUrl.length())};
    }

    public static String u(String resource) {
        return "/download/resources/" + resource + ".js";
    }

    public AssemblerTestFixture mockContent(final String filepath, final String content) {
        final InputStream is = toInputStream(content, "UTF-8");
        when(servletContext.getResourceAsStream(filepath)).thenReturn(is);
        when(servletContext.getResourceAsStream("/" + filepath)).thenReturn(is);
        return this;
    }

    public AssemblerTestFixture prefixUrlsWithHash(boolean prefix) {
        this.useHashInUrls = prefix;
        return this;
    }

    public DefaultWebResourceAssemblerFactory factory() {
        return factory(emptyStaticTransformers());
    }

    public DefaultWebResourceAssemblerFactory factory(StaticTransformers staticTransformers) {
        pluginResourceLocator = new PluginResourceLocatorImpl(
                mockWebResourceIntegration,
                servletContextFactory,
                webResourceUrlProvider,
                mockBatchingConfiguration,
                mockEventPublisher,
                staticTransformers);
        Globals globals = pluginResourceLocator.getGlobals();
        // make sure that snapshot is generated
        globals.getSnapshot();
        return new DefaultWebResourceAssemblerFactory(globals);
    }

    public WebResourceAssembler create() {
        return factory().create().build();
    }

    public void mockSyncbatch(String... syncbatchModuleKeys) {
        List<CompleteWebResourceKey> keys = Arrays.asList(syncbatchModuleKeys).stream()
                .map(key -> key.split(":"))
                .map(parts -> new CompleteWebResourceKey(parts[0], parts[1]))
                .collect(Collectors.toList());
        when(mockWebResourceIntegration.getSyncWebResourceKeys()).thenReturn(keys);
    }

    public void mockSuperbatch(String... superbatchModuleKeys) {
        mockSuperbatch(true, superbatchModuleKeys);
    }

    public void mockSuperbatch(final boolean enabled, String... superbatchModuleKeys) {
        when(mockBatchingConfiguration.isSuperBatchingEnabled()).thenReturn(enabled);
        when(mockBatchingConfiguration.getSuperBatchModuleCompleteKeys()).thenReturn(asList(superbatchModuleKeys));
        when(mockWebResourceIntegration.getSuperBatchVersion()).thenReturn(SUPERBATCH_VERSION);
    }

    public MockPluginBuilder mockPlugin(String moduleCompleteKey) {
        return new MockPluginBuilder(servletContext, moduleCompleteKey, false);
    }

    public MockPluginBuilder mockPlugin(String moduleCompleteKey, boolean isRootPage) {
        return new MockPluginBuilder(servletContext, moduleCompleteKey, isRootPage);
    }

    public MockConditionBuilder condition() {
        return new MockConditionBuilder();
    }

    public enum Types {
        CSS("css"),
        JS("js");
        private String type;

        Types(String type) {
            this.type = type;
        }

        public String toString() {
            return type;
        }
    }

    public interface UrlAdderFunction extends Function<UrlBuilder, Void> {}

    public static class MockConditionBuilder {
        private UrlAdderFunction urlAdderFunction;
        private Supplier<Boolean> shouldDisplay;
        private boolean invert;

        public MockConditionBuilder urlAdder(UrlAdderFunction urlAdderFunction) {
            this.urlAdderFunction = urlAdderFunction;
            return this;
        }

        public MockConditionBuilder shouldDisplay(boolean shouldDisplay) {
            return this.shouldDisplay(new ConditionSupplier(shouldDisplay));
        }

        public MockConditionBuilder shouldDisplay(ConditionSupplier shouldDisplay) {
            this.shouldDisplay = shouldDisplay;
            return this;
        }

        public MockConditionBuilder invert(boolean invert) {
            this.invert = invert;
            return this;
        }

        DecoratingCondition build() {
            class MockCondition implements DecoratingCondition {
                @Override
                public void addToUrl(UrlBuilder urlBuilder, UrlBuildingStrategy urlBuilderStrategy) {
                    if (urlAdderFunction != null) {
                        urlAdderFunction.apply(urlBuilder);
                    }
                }

                @Override
                public boolean shouldDisplay(QueryParams params) {
                    return shouldDisplay.get();
                }

                @Override
                public DecoratingCondition invertCondition() {
                    throw new UnsupportedOperationException("Not implemented");
                }
            }
            return new MockCondition();
        }
    }

    public static class ConditionSupplier implements Supplier<Boolean> {
        private boolean value;

        public ConditionSupplier(boolean value) {
            this.value = value;
        }

        public void set(Boolean value) {
            this.value = value;
        }

        @Override
        public Boolean get() {
            return this.value;
        }
    }

    public class MockPluginBuilder {
        private final List<ResourceDescriptor> resourceDescriptors = new LinkedList<>();
        private final List<String> dependencies = new LinkedList<>();
        private final Set<String> contextDependencies = new LinkedHashSet<>();
        private final Set<String> contexts = new HashSet<>();
        private final Map<String, WebResourceDataProvider> dataProviders = new LinkedHashMap<>();
        private final List<WebResourceTransformation> transformers = new LinkedList<>();
        private DecoratingCondition condition = null;
        private ServletContext servletContext;

        public MockPluginBuilder(ServletContext servletContext, String moduleCompleteKey, boolean isRootPage) {
            this.servletContext = servletContext;
            Supplier<DecoratingCondition> conditionSupplier = new Supplier<DecoratingCondition>() {
                @Override
                public DecoratingCondition get() {
                    return condition;
                }
            };
            WebResourceModuleDescriptor webResource = TestUtils.createWebResourceModuleDescriptor(
                    moduleCompleteKey,
                    testPlugin,
                    resourceDescriptors,
                    dependencies,
                    contextDependencies,
                    contexts,
                    transformers,
                    conditionSupplier,
                    dataProviders,
                    isRootPage);
            when(mockPluginAccessor.getEnabledPluginModule(moduleCompleteKey))
                    .thenReturn((ModuleDescriptor) webResource);
            createdWebResources.add(webResource);
        }

        /**
         * Adds a default resource, as resources are not rendered if they contain no resources
         *
         * @return this
         */
        public MockPluginBuilder defRes() {
            final String filename = "placeholder.js";
            final String contents = "placeholder content";
            resourceDescriptors.addAll(TestUtils.createResourceDescriptors(filename));
            when(servletContext.getResourceAsStream(endsWith(filename)))
                    .thenReturn(new ByteArrayInputStream(contents.getBytes(StandardCharsets.UTF_8)));
            return this;
        }

        public MockPluginBuilder res(String... resources) {
            resourceDescriptors.addAll(TestUtils.createResourceDescriptors(resources));
            return this;
        }

        public MockPluginBuilder paramRes(String resource, Map<String, String> params) {
            resourceDescriptors.add(TestUtils.createResourceDescriptor(resource, params));
            return this;
        }

        public MockPluginBuilder deps(String... dependencies) {
            this.dependencies.addAll(asList(dependencies));
            return this;
        }

        public MockPluginBuilder ctxDeps(String... ctxDeps) {
            this.contextDependencies.addAll(asList(ctxDeps));
            return this;
        }

        public MockPluginBuilder ctx(String... contexts) {
            this.contexts.addAll(asList(contexts));
            return this;
        }

        public MockPluginBuilder data(String name, WebResourceDataProvider value) {
            this.dataProviders.put(name, value);
            return this;
        }

        public MockPluginBuilder transform(WebResourceTransformation transform) {
            transformers.add(transform);
            return this;
        }

        public MockPluginBuilder condition(DecoratingCondition condition) {
            this.condition = condition;
            return this;
        }
    }
}
