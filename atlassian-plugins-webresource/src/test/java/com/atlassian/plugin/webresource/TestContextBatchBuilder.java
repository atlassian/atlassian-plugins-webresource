package com.atlassian.plugin.webresource;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;

import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;

import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.indexOf;
import static com.google.common.collect.Iterables.size;
import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.createResourceDescriptor;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.setOf;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;

public class TestContextBatchBuilder {
    private WebResourceImpl wr;
    private WebResource.ConfigurationData configurationData;
    private WebResource.PluginData pluginData;

    @Before
    public void setUp() throws Exception {
        wr = new WebResourceImpl();
        configurationData = new WebResource.ConfigurationData();
        configurationData.isContextBatchingEnabled = true;
        configurationData.isWebResourceBatchingEnabled = true;
        pluginData = new WebResource.PluginData();
        pluginData.key = "test.atlassian";
        configurationData.plugins.add(pluginData);
    }

    @Test
    public void testNoOverlapAndNoDependencies() {
        wr.configure()
                .plugin("test.atlassian")
                .webResource("xavier-resources")
                .context("xmen")
                .resource("professorx.js")
                .resource("professorx.css")
                .resource("cyclops.css")
                .webResource("magneto-resources")
                .context("brotherhood")
                .resource("magneto.js")
                .resource("magneto.css")
                .resource("sabretooth.css")
                .end();

        wr.requireContext("xmen");
        wr.requireContext("brotherhood");

        assertThat(
                wr.paths(),
                matches(
                        contextBatchUrl("xmen", "css"),
                        contextBatchUrl("brotherhood", "css"),
                        contextBatchUrl("xmen", "js"),
                        contextBatchUrl("brotherhood", "js")));
    }

    @Test
    public void testOverlappingAndNoDependencies() throws Exception {
        wr.configure()
                .plugin("test.atlassian")
                .webResource("xavier-resources")
                .context("xmen")
                .context("brotherhood")
                .resource("professorx.js")
                .resource("professorx.css")
                .resource("cyclops.css")
                .webResource("magneto-resources")
                .context("xmen")
                .context("brotherhood")
                .resource("magneto.js")
                .resource("magneto.css")
                .resource("sabretooth.css")
                .end();

        wr.requireContext("xmen");
        wr.requireContext("brotherhood");

        assertThat(
                wr.paths(),
                matches(contextBatchUrl("xmen,brotherhood", "css"), contextBatchUrl("xmen,brotherhood", "js")));
    }

    @Test
    public void testDependenciesNoOverlap() throws Exception {
        wr.configure()
                .plugin("test.atlassian")
                .webResource("xavier-resources")
                .context("xmen")
                .resource("professorx.js")
                .resource("professorx.css")
                .resource("cyclops.css")
                .webResource("magneto-resources")
                .context("brotherhood")
                .resource("magneto.js")
                .resource("magneto.css")
                .resource("sabretooth.css")
                .webResource("students-resources")
                .context("xmen")
                .resource("iceman.js")
                .resource("iceman.css")
                .resource("rogue.css")
                .webResource("evil-students-resources")
                .context("brotherhood")
                .resource("pyro.css")
                .end();

        wr.requireContext("xmen");
        wr.requireContext("brotherhood");

        assertThat(
                wr.paths(),
                matches(
                        contextBatchUrl("xmen", "css"),
                        contextBatchUrl("brotherhood", "css"),
                        contextBatchUrl("xmen", "js"),
                        contextBatchUrl("brotherhood", "js")));
    }

    @Test
    public void testOverlappingDependencies() throws Exception {
        wr.configure()
                .plugin("test.atlassian")
                .webResource("xavier-resources")
                .context("xmen")
                .context("government")
                .resource("professorx.js")
                .resource("professorx.css")
                .resource("cyclops.css")
                .webResource("magneto-resources")
                .context("brotherhood")
                .resource("magneto.js")
                .resource("magneto.css")
                .resource("sabretooth.css")
                .webResource("new-mutants-resources")
                .context("xmen")
                .resource("iceman.js")
                .resource("iceman.css")
                .resource("rogue.css")
                .webResource("government-resources")
                .context("government")
                .resource("beast.css")
                .resource("beast.js")
                .resource("deathstrike.css")
                .end();

        wr.requireContext("xmen");
        wr.requireContext("government");

        assertThat(
                wr.paths(),
                matches(contextBatchUrl("xmen,government", "css"), contextBatchUrl("xmen,government", "js")));
    }

    @Test
    public void testMultipleOverlappingContexts() throws Exception {
        wr.configure()
                .plugin("test.atlassian")
                .webResource("xavier-resources")
                .context("xmen")
                .context("rogue")
                .resource("professorx.js")
                .resource("professorx.css")
                .resource("cyclops.css")
                .webResource("magneto-resources")
                .context("rogue")
                .context("brotherhood")
                .resource("magneto.js")
                .resource("magneto.css")
                .resource("sabretooth.css")
                .webResource("new-mutants-resources")
                .context("xmen")
                .resource("iceman.js")
                .resource("iceman.css")
                .resource("rogue.css")
                .webResource("rogue-resources")
                .context("rogue")
                .resource("pyro.js")
                .resource("phoenix.css")
                .resource("mystique.css")
                .webResource("normal-resources")
                .context("normals")
                .resource("stryker.css")
                .resource("stryker.js")
                .end();

        wr.requireContext("xmen");
        wr.requireContext("brotherhood");
        wr.requireContext("rogue");
        wr.requireContext("normals");

        assertThat(
                wr.paths(),
                matches(
                        contextBatchUrl("xmen,rogue,brotherhood", "css"),
                        contextBatchUrl("normals", "css"),
                        contextBatchUrl("xmen,rogue,brotherhood", "js"),
                        contextBatchUrl("normals", "js")));
    }

    @Test
    public void testContextParamsInDependencies() {
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "rogue";
        final List<String> contexts = new ArrayList<>();
        contexts.add(context1);
        contexts.add(context2);
        contexts.add(context3);

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final List<ResourceDescriptor> resourceDescriptors1 = asList(
                createResourceDescriptor("professorx.js"),
                createResourceDescriptor("professorx.css", buildMap("media", "print")),
                createResourceDescriptor("cyclops.js"));
        final List<ResourceDescriptor> resourceDescriptors2 = asList(
                createResourceDescriptor("magneto.js"),
                createResourceDescriptor("magneto.css", buildMap("media", "print")),
                createResourceDescriptor("sabretooth.css"));

        final String dependentModule1 = "rogue-resources";
        final List<ResourceDescriptor> dependentResourceDescriptors1 = asList(
                createResourceDescriptor("nightcrawler.js"),
                createResourceDescriptor("nightcrawler.css"),
                createResourceDescriptor("gambit.css"));

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addContext(context1, asList(moduleKey1));
        addModuleDescriptor(dependentModule1, dependentResourceDescriptors1);
        addContext(context3, asList(moduleKey1, dependentModule1));
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addContext(context2, asList(moduleKey2));

        Iterable<String> resources = requireContexts(contexts);

        // We currently batch all resource params even if there isn't any overlap in a particular context/param
        // combination
        assertEquals(6, size(resources));
        assertNotNull(
                find(resources, new IsResourceWithUrl("/download/contextbatch/css/brotherhood/batch.css?media=print")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/js/brotherhood/batch.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/css/brotherhood/batch.css")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/js/xmen,rogue/batch.js")));
        assertNotNull(
                find(resources, new IsResourceWithUrl("/download/contextbatch/css/xmen,rogue/batch.css?media=print")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/css/xmen,rogue/batch.css")));

        assertResourceOrder(
                resources,
                "/download/contextbatch/css/brotherhood/batch.css",
                "/download/contextbatch/css/brotherhood/batch.css?media=print");
        assertResourceOrder(
                resources,
                "/download/contextbatch/css/xmen,rogue/batch.css",
                "/download/contextbatch/css/xmen,rogue/batch.css?media=print");
    }

    @Test
    public void testMultipleContextsWithoutBatching() throws Exception {
        configurationData.isContextBatchingEnabled = false;
        configurationData.isWebResourceBatchingEnabled = false;
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final List<String> contexts = new ArrayList<>();
        contexts.add(context1);
        contexts.add(context2);

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addContext(context1, asList(moduleKey1, moduleKey2));
        addContext(context2, asList(moduleKey1, moduleKey2));

        Iterable<String> resources = requireContexts(contexts);

        assertEquals(6, size(resources));
        assertNotNull(find(
                resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.js")));
        assertNotNull(find(
                resources,
                new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.css")));
        assertNotNull(find(
                resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/cyclops.css")));
        assertNotNull(find(
                resources, new IsResourceWithUrl("/download/resources/test.atlassian:magneto-resources/magneto.js")));
        assertNotNull(find(
                resources, new IsResourceWithUrl("/download/resources/test.atlassian:magneto-resources/magneto.css")));
        assertNotNull(find(
                resources,
                new IsResourceWithUrl("/download/resources/test" + ".atlassian:magneto-resources/sabretooth.css")));
    }

    @Test
    public void testExcludedContextWithNoOverlap() throws Exception {
        final String context1 = "xmen";
        final String context2 = "brotherhood";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 =
                TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, asList(moduleKey1, moduleKey3));
        addContext(context2, asList(moduleKey2));

        Iterable<String> resources = requireContexts(singletonList(context1), singleton(context2));

        assertEquals(2, size(resources));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/js/xmen/batch.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/css/xmen/batch.css")));
    }

    @Test
    public void testExcludedContextWithOverlap() throws Exception {
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "xapes";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 =
                TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, asList(moduleKey1, moduleKey2, moduleKey3));
        addContext(context2, asList(moduleKey2));
        addContext(context3, asList(moduleKey3));

        final Iterable<String> resources = requireContexts(asList(context1, context2), singleton(context3));

        assertEquals(2, size(resources));
        assertNotNull(
                find(resources, new IsResourceWithUrl("/download/contextbatch/js/xmen,brotherhood,-xapes/batch.js")));
        assertNotNull(find(
                resources, new IsResourceWithUrl("/download/contextbatch/css/xmen,brotherhood," + "-xapes/batch.css")));
    }

    @Test
    public void testMultipleExcludedContextWithOverlap() throws Exception {
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "xapes";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("professorx.js", "cyclops.js");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 =
                TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, asList(moduleKey1, moduleKey2, moduleKey3));
        addContext(context2, asList(moduleKey2));
        addContext(context3, asList(moduleKey3));

        Iterable<String> resources = requireContexts(singletonList(context1), ImmutableSet.of(context2, context3));

        assertEquals(1, size(resources));
        assertNotNull(
                find(resources, new IsResourceWithUrl("/download/contextbatch/js/xmen,-xapes,-brotherhood/batch.js")));
    }

    @Test
    public void testMultipleExcludedContextWithPartialOverlap() throws Exception {
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "xapes";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 =
                TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, asList(moduleKey1, moduleKey2));
        addContext(context2, asList(moduleKey2));
        addContext(context3, asList(moduleKey3));

        Iterable<String> resources = requireContexts(singletonList(context1), ImmutableSet.of(context2, context3));

        assertEquals(2, size(resources));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/js/xmen,-brotherhood/batch.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/css/xmen,-brotherhood/batch.css")));
    }

    @Test
    public void testIncludeAndExcludeLeavesNoResourcesInBatch() throws Exception {
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "xapes";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 =
                TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, asList(moduleKey1, moduleKey2, moduleKey3));
        addContext(context2, asList(moduleKey2, moduleKey1));
        addContext(context3, asList(moduleKey3));

        Iterable<String> resources = requireContexts(singletonList(context1), ImmutableSet.of(context2, context3));

        assertEquals(0, size(resources));
    }

    // You shouldn't be doing this but it could be easily done by accident.
    @Test
    public void testIncludeAndExcludeTheSameBatch() throws Exception {
        final String context1 = "xmen";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addContext(context1, asList(moduleKey1, moduleKey2));

        Iterable<String> resources = requireContexts(singletonList(context1), singleton(context1));

        assertEquals(0, size(resources));
    }

    @Test
    public void testExcludedContextWithNoOverlapUnbatched() throws Exception {
        configurationData.isContextBatchingEnabled = false;
        configurationData.isWebResourceBatchingEnabled = false;
        final String context1 = "xmen";
        final String context2 = "brotherhood";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addContext(context1, singletonList(moduleKey1));
        addContext(context2, singletonList(moduleKey2));

        final Iterable<String> resources = requireContexts(singletonList(context2), singleton(context1));

        assertEquals(3, size(resources));
        assertNotNull(find(
                resources, new IsResourceWithUrl("/download/resources/test.atlassian:magneto-resources/magneto.js")));
        assertNotNull(find(
                resources, new IsResourceWithUrl("/download/resources/test.atlassian:magneto-resources/magneto.css")));
        assertNotNull(find(
                resources,
                new IsResourceWithUrl("/download/resources/test.atlassian:magneto-resources/sabretooth.css")));
    }

    @Test
    public void testExcludedContextWithOverlapUnbatched() throws Exception {
        configurationData.isContextBatchingEnabled = false;
        configurationData.isWebResourceBatchingEnabled = false;
        final String context1 = "xmen";
        final String context2 = "brotherhood";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addContext(context1, asList(moduleKey1, moduleKey2));
        addContext(context2, singletonList(moduleKey2));

        final Iterable<String> resources = requireContexts(singletonList(context1), singleton(context2));

        assertEquals(3, size(resources));
        assertNotNull(find(
                resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.js")));
        assertNotNull(find(
                resources,
                new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.css")));
        assertNotNull(find(
                resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/cyclops.css")));
    }

    @Test
    public void testMultipleExcludedContextWithSomeOverlapUnbatched() throws Exception {
        configurationData.isContextBatchingEnabled = false;
        configurationData.isWebResourceBatchingEnabled = false;
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "xapes";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 =
                TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, asList(moduleKey1, moduleKey3));
        addContext(context2, asList(moduleKey2, moduleKey3));
        addContext(context3, asList(moduleKey3));

        Iterable<String> resources = requireContexts(singletonList(context1), ImmutableSet.of(context2, context3));

        assertEquals(3, size(resources));
        assertNotNull(find(
                resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.js")));
        assertNotNull(find(
                resources,
                new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.css")));
        assertNotNull(find(
                resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/cyclops.css")));
    }

    private void addContext(final String context, final List<String> descriptors) {
        for (final String moduleKey : descriptors) {
            for (WebResource.WebResourceData webResourceData : pluginData.webResources) {
                if (webResourceData.key.equals(moduleKey)) {
                    webResourceData.contexts.add(context);
                }
            }
        }
    }

    private void addModuleDescriptor(final String moduleKey, final List<ResourceDescriptor> descriptors) {
        WebResource.WebResourceData webResourceData = new WebResource.WebResourceData();
        webResourceData.key = moduleKey;
        pluginData.webResources.add(webResourceData);
        for (ResourceDescriptor d : descriptors) {
            WebResource.ResourceData resourceData = new WebResource.ResourceData();
            resourceData.name = d.getName();
            resourceData.location = d.getName();
            resourceData.params = d.getParameters();
            webResourceData.resources.add(resourceData);
        }
    }

    private void assertResourceOrder(
            final Iterable<String> resources, final String supposedlyBefore, final String supposedlyAfter) {
        int resource1Idx = indexOf(resources, new IsResourceWithUrl(supposedlyBefore));
        int resource2Idx = indexOf(resources, new IsResourceWithUrl(supposedlyAfter));

        assertThat(
                "resource [" + supposedlyBefore + "] is expected in the result ",
                resource1Idx,
                greaterThanOrEqualTo(0));
        assertThat(
                "resource [" + supposedlyAfter + "] is expected in the result ", resource2Idx, greaterThanOrEqualTo(0));
        assertThat(
                "resource [" + supposedlyBefore + "] must come before resource [" + supposedlyAfter + "] ",
                resource1Idx,
                lessThan(resource2Idx));
    }

    private Iterable<String> requireContexts(List<String> included) {
        return requireContexts(included, new HashSet<>());
    }

    private Iterable<String> requireContexts(List<String> included, Set<String> excluded) {
        wr.applyConfiguration(configurationData);

        wr.getPageBuilderService().assembler().resources().exclude(setOf(), excluded);

        for (String context : included) {
            wr.requireContext(context);
        }
        return wr.paths();
    }

    class IsResourceWithUrl implements Predicate<String> {
        private final String url;

        public IsResourceWithUrl(String url) {
            this.url = url;
        }

        public boolean apply(String actual) {
            return actual.contains(url);
        }
    }
}
