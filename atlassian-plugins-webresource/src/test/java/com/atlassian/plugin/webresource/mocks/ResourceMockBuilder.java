package com.atlassian.plugin.webresource.mocks;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import static org.mockito.Mockito.when;

public class ResourceMockBuilder {
    private final MockBuilder parent;
    private final Resource resource;

    public ResourceMockBuilder(@Nonnull final MockBuilder parent, @Nonnull final Resource resource) {
        this.parent = requireNonNull(parent, "The parent is mandatory for the resource mock build.");
        this.resource = requireNonNull(resource, "The resource is mandatory for the resource mock build.");
    }

    @Nonnull
    public MockBuilder and() {
        return this.parent;
    }

    public ResourceMockBuilder location(@Nullable final String location) {
        when(resource.getLocation()).thenReturn(location);
        return this;
    }

    @Nonnull
    public ResourceMockBuilder parent(@Nullable final Bundle bundle) {
        when(resource.getParent()).thenReturn(bundle);
        return this;
    }

    @Nonnull
    public ResourceMockBuilder path(@Nullable final String path) {
        when(resource.getPath()).thenReturn(path);
        return this;
    }

    @Nonnull
    public ResourceMockBuilder streamFor(@NonNull final String path, @Nullable final String content) {
        if (nonNull(content)) {
            final InputStream inputStream = new ByteArrayInputStream(content.getBytes());
            when(resource.getStreamFor(path)).thenReturn(inputStream);
        }
        return this;
    }
}
