package com.atlassian.plugin.webresource.integration.transformers;

import java.util.Set;

import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

public class AddLocationDimensionAware implements WebResourceTransformerFactory {
    @Override
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        return urlBuilder -> urlBuilder.addToQueryString("location", "true");
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters) {
        return (resource, params) -> new CharSequenceDownloadableResource(resource.nextResource()) {
            protected CharSequence transform(CharSequence originalContent) {
                if ("true".equals(params.get("location"))) {
                    return "location=" + parameters.getPluginKey() + ":" + parameters.getModuleKey() + "/"
                            + resource.location().getLocation() + " (transformer)\n" + originalContent;
                } else {
                    return originalContent;
                }
            }
        };
    }

    @Override
    public Set<String> allUsedQueryParameters() {
        return Set.of("location");
    }
}
