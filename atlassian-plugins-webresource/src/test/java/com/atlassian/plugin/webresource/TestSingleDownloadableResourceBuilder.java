package com.atlassian.plugin.webresource;

import java.util.List;

import org.junit.Test;

import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.transformers.AddLocation;
import com.atlassian.plugin.webresource.integration.transformers.AddLocationStatic;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;

public class TestSingleDownloadableResourceBuilder extends TestCase {
    @Test
    public void shouldServeSingleResource() {
        wr.configure()
                .disableContextBatching()
                .disableWebResourceBatching()
                .plugin("app")
                .webResource("page")
                .resource("image.jpg")
                .end();

        assertThat(wr.getContent(resourceUrl("app:page", "image.jpg")), equalTo("content of image.jpg"));
    }

    @Test
    public void shouldServeSingleResourceWithSlashesInName() {
        wr.configure()
                .disableContextBatching()
                .disableWebResourceBatching()
                .plugin("app")
                .webResource("page")
                .resource("path/to/image.jpg", "content of image.jpg")
                .end();

        assertThat(wr.getContent(resourceUrl("app:page", "path/to/image.jpg")), equalTo("content of image.jpg"));
    }

    @Test
    public void shouldGenerateUrlAndServeSingleResource() {
        wr.configure()
                .disableContextBatching()
                .disableWebResourceBatching()
                .plugin("app")
                .webResource("page")
                .resource("style.css")
                .end();

        wr.requireResource("app:page");

        List<String> urls = wr.paths();
        assertThat(urls, matches(resourceUrl("app:page", "style.css")));

        String url = urls.get(0);
        assertThat(wr.getContent(url), matches("content of style.css"));
        assertThat(wr.getHeaders(url).get("Content-Type"), equalTo("text/css"));
    }

    //    Disabled because condition should be evaluated for batches only but not for the single resources.
    //    @Test
    //    public void shouldEvaluateConditionForSingleResource()
    //    {
    //        wr.configure()
    //            .disableBatching()
    //            .plugin("app")
    //                .webResource("page")
    //                    .resource("style.css")
    //                .webResource("dashboard")
    //                    .condition(AlwaysFalseCondition.class)
    //                    .resource("style.css")
    //        .end();
    //
    //        assertThat(wr.getContent(resourceUrl("app:page", "style.css", buildMap("always-false", "true"))), equalTo(
    //            "content of style.css"
    //        ));
    //        assertThat(wr.getContent(resourceUrl("app:dashboard", "style.css", buildMap("always-false", "true"))),
    // equalTo(
    //            ""
    //        ));
    //    }

    @Test
    public void shouldApplyTransformationToSingleResource() {
        wr.configure()
                .plugin("app")
                .transformer("AddLocation", AddLocation.class)
                .webResource("page")
                .transformation("css", "AddLocation")
                .resource("style.css")
                .end();

        assertThat(
                wr.getContent(resourceUrl("app:page", "style.css", buildMap("location", "true"))),
                matches("location=app:page/style.css (transformer)\ncontent of style.css"));
    }

    @Test
    public void shouldApplyStaticTransformationToSingleResource() {
        wr.configure()
                .setStaticTransformers(new AddLocationStatic())
                .plugin("app")
                .webResource("page")
                //                    .transformer("css", "addCompleteKey")
                .resource("style.css")
                .end();

        assertThat(
                wr.getContent(resourceUrl("app:page", "style.css", buildMap("location", "true"))),
                matches("location=app:page/style.css (static)\ncontent of style.css"));
    }
}
