package com.atlassian.plugin.webresource.integration.transformers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.sourcemap.ReadableSourceMap;
import com.atlassian.webresource.api.QueryParams;

public class OffsetTransformerWithSourceMap extends ContentTransformerHelper {
    @Override
    public Set<String> allUsedQueryParameters() {
        return Set.of();
    }

    public Content transform(
            final Content content, ResourceLocation resourceLocation, QueryParams params, final String sourceUrl) {
        return new ContentImpl(content.getContentType(), true) {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                ByteArrayOutputStream buff = new ByteArrayOutputStream();
                ReadableSourceMap sourceMap = content.writeTo(buff, isSourceMapEnabled);
                final String contentAsString = buff.toString();

                if (isSourceMapEnabled && (sourceMap != null)) {
                    sourceMap.addOffset(1);
                }

                try {
                    out.write(("offset-transformer\n" + contentAsString + "\noffset-transformer").getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                return sourceMap;
            }
        };
    }
}
