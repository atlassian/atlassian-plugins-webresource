package com.atlassian.plugin.webresource.impl;

import org.junit.Test;

import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TestSuperbatchConfiguration {
    @Test
    public void when_PhaseWasNotSelected_Then_GetResourcePhaseShouldReturnDefaultPhase() {
        // given
        SuperbatchConfiguration superbatchConfiguration = new SuperbatchConfiguration(true);

        // when
        ResourcePhase result = superbatchConfiguration.getResourcePhase();

        // then
        assertThat(result, equalTo(ResourcePhase.defaultPhase()));
    }

    @Test
    public void when_PhaseWasPassed_Then_GetResourcePhaseShouldReturnPassedPhase() {
        // given
        SuperbatchConfiguration superbatchConfiguration = new SuperbatchConfiguration(true);
        superbatchConfiguration.setResourcePhase(ResourcePhase.DEFER);

        // when
        ResourcePhase result = superbatchConfiguration.getResourcePhase();

        // then
        assertThat(result, equalTo(ResourcePhase.DEFER));
    }
}
