package com.atlassian.plugin.webresource.assembler;

import java.io.StringWriter;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.startsWith;

import static com.atlassian.plugin.webresource.TestUtils.removeWebResourceLogs;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INLINE;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE;

public class TestWebResourceWithInlinePhase {
    private AssemblerTestFixture f;
    private StringWriter writer;

    @Before
    public void setUp() {
        f = new AssemblerTestFixture();
        f.mockPlugin("test.atlassian:first-feature")
                .res("first-1.js")
                .res("first-2.js")
                .res("first-1.css")
                .res("first-2.css")
                .ctx("first", "all");
        f.mockPlugin("test.atlassian:second-feature")
                .res("second-1.css")
                .res("second-1.js")
                .res("second-2.css")
                .res("second-2.js")
                .ctx("second", "all");
        f.mockPlugin("test.atlassian:third-feature")
                .res("third.css")
                .res("third.js")
                .deps("test.atlassian:first-feature", "test.atlassian:second-feature")
                .ctx("third", "all");

        f.mockContent("first-1.css", ".first-1{color:red}");
        f.mockContent("first-2.css", ".first-2{color:#f00}");
        f.mockContent("second-1.css", ".second-1{color:green}");
        f.mockContent("second-2.css", ".second-2{color:#0f0}");
        f.mockContent("third.css", ".third{color:blue}");

        f.mockContent("first-1.js", "console.log('first-1');");
        f.mockContent("first-2.js", "console.log('first-2');");
        f.mockContent("second-1.js", "console.log('second-1');");
        f.mockContent("second-2.js", "console.log('second-2');");
        f.mockContent("third.js", "console.log('third');");

        writer = new StringWriter();
    }

    @Test
    public void testLoadAllFeaturesInlineIndividually() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource(INLINE, "test.atlassian:first-feature");
        assembler.resources().requireWebResource(INLINE, "test.atlassian:second-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expectedStyles = "<style>" + ".first-1{color:red}\n"
                + ".first-2{color:#f00}\n"
                + ".second-1{color:green}\n"
                + ".second-2{color:#0f0}\n"
                + "</style>";
        final String expectedScripts =
                "<script data-wrm-key=\"test.atlassian:first-feature,test.atlassian:second-feature\" data-wrm-batch-type=\"resource\" data-initially-rendered>\n"
                        + "console.log('first-1');\n"
                        + "console.log('first-2');\n"
                        + "console.log('second-1');\n"
                        + "console.log('second-2');\n"
                        + "</script>";
        // todo PLUGWEB-545 add CSS support for inline phase
        final String expected = expectedScripts;
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void testLoadAllFeaturesInlineViaContext() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INLINE, "all");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expectedStyles = "<style>" + ".first-1{color:red}\n"
                + ".first-2{color:#f00}\n"
                + ".second-1{color:green}\n"
                + ".second-2{color:#0f0}\n"
                + ".third{color:blue}\n"
                + "</style>";
        final String expectedScripts =
                "<script data-wrm-key=\"all\" data-wrm-batch-type=\"context\" data-initially-rendered>\n"
                        + "console.log('first-1');\n"
                        + "console.log('first-2');\n"
                        + "console.log('second-1');\n"
                        + "console.log('second-2');\n"
                        + "console.log('third');\n"
                        + "</script>";
        // todo PLUGWEB-545 add CSS support for inline phase
        final String expected = expectedScripts;
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void testLoadAllFeaturesInlineViaMixed() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INLINE, "second");
        assembler.resources().requireWebResource(INLINE, "test.atlassian:first-feature");
        assembler.resources().requireWebResource(INLINE, "test.atlassian:third-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expectedStyles = "<style>" + ".second-1{color:green}\n"
                + ".second-2{color:#0f0}\n"
                + ".first-1{color:red}\n"
                + ".first-2{color:#f00}\n"
                + ".third{color:blue}\n"
                + "</style>";
        // note that we "upgrade" web-resource keys to act like contexts when the resource types are mixed.
        final String expectedScripts =
                "<script data-wrm-key=\"test.atlassian:first-feature,test.atlassian:third-feature,second\" data-wrm-batch-type=\"context\" data-initially-rendered>\n"
                        + "console.log('second-1');\n"
                        + "console.log('second-2');\n"
                        + "console.log('first-1');\n"
                        + "console.log('first-2');\n"
                        + "console.log('third');\n"
                        + "</script>";
        // todo PLUGWEB-545 add CSS support for inline phase
        final String expected = expectedScripts;
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void testRequireNotOutputIfRequestedAsInline() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(REQUIRE, "first");
        assembler.resources().requireWebResource(INLINE, "test.atlassian:first-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        String expectedStyles = "<style>" + ".first-1{color:red}\n" + ".first-2{color:#f00}\n" + "</style>";
        final String expectedScripts =
                "<script data-wrm-key=\"test.atlassian:first-feature\" data-wrm-batch-type=\"resource\" data-initially-rendered>\n"
                        + "console.log('first-1');\n"
                        + "console.log('first-2');\n"
                        + "</script>";
        // todo PLUGWEB-545 add CSS support for inline phase
        final String expected = expectedScripts;
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void testInteractiveNotOutputIfRequestedAsInline() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "first");
        assembler.resources().requireWebResource(INLINE, "test.atlassian:first-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expectedStyles = "<style>" + ".first-1{color:red}\n" + ".first-2{color:#f00}\n" + "</style>";
        final String expectedScripts =
                "<script data-wrm-key=\"test.atlassian:first-feature\" data-wrm-batch-type=\"resource\" data-initially-rendered>\n"
                        + "console.log('first-1');\n"
                        + "console.log('first-2');\n"
                        + "</script>";
        // todo PLUGWEB-545 add CSS support for inline phase
        final String expected = expectedScripts;
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void testRequirePartiallyOutputIfSubsetRequestedAsInline() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(REQUIRE, "all");
        assembler.resources().requireWebResource(INLINE, "test.atlassian:first-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expectedStyles = "<style>" + ".first-1{color:red}\n" + ".first-2{color:#f00}\n" + "</style>";
        final String expectedScripts =
                "<script data-wrm-key=\"test.atlassian:first-feature\" data-wrm-batch-type=\"resource\" data-initially-rendered>\n"
                        + "console.log('first-1');\n"
                        + "console.log('first-2');\n"
                        + "</script>";
        // todo PLUGWEB-545 add CSS support for inline phase
        final String expectedInline = expectedScripts;
        assertThat(actual, startsWith(expectedInline));

        final String expectedBatchTags = "<link rel=\"stylesheet\""
                + " href=\"/download/contextbatch/css/all,-test.atlassian:first-feature/batch.css\""
                + " data-wrm-key=\"all,-test.atlassian:first-feature\""
                + " data-wrm-batch-type=\"context\""
                + " media=\"all\">\n"
                + "<script src=\"/download/contextbatch/js/all,-test.atlassian:first-feature/batch.js\""
                + " data-wrm-key=\"all,-test.atlassian:first-feature\""
                + " data-wrm-batch-type=\"context\""
                + " data-initially-rendered></script>";
        assertThat(actual, containsString(expectedBatchTags));
    }

    @Test
    public void testInteractivePartiallyOutputIfWebResourceSubsetRequestedAsInline() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "all");
        assembler.resources().requireWebResource(INLINE, "test.atlassian:first-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expectedStyles = "<style>" + ".first-1{color:red}\n" + ".first-2{color:#f00}\n" + "</style>";
        final String expectedScripts =
                "<script data-wrm-key=\"test.atlassian:first-feature\" data-wrm-batch-type=\"resource\" data-initially-rendered>\n"
                        + "console.log('first-1');\n"
                        + "console.log('first-2');\n"
                        + "</script>";
        // todo PLUGWEB-545 add CSS support for inline phase
        final String expectedInline = expectedScripts;
        // note: this could either be a request for the `all` context OR the `test.atlassian:second-feature` and
        // `test.atlassian:third-feature` web-resource keys.
        final String expectedInteraction = "<script type=\"module\">WRM.requireLazily([\"wrc!all\"])</script>";
        assertThat(actual, equalTo(expectedInline + "\n" + expectedInteraction));
    }

    @Test
    public void testInteractivePartiallyOutputIfContextSubsetRequestedAsInline() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "all");
        assembler.resources().requireContext(INLINE, "first");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expectedStyles = "<style>" + ".first-1{color:red}\n" + ".first-2{color:#f00}\n" + "</style>";
        final String expectedScripts =
                "<script data-wrm-key=\"first\" data-wrm-batch-type=\"context\" data-initially-rendered>\n"
                        + "console.log('first-1');\n"
                        + "console.log('first-2');\n"
                        + "</script>";
        // todo PLUGWEB-545 add CSS support for inline phase
        final String expectedInline = expectedScripts;
        // note: this could either be a request for the `all` context OR the `test.atlassian:second-feature` and
        // `test.atlassian:third-feature` web-resource keys.
        final String expectedInteraction = "<script type=\"module\">WRM.requireLazily([\"wrc!all\"])</script>";
        assertThat(actual, equalTo(expectedInline + "\n" + expectedInteraction));
    }

    // @todo: test permutations of resources within features - e.g., no CSS, no JS - to ensure lazy require happens even
    // if one media type isn't there
}
