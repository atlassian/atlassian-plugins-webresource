package com.atlassian.plugin.webresource.integration.transformers;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.sourcemap.ReadableSourceMap;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;

public class DoNothingStaticTransformer implements StaticTransformers {

    @Override
    public void loadTwoPhaseProperties(
            ResourceLocation resourceLocation, java.util.function.Function<String, InputStream> loadFromFile) {}

    @Override
    public boolean hasTwoPhaseProperties() {
        return false;
    }

    @Override
    public void addToUrl(
            String locationType,
            TransformerParameters transformerParameters,
            UrlBuilder urlBuilder,
            UrlBuildingStrategy urlBuildingStrategy) {}

    @Override
    public Content transform(
            Content content,
            TransformerParameters transformerParameters,
            ResourceLocation resourceLocation,
            QueryParams queryParams,
            String sourceUrl) {
        return new ContentImpl(content.getContentType(), true) {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                content.writeTo(out, isSourceMapEnabled);
                // Intentionally returning null.
                return null;
            }
        };
    }

    @Override
    public Set<String> getParamKeys() {
        return new HashSet<>();
    }
}
