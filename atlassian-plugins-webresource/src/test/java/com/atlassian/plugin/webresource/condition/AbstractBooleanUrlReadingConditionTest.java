package com.atlassian.plugin.webresource.condition;

import java.util.Map;

import org.junit.Test;

import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.webresource.spi.condition.AbstractBooleanUrlReadingCondition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AbstractBooleanUrlReadingConditionTest {
    @Test
    public void testTrue() {
        AbstractBooleanUrlReadingCondition trueCondition = new TrueSimpleUrlReadingCondition();

        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        trueCondition.addToUrl(urlBuilder);

        Map<String, String> params = urlBuilder.buildParams();
        assertEquals(String.valueOf("true"), params.get("ermagherd-yerrs"));
        assertTrue(trueCondition.shouldDisplay(params::get));
    }

    @Test
    public void testFalse() {
        AbstractBooleanUrlReadingCondition falseCondition = new FalseSimpleUrlReadingCondition();

        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        falseCondition.addToUrl(urlBuilder);

        Map<String, String> params = urlBuilder.buildParams();
        assertFalse(params.containsKey("ermagherd-nerrr"));
        assertFalse(falseCondition.shouldDisplay(params::get));
    }

    private static final class TrueSimpleUrlReadingCondition extends AbstractBooleanUrlReadingCondition {
        @Override
        protected boolean isConditionTrue() {
            return true;
        }

        @Override
        protected String queryKey() {
            return "ermagherd-yerrs";
        }
    }

    private static final class FalseSimpleUrlReadingCondition extends AbstractBooleanUrlReadingCondition {
        @Override
        protected boolean isConditionTrue() {
            return false;
        }

        @Override
        protected String queryKey() {
            return "ermagherd-nerrr";
        }
    }
}
