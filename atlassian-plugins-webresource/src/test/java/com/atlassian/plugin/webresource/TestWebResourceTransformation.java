package com.atlassian.plugin.webresource;

import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.junit.Test;
import com.google.common.collect.ImmutableMap;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.transformers.AddLocation;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformableResource;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.mockTransformerAlias;
import static com.atlassian.plugin.webresource.TestUtils.mockTransformers;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;

public class TestWebResourceTransformation extends TestCase {
    @Test
    public void testMatches() throws DocumentException {
        WebResourceTransformation trans = new WebResourceTransformation(parseText());
        ResourceLocation loc = mock(ResourceLocation.class);
        when(loc.getName()).thenReturn("foo.js");
        assertTrue(trans.matches(loc));
    }

    private static Element parseText() throws DocumentException {
        String text = "<transformation extension=\"js\">\n" + "<transformer key=\"foo\" />\n" + "</transformation>";
        return parseText(text);
    }

    private static Dom4jDelegatingElement parseText(String text) throws DocumentException {
        return new Dom4jDelegatingElement(DocumentHelper.parseText(text).getRootElement());
    }

    @Test
    public void testNotMatches() throws DocumentException {
        WebResourceTransformation trans = new WebResourceTransformation(parseText());
        ResourceLocation loc = mock(ResourceLocation.class);
        when(loc.getName()).thenReturn("foo.cs");
        assertFalse(trans.matches(loc));
    }

    @Test
    public void testMatchesString() throws DocumentException {
        WebResourceTransformation trans = new WebResourceTransformation(parseText());
        assertTrue(trans.matches("js"));
    }

    @Test
    public void testNotMatchesString() throws DocumentException {
        WebResourceTransformation trans = new WebResourceTransformation(parseText());
        assertFalse(trans.matches("css"));
    }

    @Test
    public void testNoExtension() throws DocumentException {
        try {
            new WebResourceTransformation(
                    parseText("<transformation>\n" + "<transformer key=\"foo\" />\n" + "</transformation>"));
            fail("Should have forced extension");
        } catch (IllegalArgumentException ex) {
            // pass
        }
    }

    // moved to TestUseCases.shouldApplyLegacyTransformers
    // public void testTransformDownloadableResource()

    // moved to TestUseCases.shouldApplyTransformers
    // public void testTransformDownloadableResourceWithUrlReadingPresent()

    // moved to testUrlMappedDownloadableResource
    //    @Test public void testUrlMappedDownloadableResource()

    @Test
    public void testUrlMappedUrlByAlias() throws DocumentException, DownloadException {
        Element element =
                parseText("<transformation extension=\"js\"><transformer key=\"foo-alias\"/></transformation>");
        WebResourceTransformation trans = new WebResourceTransformation(element);
        PluginAccessor pluginAccessor = mock(PluginAccessor.class);
        TransformerCache transformerCache = new TransformerCache(mock(PluginEventManager.class), pluginAccessor);

        mockTransformers(pluginAccessor, ImmutableMap.of("foo", new UrlAwarePrefixTransformer("foo-key", "foo-value")));
        mockTransformerAlias(pluginAccessor, "foo", "foo-alias");

        UrlBuilder urlBuilder = mock(UrlBuilder.class);
        trans.addTransformParameters(
                transformerCache, mock(TransformerParameters.class), urlBuilder, UrlBuildingStrategy.normal());

        verify(urlBuilder).addToQueryString("foo-key", "foo-value");
    }

    // old name testUrlMappedDownloadableResourceByAlias
    @Test
    public void shouldResolveTransformerByAlias() {
        wr.configure()
                .plugin("plugin")
                .transformer("notUsed", "AddLocationAlias", AddLocation.class)
                .webResource("a")
                .transformation("js", "AddLocationAlias")
                .resource("a1.js")
                .end();

        String content = wr.getContent(resourceUrl("plugin:a", "a1.js", buildMap("location", "true")));
        assertThat(content, equalTo("location=plugin:a/a1.js (transformer)\n" + "content of a1.js"));
    }

    @Test
    public void shouldApplyTransformersForResourcesWithDifferentLocationExtension() {
        wr.configure()
                .plugin("plugin")
                .transformer("AddLocation", AddLocation.class)
                .webResource("a")
                .transformation("soy", "AddLocation")
                .resource("a.js", "content of a", "a.soy")
                .end();

        wr.requireResource("plugin:a");

        String url = webResourceBatchUrl("plugin:a", "js", buildMap("location", "true"));
        assertThat(wr.paths(), matches(url));

        assertThat(wr.getContent(url), matches("location=plugin:a/a.soy", "content of a"));
    }

    private static class UrlAwarePrefixTransformer implements WebResourceTransformerFactory {
        private String key;
        private String value;

        public UrlAwarePrefixTransformer(String key, String value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public TransformerUrlBuilder makeUrlBuilder(final TransformerParameters transformerParams) {
            return new TransformerUrlBuilder() {
                @Override
                public void addToUrl(UrlBuilder urlBuilder) {
                    urlBuilder.addToQueryString(key, value);
                }
            };
        }

        @Override
        public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters transformerParams) {
            return new UrlReadingWebResourceTransformer() {
                @Override
                public DownloadableResource transform(TransformableResource transformableResource, QueryParams params) {
                    final String value = params.get(key);
                    return new CharSequenceDownloadableResource(transformableResource.nextResource()) {
                        @Override
                        protected CharSequence transform(CharSequence originalContent) {
                            return value + ": " + originalContent;
                        }
                    };
                }
            };
        }
    }
}
