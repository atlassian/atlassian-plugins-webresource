package com.atlassian.plugin.webresource.mocks;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.http.Router;

import static java.util.Objects.requireNonNull;
import static org.mockito.Mockito.when;

public class GlobalsMockBuilder {
    private final MockBuilder parent;
    private final Globals globals;

    public GlobalsMockBuilder(@Nonnull final MockBuilder parent, @Nonnull final Globals globals) {
        this.parent = requireNonNull(parent, "The parent is mandatory for the globals mock build.");
        this.globals = requireNonNull(globals, "The globals is mandatory for the globals mock build.");
    }

    @Nonnull
    public MockBuilder and() {
        return this.parent;
    }

    @Nonnull
    public GlobalsMockBuilder config(@Nullable final Config config) {
        when(globals.getConfig()).thenReturn(config);
        return this;
    }

    @Nonnull
    public GlobalsMockBuilder router(@Nullable final Router router) {
        when(globals.getRouter()).thenReturn(router);
        return this;
    }
}
