package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformableResource;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

public abstract class TransformerWithoutSourceMapHelper implements WebResourceTransformerFactory {
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        return urlBuilder -> {};
    }

    public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters parameters) {
        return this::transform;
    }

    public abstract DownloadableResource transform(TransformableResource transformableResource, QueryParams params);
}
