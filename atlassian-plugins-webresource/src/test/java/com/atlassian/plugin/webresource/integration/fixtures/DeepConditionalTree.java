package com.atlassian.plugin.webresource.integration.fixtures;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

/**
 * <p>
 * Prepares three web-resources `feature-a`, `feature-b`, and `feature-c`:
 * </p>
 * <ul>
 *     <li>Each is added to the superbatch</li>
 *     <li>Each is added to a `context.features` context</li>
 *     <li>Each has a dependency on a web-resource named `sub-FEATURELETTER`</li>
 *     <li>Each `sub-FEATURELETTER` has a dependency on a web-resoure named `deep-FEATURELETTER`</li>
 *     <li>Each `deep-FEATURELETTER` has a single javascript file in it.</li>
 * </ul>
 * <p>
 * The particular properties for each featureset is as follows:
 * </p>
 * <ul>
 *     <li>`feature-b` has no condition. It should always be included.</li>
 *     <li>`feature-a` has a condition.</li>
 *     <li>`feature-c` has a condition.</li>
 *     <li>`deep-c` depends upon `feature-a`.</li>
 * </ul>
 */
public class DeepConditionalTree {
    private DeepConditionalTree() {}

    private static boolean conditionFeatureA;
    private static boolean conditionFeatureC;

    /**
     * A hash derived from the following expected dependency order:
     * shared-lib-x, deep-a, sub-a, feature-a, deep-b, sub-b, feature-b, deep-c, sub-c, feature-c, _super.
     */
    public static final String BUNDLEHASH_FOR_SUPERBATCH = "37f7b150445369b369176cf4411a1bc7";

    public static void configure(final WebResource wr) throws Exception {
        conditionFeatureA = true;
        conditionFeatureC = true;
        wr.configure()
                .addToSuperbatch("plugin.key:feature-a", "plugin.key:feature-b", "plugin.key:feature-c")
                .plugin("plugin.key")
                .webResource("shared-lib-x")
                .resource("shared-lib-x.js")
                .webResource("feature-a")
                .context("context.features")
                .context("context.aaa")
                .dependency(":sub-a")
                .condition(ConditionFeatureA.class)
                .webResource("sub-a")
                .dependency(":deep-a")
                .webResource("deep-a")
                .dependency(":shared-lib-x")
                .resource("deep-a.js")

                // todo test moduledescriptor enabled state?
                .webResource("feature-b")
                .context("context.features")
                .context("context.bbb")
                .dependency(":sub-b")
                .webResource("sub-b")
                .dependency(":deep-b")
                .webResource("deep-b")
                .resource("deep-b.js")
                .webResource("feature-c")
                .context("context.features")
                .context("context.ccc")
                .dependency(":sub-c")
                .condition(ConditionFeatureC.class)
                .webResource("sub-c")
                .dependency(":deep-c")
                .webResource("deep-c")
                .dependency(":shared-lib-x")
                .dependency(":feature-a")
                .resource("deep-c.js")
                .end();
    }

    public static void setConditionFeatureA(boolean val) {
        conditionFeatureA = val;
    }

    public static void setConditionFeatureC(boolean val) {
        conditionFeatureC = val;
    }

    public static class ConditionFeatureA implements UrlReadingCondition {
        @Override
        public void init(Map<String, String> params) throws PluginParseException {}

        @Override
        public void addToUrl(UrlBuilder urlBuilder) {
            urlBuilder.addToQueryString("feature-a", String.valueOf(conditionFeatureA));
        }

        @Override
        public boolean shouldDisplay(QueryParams params) {
            return conditionFeatureA;
        }
    }

    public static class ConditionFeatureC implements UrlReadingCondition {
        @Override
        public void init(Map<String, String> params) throws PluginParseException {}

        @Override
        public void addToUrl(UrlBuilder urlBuilder) {
            urlBuilder.addToQueryString("feature-c", String.valueOf(conditionFeatureC));
        }

        @Override
        public boolean shouldDisplay(QueryParams params) {
            return conditionFeatureC;
        }
    }
}
