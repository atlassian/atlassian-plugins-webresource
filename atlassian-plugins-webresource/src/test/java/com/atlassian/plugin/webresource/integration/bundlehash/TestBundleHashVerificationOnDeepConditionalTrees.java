package com.atlassian.plugin.webresource.integration.bundlehash;

import java.util.List;

import org.junit.Test;

import com.atlassian.plugin.webresource.integration.fixtures.DeepConditionalTree;
import com.atlassian.plugin.webresource.integration.stub.ResponseData;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;

import static com.atlassian.plugin.webresource.integration.fixtures.DeepConditionalTree.BUNDLEHASH_FOR_SUPERBATCH;

/**
 * This test verifies that bundlehash values are consistent independent of condition evaluation and dependency
 * tree depth.
 */
public class TestBundleHashVerificationOnDeepConditionalTrees extends BundleHashTestCase {

    @Override
    protected void setUp() throws Exception {
        DeepConditionalTree.configure(wr);
    }

    @Test
    public void testHashWhenAllFeaturesEnabled() {
        DeepConditionalTree.setConditionFeatureA(true);
        DeepConditionalTree.setConditionFeatureC(true);

        final List<String> paths = wr.paths();
        final ResponseData resp = getWithStaticHash(paths.get(0), BUNDLEHASH_FOR_SUPERBATCH);

        assertThat(paths, everyItem(containsString(BUNDLEHASH_FOR_SUPERBATCH)));
        assertThat(resp.getStatus(), equalTo(200));
        assertThat(
                resp.getContent(),
                allOf(
                        containsString("content of deep-a.js"),
                        containsString("content of deep-b.js"),
                        containsString("content of deep-c.js")));
    }

    @Test
    public void testHashWhenFeatureADisabled() {
        DeepConditionalTree.setConditionFeatureA(false);
        DeepConditionalTree.setConditionFeatureC(true);

        final List<String> paths = wr.paths();
        final ResponseData resp = getWithStaticHash(paths.get(0), BUNDLEHASH_FOR_SUPERBATCH);

        assertThat(paths, everyItem(containsString(BUNDLEHASH_FOR_SUPERBATCH)));
        assertThat(resp.getStatus(), equalTo(200));
        assertThat(
                resp.getContent(),
                allOf(
                        not(containsString("content of deep-a.js")),
                        containsString("content of deep-b.js"),
                        containsString("content of deep-c.js")));
    }

    @Test
    public void testHashWhenFeatureCDisabled() {
        DeepConditionalTree.setConditionFeatureA(true);
        DeepConditionalTree.setConditionFeatureC(false);

        final List<String> paths = wr.paths();
        final ResponseData resp = getWithStaticHash(paths.get(0), BUNDLEHASH_FOR_SUPERBATCH);

        assertThat(paths, everyItem(containsString(BUNDLEHASH_FOR_SUPERBATCH)));
        assertThat(resp.getStatus(), equalTo(200));
        assertThat(
                resp.getContent(),
                allOf(
                        containsString("content of deep-a.js"),
                        containsString("content of deep-b.js"),
                        not(containsString("content of deep-c.js"))));
    }

    @Test
    public void testHashWhenBothFeatureAAndFeatureCDisabled() {
        DeepConditionalTree.setConditionFeatureA(false);
        DeepConditionalTree.setConditionFeatureC(false);

        final List<String> paths = wr.paths();
        final ResponseData resp = getWithStaticHash(paths.get(0), BUNDLEHASH_FOR_SUPERBATCH);

        assertThat(paths, everyItem(containsString(BUNDLEHASH_FOR_SUPERBATCH)));
        assertThat(resp.getStatus(), equalTo(200));
        assertThat(
                resp.getContent(),
                allOf(
                        not(containsString("content of deep-a.js")),
                        containsString("content of deep-b.js"),
                        not(containsString("content of deep-c.js"))));
    }
}
