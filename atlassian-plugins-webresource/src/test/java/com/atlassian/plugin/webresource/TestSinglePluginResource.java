package com.atlassian.plugin.webresource;

import org.junit.Test;

import com.atlassian.plugin.webresource.integration.TestCase;

import static org.junit.Assert.assertThat;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;

public class TestSinglePluginResource extends TestCase {
    @Test
    public void shouldGetUrl() throws Exception {
        wr.configure()
                .disableContextBatching()
                .disableWebResourceBatching()
                .plugin("plugin")
                .webResource("a")
                .resource("a1.css")
                .end();

        wr.requireResource("plugin:a");

        assertThat(wr.paths(), matches(resourceUrl("plugin:a", "a1.css")));
    }
}
