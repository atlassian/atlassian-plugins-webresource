package com.atlassian.plugin.webresource.assembler;

import java.io.StringWriter;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;

import static org.junit.Assert.assertEquals;

import static com.atlassian.plugin.webresource.TestUtils.removeWebResourceLogs;
import static com.atlassian.plugin.webresource.TestUtils.stringDataProvider;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.assertResolvedResources;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.m;
import static com.atlassian.plugin.webresource.data.DataTestFixture.assertDataTag;

public class TestWebResourceSet {
    private AssemblerTestFixture f;

    @Before
    public void setUp() {
        f = new AssemblerTestFixture();
    }

    @Test
    public void testFilterJs() {
        f.mockPlugin("test.atlassian:resource-js").res("js-1.js");
        f.mockPlugin("test.atlassian:resource-css").res("css-1.css");
        f.mockPlugin("test.atlassian:resource-both").res("js-1.js", "css-1.css");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-js");
        assembler.resources().requireWebResource("test.atlassian:resource-css");
        assembler.resources().requireWebResource("test.atlassian:resource-both");

        WebResourceSet resourceSet = assembler.assembled().drainIncludedResources();
        Iterable<PluginJsResource> resources = resourceSet.getResources(PluginJsResource.class);
        assertResolvedResources(resources, m("test.atlassian:resource-js"), m("test.atlassian:resource-both"));
    }

    @Test
    public void testFilterCss() {
        f.mockPlugin("test.atlassian:resource-js").res("js-1.js");
        f.mockPlugin("test.atlassian:resource-css").res("css-1.css");
        f.mockPlugin("test.atlassian:resource-both").res("js-1.js", "css-1.css");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-js");
        assembler.resources().requireWebResource("test.atlassian:resource-css");
        assembler.resources().requireWebResource("test.atlassian:resource-both");

        WebResourceSet resourceSet = assembler.assembled().drainIncludedResources();
        Iterable<PluginCssResource> resources = resourceSet.getResources(PluginCssResource.class);
        assertResolvedResources(
                resources, m("test.atlassian:resource-css", "css"), m("test.atlassian:resource-both", "css"));
    }

    @Test
    public void testWriteJs() {
        f.mockPlugin("test.atlassian:resource-js").res("js-1.js");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-js");
        StringWriter writer = new StringWriter();
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        assertEquals(
                "<script src=\"/download/batch/test.atlassian:resource-js/test.atlassian:resource-js.js\" data-wrm-key=\"test.atlassian:resource-js\" data-wrm-batch-type=\"resource\" "
                        + Config.INITIAL_RENDERED_SCRIPT_PARAM_NAME + "></script>",
                removeWebResourceLogs(writer.toString()));
    }

    @Test
    public void testWriteCss() {
        f.mockPlugin("test.atlassian:resource-css").res("css-1.css");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-css");
        StringWriter writer = new StringWriter();
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        assertEquals(
                "<link rel=\"stylesheet\" href=\"/download/batch/test.atlassian:resource-css/test.atlassian:resource-css.css\" data-wrm-key=\"test.atlassian:resource-css\" data-wrm-batch-type=\"resource\" media=\"all\">",
                removeWebResourceLogs(writer.toString()));
    }

    @Test
    public void testWriteData() {
        f.mockPlugin("test.atlassian:a").data("abc", stringDataProvider("123"));
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertDataTag(resources, "WRM._unparsedData[\"test.atlassian:a.abc\"]=\"\\u0022123\\u0022\";\n");
    }

    @Test
    public void testWritePrefetchLinks() {
        f.mockPlugin("test.atlassian:resource-js").res("js-1.js");
        f.mockPlugin("test.atlassian:resource-css").res("css-1.css");

        f.mockPlugin("test.atlassian:resource-prefetch-js").res("prefetched.js");
        f.mockPlugin("test.atlassian:resource-prefetch-css").res("prefetched.css");

        WebResourceAssembler pageAssembler = f.create();

        // Copy before adding resources
        WebResourceAssembler prefetchAssembler = pageAssembler.copy();

        // These are on the current page's assembler but shouldn't appear on the prefetch assembler we copied
        pageAssembler.resources().requireWebResource("test.atlassian:resource-js");
        pageAssembler.resources().requireWebResource("test.atlassian:resource-css");

        // These are on the prefetch assembler but shouldn't appear on the current page's assembler
        prefetchAssembler.resources().requireWebResource("test.atlassian:resource-prefetch-js");
        prefetchAssembler.resources().requireWebResource("test.atlassian:resource-prefetch-css");

        WebResourceSet pageSet = pageAssembler.assembled().drainIncludedResources();
        StringWriter pageWriter = new StringWriter();
        pageSet.writeHtmlTags(pageWriter, UrlMode.AUTO);

        WebResourceSet prefetchSet = prefetchAssembler.assembled().drainIncludedResources();
        StringWriter prefetchWriter = new StringWriter();
        prefetchSet.writePrefetchLinks(prefetchWriter, UrlMode.AUTO);

        assertEquals(
                "<link rel=\"stylesheet\" href=\"/download/batch/test.atlassian:resource-css/test.atlassian:resource-css.css\" data-wrm-key=\"test.atlassian:resource-css\" data-wrm-batch-type=\"resource\" media=\"all\">\n"
                        + "<script src=\"/download/batch/test.atlassian:resource-js/test.atlassian:resource-js.js\" data-wrm-key=\"test.atlassian:resource-js\" data-wrm-batch-type=\"resource\" "
                        + Config.INITIAL_RENDERED_SCRIPT_PARAM_NAME + "></script>\n",
                pageWriter.toString());

        assertEquals(
                "<link rel=\"prefetch\" href=\"/download/batch/test.atlassian:resource-prefetch-css/test.atlassian:resource-prefetch-css.css\" >\n"
                        + "<link rel=\"prefetch\" href=\"/download/batch/test.atlassian:resource-prefetch-js/test.atlassian:resource-prefetch-js.js\" >\n",
                prefetchWriter.toString());
    }
}
