package com.atlassian.plugin.webresource.condition;

import java.util.Map;

import org.junit.Test;
import com.google.common.collect.ImmutableMap;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.web.baseconditions.AbstractConditionElementParser;
import com.atlassian.plugin.webresource.Dom4jFluent;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class TestUrlReadingConditionElementParser {
    @Test
    public void testCreateUrlReadingClass() throws ClassNotFoundException {
        Element element = makeCondition(ImmutableMap.of("class", TestUtils.StubUrlReadingCondition.class.getName()));
        UrlReadingCondition instantiatedCondition = new TestUtils.StubUrlReadingCondition();
        DecoratingCondition condition = make(element, instantiatedCondition);

        assertTrue(((DecoratingUrlReadingCondition) condition).urlReadingCondition == instantiatedCondition);
    }

    @Test
    public void testCreateUrlReadingClass2() throws ClassNotFoundException {
        Element element = makeCondition(
                ImmutableMap.of("class", "blahblahblah", "class2", TestUtils.StubUrlReadingCondition.class.getName()));
        UrlReadingCondition instantiatedCondition = new TestUtils.StubUrlReadingCondition();
        DecoratingCondition condition = make(element, instantiatedCondition);

        assertTrue(((DecoratingUrlReadingCondition) condition).urlReadingCondition == instantiatedCondition);
    }

    private DecoratingCondition make(Element element, Object instantiatedCondition) throws ClassNotFoundException {
        Class clazz = instantiatedCondition.getClass();
        Plugin plugin = TestUtils.createTestPlugin("com.test", "1", clazz);
        HostContainer hostContainer = mock(HostContainer.class);
        doReturn(instantiatedCondition).when(hostContainer).create(clazz);
        return new UrlReadingConditionElementParser(hostContainer)
                .makeConditions(plugin, element, AbstractConditionElementParser.CompositeType.AND);
    }

    private Element makeCondition(Map<String, String> params) {
        return new Dom4jDelegatingElement(Dom4jFluent.element("web-resource", Dom4jFluent.element("condition", params))
                .toDom());
    }
}
