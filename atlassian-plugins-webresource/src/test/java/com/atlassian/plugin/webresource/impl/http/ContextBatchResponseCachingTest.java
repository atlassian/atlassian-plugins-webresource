package com.atlassian.plugin.webresource.impl.http;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableMap;

import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.webresource.TestUtils.cacheableRequestParams;
import static com.atlassian.plugin.webresource.impl.config.Config.ENABLE_BUNDLE_HASH_VALIDATION;

@RunWith(MockitoJUnitRunner.class)
public class ContextBatchResponseCachingTest extends AbstractResponseCachingTest {
    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    @Before
    public void setUp() {
        System.setProperty(ENABLE_BUNDLE_HASH_VALIDATION, Boolean.FALSE.toString());
    }

    @Test
    public void differentRoutesHaveSeparateCacheKeys() throws Exception {
        Router router = globals.getRouter();
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/js/AAA/batch.js", cacheableRequestParams()),
                mockResponse());
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/js/BBB/batch.js", cacheableRequestParams()),
                mockResponse());

        verify(cache, times(2)).cache(eq("http"), cacheKeyCaptor.capture(), any(), any());
        List<String> uniqueKeysUsed =
                cacheKeyCaptor.getAllValues().stream().distinct().collect(Collectors.toList());
        assertThat(String.format("Unique cache keys found: %s", uniqueKeysUsed), uniqueKeysUsed.size(), equalTo(2));
    }

    @Test
    public void differentResourceTypesHaveSeparateCacheKeys() throws Exception {
        Router router = globals.getRouter();
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/js/AAA/batch.js", cacheableRequestParams()),
                mockResponse());
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/css/AAA/batch.css", cacheableRequestParams()),
                mockResponse());

        verify(cache, times(2)).cache(eq("http"), cacheKeyCaptor.capture(), any(), any());
        List<String> uniqueKeysUsed =
                cacheKeyCaptor.getAllValues().stream().distinct().collect(Collectors.toList());
        assertThat(String.format("Unique cache keys found: %s", uniqueKeysUsed), uniqueKeysUsed.size(), equalTo(2));
    }

    @Test
    public void identicalRoutesHaveSameCacheKey() throws Exception {
        Router router = globals.getRouter();
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/js/AAA/batch.js", cacheableRequestParams()),
                mockResponse());
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/js/AAA/batch.js", cacheableRequestParams()),
                mockResponse());

        verify(cache, times(2)).cache(eq("http"), cacheKeyCaptor.capture(), any(), any());
        List<String> uniqueKeysUsed =
                cacheKeyCaptor.getAllValues().stream().distinct().collect(Collectors.toList());
        assertThat(String.format("Unique cache keys found: %s", uniqueKeysUsed), uniqueKeysUsed.size(), equalTo(1));
    }

    @Test
    @Ignore("PLUGWEB-640 - Confluence rely on order significance. Can be changed in WRM v6.")
    public void contextInclusionsAndExclusionsAreCommutative() throws Exception {
        Router router = globals.getRouter();
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/js/A,B/batch.js", cacheableRequestParams()),
                mockResponse());
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/js/B,A/batch.js", cacheableRequestParams()),
                mockResponse());

        verify(cache, times(2)).cache(eq("http"), cacheKeyCaptor.capture(), any(), any());
        List<String> uniqueKeysUsed =
                cacheKeyCaptor.getAllValues().stream().distinct().collect(Collectors.toList());
        assertThat(String.format("Unique cache keys found: %s", uniqueKeysUsed), uniqueKeysUsed.size(), equalTo(1));
    }

    @Test
    public void contextInclusionsAndExclusionsAreCollapsed() throws Exception {
        Router router = globals.getRouter();
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/js/B/batch.js", cacheableRequestParams()), mockResponse());
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/js/A,B,-A/batch.js", cacheableRequestParams()),
                mockResponse());

        verify(cache, times(2)).cache(eq("http"), cacheKeyCaptor.capture(), any(), any());
        List<String> uniqueKeysUsed =
                cacheKeyCaptor.getAllValues().stream().distinct().collect(Collectors.toList());
        assertThat(String.format("Unique cache keys found: %s", uniqueKeysUsed), uniqueKeysUsed.size(), equalTo(1));
    }

    @Test
    public void urlParametersAreCommutative() throws Exception {
        Router router = globals.getRouter();

        Map<String, String> params1 =
                cacheableRequestParams(ImmutableMap.of("one", "1", "two", "2", "three", "3", "four", "4", "five", "5"));
        router.dispatch(new Request(globals, "/s/1/1/1/contextbatch/js/AAA/batch.js", params1), mockResponse());

        Map<String, String> params2 =
                cacheableRequestParams(ImmutableMap.of("five", "5", "four", "4", "three", "3", "two", "2", "one", "1"));
        router.dispatch(new Request(globals, "/s/1/1/1/contextbatch/js/AAA/batch.js", params2), mockResponse());

        verify(cache, times(2)).cache(eq("http"), cacheKeyCaptor.capture(), any(), any());
        List<String> uniqueKeysUsed =
                cacheKeyCaptor.getAllValues().stream().distinct().collect(Collectors.toList());
        assertThat(String.format("Unique cache keys found: %s", uniqueKeysUsed), uniqueKeysUsed.size(), equalTo(1));
    }

    @Test
    public void sourceMapUsesDifferentCache() throws Exception {
        when(globals.getConfig().isSourceMapEnabled()).thenReturn(true);

        Router router = globals.getRouter();
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/js/A,B,C,D/batch.js", cacheableRequestParams()),
                mockResponse());
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/js/A,B,C,D/batch.js.map", cacheableRequestParams()),
                mockResponse());
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/css/A,B,C,D/batch.css", cacheableRequestParams()),
                mockResponse());
        router.dispatch(
                new Request(globals, "/s/1/1/1/contextbatch/css/A,B,C,D/batch.css.map", cacheableRequestParams()),
                mockResponse());

        verify(cache, times(4)).cache(eq("http"), cacheKeyCaptor.capture(), any(), any());
        List<String> uniqueKeysUsed =
                cacheKeyCaptor.getAllValues().stream().distinct().collect(Collectors.toList());
        assertThat(String.format("Unique cache keys found: %s", uniqueKeysUsed), uniqueKeysUsed.size(), equalTo(4));
    }

    private Response mockResponse() {
        Response r = mock(Response.class, Answers.RETURNS_DEEP_STUBS);
        when(r.getOutputStream()).thenReturn(new ByteArrayOutputStream());
        return r;
    }
}
