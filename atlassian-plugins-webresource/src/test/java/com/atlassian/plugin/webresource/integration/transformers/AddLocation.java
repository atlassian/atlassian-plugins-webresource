package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformableResource;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

public class AddLocation implements WebResourceTransformerFactory {
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        return new TransformerUrlBuilder() {
            public void addToUrl(UrlBuilder urlBuilder) {
                urlBuilder.addToQueryString("location", "true");
                urlBuilder.addToHash("location", "location");
            }
        };
    }

    public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters) {
        return new UrlReadingWebResourceTransformer() {
            public DownloadableResource transform(final TransformableResource resource, final QueryParams params) {
                return new CharSequenceDownloadableResource(resource.nextResource()) {
                    protected CharSequence transform(CharSequence originalContent) {
                        if ("true".equals(params.get("location"))) {
                            return "location=" + parameters.getPluginKey() + ":" + parameters.getModuleKey() + "/"
                                    + resource.location().getLocation() + " (transformer)\n" + originalContent;
                        } else {
                            return originalContent;
                        }
                    }
                };
            }
        };
    }
}
