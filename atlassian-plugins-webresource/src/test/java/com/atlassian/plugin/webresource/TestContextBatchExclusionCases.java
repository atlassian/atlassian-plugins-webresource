package com.atlassian.plugin.webresource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.StreamSupport;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import com.atlassian.plugin.webresource.integration.fixtures.DeepConditionalTree;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResource;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchSubsetUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;

public class TestContextBatchExclusionCases {
    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    private WebResourceAssembler assembler;

    @Before
    public void setUp() throws Exception {
        final WebResourceImpl wr = new WebResourceImpl();
        DeepConditionalTree.configure(wr);
        assembler = wr.getWebResourceAssemblerFactory()
                .create()
                .includeSuperbatchResources(false)
                .includeSyncbatchResources(false)
                .autoIncludeFrontendRuntime(false)
                .build();

        System.setProperty("atlassian.darkfeature.atlassian.webresource.performance.tracking.disable", "true");
    }

    @Test
    public void given_allConditionsPass_when_includingContextCCC_and_excludingFeatureA_then_subtractsFeatureA() {
        // when
        requireContexts("context.ccc");
        excludeWebResources("plugin.key:feature-a");
        final List<String> urls = getUrls();

        // then
        assertThat(urls, matches(contextBatchSubsetUrl("context.ccc", asList("plugin.key:feature-a"), "js")));
    }

    @Test
    public void
            given_allConditionsPass_when_includingContextCCC_and_excludingFeatureABeforeItsSubtrees_then_subtractsFeatureAOnly() {
        requireContexts("context.ccc");
        excludeWebResources("plugin.key:feature-a", "plugin.key:sub-a", "plugin.key:deep-a");
        final List<String> urls = getUrls();

        // then
        assertThat(urls, matches(contextBatchSubsetUrl("context.ccc", asList("plugin.key:feature-a"), "js")));
    }

    @Test
    public void
            given_allConditionsPass_when_includingContextCCC_and_excludingFeatureAAfterItsSubtrees_then_subtractsFeatureAOnly() {
        requireContexts("context.ccc");
        excludeWebResources("plugin.key:feature-a", "plugin.key:sub-a", "plugin.key:deep-a");
        final List<String> urls = getUrls();

        // then
        assertThat(urls, matches(contextBatchSubsetUrl("context.ccc", asList("plugin.key:feature-a"), "js")));
    }

    @Test
    public void
            given_allConditionsPass_when_includingContextCCC_and_excludingFeatureAAndSharedLibX_then_subtractsFeatureAOnly() {
        requireContexts("context.ccc");
        excludeWebResources("plugin.key:feature-a", "plugin.key:shared-lib-x");
        final List<String> urls = getUrls();

        // then
        assertThat(urls, matches(contextBatchSubsetUrl("context.ccc", asList("plugin.key:feature-a"), "js")));
    }

    @Test
    public void
            given_conditionAFails_when_includingContextCCC_and_excludingFeatureABeforeItsSubtrees_then_subtractsSubAAndFeatureA() {
        // given
        DeepConditionalTree.setConditionFeatureA(false);

        // when
        requireContexts("context.ccc");
        excludeWebResources("plugin.key:feature-a", "plugin.key:sub-a", "plugin.key:deep-a");
        final List<String> urls = getUrls();

        // then
        assertThat(urls, matches(contextBatchSubsetUrl("context.ccc", asList("plugin.key:feature-a"), "js")));
    }

    @Test
    public void
            given_allConditionsPass_when_includingFeaturesContext_and_excludingFeatures_then_shouldSubtractAlphabetically() {
        // when
        requireContexts("context.features");
        excludeWebResources("plugin.key:feature-b", "plugin.key:feature-a");
        final List<String> urls = getUrls();

        // then
        assertThat(
                urls,
                matches(contextBatchSubsetUrl(
                        "context.features", asList("plugin.key:feature-a", "plugin.key:feature-b"), "js")));
    }

    @Test
    public void
            given_allConditionsPass_when_includingFeaturesContext_and_excludingFeaturesCAndAAndSubtrees_then_shouldReduceExclusionToFeatureC() {
        // when
        requireContexts("context.features");
        excludeWebResources("plugin.key:feature-c", "plugin.key:feature-a", "plugin.key:sub-a", "plugin.key:deep-a");
        final List<String> urls = getUrls();

        // then
        assertThat(urls, matches(contextBatchSubsetUrl("context.features", asList("plugin.key:feature-c"), "js")));
    }

    @Test
    public void
            given_conditionCFails_when_includingFeaturesContext_and_excludingFeaturesCAndAAndSubtrees_then_shouldReduceExclusionsToFeatureC() {
        // given
        DeepConditionalTree.setConditionFeatureC(false);

        // when
        requireContexts("context.features");
        excludeWebResources("plugin.key:feature-c", "plugin.key:feature-a", "plugin.key:sub-a", "plugin.key:deep-a");
        final List<String> urls = getUrls();

        // then
        assertThat(urls, matches(contextBatchSubsetUrl("context.features", asList("plugin.key:feature-c"), "js")));
    }

    @Test
    public void given_allConditionsPass_when_includingAndExcludingFeaturesContext_then_shouldReturnNothing() {
        // given

        // when
        requireContexts("context.features");
        excludeContexts("context.features");
        final List<String> urls = getUrls();

        // then
        assertThat(urls, Matchers.emptyCollectionOf(String.class));
    }

    private List<String> getUrls() {
        final Iterable<WebResource> results = assembler.assembled().peek().getResources();
        return StreamSupport.stream(results.spliterator(), false)
                .filter(PluginUrlResource.class::isInstance)
                .map(PluginUrlResource.class::cast)
                .map(r -> r.getStaticUrl(UrlMode.RELATIVE))
                .collect(toList());
    }

    private void requireContexts(final String... keys) {
        Arrays.stream(keys).forEach(ctx -> assembler.resources().requireContext(ctx));
    }

    private void excludeWebResources(final String... keys) {
        Arrays.stream(keys).forEach(wr -> assembler.resources().exclude(singleton(wr), emptySet()));
    }

    private void excludeContexts(final String... keys) {
        Arrays.stream(keys).forEach(ctx -> assembler.resources().exclude(emptySet(), singleton(ctx)));
    }
}
