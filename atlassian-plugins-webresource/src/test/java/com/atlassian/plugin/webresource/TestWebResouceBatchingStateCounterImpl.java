package com.atlassian.plugin.webresource;

import junit.framework.TestCase;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestWebResouceBatchingStateCounterImpl extends TestCase {
    private PluginEventManager pluginEventManager = mock(PluginEventManager.class);
    private WebResourceBatchingStateCounterImpl batchingStateCounter;

    private ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
    private PluginController pluginController = mock(PluginController.class);
    private PluginAccessor pluginAccessor = mock(PluginAccessor.class);

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        batchingStateCounter = new WebResourceBatchingStateCounterImpl(pluginEventManager);
    }

    public void testNoIncrementDuringStartUp() {
        batchingStateCounter.onPluginModuleEnabled(new PluginModuleEnabledEvent(moduleDescriptor));
        batchingStateCounter.onPluginModuleDisabled(new PluginModuleDisabledEvent(moduleDescriptor, false));
        assertEquals(0, batchingStateCounter.getBatchingStateCounter());
    }

    public void testIncrementAfterStartUp() {
        batchingStateCounter.onPluginModuleEnabled(new PluginModuleEnabledEvent(moduleDescriptor));
        batchingStateCounter.onPluginFrameworkStarted(
                new PluginFrameworkStartedEvent(pluginController, pluginAccessor));

        batchingStateCounter.onPluginModuleEnabled(new PluginModuleEnabledEvent(moduleDescriptor));
        batchingStateCounter.onPluginModuleEnabled(new PluginModuleEnabledEvent(moduleDescriptor));
        assertEquals(3, batchingStateCounter.getBatchingStateCounter());
    }

    public void testNoIncrementAfterShutdown() {
        batchingStateCounter.onPluginFrameworkStarted(
                new PluginFrameworkStartedEvent(pluginController, pluginAccessor));
        batchingStateCounter.onPluginModuleEnabled(new PluginModuleEnabledEvent(moduleDescriptor));
        assertEquals(2, batchingStateCounter.getBatchingStateCounter());

        batchingStateCounter.onPluginFrameworkPluginFrameworkShutdown(
                new PluginFrameworkShutdownEvent(pluginController, pluginAccessor));
        batchingStateCounter.onPluginModuleEnabled(new PluginModuleEnabledEvent(moduleDescriptor));
        assertEquals(2, batchingStateCounter.getBatchingStateCounter());
    }

    public void testUnregisterListenerAfterClose() {
        batchingStateCounter.close();
        verify(pluginEventManager).unregister(batchingStateCounter);
    }
}
