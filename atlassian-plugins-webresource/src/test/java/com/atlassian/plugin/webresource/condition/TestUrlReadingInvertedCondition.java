package com.atlassian.plugin.webresource.condition;

import org.junit.Test;

import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.webresource.TestUtils.emptyQueryParams;
import static com.atlassian.plugin.webresource.condition.ConditionTestUtils.decorate;

public class TestUrlReadingInvertedCondition {
    final UrlBuildingStrategy normalStrategy = UrlBuildingStrategy.normal();

    @Test
    public void testInvertAddToUrl() {
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        UrlReadingCondition mockCondition = mock(UrlReadingCondition.class);
        DecoratingCondition inverted = decorate(mockCondition).invertCondition();

        inverted.addToUrl(urlBuilder, normalStrategy);

        verify(mockCondition).addToUrl(urlBuilder);
    }

    @Test
    public void testInvertShouldDisplayFalse() {
        QueryParams queryParams = emptyQueryParams();
        UrlReadingCondition mockCondition = mock(UrlReadingCondition.class);
        when(mockCondition.shouldDisplay(queryParams)).thenReturn(true);
        DecoratingCondition inverted = decorate(mockCondition).invertCondition();

        assertFalse(inverted.shouldDisplay(queryParams));
    }

    @Test
    public void testInvertShouldDisplayTrue() {
        QueryParams queryParams = emptyQueryParams();
        UrlReadingCondition mockCondition = mock(UrlReadingCondition.class);
        when(mockCondition.shouldDisplay(queryParams)).thenReturn(false);
        DecoratingCondition inverted = decorate(mockCondition).invertCondition();

        assertTrue(inverted.shouldDisplay(queryParams));
    }
}
