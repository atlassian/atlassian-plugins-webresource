package com.atlassian.plugin.webresource.assembler;

import java.io.StringWriter;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import static com.atlassian.plugin.webresource.TestUtils.removeWebResourceLogs;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INLINE;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE;

public class TestWebResourceWithInteractionPhase {
    private StringWriter writer;
    private AssemblerTestFixture f;

    @Before
    public void setUp() {
        f = new AssemblerTestFixture();
        f.mockPlugin("test.atlassian:first-feature")
                .res("first-1.js")
                .res("first-2.js")
                .res("first-1.css")
                .res("first-2.css")
                .ctx("first", "all");
        f.mockPlugin("test.atlassian:second-feature")
                .res("second-1.css")
                .res("second-1.js")
                .res("second-2.css")
                .res("second-2.js")
                .ctx("second", "all");
        f.mockPlugin("test.atlassian:third-feature")
                .res("third-1.css")
                .res("third-1.js")
                .deps("test.atlassian:first-feature", "test.atlassian:second-feature")
                .ctx("third", "all");

        f.mockContent("first-1.css", ".first-1{color:red}");
        f.mockContent("first-2.css", ".first-2{color:#f00}");
        f.mockContent("second-1.css", ".second-1{color:green}");
        f.mockContent("second-2.css", ".second-2{color:#0f0}");
        f.mockContent("third-1.css", ".third{color:blue}");

        f.mockContent("first-1.js", "console.log('first-1');");
        f.mockContent("first-2.js", "console.log('first-2');");
        f.mockContent("second-1.js", "console.log('second-1');");
        f.mockContent("second-2.js", "console.log('second-2');");
        f.mockContent("third-1.js", "console.log('third');");

        writer = new StringWriter();
    }

    @Test
    public void testInteractivePartiallyOutputIfSubsetWebResourceRequestedAsRequire() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "all");
        assembler.resources().requireWebResource(REQUIRE, "test.atlassian:first-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expectedBatchTags = "<link rel=\"stylesheet\""
                + " href=\"/download/batch/test.atlassian:first-feature/test.atlassian:first-feature.css\""
                + " data-wrm-key=\"test.atlassian:first-feature\""
                + " data-wrm-batch-type=\"resource\""
                + " media=\"all\">\n"
                + "<script src=\"/download/batch/test.atlassian:first-feature/test.atlassian:first-feature.js\""
                + " data-wrm-key=\"test.atlassian:first-feature\""
                + " data-wrm-batch-type=\"resource\""
                + " data-initially-rendered></script>";
        final String expectedInteraction = "<script type=\"module\">WRM.requireLazily([\"wrc!all\"])</script>";
        assertThat(actual, equalTo(expectedBatchTags + "\n" + expectedInteraction));
    }

    @Test
    public void testInteractivePartiallyOutputIfSubsetContextRequestedAsRequire() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "all");
        assembler.resources().requireContext(REQUIRE, "first");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expectedBatchTags =
                "<link rel=\"stylesheet\"" + " href=\"/download/contextbatch/css/first/batch.css\""
                        + " data-wrm-key=\"first\""
                        + " data-wrm-batch-type=\"context\""
                        + " media=\"all\">\n"
                        + "<script src=\"/download/contextbatch/js/first/batch.js\""
                        + " data-wrm-key=\"first\""
                        + " data-wrm-batch-type=\"context\""
                        + " data-initially-rendered></script>";
        final String expectedInteraction = "<script type=\"module\">WRM.requireLazily([\"wrc!all\"])</script>";
        assertThat(actual, equalTo(expectedBatchTags + "\n" + expectedInteraction));
    }

    @Test
    public void testInteractiveNotOutputIfSupersetWebResourcesRequestedAsRequire() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "first");
        assembler.resources().requireWebResource(REQUIRE, "test.atlassian:third-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        assertThat(actual, not(containsString("WRM.requireLazily")));
    }

    @Test
    public void testInteractiveNotOutputIfSupersetContextRequestedAsInline() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "first");
        assembler.resources().requireContext(INLINE, "all");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        assertThat(actual, not(containsString("WRM.requireLazily")));
    }

    @Test
    public void testInteractiveNotOutputIfSupersetWebResourcesRequestedAsInline() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "first");
        assembler.resources().requireWebResource(INLINE, "test.atlassian:third-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        assertThat(actual, not(containsString("WRM.requireLazily")));
    }

    @Test
    public void testInteractiveNotOutputIfSupersetContextRequestedAsRequire() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "first");
        assembler.resources().requireContext(REQUIRE, "all");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        assertThat(actual, not(containsString("WRM.requireLazily")));
    }
}
