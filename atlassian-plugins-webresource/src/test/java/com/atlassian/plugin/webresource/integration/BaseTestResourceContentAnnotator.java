package com.atlassian.plugin.webresource.integration;

import com.atlassian.plugin.webresource.impl.annotators.ResourceContentAnnotator;

/**
 *
 */
public class BaseTestResourceContentAnnotator extends ResourceContentAnnotator {
    @Override
    public int hashCode() {
        return getClass().getName().hashCode();
    }
}
