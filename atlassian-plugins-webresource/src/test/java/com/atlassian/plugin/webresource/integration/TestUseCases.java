package com.atlassian.plugin.webresource.integration;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.AlwaysFalseCondition;
import com.atlassian.plugin.webresource.AlwaysTrueCondition;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.impl.annotators.ResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.integration.stub.EmptyDataProvider;
import com.atlassian.plugin.webresource.integration.transformers.AddLocation;
import com.atlassian.plugin.webresource.integration.transformers.AddLocationStatic;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.plugin.webresource.TestUtils.buildList;
import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.getField;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.pluginResourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrlRelativeToContextBatch;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrlRelativeToWebResourceBatch;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;

// Tests for base use cases, happy path.
public class TestUseCases extends TestCase {
    @Test
    public void shouldIncludeBatchForContext() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js", "content of a1")
                .webResource("b")
                .dependency("plugin:a")
                .context("general")
                .resource("b1.js", "content of b1")
                .webResource("c")
                .context("general")
                .resource("c1.js", "content of c1")
                .resource("c2.js", "content of c2")
                .webResource("d")
                .context("special")
                .resource("d1.js", "content of d1")
                .end();

        wr.requireContext("general");
        wr.requireContext("special");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(contextBatchUrl("general", "js"), contextBatchUrl("special", "js")));

        assertThat(
                wr.getContent(wr.lastDrain()),
                allOf(matches("content of a1", "content of b1"), matches("content of c1", "content of c2")));
    }

    @Test
    public void shouldIncludeWebResourceBatch() {
        // Same as testSingleResourceNoDeps
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .resource("a.js", "content of a")
                .end();

        wr.requireResource("plugin:a");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(webResourceBatchUrl("plugin:a", "js")));
        assertThat(wr.getContent(wr.lastDrain()), matches("content of a"));
    }

    @Test
    public void shouldApplyTransformersToSingleResource() {
        // Same as testSingleResourceNoDeps
        wr.configure()
                .plugin("plugin")
                .transformer("AddLocation", AddLocation.class)
                .webResource("a")
                .transformation("js", "AddLocation")
                .resource("a.js", "content of a")
                .end();

        assertThat(
                wr.getContent(resourceUrl("plugin:a", "a.js", buildMap("location", "true"))),
                matches("location=plugin:a/a.js", "content of a"));
    }

    @Test
    public void shouldServeSingleResource() {
        wr.configure()
                .plugin("app")
                .webResource("a")
                .resource("a.js", "content of a")
                .end();
        assertThat(wr.getContent(resourceUrl("app:a", "a.js")), matches("content of a"));
    }

    @Test
    public void shouldServeSinglePluginResource() {
        wr.configure().plugin("app").resource("a.js", "content of a").end();
        assertThat(wr.getContent(pluginResourceUrl("app", "a.js")), matches("content of a"));
    }

    @Test
    public void shouldIncludeResourceWithDependencies() {
        // Same as testSingleResourceWithDeps.
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js", "content of a1")
                .webResource("b")
                .dependency("plugin:a")
                .resource("b1.js", "content of b1")
                .resource("b2.js", "content of b2")
                .end();

        wr.requireResource("plugin:b");
        wr.drain();

        assertThat(
                wr.lastDrain().urls(),
                matches(webResourceBatchUrl("plugin:a", "js"), webResourceBatchUrl("plugin:b", "js")));

        assertThat(wr.getContent(wr.lastDrain()), matches("content of a", "content of b", "content of b2"));
    }

    @Test
    public void shouldNotIncludeDependenciesTwice() {
        // Same as testSingleResourceMultiDeps
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .resource("a.js", "content of a")
                .webResource("b")
                .dependency("plugin:a")
                .resource("b.js", "content of b")
                .webResource("c")
                .dependency("plugin:a")
                .dependency("plugin:b")
                .resource("c.js", "content of c")
                .end();

        wr.requireResource("plugin:c");
        wr.drain();

        assertThat(
                wr.lastDrain().urls(),
                matches(
                        webResourceBatchUrl("plugin:a", "js"),
                        webResourceBatchUrl("plugin:b", "js"),
                        webResourceBatchUrl("plugin:c", "js")));

        assertThat(
                wr.getContent(wr.lastDrain()),
                allOf(
                        matches("content of a", "content of b", "content of c"),
                        not(matches("content of a", "content of a"))));
    }

    @Test
    public void shouldEvaluateConditions() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .condition(AlwaysTrueCondition.class)
                .context("general")
                .resource("a1.js", "content of a1")
                .webResource("b")
                .condition(AlwaysFalseCondition.class)
                .context("general")
                .resource("b1.js", "content of b1")
                .end();

        wr.requireContext("general");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(contextBatchUrl("general", "js")));

        assertThat(wr.getContent(wr.lastDrain()), allOf(matches("content of a1"), not(matches("content of b1"))));
    }

    @Test
    public void shouldApplyStaticTransformers() {
        wr.configure()
                .setStaticTransformers(new AddLocationStatic())
                .plugin("plugin")
                .webResource("a")
                .context("general")
                .resource("a1.js", "content of a1")
                .webResource("b")
                .context("general")
                .resource("b1.js", "content of b1")
                .end();

        wr.requireContext("general");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(contextBatchUrl("general", "js", buildMap("location", "true"))));

        String content = wr.getContent(wr.lastDrain());
        assertThat(
                content,
                matches(
                        "location=plugin:a/a1.js (static)\ncontent of a1",
                        "location=plugin:b/b1.js (static)\ncontent of b1"));
    }

    @Test
    public void shouldApplyTransformers() {
        wr.configure()
                .plugin("plugin")
                .transformer("AddLocation", AddLocation.class)
                .webResource("a")
                .context("general")
                .transformation("js", "AddLocation")
                .resource("a1.js", "content of a1")
                .webResource("b")
                .context("general")
                .resource("b1.js", "content of b1")
                .end();

        wr.requireContext("general");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(contextBatchUrl("general", "js", buildMap("location", "true"))));

        assertThat(
                wr.getContent(wr.lastDrain()),
                allOf(
                        matches("location=plugin:a/a1.js (transformer)\ncontent of a1", "content of b1"),
                        not(matches("location=plugin:b/b1.js (transformer)\ncontent of b1"))));
    }

    @Test
    public void shouldApplyAnnotators() {
        wr.configure()
                .setAnnotators(new ResourceContentAnnotator[] {
                    new BaseTestResourceContentAnnotator() {
                        @Override
                        public int beforeResourceInBatch(
                                LinkedHashSet<String> requiredResources,
                                Resource resource,
                                final Map<String, String> params,
                                OutputStream stream)
                                throws IOException {
                            stream.write("before ".getBytes());
                            return 0;
                        }

                        @Override
                        public void afterResourceInBatch(
                                LinkedHashSet<String> requiredResources,
                                Resource resource,
                                final Map<String, String> params,
                                OutputStream stream)
                                throws IOException {
                            stream.write(" after".getBytes());
                        }
                    }
                })
                .plugin("plugin")
                .webResource("a")
                .context("general")
                .resource("a1.js", "content of a1")
                .webResource("b")
                .context("general")
                .resource("b1.js", "content of b1")
                .end();

        wr.requireContext("general");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(contextBatchUrl("general", "js")));

        String content = wr.getContent(wr.lastDrain());
        assertThat(content, matches("before.*content of a1.*after", "before.*content of b1.*after"));
    }

    // old name testGetDownloadableBatchResourceFallbacksToSingle
    @Test
    public void shouldServeSingleResourceRealtiveToBatch() {
        wr.configure()
                .plugin("app")
                .webResource("page")
                .resource("style.css")
                .resource("image.png")
                .end();

        String content = wr.getContent(resourceUrlRelativeToWebResourceBatch("app:page", "image.png"));
        assertThat(content, CoreMatchers.equalTo("content of image.png"));
    }

    // old name testGetDownloadableContextBatchSubResource
    @Test
    public void shouldServeFilesRelativeToContextBatch() {
        wr.configure()
                .plugin("app")
                .webResource("page")
                .context("general")
                .resource("style.css")
                .resource("image.png")
                .end();

        String url = resourceUrlRelativeToContextBatch("general", "css", "image.png");
        assertThat(wr.getResource(url).getName(), equalTo("image.png"));
        assertThat(wr.getContent(url), equalTo("content of image.png"));
    }

    @Test
    public void shouldServeResourcesWithRelativePathName() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .resource("web/style.css/", "content of style.css", "file-path/style.css")
                .withRelativeFile("/relative.png", "a relative revelation")
                .end();

        assertThat(
                wr.getContent(resourceUrl("plugin:a", "web/style.css/relative.png")), equalTo("a relative revelation"));
    }

    @Test
    public void shouldServePluginResourcesWithRelativePathName() {
        wr.configure()
                .plugin("plugin")
                .resource("web/style.css/", "content of style.css", "file-path/style.css")
                .withRelativeFile("/relative.png", "a relative triumph!")
                .end();

        assertThat(
                wr.getContent(resourceUrl("plugin:nonExistingWebResourceKey", "web/style.css/relative.png")),
                equalTo("a relative triumph!"));
        assertThat(wr.getContent(resourceUrl("plugin", "web/style.css/relative.png")), equalTo("a relative triumph!"));
    }

    @Test
    public void shouldFallBackToPluginIfWebResourceNotExist() {
        wr.configure().plugin("plugin").resource("style.css").end();

        assertThat(
                wr.getContent(resourceUrl("plugin:nonExistingWebResourceKey", "style.css")),
                equalTo("content of style.css"));
    }

    /**
     * @deprecated since v3.3.2
     */
    @Test
    @Deprecated
    public void shouldSupportOutdatedMethodForConfluence() {
        wr.configure().plugin("plugin").webResource("a").resource("style.css").end();

        PluginResourceLocator resourceLocator = (PluginResourceLocator) getField(wr, "resourceLocator");
        DownloadableResource downloadableResource =
                resourceLocator.getDownloadableResource(resourceUrl("plugin:a", "style.css"), buildMap());
        assertThat(TestUtils.toString(downloadableResource), equalTo("content of style.css"));
    }

    @Test
    public void shouldNotIncludeDependenciesFromNotSatisfiedConditions() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .dependency("plugin:b")
                .context("general")
                .resource("a1.js")
                .webResource("b")
                .dependency("plugin:c")
                .condition(AlwaysFalseCondition.class)
                .resource("b1.js")
                .webResource("c")
                .resource("c1.js")
                .end();

        wr.requireContext("general");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(contextBatchUrl("general", "js")));

        String content = wr.getContent(wr.lastDrain());
        assertThat(content, not(matches("content of b1")));
        assertThat(content, not(matches("content of c1")));
    }

    @Test
    public void shouldNotIncludeQueryStringFromDependenciesFromNotSatisfiedConditions() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .dependency("plugin:b")
                .context("general")
                .resource("a1.js")
                .webResource("b")
                .dependency("plugin:c")
                .condition(AlwaysFalseCondition.class)
                .resource("b1.js")
                .webResource("c")
                .condition(AlwaysTrueCondition.class)
                .resource("c1.js")
                .end();

        wr.requireContext("general");
        List<String> paths = wr.drain().urls();

        assertThat(paths, matches(contextBatchUrl("general", "js", buildMap("always-false", "true"))));

        assertThat(paths, not(matches("always-true")));
    }

    @Test
    public void shouldNotEvaluateConditionsFromDependenciesFromNotSatisfiedConditions() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .dependency("plugin:b")
                .context("general")
                .resource("a1.js")
                .webResource("b")
                .dependency("plugin:c")
                .condition(AlwaysFalseCondition.class)
                .resource("b1.js")
                .webResource("c")
                .condition(AlwaysTrueCondition.class)
                .resource("c1.js")
                .end();

        AlwaysTrueCondition.log.clear();
        wr.requireContext("general");
        wr.drain();
        assertThat(AlwaysTrueCondition.log.size(), equalTo(0));
    }

    @Test
    public void shouldNotIncludeDependenciesFromNotSatisfiedConditionsComplexCase() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .context("general")
                .dependency("plugin:b")
                .dependency("plugin:c")
                .resource("a1.js")
                .webResource("b")
                .condition(AlwaysFalseCondition.class)
                .dependency("plugin:d")
                .dependency("plugin:e")
                .resource("b1.js")
                .webResource("c")
                .dependency("plugin:d")
                .resource("c1.js")
                .webResource("d")
                .resource("d1.js")
                .webResource("e")
                .resource("e1.js")
                .end();

        wr.requireContext("general");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(contextBatchUrl("general", "js")));

        assertThat(
                wr.getContent(wr.lastDrain()),
                allOf(
                        matches("content of d1", "content of c1", "content of a1"),
                        not(matches("content of b1")),
                        not(matches("content of e1"))));
    }

    @Test
    public void shouldSupplyData() {
        wr.configure()
                .addToSuperbatch("plugin:a")
                .plugin("plugin")
                .webResource("a")
                .data("a", EmptyDataProvider.class)
                .webResource("b")
                .data("b", EmptyDataProvider.class)
                .webResource("c")
                .context("general")
                .data("c", EmptyDataProvider.class)
                .webResource("d")
                .data("d", EmptyDataProvider.class)
                .webResource("e")
                .condition(AlwaysFalseCondition.class)
                .data("e", EmptyDataProvider.class)
                .webResource("f")
                .condition(AlwaysFalseCondition.class)
                .data("f", EmptyDataProvider.class)
                .end();

        wr.requireResource("plugin:b");
        wr.requireContext("general");
        wr.requireResource("plugin:d");

        String html = wr.drain().html();

        assertThat(
                html,
                allOf(
                        containsString("plugin:a.a"),
                        containsString("plugin:b.b"),
                        containsString("plugin:c.c"),
                        containsString("plugin:d.d")));
        assertThat(html, not(containsString("plugin:e.e")));
        assertThat(html, not(containsString("plugin:f.f")));
    }

    @Test
    public void shouldIgnoreParamsFromNonBatchableResources() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .resource("a1.png")
                .param("media", "print")
                .resource("a2.js")
                .end();

        wr.requireResource("plugin:a");
        List<String> paths = wr.drain().urls();

        assertThat(paths, matches(webResourceBatchUrl("plugin:a", "js")));
        assertThat(paths.get(0), not(containsString("media")));
    }

    @Test
    public void shouldNotIncludeDependenciesFromNotSatisfiedConditionsInWebResourceBatch() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .dependency("plugin:b")
                .resource("a1.js")
                .webResource("b")
                .dependency("plugin:c")
                .condition(AlwaysFalseCondition.class)
                .resource("b1.js")
                .webResource("c")
                .resource("c1.js")
                .end();

        wr.requireResource("plugin:a");
        wr.drain();
        assertThat(wr.lastDrain().urls(), matches(webResourceBatchUrl("plugin:a", "js")));
    }

    @Test
    public void shouldServeResourceBatchWithoutEvaluatingCondition() {
        wr.configure()
                .plugin("p")
                .webResource("a")
                .condition(AlwaysFalseCondition.class)
                .resource("a1.css")
                .webResource("b")
                .condition(AlwaysFalseCondition.class)
                .resource("b1.css")
                .end();

        assertThat(wr.getContent(resourceUrl("p:a", "a1.css")), matches("content of a1.css"));
        assertThat(wr.getContent(resourceUrl("p:b", "b1.css")), matches("content of b1.css"));
    }

    @Test
    public void shouldNotExcludeRequiredDependency() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js")
                .webResource("b")
                .dependency("plugin:a")
                .condition(AlwaysFalseCondition.class)
                .context("general")
                .resource("b1.js")
                .plugin("plugin2")
                .webResource("c")
                .dependency("plugin:a")
                .resource("c1.js")
                .end();

        wr.requireContext("general");
        wr.requireResource("plugin2:c");
        wr.drain();

        assertThat(wr.getContent(wr.lastDrain()), matches("content of a1"));
    }

    @Test
    public void shouldAppendConditionalParametersForEmptyResourceWithNonEmptyDependencies() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .condition(AlwaysTrueCondition.class)
                .context("general")
                .dependency("plugin:b")
                .webResource("b")
                .resource("b1.js")
                .end();

        wr.requireContext("general");

        assertThat(wr.drain().urls(), matches(contextBatchUrl("general", "js", buildMap("always-true", "true"))));
    }

    @Test
    public void shouldAppendConditionalParametersForEmptyResourceWithNonEmptyDependenciesForWebResourceBatch() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .condition(AlwaysTrueCondition.class)
                .context("general")
                .dependency("plugin:b")
                .webResource("b")
                .resource("b1.js")
                .end();

        wr.requireResource("plugin:a");

        assertThat(wr.drain().urls(), matches(webResourceBatchUrl("plugin:b", "js")));
    }

    @Test
    public void shouldEvaluateUrlReadingConditionsOnlyOnce() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .condition(AlwaysTrueCondition.class)
                .context("general")
                .resource("a1.css")
                .resource("a1.js")
                .webResource("b")
                .condition(AlwaysTrueCondition.class)
                .context("general")
                .resource("b1.css")
                .resource("b1.js")
                .end();

        // First call, the cache not exists yet, legacy implementation used to calculate batch.
        AlwaysTrueCondition.log.clear();
        wr.requireContext("general");
        wr.drain();
        assertThat(AlwaysTrueCondition.log, equalTo(buildList("addToUrl", "shouldDisplay")));

        // Second call, the cache should be ready, there should be only one call for condition.
        AlwaysTrueCondition.log.clear();
        wr.reset();
        wr.requireContext("general");
        wr.drain();
        assertThat(AlwaysTrueCondition.log, equalTo(buildList("addToUrl", "shouldDisplay")));

        // Getting the content.
        AlwaysTrueCondition.log.clear();
        wr.getContent(contextBatchUrl("general", "js"));
        assertThat(AlwaysTrueCondition.log, equalTo(buildList("shouldDisplay")));
    }

    @Test
    public void shouldServeImplicitContext() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js")
                .webResource("b")
                .dependency("plugin:a")
                .context("general")
                .resource("b1.js")
                .end();

        assertThat(wr.getContent(contextBatchUrl("plugin:b", "js")), matches("content of a1", "content of b1"));
    }

    @Test
    public void shouldMaintainResourceOrdering() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js")
                .resource("a2.js")
                .end();

        assertThat(wr.getContent(webResourceBatchUrl("plugin:a", "js")), matches("content of a1", "content of a2"));
    }

    @Test
    public void shouldProcessUrlReadingConditionsForWebResourcesWithoutResources() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .context("general")
                .dependency("plugin:b")
                .resource("a1.js")
                .webResource("b")
                .condition(AlwaysTrueCondition.class)
                .dependency("plugin:c")
                .webResource("c")
                .resource("c1.js")
                .end();

        wr.requireContext("general");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(contextBatchUrl("general", "js", buildMap("always-true", "true"))));
    }

    @Test
    public void shouldIncludeDependenciesForEmptyResource() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .context("general")
                .dependency("plugin:b")
                .resource("a1.js")
                .webResource("b")
                .condition(AlwaysTrueCondition.class)
                .dependency("plugin:c")
                .webResource("c")
                .resource("c1.js")
                .end();

        wr.requireContext("general");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(contextBatchUrl("general", "js", buildMap("always-true", "true"))));

        assertThat(wr.getContent(wr.lastDrain()), matches("content of c1", "content of a1"));
    }
}
