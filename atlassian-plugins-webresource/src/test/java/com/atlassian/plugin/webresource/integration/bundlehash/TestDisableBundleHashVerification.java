package com.atlassian.plugin.webresource.integration.bundlehash;

import java.util.List;

import org.junit.Test;

import com.atlassian.plugin.webresource.integration.stub.ResponseData;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import static com.atlassian.plugin.webresource.ResourceUtils.STATIC_HASH;
import static com.atlassian.plugin.webresource.ResourceUtils.WRM_INTEGRITY;
import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.impl.config.Config.ENABLE_BUNDLE_HASH_VALIDATION;

public class TestDisableBundleHashVerification extends BundleHashTestCase {
    private static final String INVALID_BUNDLEHASH = "123invalid";

    /**
     * Prepares three web-resources `feature-a`, `feature-b`, and `feature-c`:
     * <ul>
     *     <li>Features A and B are in the `foo` context</li>
     *     <li>Features B and C are in the `bar` context</li>
     * </ul>
     */
    @Override
    protected void setUp() throws Exception {
        wr.configure()
                .plugin("plugin.key")
                .webResource("feature-a")
                .context("foo")
                .resource("feature-a.js")
                .webResource("feature-b")
                .context("foo")
                .context("bar")
                .resource("feature-b.js")
                .webResource("feature-c")
                .context("bar")
                .resource("feature-c.js")
                .end();
    }

    @Test
    public void given_enabledViaConfig_when_contextBatchRequested_then_response404s() {
        // given
        System.setProperty(ENABLE_BUNDLE_HASH_VALIDATION, Boolean.TRUE.toString());

        // when
        wr.requireContext("foo");
        final List<String> paths = wr.paths();
        final ResponseData resp = wr.get(paths.get(0), buildMap(STATIC_HASH, INVALID_BUNDLEHASH));

        // then
        assertThat(resp.getStatus(), is(404));
    }

    @Test
    public void given_disabledViaConfig_when_contextBatchRequested_then_responseWorks() {
        // given
        System.setProperty(ENABLE_BUNDLE_HASH_VALIDATION, Boolean.FALSE.toString());

        // when
        wr.requireContext("foo");
        final List<String> paths = wr.paths();
        final ResponseData resp = wr.get(paths.get(0), buildMap(STATIC_HASH, INVALID_BUNDLEHASH));

        // then
        assertThat(resp.getStatus(), is(200));
    }

    @Test
    public void given_disabledViaHttpRequest_when_contextBatchRequested_then_responseWorks() {
        // given
        System.setProperty(ENABLE_BUNDLE_HASH_VALIDATION, Boolean.TRUE.toString());

        // when
        wr.requireContext("foo");
        final List<String> paths = wr.paths();
        final ResponseData resp =
                wr.get(paths.get(0), buildMap(STATIC_HASH, INVALID_BUNDLEHASH, WRM_INTEGRITY, "no-validate"));

        // then
        assertThat(resp.getStatus(), is(200));
    }
}
