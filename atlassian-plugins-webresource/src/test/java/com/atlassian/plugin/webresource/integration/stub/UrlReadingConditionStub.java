package com.atlassian.plugin.webresource.integration.stub;

import java.util.Map;

import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

import static com.atlassian.plugin.webresource.TestUtils.getField;

public class UrlReadingConditionStub implements UrlReadingCondition {
    public static Map<String, String> params;

    @Override
    public void init(Map<String, String> params) {}

    @Override
    public void addToUrl(UrlBuilder urlBuilder) {}

    @Override
    public boolean shouldDisplay(QueryParams params) {
        UrlReadingConditionStub.params = (Map<String, String>) getField(params, "map");
        return true;
    }
}
