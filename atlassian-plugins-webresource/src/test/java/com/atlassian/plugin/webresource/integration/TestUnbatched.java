package com.atlassian.plugin.webresource.integration;

import org.junit.Test;

import com.atlassian.plugin.webresource.AlwaysFalseCondition;
import com.atlassian.plugin.webresource.AlwaysTrueCondition;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.transformers.AddLocation;

import static org.junit.Assert.assertThat;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;

public class TestUnbatched extends TestCase {
    protected void configure(WebResource.ConfigurationDsl wr) {
        wr.plugin("plugin")
                .transformer("AddLocation", AddLocation.class)
                .webResource("a")
                .context("general")
                .transformation("js", "AddLocation")
                .resource("a1.js")
                .webResource("b")
                .context("general")
                .resource("b1.js")
                .webResource("c")
                .condition(AlwaysTrueCondition.class)
                .context("general")
                .resource("c1.js")
                .webResource("d")
                .condition(AlwaysFalseCondition.class)
                .context("general")
                .resource("d1.js")
                .end();
    }

    @Test
    public void testWebResourceBatch() {
        configure(wr.configure().disableContextBatching());

        wr.requireContext("general");

        assertThat(
                wr.paths(),
                matches(
                        webResourceBatchUrl("plugin:a", "js", buildMap("location", "true")),
                        webResourceBatchUrl("plugin:b", "js"),
                        webResourceBatchUrl("plugin:c", "js")));
    }

    @Test
    public void testUnbatched() {
        configure(wr.configure().disableContextBatching().disableWebResourceBatching());

        wr.requireContext("general");

        assertThat(
                wr.paths(),
                matches(
                        resourceUrl("plugin:a", "a1.js", buildMap("location", "true")),
                        resourceUrl("plugin:b", "b1.js"),
                        resourceUrl("plugin:c", "c1.js")));
    }
}
