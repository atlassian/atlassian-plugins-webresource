package com.atlassian.plugin.webresource.integration.stub;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.http.Controller;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;

public class RouterWithoutHashesCapturingHandlerCalls extends RouterWithoutHashes {
    public String[] last_args = null;

    public RouterWithoutHashesCapturingHandlerCalls(Globals globals) {
        super(globals);
    }

    protected void callHandler(
            Handler handler, Controller controller, Request request, Response response, String... args) {
        this.last_args = args;

        super.callHandler(handler, controller, request, response, args);
    }
}
