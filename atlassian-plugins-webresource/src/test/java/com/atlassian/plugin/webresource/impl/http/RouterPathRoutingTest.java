package com.atlassian.plugin.webresource.impl.http;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;

import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;
import com.atlassian.plugin.webresource.integration.stub.RouterWithoutHashesCapturingHandlerCalls;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

@RunWith(JUnitParamsRunner.class)
public class RouterPathRoutingTest {

    private Globals globals;
    private RouterWithoutHashesCapturingHandlerCalls router;

    @Before
    public void setUp() throws Exception {
        globals = TestUtils.createGlobals(
                TestUtils.createWebResourceIntegration(), RouterWithoutHashesCapturingHandlerCalls.class);
        router = (RouterWithoutHashesCapturingHandlerCalls) globals.getRouter();
    }

    @After
    public void tearDown() throws Exception {}

    // This is a full list of sorted unique request paths extracted from the logs after running the Client Side
    // Extensions test suite
    // Please don't extend it manually and add any future scenarios to their separate methods/tests etc
    private Object[] getCSEItPaths() {
        Stream<String> paths = Stream.of(
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/2c2e7b57a1e38de3e65b807e1b61af5e/_/download/contextbatch/js/com.atlassian.analytics.analytics-client:policy-update,-_super/batch.js",
                "/refapp/download/contextbatch/js/atl.admin,-_super,-com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async,-com.atlassian.refapp.decorator:decorator-resources/batch.js",
                "/refapp/s/0302f689882e0d9236bc7ae7b9dee115-CDN/986556196/1713866807582/1/32361940f8dce1785d1475bb8fe607da/_/download/contextbatch/js/atl.general,-_super,-com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async,-com.atlassian.refapp.decorator:decorator-resources,-com.atlassian.plugins.atlassian-clientside-extensions-page-bootstrapper:client-resources/batch.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/015cef454ddeb22eafca8d7b22a19bd8/_/download/contextbatch/js/com.atlassian.plugins.atlassian-clientside-extensions-demo:demo-async-tabs,-_super/batch.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/074ced4d93ce725b53fb5076781f4a4a/_/download/contextbatch/js/com.atlassian.plugins.atlassian-clientside-extensions-demo:demo-basic,-_super/batch.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/074ced4d93ce725b53fb5076781f4a4a/_/download/contextbatch/js/com.atlassian.plugins.atlassian-clientside-extensions-demo:demo-basic,-_super/demo-basic.bundle.js.map;",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/0d6e8c98f411596a70f6fe39c08ea654/_/download/contextbatch/js/com.atlassian.plugins.atlassian-clientside-extensions-demo:demo-complex-page,-_super/batch.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/2946cffbab85f99a0deb873efff734e5/_/download/contextbatch/js/footer-links-handler,-_super,-header-links-handler/batch.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/2946cffbab85f99a0deb873efff734e5/_/download/contextbatch/js/footer-links-handler,-_super,-system-general-handler,-header-links-handler/batch.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/2c2e7b57a1e38de3e65b807e1b61af5e/_/download/contextbatch/js/com.atlassian.analytics.analytics-client:policy-update,-_super/batch.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/2e4c8c1313e4cb80437883edd899c365/_/download/contextbatch/js/homepage.resources.index,-_super/batch.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/3.0.0-m06-SNAPSHOT/_/download/batch/com.atlassian.plugins.atlassian-clientside-extensions-demo:common-runtime/com.atlassian.plugins.atlassian-clientside-extensions-demo:common-runtime.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/3.0.0-m06-SNAPSHOT/_/download/batch/com.atlassian.plugins.atlassian-clientside-extensions-demo:split_customElementHandler/com.atlassian.plugins.atlassian-clientside-extensions-demo:split_customElementHandler.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/3.0.0-m06-SNAPSHOT/_/download/batch/com.atlassian.plugins.atlassian-clientside-extensions-demo:split_my-page-extension-my-page-extension-tsx/com.atlassian.plugins.atlassian-clientside-extensions-demo:split_my-page-extension-my-page-extension-tsx.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/3.0.0-m06-SNAPSHOT/_/download/batch/com.atlassian.plugins.atlassian-clientside-extensions-demo:split_vendor/com.atlassian.plugins.atlassian-clientside-extensions-demo:split_vendor.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/75403b888ac42342a156e2c5068d7320/_/download/contextbatch/js/header.links,-_super,-com.atlassian.plugins.atlassian-clientside-extensions-demo:common-runtime,-com.atlassian.plugins.atlassian-clientside-extensions-demo:split_my-page-extension-my-page-extension-tsx,-com.atlassian.plugins.atlassian-clientside-extensions-demo:split_vendor/batch.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/89a738d96a06a0c97feb698144d52bc5/_/download/contextbatch/js/com.atlassian.plugins.atlassian-clientside-extensions-demo:tab-1-content,-_super/batch.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.component.async-header/com.atlassian.auiplugin:split_aui.component.async-header.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.component.button/com.atlassian.auiplugin:split_aui.component.button.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.component.dropdown2/com.atlassian.auiplugin:split_aui.component.dropdown2.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.component.message/com.atlassian.auiplugin:split_aui.component.message.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.component.spinner/com.atlassian.auiplugin:split_aui.component.spinner.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.core/com.atlassian.auiplugin:split_aui.core.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.pattern.table/com.atlassian.auiplugin:split_aui.pattern.table.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.084821f40b/com.atlassian.auiplugin:split_aui.splitchunk.084821f40b.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.36cd9d521c/com.atlassian.auiplugin:split_aui.splitchunk.36cd9d521c.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.3d2cb1af14/com.atlassian.auiplugin:split_aui.splitchunk.3d2cb1af14.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.4248e12b20/com.atlassian.auiplugin:split_aui.splitchunk.4248e12b20.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.57d1ac075f/com.atlassian.auiplugin:split_aui.splitchunk.57d1ac075f.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.5b8c290363/com.atlassian.auiplugin:split_aui.splitchunk.5b8c290363.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.5f851f97df/com.atlassian.auiplugin:split_aui.splitchunk.5f851f97df.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.9c48cc20a9/com.atlassian.auiplugin:split_aui.splitchunk.9c48cc20a9.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.af1935061c/com.atlassian.auiplugin:split_aui.splitchunk.af1935061c.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.b548966f06/com.atlassian.auiplugin:split_aui.splitchunk.b548966f06.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.baa83dbaf9/com.atlassian.auiplugin:split_aui.splitchunk.baa83dbaf9.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.e5af17b48e/com.atlassian.auiplugin:split_aui.splitchunk.e5af17b48e.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.ec75f48bb8/com.atlassian.auiplugin:split_aui.splitchunk.ec75f48bb8.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.ed86a19e01/com.atlassian.auiplugin:split_aui.splitchunk.ed86a19e01.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.f154095da3/com.atlassian.auiplugin:split_aui.splitchunk.f154095da3.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.vendors--105edd7c71/com.atlassian.auiplugin:split_aui.splitchunk.vendors--105edd7c71.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.vendors--9c48cc20a9/com.atlassian.auiplugin:split_aui.splitchunk.vendors--9c48cc20a9.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.vendors--c76910041b/com.atlassian.auiplugin:split_aui.splitchunk.vendors--c76910041b.js",
                "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/f787bcbcb95215e54a34bb99855f4a85/_/download/contextbatch/js/system-general-handler,-_super,-header-links-handler/batch.js",
                "/refapp/s/4662dd35d0b55f52a9c5cddb36fcad71-CDN/986556196/1713866807582/1/7f930069d22c1a77ea04523908431a4d/_/download/contextbatch/js/atl.admin,-_super,-com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async,-com.atlassian.refapp.decorator:decorator-resources/batch.js",
                "/refapp/s/5161fe4dae7334af1e241c4b5a1ca353-CDN/986556196/1713866807582/1/32361940f8dce1785d1475bb8fe607da/_/download/contextbatch/js/atl.general,-_super/batch.js",
                "/refapp/s/56f5e7775a5196a43038e637af04d001-CDN/986556196/1713866807582/1/75403b888ac42342a156e2c5068d7320/_/download/contextbatch/js/header.links,-_super/batch.js",
                "/refapp/s/56f5e7775a5196a43038e637af04d001-CDN/986556196/1713866807582/1/75403b888ac42342a156e2c5068d7320/_/download/contextbatch/js/header.links,-_super/my-page-extension-my-page-extension-tsx.bundle.js.map;",
                "/refapp/s/6f38bf8f224ba16f27129916fcc5654a-CDN/986556196/1713866807582/1/6e95fd818582622f8c85126ec661a16f/_/download/contextbatch/js/demo-product.extend-navigation,-_super,-client-plugins.demos.watchmode-shim,-header.links,-client-plugins.demos.testpage/batch.js",
                "/refapp/s/6f38bf8f224ba16f27129916fcc5654a-CDN/986556196/1713866807582/1/6e95fd818582622f8c85126ec661a16f/_/download/contextbatch/js/demo-product.extend-navigation,-_super,-client-plugins.demos.watchmode-shim,-header.links,-client-plugins.demos.testpage/pages-without-section-register-nav-tsx.bundle.js.map;",
                "/refapp/s/6f38bf8f224ba16f27129916fcc5654a-CDN/986556196/1713866807582/1/e81eab538893f44236a142837b4d9075/_/download/contextbatch/js/reff.plugins-example-location.circular,reff.plugins-example-location,-_super,-client-plugins.demos.testpage,-client-plugins.demos.watchmode-shim,-header.links,-demo-product.extend-navigation/batch.js",
                "/refapp/s/8a0cf3eeb4ccd71c5686c84fec7d4b01-CDN/986556196/1713866807582/1/489165c468966f1731dbe36a12e74f08/_/download/contextbatch/js/reff.plugins-example-location,-_super,-client-plugins.demos.testpage,-client-plugins.demos.watchmode-shim,-header.links,-demo-product.extend-navigation/batch.js",
                "/refapp/s/94500b8c8e6a316398bb3f27fc26fdf5-CDN/986556196/1713866807582/1/75403b888ac42342a156e2c5068d7320/_/download/contextbatch/js/header.links,-_super,-client-plugins.demos.watchmode-shim,-com.atlassian.plugins.atlassian-clientside-extensions-demo:split_vendor/batch.js",
                "/refapp/s/94500b8c8e6a316398bb3f27fc26fdf5-CDN/986556196/1713866807582/1/7d5ef4543a6a2be2b4795b29699c21f1/_/download/contextbatch/js/demo.07-async-panel-with-context,-_super,-client-plugins.demos.testpage,-client-plugins.demos.watchmode-shim,-header.links,-demo-product.extend-navigation/batch.js",
                "/refapp/s/94500b8c8e6a316398bb3f27fc26fdf5-CDN/986556196/1713866807582/1/99d8e309f7f3081d606af502048cdfc5/_/download/contextbatch/js/client-plugins.demos.watchmode-shim,-_super/batch.js",
                "/refapp/s/94500b8c8e6a316398bb3f27fc26fdf5-CDN/986556196/1713866807582/1/99d8e309f7f3081d606af502048cdfc5/_/download/contextbatch/js/client-plugins.demos.watchmode-shim,-_super/watchmode-shim.bundle.js.map;",
                "/refapp/s/94500b8c8e6a316398bb3f27fc26fdf5-CDN/986556196/1713866807582/1/bd45bd394a562f2be83972649a84bbfb/_/download/contextbatch/js/index.links,-_super,-header-links-handler/batch.js",
                "/refapp/s/94500b8c8e6a316398bb3f27fc26fdf5-CDN/986556196/1713866807582/1/d6fb74fe67c82f1bd7ac29881f8dcf71/_/download/contextbatch/js/demo.01-basic,-_super,-client-plugins.demos.testpage,-client-plugins.demos.watchmode-shim,-header.links,-demo-product.extend-navigation/01-basic-my-second-plugin-tsx.bundle.js.map;",
                "/refapp/s/94500b8c8e6a316398bb3f27fc26fdf5-CDN/986556196/1713866807582/1/d6fb74fe67c82f1bd7ac29881f8dcf71/_/download/contextbatch/js/demo.01-basic,-_super,-client-plugins.demos.testpage,-client-plugins.demos.watchmode-shim,-header.links,-demo-product.extend-navigation/batch.js",
                "/refapp/s/986556196/1713866807582/1/7.0.0-m57/_/download/resources/com.atlassian.refapp.decorator:decorator-resources/images/developer16.ico",
                "/refapp/s/986556196/1713866807582/1/7.0.0-m57/_/download/resources/com.atlassian.refapp.decorator:decorator-resources/images/developer16.png",
                "/refapp/s/986556196/1713866807582/1/9.10.0/_/download/resources/com.atlassian.auiplugin:split_aui.splitchunk.56dfb54d0c/assets/adgs-icons.woff",
                "/refapp/s/99914b932bd37a50b983c5e7c90ae93b-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:soy/com.atlassian.auiplugin:soy.js",
                "/refapp/s/a360de14e884740c51d8b22e5ed8fb88-CDN/986556196/1713866807582/1/aa0ece7742dbb78cce393af9d0468924/_/download/contextbatch/js/_super,-com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-perf-observer/batch.js",
                "/refapp/s/a360de14e884740c51d8b22e5ed8fb88-CDN/986556196/1713866807582/1/aa0ece7742dbb78cce393af9d0468924/_/download/contextbatch/js/_super,-com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-perf-observer/index.js.map",
                "/refapp/s/a782266085f232a610371ee12a0da5ca-CDN/986556196/1713866807582/1/32361940f8dce1785d1475bb8fe607da/_/download/contextbatch/js/atl.general,-_super,-com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async,-com.atlassian.refapp.decorator:decorator-resources/batch.js",
                "/refapp/s/a7a6658233ca4c72f832d04e49df0277-CDN/986556196/1713866807582/1/489165c468966f1731dbe36a12e74f08/_/download/contextbatch/js/reff.plugins-example-location,-_super,-client-plugins.demos.testpage,-client-plugins.demos.watchmode-shim,-header.links,-demo-product.extend-navigation/batch.js",
                "/refapp/s/c7dac9c74f42fbeb0c1e258dfcc3b542-CDN/986556196/1713866807582/1/0ea481991beb81ec673af2659636db72/_/download/contextbatch/js/reff.plugins-example-location.circular,-_super,-client-plugins.demos.testpage,-client-plugins.demos.watchmode-shim,-header.links,-demo-product.extend-navigation,-reff.plugins-example-location/batch.js",
                "/refapp/s/c7dac9c74f42fbeb0c1e258dfcc3b542-CDN/986556196/1713866807582/1/2100e1d87036cde3b9797b565d34fb5d/_/download/contextbatch/js/header-links-handler,-_super/batch.js",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/1a60219f3b3aa861654e013d47504057/_/download/contextbatch/css/header.links,client-plugins.demos.testpage,-_super,-atl.general,-client-plugins.demos.watchmode-shim/batch.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/2c2e7b57a1e38de3e65b807e1b61af5e/_/download/contextbatch/css/com.atlassian.analytics.analytics-client:policy-update,-_super/batch.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/3.0.0-m06-SNAPSHOT/_/download/batch/com.atlassian.plugins.atlassian-clientside-extensions-page-bootstrapper:page-bootstrapper/com.atlassian.plugins.atlassian-clientside-extensions-page-bootstrapper:page-bootstrapper.js",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/32361940f8dce1785d1475bb8fe607da/_/download/contextbatch/css/atl.general,-_super,-com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async,-com.atlassian.refapp.decorator:decorator-resources,-com.atlassian.plugins.atlassian-clientside-extensions-page-bootstrapper:client-resources/batch.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/32361940f8dce1785d1475bb8fe607da/_/download/contextbatch/css/atl.general,-_super,-com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async,-com.atlassian.refapp.decorator:decorator-resources/batch.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/32361940f8dce1785d1475bb8fe607da/_/download/contextbatch/css/atl.general,-_super/batch.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/7.0.0-m57/_/download/batch/com.atlassian.refapp.decorator:decorator-resources/com.atlassian.refapp.decorator:decorator-resources.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/7.0.0/_/download/batch/com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async/com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async.js",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/7.0.0/_/download/batch/com.atlassian.soy.soy-template-plugin:soy-deps/com.atlassian.soy.soy-template-plugin:soy-deps.js",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/7f930069d22c1a77ea04523908431a4d/_/download/contextbatch/css/atl.admin,-_super,-com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async,-com.atlassian.refapp.decorator:decorator-resources/batch.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.component.async-header/com.atlassian.auiplugin:split_aui.component.async-header.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.16f099a0da/com.atlassian.auiplugin:split_aui.splitchunk.16f099a0da.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.3d2cb1af14/com.atlassian.auiplugin:split_aui.splitchunk.3d2cb1af14.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.57d1ac075f/com.atlassian.auiplugin:split_aui.splitchunk.57d1ac075f.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.5b8c290363/com.atlassian.auiplugin:split_aui.splitchunk.5b8c290363.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.b548966f06/com.atlassian.auiplugin:split_aui.splitchunk.b548966f06.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.b652d2668a/com.atlassian.auiplugin:split_aui.splitchunk.b652d2668a.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/9.10.0/_/download/batch/com.atlassian.auiplugin:split_aui.splitchunk.f5244b0828/com.atlassian.auiplugin:split_aui.splitchunk.f5244b0828.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/aa0ece7742dbb78cce393af9d0468924/_/download/contextbatch/css/_super,-com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-perf-observer/batch.css",
                "/refapp/s/d41d8cd98f00b204e9800998ecf8427e-CDN/986556196/1713866807582/1/d7f81f4a14cfa5f755d9a007d5151104/_/download/contextbatch/js/reff.old-web-items-location,-_super/batch.js",
                "/refapp/s/db0e01ea0b3d8f5192d4bc3dc6dce8dd-CDN/986556196/1713866807582/1/0a75b9583fd3156f53e61cae3e20d02d/_/download/contextbatch/js/reff.plugins-example-location,reff.plugins-example-location.circular,-_super,-client-plugins.demos.testpage,-client-plugins.demos.watchmode-shim,-header.links,-demo-product.extend-navigation/batch.js",
                "/refapp/s/db0e01ea0b3d8f5192d4bc3dc6dce8dd-CDN/986556196/1713866807582/1/1a60219f3b3aa861654e013d47504057/_/download/contextbatch/js/header.links,client-plugins.demos.testpage,-_super,-atl.general,-client-plugins.demos.watchmode-shim/batch.js",
                "/refapp/s/db0e01ea0b3d8f5192d4bc3dc6dce8dd-CDN/986556196/1713866807582/1/1a60219f3b3aa861654e013d47504057/_/download/contextbatch/js/header.links,client-plugins.demos.testpage,-_super,-atl.general,-client-plugins.demos.watchmode-shim/testpage.bundle.js.map;");

        if (false) {
            // This is currently broken :(
            // https://hello.atlassian.net/wiki/spaces/~712020c753f12dec1648188b59d3cd1e9acdea/blog/2024/03/22/3534329683/We+need+to+go+deeper...#Batched-downloads-of-resources-with-%2F's-in-their-names
            paths = Stream.concat(
                    paths,
                    Stream.of(
                            "/refapp/s/0bbd4ab414992c3287fd84a397c5fbfa-CDN/986556196/1713866807582/1/8b3f202bad3006c75c2784d78d7e4572/_/download/contextbatch/js/system.admin/general,-_super,-header.links/batch.js"));
        }

        return paths.map(RouterPathRoutingTest::getExpectedPathParameters).toArray();
    }
    ;

    static final Pattern contextBatchJsPattern = Pattern.compile(".*/contextbatch/(js)/(.*)/(batch.js)");
    static final Pattern contextBatchMapPattern = Pattern.compile(".*/contextbatch/(js)/(.*)/(.*).map");
    static final Pattern contextBatchMapPatternStrange = Pattern.compile(".*/contextbatch/(js)/(.*)/(.*)");
    static final Pattern batchPattern = Pattern.compile(".*/batch/(.*)/(.*)");
    static final Pattern resourcesPattern = Pattern.compile(".*/resources/([^/]*)/(.*)");
    static final Pattern contextBatchCssPattern = Pattern.compile(".*/contextbatch/(css)/(.*)/(batch.css)");
    static final List<Pattern> patterns = List.of(
            contextBatchJsPattern,
            contextBatchMapPattern,
            contextBatchMapPatternStrange,
            batchPattern,
            resourcesPattern,
            contextBatchCssPattern);

    private static Object[] getExpectedPathParameters(String path) {
        for (Pattern pattern : patterns) {
            Matcher matcher = pattern.matcher(path);
            if (matcher.matches()) {
                if (matcher.groupCount() == 3) {
                    return new Object[] {path, new String[] {matcher.group(1), matcher.group(2), matcher.group(3)}};
                } else if (matcher.groupCount() == 2) {
                    return new Object[] {path, new String[] {matcher.group(1), matcher.group(2)}};
                } else {
                    throw new IllegalArgumentException("Unexpected match: " + path + " -> " + matcher);
                }
            }
        }

        throw new IllegalArgumentException("Unexpected path: " + path);
    }

    @Test
    @Parameters(method = "getCSEItPaths")
    @TestCaseName("canDispatch {0} -> {1}")
    public void canDispatchCSEPaths(String path, String[] expected_args) {
        assertThat(router.canDispatch(path), is(true));
    }

    @Test
    @Parameters(method = "getCSEItPaths")
    @TestCaseName("correctlyDispatchesCSERequests {0} -> {1}")
    public void correctlyDispatchesCSERequests(String path, String[] expected_args) {
        Request request = new Request(globals, path, Map.of());
        Response response = new Response(request, mock(HttpServletResponse.class));

        router.dispatch(request, response);
        if (!Arrays.equals(router.last_args, expected_args)) {
            router.dispatch(request, response); // Your second chance to debug!
        }

        assertThat(router.last_args, equalTo(expected_args));
    }
}
