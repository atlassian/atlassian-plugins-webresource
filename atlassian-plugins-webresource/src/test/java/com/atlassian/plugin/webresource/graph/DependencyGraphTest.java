package com.atlassian.plugin.webresource.graph;

import java.util.Collection;

import org.junit.Test;
import com.fasterxml.jackson.core.type.TypeReference;

import com.atlassian.plugin.webresource.mocks.WebResourceModuleDescriptorMock;
import com.atlassian.plugin.webresource.models.Requestable;
import com.atlassian.plugin.webresource.models.WebResourceContextKey;
import com.atlassian.plugin.webresource.models.WebResourceKey;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static com.atlassian.util.JSONUtil.fileToEntity;

public class DependencyGraphTest {
    private static final String FEATURE_PATTERN_NAME = "feature_%s.js";
    private static final String FEATURE_DEPENDENCY_PATTERN_NAME = "feature_%s.%s.js";
    private static final int ROOT_NODE_INDEX = 0;
    private static final String TEST_CASE_FOLDER = "/test-cases/graph/%s";

    @Test
    public void when_requestingSomethingNotInGraph_Then_hasDependencyReturnsFalse() {
        // given
        final DependencyGraph<Requestable> originalGraph = buildArtificialGraph(10, 10);
        // when
        final String requestableKey = format(FEATURE_PATTERN_NAME, "abcd");
        final boolean result = originalGraph.hasDependency(new WebResourceKey(requestableKey));
        // then
        assertFalse(result);
    }

    @Test
    public void when_ThereIsBigDependencyGraph_And_SourceRequestableKeyIsRoot_Then_ReturnEntireDependencyGraph() {
        // given
        final DependencyGraph<Requestable> originalGraph = buildArtificialGraph(1000, 1000);
        // when
        final String rootRequestableKey = format(FEATURE_PATTERN_NAME, ROOT_NODE_INDEX);
        final DependencyGraph<Requestable> actualGraph =
                originalGraph.findDependencySubGraphByRequestableKey(new WebResourceKey(rootRequestableKey));
        // then
        assertEquals(originalGraph, actualGraph);
    }

    @Test
    public void when_ThereIsMatchingDependantsForSourceRequestableKey_Then_ReturnDependantsSubGraph() {
        // given
        final DependencyGraph<Requestable> fullResourceDependencyGraph =
                buildGraphFromFile("when-there-is-matching-for-source-vertex/full-resource-dependency-graph.json");
        // when
        final DependencyGraph<Requestable> actualResourceDependantSubGraph =
                fullResourceDependencyGraph.findDependantsSubGraphByKey(
                        new WebResourceKey("atlassian-plugins-webresource-parent.refapp-integration-test:r6"));
        // then
        final DependencyGraph<Requestable> expectedResourceDependantSubGraph = buildGraphFromFile(
                "when-there-is-matching-for-source-vertex/expected-resource-dependant-sub-graph.json");
        assertEquals(expectedResourceDependantSubGraph, actualResourceDependantSubGraph);
    }

    @Test
    public void when_ThereIsMatchingDependantsForSourceRequestableKeyWithMultipleEdges_Then_ReturnDependantsSubGraph() {
        // given
        final DependencyGraph<Requestable> fullResourceDependencyGraph = buildGraphFromFile(
                "when-there-is-matching-for-source-vertex-with-mult-edges/full-resource-dependency-graph.json");
        // when
        final DependencyGraph<Requestable> actualResourceDependantSubGraph =
                fullResourceDependencyGraph.findDependantsSubGraphByKey(
                        new WebResourceKey("atlassian-plugins-webresource-parent.refapp-integration-test:r4"));
        // then
        final DependencyGraph<Requestable> expectedResourceDependantSubGraph = buildGraphFromFile(
                "when-there-is-matching-for-source-vertex-with-mult-edges/expected-resource-dependant-sub-graph.json");
        assertEquals(expectedResourceDependantSubGraph, actualResourceDependantSubGraph);
    }

    @Test
    public void when_ThereIsCycleDependencyInGraph_Then_ReturnCyclicSubGraph() {
        // given
        final DependencyGraph<Requestable> fullResourceDependencyGraph =
                buildGraphFromFile("when-there-is-cyclic-dependency/full-resource-dependency-graph.json");
        // when
        final DependencyGraph<Requestable> actualCyclicSubGraph =
                fullResourceDependencyGraph.findCyclicSubGraphByVertex(
                        new WebResourceKey("atlassian-plugins-webresource-parent.refapp-integration-test:r1"));
        // then
        final DependencyGraph<Requestable> expectedCyclicSubGraph =
                buildGraphFromFile("when-there-is-cyclic-dependency/expected-resource-dependant-sub-graph.json");
        assertEquals(expectedCyclicSubGraph, actualCyclicSubGraph);
    }

    @Test
    public void when_ThereIsIntersectionBetweenTwoSubGraphs_Then_ReturnIntersectionGraph() {
        // given
        final DependencyGraph<Requestable> fullResourceDependencyGraph = buildGraphFromFile(
                "when-there-is-intersection-between-two-sub-graphs/full-resource-dependency-graph.json");
        // when
        final DependencyGraph<Requestable> actualIntersectionSubGraph =
                fullResourceDependencyGraph.findIntersectionSubGraph(
                        new WebResourceKey("atlassian-plugins-webresource-parent.refapp-integration-test:r6"),
                        new WebResourceKey("atlassian-plugins-webresource-parent.refapp-integration-test:r4"));
        // then
        final DependencyGraph<Requestable> expectedIntersectionSubGraph = buildGraphFromFile(
                "when-there-is-intersection-between-two-sub-graphs/expected-resource-dependant-sub-graph.json");
        assertEquals(expectedIntersectionSubGraph, actualIntersectionSubGraph);
    }

    @Test
    public void given_ThereAreMutlipleWebResourcesInAContext_Then_hasDependencyReturnsTrueForFeatureContext() {
        // given
        final DependencyGraph<Requestable> fullResourceDependencyGraph =
                buildGraphFromFile("given-multiple-web-resources-in-a-context/full-resource-dependency-graph.json");
        // when
        final boolean result = fullResourceDependencyGraph.hasDependency(new WebResourceContextKey("features"));
        // then
        assertTrue(result);
    }

    @Test
    public void
            given_ThereAreMutlipleWebResourcesInAContext_when_FeatureContextRequested_Then_ReturnGraphForAllFeatures() {
        // given
        final DependencyGraph<Requestable> fullResourceDependencyGraph =
                buildGraphFromFile("given-multiple-web-resources-in-a-context/full-resource-dependency-graph.json");
        // when
        final DependencyGraph<Requestable> actualResultSubGraph =
                fullResourceDependencyGraph.findDependencySubGraphByRequestableKey(
                        new WebResourceContextKey("features"));
        // then
        final DependencyGraph<Requestable> expectedResultSubGraph =
                buildGraphFromFile("given-multiple-web-resources-in-a-context/expected-feature-context-graph.json");
        assertEquals(expectedResultSubGraph, actualResultSubGraph);
    }

    @Test
    public void
            given_ThereAreMutlipleWebResourcesInAContext_when_NorthContextRequested_Then_ReturnGraphForNorthernWebResources() {
        // given
        final DependencyGraph<Requestable> fullResourceDependencyGraph =
                buildGraphFromFile("given-multiple-web-resources-in-a-context/full-resource-dependency-graph.json");
        // when
        final DependencyGraph<Requestable> actualResultSubGraph =
                fullResourceDependencyGraph.findDependencySubGraphByRequestableKey(new WebResourceContextKey("north"));
        // then
        final DependencyGraph<Requestable> expectedResultSubGraph =
                buildGraphFromFile("given-multiple-web-resources-in-a-context/expected-north-context-graph.json");
        assertEquals(expectedResultSubGraph, actualResultSubGraph);
    }

    /**
     * Build a certain graph with random values based on a max height and max width.
     *
     * @param maxHeight The graph max height.
     * @param maxWidth  The graph max width.
     * @return The created dependency graph.
     */
    private DependencyGraph<Requestable> buildArtificialGraph(final int maxHeight, final int maxWidth) {
        final DependencyGraphBuilder builder = DependencyGraph.builder();

        for (int width = 0; width < maxWidth; width++) {
            // Linking the previous vertex with the current one.
            final String sourceVertex = format(FEATURE_PATTERN_NAME, width);
            final String targetVertex = format(FEATURE_PATTERN_NAME, width + 1);
            builder.addWebResourceDependency(new WebResourceKey(sourceVertex), targetVertex);

            // Creating the height of the graph based on a origin vertex.
            for (int height = 0; height < maxHeight; height++) {
                final String sourceDependencyVertex = format(FEATURE_DEPENDENCY_PATTERN_NAME, width, height);
                final String targetDependencyVertex = format(FEATURE_DEPENDENCY_PATTERN_NAME, width, height + 1);
                builder.addWebResourceDependency(new WebResourceKey(sourceDependencyVertex), targetDependencyVertex);
            }

            // Linking all the base vertexes with the root vertex.
            final String sourceDependencyVertex = format(FEATURE_PATTERN_NAME, width + "." + ROOT_NODE_INDEX);
            builder.addWebResourceDependency(new WebResourceKey(sourceVertex), sourceDependencyVertex);
        }
        return builder.build();
    }

    /**
     * Build the dependency relation between two resources based on a configuration file.
     *
     * @param filePath                      The file path used to build the entire graph.
     * @return The built resource dependency graph.
     */
    private DependencyGraph<Requestable> buildGraphFromFile(final String filePath) {
        final DependencyGraphBuilder builder = DependencyGraph.builder();
        for (final WebResourceModuleDescriptorMock resource :
                fileToEntity(format(TEST_CASE_FOLDER, filePath), new ResourceCollectionTypeReference())) {
            builder.addDependencies(resource.toWebResourceModuleDescriptor());
        }
        return builder.build();
    }

    /**
     * Represents a resource dependency collection type used to extract information from a JSON file.
     */
    private static class ResourceCollectionTypeReference
            extends TypeReference<Collection<WebResourceModuleDescriptorMock>> {}
}
