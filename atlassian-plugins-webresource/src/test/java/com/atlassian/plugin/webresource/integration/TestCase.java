package com.atlassian.plugin.webresource.integration;

import org.junit.Before;

import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;

public abstract class TestCase {
    protected WebResource wr;

    @Before
    public void before() throws Exception {
        wr = new WebResourceImpl();
    }
}
