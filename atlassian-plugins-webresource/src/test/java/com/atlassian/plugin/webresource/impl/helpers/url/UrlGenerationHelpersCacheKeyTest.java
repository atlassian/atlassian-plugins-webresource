package com.atlassian.plugin.webresource.impl.helpers.url;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.support.UrlCache;
import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.fixtures.DeepConditionalTree;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

import static com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers.buildIncludedExcludedConditionsAndBatchingOptions;

public class UrlGenerationHelpersCacheKeyTest extends TestCase {

    private RequestCache requestCache;
    private UrlBuildingStrategy urlBuildingStrategy;

    @Before
    public void setUp() throws Exception {
        DeepConditionalTree.configure(wr);
        requestCache = new RequestCache(wr.getGlobals());
        urlBuildingStrategy = UrlBuildingStrategy.normal();
    }

    @Test
    public void given_allConditionsPass_when_includingFeatureA_then_capturesFeatureA() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache, urlBuildingStrategy, asList("plugin.key:feature-a"), emptyList());

        assertThat(results.getIncluded(), contains("plugin.key:feature-a"));
    }

    @Test
    public void given_allConditionsPass_when_includingFeatureABeforeB_then_capturesFeatureAThenB() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("plugin.key:feature-a", "plugin.key:feature-b"),
                        emptyList());

        assertThat(results.getIncluded(), contains("plugin.key:feature-a", "plugin.key:feature-b"));
    }

    /**
     * The inclusion order is, unfortunately, significant. This affects code execution order, which developers likely
     * unintentionally rely upon.
     * I'd love to replace this with explicit priority orders via OrderedSet instead of List for inclusions,
     * so that the only guarantee of load order was based on natural sort of priority values,
     * but that kind of behavioural change would require a major version.
     */
    @Test
    @Ignore("Uncomment when implementing PLUGWEB-636")
    public void given_allConditionsPass_when_includingFeatureAAfterB_then_capturesFeatureBThenA() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("plugin.key:feature-b", "plugin.key:feature-a"),
                        emptyList());

        assertThat(results.getIncluded(), contains("plugin.key:feature-b", "plugin.key:feature-a"));
    }

    @Test
    @Ignore("Uncomment when implementing PLUGWEB-636")
    public void given_allConditionsPass_when_includingFeatureABeforeItsSubtrees_then_reducesToFeatureA() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("plugin.key:feature-a", "plugin.key:sub-a", "plugin.key:deep-a"),
                        emptyList());

        assertThat(results.getIncluded(), contains("plugin.key:feature-a"));
    }

    @Test
    @Ignore("Uncomment when implementing PLUGWEB-636. The inclusion order is, unfortunately, significant..."
            + " This scenario cannot be served in request order since sub-a -> deep-a."
            + " The deep-a inclusion could be removed safely, but undecided about whether to remove sub-a, too.")
    public void given_allConditionsPass_when_includingSubAThenDeepAThenFeatureA_then_undecidedBehaviour() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("plugin.key:sub-a", "plugin.key:deep-a", "plugin.key:feature-a"),
                        emptyList());

        assertThat(results.getIncluded(), contains("plugin.key:sub-a", "plugin.key:feature-a"));
        assertThat(results.getIncluded(), contains("plugin.key:feature-a"));
    }

    @Test
    @Ignore("Uncomment when implementing PLUGWEB-636. The inclusion order is, unfortunately, significant..."
            + " Note, however, this specific scenario yields the correct topological sort order."
            + " Decide how far to collapse.")
    public void given_allConditionsPass_when_includingFeatureAAfterItsSubtrees_then_undecidedBehaviour() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("plugin.key:deep-a", "plugin.key:sub-a", "plugin.key:feature-a"),
                        emptyList());

        assertThat(results.getIncluded(), contains("plugin.key:deep-a", "plugin.key:sub-a", "plugin.key:feature-a"));
        assertThat(results.getIncluded(), contains("plugin.key:feature-a"));
    }

    @Test
    @Ignore("Uncomment when implementing PLUGWEB-636")
    public void given_conditionAFails_when_includingFeatureABeforeItsSubtrees_then_doesNotReduceSubA() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("plugin.key:feature-a", "plugin.key:sub-a", "plugin.key:deep-a"),
                        emptyList());

        assertThat(results.getIncluded(), contains("plugin.key:feature-a", "plugin.key:sub-a"));
    }

    @Test
    @Ignore("Uncomment when implementing PLUGWEB-636")
    public void given_allConditionsPass_when_includingFeatureC_then_identifiesSubAIsSubtreeOfFeaturesAC() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("plugin.key:feature-c", "plugin.key:feature-a"),
                        emptyList());

        assertThat(results.getIncluded(), contains("plugin.key:feature-c"));
    }

    @Test
    public void given_allConditionsPass_when_includingFeatureC_and_excludingFeatureA_then_shouldCaptureBoth() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("plugin.key:feature-c"),
                        asList("plugin.key:feature-a"));

        assertThat(results.getIncluded(), contains("plugin.key:feature-c"));
        assertThat(results.getExcluded(), contains("plugin.key:feature-a"));
    }

    @Test
    public void
            given_allConditionsPass_when_includingFeatureC_and_excludingFeatureAAndSubtrees_then_shouldReduceExclusionToFeatureA() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("plugin.key:feature-c"),
                        asList("plugin.key:feature-a", "plugin.key:sub-a", "plugin.key:deep-a"));

        assertThat(results.getIncluded(), contains("plugin.key:feature-c"));
        assertThat(results.getExcluded(), contains("plugin.key:feature-a"));
    }

    @Test
    public void
            given_allConditionsPass_when_includingSuperbatch_and_excludingFeatureAAndSubtrees_then_shouldReduceExclusionToFeatureA() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("_context:_super"),
                        asList("plugin.key:feature-a", "plugin.key:sub-a", "plugin.key:deep-a"));

        assertThat(results.getIncluded(), contains("_context:_super"));
        assertThat(results.getExcluded(), contains("plugin.key:feature-a"));
    }

    @Test
    public void
            given_allConditionsPass_when_includingSuperbatch_and_excludingFeaturesCAndAAndSubtrees_then_shouldReduceExclusionToFeatureC() {
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("_context:_super"),
                        asList(
                                "plugin.key:feature-c",
                                "plugin.key:feature-a",
                                "plugin.key:sub-a",
                                "plugin.key:deep-a"));

        assertThat(results.getIncluded(), contains("_context:_super"));
        assertThat(results.getExcluded(), contains("plugin.key:feature-c"));
    }

    @Test
    public void
            given_conditionCFails_when_includingSuperbatch_and_excludingFeaturesCAndAAndSubtrees_then_shouldReduceExclusionsToFeatureCAndA() {
        DeepConditionalTree.setConditionFeatureC(false);
        final UrlCache.IncludedExcludedConditionsAndBatchingOptions results =
                buildIncludedExcludedConditionsAndBatchingOptions(
                        requestCache,
                        urlBuildingStrategy,
                        asList("_context:_super"),
                        asList(
                                "plugin.key:feature-c",
                                "plugin.key:feature-a",
                                "plugin.key:sub-a",
                                "plugin.key:deep-a"));

        assertThat(results.getIncluded(), contains("_context:_super"));
        assertThat(results.getExcluded(), contains("plugin.key:feature-c"));
    }
}
