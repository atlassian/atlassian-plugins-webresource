package com.atlassian.plugin.webresource.assembler;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import com.google.common.collect.ImmutableMap;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.assembler.resource.PluginCssResourceParams;
import com.atlassian.webresource.api.assembler.resource.PluginJsResourceParams;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;

import static org.junit.Assert.assertEquals;

public class TestPluginUrlResourceParams {
    @Test
    public void testJs() {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("charset", "UTF-16");
        paramsMap.put("title", "4shizzle");

        PluginJsResourceParams params =
                new DefaultPluginJsResourceParams(paramsMap, "com.atlassian.wrm:test-resource", BatchType.RESOURCE);

        assertEquals(
                ImmutableMap.builder()
                        .put("charset", "UTF-16")
                        .put("title", "4shizzle")
                        .put(Config.WRM_KEY_PARAM_NAME, "com.atlassian.wrm:test-resource")
                        .put(Config.WRM_BATCH_TYPE_PARAM_NAME, "resource")
                        .build(),
                params.all());
    }

    @Test
    public void testCss() {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("media", "print");
        paramsMap.put("charset", "UTF-16");
        paramsMap.put("title", "4shizzle");

        PluginCssResourceParams params =
                new DefaultPluginCssResourceParams(paramsMap, "com.atlassian.wrm:test-resource", BatchType.RESOURCE);

        assertEquals("print", params.media());
        assertEquals(
                ImmutableMap.builder()
                        .put("charset", "UTF-16")
                        .put("media", "print")
                        .put("title", "4shizzle")
                        .put(Config.WRM_KEY_PARAM_NAME, "com.atlassian.wrm:test-resource")
                        .put(Config.WRM_BATCH_TYPE_PARAM_NAME, "resource")
                        .build(),
                params.all());
    }
}
