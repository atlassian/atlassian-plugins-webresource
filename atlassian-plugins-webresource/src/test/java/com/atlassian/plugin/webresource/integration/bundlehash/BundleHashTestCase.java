package com.atlassian.plugin.webresource.integration.bundlehash;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Rule;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import com.atlassian.plugin.webresource.impl.http.Controller;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.integration.stub.ResponseData;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singletonMap;

import static com.atlassian.plugin.webresource.impl.config.Config.ENABLE_BUNDLE_HASH_VALIDATION;
import static com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers.calculateBundlesHash;

public class BundleHashTestCase {
    static final String ATLASSIAN_CACHES_ENABLED_PROPERTY = "atlassian.disable.caches";
    static final String STATIC_HASH_PARAM = "_statichash";

    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    protected WebResource wr;

    @Before
    public void before() throws Exception {
        System.setProperty(ENABLE_BUNDLE_HASH_VALIDATION, Boolean.TRUE.toString());
        System.setProperty(ATLASSIAN_CACHES_ENABLED_PROPERTY, Boolean.FALSE.toString());
        wr = new WebResourceImpl();

        setUp();
    }

    protected void setUp() throws Exception {}

    protected String getBundleHashForRequestables(final String... items) {
        final List<Bundle> bundles = wr.getGlobals().getSnapshot().toBundles(newArrayList(items));
        return calculateBundlesHash(bundles);
    }

    protected ResponseData getWithStaticHash(final String url, final String statichash) {
        return wr.get(url, singletonMap(STATIC_HASH_PARAM, statichash));
    }

    protected ResponseData getWithOwnStaticHash(final String url) {
        return getWithStaticHash(url, getStaticHashFromUrl(url));
    }

    /**
     * this would be better done in a helper elsewhere.
     * See {@link com.atlassian.plugin.webresource.filter.rewrite.ResourceDownloadRewriteRule#NON_WEB_INF_RESOURCES_URI_PATTERN}
     * for the original regex.
     * See {@link Controller#isBundleHashValid} for where the value gets sliced up.
     */
    private String getStaticHashFromUrl(String url) {
        final Matcher matcher = Pattern.compile("^/s/(.*)/_/").matcher(url);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }
}
