package com.atlassian.plugin.webresource.integration.fixtures;

import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.spi.condition.AbstractBooleanUrlReadingCondition;

/**
 * <p>
 * Prepares five web-resources: `feature-a`, `feature-b`, `feature-s`, `shared-lib`, and `deep-lib`.
 * <p>
 * Their relationships are as follows:
 * </p>
 * <ul>
 *     <li>Feature A is in the `foo` context</li>
 *     <li>Feature B is in the `bar` context</li>
 *     <li>Features A and B depend on `shared-lib`</li>
 *     <li>`shared-lib` depends on `deep-lib`</li>
 *     <li>Feature A is conditionally enabled.</li>
 * </ul>
 * <p>
 *     This results in a scenario where `shared-lib` will sometimes be needed in
 *     the `foo` context (via `feature-a`), but always needed in the `bar` context.
 * </p>
 * <p>
 *     When sub-batch generation or other ordering requirements dictate that `foo`
 *     should load before `bar`, we have a scenario where `shared-lib` can jump
 *     between two context batches.
 * </p>
 * <p>
 *     Note: Feature S is added to both `foo` and `bar` contexts
 *     to ensure there's at least one unconditional overlap, which should
 *     guarantee the second URL generated has an explicit exclusion in it.
 * </p>
 *
 * @see <a href="https://ecosystem.atlassian.net/browse/PLUGWEB-624">PLUGWEB-624</a>
 * @see <a href="https://ecosystem.atlassian.net/browse/PLUGWEB-648">PLUGWEB-648</a>
 */
public class ConditionalDiamond {
    private static boolean featureAEnabled;

    private ConditionalDiamond() {}

    public static void configure(final WebResource wr) {
        configure(wr, null);
    }

    public static void configure(final WebResource wr, final WebResource.ConfigCallback cb) {
        wr.configure(cb)
                .plugin("plugin.key")
                .webResource("feature-a")
                .context("foo")
                .condition(FeatureACondition.class)
                .dependency(":shared-lib")
                .resource("feature-a.js")
                .webResource("feature-b")
                .context("bar")
                .dependency(":shared-lib")
                .resource("feature-b.js")
                .webResource("feature-s")
                .context("foo")
                .context("bar")
                .resource("feature-s.js")
                .webResource("shared-lib")
                .dependency(":deep-lib")
                .webResource("deep-lib")
                .resource("lib.js")
                .end();
    }

    public static void setConditionFeatureA(boolean val) {
        featureAEnabled = val;
    }

    public static class FeatureACondition extends AbstractBooleanUrlReadingCondition {
        @Override
        public boolean shouldDisplay(QueryParams params) {
            return featureAEnabled;
        }

        @Override
        protected boolean isConditionTrue() {
            return featureAEnabled;
        }

        @Override
        protected String queryKey() {
            return "feature-a";
        }
    }
}
