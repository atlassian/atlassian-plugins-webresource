package com.atlassian.plugin.webresource;

import javax.annotation.Nonnull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.webresource.impl.helpers.url.UrlGenerationHelpers.calculateBundlesHash;

@RunWith(MockitoJUnitRunner.class)
public class TestBundleHashGeneration {

    @Test
    public void accountsForBundleVersion() {
        Bundle a1 = bundle("aaa", "1");
        Bundle a2 = bundle("aaa", "2");

        final String hashA1 = calculateBundlesHash(newArrayList(a1));
        final String hashA2 = calculateBundlesHash(newArrayList(a2));

        assertThat(hashA1, not(equalTo(hashA2)));
    }

    @Test
    public void bundleOrderIsImportant() {
        Bundle a = bundle("aaa", "1");
        Bundle b = bundle("bbb", "123");
        Bundle c = bundle("ccc", "1");

        final String forwards = calculateBundlesHash(newArrayList(a, b, c));
        final String backwards = calculateBundlesHash(newArrayList(c, b, a));

        assertThat(
                "Hash should change if content order would also change (e.g., dependencies changed)",
                forwards,
                not(equalTo(backwards)));
    }

    private Bundle bundle(@Nonnull final String name, @Nonnull final String version) {
        Bundle b = mock(Bundle.class);
        when(b.getKey()).thenReturn(name);
        when(b.getVersion()).thenReturn(version);
        return b;
    }
}
