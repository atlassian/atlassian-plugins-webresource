package com.atlassian.plugin.webresource.impl.helpers;

import java.util.Map;

import org.junit.Test;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.transformers.AddLocation;
import com.atlassian.plugin.webresource.integration.transformers.AddLocationDimensionAware;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerFactory;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.buildKey;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.getResource;
import static com.atlassian.plugin.webresource.impl.support.http.BaseRouter.buildUrl;

public class TestResourceCaching extends TestCase {

    @Test
    public void shouldBuildDistinctCacheKeyForResources() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js", "first", "first/aaa.js")
                .resource("a2.js", "second", "second/aaa.js")
                .resource("b.js")
                .resource("like.png", "like!", "images/like.png")
                .end();

        RequestCache requestCache = new RequestCache(wr.getGlobals());
        Map<String, String> params = buildMap();

        Resource a1 = getResource(requestCache, "plugin:a", "a1.js");
        Resource a2 = getResource(requestCache, "plugin:a", "a2.js");
        Resource b = getResource(requestCache, "plugin:a", "b.js");
        Resource like = getResource(requestCache, "plugin:a", "like.png");

        assertThat(buildKey(wr.getGlobals(), a1, params), equalTo("plugin:a:first/aaa.js"));
        assertThat(buildKey(wr.getGlobals(), a2, params), equalTo("plugin:a:second/aaa.js"));
        assertThat(buildKey(wr.getGlobals(), b, params), equalTo("plugin:a:b.js"));
        assertThat(buildKey(wr.getGlobals(), like, params), equalTo("plugin:a:images/like.png"));
    }

    @Test
    public void shouldBuildDistinctCacheKeyForIndirectlyReferencedResources() {
        wr.configure()
                .plugin("plugin")
                .webResource("a")
                .resource("style.css", "content of style")
                .resource("assets/icons/", null, "assets/icons")
                .withRelativeFile("like.png")
                .withRelativeFile("star.png")
                .end();

        RequestCache requestCache = new RequestCache(wr.getGlobals());
        Map<String, String> params = buildMap();

        Resource like = getResource(requestCache, "plugin:a", "assets/icons/like.png");
        Resource star = getResource(requestCache, "plugin:a", "assets/icons/star.png");
        Resource nonexistent = getResource(requestCache, "plugin:a", "assets/icons/nonexistent.png");

        assertThat(buildKey(wr.getGlobals(), like, params), equalTo("plugin:a:assets/icons/like.png"));

        assertThat(buildKey(wr.getGlobals(), star, params), equalTo("plugin:a:assets/icons/star.png"));

        assertThat(buildKey(wr.getGlobals(), nonexistent, params), equalTo("plugin:a:assets/icons/nonexistent.png"));
    }

    @Test
    public void shouldAddAllQueryParametersIfTransformersDontProvideUsedParams() {
        wr.configure()
                .plugin("plugin")
                .transformer("AddLocation", AddLocation.class)
                .webResource("a")
                .transformation("js", "AddLocation")
                .resource("a.js")
                .end();

        RequestCache requestCache = new RequestCache(wr.getGlobals());
        Resource jsResource = getResource(requestCache, "plugin:a", "a.js");
        String fullResourceNameJs = jsResource.getKey() + ":" + jsResource.getName();

        assertThat(
                buildKey(wr.getGlobals(), jsResource, buildMap("location", "true", "some", "thing")),
                equalTo(buildUrl(fullResourceNameJs, buildMap("location", "true", "some", "thing"))));
    }

    @Test
    public void shouldBuildCacheKeyInSuchWayItIncludesOnlyParametersThatAreActuallyUsed() {
        wr.configure()
                .plugin("plugin")
                .transformer("AddLocation", AddLocationDimensionAware.class)
                .webResource("a")
                .transformation("js", "AddLocation")
                .resource("a.js")
                .resource("a.css")
                .end();

        RequestCache requestCache = new RequestCache(wr.getGlobals());
        Resource jsResource = getResource(requestCache, "plugin:a", "a.js");
        String fullResourceNameJs = jsResource.getKey() + ":" + jsResource.getName();

        // It should return parameters if used by the transformers.
        assertThat(
                buildKey(wr.getGlobals(), jsResource, buildMap("location", "true", "some", "thing")),
                equalTo(buildUrl(fullResourceNameJs, buildMap("location", "true"))));

        // It should only add those parameters if they are present in the request.
        assertThat(
                buildKey(wr.getGlobals(), jsResource, buildMap("some", "thing")),
                equalTo(buildUrl(fullResourceNameJs, buildMap())));
    }

    @Test
    public void shouldOnlyUseParametersInTransformsForThisResourceFileType() {
        wr.configure()
                .plugin("plugin")
                .transformer("AddLocation", AddLocationDimensionAware.class)
                .webResource("a")
                .transformation("js", "AddLocation")
                .resource("a.js")
                .resource("a.css")
                .end();

        RequestCache requestCache = new RequestCache(wr.getGlobals());
        Resource jsResource = getResource(requestCache, "plugin:a", "a.js");
        Resource cssResource = getResource(requestCache, "plugin:a", "a.css");
        String fullJsResourceName = jsResource.getKey() + ":" + jsResource.getName();

        // It should return only used parameters if parameters for transformer is known,
        Map<String, String> params = buildMap(
                "location", "true", "some", "thing", RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY, "false");
        assertThat(
                buildKey(wr.getGlobals(), jsResource, params),
                equalTo(buildUrl(fullJsResourceName, buildMap("location", "true"))));
        assertThat(
                "nothing transforming CSS files, so no params should affect it",
                buildKey(wr.getGlobals(), cssResource, params),
                equalTo("plugin:a:a.css"));

        // It should only add known parameters if they are present.
        params = buildMap("some", "thing");
        assertThat(buildKey(wr.getGlobals(), jsResource, params), equalTo(buildUrl(fullJsResourceName, buildMap())));
        assertThat(buildKey(wr.getGlobals(), cssResource, params), equalTo("plugin:a:a.css"));
    }
}
