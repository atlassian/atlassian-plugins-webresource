package com.atlassian.plugin.webresource.graph;

import java.util.Collection;

import org.junit.Test;
import com.fasterxml.jackson.core.type.TypeReference;

import com.atlassian.plugin.webresource.graph.DependencyGraphMock.Dependency;
import com.atlassian.plugin.webresource.models.Requestable;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

import static com.atlassian.util.JSONUtil.fileToEntity;

public class DependencyGraphBuilderTest {
    private static final String TEST_CASE_FOLDER = "/test-cases/graph-builder/%s";

    private static Collection<Dependency> buildDependenciesFromFile(final String filePath) {
        final String dependenciesFullFilePath = format(TEST_CASE_FOLDER, filePath);
        return fileToEntity(dependenciesFullFilePath, new DependencyCollectionTypeReference());
    }

    private static DependencyGraph<Requestable> buildGraphFromFile(final String filePath) {
        final String dependenciesFullFilePath = format(TEST_CASE_FOLDER, filePath);
        final DependencyGraphMock dependencyGraphMock =
                fileToEntity(dependenciesFullFilePath, DependencyGraphMock.class);
        return dependencyGraphMock.toDependencyGraph();
    }

    @Test
    public void
            when_RequestableResourceIsRootPage_And_AllContextDependenciesAreValid_And_SuperBatchDisabled_Then_ShouldReturnGraphWithAllContextsAsDependencies() {
        // given
        final Collection<Dependency> inputDependencies = buildDependenciesFromFile(
                "when-requestable-resource-is-not-root-and-contexts-valid/input-dependencies.json");

        // when
        final DependencyGraphBuilder underTest = new DependencyGraphBuilder();
        inputDependencies.stream()
                .map(Dependency::toWebResourceModuleDescriptor)
                .forEach(underTest::addDependencies);
        final DependencyGraph<Requestable> actualDependencyGraph = underTest.build();

        // then
        final DependencyGraph<Requestable> expectedDependencyGraph = buildGraphFromFile(
                "when-requestable-resource-is-root-and-contexts-valid/expected-dependency-graph.json");
        assertEquals(expectedDependencyGraph, actualDependencyGraph);
    }

    @Test
    public void
            when_RequestableResourceIsRootPage_And_AllContextDependenciesAreInvalid_And_SuperBatchDisabled_Then_ShouldReturnEmptyGraph() {
        // given
        final Collection<Dependency> inputDependencies = buildDependenciesFromFile(
                "when-requestable-resource-is-root-and-contexts-invalid/input-dependencies.json");

        // when
        final DependencyGraphBuilder underTest = new DependencyGraphBuilder();
        inputDependencies.stream()
                .map(Dependency::toWebResourceModuleDescriptor)
                .forEach(underTest::addDependencies);
        final DependencyGraph<Requestable> actualDependencyGraph = underTest.build();

        // then
        final DependencyGraph<Requestable> expectedDependencyGraph = new DependencyGraph<>(Requestable.class);
        assertEquals(expectedDependencyGraph, actualDependencyGraph);
    }

    @Test
    public void
            when_RequestableResourceIsNotRootPage_And_AllContextDependenciesAreValid_And_SuperBatchDisabled_Then_ShouldReturnEmptyGraph() {
        // given
        final Collection<Dependency> inputDependencies = buildDependenciesFromFile(
                "when-requestable-resource-is-not-root-and-contexts-valid/input-dependencies.json");

        // when
        final DependencyGraphBuilder underTest = new DependencyGraphBuilder();
        inputDependencies.stream()
                .map(Dependency::toWebResourceModuleDescriptor)
                .forEach(underTest::addDependencies);
        final DependencyGraph<Requestable> actualDependencyGraph = underTest.build();

        // then
        final DependencyGraph<Requestable> expectedDependencyGraph = new DependencyGraph<>(Requestable.class);
        assertEquals(expectedDependencyGraph, actualDependencyGraph);
    }

    @Test
    public void
            when_RequestableResourceIsRootPage_And_AllDependenciesAreValid_And_SuperBatchDisabled_Then_ShouldReturnGraphWithAllDependencies() {
        // given
        final Collection<Dependency> inputDependencies = buildDependenciesFromFile(
                "when-requestable-resource-is-root-and-dependencies-valid/input-dependencies.json");

        // when
        final DependencyGraphBuilder underTest = new DependencyGraphBuilder();
        inputDependencies.stream()
                .map(Dependency::toWebResourceModuleDescriptor)
                .forEach(underTest::addDependencies);
        final DependencyGraph<Requestable> actualDependencyGraph = underTest.build();

        // then
        final DependencyGraph<Requestable> expectedDependencyGraph = buildGraphFromFile(
                "when-requestable-resource-is-root-and-dependencies-valid/expected-dependency-graph.json");
        assertEquals(expectedDependencyGraph, actualDependencyGraph);
    }

    /**
     * Represents a requestable dependency collection type used to extract information from a JSON file.
     */
    @SuppressWarnings("JUnitTestCaseWithNoTests")
    private static class DependencyCollectionTypeReference extends TypeReference<Collection<Dependency>> {}
}
