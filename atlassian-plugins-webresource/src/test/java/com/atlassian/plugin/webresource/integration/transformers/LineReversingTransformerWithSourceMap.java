package com.atlassian.plugin.webresource.integration.transformers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.sourcemap.ReadableSourceMap;
import com.atlassian.sourcemap.Util;
import com.atlassian.sourcemap.WritableSourceMap;
import com.atlassian.sourcemap.WritableSourceMapImpl;
import com.atlassian.webresource.api.QueryParams;

import static java.util.Collections.singletonList;

import static com.atlassian.sourcemap.WritableSourceMap.toReadableSourceMap;

public class LineReversingTransformerWithSourceMap extends ContentTransformerHelper {
    @Override
    public Set<String> allUsedQueryParameters() {
        return Set.of();
    }

    public Content transform(
            final Content content, ResourceLocation resourceLocation, QueryParams params, final String sourceUrl) {
        return new ContentImpl(content.getContentType(), true) {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                ByteArrayOutputStream buff = new ByteArrayOutputStream();
                ReadableSourceMap inputSourceMap = content.writeTo(buff, isSourceMapEnabled);
                String contentAsString = buff.toString();

                final String[] lines = contentAsString.split("\n");
                ArrayUtils.reverse(lines);

                WritableSourceMap sourceMap = null;
                if (isSourceMapEnabled && (inputSourceMap != null)) {
                    sourceMap = new WritableSourceMapImpl.Builder()
                            .withSources(singletonList(sourceUrl))
                            .build();
                    int length = lines.length;
                    for (int i = length - 1; i >= 0; i--) {
                        sourceMap.addMapping(length - 1 - i, 0, i, 0, sourceUrl);
                    }
                    sourceMap = Util.rebase(toReadableSourceMap(sourceMap), inputSourceMap);
                }

                try {
                    out.write(StringUtils.join(lines, "\n").getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                return toReadableSourceMap(sourceMap);
            }
        };
    }
}
