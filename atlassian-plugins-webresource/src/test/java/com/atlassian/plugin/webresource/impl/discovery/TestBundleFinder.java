package com.atlassian.plugin.webresource.impl.discovery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.fixtures.ConditionalDiamond;

import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.is;

import static com.atlassian.plugin.webresource.impl.helpers.BaseHelpers.isConditionsSatisfied;

public class TestBundleFinder extends TestCase {
    private PredicateFailStrategy strategy;
    private Predicate<Bundle> condition;

    @Before
    public void setUp() throws Exception {
        ConditionalDiamond.configure(wr);

        final Map<String, String> params = new HashMap<>();
        final RequestCache requestCache = new RequestCache(wr.getGlobals());
        condition = isConditionsSatisfied(requestCache, params);
        strategy = PredicateFailStrategy.STOP;
    }

    @Test
    // PLUGWEB-648
    public void
            given_featureAEnabled_when_FollowingContextSubBatchesHaveCommonDependency_then_ShouldIncludeSharedLib() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:foo", "_context:bar"))
                .deepFilter(condition)
                .endAndGetResult();

        // then
        assertThat(
                found.getFound(),
                contains(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-a",
                        "plugin.key:feature-s",
                        "_context:foo",
                        "plugin.key:feature-b",
                        "_context:bar"));
        assertThat(found.getSkipped(), is(emptyCollectionOf(String.class)));
    }

    @Test
    // PLUGWEB-648
    public void
            given_featureADisabled_when_FollowingContextSubBatchesHaveCommonDependency_then_ShouldIncludeSharedLib() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:foo", "_context:bar"))
                .deepFilter(condition)
                .endAndGetResult();

        // then
        assertThat(
                found.getFound(),
                contains(
                        // feature-s does not directly depend upon the shared lib.
                        "plugin.key:feature-s",
                        // feature-a depends on shared-lib but its condition was false,
                        // so the shared lib shouldn't appear here.
                        "_context:foo",
                        // this is the first location where shared lib was discovered through
                        // a to-be-included parent tree, so this is where the shared-lib subtree
                        // should appear.
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        // feature-b depends on shared-lib and had no condition.
                        "plugin.key:feature-b",
                        "_context:bar"));
        assertThat(found.getSkipped(), allOf(hasItem("plugin.key:feature-a")));
    }

    @Test
    public void given_featureAEnabled_when_ContextSubBatchRequested_then_SharedLibIsOverlap() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);
        strategy = PredicateFailStrategy.CONTINUE;

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:bar"))
                .excluded(newHashSet("_context:foo"), condition)
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(found.getFound(), contains("plugin.key:feature-b", "_context:bar"));
        assertThat(
                getIgnoredItems(found),
                contains("plugin.key:deep-lib", "plugin.key:shared-lib", "plugin.key:feature-s"));
    }

    @Test
    public void given_featureADisabled_when_FooRequested_then_FeatureAIsSkipped_And_SharedLibIsAbsent() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:foo"))
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(found.getFound(), contains("plugin.key:feature-s", "_context:foo"));
        assertThat(found.getSkipped(), contains("plugin.key:feature-a"));
        assertThat(found.getAll(), allOf(not(hasItem("plugin.key:shared-lib")), not(hasItem("plugin.key:deep-lib"))));
    }

    @Test
    public void given_featureADisabled_and_ShouldDiscoverFullGraph_when_FooRequested_then_SharedLibIsPresent() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);
        strategy = PredicateFailStrategy.CONTINUE;

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:foo"))
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(found.getAll(), allOf(hasItem("plugin.key:shared-lib"), hasItem("plugin.key:deep-lib")));
    }

    @Test
    public void given_featureADisabled_when_BarIncludedAndFooExcluded_then_SharedLibIncluded() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);
        strategy = PredicateFailStrategy.CONTINUE;

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:bar"))
                .excluded(newHashSet("_context:foo"), condition)
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(
                found.getFound(),
                contains("plugin.key:deep-lib", "plugin.key:shared-lib", "plugin.key:feature-b", "_context:bar"));
        assertThat(getIgnoredItems(found), contains("plugin.key:feature-s"));
    }

    @Test
    // PLUGWEB-635 - demonstrates that entrypoint iteration order affects the discovery order.
    // PLUGWEB-648 - demonstrates conditional evaluation affects content inclusion order.
    public void given_featureAEnabled_when_BarIncludedThenFooIncluded_then_SharedLibIncluded() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);
        strategy = PredicateFailStrategy.CONTINUE;

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:bar", "_context:foo"))
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(
                "subtree should appear early",
                found.getFound(),
                contains(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-b",
                        "plugin.key:feature-s",
                        "_context:bar",
                        "plugin.key:feature-a",
                        "_context:foo"));
        assertThat(
                "subtree should appear early; should be in depth first sort order, regardless of conditional evaluation",
                found.getAll(),
                contains(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-b",
                        "plugin.key:feature-s",
                        "_context:bar",
                        "plugin.key:feature-a",
                        "_context:foo"));
        assertThat(found.getSkipped(), is(emptyCollectionOf(String.class)));
    }

    @Test
    // PLUGWEB-635 - demonstrates that entrypoint iteration order affects the discovery order.
    // PLUGWEB-648 - demonstrates conditional evaluation affects content inclusion order.
    public void given_featureAEnabled_when_FooIncludedThenBarIncluded_then_SharedLibIncluded() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);
        strategy = PredicateFailStrategy.CONTINUE;

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:foo", "_context:bar"))
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(
                "subtree should appear early",
                found.getFound(),
                contains(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-a",
                        "plugin.key:feature-s",
                        "_context:foo",
                        "plugin.key:feature-b",
                        "_context:bar"));
        assertThat(
                "subtree should appear early; should be in depth first sort order, regardless of conditional evaluation",
                found.getAll(),
                contains(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-a",
                        "plugin.key:feature-s",
                        "_context:foo",
                        "plugin.key:feature-b",
                        "_context:bar"));
        assertThat(found.getSkipped(), is(emptyCollectionOf(String.class)));
    }

    @Test
    // PLUGWEB-635 - demonstrates that entrypoint iteration order affects the discovery order.
    // PLUGWEB-648 - demonstrates conditional evaluation affects content inclusion order.
    public void given_featureADisabled_when_BarIncludedThenFooIncluded_then_SharedLibIncluded() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);
        strategy = PredicateFailStrategy.CONTINUE;

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:bar", "_context:foo"))
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(
                "subtree should appear early",
                found.getFound(),
                contains(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-b",
                        "plugin.key:feature-s",
                        "_context:bar",
                        "_context:foo"));
        assertThat(
                "subtree should appear early; should be in depth first sort order, regardless of conditional evaluation",
                found.getAll(),
                contains(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-b",
                        "plugin.key:feature-s",
                        "_context:bar",
                        "plugin.key:feature-a",
                        "_context:foo"));
        assertThat(found.getSkipped(), contains("plugin.key:feature-a"));
    }

    @Test
    // PLUGWEB-635 - demonstrates that entrypoint iteration order affects the discovery order.
    // PLUGWEB-648 - demonstrates conditional evaluation affects content inclusion order.
    public void given_featureADisabled_when_FooIncludedThenBarIncluded_then_SharedLibIncluded() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);
        strategy = PredicateFailStrategy.CONTINUE;

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:foo", "_context:bar"))
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(
                "subtree should appear late; should be sensitive to conditional evaluation",
                found.getFound(),
                contains(
                        "plugin.key:feature-s",
                        "_context:foo",
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-b",
                        "_context:bar"));
        assertThat(
                "subtree should appear early; should be in depth first sort order, regardless of conditional evaluation",
                found.getAll(),
                contains(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-a",
                        "plugin.key:feature-s",
                        "_context:foo",
                        "plugin.key:feature-b",
                        "_context:bar"));
        assertThat(found.getSkipped(), contains("plugin.key:feature-a"));
    }

    @Test
    public void given_shouldDiscoverFullGraph_when_contextIncludedAndExcluded_then_nothingIncluded() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);
        strategy = PredicateFailStrategy.CONTINUE;

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:foo"))
                .excluded(newHashSet("_context:foo"), condition)
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(found.getFound(), emptyCollectionOf(String.class));
        assertThat(
                found.getAll(),
                contains(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-a",
                        "plugin.key:feature-s",
                        "_context:foo"));
    }

    @Test
    // PLUGWEB-662
    public void given_shouldDiscoverFullGraph_when_contextIncludedPresentInResolvedExclusions_then_nothingIncluded() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);
        strategy = PredicateFailStrategy.CONTINUE;

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:foo"))
                .excludedResolved(newHashSet(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-a",
                        "plugin.key:feature-s",
                        "_context:foo"))
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(found.getFound(), emptyCollectionOf(String.class));
        assertThat(
                found.getAll(),
                contains(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-a",
                        "plugin.key:feature-s",
                        "_context:foo"));
    }

    @Test
    // PLUGWEB-662
    public void
            given_shouldDiscoverFullGraph_when_webResourceAsContextIncludedAndPresentInResolvedExclusions_then_nothingIncluded() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);
        strategy = PredicateFailStrategy.CONTINUE;

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:plugin.key:feature-a"))
                .excludedResolved(newHashSet(
                        "plugin.key:deep-lib",
                        "plugin.key:shared-lib",
                        "plugin.key:feature-a",
                        "_context:plugin.key:feature-a"))
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(found.getFound(), emptyCollectionOf(String.class));
        assertThat(found.getAll(), contains("plugin.key:deep-lib", "plugin.key:shared-lib", "plugin.key:feature-a"));
    }

    @Test
    // PLUGWEB-662
    public void given_shouldDiscoverFullGraph_when_nonexistentContextIncludedAndExcluded_then_nothingIncluded() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);
        strategy = PredicateFailStrategy.CONTINUE;

        // when
        final Found found = new BundleFinder(wr.getGlobals().getSnapshot())
                .included(newHashSet("_context:nonexistent"))
                .excludedResolved(newHashSet("_context:nonexistent"))
                .deepFilter(condition)
                .onDeepFilterFail(strategy)
                .endAndGetResult();

        // then
        assertThat(found.getFound(), emptyCollectionOf(String.class));
        assertThat(found.getAll(), emptyCollectionOf(String.class));
    }

    private List<String> getIgnoredItems(Found found) {
        final List<String> ignoredItems = found.getAll();
        ignoredItems.removeAll(found.getFound());
        ignoredItems.removeAll(found.getSkipped());
        return ignoredItems;
    }
}
