package com.atlassian.plugin.webresource.integration.bundlehash;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import com.google.common.collect.ImmutableMap;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.webresource.integration.stub.ResponseData;
import com.atlassian.webresource.spi.condition.AbstractBooleanUrlReadingCondition;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * This test verifies that hash verification works properly and that hash is consistent. It was introduced due to plugins
 * incorrectly implemented {@link AbstractBooleanUrlReadingCondition#isConditionTrue()}.
 *
 * The condition in plugins was alternating basing on outside conditions. That resulted in WRM generating resource URL with
 * a given hash (A). When browser requested said resource with said hash (A) the verification method calculated different
 * hash (B) due to alternating conditions (the resource was being omitted thus changing hash).
 *
 * Now hash should be constant for given set of plugins (bundles) regardless of their
 * {@link AbstractBooleanUrlReadingCondition#isConditionTrue()} returned values.
 */
public class TestBundleHashVerificationOnFlakyConditions extends BundleHashTestCase {
    private static final String BUNDLE_HASH_VAL = "275aa153c2ae838f5ee0411e4d1d93da";

    @Test
    public void testVerifyHashCalculation_whenResourceNotAvailableDuringHashVerification() {
        prepareWR(true);

        List<String> paths = wr.paths();
        ResponseData responseData = getWithStaticHash(paths.get(0), BUNDLE_HASH_VAL);

        assertThat(responseData.getStatus(), is(200));
        assertThat(responseData.getContent(), containsString("content of a"));
        assertThat(responseData.getContent(), containsString("content of b"));
    }

    @Test
    public void testVerifyHashCalculation_whenResourceNotAvailableDuringPageGeneration() {
        prepareWR(false);

        List<String> paths = wr.paths();
        ResponseData responseData = getWithStaticHash(paths.get(0), BUNDLE_HASH_VAL);

        assertThat(responseData.getStatus(), is(200));
        assertThat(responseData.getContent(), not(containsString("content of a")));
        assertThat(responseData.getContent(), containsString("content of b"));
    }

    @Test
    public void testVerifyHashCalculation_whenResourceAvailableDuringPageGeneration_thenAddRequestParam() {
        prepareWR(true);

        String path = wr.paths().get(0);

        assertThat(path, containsString(BUNDLE_HASH_VAL));
        assertThat(path, containsString(AlternatingCondition.QUERY_KEY + "=true"));
    }

    @Test
    public void testVerifyHashCalculation_whenResourceNotAvailableDuringPageGeneration_thenDoNotAddRequestParam() {
        prepareWR(false);

        String path = wr.paths().get(0);

        assertThat(path, containsString(BUNDLE_HASH_VAL));
        assertThat(path, not(containsString(AlternatingCondition.QUERY_KEY + "=true")));
    }

    @Test
    public void testVerifyHashCalculation_whenResourceNotAvailableDuringPageGeneration2() {
        wr.configure()
                .addToSuperbatch("plugin.key:resource1")
                .plugin("plugin.key")
                .webResource("resource1")
                .resource("a.js")
                .end();

        List<String> paths = wr.paths();

        // single bundle sub batch hash
        String superbatchBundleHash = "9019774f3a8472e5aca804200ba5ac73";
        ResponseData responseData = getWithStaticHash(paths.get(0), superbatchBundleHash);

        assertThat(responseData.getStatus(), is(200));
        assertThat(responseData.getContent(), containsString("content of a"));
    }

    private void prepareWR(boolean conditionStartingPoint) {
        wr.configure()
                .addToSuperbatch("plugin.key:resource1", "plugin.key:resource2")
                .plugin("plugin.key")
                .webResource("resource1")
                .condition(
                        AlternatingCondition.class,
                        ImmutableMap.of("conditionStartingPoint", Boolean.toString(conditionStartingPoint)))
                .resource("a.js")
                .webResource("resource2")
                .resource("b.js")
                .end();
    }

    public static class AlternatingCondition extends AbstractBooleanUrlReadingCondition {
        static final String QUERY_KEY = "alternating";
        private boolean state = true;

        @Override
        public void init(Map<String, String> params) throws PluginParseException {
            state = Boolean.parseBoolean(params.get("conditionStartingPoint"));
        }

        @Override
        protected boolean isConditionTrue() {
            boolean result = state;
            state = !state;
            return result;
        }

        @Override
        protected String queryKey() {
            return QUERY_KEY;
        }
    }
}
