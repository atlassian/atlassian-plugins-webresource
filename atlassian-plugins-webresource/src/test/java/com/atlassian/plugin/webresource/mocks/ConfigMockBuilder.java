package com.atlassian.plugin.webresource.mocks;

import javax.annotation.Nonnull;

import com.atlassian.plugin.webresource.impl.config.Config;

import static java.util.Objects.requireNonNull;
import static org.mockito.Mockito.when;

public class ConfigMockBuilder {
    private final MockBuilder parent;
    private final Config config;

    public ConfigMockBuilder(@Nonnull final MockBuilder parent, @Nonnull final Config config) {
        this.parent = requireNonNull(parent, "The parent is mandatory for the config mock build.");
        this.config = requireNonNull(config, "The config is mandatory for the config mock build.");
    }

    @Nonnull
    public MockBuilder and() {
        return this.parent;
    }

    @Nonnull
    public ConfigMockBuilder minificationEnabled(boolean enabled) {
        when(config.isMinificationEnabled()).thenReturn(enabled);
        return this;
    }
}
