package com.atlassian.plugin.webresource.integration.bundlehash;

import java.util.List;

import org.junit.Test;

import com.atlassian.plugin.webresource.integration.stub.ResponseData;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.stringContainsInOrder;

/**
 * This test verifies that bundlehash values are consistent when serving context subsets.
 */
public class TestBundleHashVerificationOnPartialTrees extends BundleHashTestCase {
    /**
     * A hash derived from the following expected dependency order:
     * feature-a, feature-b, _context:foo
     */
    private static final String BUNDLEHASH_FOR_FOO = "7b8f4df15c9be1e5c3e0702b5e33a343";
    /**
     * A hash derived from the following expected dependency order:
     * feature-b, feature-c, _context:bar
     */
    private static final String BUNDLEHASH_FOR_BAR = "b5187a52628507e8ed8e3fd3a65ddff3";
    /**
     * A hash derived from the following expected dependency order:
     * feature-a, _context:foo
     */
    private static final String BUNDLEHASH_FOR_FOO_MINUS_BAR = "7b8f4df15c9be1e5c3e0702b5e33a343";

    /**
     * Prepares three web-resources `feature-a`, `feature-b`, and `feature-c`:
     * <ul>
     *     <li>Features A and B are in the `foo` context</li>
     *     <li>Features B and C are in the `bar` context</li>
     * </ul>
     */
    @Override
    protected void setUp() throws Exception {
        wr.configure()
                .plugin("plugin.key")
                .webResource("feature-a")
                .context("foo")
                .resource("feature-a.js")
                .webResource("feature-b")
                .context("foo")
                .context("bar")
                .resource("feature-b.js")
                .webResource("feature-c")
                .context("bar")
                .resource("feature-c.js")
                .end();
    }

    @Test
    public void testFooHash() {
        // given
        final String bundlehash =
                getBundleHashForRequestables("plugin.key:feature-a", "plugin.key:feature-b", "_context:foo");

        // when
        wr.requireContext("foo");
        final List<String> paths = wr.paths();
        final ResponseData resp = getWithStaticHash(paths.get(0), bundlehash);

        // then
        assertThat(paths, everyItem(containsString(BUNDLEHASH_FOR_FOO)));
        assertThat(bundlehash, equalTo(BUNDLEHASH_FOR_FOO));
        assertThat(resp.getStatus(), equalTo(200));
        assertThat(
                resp.getContent(),
                stringContainsInOrder(newArrayList("content of feature-a.js", "content of feature-b.js")));
    }

    @Test
    public void testBarHash() {
        // given
        final String bundlehash =
                getBundleHashForRequestables("plugin.key:feature-b", "plugin.key:feature-c", "_context:bar");

        // when
        wr.requireContext("bar");
        final List<String> paths = wr.paths();
        final ResponseData resp = getWithStaticHash(paths.get(0), bundlehash);

        // then
        assertThat(paths, everyItem(containsString(BUNDLEHASH_FOR_BAR)));
        assertThat(bundlehash, equalTo(BUNDLEHASH_FOR_BAR));
        assertThat(resp.getStatus(), equalTo(200));
        assertThat(
                resp.getContent(),
                stringContainsInOrder(newArrayList("content of feature-b.js", "content of feature-c.js")));
    }

    @Test
    public void testFooMinusBarHash() {
        // when
        wr.requireContext("foo");
        wr.excludeContext("bar");
        final List<String> paths = wr.paths();
        final ResponseData resp = getWithOwnStaticHash(paths.get(0));

        // then
        assertThat(
                "despite removing bar, graph overlap should still count toward the hash",
                paths,
                everyItem(containsString(BUNDLEHASH_FOR_FOO_MINUS_BAR)));
        assertThat(resp.getStatus(), equalTo(200));
        assertThat(
                resp.getContent(),
                allOf(containsString("content of feature-a.js"), not(containsString("content of feature-b.js"))));
    }

    @Test
    public void testBarMinusFooHash() {
        // when
        wr.requireContext("bar");
        wr.excludeContext("foo");
        final List<String> paths = wr.paths();
        final ResponseData resp = getWithOwnStaticHash(paths.get(0));

        // then
        assertThat(
                "despite removing foo, graph overlap should still count toward the hash",
                paths,
                everyItem(containsString(BUNDLEHASH_FOR_BAR)));
        assertThat(resp.getStatus(), equalTo(200));
        assertThat(
                resp.getContent(),
                allOf(containsString("content of feature-c.js"), not(containsString("content of feature-b.js"))));
    }
}
