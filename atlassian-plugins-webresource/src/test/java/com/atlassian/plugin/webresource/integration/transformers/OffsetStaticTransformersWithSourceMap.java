package com.atlassian.plugin.webresource.integration.transformers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.sourcemap.ReadableSourceMap;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.api.url.UrlBuilder;

public class OffsetStaticTransformersWithSourceMap implements StaticTransformers {

    @Override
    public void loadTwoPhaseProperties(
            ResourceLocation resourceLocation, java.util.function.Function<String, InputStream> loadFromFile) {}

    @Override
    public boolean hasTwoPhaseProperties() {
        return false;
    }

    @Override
    public void addToUrl(
            String locationType,
            TransformerParameters transformerParameters,
            UrlBuilder urlBuilder,
            UrlBuildingStrategy urlBuildingStrategy) {}

    @Override
    public Content transform(
            Content content,
            TransformerParameters transformerParameters,
            ResourceLocation resourceLocation,
            QueryParams queryParams,
            String sourceUrl) {
        return new ContentImpl(content.getContentType(), true) {
            @Override
            public ReadableSourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                ReadableSourceMap sourceMap;
                try {
                    out.write(("offset-static-transformer\n").getBytes());
                    sourceMap = content.writeTo(out, isSourceMapEnabled);
                    out.write(("\noffset-static-transformer").getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                if (isSourceMapEnabled && (sourceMap != null)) {
                    sourceMap.addOffset(1);
                }
                return sourceMap;
            }
        };
    }

    @Override
    public Set<String> getParamKeys() {
        return new HashSet<>();
    }
}
