package com.atlassian.plugin.webresource.data;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.data.PluginDataResource;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;

import static com.atlassian.plugin.webresource.TestUtils.removeTrailingSpaces;
import static com.atlassian.plugin.webresource.TestUtils.removeWebResourceLogs;

public class DataTestFixture {
    public static final String EXPECTED_PRE_DATA =
            "<script>\nwindow.WRM=window.WRM||{};" + "window.WRM._unparsedData=window.WRM._unparsedData||{};"
                    + "window.WRM._unparsedErrors=window.WRM._unparsedErrors||{};";
    public static final String EXPECTED_POST_DATA = "if(window.WRM._dataArrived)window.WRM._dataArrived();</script>";

    public static void assertDataTag(String key, final String json, String expectedMarkup) throws IOException {
        Iterable<PluginDataResource> data =
                singletonList(new DefaultPluginDataResource(key, writer -> writer.write(json)));

        Collection<Matcher<? super String>> matchers = newArrayList(containsString(expectedMarkup));
        matchers.add(startsWith(EXPECTED_PRE_DATA));
        matchers.add(endsWith(EXPECTED_POST_DATA));

        StringWriter writer = new StringWriter();
        new DataTagWriter().write(writer, data);
        assertThat(removeTrailingSpaces(writer.toString()), allOf(matchers));
    }

    public static void assertDataTag(Iterable<PluginDataResource> data, String expectedMarkup) throws IOException {
        StringWriter writer = new StringWriter();
        new DataTagWriter().write(writer, data);
        assertThat(writer.toString(), equalTo(expectedMarkup));
    }

    public static void assertDataTag(WebResourceSet resources, String... expectedEntries) {
        Collection<Matcher<? super String>> matchers =
                Arrays.stream(expectedEntries).map(CoreMatchers::containsString).collect(toList());
        matchers.add(startsWith(EXPECTED_PRE_DATA));
        matchers.add(endsWith(EXPECTED_POST_DATA));
        StringWriter writer = new StringWriter();
        resources.writeHtmlTags(writer, UrlMode.AUTO);
        assertThat(removeWebResourceLogs(writer.toString()), allOf(matchers));
    }
}
