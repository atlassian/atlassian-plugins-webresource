package com.atlassian.plugin.webresource.transformer.instance;

import org.junit.Test;

import com.atlassian.plugin.elements.ResourceLocation;

import static java.util.Collections.emptyMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TestRelativeUrlTransformerMatcher {
    private final RelativeUrlTransformerMatcher matcher = new RelativeUrlTransformerMatcher();

    @Test
    public void testMatchesLess() {
        assertThat(matcher.matches("less"), is(true));
    }

    @Test
    public void testMatchesCss() {
        assertThat(matcher.matches("css"), is(true));
    }

    @Test
    public void testDoesNotMatchSass() {
        assertThat(matcher.matches("sass"), is(false));
        assertThat(matcher.matches("scss"), is(false));
    }

    @Test
    public void testMatchesWebResourceEndingWithCss() {
        final ResourceLocation cssResourceLocation =
                new ResourceLocation(null, "abcd.css", "css", null, null, emptyMap());

        assertThat(matcher.matches(cssResourceLocation), is(true));
    }

    @Test
    public void testMatchesWebResourceLocationEndingWithLess() {
        final ResourceLocation resourceLocation =
                new ResourceLocation(null, "abcd.less", "less", null, null, emptyMap());

        assertThat(matcher.matches(resourceLocation), is(true));
    }

    @Test
    public void testDoesNotMatchWebResourceLocationForSass() {
        final ResourceLocation sassFile = new ResourceLocation(null, "abcd.sass", "sass", null, null, emptyMap());
        final ResourceLocation scssFile = new ResourceLocation(null, "abcd.scss", "scss", null, null, emptyMap());

        assertThat(matcher.matches(sassFile), is(false));
        assertThat(matcher.matches(scssFile), is(false));
    }

    @Test
    public void testHandlesNullExtension() {
        assertThat(matcher.matches((String) null), is(false));
    }

    @Test
    public void testHandlesNullFileNameAndType() {
        final ResourceLocation resourceLocation = new ResourceLocation(null, null, null, null, null, emptyMap());

        assertThat(matcher.matches(resourceLocation), is(false));
    }
}
