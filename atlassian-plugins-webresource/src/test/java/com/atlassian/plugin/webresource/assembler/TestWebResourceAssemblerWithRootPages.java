package com.atlassian.plugin.webresource.assembler;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceSet;

import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.assertResolvedResources;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.c;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.m;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.superbatch;

public class TestWebResourceAssemblerWithRootPages {
    private AssemblerTestFixture f;

    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    @Before
    public void setUp() throws Exception {
        f = new AssemblerTestFixture();
        System.setProperty("atlassian.darkfeature.atlassian.webresource.performance.tracking.disable", "true");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRootPageDoesNotExist() {
        f.create().resources().requirePage("test-page");
    }

    @Test
    public void testRootPageSingleResourceDep() throws Exception {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:page-a", true).deps("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().requirePage("test.atlassian:page-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-a"));
    }

    @Test
    public void testRootPageMultipleResourceDeps() throws Exception {
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes();
        f.mockPlugin("test.atlassian:resource-c").defRes();
        f.mockPlugin("test.atlassian:page-a", true)
                .deps("test.atlassian:resource-a")
                .deps("test.atlassian:resource-b")
                .deps("test.atlassian:resource-c");

        WebResourceAssembler assembler = f.create();
        assembler.resources().requirePage("test.atlassian:page-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(
                resources,
                m("test.atlassian:resource-a"),
                m("test.atlassian:resource-b"),
                m("test.atlassian:resource-c"));
    }

    @Test
    public void testRootPageSuperbatchIncluded() throws Exception {
        f.mockPlugin("test.atlassian:resource-sb").defRes();
        f.mockSuperbatch("test.atlassian:resource-sb");

        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:page-a", true).deps("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().requirePage("test.atlassian:page-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, superbatch(), m("test.atlassian:resource-a"));
    }

    @Test
    public void testRootPageSuperbatchNotIncluded() throws Exception {
        f.mockPlugin("test.atlassian:resource-sb").defRes();
        f.mockSuperbatch("test.atlassian:resource-sb");

        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes();
        f.mockPlugin("test.atlassian:page-a", true)
                .deps("test.atlassian:resource-a")
                .deps("test.atlassian:resource-b");

        WebResourceAssembler assembler =
                f.factory().create().includeSuperbatchResources(false).build();
        assembler.resources().requirePage("test.atlassian:page-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-a"), m("test.atlassian:resource-b"));
    }

    @Test
    public void testRootPageContextDeps() throws Exception {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-c").defRes();
        f.mockPlugin("test.atlassian:page-a", true).ctxDeps("context.a").deps("test.atlassian:resource-c");

        WebResourceAssembler assembler = f.create();
        assembler.resources().requirePage("test.atlassian:page-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a"), m("test.atlassian:resource-c"));
    }
}
