package com.atlassian.plugin.webresource.integration.content;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.fixtures.BroadFeatures;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.webresource.api.assembler.resource.ResourcePhase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.doReturn;

import static com.atlassian.plugin.webresource.util.DuplicateContentMatcher.doesNotDuplicateSuperbatchContents;

@RunWith(Parameterized.class)
public class TestContentInclusionWithSuperbatch extends TestCase {
    private static final boolean[] possibleContextBatchingValues = new boolean[] {true, false};
    private static final boolean[] possibleWebresourceBatchingValues = new boolean[] {true, false};

    private final boolean contextBatchingEnabled;
    private final boolean webresourceBatchingEnabled;
    private final ResourcePhase superbatchPhase;

    public TestContentInclusionWithSuperbatch(
            final boolean contextBatchingEnabled,
            final boolean webresourceBatchingEnabled,
            final ResourcePhase superbatchPhase) {
        this.contextBatchingEnabled = contextBatchingEnabled;
        this.webresourceBatchingEnabled = webresourceBatchingEnabled;
        this.superbatchPhase = superbatchPhase;
    }

    @Parameterized.Parameters(name = "superbatch-phase: {2}, context-batching: {0}, webresource-batching: {1}")
    public static Collection<Object[]> configure() {
        final Collection<Object[]> values = new ArrayList<>();
        for (final ResourcePhase phase : ResourcePhase.values()) {
            for (final boolean contextBatching : possibleContextBatchingValues) {
                for (final boolean webresourceBatching : possibleWebresourceBatchingValues) {
                    values.add(new Object[] {contextBatching, webresourceBatching, phase});
                }
            }
        }
        return values;
    }

    /**
     *
     */
    @Before
    public void setUp() throws Exception {
        BroadFeatures.configure(wr.configure(config -> {
                    doReturn(contextBatchingEnabled).when(config).isContextBatchingEnabled();
                    doReturn(webresourceBatchingEnabled).when(config).isWebResourceBatchingEnabled();
                    doReturn(true).when(config).isSuperBatchingEnabled();
                    doReturn(false).when(config).isPerformanceTrackingEnabled();
                })
                .addToSuperbatch(
                        "plugin.key:feature-04",
                        "plugin.key:feature-05",
                        "plugin.key:feature-14",
                        "plugin.key:feature-15",
                        "plugin.key:feature-24",
                        "plugin.key:feature-25"));
        wr.getPageBuilderService().assembler().resources().requireSuperbatch(superbatchPhase);
    }

    @Test
    public void given_nothingRequested_when_1NcontextRequested_then_shouldOnlyServe1NContentOnce() {
        // when
        wr.requireContext("1N");
        final WebResource.DrainResult result = wr.drain();
        final String content = wr.getContent(result);

        // then
        assertThat(content, doesNotDuplicateSuperbatchContents());
    }

    @Test
    public void given_nothingRequested_when_1Nand2NcontextRequested_then_shouldOnlyServeContentOnce() {
        // when
        wr.requireContext("1N");
        wr.requireContext("2N");
        final WebResource.DrainResult result = wr.drain();
        final String content = wr.getContent(result);

        // then
        assertThat(content, doesNotDuplicateSuperbatchContents());
    }

    @Test
    public void given_nothingRequested_when_1NContextRequestedMultipleTimes_then_shouldOnlyServeContentOnce() {
        // given
        final StringBuffer sb = new StringBuffer();

        // when
        wr.requireContext("1N");
        sb.append(wr.getContent(wr.drain()));

        wr.requireContext("1N");
        sb.append(wr.getContent(wr.drain()));

        wr.requireContext("1N");
        sb.append(wr.getContent(wr.drain()));

        final String content = sb.toString();

        // then
        assertThat(content, doesNotDuplicateSuperbatchContents());
    }

    @Test
    public void given_nothingRequested_when_AllContextsRequestedOverMultipleDrains_then_shouldOnlyServeContentOnce() {
        // given
        final StringBuffer sb = new StringBuffer();

        // when
        wr.requireContext("0N");
        sb.append(wr.getContent(wr.drain()));

        wr.requireContext("1N");
        sb.append(wr.getContent(wr.drain()));

        wr.requireContext("2N");
        sb.append(wr.getContent(wr.drain()));

        final String content = sb.toString();

        // then
        assertThat(content, doesNotDuplicateSuperbatchContents());
    }

    @Test
    public void given_0NcontextAlreadyRequested_when_1Nand2NcontextRequested_then_shouldOnlyServeContentOnce() {
        // given
        wr.excludeContext("0N");

        // when
        wr.requireContext("1N");
        wr.requireContext("2N");

        final WebResource.DrainResult result = wr.drain();
        final String content = wr.getContent(result);

        // then
        assertThat(content, doesNotDuplicateSuperbatchContents());
        assertThat(
                content,
                not(anyOf(
                        containsString("content of feature-00.js"),
                        containsString("content of feature-01.js"),
                        containsString("content of feature-02.js"),
                        containsString("content of feature-03.js"),
                        containsString("content of feature-04.js"),
                        containsString("content of feature-05.js"),
                        containsString("content of feature-06.js"),
                        containsString("content of feature-07.js"),
                        containsString("content of feature-08.js"),
                        containsString("content of feature-09.js"))));
    }
}
