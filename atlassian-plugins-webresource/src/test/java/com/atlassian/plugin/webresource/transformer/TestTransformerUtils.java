package com.atlassian.plugin.webresource.transformer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;

public class TestTransformerUtils extends TestCase {
    public void testTransform() throws Exception {
        final DownloadableResource resource = new MockDownloadableResource("hideho!");
        final OutputStream capture = new ByteArrayOutputStream();
        TransformerUtils.transformAndStreamResource(resource, TransformerUtils.UTF8, capture, from -> "hoodeha?");
        assertEquals("hoodeha?", capture.toString());
    }

    public static class MockDownloadableResource implements DownloadableResource {
        private final String content;

        public MockDownloadableResource(final String content) {
            this.content = content;
        }

        public void streamResource(final OutputStream out) throws DownloadException {
            try {
                out.write(content.getBytes(TransformerUtils.UTF8));
            } catch (final IOException e) {
                throw new DownloadException(e);
            }
        }

        public void serveResource(final HttpServletRequest request, final HttpServletResponse response)
                throws DownloadException {}

        public boolean isResourceModified(final HttpServletRequest request, final HttpServletResponse response) {
            return false;
        }

        public String getContentType() {
            return null;
        }
    }
}
