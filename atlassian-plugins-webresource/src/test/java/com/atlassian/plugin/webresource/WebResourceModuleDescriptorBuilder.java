package com.atlassian.plugin.webresource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableMap;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.ModuleFactory;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

import static com.atlassian.plugin.webresource.Dom4jFluent.element;
import static com.atlassian.plugin.webresource.TestUtils.buildMap;

/**
 *
 */
public class WebResourceModuleDescriptorBuilder {
    private final Plugin plugin;
    private final String moduleKey;
    private List<Dom4jFluent.Element> resourceDescriptors = new ArrayList<>();
    private List<Dom4jFluent.Element> dependencies = new ArrayList<>();
    private Dom4jFluent.Element condition = null;
    private List<Dom4jFluent.Element> conditions = new ArrayList<>();
    private List<Dom4jFluent.Element> contexts = new ArrayList<>();
    private List<Dom4jFluent.Element> transformers = new ArrayList<>();
    private List<Dom4jFluent.Element> dataProviders = new ArrayList<>();

    public WebResourceModuleDescriptorBuilder(Plugin plugin, String moduleKey) {
        this.plugin = plugin;
        this.moduleKey = moduleKey;
    }

    public WebResourceModuleDescriptorBuilder setCondition(Class<?> condition) {
        this.condition = element("condition", ImmutableMap.of("class", condition.getName()));
        return this;
    }

    public WebResourceModuleDescriptorBuilder addCondition(
            Class<?> condition, Map<String, String> params, boolean invert) {
        Function<Map.Entry<String, String>, Dom4jFluent.Element> toParamElement =
                e -> element("param", ImmutableMap.of("name", e.getKey()), e.getValue());
        List<Dom4jFluent.Element> collect =
                params.entrySet().stream().map(toParamElement).collect(Collectors.toList());
        Map<String, String> attributes = buildMap("class", condition.getName());
        if (invert) {
            attributes.put("invert", "true");
        }
        this.conditions.add(element("condition", attributes, singletonList(collect)));
        return this;
    }

    public WebResourceModuleDescriptorBuilder addDescriptor(String downloadableFile) {
        return addDescriptor("download", downloadableFile, downloadableFile);
    }

    public WebResourceModuleDescriptorBuilder addDescriptor(String type, String name, String location) {
        resourceDescriptors.add(element(
                "resource",
                ImmutableMap.of(
                        "type", type,
                        "name", name,
                        "location", location)));
        return this;
    }

    public WebResourceModuleDescriptorBuilder addDescriptor(
            String type, String name, String location, String contentType, Map<String, String> params) {
        List<Dom4jFluent.Element> xmlParams = new ArrayList<>();
        if (contentType != null)
            xmlParams.add(element("param", ImmutableMap.of("name", "content-type", "value", contentType)));
        for (Map.Entry<String, String> entry : params.entrySet()) {
            xmlParams.add(element("param", ImmutableMap.of("name", entry.getKey(), "value", entry.getValue())));
        }
        Dom4jFluent.Element[] asArray =
                (Dom4jFluent.Element[]) xmlParams.toArray(new Dom4jFluent.Element[xmlParams.size()]);
        resourceDescriptors.add(element(
                "resource",
                ImmutableMap.of(
                        "type", type,
                        "name", name,
                        "location", location),
                asArray));
        return this;
    }

    public WebResourceModuleDescriptorBuilder addDependency(String moduleKey) {
        dependencies.add(element("dependency", moduleKey));
        return this;
    }

    public WebResourceModuleDescriptorBuilder addContext(String context) {
        contexts.add(element("context", context));
        return this;
    }

    public void addDataProvider(String key, Class<?> provider) {
        dataProviders.add(element("data", ImmutableMap.of("key", key, "class", provider.getName())));
    }

    public WebResourceModuleDescriptorBuilder addTransformers(String extension, List<String> transformerKeys) {
        Dom4jFluent.Element[] list = new Dom4jFluent.Element[transformerKeys.size()];
        for (int i = 0; i < transformerKeys.size(); i++) {
            list[i] = element("transformer", ImmutableMap.of("key", transformerKeys.get(i)));
        }
        transformers.add(element("transformation", ImmutableMap.of("extension", extension), list));
        return this;
    }

    public WebResourceModuleDescriptor build() {
        return build(new DefaultHostContainer());
    }

    public WebResourceModuleDescriptor build(HostContainer hostContainer) {
        List<Dom4jFluent.Element> conditionList = null == condition ? emptyList() : singletonList(condition);
        List<Dom4jFluent.Element> conditionsList = conditions.isEmpty()
                ? emptyList()
                : singletonList(element("conditions", ImmutableMap.of("type", "and"), singletonList(conditions)));
        WebResourceModuleDescriptor descriptor =
                new WebResourceModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, hostContainer);
        Dom4jFluent.Element element = element(
                "web-resource",
                ImmutableMap.of("key", moduleKey),
                asList(
                        resourceDescriptors,
                        dependencies,
                        conditionList,
                        conditionsList,
                        contexts,
                        transformers,
                        dataProviders));
        descriptor.init(plugin, new Dom4jDelegatingElement(element.toDom()));
        descriptor.enabled();
        return descriptor;
    }
}
