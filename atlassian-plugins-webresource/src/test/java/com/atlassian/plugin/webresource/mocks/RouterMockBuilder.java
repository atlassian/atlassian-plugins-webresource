package com.atlassian.plugin.webresource.mocks;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;

import static java.util.Objects.requireNonNull;
import static org.mockito.Mockito.when;

public class RouterMockBuilder {
    private final MockBuilder parent;
    private final Router router;

    public RouterMockBuilder(@Nonnull final MockBuilder parent, @Nonnull final Router router) {
        this.parent = requireNonNull(parent, "The parent is mandatory for the router mock build.");
        this.router = requireNonNull(router, "The router is mandatory for the router mock build.");
    }

    @Nonnull
    public RouterMockBuilder prebuildSourceUrl(@Nullable final Resource resource, @Nullable final String url) {
        when(router.prebuildSourceUrl(resource)).thenReturn(url);
        return this;
    }

    @Nonnull
    public RouterMockBuilder sourceUrl(@Nullable final Resource resource, @Nullable final String url) {
        when(router.sourceUrl(resource)).thenReturn(url);
        return this;
    }
}
