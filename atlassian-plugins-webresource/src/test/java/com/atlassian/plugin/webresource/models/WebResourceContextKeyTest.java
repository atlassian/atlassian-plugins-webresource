package com.atlassian.plugin.webresource.models;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class WebResourceContextKeyTest {
    @Test
    public void when_KeyWithoutPrefixPassed_Then_ConstructNormally() {
        // given
        final String key = "wrm.key";

        // when
        final WebResourceContextKey webResourceContextKey = new WebResourceContextKey(key);

        // then
        assertThat(webResourceContextKey.getKey(), equalTo("wrm.key"));
        assertThat(webResourceContextKey.toLooseType(), equalTo("_context:wrm.key"));
    }

    @Test
    public void when_KeyWithLegacyPrefixPassed_Then_StripPrefix() {
        // given
        final String key = "_context:wrm.key";

        // when
        final WebResourceContextKey webResourceContextKey = new WebResourceContextKey(key);

        // then
        assertThat(webResourceContextKey.getKey(), equalTo("wrm.key"));
        assertThat(webResourceContextKey.toLooseType(), equalTo("_context:wrm.key"));
    }

    @Test
    public void when_KeyWithDoubleLegacyContextPrefixPassed_Then_StripPrefix() {
        // given
        final String key = "_context:_context:wrm.key";

        // when
        final WebResourceContextKey webResourceContextKey = new WebResourceContextKey(key);

        // then
        assertThat(webResourceContextKey.getKey(), equalTo("wrm.key"));
        assertThat(webResourceContextKey.toLooseType(), equalTo("_context:wrm.key"));
    }

    @Test
    public void when_KeyWithOddLegacyContextPrefixPassed_Then_StripPrefix() {
        // given
        final String key = "_context_context:wrm.key";

        // when
        final WebResourceContextKey webResourceContextKey = new WebResourceContextKey(key);

        // then
        assertThat(webResourceContextKey.getKey(), equalTo("wrm.key"));
        assertThat(webResourceContextKey.toLooseType(), equalTo("_context:wrm.key"));
    }

    @Test
    public void when_KeyWithLegacyContextSuffix_Then_ConstructNormally() {
        // given
        final String key = "wrm.key_context:";

        // when
        final WebResourceContextKey webResourceContextKey = new WebResourceContextKey(key);

        // then
        assertThat(webResourceContextKey.getKey(), equalTo("wrm.key_context:"));
        assertThat(webResourceContextKey.toLooseType(), equalTo("_context:wrm.key_context:"));
    }

    @Test
    public void when_KeyWithLegacyContextInfix_Then_ConstructNormally() {
        // given
        final String key = "wrm._context:key";

        // when
        final WebResourceContextKey webResourceContextKey = new WebResourceContextKey(key);

        // then
        assertThat(webResourceContextKey.getKey(), equalTo("wrm._context:key"));
        assertThat(webResourceContextKey.toLooseType(), equalTo("_context:wrm._context:key"));
    }
}
