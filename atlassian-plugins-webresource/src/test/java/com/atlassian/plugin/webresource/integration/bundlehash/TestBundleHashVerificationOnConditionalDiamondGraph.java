package com.atlassian.plugin.webresource.integration.bundlehash;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.atlassian.plugin.webresource.integration.fixtures.ConditionalDiamond;
import com.atlassian.plugin.webresource.integration.stub.ResponseData;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.stringContainsInOrder;

public class TestBundleHashVerificationOnConditionalDiamondGraph extends BundleHashTestCase {
    private static final String BUNDLEHASH_FOR_FOO = "7a8e2e366ea6a3f489ccf49f75dfda19";
    private static final String BUNDLEHASH_FOR_BAR = "6b9114be0d143b722f6236be32b39931";
    private static final String BUNDLEHASH_FOR_FOOBAR = "4515c9c0582de984966175d2011fbdfd";
    private static final String BUNDLEHASH_FOR_BARFOO = "703ef1624c7296f3ac16299335fee69c";

    private List<String> urls;

    @Override
    protected void setUp() throws Exception {
        ConditionalDiamond.configure(wr);

        // sanity check
        final String fooBundleHash = getBundleHashForRequestables(
                "plugin.key:deep-lib",
                "plugin.key:shared-lib",
                "plugin.key:feature-a",
                "plugin.key:feature-s",
                "_context:foo");

        final String barBundleHash = getBundleHashForRequestables(
                "plugin.key:deep-lib",
                "plugin.key:shared-lib",
                "plugin.key:feature-b",
                "plugin.key:feature-s",
                "_context:bar");

        final String fooBarBundleHash = getBundleHashForRequestables(
                "plugin.key:deep-lib",
                "plugin.key:shared-lib",
                "plugin.key:feature-a",
                "plugin.key:feature-s",
                "_context:foo",
                "plugin.key:feature-b",
                "_context:bar");
        final String barFooBundleHash = getBundleHashForRequestables(
                "plugin.key:deep-lib",
                "plugin.key:shared-lib",
                "plugin.key:feature-b",
                "plugin.key:feature-s",
                "_context:bar",
                "plugin.key:feature-a",
                "_context:foo");

        assertThat("hash for foo context is unexpected", fooBundleHash, equalTo(BUNDLEHASH_FOR_FOO));
        assertThat("hash for bar context is unexpected", barBundleHash, equalTo(BUNDLEHASH_FOR_BAR));
        assertThat("hash for foo,bar context is unexpected", fooBarBundleHash, equalTo(BUNDLEHASH_FOR_FOOBAR));
        assertThat("hash for bar,foo context is unexpected", barFooBundleHash, equalTo(BUNDLEHASH_FOR_BARFOO));

        urls = new ArrayList<>();
    }

    @Test
    public void given_featureAEnabled_when_ContextSubBatchRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);

        // when
        wr.requireContext("foo");
        urls.addAll(wr.paths());
        wr.requireContext("bar");
        urls.addAll(wr.paths());
        assertThat("the second url needs to be for a subbatch", urls.get(1), containsString("bar,-foo"));

        final ResponseData fooResult = getWithStaticHash(urls.get(0), BUNDLEHASH_FOR_FOO);
        final ResponseData barResult = getWithStaticHash(urls.get(1), BUNDLEHASH_FOR_BAR);

        // then
        assertThat(fooResult.getStatus(), is(200));
        assertThat(
                fooResult.getContent(),
                stringContainsInOrder(
                        newArrayList("content of lib.js", "content of feature-a.js", "content of feature-s.js")));
        assertThat(
                "despite removing foo, graph overlap should still count toward the hash",
                barResult.getStatus(),
                is(200));
        assertThat(
                barResult.getContent(),
                allOf(not(containsString("content of lib.js")), containsString("content of feature-b.js")));
    }

    @Test
    public void given_featureADisabled_when_ContextSubBatchRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);

        // when
        wr.requireContext("foo");
        urls.addAll(wr.paths());
        wr.requireContext("bar");
        urls.addAll(wr.paths());
        assertThat("the second url needs to be for a subbatch", urls.get(1), containsString("bar,-foo"));

        final ResponseData fooResult = getWithStaticHash(urls.get(0), BUNDLEHASH_FOR_FOO);
        final ResponseData barResult = getWithStaticHash(urls.get(1), BUNDLEHASH_FOR_BAR);

        // then
        assertThat(fooResult.getStatus(), is(200));
        assertThat(
                fooResult.getContent(),
                allOf(not(containsString("content of lib.js")), containsString("content of feature-s.js")));
        assertThat(
                "despite removing foo, graph overlap should still count toward the hash",
                barResult.getStatus(),
                is(200));
        assertThat(
                barResult.getContent(),
                stringContainsInOrder(newArrayList("content of lib.js", "content of feature-b.js")));
    }

    @Test
    public void given_featureAEnabled_when_FooAndBarRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);

        // when
        wr.requireContext("foo");
        wr.requireContext("bar");
        urls.addAll(wr.paths());

        final ResponseData fooResult = getWithStaticHash(urls.get(0), BUNDLEHASH_FOR_FOOBAR);

        // then
        assertThat(fooResult.getStatus(), is(200));
        assertThat(
                fooResult.getContent(),
                stringContainsInOrder(newArrayList(
                        "content of lib.js",
                        "content of feature-a.js",
                        "content of feature-s.js",
                        "content of feature-b.js")));
    }

    @Test
    public void given_featureADisabled_when_FooAndBarRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);

        // when
        wr.requireContext("foo");
        wr.requireContext("bar");
        urls.addAll(wr.paths());

        final ResponseData fooResult = getWithStaticHash(urls.get(0), BUNDLEHASH_FOR_FOOBAR);

        // then
        assertThat(fooResult.getStatus(), is(200));
        assertThat(
                fooResult.getContent(),
                stringContainsInOrder(
                        newArrayList("content of feature-s.js", "content of lib.js", "content of feature-b.js")));
    }

    @Test
    public void given_featureAEnabled_when_BarAndFooRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);

        // when
        wr.requireContext("bar");
        wr.requireContext("foo");
        urls.addAll(wr.paths());

        final ResponseData fooResult = getWithStaticHash(urls.get(0), BUNDLEHASH_FOR_BARFOO);

        // then
        assertThat(fooResult.getStatus(), is(200));
        assertThat(
                fooResult.getContent(),
                stringContainsInOrder(newArrayList(
                        "content of lib.js",
                        "content of feature-b.js",
                        "content of feature-s.js",
                        "content of feature-a.js")));
    }

    @Test
    public void given_featureADisabled_when_BarAndFooRequested_then_BundlehashIsConsistent() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);

        // when
        wr.requireContext("bar");
        wr.requireContext("foo");
        urls.addAll(wr.paths());

        final ResponseData fooResult = getWithStaticHash(urls.get(0), BUNDLEHASH_FOR_BARFOO);

        // then
        assertThat(fooResult.getStatus(), is(200));
        assertThat(
                fooResult.getContent(),
                stringContainsInOrder(
                        newArrayList("content of lib.js", "content of feature-b.js", "content of feature-s.js")));
    }
}
