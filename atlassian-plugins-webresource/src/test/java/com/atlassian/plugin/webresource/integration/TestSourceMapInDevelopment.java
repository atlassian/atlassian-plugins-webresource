package com.atlassian.plugin.webresource.integration;

import org.junit.Before;

public class TestSourceMapInDevelopment extends TestSourceMapInProduction {
    @Before
    public void configure() {
        optimiseSourceMapForDevelopment = true;
    }
}
