package com.atlassian.plugin.webresource.impl.config;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.slf4j.Logger;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.internal.util.PluginUtils;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.impl.snapshot.Deprecation;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.webresource.api.WebResourceUrlProvider;

import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.sal.api.features.DarkFeatureManager.ATLASSIAN_DARKFEATURE_PREFIX;

public class ConfigTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    @Mock
    ResourceBatchingConfiguration resourceBatchingConfiguration;

    @Mock
    WebResourceIntegration webResourceIntegration;

    @Mock
    WebResourceUrlProvider webResourceUrlProvider;

    @Mock
    ServletContextFactory servletContextFactory;

    @Mock
    TransformerCache transformerCache;

    @Mock
    Logger logger;

    @Mock
    Plugin plugin;

    @Before
    public void setUp() {
        when(plugin.getName()).thenReturn("fakeplugin");
    }

    @Test
    public void testContentCacheEnabledWithDefaults() throws Exception {
        // given
        Config config = createConfig();

        // when
        boolean cacheEnabled = config.isContentCacheEnabled();

        // then
        assertThat(cacheEnabled, is(true));
    }

    @Test
    public void testContentCacheDisabledWithDevMode() throws Exception {
        // given
        System.setProperty(PluginUtils.ATLASSIAN_DEV_MODE, "true");
        Config config = createConfig();

        // when
        boolean cacheEnabled = config.isContentCacheEnabled();

        // then
        assertThat(cacheEnabled, is(false));
    }

    @Test
    public void testContentCacheDisabledWithDisabledFileCache() throws Exception {
        // given
        System.setProperty(PluginUtils.WEBRESOURCE_DISABLE_FILE_CACHE, "true");
        Config config = createConfig();

        // when
        boolean cacheEnabled = config.isContentCacheEnabled();

        // then
        assertThat(cacheEnabled, is(false));
    }

    @Test
    public void testDependingOnDeprecatedWebResourcesLogsWarnings() throws Exception {
        System.setProperty(PluginUtils.ATLASSIAN_DEV_MODE, "true");
        final WebResourceModuleDescriptor deprecated = makeDeprecatedWebResource("foo-bar");
        final List<WebResourceModuleDescriptor> list = asList(
                makeWebResource("test-before-defined", deprecated),
                deprecated,
                makeWebResource("test-after-defined", deprecated));
        final Config config = createConfig();
        final PluginAccessor pa = mock(PluginAccessor.class);
        when(webResourceIntegration.getPluginAccessor()).thenReturn(pa);
        when(pa.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
                .thenReturn(list);

        config.getWebResourcesWithoutCache();
        verify(logger).warn("fakeplugin:foo-bar is deprecated (required by \"fakeplugin:test-before-defined\")");
        verify(logger).warn("fakeplugin:foo-bar is deprecated (required by \"fakeplugin:test-after-defined\")");
    }

    @Test
    public void whenDarkFeatureManagerNull_thenShouldFallbackToSystemProperty() {
        // Under test
        final Config config = createConfig();

        // Given
        when(webResourceIntegration.getDarkFeatureManager()).thenReturn(null);
        final String someFeatureFlag = "some-random-feature-flag";
        // we must test with true, because Boolean#getBoolean et al will default to false -> not helpful for testing
        final Boolean featureFlagOn = TRUE;
        System.setProperty(someFeatureFlag, featureFlagOn.toString());

        // When
        final boolean flagValue = config.getBooleanFromDarkFeatureManagerThenSystemProperty(someFeatureFlag);

        // Then
        assertThat(flagValue, equalTo(featureFlagOn));
    }

    @Test
    public void whenDarkFeatureManagerNull_thenShouldFallbackToAtlassianDarkfeaturePrefixedSystemProperty() {
        // Under test
        final Config config = createConfig();

        // Given
        when(webResourceIntegration.getDarkFeatureManager()).thenReturn(null);
        final String someFeatureFlag = "some-random-feature-flag";
        // we must test with true, because Boolean#getBoolean et al will default to false -> not helpful for testing
        final Boolean featureFlagOn = TRUE;
        System.setProperty(ATLASSIAN_DARKFEATURE_PREFIX + someFeatureFlag, featureFlagOn.toString());

        // When
        final boolean flagValue = config.getBooleanFromDarkFeatureManagerThenSystemProperty(someFeatureFlag);

        // Then
        assertThat(flagValue, equalTo(featureFlagOn));
    }

    @Test
    public void testBundleHashValidationEnabledFlag() {
        System.clearProperty(Config.ENABLE_BUNDLE_HASH_VALIDATION);
        boolean whenNoSysProp = Config.isBundleHashValidationEnabled();
        System.setProperty(Config.ENABLE_BUNDLE_HASH_VALIDATION, "beagles");
        boolean whenRandomString = Config.isBundleHashValidationEnabled();
        System.setProperty(Config.ENABLE_BUNDLE_HASH_VALIDATION, "TrUe");
        boolean whenTrueCaseInsensitive = Config.isBundleHashValidationEnabled();
        System.setProperty(Config.ENABLE_BUNDLE_HASH_VALIDATION, "false");
        boolean whenFalseCaseInsensitive = Config.isBundleHashValidationEnabled();
        System.clearProperty(Config.ENABLE_BUNDLE_HASH_VALIDATION);

        assertThat("Should be enabled by default", whenNoSysProp, is(true));
        assertThat("Should be enabled if not explicitly false", whenRandomString, is(true));
        assertThat("Should be disabled if explicitly set to false", whenFalseCaseInsensitive, is(false));
        assertThat("should be enabled if explicitly set to true", whenTrueCaseInsensitive, is(true));
    }

    private WebResourceModuleDescriptor makeDeprecatedWebResource(final String wrKey) {
        final String completeKey = plugin.getName() + ":" + wrKey;
        final Deprecation deprecation = mock(Deprecation.class);
        when(deprecation.buildLogMessage()).thenReturn(completeKey + " is deprecated");

        final WebResourceModuleDescriptor deprecatedWR = mock(WebResourceModuleDescriptor.class);
        when(deprecatedWR.getPlugin()).thenReturn(plugin);
        when(deprecatedWR.getKey()).thenReturn(wrKey);
        when(deprecatedWR.getCompleteKey()).thenReturn(completeKey);
        when(deprecatedWR.isDeprecated()).thenReturn(true);
        when(deprecatedWR.getDeprecation()).thenReturn(deprecation);
        return deprecatedWR;
    }

    private WebResourceModuleDescriptor makeWebResource(
            final String wrKey, @Nullable WebResourceModuleDescriptor dependsOn) {
        final String completeKey = plugin.getName() + ":" + wrKey;
        final List<String> deps = new ArrayList<>();
        if (dependsOn != null) {
            deps.add(dependsOn.getCompleteKey());
        }
        final WebResourceModuleDescriptor normalWR = mock(WebResourceModuleDescriptor.class);
        when(normalWR.getPlugin()).thenReturn(plugin);
        when(normalWR.getKey()).thenReturn(wrKey);
        when(normalWR.getCompleteKey()).thenReturn(completeKey);
        when(normalWR.isDeprecated()).thenReturn(false);
        when(normalWR.getDependencies()).thenReturn(deps);
        return normalWR;
    }

    private Config createConfig() {
        final Config config = new Config(
                resourceBatchingConfiguration,
                webResourceIntegration,
                webResourceUrlProvider,
                servletContextFactory,
                transformerCache);
        config.setLogger(logger);
        return config;
    }
}
