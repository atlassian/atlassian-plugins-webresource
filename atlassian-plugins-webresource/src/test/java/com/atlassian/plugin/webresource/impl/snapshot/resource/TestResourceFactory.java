package com.atlassian.plugin.webresource.impl.snapshot.resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream.PluginSharedHomeStreamStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream.PluginStreamStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream.TomcatStreamStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream.WebResourceStreamStrategy;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream.StreamStrategyFactory.WEB_CONTEXT_STATIC;

@RunWith(MockitoJUnitRunner.class)
public class TestResourceFactory {
    private static final String PLUGIN_KEY = "security.romulan.cloak.plugin";
    private static final String WEBRESOURCE_KEY = PLUGIN_KEY + ":web-resource";

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ServletContextFactory servletContextFactory;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private WebResourceIntegration webResourceIntegration;

    @Mock
    private Bundle bundle;

    @Mock
    private ResourceLocation resourceLocation;

    private ResourceFactory resourceFactory;

    @Before
    public void setUp() {

        resourceFactory = new ResourceFactory(servletContextFactory, webResourceIntegration);
    }

    @Test
    public void shouldAccessResourceViaDescriptorWhenContainsWebresourceKey() {
        when(bundle.getKey()).thenReturn(WEBRESOURCE_KEY);

        Resource resource = resourceFactory.createResource(bundle, resourceLocation, "", "");

        assertThat(resource.streamStrategy, instanceOf(WebResourceStreamStrategy.class));
    }

    @Test
    public void shouldAccessResourceViaPluginWhenContainsPluginKey() {
        when(bundle.getKey()).thenReturn(PLUGIN_KEY);
        Resource resource = resourceFactory.createResource(bundle, resourceLocation, "", "");

        assertThat(resource.streamStrategy, instanceOf(PluginStreamStrategy.class));
    }

    @Test
    public void shouldAccessResourceViaResourceContextWhenStaticContextParamIsEnabled() {
        when(resourceLocation.getParameter(Config.SOURCE_PARAM_NAME)).thenReturn(WEB_CONTEXT_STATIC);

        Resource resource = resourceFactory.createResource(bundle, resourceLocation, "", "");

        assertThat(resource.streamStrategy, instanceOf(TomcatStreamStrategy.class));
    }

    @Test
    public void shouldAccessWebResourcesResourceFromPluginSharedHomeWhenPluginSharedHomeParamIsEnabled() {
        when(bundle.getKey()).thenReturn(WEBRESOURCE_KEY);
        when(resourceLocation.getParameter(Config.SOURCE_PARAM_NAME)).thenReturn("pluginSharedHome");

        Resource resource = resourceFactory.createResource(bundle, resourceLocation, "", "");

        assertThat(resource.streamStrategy, instanceOf(PluginSharedHomeStreamStrategy.class));
    }
}
