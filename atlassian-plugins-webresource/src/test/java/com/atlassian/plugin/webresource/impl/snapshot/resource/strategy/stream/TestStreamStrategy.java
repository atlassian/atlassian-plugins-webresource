package com.atlassian.plugin.webresource.impl.snapshot.resource.strategy.stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestStreamStrategy {
    private static String PATH_TO_RESOURCE = "path/to/resource.js";
    private static String WEBRESOURCE_KEY = "plugin:web-resource";
    private static String PLUGIN_KEY = "plugin";

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ServletContextFactory servletContextFactory;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private WebResourceIntegration webResourceIntegration;

    @Mock
    private Bundle bundle;

    @Test
    public void shouldAccessResourceViaDescriptorWhenWebResourceStrategyIsUsed() {
        when(bundle.getKey()).thenReturn(WEBRESOURCE_KEY);
        StreamStrategy streamStrategy = new WebResourceStreamStrategy(webResourceIntegration, bundle);

        streamStrategy.getInputStream(PATH_TO_RESOURCE);

        verify(webResourceIntegration.getPluginAccessor()).getEnabledPluginModule(WEBRESOURCE_KEY);
        verify(webResourceIntegration
                        .getPluginAccessor()
                        .getEnabledPluginModule(WEBRESOURCE_KEY)
                        .getPlugin())
                .getResourceAsStream(PATH_TO_RESOURCE);
    }

    @Test
    public void shouldAccessResourceViaPluginWhenPluginStrategyIsUsed() {
        when(bundle.getKey()).thenReturn(PLUGIN_KEY);
        StreamStrategy streamStrategy = new PluginStreamStrategy(webResourceIntegration, bundle);

        streamStrategy.getInputStream(PATH_TO_RESOURCE);

        verify(webResourceIntegration.getPluginAccessor()).getEnabledPlugin(PLUGIN_KEY);
        verify(webResourceIntegration.getPluginAccessor().getEnabledPlugin(PLUGIN_KEY))
                .getResourceAsStream(PATH_TO_RESOURCE);
    }

    @Test
    public void shouldAccessResourceViaServletContextWhenTomcatStrategyIsUsed() {
        StreamStrategy streamStrategy = new TomcatStreamStrategy(servletContextFactory);

        streamStrategy.getInputStream(PATH_TO_RESOURCE);

        verify(servletContextFactory.getServletContext())
                .getResourceAsStream("/" + PATH_TO_RESOURCE); // Tomcat needs prefix "/"
    }
}
