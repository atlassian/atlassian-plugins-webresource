package com.atlassian.plugin.webresource.integration.special;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.stub.WebResource;

import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.superBatchUrl;
import static com.atlassian.plugin.webresource.util.DuplicateContentMatcher.doesNotDuplicateAnyContents;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INLINE;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE;

public class TestSyncbatchAcrossPhases extends TestCase {
    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    private WebResource.ConfigurationDsl cfg;

    @Before
    public void setUp() {
        System.setProperty("atlassian.darkfeature.atlassian.webresource.performance.tracking.disable", "true");

        cfg = wr.configure();
        cfg.plugin("test.atlassian")
                .webResource("first-feature")
                .context("first")
                .context("all")
                .resource("first-1.js")
                .resource("first-2.js")
                .resource("first-1.css")
                .resource("first-2.css")
                .webResource("second-feature")
                .context("second")
                .context("all")
                .resource("second-1.css")
                .resource("second-1.js")
                .resource("second-2.css")
                .resource("second-2.js")
                .webResource("third-feature")
                .context("third")
                .context("all")
                .dependency("test.atlassian:first-feature")
                .dependency("test.atlassian:second-feature")
                .resource("third-1.css")
                .resource("third-1.js")
                .end();
    }

    @Test
    public void when_EmptyMarkedSyncResources_then_NoInlineContentWritten() {
        cfg.addToSyncbatch(emptyList());
        cfg.end();

        wr.requireResource("test.atlassian:first-feature");
        final WebResource.DrainResult result = wr.drain();

        assertThat(
                result.html(),
                allOf(
                        matches("<link[^\\>]+?first-feature.css"),
                        not(matches("content of first-1.css", "content of first-2.css")),
                        matches("<script[^\\>]+?first-feature.js"),
                        not(matches("content of first-1.js", "content of first-2.js"))));
    }

    @Test
    public void when_NonEmptyMarkedSyncResources_then_InlineContentWritten() {
        cfg.addToSyncbatch("test.atlassian:first-feature");
        cfg.end();

        wr.requireResource("test.atlassian:first-feature");
        final WebResource.DrainResult result = wr.drain();

        assertThat(
                result.html(),
                allOf(
                        // todo PLUGWEB-545 add CSS support for inline phase
                        //                not(matches("<link[^\\>]+?first-feature.css")),
                        //                matches("content of first-1.css", "content of first-2.css"),
                        not(matches("<script[^\\>]+?first-feature.js")),
                        matches("content of first-1.js", "content of first-2.js")));
    }

    @Test
    public void when_NonEmptyMarkedSyncResourcesButNotExplicitlyRequired_then_InlineContentStillWritten() {
        cfg.addToSyncbatch("test.atlassian:first-feature");
        cfg.end();

        wr.requireResource("test.atlassian:second-feature");
        final WebResource.DrainResult result = wr.drain();

        assertThat(
                result.html(),
                allOf(
                        // todo PLUGWEB-545 add CSS support for inline phase
                        //                not(matches("<link[^\\>]+?first-feature.css")),
                        //                matches("content of first-1.css", "content of first-2.css"),
                        not(matches("<script[^\\>]+?first-feature.js")),
                        matches("content of first-1.js", "content of first-2.js"),
                        matches("<link[^\\>]+?second-feature.css"),
                        matches("<script[^\\>]+?second-feature.js")));
    }

    @Test
    public void when_MarkedSyncResourcesHaveTransitiveDependencies_then_AllTransitiveResourcesWrittenAsInlineContent() {
        cfg.addToSyncbatch("test.atlassian:third-feature");
        cfg.end();

        wr.requireResource("test.atlassian:third-feature");
        final WebResource.DrainResult result = wr.drain();

        assertThat(
                result.html(),
                allOf(
                        // todo PLUGWEB-545 add CSS support for inline phase
                        //                matches("content of first-1.css", "content of first-2.css", ...),
                        matches(
                                "content of first-1.js",
                                "content of first-2.js",
                                "content of second-1.js",
                                "content of second-2.js",
                                "content of third-1.js")));
    }

    @Test
    public void when_NonexistentResourcesMarkedAsSync_then_NoInlineContentWritten() {
        cfg.addToSyncbatch(
                "plugin-which-does-not-exist:web-resource-key",
                "test.atlassian:incorrect-key",
                "test.atlassian:also-nope");
        cfg.end();

        wr.requireResource("test.atlassian:second-feature");
        final WebResource.DrainResult result = wr.drain();

        assertThat(
                result.html(),
                allOf(
                        // todo PLUGWEB-545 add CSS support for inline phase
                        //                not(matches("<style>.+?</style>")),
                        not(matches("<script[^>]*?>.+?</script>"))));
    }

    @Test
    public void when_MarkedSyncResourcesAreEquivalentOfSuperbatch_then_NoSuperbatchWritten() {
        cfg.addToSyncbatch("test.atlassian:first-feature");
        cfg.addToSuperbatch("test.atlassian:first-feature");
        cfg.end();

        wr.requireResource("test.atlassian:first-feature");
        final WebResource.DrainResult result = wr.drain();

        assertThat(
                result.html(),
                allOf(
                        // todo PLUGWEB-545 add CSS support for inline phase
                        //                matches("content of first-1.css", "content of first-2.css", ...),
                        matches("content of first-1.js", "content of first-2.js"),
                        not(matches("<script[^\\>]+?first-feature.js")),
                        not(matches("<script[^\\>]+?super/batch.js"))));
    }

    @Test
    public void when_MarkedSyncResourcesAreSubsetOfSuperbatch_then_overlappingContentNotPresentInSuperbatch() {
        cfg.addToSyncbatch("test.atlassian:first-feature");
        cfg.addToSuperbatch("test.atlassian:third-feature");
        cfg.end();

        final String superbatchJsContent = wr.getContent(superBatchUrl("js"));
        assertThat(superbatchJsContent, not(matches("content of first-1.js")));

        final String superbatchCssContent = wr.getContent(superBatchUrl("css"));
        assertThat(superbatchCssContent, not(matches("content of first-1.css")));
    }

    @Test
    public void when_MarkedSyncResourcesAndSuperbatchDoNotOverlap_then_undefinedBehaviour() {
        cfg.addToSyncbatch("test.atlassian:first-feature");
        cfg.addToSuperbatch("test.atlassian:second-feature");
        cfg.end();

        wr.requireResource("test.atlassian:second-feature");

        final List<String> tags = wr.drain().tags();

        // The first tag is for the sync resource(s)
        assertThat(
                tags,
                contains(
                        startsWith("<script"),
                        // note: The URLs contain -_sync, but don't have to. Could be optimised.
                        allOf(startsWith("<link"), matches("-_sync"), matches("_super")),
                        allOf(startsWith("<script"), matches("-_sync"), matches("_super"))));
    }

    @Test
    public void given_emptySyncResources_when_otherContentRequested_then_syncNotSubtracted() {
        cfg.addToSyncbatch(emptyList());
        cfg.addToSuperbatch("test.atlassian:second-feature");
        cfg.end();

        wr.requireResource("test.atlassian:second-feature");

        final List<String> tags = wr.drain().tags();

        assertThat(
                tags,
                contains(
                        allOf(startsWith("<link"), not(matches("-_sync")), matches("_super")),
                        allOf(startsWith("<script"), not(matches("-_sync")), matches("_super"))));
    }

    @Test
    public void given_SyncResourcesContainsFirstFeature_when_secondFeatureRequestedInline_then_ContentNotDuplicated() {
        // given
        cfg.addToSyncbatch("test.atlassian:first-feature");
        cfg.end();

        // when
        wr.getPageBuilderService().assembler().resources().requireWebResource(INLINE, "test.atlassian:second-feature");
        wr.getPageBuilderService().assembler().resources().requireWebResource(REQUIRE, "test.atlassian:second-feature");
        wr.getPageBuilderService()
                .assembler()
                .resources()
                .requireWebResource(INTERACTION, "test.atlassian:second-feature");
        final String html = wr.drain().html();

        // then
        assertThat(
                html,
                allOf(
                        doesNotDuplicateAnyContents(),
                        containsString("content of first-1.js"),
                        containsString("content of first-2.js"),
                        containsString("content of second-1.js"),
                        containsString("content of second-2.js"),
                        containsString("test.atlassian:second-feature"),
                        not(containsString("WRM.requireLazily"))));
    }

    @Test
    public void given_SyncResourcesContainsSecondFeature_when_secondFeatureRequestedInline_then_ContentNotDuplicated() {
        // given
        cfg.addToSyncbatch("test.atlassian:second-feature");
        cfg.end();

        // when
        wr.getPageBuilderService().assembler().resources().requireWebResource(INLINE, "test.atlassian:second-feature");
        wr.getPageBuilderService().assembler().resources().requireWebResource(REQUIRE, "test.atlassian:second-feature");
        wr.getPageBuilderService()
                .assembler()
                .resources()
                .requireWebResource(INTERACTION, "test.atlassian:second-feature");
        final String html = wr.drain().html();

        // then
        assertThat(
                html,
                allOf(
                        doesNotDuplicateAnyContents(),
                        containsString("content of second-1.js"),
                        containsString("content of second-2.js"),
                        not(containsString("test.atlassian:second-feature")),
                        not(containsString("WRM.requireLazily"))));
    }

    @Test
    public void given_SyncResourcesContainsThirdFeature_when_secondFeatureRequestedInline_then_ContentNotDuplicated() {
        // given
        cfg.addToSyncbatch("test.atlassian:third-feature");
        cfg.end();

        // when
        wr.getPageBuilderService().assembler().resources().requireWebResource(INLINE, "test.atlassian:second-feature");
        wr.getPageBuilderService().assembler().resources().requireWebResource(REQUIRE, "test.atlassian:second-feature");
        wr.getPageBuilderService()
                .assembler()
                .resources()
                .requireWebResource(INTERACTION, "test.atlassian:second-feature");
        final String html = wr.drain().html();

        // then
        assertThat(
                html,
                allOf(
                        doesNotDuplicateAnyContents(),
                        containsString("content of second-1.js"),
                        containsString("content of second-2.js"),
                        not(containsString("test.atlassian:second-feature")),
                        not(containsString("WRM.requireLazily"))));
    }
}
