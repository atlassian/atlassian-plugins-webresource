package com.atlassian.plugin.webresource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

public class AlwaysTrueCondition implements UrlReadingCondition {
    public static List<String> log = new ArrayList<>();

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
        log.add("init");
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder) {
        log.add("addToUrl");
        urlBuilder.addToQueryString("always-true", "true");
    }

    @Override
    public boolean shouldDisplay(QueryParams params) {
        log.add("shouldDisplay");
        return "true".equals(params.get("always-true"));
    }
}
