package com.atlassian.plugin.webresource.impl.helpers;

import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.WebResource;
import com.atlassian.plugin.webresource.impl.snapshot.resource.Resource;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ResourceServingHelpersTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    StaticTransformers staticTransformers;

    @Mock
    Config config;

    @Mock
    Globals globals;

    @Mock
    WebResource parent;

    @Mock
    CachedTransformers cachedTransformers;

    @Mock
    Resource resource;

    @Before
    public void setUp() {
        when(config.getStaticTransformers()).thenReturn(staticTransformers);

        when(globals.getConfig()).thenReturn(config);

        when(parent.getTransformers()).thenReturn(cachedTransformers);

        when(resource.getKey()).thenReturn("resourceKey");
        when(resource.getPath()).thenReturn("resourcePath");
        when(resource.getLocationType()).thenReturn("desiredLocationType");
        lenient().when(resource.getNameType()).thenReturn("undesiredNameType");
        when(resource.getParent()).thenReturn(parent);
    }

    @Test
    public void buildKeyShouldUseResourceLocationTypeToFetchUsedQueryParametersFromTransformers() {
        ResourceServingHelpers.buildKey(globals, resource, Map.of());

        verify(cachedTransformers).addAllUsedQueryParameters(any(), eq("desiredLocationType"), any());
    }

    @Test
    public void buildKeyShouldAddAllQueryParametersIfTransformersDontSupportGetAllQueryParameters() {
        when(cachedTransformers.addAllUsedQueryParameters(any(), any(), any())).thenReturn(false);

        String key = ResourceServingHelpers.buildKey(globals, resource, Map.of("a", "b", "c", "d"));

        assertEquals("resourceKey:resourcePath?a=b&c=d", key);
    }

    @Test
    public void buildKeyShouldOnlyAddQueryParametersSelectedByTransformersThroughGetAllQueryParameters() {
        when(cachedTransformers.addAllUsedQueryParameters(any(), any(), any())).then(invocation -> {
            Set<String> queryParameters = invocation.getArgument(0);
            queryParameters.add("a");
            return true;
        });

        String key = ResourceServingHelpers.buildKey(globals, resource, Map.of("a", "b", "c", "d"));

        assertEquals("resourceKey:resourcePath?a=b", key);
    }
}
