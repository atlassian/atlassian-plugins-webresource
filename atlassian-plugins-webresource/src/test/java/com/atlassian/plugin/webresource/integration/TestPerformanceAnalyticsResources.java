package com.atlassian.plugin.webresource.integration;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import static java.lang.Boolean.FALSE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.mockito.Mockito.doReturn;

import static com.atlassian.plugin.webresource.impl.config.Config.DISABLE_PERFORMANCE_TRACKING;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;

public class TestPerformanceAnalyticsResources extends TestCase {

    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    @Before
    public void setUp() throws Exception {
        System.setProperty(DISABLE_PERFORMANCE_TRACKING, FALSE.toString());

        wr.configure(config -> {
                    doReturn(true).when(config).isPerformanceTrackingEnabled();
                    doReturn(false).when(config).isContextBatchingEnabled();
                    doReturn(false).when(config).isSuperBatchingEnabled();
                    doReturn(false).when(config).isWebResourceBatchingEnabled();
                })

                // some random plugin features
                .plugin("plugin")
                .webResource("first-feature")
                .resource("first-feature.js")
                .webResource("second-feature")
                .resource("second-feature.js")
                .webResource("third-feature")
                .resource("third-feature.js")

                // the system web-resources
                .plugin("com.atlassian.plugins.atlassian-plugins-webresource-rest")
                .webResource("web-resource-manager")
                .resource("wrm.js")
                .webResource("data-collector-perf-observer")
                .resource("perf-observer.js")
                .webResource("data-collector-async")
                .resource("perf-collector.js")
                .end();
    }

    @Test
    public void given_featureEnabled_when_loadingNoExplicitRequires_then_shouldLoadAnalyticsModules() {
        // when
        final String result = wr.pathsAsHtml();

        // then
        assertThat(
                result,
                stringContainsInOrder(
                        "content of perf-observer.js",
                        resourceUrl(
                                "com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager",
                                "wrm.js"),
                        "WRM.requireLazily([\"wr!com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async\"])"));
    }

    @Test
    public void given_featureEnabled_requireAnything_when_loadingAnything_then_shouldLoadAnalyticsModules() {
        // when
        wr.requireResource("plugin:first-feature");
        final String result = wr.pathsAsHtml();

        // then
        assertThat(
                result,
                stringContainsInOrder(
                        "content of perf-observer.js",
                        resourceUrl(
                                "com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager",
                                "wrm.js"),
                        resourceUrl("plugin:first-feature", "first-feature.js"),
                        "WRM.requireLazily([\"wr!com.atlassian.plugins.atlassian-plugins-webresource-rest:data-collector-async\"])"));
    }
}
