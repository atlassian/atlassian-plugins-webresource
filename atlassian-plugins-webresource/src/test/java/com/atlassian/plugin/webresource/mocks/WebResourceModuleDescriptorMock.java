package com.atlassian.plugin.webresource.mocks;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;

import static java.util.Objects.requireNonNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WebResourceModuleDescriptorMock {
    private final WebResourceModuleDescriptor webResourceModuleDescriptor;

    @JsonCreator
    public WebResourceModuleDescriptorMock(
            @JsonProperty("completeKey") @Nonnull final String completeKey,
            @JsonProperty("contexts") @Nullable final Set<String> contexts,
            @JsonProperty("contextDependencies") @Nonnull final Set<String> contextDependencies,
            @JsonProperty("dependencies") @Nonnull final List<String> dependencies,
            @JsonProperty("rootPage") final boolean isRootPage) {
        requireNonNull(completeKey, "The plugin complete key is mandatory.");
        requireNonNull(contextDependencies, "The plugin context dependencies are mandatory.");
        requireNonNull(dependencies, "The plugin dependencies are mandatory.");

        final Set<String> effectiveContexts = new HashSet<>();
        /**
         * PLUGWEB-59 - All web-resources are also implicitly contexts.
         * @see WebResourceModuleDescriptor#init(com.atlassian.plugin.Plugin, org.dom4j.Element)
         */
        effectiveContexts.add(completeKey);
        if (contexts != null) {
            effectiveContexts.addAll(contexts);
        }

        webResourceModuleDescriptor = mock(WebResourceModuleDescriptor.class);
        when(webResourceModuleDescriptor.getCompleteKey()).thenReturn(completeKey);
        when(webResourceModuleDescriptor.getContextDependencies()).thenReturn(contextDependencies);
        when(webResourceModuleDescriptor.getDependencies()).thenReturn(dependencies);
        when(webResourceModuleDescriptor.getContexts()).thenReturn(effectiveContexts);
        when(webResourceModuleDescriptor.isRootPage()).thenReturn(isRootPage);
    }

    @Nonnull
    public WebResourceModuleDescriptor toWebResourceModuleDescriptor() {
        return webResourceModuleDescriptor;
    }
}
