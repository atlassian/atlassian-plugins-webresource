package com.atlassian.plugin.webresource.impl.http;

import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;

import com.atlassian.plugin.cache.filecache.Cache;
import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.Globals;

import static org.mockito.ArgumentMatchers.endsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.webresource.TestUtils.createGlobals;
import static com.atlassian.plugin.webresource.TestUtils.createWebResourceIntegration;
import static com.atlassian.plugin.webresource.TestUtils.setField;

public class AbstractResponseCachingTest {
    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Captor
    public ArgumentCaptor<String> cacheKeyCaptor;

    protected Cache cache;
    protected Globals globals;

    @Before
    public void setup() throws Exception {
        cache = mock(Cache.class);
        globals = createGlobalMess(cache);
    }

    // The mess was here before I was. I'm doing what I can to clean it up, or at least pile it neatly in the corner.
    private Globals createGlobalMess(Cache cache) throws IOException {
        final ContentTypeResolver resolver = mock(ContentTypeResolver.class);
        when(resolver.getContentType(endsWith("js"))).thenReturn("js");
        when(resolver.getContentType(endsWith("css"))).thenReturn("css");

        final WebResourceIntegration integration = createWebResourceIntegration();
        when(integration.getTemporaryDirectory()).thenReturn(tempFolder.newFolder("router-test"));

        final Globals globals = createGlobals(integration, Router.class);
        globals.getConfig().setContentTypeResolver(resolver);

        if (cache != null) {
            setField(globals, "contentCache", cache);
        }

        return globals;
    }
}
