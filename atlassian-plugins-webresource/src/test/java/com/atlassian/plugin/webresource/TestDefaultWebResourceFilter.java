package com.atlassian.plugin.webresource;

import junit.framework.TestCase;

public class TestDefaultWebResourceFilter extends TestCase {
    public void testMatches() {
        DefaultWebResourceFilter filter = new DefaultWebResourceFilter();
        assertTrue("should match css files", filter.matches("foo.css"));
        assertTrue("should match javascript files", filter.matches("foo.js"));
        assertFalse("should not match html files", filter.matches("foo.html"));
        assertFalse("should not match soy files", filter.matches("foo.soy"));
    }
}
