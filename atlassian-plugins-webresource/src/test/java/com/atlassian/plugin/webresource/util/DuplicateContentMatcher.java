package com.atlassian.plugin.webresource.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static java.util.Arrays.asList;

public class DuplicateContentMatcher extends TypeSafeMatcher<String> {
    private final Set<String> ignore = new HashSet<>();
    private final Predicate<String> lineNotIgnored = ((Predicate<String>) ignore::contains).negate();

    public DuplicateContentMatcher(final String... ignoreLines) {
        ignore.addAll(asList(ignoreLines));
    }

    @Override
    protected boolean matchesSafely(final String s) {
        final List<String> lines = Arrays.stream(s.split("\n+"))
                .map(StringUtils::trim)
                .filter(StringUtils::isNotBlank)
                .filter(lineNotIgnored)
                .collect(Collectors.toList());
        final List<String> uniqueLines = lines.stream().distinct().collect(Collectors.toList());
        return lines.size() == uniqueLines.size();
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("contains no duplicate lines (separated by \\n, ignoring: " + ignore + ")");
    }

    public static Matcher<String> doesNotDuplicateAnyContents() {
        return new DuplicateContentMatcher(";");
    }

    public static Matcher<String> doesNotDuplicateSuperbatchContents() {
        return doesNotDuplicateAnyContents();
    }
}
