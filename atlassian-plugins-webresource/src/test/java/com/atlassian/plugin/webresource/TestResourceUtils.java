package com.atlassian.plugin.webresource;

import junit.framework.TestCase;

import com.atlassian.plugin.webresource.impl.http.Router;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;

public class TestResourceUtils extends TestCase {
    public void testGetType() {
        assertEquals("css", ResourceUtils.getType("/foo.css"));
        assertEquals("js", ResourceUtils.getType("/superbatch/js/foo.js"));
        assertEquals("", ResourceUtils.getType("/superbatch/js/foo."));
        assertEquals("", ResourceUtils.getType("/superbatch/js/foo"));
    }

    public void testAddParamsToUrlPreservesOrder() {
        assertEquals("?aa=1&bb=1&cc=1&dd=1", Router.buildUrl("", buildMap("dd", "1", "bb", "1", "cc", "1", "aa", "1")));
    }
}
