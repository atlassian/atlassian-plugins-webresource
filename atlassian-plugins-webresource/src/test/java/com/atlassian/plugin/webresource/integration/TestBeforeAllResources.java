package com.atlassian.plugin.webresource.integration;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import com.atlassian.plugin.webresource.integration.stub.WebResource;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Mockito.doReturn;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.superBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;

public class TestBeforeAllResources extends TestCase {

    @Rule
    public final TestRule restoreSystemPropertiesRule = new RestoreSystemProperties();

    @Before
    public void setUp() throws Exception {
        System.setProperty("atlassian.darkfeature.atlassian.webresource.performance.tracking.disable", "true");
    }

    protected void prepare(WebResource.ConfigCallback configCallback) {
        wr.configure(config -> {
                    doReturn(asList("plugin:before-all")).when(config).getBeforeAllResources();
                    doReturn(true).when(config).isSuperBatchingEnabled();
                    configCallback.apply(config);
                })
                .plugin("plugin")
                .webResource("before-all")
                .resource("before-all.js")
                .webResource("a")
                .context("context-a")
                .resource("a1.js")
                .webResource("b")
                .context("context-b")
                .resource("b1.js")
                .end();
    }

    @Test
    public void shouldSupplyBeforeAllResourcesForContextBatch() {
        prepare(config -> {});

        wr.requireContext("context-a");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(superBatchUrl("js"), contextBatchUrl("context-a,-_super", "js")));
        assertThat(wr.getContent(wr.lastDrain()), matches("content of before-all.js", "content of a1.js"));

        wr.requireContext("context-b");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(contextBatchUrl("context-b,-_super", "js")));
        assertThat(
                wr.getContent(wr.lastDrain()),
                allOf(not(matches("content of before-all.js")), matches("content of b1.js")));
    }

    @Test
    public void shouldSupplyBeforeAllResourcesForWebResourceBatch() {
        prepare(config -> {
            doReturn(false).when(config).isContextBatchingEnabled();
        });

        wr.requireContext("context-a");
        wr.drain();

        assertThat(
                wr.lastDrain().urls(),
                matches(webResourceBatchUrl("plugin:before-all", "js"), webResourceBatchUrl("plugin:a", "js")));
        assertThat(wr.getContent(wr.lastDrain()), matches("content of before-all.js", "content of a1.js"));

        wr.requireContext("context-b");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(webResourceBatchUrl("plugin:b", "js")));
        assertThat(
                wr.getContent(wr.lastDrain()),
                allOf(not(matches("content of before-all.js")), matches("content of b1.js")));
    }

    @Test
    public void shouldSupplyBeforeAllResourcesWithoutBatching() {
        prepare(config -> {
            doReturn(false).when(config).isContextBatchingEnabled();
            doReturn(false).when(config).isWebResourceBatchingEnabled();
        });

        wr.requireContext("context-a");
        wr.drain();

        assertThat(
                wr.lastDrain().urls(),
                matches(resourceUrl("plugin:before-all", "before-all.js"), resourceUrl("plugin:a", "a1.js")));

        wr.requireContext("context-b");
        wr.drain();

        assertThat(wr.lastDrain().urls(), matches(resourceUrl("plugin:b", "b1.js")));
    }
}
