package com.atlassian.plugin.webresource.integration.content;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.fixtures.ConditionalDiamond;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.mockito.Mockito.doReturn;

import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INLINE;

@RunWith(Parameterized.class)
public class TestContentInclusionViaInlinePhase extends TestCase {
    private final boolean contextBatchingEnabled;
    private final boolean webresourceBatchingEnabled;

    private WebResourceAssembler assembler;

    public TestContentInclusionViaInlinePhase(
            final boolean contextBatchingEnabled, final boolean webresourceBatchingEnabled) {
        this.contextBatchingEnabled = contextBatchingEnabled;
        this.webresourceBatchingEnabled = webresourceBatchingEnabled;
    }

    @Parameterized.Parameters(name = "context-batching: {0}, webresource-batching: {1}")
    public static Collection<Object[]> configure() {
        return asList(new Object[][] {
            {true, true},
            {true, false},
            {false, true},
            {false, false}
        });
    }

    @Before
    public void setUp() throws Exception {
        ConditionalDiamond.configure(wr, config -> {
            doReturn(contextBatchingEnabled).when(config).isContextBatchingEnabled();
            doReturn(webresourceBatchingEnabled).when(config).isWebResourceBatchingEnabled();
            doReturn(false).when(config).isSuperBatchingEnabled();
            doReturn(false).when(config).isPerformanceTrackingEnabled();
        });
        assembler = wr.getPageBuilderService().assembler();
    }

    @Test
    public void when_featureBLoadedInline_then_noUrlsGenerated() {
        // when
        assembler.resources().requireWebResource(INLINE, "plugin.key:feature-b");
        final WebResource.DrainResult result = wr.drain();

        // then
        assertThat(
                result.html(),
                stringContainsInOrder("<script", ">", "content of lib.js", "content of feature-b.js", "</script>"));
        assertThat(result.urls(), emptyCollectionOf(String.class));
    }

    @Test
    public void given_featureAEnabled_when_featureALoadedInline_then_contentLoadsInline() {
        // given
        ConditionalDiamond.setConditionFeatureA(true);

        // when
        assembler.resources().requireWebResource(INLINE, "plugin.key:feature-a");
        final WebResource.DrainResult result = wr.drain();

        // then
        assertThat(
                result.html(),
                stringContainsInOrder("<script", ">", "content of lib.js", "content of feature-a.js", "</script>"));
        assertThat(result.urls(), emptyCollectionOf(String.class));
    }

    @Test
    public void given_featureADisabled_when_featureALoadedInline_then_noContentLoadsInline() {
        // given
        ConditionalDiamond.setConditionFeatureA(false);

        // when
        assembler.resources().requireWebResource(INLINE, "plugin.key:feature-a");
        final WebResource.DrainResult result = wr.drain();

        // then
        assertThat(result.html(), not(containsString("<script")));
        assertThat(result.urls(), emptyCollectionOf(String.class));
    }
}
