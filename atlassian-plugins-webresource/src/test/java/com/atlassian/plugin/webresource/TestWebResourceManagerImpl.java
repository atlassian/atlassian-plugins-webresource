package com.atlassian.plugin.webresource;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;

import org.dom4j.DocumentException;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import junit.framework.TestCase;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.webresource.integration.stub.WebResourceIntegrationImpl;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.WebResourceFilter;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.webresource.TestUtils.createNamedHashes;
import static com.atlassian.plugin.webresource.TestUtils.createWebResourceTransformation;
import static com.atlassian.plugin.webresource.TestUtils.removeWebResourceLogs;
import static com.atlassian.plugin.webresource.TestUtils.setupSuperBatch;

public class TestWebResourceManagerImpl extends TestCase {
    private static final String BASEURL = "http://www.foo.com";
    private static final String SYSTEM_COUNTER = "123";
    private static final String SYSTEM_BUILD_NUMBER = "650";
    private static final String BATCH_URL_PREFIX = "/download/batch";
    private final WebResourceUrlProvider mockUrlProvider = mock(WebResourceUrlProvider.class);
    private WebResourceIntegration mockWebResourceIntegration;
    private PluginAccessor mockPluginAccessor = mock(PluginAccessor.class);
    private ResourceBatchingConfiguration mockBatchingConfiguration = mock(ResourceBatchingConfiguration.class);
    private PluginResourceLocatorImpl pluginResourceLocator;
    private WebResourceManagerImpl webResourceManager;
    private PluginEventManager mockEventPublisher = mock(PluginEventManager.class);
    private Plugin testPlugin;
    private List<WebResourceModuleDescriptor> listOfModuleDescriptors;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        mockWebResourceIntegration = WebResourceIntegrationImpl.create();
        when(mockWebResourceIntegration.getPluginAccessor()).thenReturn(mockPluginAccessor);
        when(mockWebResourceIntegration.getSuperBatchVersion()).thenReturn(SYSTEM_BUILD_NUMBER);
        when(mockBatchingConfiguration.isSuperBatchingEnabled()).thenReturn(true);
        when(mockBatchingConfiguration.isPluginWebResourceBatchingEnabled()).thenReturn(true);

        pluginResourceLocator = new PluginResourceLocatorImpl(
                mockWebResourceIntegration, null, mockUrlProvider, mockBatchingConfiguration, mockEventPublisher);
        webResourceManager = new WebResourceManagerImpl(
                pluginResourceLocator.getGlobals(),
                mockWebResourceIntegration,
                mockUrlProvider,
                mockBatchingConfiguration);

        when(mockUrlProvider.getBaseUrl(UrlMode.ABSOLUTE)).thenReturn(BASEURL);
        when(mockUrlProvider.getBaseUrl(UrlMode.AUTO)).thenReturn("");
        when(mockUrlProvider.getBaseUrl(UrlMode.RELATIVE)).thenReturn("");

        listOfModuleDescriptors = new ArrayList<>();
        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
                .thenReturn(listOfModuleDescriptors);

        testPlugin = TestUtils.createTestPlugin();
    }

    @Override
    protected void tearDown() throws Exception {
        webResourceManager = null;
        mockPluginAccessor = null;
        mockWebResourceIntegration = null;
        testPlugin = null;
        mockBatchingConfiguration = null;

        super.tearDown();
    }

    private void mockEnabledPluginModule(final String key, final WebResourceModuleDescriptor md) {
        when(mockPluginAccessor.getEnabledPluginModule(key)).thenReturn((ModuleDescriptor) md);
        listOfModuleDescriptors.add(md);
    }

    public void testRequireResources() throws ClassNotFoundException {
        final String resource1 = "test.atlassian:cool-stuff";
        final String resource2 = "test.atlassian:hot-stuff";
        final Plugin plugin = TestUtils.createTestPlugin("test.atlassian", "1");

        mockEnabledPluginModule(
                resource1,
                new WebResourceModuleDescriptorBuilder(plugin, "cool-stuff")
                        .addDescriptor("cool.js")
                        .build());
        mockEnabledPluginModule(
                resource2,
                new WebResourceModuleDescriptorBuilder(plugin, "hot-stuff")
                        .addDescriptor("hot.js")
                        .build());

        setupRequestCache();
        webResourceManager.requireResource(resource1);
        webResourceManager.requireResource(resource2);
        webResourceManager.requireResource(resource1); // require again to test it only gets included once

        assertRequiredResources(resource1 + ".js", resource2 + ".js");
    }

    public void testRequireResourcesWithCondition() throws ClassNotFoundException {
        final String resource1 = "test.atlassian:cool-stuff";
        final String resource2 = "test.atlassian:hot-stuff";
        final Plugin plugin = TestUtils.createTestPlugin(
                "test.atlassian", "1", AlwaysTrueCondition.class, AlwaysFalseCondition.class);

        mockEnabledPluginModule(
                resource1,
                new WebResourceModuleDescriptorBuilder(plugin, "cool-stuff")
                        .setCondition(AlwaysTrueCondition.class)
                        .addDescriptor("cool.js")
                        .build());
        mockEnabledPluginModule(
                resource2,
                new WebResourceModuleDescriptorBuilder(plugin, "hot-stuff")
                        .setCondition(AlwaysFalseCondition.class)
                        .addDescriptor("hot.js")
                        .build());

        setupRequestCache();
        webResourceManager.requireResource(resource1);
        webResourceManager.requireResource(resource2);

        final String tags = removeWebResourceLogs(webResourceManager.getRequiredResources(UrlMode.AUTO));
        assertTrue(tags.contains(resource1));
        assertFalse(tags.contains(resource2));
    }

    public void testRequireResourcesWithUrlReadingCondition() throws ClassNotFoundException {
        final String resource1 = "test.atlassian:cool-stuff";
        final String resource2 = "test.atlassian:hot-stuff";
        final Plugin plugin = TestUtils.createTestPlugin(
                "test.atlassian", "1", AlwaysTrueCondition.class, AlwaysFalseCondition.class);

        mockEnabledPluginModule(
                resource1,
                new WebResourceModuleDescriptorBuilder(plugin, "cool-stuff")
                        .setCondition(AlwaysTrueCondition.class)
                        .addDescriptor("cool.js")
                        .build());
        mockEnabledPluginModule(
                resource2,
                new WebResourceModuleDescriptorBuilder(plugin, "hot-stuff")
                        .setCondition(AlwaysFalseCondition.class)
                        .addDescriptor("hot.js")
                        .build());

        setupRequestCache();
        webResourceManager.requireResource(resource1);
        webResourceManager.requireResource(resource2);

        final String tags = webResourceManager.getRequiredResources(UrlMode.AUTO);
        assertThat(tags, containsString(resource1));
        assertThat(tags, not(containsString(resource2)));
    }

    public void testRequireResourcesWithDependencies() {
        final String resource = "test.atlassian:cool-stuff";
        final String dependencyResource = "test.atlassian:hot-stuff";
        final Plugin plugin = TestUtils.createTestPlugin(
                "test.atlassian", "1", AlwaysTrueCondition.class, AlwaysFalseCondition.class);

        // cool-stuff depends on hot-stuff
        mockEnabledPluginModule(
                resource,
                new WebResourceModuleDescriptorBuilder(plugin, "cool-stuff")
                        .addDependency("test.atlassian:hot-stuff")
                        .addDescriptor("cool.js")
                        .build());
        mockEnabledPluginModule(
                dependencyResource,
                new WebResourceModuleDescriptorBuilder(plugin, "hot-stuff")
                        .addDescriptor("hot.js")
                        .build());

        setupRequestCache();
        webResourceManager.requireResource(resource);

        assertRequiredResources(dependencyResource + ".js", resource + ".js");
    }

    public void testRequireResourcesWithDependencyHiddenByCondition() throws ClassNotFoundException {
        final String resource1 = "test.atlassian:cool-stuff";
        final String resource2 = "test.atlassian:hot-stuff";
        final Plugin plugin = TestUtils.createTestPlugin(
                "test.atlassian", "1", AlwaysTrueCondition.class, AlwaysFalseCondition.class);

        mockEnabledPluginModule(
                resource1,
                new WebResourceModuleDescriptorBuilder(plugin, "cool-stuff")
                        .setCondition(AlwaysTrueCondition.class)
                        .addDependency(resource2)
                        .addDescriptor("cool.js")
                        .build());
        mockEnabledPluginModule(
                resource2,
                new WebResourceModuleDescriptorBuilder(plugin, "hot-stuff")
                        .setCondition(AlwaysFalseCondition.class)
                        .addDescriptor("hot.js")
                        .build());

        setupRequestCache();
        webResourceManager.requireResource(resource1);

        final String tags = removeWebResourceLogs(webResourceManager.getRequiredResources(UrlMode.AUTO));
        assertTrue(tags.contains(resource1));
        assertFalse(tags.contains(resource2));
    }

    public void testRequireResourcesRequiredAsDependencyButWithFalseCondition() throws ClassNotFoundException {
        final String resource1 = "test.atlassian:cool-stuff";
        final String resource2 = "test.atlassian:hot-stuff";
        final Plugin plugin = TestUtils.createTestPlugin(
                "test.atlassian", "1", AlwaysTrueCondition.class, AlwaysFalseCondition.class);

        mockEnabledPluginModule(
                resource1,
                new WebResourceModuleDescriptorBuilder(plugin, "cool-stuff")
                        .setCondition(AlwaysTrueCondition.class)
                        .addDependency(resource2)
                        .addDescriptor("cool.js")
                        .build());
        mockEnabledPluginModule(
                resource2,
                new WebResourceModuleDescriptorBuilder(plugin, "hot-stuff")
                        .setCondition(AlwaysTrueCondition.class)
                        .addDescriptor("hot.js")
                        .build());

        setupRequestCache();
        webResourceManager.requireResource(resource1);

        final String tags = webResourceManager.getRequiredResources(UrlMode.AUTO);
        assertTrue(tags.contains(resource1));
        assertTrue(tags.contains(resource2));
    }

    public void testRequireResourcesShouldRenderUnconditionalResourcesFirst() throws ClassNotFoundException {
        final String resource1 = "test.atlassian:cool-stuff";
        final String resource2 = "test.atlassian:hot-stuff";
        final Plugin plugin = TestUtils.createTestPlugin(
                "test.atlassian", "1", AlwaysTrueCondition.class, AlwaysFalseCondition.class);

        mockEnabledPluginModule(
                resource1,
                new WebResourceModuleDescriptorBuilder(plugin, "cool-stuff")
                        .setCondition(AlwaysTrueCondition.class)
                        .addDependency(resource2)
                        .addDescriptor("cool.js")
                        .build());
        mockEnabledPluginModule(
                resource2,
                new WebResourceModuleDescriptorBuilder(plugin, "hot-stuff")
                        .setCondition(AlwaysFalseCondition.class)
                        .addDescriptor("hot.js")
                        .build());

        setupRequestCache();
        webResourceManager.requireResource(resource1);

        final String tags = removeWebResourceLogs(webResourceManager.getRequiredResources(UrlMode.AUTO));
        assertTrue(tags.contains(resource1));
        assertFalse(tags.contains(resource2));
    }

    public void testRequireResourcesWithCyclicDependency() throws ClassNotFoundException {
        final String resource1 = "test.atlassian:cool-stuff";
        final String resource2 = "test.atlassian:hot-stuff";
        final Plugin plugin = TestUtils.createTestPlugin("test.atlassian", "1");

        // cool-stuff and hot-stuff depend on each other
        mockEnabledPluginModule(
                resource1,
                new WebResourceModuleDescriptorBuilder(plugin, "cool-stuff")
                        .addDependency(resource2)
                        .addDescriptor("hot-stuff.js")
                        .build());
        mockEnabledPluginModule(
                resource2,
                new WebResourceModuleDescriptorBuilder(plugin, "hot-stuff")
                        .addDependency(resource1)
                        .addDescriptor("cool-stuff.js")
                        .build());

        setupRequestCache();
        webResourceManager.requireResource(resource1);

        assertRequiredResources(resource2 + ".js", resource1 + ".js");
    }

    public void testRequireResourcesWithComplexCyclicDependency() throws ClassNotFoundException {
        final String resourceA = "test.atlassian:a";
        final String resourceB = "test.atlassian:b";
        final String resourceC = "test.atlassian:c";
        final String resourceD = "test.atlassian:d";
        final String resourceE = "test.atlassian:e";
        final Plugin plugin = TestUtils.createTestPlugin("test.atlassian", "1");

        // A depends on B, C
        mockEnabledPluginModule(
                resourceA,
                new WebResourceModuleDescriptorBuilder(plugin, "a")
                        .addDependency(resourceB)
                        .addDependency(resourceC)
                        .addDescriptor("a.js")
                        .build());
        // B depends on D
        mockEnabledPluginModule(
                resourceB,
                new WebResourceModuleDescriptorBuilder(plugin, "b")
                        .addDependency(resourceD)
                        .addDescriptor("b.js")
                        .build());
        // C has no dependencies
        mockEnabledPluginModule(
                resourceC,
                new WebResourceModuleDescriptorBuilder(plugin, "c")
                        .addDescriptor("c.js")
                        .build());
        // D depends on E, A (cyclic dependency)
        mockEnabledPluginModule(
                resourceD,
                new WebResourceModuleDescriptorBuilder(plugin, "d")
                        .addDependency(resourceE)
                        .addDependency(resourceA)
                        .addDescriptor("d.js")
                        .build());
        // E has no dependencies
        mockEnabledPluginModule(
                resourceE,
                new WebResourceModuleDescriptorBuilder(plugin, "e")
                        .addDescriptor("e.js")
                        .build());

        setupRequestCache();
        webResourceManager.requireResource(resourceA);
        // requiring a resource already included by A's dependencies shouldn't change the order
        webResourceManager.requireResource(resourceD);

        assertRequiredResources(
                resourceE + ".js", resourceD + ".js", resourceB + ".js", resourceC + ".js", resourceA + ".js");
    }

    public void testGetResourceContext() throws Exception {
        when(mockBatchingConfiguration.isContextBatchingEnabled()).thenReturn(true);
        final String resourceA = "test.atlassian:a";
        final String resourceB = "test.atlassian:b";
        final String resourceC = "test.atlassian:c";

        final WebResourceModuleDescriptor descriptor1 = TestUtils.createWebResourceModuleDescriptor(
                resourceA, testPlugin, TestUtils.createResourceDescriptors("resourceA.css"), emptyList(), emptySet());
        final WebResourceModuleDescriptor descriptor2 = TestUtils.createWebResourceModuleDescriptor(
                resourceB,
                testPlugin,
                TestUtils.createResourceDescriptors("resourceB.css"),
                emptyList(),
                new HashSet<String>() {
                    {
                        add("foo");
                    }
                });
        final WebResourceModuleDescriptor descriptor3 = TestUtils.createWebResourceModuleDescriptor(
                resourceC,
                testPlugin,
                TestUtils.createResourceDescriptors("resourceC.css"),
                emptyList(),
                new HashSet<String>() {
                    {
                        add("foo");
                        add("bar");
                    }
                });

        final List<WebResourceModuleDescriptor> descriptors = asList(descriptor1, descriptor2, descriptor3);

        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
                .thenReturn(descriptors);
        mockEnabledPluginModule(resourceA, descriptor1);
        mockEnabledPluginModule(resourceB, descriptor2);
        mockEnabledPluginModule(resourceC, descriptor3);

        setupRequestCache();

        // write includes for all resources for "foo":
        webResourceManager.requireResourcesForContext("foo");
        StringWriter writer = new StringWriter();
        webResourceManager.includeResources(writer, UrlMode.AUTO);
        String resources = writer.toString();
        assertFalse(resources.contains(resourceA + ".css"));
        assertFalse(resources.contains(resourceB + ".css"));
        assertFalse(resources.contains(resourceC + ".css"));
        assertTrue(resources.contains("/contextbatch/css/foo/batch.css"));
        assertFalse(resources.contains("/contextbatch/css/bar/batch.css"));

        // write includes for all resources for "bar":
        webResourceManager.requireResourcesForContext("bar");
        writer = new StringWriter();
        webResourceManager.includeResources(writer, UrlMode.AUTO);
        resources = writer.toString();
        assertFalse(resources.contains(resourceA + ".css"));
        assertFalse(resources.contains(resourceB + ".css"));
        assertFalse(resources.contains(resourceC + ".css"));
        assertFalse(resources.contains("/contextbatch/css/foo/batch.css"));
        assertTrue(resources.contains("/contextbatch/css/bar/batch.css"));
    }

    public void testGetResourceContextWithCondition() throws ClassNotFoundException, DocumentException {
        when(mockBatchingConfiguration.isContextBatchingEnabled()).thenReturn(true);

        final String resource1 = "test.atlassian:cool-stuff";
        final String resource2 = "test.atlassian:hot-stuff";
        final Plugin plugin = TestUtils.createTestPlugin(
                "test.atlassian", "1", AlwaysTrueCondition.class, AlwaysFalseCondition.class);
        final WebResourceModuleDescriptor resource1Descriptor = new WebResourceModuleDescriptorBuilder(
                        plugin, "cool-stuff")
                .setCondition(AlwaysTrueCondition.class)
                .addDescriptor("cool.js")
                .addContext("foo")
                .build();
        mockEnabledPluginModule(resource1, resource1Descriptor);

        final WebResourceModuleDescriptor resource2Descriptor = new WebResourceModuleDescriptorBuilder(
                        plugin, "hot-stuff")
                .setCondition(AlwaysFalseCondition.class)
                .addDescriptor("hot.js")
                .addContext("foo")
                .build();
        mockEnabledPluginModule(resource2, resource2Descriptor);

        final String resource3 = "test.atlassian:warm-stuff";
        final WebResourceModuleDescriptor resourceDescriptor3 = TestUtils.createWebResourceModuleDescriptor(
                resource3,
                testPlugin,
                TestUtils.createResourceDescriptors("warm.js"),
                emptyList(),
                new HashSet<String>() {
                    {
                        add("foo");
                    }
                });
        mockEnabledPluginModule(resource3, resourceDescriptor3);

        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
                .thenReturn(asList(resource1Descriptor, resource2Descriptor, resourceDescriptor3));

        setupRequestCache();
        webResourceManager.requireResourcesForContext("foo");

        final String tags = removeWebResourceLogs(webResourceManager.getRequiredResources(UrlMode.AUTO));
        assertFalse(tags.contains(resource1));
        assertFalse(tags.contains(resource2));
        assertTrue(tags.contains("foo/batch.js"));
    }

    public void testGetResourceContextWithUrlReadingCondition() throws ClassNotFoundException, DocumentException {
        when(mockBatchingConfiguration.isContextBatchingEnabled()).thenReturn(true);

        final String resource1 = "test.atlassian:cool-stuff";
        final String resource2 = "test.atlassian:hot-stuff";
        final Plugin plugin = TestUtils.createTestPlugin(
                "test.atlassian", "1", AlwaysTrueCondition.class, AlwaysFalseCondition.class);
        final WebResourceModuleDescriptor resource1Descriptor = new WebResourceModuleDescriptorBuilder(
                        plugin, "cool-stuff")
                .setCondition(AlwaysTrueCondition.class)
                .addDescriptor("cool.js")
                .addContext("foo")
                .build();
        mockEnabledPluginModule(resource1, resource1Descriptor);

        final WebResourceModuleDescriptor resource2Descriptor = new WebResourceModuleDescriptorBuilder(
                        plugin, "hot-stuff")
                .setCondition(AlwaysFalseCondition.class)
                .addDescriptor("hot.js")
                .addContext("foo")
                .build();
        mockEnabledPluginModule(resource2, resource2Descriptor);

        final String resource3 = "test.atlassian:warm-stuff";
        final WebResourceModuleDescriptor resourceDescriptor3 = TestUtils.createWebResourceModuleDescriptor(
                resource3,
                testPlugin,
                TestUtils.createResourceDescriptors("warm.js"),
                emptyList(),
                new HashSet<String>() {
                    {
                        add("foo");
                    }
                });
        mockEnabledPluginModule(resource3, resourceDescriptor3);

        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
                .thenReturn(asList(resource1Descriptor, resource2Descriptor, resourceDescriptor3));

        setupRequestCache();
        webResourceManager.requireResourcesForContext("foo");

        final String tags = webResourceManager.getRequiredResources(UrlMode.AUTO);
        assertFalse(tags.contains(resource1));
        assertFalse(tags.contains(resource2));
        assertTrue(tags.contains("foo/batch.js"));
    }

    public void testRequireResourceWithDuplicateDependencies() {
        final String resourceA = "test.atlassian:a";
        final String resourceB = "test.atlassian:b";
        final String resourceC = "test.atlassian:c";
        final String resourceD = "test.atlassian:d";
        final String resourceE = "test.atlassian:e";
        final Plugin plugin = TestUtils.createTestPlugin("test.atlassian", "1");

        // A depends on B, C
        mockEnabledPluginModule(
                resourceA,
                new WebResourceModuleDescriptorBuilder(plugin, "a")
                        .addDependency(resourceB)
                        .addDependency(resourceC)
                        .addDescriptor("a.js")
                        .build());
        // B depends on D
        mockEnabledPluginModule(
                resourceB,
                new WebResourceModuleDescriptorBuilder(plugin, "b")
                        .addDependency(resourceD)
                        .addDescriptor("b.js")
                        .build());
        // C depends on E
        mockEnabledPluginModule(
                resourceC,
                new WebResourceModuleDescriptorBuilder(plugin, "c")
                        .addDependency(resourceE)
                        .addDescriptor("c.js")
                        .build());
        // D depends on C
        mockEnabledPluginModule(
                resourceD,
                new WebResourceModuleDescriptorBuilder(plugin, "d")
                        .addDependency(resourceC)
                        .addDescriptor("d.js")
                        .build());
        // E has no dependencies
        mockEnabledPluginModule(
                resourceE,
                new WebResourceModuleDescriptorBuilder(plugin, "e")
                        .addDescriptor("e.js")
                        .build());

        setupRequestCache();
        webResourceManager.requireResource(resourceA);

        assertRequiredResources(
                resourceE + ".js", resourceC + ".js", resourceD + ".js", resourceB + ".js", resourceA + ".js");
    }

    public void testRequireSingleResourceGetsDeps() throws Exception {
        final String resourceA = "test.atlassian:a";
        final String resourceB = "test.atlassian:b";

        final List<ResourceDescriptor> resourceDescriptorsA = TestUtils.createResourceDescriptors("resourceA.css");
        final List<ResourceDescriptor> resourceDescriptorsB =
                TestUtils.createResourceDescriptors("resourceB.css", "resourceB-more.css");

        // A depends on B
        mockEnabledPluginModule(
                resourceA,
                TestUtils.createWebResourceModuleDescriptor(
                        resourceA, testPlugin, resourceDescriptorsA, singletonList(resourceB)));
        mockEnabledPluginModule(
                resourceB,
                TestUtils.createWebResourceModuleDescriptor(resourceB, testPlugin, resourceDescriptorsB, emptyList()));

        final String s = webResourceManager.getResourceTags(resourceA, UrlMode.AUTO);
        final int indexA = s.indexOf(resourceA);
        final int indexB = s.indexOf(resourceB);

        assertNotSame(-1, indexA);
        assertNotSame(-1, indexB);
        assertTrue(indexB < indexA);
    }

    public void testIncludeResourcesWithResourceList() throws Exception {
        final String resourceA = "test.atlassian:a";

        final List<ResourceDescriptor> resourceDescriptorsA = TestUtils.createResourceDescriptors("resourceA.css");

        mockEnabledPluginModule(
                resourceA,
                TestUtils.createWebResourceModuleDescriptor(resourceA, testPlugin, resourceDescriptorsA, emptyList()));

        final StringWriter requiredResourceWriter = new StringWriter();
        webResourceManager.includeResources(Arrays.<String>asList(resourceA), requiredResourceWriter, UrlMode.ABSOLUTE);
        final String result = requiredResourceWriter.toString();
        assertTrue(result.contains(resourceA));
    }

    public void testIncludeResourcesWithResourceListIgnoresRequireResource() throws Exception {
        final String resourceA = "test.atlassian:a";
        final String resourceB = "test.atlassian:b";

        final List<ResourceDescriptor> resourceDescriptorsA = TestUtils.createResourceDescriptors("resourceA.css");
        final List<ResourceDescriptor> resourceDescriptorsB =
                TestUtils.createResourceDescriptors("resourceB.css", "resourceB-more.css");

        final Map<String, Object> requestCache = new HashMap<>();
        when(mockWebResourceIntegration.getRequestCache()).thenReturn(requestCache);

        mockEnabledPluginModule(
                resourceA,
                TestUtils.createWebResourceModuleDescriptor(resourceA, testPlugin, resourceDescriptorsA, emptyList()));

        mockEnabledPluginModule(
                resourceB,
                TestUtils.createWebResourceModuleDescriptor(resourceB, testPlugin, resourceDescriptorsB, emptyList()));

        final StringWriter requiredResourceWriter = new StringWriter();
        webResourceManager.requireResource(resourceB);
        webResourceManager.includeResources(Arrays.<String>asList(resourceA), requiredResourceWriter, UrlMode.ABSOLUTE);
        final String result = requiredResourceWriter.toString();
        assertFalse(result.contains(resourceB));
    }

    public void testIncludeResourcesWithResourceListIncludesDependences() throws Exception {
        final String resourceA = "test.atlassian:a";
        final String resourceB = "test.atlassian:b";

        final List<ResourceDescriptor> resourceDescriptorsA = TestUtils.createResourceDescriptors("resourceA.css");
        final List<ResourceDescriptor> resourceDescriptorsB =
                TestUtils.createResourceDescriptors("resourceB.css", "resourceB-more.css");

        // A depends on B
        mockEnabledPluginModule(
                resourceA,
                TestUtils.createWebResourceModuleDescriptor(
                        resourceA, testPlugin, resourceDescriptorsA, singletonList(resourceB)));
        mockEnabledPluginModule(
                resourceB,
                TestUtils.createWebResourceModuleDescriptor(resourceB, testPlugin, resourceDescriptorsB, emptyList()));

        final StringWriter requiredResourceWriter = new StringWriter();
        webResourceManager.includeResources(Arrays.<String>asList(resourceA), requiredResourceWriter, UrlMode.ABSOLUTE);
        final String result = requiredResourceWriter.toString();
        assertTrue(result.contains(resourceB));
    }

    public void testIncludeResourcesWithResourceListIncludesDependencesFromSuperBatch() throws Exception {
        final String resourceA = "test.atlassian:a";
        final String resourceB = "test.atlassian:b";

        final ResourceBatchingConfiguration mockBatchingConfiguration = mock(ResourceBatchingConfiguration.class);
        when(mockBatchingConfiguration.isSuperBatchingEnabled()).thenReturn(true);
        when(mockBatchingConfiguration.getSuperBatchModuleCompleteKeys()).thenReturn(asList(resourceB));

        webResourceManager = new WebResourceManagerImpl(
                pluginResourceLocator.getGlobals(),
                mockWebResourceIntegration,
                mockUrlProvider,
                mockBatchingConfiguration);

        final List<ResourceDescriptor> resourceDescriptorsA = TestUtils.createResourceDescriptors("resourceA.css");
        final List<ResourceDescriptor> resourceDescriptorsB = TestUtils.createResourceDescriptors("resourceB.css");

        // A depends on B
        mockEnabledPluginModule(
                resourceA,
                TestUtils.createWebResourceModuleDescriptor(
                        resourceA, testPlugin, resourceDescriptorsA, singletonList(resourceB)));
        mockEnabledPluginModule(
                resourceB,
                TestUtils.createWebResourceModuleDescriptor(resourceB, testPlugin, resourceDescriptorsB, emptyList()));

        final StringWriter requiredResourceWriter = new StringWriter();
        webResourceManager.includeResources(Arrays.<String>asList(resourceA), requiredResourceWriter, UrlMode.ABSOLUTE);
        final String result = requiredResourceWriter.toString();
        assertTrue(result.contains(resourceB));
    }

    public void testIncludeResourcesWithSharedDependencies() throws Exception {
        final String resourceA = "test.atlassian:a";
        final String resourceB = "test.atlassian:b";
        final String resourceShared = "test.atlassian:c";

        when(mockBatchingConfiguration.getSuperBatchModuleCompleteKeys()).thenReturn(asList(resourceB));

        webResourceManager = new WebResourceManagerImpl(
                pluginResourceLocator.getGlobals(),
                mockWebResourceIntegration,
                mockUrlProvider,
                mockBatchingConfiguration);

        final List<ResourceDescriptor> resourceDescriptorsA = TestUtils.createResourceDescriptors("resourceA.css");
        final List<ResourceDescriptor> resourceDescriptorsB = TestUtils.createResourceDescriptors("resourceB.css");
        final List<ResourceDescriptor> resourceDescriptorsShared = TestUtils.createResourceDescriptors("resourceC.css");

        // A depends on C
        mockEnabledPluginModule(
                resourceA,
                TestUtils.createWebResourceModuleDescriptor(
                        resourceA, testPlugin, resourceDescriptorsA, singletonList(resourceShared)));
        // B depends on C
        mockEnabledPluginModule(
                resourceB,
                TestUtils.createWebResourceModuleDescriptor(
                        resourceB, testPlugin, resourceDescriptorsB, singletonList(resourceShared)));
        mockEnabledPluginModule(
                resourceShared,
                TestUtils.createWebResourceModuleDescriptor(
                        resourceShared, testPlugin, resourceDescriptorsShared, emptyList()));

        final StringWriter requiredResourceWriter = new StringWriter();
        webResourceManager.includeResources(
                Arrays.<String>asList(resourceA, resourceB), requiredResourceWriter, UrlMode.ABSOLUTE);
        final String result = removeWebResourceLogs(requiredResourceWriter.toString());
        assertTrue(result.contains(resourceA));
        assertTrue(result.contains(resourceB));

        final Pattern resourceCount = Pattern.compile("/download/batch/test\\.atlassian:c/test\\.atlassian:c.css");
        final Matcher m = resourceCount.matcher(result);

        assertTrue(m.find());
        assertFalse(m.find(m.end()));
    }

    public void testRequireResourcesAreClearedAfterIncludesResourcesIsCalled() throws Exception {
        final String moduleKey = "cool-resources";
        final String completeModuleKey = "test.atlassian:" + moduleKey;

        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("cool.css", "more-cool.css", "cool.js");

        setupRequestCache();

        mockEnabledPluginModule(
                completeModuleKey,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey, testPlugin, resourceDescriptors1));

        // test requireResource() methods
        webResourceManager.requireResource(completeModuleKey);
        webResourceManager.includeResources(new StringWriter(), UrlMode.RELATIVE);

        final StringWriter requiredResourceWriter = new StringWriter();
        webResourceManager.includeResources(requiredResourceWriter, UrlMode.RELATIVE);
        assertEquals("", removeWebResourceLogs(requiredResourceWriter.toString()));
    }

    // testRequireResourceAndResourceTagMethods

    public void testRequireResourceAndResourceTagMethods() throws Exception {
        final String completeModuleKey = "test.atlassian:cool-resources";
        final String staticBase = setupRequireResourceAndResourceTagMethods(false, completeModuleKey);

        // test requireResource() methods
        final StringWriter requiredResourceWriter = new StringWriter();
        webResourceManager.requireResource(completeModuleKey);
        final String requiredResourceResult = webResourceManager.getRequiredResources(UrlMode.RELATIVE);
        webResourceManager.includeResources(requiredResourceWriter, UrlMode.RELATIVE);
        assertEquals(requiredResourceResult, requiredResourceWriter.toString());

        assertTrue(requiredResourceResult.contains(
                "href=\"" + staticBase + "/" + completeModuleKey + "/" + completeModuleKey + ".css"));
        assertTrue(requiredResourceResult.contains(
                "src=\"" + staticBase + "/" + completeModuleKey + "/" + completeModuleKey + ".js"));

        // test resourceTag() methods
        final StringWriter resourceTagsWriter = new StringWriter();
        webResourceManager.requireResource(completeModuleKey, resourceTagsWriter, UrlMode.RELATIVE);
        final String resourceTagsResult = webResourceManager.getResourceTags(completeModuleKey, UrlMode.RELATIVE);
        assertEquals(resourceTagsResult, resourceTagsWriter.toString());

        // calling requireResource() or resourceTag() on a single webresource should be the same
        assertEquals(removeWebResourceLogs(requiredResourceResult), removeWebResourceLogs(resourceTagsResult));
    }

    public void testRequireResourceAndResourceTagMethodsWithAbsoluteUrlMode() throws Exception {
        testRequireResourceAndResourceTagMethods(UrlMode.ABSOLUTE, true);
    }

    public void testRequireResourceAndResourceTagMethodsWithRelativeUrlMode() throws Exception {
        testRequireResourceAndResourceTagMethods(UrlMode.RELATIVE, false);
    }

    public void testRequireResourceAndResourceTagMethodsWithAutoUrlMode() throws Exception {
        testRequireResourceAndResourceTagMethods(UrlMode.AUTO, false);
    }

    private void testRequireResourceAndResourceTagMethods(final UrlMode urlMode, final boolean baseUrlExpected)
            throws Exception {
        final String completeModuleKey = "test.atlassian:cool-resources";
        final String staticBase = setupRequireResourceAndResourceTagMethods(baseUrlExpected, completeModuleKey);

        // test requireResource() methods
        final StringWriter requiredResourceWriter = new StringWriter();
        webResourceManager.requireResource(completeModuleKey);
        final String requiredResourceResult = webResourceManager.getRequiredResources(urlMode);
        webResourceManager.includeResources(requiredResourceWriter, urlMode);
        assertEquals(requiredResourceResult, requiredResourceWriter.toString());

        assertTrue(requiredResourceResult.contains(
                "href=\"" + staticBase + "/" + completeModuleKey + "/" + completeModuleKey + ".css"));
        assertTrue(requiredResourceResult.contains(
                "src=\"" + staticBase + "/" + completeModuleKey + "/" + completeModuleKey + ".js"));

        // test resourceTag() methods
        final StringWriter resourceTagsWriter = new StringWriter();
        webResourceManager.requireResource(completeModuleKey, resourceTagsWriter, urlMode);
        final String resourceTagsResult = webResourceManager.getResourceTags(completeModuleKey, urlMode);
        assertEquals(resourceTagsResult, resourceTagsWriter.toString());

        // calling requireResource() or resourceTag() on a single webresource should be the same
        assertEquals(removeWebResourceLogs(requiredResourceResult), removeWebResourceLogs(resourceTagsResult));
    }

    private String setupRequireResourceAndResourceTagMethods(
            final boolean baseUrlExpected, final String completeModuleKey) throws DocumentException {
        final List<ResourceDescriptor> descriptors =
                TestUtils.createResourceDescriptors("cool.css", "more-cool.css", "cool.js");

        final Map<String, Object> requestCache = new HashMap<>();
        when(mockWebResourceIntegration.getRequestCache()).thenReturn(requestCache);

        mockEnabledPluginModule(
                completeModuleKey,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey, testPlugin, descriptors));

        final String staticPrefix = (baseUrlExpected ? BASEURL : "") + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_PREFIX + "/" + SYSTEM_BUILD_NUMBER + "/" + SYSTEM_COUNTER
                + "/" + testPlugin.getPluginInformation().getVersion() + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_SUFFIX;
        when(mockUrlProvider.getStaticResourcePrefix(anyString(), eq("1"), isA(UrlMode.class)))
                .thenReturn(staticPrefix);
        return staticPrefix + BATCH_URL_PREFIX;
    }

    // testRequireResourceWithCacheParameter

    public void testRequireResourceWithCacheParameter() throws Exception {
        final String completeModuleKey = "test.atlassian:no-cache-resources";

        final String expectedResult = setupRequireResourceWithCacheParameter(false, completeModuleKey);
        assertTrue(webResourceManager
                .getResourceTags(completeModuleKey, UrlMode.AUTO)
                .contains(expectedResult));
    }

    public void testRequireResourceWithCacheParameterAndAbsoluteUrlMode() throws Exception {
        testRequireResourceWithCacheParameter(UrlMode.ABSOLUTE, true);
    }

    public void testRequireResourceWithCacheParameterAndRelativeUrlMode() throws Exception {
        testRequireResourceWithCacheParameter(UrlMode.RELATIVE, false);
    }

    public void testRequireResourceWithCacheParameterAndAutoUrlMode() throws Exception {
        testRequireResourceWithCacheParameter(UrlMode.AUTO, false);
    }

    private void testRequireResourceWithCacheParameter(final UrlMode urlMode, final boolean baseUrlExpected)
            throws Exception {
        final String completeModuleKey = "test.atlassian:no-cache-resources";
        final String expectedResult = setupRequireResourceWithCacheParameter(baseUrlExpected, completeModuleKey);
        assertTrue(
                webResourceManager.getResourceTags(completeModuleKey, urlMode).contains(expectedResult));
    }

    private String setupRequireResourceWithCacheParameter(final boolean baseUrlExpected, final String completeModuleKey)
            throws DocumentException {
        final Map<String, String> params = new HashMap<>();
        params.put("cache", "false");
        final ResourceDescriptor resourceDescriptor = TestUtils.createResourceDescriptor("no-cache.js", params);

        mockEnabledPluginModule(
                completeModuleKey,
                TestUtils.createWebResourceModuleDescriptor(
                        completeModuleKey, testPlugin, singletonList(resourceDescriptor)));

        return "src=\"" + (baseUrlExpected ? BASEURL : "") + BATCH_URL_PREFIX + "/" + completeModuleKey + "/"
                + completeModuleKey + ".js?cache=false";
    }

    public void testGetRequiredResourcesWithFilter() throws Exception {
        final String moduleKey = "cool-resources";
        final String completeModuleKey = "test.atlassian:" + moduleKey;

        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("cool.css", "cool.js", "more-cool.css");

        setupRequestCache();

        mockEnabledPluginModule(
                completeModuleKey,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey, testPlugin, resourceDescriptors1));

        // test includeResources(writer, type) method
        webResourceManager.requireResource(completeModuleKey);

        final String staticPrefix = BASEURL + "/" + WebResourceUrlProviderImpl.STATIC_RESOURCE_PREFIX + "/"
                + SYSTEM_BUILD_NUMBER + "/" + SYSTEM_COUNTER + "/"
                + testPlugin.getPluginInformation().getVersion() + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_SUFFIX;
        when(mockUrlProvider.getStaticResourcePrefix(anyString(), eq("1"), eq(UrlMode.ABSOLUTE)))
                .thenReturn(staticPrefix);
        final String staticBase = staticPrefix + BATCH_URL_PREFIX;

        final String cssRef = "href=\"" + staticBase + "/" + completeModuleKey + "/" + completeModuleKey + ".css";
        final String jsRef = "src=\"" + staticBase + "/" + completeModuleKey + "/" + completeModuleKey + ".js";

        // CSS
        String requiredResourceResult = webResourceManager.getRequiredResources(UrlMode.ABSOLUTE, new CssWebResource());
        assertTrue(requiredResourceResult.contains(cssRef));
        assertFalse(requiredResourceResult.contains(jsRef));

        // JS
        requiredResourceResult = webResourceManager.getRequiredResources(UrlMode.ABSOLUTE, new JavascriptWebResource());
        assertFalse(requiredResourceResult.contains(cssRef));
        assertTrue(requiredResourceResult.contains(jsRef));

        // BOTH
        requiredResourceResult = webResourceManager.getRequiredResources(UrlMode.ABSOLUTE);
        assertTrue(requiredResourceResult.contains(cssRef));
        assertTrue(requiredResourceResult.contains(jsRef));
    }

    public void testGetRequiredResourcesWithCustomFilters() throws Exception {
        final WebResourceFilter atlassianFilter = new WebResourceFilter() {
            public boolean matches(final String resourceName) {
                return resourceName.contains("atlassian");
            }
        };
        final WebResourceFilter bogusFilter = new WebResourceFilter() {
            public boolean matches(final String resourceName) {
                return true;
            }
        };

        final String moduleKey = "cool-resources";
        final String completeModuleKey = "test.atlassian:" + moduleKey;

        final List<ResourceDescriptor> resources =
                TestUtils.createResourceDescriptors("foo.css", "foo-bar.js", "atlassian.css", "atlassian-plugins.js");

        setupRequestCache();
        mockEnabledPluginModule(
                completeModuleKey,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey, testPlugin, resources));

        // easier to test which resources were included by the filter with batching turned off
        when(mockBatchingConfiguration.isPluginWebResourceBatchingEnabled()).thenReturn(false);
        webResourceManager.requireResource(completeModuleKey);
        final String atlassianResources =
                removeWebResourceLogs(webResourceManager.getRequiredResources(UrlMode.RELATIVE, atlassianFilter));
        assertEquals(-1, atlassianResources.indexOf("foo"));
        assertTrue(atlassianResources.contains("atlassian.css"));
        assertTrue(atlassianResources.contains("atlassian-plugins.js"));

        final String allResources = webResourceManager.getRequiredResources(UrlMode.RELATIVE, bogusFilter);
        for (final ResourceDescriptor resource : resources) {
            assertTrue(allResources.contains(resource.getName()));
        }
    }

    public void testGetRequiredResourcesWithExcludedResources() throws Exception {
        final String moduleKey = "cool-resources";
        final String completeModuleKey = "test.atlassian:" + moduleKey;

        final String existingModuleKey = "test.atlassian:alphabets";

        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("cool.js", "more-cool.js");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("abc.js", "def.js");

        setupRequestCache();

        mockEnabledPluginModule(
                completeModuleKey,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey, testPlugin, resourceDescriptors1));
        mockEnabledPluginModule(
                existingModuleKey,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey, testPlugin, resourceDescriptors2));

        // test includeResources(writer, type) method
        webResourceManager.requireResource(completeModuleKey);
        webResourceManager.requireResource(existingModuleKey);

        final String staticPrefix = BASEURL + "/" + WebResourceUrlProviderImpl.STATIC_RESOURCE_PREFIX + "/"
                + SYSTEM_BUILD_NUMBER + "/" + SYSTEM_COUNTER + "/"
                + testPlugin.getPluginInformation().getVersion() + "/"
                + WebResourceUrlProviderImpl.STATIC_RESOURCE_SUFFIX;
        when(mockUrlProvider.getStaticResourcePrefix(anyString(), eq("1"), eq(UrlMode.ABSOLUTE)))
                .thenReturn(staticPrefix);
        final String staticBase = staticPrefix + BATCH_URL_PREFIX;

        String requiredResourceResult = removeWebResourceLogs(webResourceManager.getRequiredResources(
                UrlMode.ABSOLUTE, new JavascriptWebResource(), singleton(existingModuleKey), emptyList()));
        assertTrue(requiredResourceResult.contains(
                staticBase + "/" + completeModuleKey + "/" + completeModuleKey + ".js"));
        assertFalse(requiredResourceResult.contains(existingModuleKey));
    }

    public void testGetRequiredResourcesOrdersByType() throws Exception {
        final String moduleKey1 = "cool-resources";
        final String moduleKey2 = "hot-resources";
        final String completeModuleKey1 = "test.atlassian:" + moduleKey1;
        final String completeModuleKey2 = "test.atlassian:" + moduleKey2;

        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("cool.js", "cool.css", "more-cool.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("hot.js", "hot.css", "more-hot.css");

        final Plugin plugin = TestUtils.createTestPlugin();

        setupRequestCache();

        mockEnabledPluginModule(
                completeModuleKey1,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey1, plugin, resourceDescriptors1));
        mockEnabledPluginModule(
                completeModuleKey2,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey2, plugin, resourceDescriptors2));

        // test includeResources(writer, type) method
        webResourceManager.requireResource(completeModuleKey1);
        webResourceManager.requireResource(completeModuleKey2);

        final String staticPrefix = BASEURL + "/" + WebResourceUrlProviderImpl.STATIC_RESOURCE_PREFIX + "/"
                + SYSTEM_BUILD_NUMBER + "/" + SYSTEM_COUNTER + "/"
                + plugin.getPluginInformation().getVersion() + "/" + WebResourceUrlProviderImpl.STATIC_RESOURCE_SUFFIX;
        when(mockUrlProvider.getStaticResourcePrefix(anyString(), eq("1"), eq(UrlMode.ABSOLUTE)))
                .thenReturn(staticPrefix);
        final String staticBase = staticPrefix + BATCH_URL_PREFIX;

        final String cssRef1 = "href=\"" + staticBase + "/" + completeModuleKey1 + "/" + completeModuleKey1 + ".css";
        final String cssRef2 = "href=\"" + staticBase + "/" + completeModuleKey2 + "/" + completeModuleKey2 + ".css";
        final String jsRef1 = "src=\"" + staticBase + "/" + completeModuleKey1 + "/" + completeModuleKey1 + ".js";
        final String jsRef2 = "src=\"" + staticBase + "/" + completeModuleKey2 + "/" + completeModuleKey2 + ".js";

        final String requiredResourceResult = webResourceManager.getRequiredResources(UrlMode.ABSOLUTE);

        assertTrue(requiredResourceResult.contains(cssRef1));
        assertTrue(requiredResourceResult.contains(cssRef2));
        assertTrue(requiredResourceResult.contains(jsRef1));
        assertTrue(requiredResourceResult.contains(jsRef2));

        final int cssRef1Index = requiredResourceResult.indexOf(cssRef1);
        final int cssRef2Index = requiredResourceResult.indexOf(cssRef2);
        final int jsRef1Index = requiredResourceResult.indexOf(jsRef1);
        final int jsRef2Index = requiredResourceResult.indexOf(jsRef2);

        assertTrue(cssRef1Index < jsRef1Index);
        assertTrue(cssRef2Index < jsRef2Index);
        assertTrue(cssRef2Index < jsRef1Index);
    }

    public void testRequireResourceInSuperbatch() throws ClassNotFoundException {
        setupSuperBatch(mockBatchingConfiguration);

        setupRequestCache();
        mockOutSuperbatchPluginAccesses();

        webResourceManager.requireResource("test.atlassian:superbatch");

        assertRequiredResources("batch.css", "batch.js");
    }

    public void testRequireResourceWithDependencyInSuperbatch() throws DocumentException, ClassNotFoundException {
        String resource1 = "test.atlassian:included-resource";
        setupSuperBatch(mockBatchingConfiguration);
        final Plugin plugin = TestUtils.createTestPlugin("test.atlassian", "1");

        mockOutSuperbatchPluginAccesses();

        setupRequestCache();

        mockEnabledPluginModule(
                resource1,
                new WebResourceModuleDescriptorBuilder(plugin, "included-resource")
                        .addDependency("test.atlassian:superbatch")
                        .addDescriptor("blah.js")
                        .build());

        webResourceManager.requireResource(resource1);

        assertRequiredResources("batch.css", "batch.js", resource1 + ".js");
    }

    public void testSuperBatchResolution() throws DocumentException, ClassNotFoundException {
        setupSuperBatch(mockBatchingConfiguration);

        ResourceDescriptor masterCssResource = TestUtils.createResourceDescriptor("master.css");
        ResourceDescriptor masterPrintOnlyCssResource =
                TestUtils.createResourceDescriptor("master.css", singletonMap("media", "print"));
        ResourceDescriptor irrelevantParameterCssResource =
                TestUtils.createResourceDescriptor("two.css", singletonMap("fish", "true"));
        ResourceDescriptor masterJavascriptResource = TestUtils.createResourceDescriptor("master.js");

        ResourceDescriptor pluginCssResource = TestUtils.createResourceDescriptor("plugin.css");
        ResourceDescriptor pluginJsResource = TestUtils.createResourceDescriptor("plugin.js");

        WebResourceTransformation cssTransformer = createWebResourceTransformation(
                ImmutableMap.of("less", "3"), createNamedHashes("hash-name-css", "hash-csss"), "css");
        WebResourceTransformation i18nTransformer = createWebResourceTransformation(
                ImmutableMap.of("locale", "en_AU"), createNamedHashes("hash-name-js", "hash-js"), "js");

        WebResourceModuleDescriptor master = TestUtils.createWebResourceModuleDescriptor(
                "test.atlassian:superbatch",
                testPlugin,
                asList(
                        masterCssResource,
                        masterPrintOnlyCssResource,
                        masterJavascriptResource,
                        irrelevantParameterCssResource),
                emptyList(),
                emptySet(),
                asList(i18nTransformer, cssTransformer));
        WebResourceModuleDescriptor plugin = TestUtils.createWebResourceModuleDescriptor(
                "test.atlassian:superbatch2",
                testPlugin,
                asList(pluginCssResource, pluginJsResource),
                emptyList(),
                emptySet(),
                singletonList(i18nTransformer));

        mockEnabledPluginModule("test.atlassian:superbatch", master);
        mockEnabledPluginModule("test.atlassian:superbatch2", plugin);
        mockEnabledPluginModule("test.atlassian:missing-plugin", null);

        setupRequestCache();
        final List<PluginCssResource> cssResources = Lists.newArrayList(webResourceManager
                .getRequestLocalAssembledResources()
                .drainIncludedResources()
                .getResources(PluginCssResource.class));
        assertEquals(2, cssResources.size());

        assertThat(
                cssResources.get(0).getStaticUrl(com.atlassian.webresource.api.UrlMode.RELATIVE),
                containsString("batch.css"));
        assertThat(
                cssResources.get(1).getStaticUrl(com.atlassian.webresource.api.UrlMode.RELATIVE),
                containsString("batch.css"));

        setupRequestCache();
        final List<PluginJsResource> jsResources = Lists.newArrayList(webResourceManager
                .getRequestLocalAssembledResources()
                .drainIncludedResources()
                .getResources(PluginJsResource.class));
        assertEquals(1, jsResources.size());

        assertThat(
                jsResources.get(0).getStaticUrl(com.atlassian.webresource.api.UrlMode.RELATIVE),
                containsString("batch.js"));
    }

    // First part: execute some WRM code on in a new context on top of a non-empty context and write out.
    //             Check that the expected resources are included in the inner write out and that the outer resources
    // aren't included
    // Second part: after the inner execution is completed, write the outer resources out, checking that we didn't get
    // the inner resources and that we did get
    //              the resources which were in the context before the inner execution.
    public void testNestedContextExclusivity() throws Exception {

        final String moduleKey1 = "cool-resources";
        final String moduleKey2 = "hot-resources";
        final String completeModuleKey1 = "test.atlassian:" + moduleKey1;
        final String completeModuleKey2 = "test.atlassian:" + moduleKey2;

        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("cool.js", "cool.css", "more-cool.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("hot.js", "hot.css", "more-hot.css");

        final Plugin plugin = TestUtils.createTestPlugin();

        setupRequestCache();

        mockEnabledPluginModule(
                completeModuleKey1,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey1, plugin, resourceDescriptors1));
        mockEnabledPluginModule(
                completeModuleKey2,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey2, plugin, resourceDescriptors2));

        final String cssRef1 = completeModuleKey1 + "/" + completeModuleKey1 + ".css";
        final String cssRef2 = completeModuleKey2 + "/" + completeModuleKey2 + ".css";
        final String jsRef1 = completeModuleKey1 + "/" + completeModuleKey1 + ".js";
        final String jsRef2 = completeModuleKey2 + "/" + completeModuleKey2 + ".js";

        // test includeResources(writer, type) method
        webResourceManager.requireResource(completeModuleKey1);

        final StringWriter writer = new StringWriter();
        webResourceManager.includeResources(writer, UrlMode.AUTO);
        final String resources = writer.toString();

        assertTrue(resources.contains(cssRef1));
        assertFalse(resources.contains(cssRef2));

        assertTrue(resources.contains(jsRef1));
        assertFalse(resources.contains(jsRef2));
    }

    // Require and include some resource in an inner context with an empty outer context. Check that the inner context
    // gets the expected resources.
    // After the inner code is executed, check that we didn't get any resources in the outer context
    public void testNestedContextOnEmptyBase() throws Exception {
        final String moduleKey1 = "cool-resources";
        final String completeModuleKey1 = "test.atlassian:" + moduleKey1;

        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("cool.js", "cool.css", "more-cool.css");

        final Plugin plugin = TestUtils.createTestPlugin();

        setupRequestCache();

        mockEnabledPluginModule(
                completeModuleKey1,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey1, plugin, resourceDescriptors1));

        final String cssRef1 = completeModuleKey1 + "/" + completeModuleKey1 + ".css";
        final String jsRef1 = completeModuleKey1 + "/" + completeModuleKey1 + ".js";

        final StringWriter writer = new StringWriter();
        webResourceManager.includeResources(writer, UrlMode.AUTO);
        final String resources = writer.toString();

        assertFalse(resources.contains(cssRef1));
        assertFalse(resources.contains(jsRef1));
    }

    // Require (but not include) some resource in an inner context with an empty outer context. After the inner code is
    // executed,
    // check that we didn't get any resources in the outer context
    public void testNestedContextWithoutWriting() throws Exception {
        final String moduleKey1 = "cool-resources";
        final String completeModuleKey1 = "test.atlassian:" + moduleKey1;

        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("cool.js", "cool.css", "more-cool.css");

        final Plugin plugin = TestUtils.createTestPlugin();

        setupRequestCache();

        mockEnabledPluginModule(
                completeModuleKey1,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey1, plugin, resourceDescriptors1));

        final String cssRef1 = completeModuleKey1 + "/" + completeModuleKey1 + ".css";
        final String jsRef1 = completeModuleKey1 + "/" + completeModuleKey1 + ".js";

        final StringWriter writer = new StringWriter();
        webResourceManager.includeResources(writer, UrlMode.AUTO);
        final String resources = writer.toString();

        // check that we didn't get these resources (meaning that the require resources call never caused the module to
        // be included)
        assertFalse(resources.contains(cssRef1));
        assertFalse(resources.contains(jsRef1));
    }

    // push content into the outer-most context, nest multiple non-empty executeInNewContext calls, ensuring that the
    // expected content is
    // written in each context
    public void testMultipleNestedContexts() throws Exception {
        final String moduleKey1 = "cool-resources";
        final String moduleKey2 = "hot-resources";
        final String moduleKey3 = "warm-resources";
        final String completeModuleKey1 = "test.atlassian:" + moduleKey1;
        final String completeModuleKey2 = "test.atlassian:" + moduleKey2;
        final String completeModuleKey3 = "test.atlassian:" + moduleKey3;

        final List<ResourceDescriptor> resourceDescriptors1 =
                TestUtils.createResourceDescriptors("cool.js", "cool.css", "more-cool.css");
        final List<ResourceDescriptor> resourceDescriptors2 =
                TestUtils.createResourceDescriptors("hot.js", "hot.css", "more-hot.css");
        final List<ResourceDescriptor> resourceDescriptors3 =
                TestUtils.createResourceDescriptors("warm.js", "warm.css", "more-warm.css");

        final Plugin plugin = TestUtils.createTestPlugin();

        setupRequestCache();

        mockEnabledPluginModule(
                completeModuleKey1,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey1, plugin, resourceDescriptors1));
        mockEnabledPluginModule(
                completeModuleKey2,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey2, plugin, resourceDescriptors2));
        mockEnabledPluginModule(
                completeModuleKey3,
                TestUtils.createWebResourceModuleDescriptor(completeModuleKey3, plugin, resourceDescriptors3));

        final String cssRef1 = completeModuleKey1 + "/" + completeModuleKey1 + ".css";
        final String cssRef2 = completeModuleKey2 + "/" + completeModuleKey2 + ".css";
        final String cssRef3 = completeModuleKey3 + "/" + completeModuleKey3 + ".css";
        final String jsRef1 = completeModuleKey1 + "/" + completeModuleKey1 + ".js";
        final String jsRef2 = completeModuleKey2 + "/" + completeModuleKey2 + ".js";
        final String jsRef3 = completeModuleKey3 + "/" + completeModuleKey3 + ".js";

        setupRequestCache();

        // require module 1 for outermost resource
        webResourceManager.requireResource(completeModuleKey1);

        // check that the outer context only got module 1

        final StringWriter writer = new StringWriter();
        webResourceManager.includeResources(writer, UrlMode.AUTO);
        final String resources = writer.toString();

        assertTrue(resources.contains(cssRef1));
        assertTrue(resources.contains(jsRef1));
        assertFalse(resources.contains(cssRef2));
        assertFalse(resources.contains(jsRef2));
        assertFalse(resources.contains(cssRef3));
        assertFalse(resources.contains(jsRef3));
    }

    private Map<String, Object> setupRequestCache() {
        final Map<String, Object> requestCache = new HashMap<>();
        when(mockWebResourceIntegration.getRequestCache()).thenReturn(requestCache);
        return requestCache;
    }

    private void mockOutSuperbatchPluginAccesses() throws ClassNotFoundException {
        mockOutPluginModule("test.atlassian:superbatch", "superbatch.js", "superbatch.css");
        mockOutPluginModule("test.atlassian:superbatch2", "superbatch2.js", "superbatch2.css");
        when(mockPluginAccessor.getPluginModule("test.atlassian:missing-plugin"))
                .thenReturn(null);
        when(mockPluginAccessor.getEnabledPluginModule("test.atlassian:missing-plugin"))
                .thenReturn(null);
    }

    private void mockOutPluginModule(final String moduleKey, final String... resourceNames)
            throws ClassNotFoundException {
        final Plugin p = TestUtils.createTestPlugin();
        final WebResourceModuleDescriptor module = TestUtils.createWebResourceModuleDescriptor(
                moduleKey, p, TestUtils.createResourceDescriptors(resourceNames));
        when(mockPluginAccessor.getPluginModule(moduleKey)).thenReturn((ModuleDescriptor) module);
        when(mockPluginAccessor.getEnabledPluginModule(moduleKey)).thenReturn((ModuleDescriptor) module);
        listOfModuleDescriptors.add(module);
    }

    private void assertRequiredResources(String... expectedResourceNames) {
        Iterable<PluginUrlResource> resources = webResourceManager
                .getRequestLocalAssembledResources()
                .drainIncludedResources()
                .getResources(PluginUrlResource.class);
        Iterable<String> resourceNames = Iterables.transform(resources, new Function<PluginUrlResource, String>() {
            @Override
            public String apply(@Nullable PluginUrlResource input) {
                String url = input.getStaticUrl(com.atlassian.webresource.api.UrlMode.RELATIVE);
                return url.substring(url.lastIndexOf("/") + 1);
            }
        });
        assertEquals(asList(expectedResourceNames), Lists.newArrayList(resourceNames));
    }
}
