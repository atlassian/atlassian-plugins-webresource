package com.atlassian.plugin.webresource.assembler;

import java.io.StringWriter;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import static com.atlassian.plugin.webresource.TestUtils.removeWebResourceLogs;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.INTERACTION;
import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.REQUIRE;

public class TestWebResourceSetWithPhases {
    private AssemblerTestFixture f;
    private StringWriter writer;

    @Before
    public void setUp() {
        f = new AssemblerTestFixture();
        f.mockPlugin("test.atlassian:first-feature")
                .res("first-1.js")
                .res("first-2.js")
                .res("first-1.css")
                .res("first-2.css")
                .ctx("first", "all");
        f.mockPlugin("test.atlassian:second-feature")
                .res("second-1.css")
                .res("second-1.js")
                .res("second-2.css")
                .res("second-2.js")
                .ctx("second", "all");
        f.mockPlugin("test.atlassian:third-feature")
                .res("third.css")
                .res("third.js")
                .deps("test.atlassian:first-feature", "test.atlassian:second-feature")
                .ctx("third", "all");

        f.mockContent("first-1.css", ".first-1{color:red}");
        f.mockContent("first-2.css", ".first-2{color:#f00}");
        f.mockContent("second-1.css", ".second-1{color:green}");
        f.mockContent("second-2.css", ".second-2{color:#0f0}");
        f.mockContent("third.css", ".third{color:blue}");

        f.mockContent("first-1.js", "console.log('first-1');");
        f.mockContent("first-2.js", "console.log('first-2');");
        f.mockContent("second-1.js", "console.log('second-1');");
        f.mockContent("second-2.js", "console.log('second-2');");
        f.mockContent("third.js", "console.log('third');");

        writer = new StringWriter();
    }

    @Test
    public void testLoadAllFeaturesInteractiveIndividually() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource(INTERACTION, "test.atlassian:first-feature");
        assembler.resources().requireWebResource(INTERACTION, "test.atlassian:second-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expected =
                "<script type=\"module\">WRM.requireLazily([\"wr!test.atlassian:first-feature\",\"wr!test.atlassian:second-feature\"])</script>";
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void testLoadAllFeaturesInteractiveViaContext() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "all");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expected = "<script type=\"module\">WRM.requireLazily([\"wrc!all\"])</script>";
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void testLoadOneFeatureInteractive() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource(INTERACTION, "test.atlassian:first-feature");
        assembler.resources().requireWebResource(REQUIRE, "test.atlassian:second-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expected =
                "<link rel=\"stylesheet\" href=\"/download/batch/test.atlassian:second-feature/test.atlassian:second-feature.css\""
                        + " data-wrm-key=\"test.atlassian:second-feature\" data-wrm-batch-type=\"resource\" media=\"all\">"
                        + "\n"
                        + "<script src=\"/download/batch/test.atlassian:second-feature/test.atlassian:second-feature.js\""
                        + " data-wrm-key=\"test.atlassian:second-feature\" data-wrm-batch-type=\"resource\" "
                        + Config.INITIAL_RENDERED_SCRIPT_PARAM_NAME + "></script>" + "\n"
                        + "<script type=\"module\">WRM.requireLazily([\"wr!test.atlassian:first-feature\"])</script>";
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void testLoadTwoFeaturesInteractive() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource(INTERACTION, "test.atlassian:first-feature");
        assembler.resources().requireWebResource(REQUIRE, "test.atlassian:second-feature");
        assembler.resources().requireWebResource(INTERACTION, "test.atlassian:third-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expected =
                "<link rel=\"stylesheet\" href=\"/download/batch/test.atlassian:second-feature/test.atlassian:second-feature.css\""
                        + " data-wrm-key=\"test.atlassian:second-feature\" data-wrm-batch-type=\"resource\" media=\"all\">"
                        + "\n"
                        + "<script src=\"/download/batch/test.atlassian:second-feature/test.atlassian:second-feature.js\""
                        + " data-wrm-key=\"test.atlassian:second-feature\" data-wrm-batch-type=\"resource\" "
                        + Config.INITIAL_RENDERED_SCRIPT_PARAM_NAME + "></script>" + "\n"
                        + "<script type=\"module\">WRM.requireLazily([\"wr!test.atlassian:first-feature\",\"wr!test.atlassian:third-feature\"])</script>";
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void testLoadMixedInteractive() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "first");
        assembler.resources().requireContext(REQUIRE, "second");
        assembler.resources().requireWebResource(INTERACTION, "test.atlassian:third-feature");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        final String expected = "<link rel=\"stylesheet\" href=\"/download/contextbatch/css/second/batch.css\""
                + " data-wrm-key=\"second\" data-wrm-batch-type=\"context\" media=\"all\">"
                + "\n"
                + "<script src=\"/download/contextbatch/js/second/batch.js\""
                + " data-wrm-key=\"second\" data-wrm-batch-type=\"context\" data-initially-rendered></script>"
                + "\n"
                + "<script type=\"module\">WRM.requireLazily([\"wrc!first\",\"wr!test.atlassian:third-feature\"])</script>";
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void testInteractiveFeatureNotOutputIfRequestedAsRender() {
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext(INTERACTION, "first");
        assembler.resources().requireContext(REQUIRE, "first");
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        final String actual = removeWebResourceLogs(writer.toString());
        assertThat(actual, not(containsString("WRM.requireLazily")));
    }

    // @todo: test permutations of resources within features - e.g., no CSS, no JS - to ensure lazy require happens even
    // if one media type isn't there
    // @todo: test legacy... nothing output if IE conditionals present

    // @todo: demonstrate how to use WebResourceSet#getResources() with phases. needs separate issue key
}
