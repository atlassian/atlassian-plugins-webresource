package com.atlassian.plugin.webresource.mocks;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.plugin.elements.ResourceLocation;

import static java.util.Objects.requireNonNull;
import static org.mockito.Mockito.when;

public class ResourceLocationMockBuilder {
    private final MockBuilder parent;
    private final ResourceLocation resourceLocation;

    public ResourceLocationMockBuilder(
            @Nonnull final MockBuilder parent, @Nonnull final ResourceLocation resourceLocation) {
        this.parent = requireNonNull(parent, "The parent is mandatory for the resource location mock build.");
        this.resourceLocation = requireNonNull(
                resourceLocation, "The resource location is mandatory for the resource location mock build.");
    }

    @Nonnull
    public ResourceLocationMockBuilder singletonParams(@Nonnull final String name, @Nullable final String value) {
        final Map<String, String> params = new HashMap<>();
        params.put(name, value);
        when(resourceLocation.getParams()).thenReturn(params);
        return this;
    }

    @Nonnull
    public ResourceLocationMockBuilder params(@Nullable final Map<String, String> params) {
        when(resourceLocation.getParams()).thenReturn(new HashMap<>(params));
        return this;
    }
}
