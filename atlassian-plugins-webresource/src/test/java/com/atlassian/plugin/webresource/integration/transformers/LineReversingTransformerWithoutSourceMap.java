package com.atlassian.plugin.webresource.integration.transformers;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformableResource;

public class LineReversingTransformerWithoutSourceMap extends TransformerWithoutSourceMapHelper {
    public DownloadableResource transform(TransformableResource transformableResource, final QueryParams params) {
        DownloadableResource originalResource = transformableResource.nextResource();
        String content = TestUtils.toString(originalResource);
        String[] lines = content.split("\n");
        ArrayUtils.reverse(lines);
        return new StringDownloadableResource(originalResource, StringUtils.join(lines, "\n"));
    }
}
