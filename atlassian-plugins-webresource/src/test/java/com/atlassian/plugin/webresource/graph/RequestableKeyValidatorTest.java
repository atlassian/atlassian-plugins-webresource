package com.atlassian.plugin.webresource.graph;

import org.junit.Test;

import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Stream.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static com.atlassian.plugin.webresource.graph.RequestableKeyValidator.isWebResourceContext;

public class RequestableKeyValidatorTest {

    /**
     * Represents a requestable resource key which is the weak side of the dependency relationship.
     */
    private static final String SOURCE_REQUESTABLE_DEPENDENCY_KEY =
            "com.atlassian.plugins.atlassian-plugins-webresource-tests:feature_dependency";

    /**
     * Represents a requestable context resource key which is the weak side of the dependency relationship.
     */
    private static final String SOURCE_REQUESTABLE_CONTEXT_DEPENDENCY_KEY = "atl.general";

    @Test
    public void
            when_RequestableDependencyKeyIsNotRootPage_And_ContainsWebResourceSymbol_Then_ShouldBeConsideredWebResource() {
        // given
        final RequestableKeyValidator underTest = new RequestableKeyValidator(emptySet());
        // when
        final boolean isWebResource = underTest.isWebResource(SOURCE_REQUESTABLE_DEPENDENCY_KEY);
        // then
        assertTrue(
                "When requestable resource is not root page and contains the symbol ':', it should be considered a valid web resource to be added as dependency.",
                isWebResource);
    }

    @Test
    public void
            when_RequestableDependencyKeyIsRootPage_And_ContainsWebResourceSymbol_Then_ShouldNotBeConsideredWebResource() {
        // given
        final RequestableKeyValidator underTest = new RequestableKeyValidator(
                of(SOURCE_REQUESTABLE_DEPENDENCY_KEY).collect(toSet()));
        // when
        final boolean isWebResource = underTest.isWebResource(SOURCE_REQUESTABLE_DEPENDENCY_KEY);
        // then
        assertFalse(
                "When requestable resource is root page, it should not be considered a valid web resource to be added as dependency.",
                isWebResource);
    }

    @Test
    public void
            when_RequestableDependencyKeyIsNotRootPage_And_DoesNotContainsWebResourceSymbol_Then_ShouldNotBeConsideredWebResource() {
        // given
        final RequestableKeyValidator underTest = new RequestableKeyValidator(emptySet());
        // when
        final boolean isWebResource = underTest.isWebResource(SOURCE_REQUESTABLE_DEPENDENCY_KEY.replace(":", ""));
        // then
        assertFalse(
                "When requestable key does not contain the symbol ':', it should not be considered a valid web resource to be added as dependency.",
                isWebResource);
    }

    @Test
    public void
            when_RequestableDependencyKeyDoesNotContainsWebResourceSymbol_Then_ShouldBeConsideredWebResourceContext() {
        // when
        final boolean isWebResourceContext = isWebResourceContext(SOURCE_REQUESTABLE_CONTEXT_DEPENDENCY_KEY);
        // then
        assertTrue(
                "When requestable key does not contain the symbol ':', it should be considered a valid web resource context to be added as dependency.",
                isWebResourceContext);
    }

    @Test
    public void when_RequestableDependencyKeyContainsWebResourceSymbol_Then_ShouldBeConsideredWebResourceContext() {
        // when
        final boolean isWebResourceContext = isWebResourceContext(SOURCE_REQUESTABLE_DEPENDENCY_KEY);
        // then
        assertFalse(
                "When requestable key contain the symbol ':', it should not be considered a valid web resource context to be added as dependency.",
                isWebResourceContext);
    }
}
