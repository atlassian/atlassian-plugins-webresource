package com.atlassian.plugin.webresource.cdn;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.integration.stub.WebResourceIntegrationImpl;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.webresource.api.WebResourceUrlProvider;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 */
public class TestGlobalStateHash {
    @Test
    public void testProductVersion() {
        checkDifferentState(
                (integration, batchingConfiguration, accessor) -> {
                    when(integration.getHostApplicationVersion()).thenReturn("7.0.0");
                },
                (integration, batchingConfiguration, accessor) -> {
                    when(integration.getHostApplicationVersion()).thenReturn("7.0.1");
                });
    }

    @Test
    public void testJsTryCatch() {
        checkDifferentState(
                (integration, batchingConfiguration, accessor) -> {
                    when(batchingConfiguration.isJavaScriptTryCatchWrappingEnabled())
                            .thenReturn(true);
                },
                (integration, batchingConfiguration, accessor) -> {
                    when(batchingConfiguration.isJavaScriptTryCatchWrappingEnabled())
                            .thenReturn(false);
                });
    }

    @Test
    public void testDifferentPlugins() {
        checkDifferentState(
                (integration, batchingConfiguration, accessor) -> {
                    mockPlugins(accessor, "plug1", "1.1");
                },
                (integration, batchingConfiguration, accessor) -> {
                    mockPlugins(accessor, "plug1", "1.1", "plug2", "1.0");
                });
    }

    @Test
    public void testSamePluginsInDifferentOrders() {
        checkSameState(
                (integration, batchingConfiguration, accessor) -> {
                    mockPlugins(accessor, "plug1", "1.1", "plug2", "1.0");
                },
                (integration, batchingConfiguration, accessor) -> {
                    mockPlugins(accessor, "plug2", "1.0", "plug1", "1.1");
                });
    }

    private void checkDifferentState(StateSetter state1, StateSetter state2) {

        String hash1a = makeConfig(state1).computeGlobalStateHash();
        String hash1b = makeConfig(state1).computeGlobalStateHash();
        String hash2 = makeConfig(state2).computeGlobalStateHash();

        assertEquals(hash1a, hash1b);
        assertNotEquals(hash1a, hash2);
    }

    private void checkSameState(StateSetter state1, StateSetter state2) {

        String hash1a = makeConfig(state1).computeGlobalStateHash();
        String hash1b = makeConfig(state1).computeGlobalStateHash();
        String hash2 = makeConfig(state2).computeGlobalStateHash();

        assertEquals(hash1a, hash1b);
        assertEquals(hash1a, hash2);
    }

    private Config makeConfig(StateSetter setter) {
        ResourceBatchingConfiguration batchingConfiguration = mock(ResourceBatchingConfiguration.class);
        WebResourceUrlProvider urlProvider = mock(WebResourceUrlProvider.class);
        ServletContextFactory servletContextFactory = mock(ServletContextFactory.class);
        TransformerCache transformerCache = mock(TransformerCache.class);
        PluginAccessor pluginAccessor = mock(PluginAccessor.class);

        WebResourceIntegration integration = WebResourceIntegrationImpl.create();
        when(integration.getHostApplicationVersion()).thenReturn("1.2.3");
        when(integration.getPluginAccessor()).thenReturn(pluginAccessor);

        setter.setup(integration, batchingConfiguration, pluginAccessor);

        return new Config(batchingConfiguration, integration, urlProvider, servletContextFactory, transformerCache);
    }

    private void mockPlugins(PluginAccessor accessor, String... args) {
        Iterator<String> rest = asList(args).iterator();
        List<Plugin> plugins = new LinkedList<>();
        while (rest.hasNext()) {
            String key = rest.next();
            String ver = rest.next();
            plugins.add(TestUtils.createTestPlugin(key, ver));
        }

        when(accessor.getEnabledPlugins()).thenReturn(plugins);
    }

    interface StateSetter {
        void setup(
                WebResourceIntegration integration,
                ResourceBatchingConfiguration batchingConfiguration,
                PluginAccessor accessor);
    }
}
