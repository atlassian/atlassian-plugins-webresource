package com.atlassian.util;

import java.io.IOException;
import java.io.InputStream;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import static com.fasterxml.jackson.databind.type.TypeFactory.defaultInstance;
import static java.util.Objects.requireNonNull;

/**
 * Class responsible for manipulating the data related to a JSON object.
 */
public final class JSONUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(JSONUtil.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private JSONUtil() {}

    @Nonnull
    public static <T> String toString(@Nonnull final T entity) {
        try {
            requireNonNull(entity, "The entity is mandatory.");
            return MAPPER.writeValueAsString(entity);
        } catch (final IOException exception) {
            LOGGER.error("Error while converting the entity to string.");
            throw new IllegalArgumentException(exception);
        }
    }

    /**
     * Read a certain JSON file and map its value to a certain bean type.
     *
     * @param path The of the file to be read.
     * @param type The {@link JavaType} used for the conversion.
     * @return The representation of the file content as a bean.
     */
    public static <T> T fileToEntity(final String path, final Class<T> type) {
        try {
            final InputStream file = JSONUtil.class.getResourceAsStream(path);
            return MAPPER.readValue(file, defaultInstance().constructType(type));
        } catch (final IOException exception) {
            LOGGER.error("Error while converting the file to bean.");
            throw new IllegalArgumentException(exception);
        }
    }

    /**
     * Read a certain JSON file and map its value to a certain bean type.
     *
     * @param path The of the file to be read.
     * @param type The {@link TypeReference} used for the conversion.
     * @return The representation of the file content as a bean.
     */
    public static <T> T fileToEntity(final String path, final TypeReference<T> type) {
        try {
            final InputStream file = JSONUtil.class.getResourceAsStream(path);
            return MAPPER.readValue(file, defaultInstance().constructType(type));
        } catch (final IOException exception) {
            LOGGER.error("Error while converting the file to bean.");
            throw new IllegalArgumentException(exception);
        }
    }
}
