# Host application upgrade notes

Version 5.2.0 includes more analytics to track effectiveness of resource processing caching and plugin system state
caching.

## Analytics

No effort is required to turn the analytics on, they are on by default.

Analytics whitelist will need to be updated to 3.89

There are some things that will need to be provided though:

- com.atlassian.plugin.webresource.WebResourceIntegration
  - add DarkFeatureManager
  - add EventPublisher
- com.atlassian.plugin.webresource.PluginResourceLocatorImpl
  - add EventPublisher

## Refactoring

There was some dead code removal and internal refactoring. Usages, implementations, and extensions were checked for with
marketplace scanner and sourcegraph targeting all latest versions of our products and apps.

If something does break, please ping in #server-frontend so we can see if there was a flaw in the checking process.
