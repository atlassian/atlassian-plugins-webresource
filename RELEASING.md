# Releasing

All packages in this repository are released as a public artifacts to [Atlassian's registry][pac].

> ## WARNING! Before running a release read the next paragraphs!

## Release steps

### Stable

These are official versions of the project, with a `major.minor.patch` number. e.g., `5.5.1`.

To perform a release:

1. Go to [the Bamboo builds on EcoBAC][builds].
2. Open the appropriate build for the `major.minor` version you wish to release.
   e.g., [here is the test build for WRM 5.5.x](https://ecosystem-bamboo.internal.atlassian.com/browse/PLUGWEBR-PLUGWEBRB5D5X).
   If an appropriate build does not exist, create one (see below).
3. Run the tests. Ensure they are green.
4. Open the "upmerge and release" build for the `major.minor` version you wish to release.
   e.g., [here is the release build for WRM 5.5.x](https://ecosystem-bamboo.internal.atlassian.com/browse/PLUGWEBR-PLUGWEBRB5D5XMR).
5. Run the build.
6. Run the manual "Release" stage.

### Hashed

You can release a "hashed" version, meant to allow early adopters to test the project in their product.

This will release a version number like `5.5.1-abc123def`.

1. Create an issue branch and make changes as normal.
2. Go to [the Bamboo builds on EcoBAC][builds].
3. Open the appropriate `major.minor` build.
4. Navigate to the branch build for your issue branch.
5. Run the release process from here as normal.

It **WILL** push version changes to your issue branch, but you don't need to merge the changes. Continue development as normal.

### Post-release

#### Documentation

We're releasing the documentation to [DAC][dac].
The docs are stored in [the Atlassian SDK documentation repository][docs].

To release new documentation:

1. Clone the docs repository.
2. Make the appropriate changes.
3. Raise a pull request to the repository.

When the PR is accepted, the documentation will be automatically published within the next 24 hours.

#### Upmerging

In most cases changes will automatically get merged from the `release/X.Y.Z` branch to newer `release` branches, and finally back to `master` branch.

If the "upmerge and release" build fails, then the merges will need to be performed manually.

Pushing a manual upmerge can be done via a Pull Request.

#### Announcing the release

Announce the new version in our Slack channel and `#server-platform-announcements` and post an internal blog post.

#### Updating the version on `master` branch

Once you release a new minor or a major version you might need to update the version on the `master` branch.

Refer to **Preparing a new minor or a major release from the `master` branch** paragraph for more information on how to do that.

## Preparing a new minor or a major release from the `master` branch

Before releasing a new minor or a major version from the `master` you should update the artifact versions on the `master` branch.
You can do that by using **Maven**.

Let's say we want to release a new minor version `3.4.0`. Open the terminal, and in the root of the project call those commands:

```shell
mvn versions:set --batch-mode -DgenerateBackupPoms=false -DnewVersion=3.4.0-SNAPSHOT
```

Next, verify the changes and commit those to you branch with commit message, .e.g:

```
chore(release): Prepare a new development version 3.4.0-SNAPSHOT
```

You should push your branch and create a pull request with the changes.


[builds]: https://ecosystem-bamboo.internal.atlassian.com/browse/PLUGWEBR
[pac]: https://packages.atlassian.com "Atlassian's registry"
[dac]: https://developer.atlassian.com/server/framework/clientside-extensions/ 'Atlassian Developer'
[docs]: https://bitbucket.org/atlassian-developers/atlassian-sdk-docs
