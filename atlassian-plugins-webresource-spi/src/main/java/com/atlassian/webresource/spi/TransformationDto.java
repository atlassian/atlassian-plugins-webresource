package com.atlassian.webresource.spi;

/**
 * Java DTO for `transformation` XML element. Needed to configure transformers from Java code.
 *
 * @since v3.5.13
 */
public class TransformationDto {
    /**
     * File extension this transformation should be applied to.
     */
    public final String extension;

    public final Iterable<TransformerDto> transformers;

    /**
     * @param extension    file extension this transformation should be applied to.
     * @param transformers list of transformers.
     */
    public TransformationDto(final String extension, final Iterable<TransformerDto> transformers) {
        this.extension = extension;
        this.transformers = transformers;
    }
}
