# Atlassian Webresources

A framework for Atlassian plugins to manage Javascript and CSS resources provided by plugins. The
framework allows for plugins to declare web resources, bind them to contexts in applications, and
set up dependencies so that requiring one resource will also pull in all its dependencies.

Batching of related resources into a single download is also provided.

## Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/PLUGWEB).

## Contributing

Refer to [the contribution guide](./CONTRIBUTING.md) for more details.

## Documentation

Overview:

- [Web Resource Plugin Module](https://developer.atlassian.com/docs/getting-started/plugin-modules/web-resource-plugin-module)
- [Web Resource Transformer Plugin Module](https://developer.atlassian.com/docs/getting-started/plugin-modules/web-resource-transformer-plugin-module)

Guides:

- [Clientside API](https://hello.atlassian.net/wiki/spaces/SVRFE/pages/491199354/WRM+Front-end+API)

Product-specific implementations:

- [JIRA Web Resources](https://developer.atlassian.com/display/JIRADEV/Web+Resource+Plugin+Module)
- [Confluence Web Resources](https://developer.atlassian.com/display/CONFDEV/Web+Resource+Module)


## Code Quality

This repository enforces the Palantir code style using the Spotless Maven plugin. Any violations of the code style will result in a build failure.

### Guidelines:
- **Formatting Code:** Before raising a pull request, run `mvn spotless:apply` to format your code.
- **Configuring IntelliJ:** Follow [this detailed guide](https://hello.atlassian.net/wiki/spaces/AB/pages/4249202122/Spotless+configuration+in+IntelliJ+IDE) to integrate Spotless into your IntelliJ IDE.

